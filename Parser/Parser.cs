using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;


namespace FileParser
{
    public class Parser // parasoft-suppress  CS.OOM.MI "Coding style - method logic was verified"
    {
        //const int header_size = 16;
        const int TimeStampSize = 4;
        bool NewLogVersion = true;
        bool TimeStampIncluded = false;
        DateTime fileCreatedDate;
        public int ParseFile(string fileName, bool openFile, string newFileName)// , string LogVersion) // parasoft-suppress  CS.MLC "Methods executing atomic functions" OOM.CYCLO "Coding style - method logic was verified" METRICS.MCCC "Coding style - method logic was verified" METRICS.MLOC "Methods executing atomic functions"

        {
            string path_name = Directory.GetCurrentDirectory();
            NewLogVersion = true;
            TimeStampIncluded = false; 
            if (null == fileName)
            {
                return 1;
            }
            //string NewFileName = FileName + ".txt";
            using (BinaryReader b = new BinaryReader(File.Open(fileName, FileMode.Open)))
            {
                /*  Rasem change for increasing log modules June 2016 */
                /*  Increasing string range */
                string[] CardNum2String = new string[100];
                string[] ModuleNum2String = new string[100];
                string[] MessageNum2String = new string[2000];

                int pos = 0;
               
                int becoolint32 = ReverseEndianess32Bit(b.ReadBytes(4));
                
                int header_size = (int)((becoolint32 & 0xff000000 )>> 24);
                int log_data_size;
                if( header_size == 0 )
                {
                    header_size = 12;
                    NewLogVersion = false;
                }
                becoolint32 = becoolint32 & 0xffffff;

                // read header
                if (becoolint32 == 0xbec001)
                {
                     TimeStampIncluded = false;  //changed to local 
                    int TimeStamp;
                    int length = (int)b.BaseStream.Length;
                    int LUTOffsetint32 = ReverseEndianess32Bit(b.ReadBytes(4));
                    bool CyclicOverRun = (0x80000000 & LUTOffsetint32) == 0x80000000;
                    LUTOffsetint32 = 0x0fffffff & LUTOffsetint32;
                    
                    int DataEndOffset = ReverseEndianess32Bit(b.ReadBytes(4)) - header_size;
                    int LogStartAddress = DataEndOffset;
                    if( NewLogVersion )
                    {
                        LogStartAddress = ReverseEndianess32Bit(b.ReadBytes(4)) - header_size; 
                    }
                    byte[] LogData = new byte[LUTOffsetint32 - header_size];  //12 is size of header

                    

                    int i = 0;
                    while (pos < LUTOffsetint32 - header_size)
                    {
                        // Read integer.
                        LogData[i] = b.ReadByte();
                        // Advance our position variable.
                        pos += sizeof(byte);
                        i++;
                    }


                    if (!NewLogVersion)
                    {
                        CyclicOverRun = 0xff != (LogData[DataEndOffset] & LogData[DataEndOffset + 1] & LogData[DataEndOffset + 2]);
                    }

                    if (CyclicOverRun)
                    {
                        byte[] tempbuff = new byte[LUTOffsetint32 - header_size];
                        int Address = LogStartAddress;
                        i = 0;
                        while (i < LUTOffsetint32 - header_size)
                        {
                            tempbuff[i] = LogData[Address]; i++; Address++;
                            if (Address == DataEndOffset)
                            {

                                break;
                            }
                            if (Address >= LUTOffsetint32 - header_size)
                                Address = 0;
                        }
                        log_data_size = i;
                        for (i = 0; i < LUTOffsetint32 - header_size; i++)
                        {
                            LogData[i] = tempbuff[i];
                        }



                    }
                    else
                    {
                        log_data_size = DataEndOffset;
                    }
                    

                    i = 0;
                    /*  Rasem change for increasing log modules June 2016 */
                    /*  Increasing string range */
                    char[] LUTData = new char[length - LUTOffsetint32];
                    int bread = b.Read(LUTData, 0, length - LUTOffsetint32);
                    string LUTStringData = new string(LUTData);
                    string[] dataarray = new string[6000];
                    dataarray = LUTStringData.Split('[');
                    string[] CardNumArray = new string[1000];
                    string[] ModuleNumArray = new string[1000];
                    string[] MessageTypeArray = new string[2000];
                    string[] ParamsArray = new string[1000];
                   
                    //ParamsString = ParamsString.;

                    CardNumArray = String2Array(dataarray[1]);
                    ModuleNumArray = String2Array(dataarray[2]);
                    MessageTypeArray = String2Array(dataarray[3]);
                    
                    
                    string LUTDataString = new string(LUTData);
                    bool SysParamsIncluded = LUTDataString.Contains("SYS_PARAMS");
                    TimeStampIncluded = SysParamsIncluded;
                    /*foreach( string stringcell in MessageTypeArray)
                    {
                        if( stringcell.Contains("MAIN_AUX_BAT_FILT") )
                        {
                            TimeStampIncluded = true;
                            TimeStampFlagIncluded();
                        }
                    }*/
                    bool end_of_log = false;

                    pos = 0;
                    int line_num = 1;
                    using (StreamWriter outfile = new StreamWriter(newFileName))
                    {
                        int MsgNum = 0;
                        int PrevMsgNum = 0;
                        if (SysParamsIncluded)
                        {
                            ParamsArray = dataarray[4].Split(']');
                            ParamsArray = ParamsArray[1].Split('>');
                            fileCreatedDate = File.GetCreationTime(fileName);                          
                            //DateTime CurrentTime = DateTime.Now;
                            string headerline = "Rewalk log downloaded at " + fileCreatedDate;
                            outfile.WriteLine(headerline);
                            outfile.WriteLine("***********************************************************");
                            outfile.WriteLine();
                            string line2erite;
                            foreach (string line in ParamsArray)
                            {
                                line2erite = line.Replace('<', ' ');
                                outfile.WriteLine(line2erite);
                            }
                        }
                        
                        while (!end_of_log)
                        {
                            MsgNum = (LogData[pos] << 8); pos++;
                            MsgNum += LogData[pos]; pos++;
                           // MsgNum = (LogData[pos++] << 8) + LogData[pos++];
                            if ( (PrevMsgNum != 0xFFFB) && !NewLogVersion )// && (!CyclicOverRun) )
                            {

                                if (PrevMsgNum != MsgNum - 1)
                                {

                                    while ((MsgNum != PrevMsgNum + 2) && pos <= LUTOffsetint32 - header_size - 2)
                                    {
                                        //pos--;
                                       // MsgNum = (LogData[pos++] << 8) + LogData[pos++];
                                        MsgNum = (LogData[pos] << 8); pos++;
                                        MsgNum += LogData[pos]; pos++;
                                    }
                                }
                            }
                            
                            PrevMsgNum = MsgNum;

                            string LogLine = "";
                            int CardNum = LogData[pos++];
                            if (0xff == CardNum)
                            {
                                break;
                            }

                            int ModuleNum = LogData[pos++];

                            LogLevel loglevel = new LogLevel();
                            loglevel.Myloglevel = LogData[pos]; pos++;
                       
                            int NumofData = LogData[pos++];
                            if (NumofData > 0)
                            {
                                int stringparam1 = LogData[pos++];
                                if (NumofData > 1)
                                {
                                    int stringparam2 = LogData[pos++];
                                    if (TimeStampIncluded)
                                    {
                                        int hours, minutes, seconds, ms;
                                        TimeStamp = (LogData[pos] << 24); pos++;
                                        TimeStamp = TimeStamp + (LogData[pos] << 16); pos++;
                                        TimeStamp = TimeStamp + (LogData[pos] << 8); pos++;
                                        TimeStamp = TimeStamp + LogData[pos]; pos++;
                                        TimeStamp = TimeStamp * 10; // timestamp in ms
                                        hours = TimeStamp / 3600000;// hours
                                        minutes = (TimeStamp - hours * 3600000) / 60000;
                                        seconds = (TimeStamp - hours * 3600000 - minutes*60000) / 1000;
                                        ms = TimeStamp - hours * 3600000 - minutes * 60000 - seconds*1000;
                                        LogLine = LogLine + hours.ToString("D4") + ":" + minutes.ToString("D2") + ":" + seconds.ToString("D2") + ":" + ms.ToString("D4") + " ";

                                    }
                                       
                                    
                                    if (NumofData > 2)
                                    {

                                            LogLine = LogLine + line_num.ToString("D6") + " [" + MsgNum.ToString("D6") + ", ";
                                            LogLine = LogLine + ModuleNumArray[ModuleNum] + ", ";
                                            LogLine = LogLine + CardNumArray[CardNum] + ", ";
                                            LogLine = LogLine + loglevel.MyLoglevelstring + "]\r\n";
                                            LogLine = LogLine + "                             " + MessageTypeArray[stringparam1] + " ";
                                            LogLine = LogLine + MessageTypeArray[stringparam2] + " [";
                                            if(TimeStampIncluded)
                                            {
                                                for (int j = 0; j < NumofData - 2 - TimeStampSize; j++)
                                                {
                                                    /*  Rasem change for bluetooth May 2016 */
                                                    if ((pos >= (int)0x0c) && (ModuleNum== (int)0x0c))  // || (pos=0x0e))
                                                    { LogLine = LogLine + (char)LogData[pos] ; pos++; }
                                                    else
                                                    { LogLine = LogLine + LogData[pos].ToString("X2") + " "; pos++; }
                                                    /****** End rasem change for displaying bluetooth message as characters*/

                                                }
                                            
                                            }
                                            else
                                            {
                                                for (int j = 0; j < NumofData - 2; j++)
                                                {

                                                    LogLine = LogLine + LogData[pos].ToString("X2") + " "; pos++;

                                                }
                                            }
                                            LogLine = LogLine + "]";
                                        }
                                    
                                }
                            }
                            outfile.WriteLine(LogLine);
                            outfile.WriteLine("");
                            
                            line_num++;
                            if (pos >= log_data_size)
                            {
                                break;
                            }
                        }
                        if (openFile)
                        {
                            Process.Start("notepad", newFileName);
                            return 0;
                           
                        }
                    }
                    i = 2;

                }
                else
                {
                    return 2; 

                }
            }
            return 0;
        }

        public bool TimeStampFlagIncluded ()
        {
            bool IntTimeStamp = TimeStampIncluded;
            return IntTimeStamp; 
        }

        private string [] String2Array( string stringData )
        {

            /*  Rasem change for increasing log modules June 2016 */
            /*  Increasing string range */
            string [] string_arr = new string[1000];//range increased
            string[] temp_string_arr = new string[1000];//range increased
            temp_string_arr = stringData.Split('<');
            int elementnum = 0;
            foreach (string element in temp_string_arr)
            {
                string[] elementarray = new string[1000];
                int CardNum = 0;
                if (element.Contains(']'))
                {
                    continue;
                }
                elementarray = element.Split(',');
                try
                {
                    CardNum = Convert.ToInt32(elementarray[0]);
                }
                catch
                {
                    throw; 
                }
                elementarray = elementarray[1].Split('>');
                string_arr[CardNum] = elementarray[0];
                elementnum++;

            }
            string [] ret_string_arr = new string[elementnum];
            for( int j=0; j < elementnum; j++  )
            {
                ret_string_arr[j] = string_arr[j];
            }
            return ret_string_arr;
        }

        private int ReverseEndianess32Bit(byte[] data)
        {
            
            Array.Reverse(data);
            return(BitConverter.ToInt32(data, 0));
        }

        private sealed class LogLevel
        {
            public int Myloglevel
            {
                get { return Loglevel; }
                
                set 
                {
                    Loglevel = value;
                    switch (value)
                    {
                        case 0x1:
                            Loglevelstring = "ERROR";
                            break;
                        case 0x2:
                            Loglevelstring = "WARNING";
                            break;
                        case 0x4:
                            Loglevelstring = "DATA";
                            break;
                        default:
                            Loglevelstring = "UNKNOWN LEVEL";
                            break;
                    }
 
                }
            }
            public string MyLoglevelstring
            {
                get { return Loglevelstring; }
              
            }
            
            //get{ return loglevel};
            private string Loglevelstring;
            private int Loglevel;
        } 
        


    }
}
