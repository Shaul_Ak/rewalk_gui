﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RewalkControls
{
    /// <summary>
    /// Interaction logic for CommandCtrl.xaml
    /// </summary>
    public partial class CommandCtrl : UserControl
    {
        public event Action<FrameworkElement ,byte[]> Run; // parasoft-suppress  CMUG.EVU.ECEE "Coding style - method logic was verified"

      

        public CommandCtrl()
        {
            InitializeComponent();
        }

        public bool  SetResult(byte[] result)
        {
            int index = 2;
            foreach (TextBox tb in lower.Children)
            {
                tb.Text = result[index].ToString("X", System.Globalization.CultureInfo.InvariantCulture);
                index++;
            }
            return true;
        }

        public string IDLabel
        {
            get { return lblID.Text; }
            set { lblID.Text = value; }
        }

        public string ID
        {
            get { return tbID.Text; }
            set { tbID.Text = value; }
        }

        public string ButtonText
        {
            get { return btnRun.Content as string; }
            set { btnRun.Content = value; }
        }

        public string DisableIDBox
        {
            set { tbID.IsEnabled = false; }
        }

        public string ZeroSendBox
        {
            set
            {
                Sb0_t.Text = "00";
                Sb1_t.Text = "00";
                Sb2_t.Text = "00";
                Sb3_t.Text = "00";
                Sb4_t.Text = "00";
                Sb5_t.Text = "00";
                Sb6_t.Text = "00";
                Sb7_t.Text = "00";
            }
        }

        public string ClearRecvBox
        {
            set
            {
                Rb0_t.Text = "";
                Rb1_t.Text = "";
                Rb2_t.Text = "";
                Rb3_t.Text = "";
                Rb4_t.Text = "";
                Rb5_t.Text = "";
                Rb6_t.Text = "";
                Rb7_t.Text = "";
            }
        }

        public string  SetBRBoxBackgroundWhite
        {
            set { tbID.Background = Brushes.White; }
        }
        private void OnRun(object sender, RoutedEventArgs e)
        {
            byte[] parts = new byte[10];
            
            ushort id = 0;
            bool answ = ushort.TryParse(tbID.Text, System.Globalization.NumberStyles.AllowHexSpecifier, System.Globalization.CultureInfo.InvariantCulture, out id);
            parts[0] = (byte)id;
            parts[1] = (byte)(id >> 8);

            int index = 2;
            foreach(TextBox tb in upper.Children)
            {
                bool answer = byte.TryParse(tb.Text, System.Globalization.NumberStyles.AllowHexSpecifier, System.Globalization.CultureInfo.InvariantCulture, out parts[index]);
                index++;
            }
            
            if (Run!= null)
                Run(this, parts);
        }

     

       
        
    }
}
