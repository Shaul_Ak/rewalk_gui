﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Rewalk.Device;

namespace RewalkControls
{
    /// <summary>
    /// Interaction logic for StatusCtrl.xaml
    /// </summary>
    public partial class StatusCtrl : UserControl
    {
        static StatusCtrl()
        {
        }
        public    StatusCtrl()
        {
            InitializeComponent();

            timer = new DispatcherTimer(TimeSpan.FromMilliseconds(200), DispatcherPriority.Background, OnInProgressTimer, this.Dispatcher);
            timer.IsEnabled = false;
        }

       
        public string Label
        {
            get { return tbLabel.Text; }
            set { tbLabel.Text = value; }
        }

        public TestStatus Status
        {
            get { return (TestStatus)GetValue(StatusProperty); }
            set { SetValue(StatusProperty, value); }
        }

        public static readonly DependencyProperty StatusProperty =
            DependencyProperty.Register("Status", typeof(TestStatus), typeof(StatusCtrl), new UIPropertyMetadata(TestStatus.Unknown, OnStatusChanged));

        public static void OnStatusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            StatusCtrl control = d as StatusCtrl;

            if (control == null) return;

            if (control.Status == TestStatus.Unknown)
            {
                control.timer.Stop();
                control.ellipse.Fill = unknownBrush;
            }
            else if (control.Status == TestStatus.InProgress)
            {
                control.ellipse.Fill = inProgressBrush;
                control.timer.Start();
            }
            else if (control.Status == TestStatus.Fail)
            {
                control.timer.Stop();
                control.ellipse.Fill = failBrush;
            }
            else if (control.Status == TestStatus.Success)
            {
                control.timer.Stop();
                control.ellipse.Fill = successBrush;
            }
        }

        public short ErrorCode
        {
            get { return (short)GetValue(ErrorCodeProperty); }
            set { SetValue(ErrorCodeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ErrorCode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ErrorCodeProperty =
            DependencyProperty.Register("ErrorCode", typeof(short), typeof(StatusCtrl), new UIPropertyMetadata((short)0, OnErrorCodeChanged));


        public static void OnErrorCodeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            StatusCtrl control = d as StatusCtrl;
            if (control != null)
            {
                control.tbError.Text = "Er: " + control.ErrorCode.ToString();
                control.tbError.Visibility = (control.ErrorCode == 0) ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        private void OnInProgressTimer(object sender, EventArgs e)
        {
            if (ellipse.Fill == inProgressBrush)
            {
                ellipse.Fill = inProgressBrushBlink;
            }
            else
            {
                ellipse.Fill = inProgressBrush;
            }
        }

        private DispatcherTimer timer;

        private static SolidColorBrush unknownBrush = new SolidColorBrush(Color.FromRgb(0xBB, 0xBB, 0xBB));
        private static SolidColorBrush failBrush = new SolidColorBrush(Color.FromRgb(0xDD, 0x11, 0x11));
        private static SolidColorBrush inProgressBrush = new SolidColorBrush(Color.FromRgb(0xDD, 0xDD, 0x11));
        private static SolidColorBrush inProgressBrushBlink = new SolidColorBrush(Color.FromRgb(0xFF, 0xFF, 0x00));
        private static SolidColorBrush successBrush = new SolidColorBrush(Color.FromRgb(0x11, 0xBB, 0x11));
    }
}
