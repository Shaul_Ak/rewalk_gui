﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;


namespace ADIHostInstaller
{
    sealed class Program
    {
        static void Main(string[] args)
        {

            string SysArch = Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE"); // parasoft-suppress  SEC.AASV "Calls to access enviroment variables were inspected and verified"

            string path = "";


            for (int i = 0; i < args.Length; i++)
            {
                if (i < args.Length - 1)
                    path = path + args[i] + " ";
                else
                    path = path + args[i];
            }
            // Console.WriteLine(SysArch);
            // Console.WriteLine(path);

            if ("x86" == SysArch)
            {
                path = path + "bulkadi/dpinst32.exe";
            }
            else
            {
                path = path + "bulkadi/dpinst64.exe";
            }
            //Console.WriteLine(path);

            Process.Start(Valid(path));

        }


        public static string Valid(string s)
        {


            char[] dest = new char[5000];
            int i;
            string p = "";

            if (s != null)
            {
                s.CopyTo(0, dest, 0, s.Length);
                for (i = 0; i < s.Length; i++)
                {
                    p = p + dest[i];
                }
                return p;
            }
            else
            {
                return "Not known";
            }

        } 

    }

}
