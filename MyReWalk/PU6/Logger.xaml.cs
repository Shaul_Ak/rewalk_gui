﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PU
{
    /// <summary>
    /// Interaction logic for logger.xaml
    /// </summary>
    public partial class Logger : Window
    {
        public Logger()
        {
            InitializeComponent();
        }
        public static void Error(string message, string ex)
        {

            string s_ex = ex;

            if (s_ex == "Could not connect")
                s_ex = " : Could not connect.";
            else
                s_ex = "";

            MessageBox.Show(message + s_ex);
        }

        public static void Dummy_Error(string message, string ex)
        {

            string s_ex = ex;

            if (s_ex == "Could not connect")
                s_ex = " : Could not connect.";
            else
                s_ex = "";


        }


        public static string Validate(string s)
        {
            //some validation
            string ss = s;
            string s_version = "";
            int i;
            if (ss.Contains("Rewalk"))
            {
                char[] destination = new char[50];


                ss.CopyTo(0, destination, 0, ss.Length);

                for (i = 0x11; i <= ss.Length; i++)
                {
                    if (destination[i] != '\0')
                    {
                        s_version = s_version + destination[i];
                    }
                    else
                    {
                        break;
                    }

                }
                return s_version;
            }
            else
            {
                return "Not known";
            }


        }





        public static void Error_MessageBox_Advanced(string message, string message_title, string ex)
        {

            string s_ex = ex;

            if (s_ex == "Could not connect")
                s_ex = " : Could not connect.";
            else
                s_ex = "";

            MessageBox.Show(message + s_ex, message_title, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        //Trans
        public static void Show_Selected_MessageBox_Exception(string message, string message_title, string ex)
        {
            string s_ex;
            s_ex = ex;
            MessageBox.Show(message + s_ex, message_title.ToString(), MessageBoxButton.OK, MessageBoxImage.Error);
        }

    }
}
