﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PU
{
    /// <summary>
    /// Interaction logic for GenerateRCAddressID.xaml
    /// </summary>
    public partial class GenerateRCAddressID : Window
    {
        public GenerateRCAddressID()
        {
            InitializeComponent();
            iSDone = false;
        }

        public void Init(int cbIndex, string sn2, string sn3)
        {
            tbGenrateAddressIDSn2.Text = sn2;
            InitialSN2 = sn2;
            tbGenrateAddressIDSn3.Text = sn3;
            InitialSN3 = sn3;
            InitialIndex = cbIndex;
            GenerateWindow_SN_ComBox.SelectedIndex = cbIndex;
        }

        public void OnGenerateAddressID(object sender, RoutedEventArgs e)
        {
           
            iSDone = false;
            if (tbGenrateAddressIDSn2.Text.Length > 2 ||
                tbGenrateAddressIDSn3.Text.Length > 4 ||
                !tbGenrateAddressIDSn2.Text.All(char.IsDigit) ||
                !tbGenrateAddressIDSn3.Text.All(char.IsDigit))
            {
               
                MessageBox.Show((string)this.FindResource("Illegal input please input only digits !"), (string)this.FindResource("Input Error"), MessageBoxButton.OK, MessageBoxImage.Error);
           
                return;
            }

            if (InitialSN2 != tbGenrateAddressIDSn2.Text ||
                InitialSN3 != tbGenrateAddressIDSn3.Text ||
                InitialIndex != GenerateWindow_SN_ComBox.SelectedIndex)
            {
                MessageBoxResult Result;
                Result = MessageBox.Show((string)this.FindResource("SN inputed is different from system SN.\nDo you want to proceed?"), (string)this.FindResource("System SN Alert"), MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                       //while (Result != MessageBoxResult.None) ;
                if (Result != MessageBoxResult.Yes)
                    return;

            }
            ushort SN = Convert.ToUInt16(tbGenrateAddressIDSn3.Text);


            iD = (ushort)(GetSystemType() * 10000 + SN);
            address = (ushort)(1 + (SN - SN / 1000 * 1000) % 255);

            iSDone = true;
            this.Close();
            return;
        }

        public ushort GetSystemType()
        {
            ushort SysType = 0;
            switch (GenerateWindow_SN_ComBox.SelectedIndex)
            {
                case 0:
                    SysType = 2;
                    break;
                case 1:
                    SysType = 3;
                    break;
                case 2:
                    SysType = 4;
                    break;
                default:
                    SysType = 2;
                    break;

            }
            return SysType;
        }

        public ushort Address
        {
            get { return address; }
            set { address = value; }
        }


        public ushort ID
        {
            get { return iD; }
            set { iD = value; }
        }

        public bool IsDone
        {
            get { return iSDone; }
            set { iSDone = value; }
        }

        private ushort address;
        private ushort iD;
        private bool iSDone;

        public void On_Close()
        {
            Close();
            return;
        }

        private string InitialSN2, InitialSN3;
        private int InitialIndex;




    }
}
