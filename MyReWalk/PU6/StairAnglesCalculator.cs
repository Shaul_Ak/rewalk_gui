﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReWalk6
{
    class StairAnglesCalculator
    {
        
        private const double HipMaxMotorSpeed = 56 ;
        private const double KneeMaxMotorSpeed = 78;
        private const double HipKneeMaxMotorSpeedratio = KneeMaxMotorSpeed / HipMaxMotorSpeed;

        public struct StairsFlexions
        {
            public double Ack_Movement_Point
            {
                get { return ack_movement_point; }
                set { ack_movement_point = value; }
            }
            

            public double ASC_LH
            {
                get { return asc_lh; }
                set { asc_lh = value; }
            }
            public double ASC_LK
            {
                get { return asc_lk; }
                set { asc_lk = value; }
            }
            public double ASC_RH
            {
                get { return asc_rh; }
                set { asc_rh = value; }
            }
            public double ASC_RK
            {
                get { return asc_rk; }
                set { asc_rk = value; }
            }
            public double DSC_LH
            {
                get { return dsc_lh; }
                set { dsc_lh = value; }
            }
            public double DSC_Lk
            {
                get { return dsc_lk; }
                set { dsc_lk = value; }
            }
            public double DSC_RH
            {
                get { return dsc_rh; }
                set { dsc_rh = value; }
            }
            public double DSC_RK
            {
                get { return dsc_rk; }
                set { dsc_rk = value; }
            }

            public double PLACING_RH
            {
                get { return placing_rh; }
                set { placing_rh = value; }
            }
            public double PLACING_RK
            {
                get { return placing_rk; }
                set { placing_rk = value; }
            }


            private double asc_lh;
            private double asc_lk;
            private double asc_rh;
            private double asc_rk;
            private double dsc_lh;
            private double dsc_lk;
            private double dsc_rh;
            private double dsc_rk;
            private double placing_rh;
            private double placing_rk;
            private double ack_movement_point;



        }
        public StairsFlexions CalculateStarisAngle(double upperleglength, double lowerleglength, double shoeSize, double stairHeight)
        {
            StairsFlexions StairsFlexionResults = new StairsFlexions();
            

            StairsFlexionResults.DSC_RK = 30;
            StairsFlexionResults.ASC_LH = 35;
            StairsFlexionResults.ASC_LK = 85;


            double system_width = 70;
            double SoleHeightMeasurement = 30;
            //ASC Right Knee Equations 
            double ToltalLowerLegLength = lowerleglength + SoleHeightMeasurement;
            double ShoeSizeCenter = shoeSize - system_width;
            double RadiusShoeLowerleglength = Math.Sqrt(Math.Pow(ShoeSizeCenter, 2) + Math.Pow(ToltalLowerLegLength, 2));
            double ShoeKneeFlexionSlope = (Math.Atan((ShoeSizeCenter) / (ToltalLowerLegLength)) * 180 / Math.PI);
            double ASCRightKnee_Alpha_2 = (Math.Acos((ToltalLowerLegLength - stairHeight) / RadiusShoeLowerleglength) * 180 / Math.PI);
            ///////////////////////////__//////////////
            double ASCRightKnee_Alpha_3 = (Math.Atan((system_width) / ToltalLowerLegLength) * 180 / Math.PI);


            StairsFlexionResults.Ack_Movement_Point = (Math.Atan((ShoeSizeCenter) / (upperleglength + ToltalLowerLegLength - stairHeight)) * 180 / Math.PI);
            StairsFlexionResults.Ack_Movement_Point = StairsFlexionResults.Ack_Movement_Point * HipKneeMaxMotorSpeedratio;
            StairsFlexionResults.Ack_Movement_Point = Math.Round(StairsFlexionResults.Ack_Movement_Point);




            StairsFlexionResults.ASC_RK = ShoeKneeFlexionSlope + ASCRightKnee_Alpha_2 + ASCRightKnee_Alpha_3;
            StairsFlexionResults.ASC_RK = Math.Round(StairsFlexionResults.ASC_RK);
            StairsFlexionResults.Ack_Movement_Point = StairsFlexionResults.ASC_RK - StairsFlexionResults.Ack_Movement_Point;
            //double ASCRightKnee = ASCRightKnee_Alpha_1 + ASCRightKnee_Alpha_2;// +ASCRightKnee_Alpha_3; 
            //ASCRightKnee = Math.Round(ASCRightKnee);

            double KneeFlexion_X = Math.Cos(((StairsFlexionResults.ASC_RK - ASCRightKnee_Alpha_3) * Math.PI) / 180) * ToltalLowerLegLength;
            double KneeFlexion_Y = Math.Sin(((StairsFlexionResults.ASC_RK - ASCRightKnee_Alpha_3) * Math.PI) / 180) * ToltalLowerLegLength;

            double ASCRightHip_Beta_1 = Math.Atan(KneeFlexion_Y / (upperleglength + KneeFlexion_X)) * 180 / Math.PI;
            double ASCRightHip_Beta_2 = Math.Atan((system_width) / (upperleglength + KneeFlexion_X)) * 180 / Math.PI;
            double ASCRightHip_Beta_3 = Math.Atan(ShoeSizeCenter / (upperleglength + KneeFlexion_X)) * 180 / Math.PI;


            StairsFlexionResults.ASC_RH = ASCRightHip_Beta_3 + ASCRightHip_Beta_2 + ASCRightHip_Beta_1;
            StairsFlexionResults.ASC_RH = Math.Round(StairsFlexionResults.ASC_RH);
            //double ASCRightHip = ASCRightHip_Beta_3 + ASCRightHip_Beta_2 + ASCRightHip_Beta_1;
            //ASCRightHip = Math.Round(ASCRightHip);

            double PlacingRighHip_Alpha = Math.Asin(((shoeSize) / upperleglength)) * 180 / Math.PI;

            StairsFlexionResults.PLACING_RH = StairsFlexionResults.ASC_RH - PlacingRighHip_Alpha;
            StairsFlexionResults.PLACING_RH = Math.Round(StairsFlexionResults.PLACING_RH);
            // double PlacingRighHip = StairsFlexionResults.ASC_RH - PlacingRighHip_Alpha;//Sub foot backheel 
            // PlacingRighHip = Math.Round(PlacingRighHip);

            double PlacingRightKnee_Beta = Math.Atan((system_width / ToltalLowerLegLength)) * 180 / Math.PI;
            StairsFlexionResults.PLACING_RK = StairsFlexionResults.ASC_RK - PlacingRighHip_Alpha - PlacingRightKnee_Beta;
            StairsFlexionResults.PLACING_RK = Math.Round(StairsFlexionResults.PLACING_RK);

            //double PlacingRightKnee = StairsFlexionResults.ASC_RK - PlacingRighHip_Alpha - PlacingRightKnee_Beta;
            //PlacingRightKnee = Math.Round(PlacingRightKnee);
            //////////////////////////////////DSC Stairs Calculation Angles ///////////////////////

            // double DSCRightKnee = 30; //Constant 

            double DSCRightKnee_X = Math.Sin((StairsFlexionResults.DSC_RK * Math.PI) / 180) * ToltalLowerLegLength;
            double DSCRightKnee_Y = Math.Cos((StairsFlexionResults.DSC_RK * Math.PI) / 180) * ToltalLowerLegLength;

            double DSCRightHip_Aplha_1 = Math.Atan(DSCRightKnee_X / (upperleglength + DSCRightKnee_Y)) * 180 / Math.PI;
            double DSCRightHip_Aplha_2 = Math.Atan(system_width / (upperleglength + ToltalLowerLegLength)) * 180 / Math.PI;
            double DSCRightHip_Aplha_3 = Math.Atan(ShoeSizeCenter / (upperleglength + DSCRightKnee_Y)) * 180 / Math.PI;

            StairsFlexionResults.DSC_RH = DSCRightHip_Aplha_1 + DSCRightHip_Aplha_2 + DSCRightHip_Aplha_3;
            StairsFlexionResults.DSC_RH = Math.Round(StairsFlexionResults.DSC_RH);

            //double DSCRightHip = DSCRightHip_Aplha_1 + DSCRightHip_Aplha_2 + DSCRightHip_Aplha_3;
            //DSCRightHip = Math.Round(DSCRightHip);

            double RadiusKneeFlexion = Math.Sqrt(Math.Pow(system_width, 2) + Math.Pow((upperleglength + ToltalLowerLegLength - stairHeight), 2));

            double DSCLeftKnee_Alpha = Math.Acos((Math.Pow(upperleglength, 2) + Math.Pow(ToltalLowerLegLength, 2) - Math.Pow(RadiusKneeFlexion, 2)) / (2 * ToltalLowerLegLength * upperleglength)) * 180 / Math.PI;



            StairsFlexionResults.DSC_Lk = (180 - DSCLeftKnee_Alpha) + ShoeKneeFlexionSlope;
            StairsFlexionResults.DSC_Lk = Math.Round(StairsFlexionResults.DSC_Lk);

            // double DSCLeftKnee       = (180 - DSCLeftKnee_Alpha) + ASCRightKnee_Alpha_1;
            //DSCLeftKnee = Math.Round(DSCLeftKnee);


            double DSCLeftHip_Alpha_1 = Math.Acos((Math.Pow(ToltalLowerLegLength, 2) - Math.Pow(RadiusKneeFlexion, 2) - Math.Pow(upperleglength, 2)) / (-2 * upperleglength * RadiusKneeFlexion)) * 180 / Math.PI;
            double DSCLeftHip_Alpha_2 = Math.Atan(system_width / (upperleglength + ToltalLowerLegLength)) * 180 / Math.PI;



            StairsFlexionResults.DSC_LH = DSCLeftHip_Alpha_1 + DSCLeftHip_Alpha_2;
            StairsFlexionResults.DSC_LH = Math.Round(StairsFlexionResults.DSC_LH);

            // double DSCLeftHip = DSCLeftHip_Alpha_1 + DSCLeftHip_Alpha_2;
            // DSCLeftHip = Math.Round(DSCLeftHip);

            return StairsFlexionResults;

            // return new double[] { ASCRightHip, ASCRightKnee, PlacingRighHip, PlacingRightKnee, DSCLeftHip, DSCLeftKnee, DSCRightHip }; 
        }
  
    }
}
