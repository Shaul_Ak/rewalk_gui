﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Rewalk.Device;

namespace PU
{
    /// <summary>
    /// Interaction logic for Report_Progress.xaml
    /// </summary>
    public partial class Report_Progress : Window
    {
        public Report_Progress()
        {
            InitializeComponent();

        }

        public void AppendProgress(string text)
        {
            this.rtfUserReport.AppendText(text);
        }

        public void Close_Report()
        {
            //rtfUserReport.AppendText("Please contact technical support before using the system again.");
            //  btnClose.IsEnabled = true;
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            Close(); //this.Close();
        }

        private void btnClearError_Click(object sender, RoutedEventArgs e)
        {
            Data deviceData = new Data();
            deviceData.ClearSystemError();
            deviceData.Parameters.SysErrorType = deviceData.GetErrorStatus();
            // tbSysErr.Text = deviceData.Parameters.SysErrorType == -1 ? "None" : deviceData.Parameters.SysErrorType.ToString();
            MessageBoxResult result = MessageBox.Show((string)this.FindResource("System error cleared successfully !"), (string)this.FindResource("Confirmation"), MessageBoxButton.OK, MessageBoxImage.Exclamation);
            while (result != MessageBoxResult.OK) ;

           // btnClearError.Visibility = Visibility.Collapsed;
            Close();
      //      (App.Current as App).DeptName_BrowseSaveAsFlashLogFile = true;
            //  btnFlashLogBrowse.IsEnabled = false;

        }
        private void WinClose(object sender, System.Windows.RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
