/*********************************************************************************

Copyright(c) 2005 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

#if !defined CONSOLE_H
#define CONSOLE_H

#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include <windows.h>

void SaveConsoleAttributes();
void RestoreConsoleAttributes();

void  Clrscr();
COORD GetCurrentCursorPosition();
void  SetCurrentCursorPosition(int, int);
void  SetCurrentCursorPosition(COORD);
void  SetRGB(int);

int   ColorPrintf(int color, const char * str);
int   ColorPrintf(int color, const char * str, int   arg1);
int   ColorPrintf(int color, const char * str, char* arg1);


enum TEXTCOLOR
{
	WHITE = 0,
	BR_WHITE,
	RED,
	BR_RED,
	YELLOW,
	BR_YELLOW,
	GREEN,
	BR_GREEN,
	CYAN,
	BR_CYAN,
	MAGENTA,
	BR_MAGENTA,
	NUM_COLORS,
};

#endif
