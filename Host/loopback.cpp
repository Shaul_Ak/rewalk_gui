/*********************************************************************************

Copyright(c) 2005 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <setupapi.h>
#include <time.h>
#include "usbcmd.h"
#include "usbdriver.h"
#include "console.h"

extern int GetFirmwareVersionStrings(BYTE* pbyVersion);
void PrintLoopbackError(COORD origin, char * str);

#define KBYTE ((double)1024)
#define MBYTE ((double)1024*KBYTE)
#define GBYTE ((double)1024*MBYTE)
#define TBYTE ((double)1024*GBYTE)


// choose whether to display (1) or supress (0) printf debug information in this file
#if 1
	#define NOISY(_x_) printf _x_ ;
	#define CNOISY(c, _x_) SetRGB(c);printf _x_;SetRGB(WHITE);
#else
	#define NOISY(_x_) ;
	#define CNOISY(_x_);
#endif

/////////////////////////////////////////////////////
// extern globals
/////////////////////////////////////////////////////

extern DWORD g_dwTotalDevices;					// total devices detected
extern UINT g_unDeviceNumber;					// device number selected
extern char g_pszDeviceDesc[];					// device name string for display only
extern HANDLE g_hRead;							// USB device read handle
extern HANDLE g_hWrite;							// USB device write handle
extern CONSOLE_SCREEN_BUFFER_INFO gSavedCsbi;	// screen info

/******************************************************************************
Routine Description:

    Runs a loopback test

Arguments:

    BOOL bRandomSize -	Indicates if we should loop random data sizes
	UINT uiNumBytes -	If random this indicates the maximum transfer size, else
						indicates the transfer size of each loop
	UINT uiLoops -		Number of loops to execute, NULL is the default argument
						and if NULL we loop until user quits

Return Value:

    int - return status

******************************************************************************/

int Loopback( BOOL bRandomSize, UINT uiMaxDataBytes, UINT uiLoops = NULL )
{
	char *pcOutBuff = NULL;						// output buffer pointer
	char *pcInBuff = NULL;						// input buffer pointer
	char * pcTemp;								// temp pointer
	USBCB usbcb;								// USB control block
	BOOL bKeepRunning = TRUE;					// flag to keep running loopback
	UINT *puiOutBuffHeader = NULL;				// output buffer header pointer
	UINT nDataBytesThisRound = 0;				// data bytes for the current transfer
	UINT nDataBytesNextRound = 0;				// data bytes for the next transfer
	ULONG nCountRcvd = 0;						// received byte count
	ULONG nCountSent = 0;						// sent byte count
	ULONG nCountCurrent = 0;					// current byte count
	double dTotalRcvd = 0;						// total received byte count
	double dTotalSent = 0;						// total sent byte count
	double dTotalBytes = 0;						// total byte count
	UINT nErrorCount = 0;						// data error count
	UINT uiLoopCount = 0;						// loop count
	ULONG i = 0;								// index
    BOOL IoStatus = TRUE;						// IO status
	UINT nWriteLoopCtr = 0;						// write loop count
	UINT nReadLoopCtr = 0;						// read loop count
	LARGE_INTEGER liStartTime;					// start time
	LARGE_INTEGER liStartTimeOverall;			// overall start time
	LARGE_INTEGER liEndTime;					// end time
	LARGE_INTEGER liCPUFrequency;				// cpu frequency
	double fElapsed = 0;						// elapsed time
	double dLastTx = 0;							// last transmit rate
	double dLastRx = 0;							// last receive rate
	double dMinTx = 1000.0;						// minimum transmit rate
	double dMinRx = 1000.0;						// minimum receive rate
	double dMaxTx = 0;							// maximum transmit rate
	double dMaxRx = 0;							// maximum receive rate
	double dAvgTxTotal = 0;						// total transmit rate
	double dAvgRxTotal = 0;						// total receive rate

	char cLastMaxTimeOut[16];					// timestamp for maximum transmit
	char cLastMinTimeOut[16];					// timestamp for minimum transmit
	char cLastMaxTimeIn[16];					// timestamp for maximum receive
	char cLastMinTimeIn[16];					// timestamp for minimum receive
	char cLastErrorTime[16];					// timestamp for last error
	char cLastErrorInfo[50];					// last error info string
	bool bError = false;						// error flag
	int nRetVal = OPERATION_PASSED;				// return value

	const int dAvgBlockSize = 100;				// average block size
	double dAvgTxLastN[dAvgBlockSize];			// holds the bandwidth of the last N transmits
	double dAvgRxLastN[dAvgBlockSize];			// holds the bandwidth of the last N receives

	// allocate some storage for buffer, this is the most we'll transfer
	pcOutBuff = new char [uiMaxDataBytes];		// output buffer, this includes room for the header bytes
	pcInBuff = new char [uiMaxDataBytes];		// input buffer
	puiOutBuffHeader = (UINT*)pcOutBuff;		// header pointer


	// seed the random-number generator
	srand( (unsigned)time( NULL ) );

	BYTE pbyVersion[VERSION_STRING_BLOCK_SIZE];	// allocate memory for the strings
	nRetVal = GetFirmwareVersionStrings(pbyVersion);
	if ( nRetVal != OPERATION_PASSED )
		return nRetVal;

	// setup pointers to each of the strings returned in the buffer
	BYTE* pbyFirmwareVersion = pbyVersion + (FW_VERSION_NUMBER*MAX_VERSION_STRING_LEN);
	BYTE* pbyBuildDate       = pbyVersion + (FW_BUILD_DATE*MAX_VERSION_STRING_LEN);
	BYTE* pbyTargetProcessor = pbyVersion + (FW_TARGET_PROC*MAX_VERSION_STRING_LEN);

	// get and save the current cursor position, this will be used later to help eliminate screen flicker
	COORD origin = GetCurrentCursorPosition();
	COORD errorOrigin = {0,0};

	// save the current console window attributes
	CONSOLE_SCREEN_BUFFER_INFO csbi = gSavedCsbi;
	// if we are within 20 lines of the end of the screen buffer we need to expand the buffer (by printing \n) and
	// than adjust our window and origin coorindates so that the screen will not scroll during the loopback test.
	// When the screen scrolls during the test in breaks our cursor repositioning scheme.
	if (csbi.dwCursorPosition.Y + 20 >= csbi.dwSize.Y)
	{
		for(int i = 0; i < 20; ++i)
			printf("\n");

		SMALL_RECT srctWindow;
		srctWindow = csbi.srWindow;

		srctWindow.Top -= 20;
		srctWindow.Bottom -= 20;

		SetConsoleWindowInfo(GetStdHandle(STD_OUTPUT_HANDLE), TRUE, &srctWindow);

		origin.Y -= 20;
	}

	// do we want random size transfers (LOOPBACK_HEADER_BYTES to uiMaxDataBytes)
	if ( bRandomSize )
	{
		nDataBytesNextRound = rand();
		nDataBytesNextRound %= uiMaxDataBytes;

		// make sure it's at least LOOPBACK_HEADER_BYTES
		if (nDataBytesNextRound < LOOPBACK_HEADER_BYTES)
			nDataBytesNextRound = LOOPBACK_HEADER_BYTES;
	}
	// else always use this
	else
	{
		nDataBytesNextRound = uiMaxDataBytes;
		nDataBytesThisRound = nDataBytesNextRound;
	}

	// send out the one and only USBCB for loopback, it tells the device we want
	// to start a loopback and gives it the initial transfer count
	usbcb.ulCommand = LOOPBACK;				// command
	usbcb.ulCount = nDataBytesNextRound;	// number of bytes
	usbcb.ulData = 0x0;						// doesn't matter, device determines location

	IoStatus = WritePipe(g_hWrite, &usbcb, sizeof(USBCB), &nCountCurrent);

	// check for error
	if (!IoStatus)
	{
		PrintLoopbackError(errorOrigin, "OUT operation failed:  Error WRITING usbcb for LOOPBACK");
		nRetVal = IO_WRITE_USBCB_FAILED;
		bError = true;
	}

	// init timer
	QueryPerformanceCounter( &liStartTimeOverall );
	QueryPerformanceFrequency( &liCPUFrequency );

	// loop until the user wants to quit or if we're counting loops until we reach uiLoops
	while (bKeepRunning)
	{
		// see if we need to quit, if so this will be our last transfer
		bKeepRunning = !kbhit() && ( (uiLoops > uiLoopCount) || (!uiLoops) );

		// we calculate the data bytes for the next round on this round
		// because we encode this in the current data transfer so the
		// firmware can setup for the next transfer once this one is done
		nDataBytesThisRound = nDataBytesNextRound;

		// do we want random size transfers (LOOPBACK_HEADER_BYTES to uiMaxDataBytes),
		// otherwise we just keep using the same value and don't need to update it
		if ( bRandomSize )
		{
			nDataBytesNextRound = rand();
			nDataBytesNextRound %= uiMaxDataBytes;

			// make sure it's at least LOOPBACK_HEADER_BYTES
			if (nDataBytesNextRound < LOOPBACK_HEADER_BYTES)
				nDataBytesNextRound = LOOPBACK_HEADER_BYTES;
		}

		////////////////////////////////////////////////
		// WRITE TO THE DEVICE
		////////////////////////////////////////////////
		nCountSent = 0;
		nCountCurrent = 0;
		pcTemp = pcOutBuff;		// point to start of buffer

		// fill in the header bytes
		*puiOutBuffHeader = nDataBytesNextRound;	// next count
		*puiOutBuffHeader |= bKeepRunning << 24;	// stop loopback

		// init buffer with random data, skip the header bytes so we don't overwrite it
		for ( i = LOOPBACK_HEADER_BYTES; i < nDataBytesThisRound; i++)
			pcTemp[i] = (char)rand();

		// set the cursor back to it's starting position to help eliminate screen flicker
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), origin);

		NOISY(("\nRunning loopback on "));
		CNOISY(BR_GREEN,("%s #%d ", g_pszDeviceDesc, g_unDeviceNumber));
		NOISY(("("));
		CNOISY(BR_GREEN,("%s", pbyTargetProcessor));
		NOISY((")\n"));
		NOISY(("\nRunning time:      %d days %d hrs %d mins %d secs    ",(int)fElapsed/3600/24,		// days
																	 	 (int)(fElapsed/3600) % 24,	// hrs
																	 	 (int)(fElapsed/60) % 60,	// mins
																		 (int)fElapsed%60));		// secs
		NOISY(("\nTransfer size:     %d Bytes   ", nDataBytesThisRound ));
		NOISY(("\nTotal loops:       %d   ", uiLoopCount));

		NOISY(("\nTotal data looped: "));
		if (dTotalBytes < GBYTE)
		{
			CNOISY(BR_GREEN,("%.2f ", (float)dTotalBytes/(float)MBYTE));
			NOISY(("MBytes           "));
		}
		else if (dTotalBytes >= GBYTE && dTotalBytes < TBYTE)
		{
			CNOISY(BR_GREEN,("%.4f ", (float)dTotalBytes/(float)GBYTE));
			NOISY(("GBytes           "));
		}
		else
		{
			CNOISY(BR_GREEN,("%.6f ", (float)dTotalBytes/(float)TBYTE));
			NOISY(("TBytes           "));
		}

		NOISY(("\nAvg loopback rate: "));
		CNOISY(BR_GREEN,("%.2f ",(dTotalRcvd /fElapsed)/MBYTE));
		NOISY(("MBytes/sec   "));

		if (nErrorCount > 0)
		{
			NOISY(("\nData error count:  "));
			CNOISY(BR_RED,("%d  (Last @ %s, %s)\n", nErrorCount, cLastErrorTime, cLastErrorInfo));
		}
		else
		{
			NOISY(("\nData error count:  %d   \n", nErrorCount));
		}

		NOISY(("\n                           LAST   AVERAGE    AVERAGE     MAX     MIN"));
		NOISY(("\n                                  LAST %d", dAvgBlockSize));
		NOISY(("\n---------------------------------------------------------------------"));

		// track how many WriteFile() calls we make each time
		nWriteLoopCtr = 0;

		do
		{
			// write the data

			QueryPerformanceCounter( &liStartTime );
			IoStatus = WritePipe(	g_hWrite, pcTemp, sizeof(char) * (nDataBytesThisRound - nCountSent), &nCountCurrent);
			QueryPerformanceCounter( &liEndTime );

			// check for error
			if (!IoStatus)
			{
				PrintLoopbackError(errorOrigin, "OUT operation failed:  Error WRITING data for LOOPBACK");
				bError = true;
				nRetVal = IO_WRITE_DATA_FAILED;
				break;
			}

			fElapsed = (double)(liEndTime.QuadPart - liStartTime.QuadPart);
			fElapsed /= (double)liCPUFrequency.QuadPart;
			dLastTx = (nCountCurrent/fElapsed)/MBYTE;
			dAvgTxTotal += dLastTx;

			dAvgTxLastN[uiLoopCount%dAvgBlockSize] = dLastTx;

			if(dLastTx > dMaxTx)
			{
				dMaxTx = dLastTx;
				fElapsed = (double)(liEndTime.QuadPart - liStartTimeOverall.QuadPart);
				fElapsed /= (double)liCPUFrequency.QuadPart;
				sprintf(cLastMaxTimeOut, "%02d:%02d:%02d:%02d\0", (int)(fElapsed/3600)/24, (int)(fElapsed/3600), (int)(fElapsed/60) % 60,	(int)fElapsed%60);
			}

			if(dLastTx < dMinTx && nCountCurrent != 0)
			{
				dMinTx = dLastTx;
				fElapsed = (double)(liEndTime.QuadPart - liStartTimeOverall.QuadPart);
				fElapsed /= (double)liCPUFrequency.QuadPart;
				sprintf(cLastMinTimeOut, "%02d:%02d:%02d:%02d\0", (int)(fElapsed/3600)/24, (int)(fElapsed/3600), (int)(fElapsed/60) % 60,	(int)fElapsed%60);
			}

			if (nCountCurrent)
			{
				NOISY(("\nOUT - from host (MB/s):   "));
				SetRGB(BR_GREEN);
				NOISY(("%05.2f", dLastTx));	// LAST
				NOISY(("    "));
				if (uiLoopCount >= dAvgBlockSize-1)					// LAST N AVG
				{
					double sum = 0;
					double avg = 0;
					for (int i = 0; i < dAvgBlockSize; ++i)
						sum += dAvgTxLastN[i];
					avg = sum/dAvgBlockSize;
					NOISY(("%05.2f", avg));
				}
				else
				{
					NOISY(("n/a"));
				}
				NOISY(("      "));
				NOISY(("%05.2f", dAvgTxTotal/(uiLoopCount+1)));		// AVERAGE
				NOISY(("     "));
				NOISY(("%05.2f   %05.2f", dMaxTx, dMinTx));				// MAX / MIN
				//NOISY(("%.2f(%s)   %.2f(%s)", dMaxTx, cLastMaxTime, dMinTx, cLastMinTime));				// MAX / MIN
				SetRGB(WHITE);
			}

			// add to the totals we've already sent
			nCountSent += nCountCurrent;
			dTotalSent += nCountCurrent;

			// advance pointer on host
			pcTemp += sizeof(char) * nCountCurrent;

			nWriteLoopCtr++;

		} while( nCountSent < nDataBytesThisRound );

		// check for error
		if (!IoStatus)
		{
			break;
		}

		////////////////////////////////////////////////
		// READ FROM THE DEVICE
		////////////////////////////////////////////////
		nCountRcvd = 0;
		nCountCurrent = 0;
		pcTemp = pcInBuff;

		// track how many ReadFile() calls we make each time
		nReadLoopCtr = 0;

		do
		{
			QueryPerformanceCounter( &liStartTime );
			IoStatus = ReadPipe(g_hRead, pcTemp, sizeof(char) * (nDataBytesThisRound - nCountRcvd), &nCountCurrent);
			QueryPerformanceCounter( &liEndTime );

			// check for error
			if (!IoStatus)
			{
				PrintLoopbackError(errorOrigin, "IN operation failed:  Error READING data for LOOPBACK");
				bError = true;
				nRetVal = IO_READ_DATA_FAILED;
				break;
			}

			fElapsed = (double)(liEndTime.QuadPart - liStartTime.QuadPart);
			fElapsed /= (double)liCPUFrequency.QuadPart;
			dLastRx = (nCountCurrent/fElapsed)/MBYTE;
			dAvgRxTotal += dLastRx;

			dAvgRxLastN[uiLoopCount%dAvgBlockSize] = dLastRx;

			if(dLastRx > dMaxRx)
			{
				dMaxRx = dLastRx;
				fElapsed = (double)(liEndTime.QuadPart - liStartTimeOverall.QuadPart);
				fElapsed /= (double)liCPUFrequency.QuadPart;
				sprintf(cLastMaxTimeIn, "%02d:%02d:%02d:%02d\0", (int)(fElapsed/3600)/24, (int)(fElapsed/3600), (int)(fElapsed/60) % 60,	(int)fElapsed%60);
			}

			if(dLastRx < dMinRx && nCountCurrent != 0)
			{
				dMinRx = dLastRx;
				fElapsed = (double)(liEndTime.QuadPart - liStartTimeOverall.QuadPart);
				fElapsed /= (double)liCPUFrequency.QuadPart;
				sprintf(cLastMinTimeIn, "%02d:%02d:%02d:%02d\0", (int)(fElapsed/3600)/24, (int)(fElapsed/3600), (int)(fElapsed/60) % 60,	(int)fElapsed%60);
			}

			if (nCountCurrent)
			{
				NOISY(("\nIN  - to host   (MB/s):   "));
				SetRGB(BR_GREEN);
				NOISY(("%05.2f", dLastRx));		// LAST
				NOISY(("    "));
				if (uiLoopCount >= dAvgBlockSize-1)						// LAST N AVG
				{
					double sum = 0;
					double avg = 0;
					for (int i = 0; i < dAvgBlockSize; ++i)
						sum += dAvgRxLastN[i];
					avg = sum/dAvgBlockSize;
					NOISY(("%05.2f", avg));
				}
				else
				{
					NOISY(("n/a"));
				}
				NOISY(("      "));
				NOISY(("%05.2f", dAvgRxTotal/(uiLoopCount+1)));			// AVERAGE
				NOISY(("     "));
				NOISY(("%05.2f   %05.2f", dMaxRx, dMinRx));					// MAX / MIN
				SetRGB(WHITE);
			}

			// add to the totals we've already received
			nCountRcvd += nCountCurrent;
			dTotalRcvd += nCountCurrent;

			// advance pointer on host
			pcTemp += sizeof(char) * nCountCurrent;

			nReadLoopCtr++;

		} while ( nCountRcvd < nDataBytesThisRound );

		// check for error
		if (!IoStatus)
		{
			break;
		}

		if (!bError)
		{
			NOISY(("\nTimestamps:               "));
			CNOISY(GREEN, ("Out Max: %s   Out Min: %s ", cLastMaxTimeOut, cLastMinTimeOut));
			CNOISY(GREEN, ("\n                          In Max:  %s   In Min:  %s ", cLastMaxTimeIn, cLastMinTimeIn));
		}

		// get time
		QueryPerformanceCounter( &liEndTime );
		fElapsed = (double)(liEndTime.QuadPart - liStartTimeOverall.QuadPart);
		fElapsed /= (double)liCPUFrequency.QuadPart;

		// add it to the total
		dTotalBytes += nCountSent;

		// now compare the IN and OUT buffers
		for ( i = 0; i < nDataBytesThisRound; i++ )
		{
			if ( pcOutBuff[i] != pcInBuff[i] )
			{
				nErrorCount++;
				sprintf(cLastErrorTime, "%02d:%02d:%02d:%02d", (int)(fElapsed/3600)/24, (int)(fElapsed/3600), (int)(fElapsed/60) % 60,	(int)fElapsed%60);
				sprintf(cLastErrorInfo, "byte 0x%x exp 0x%02x rcv 0x%02x", i, pcOutBuff[i], pcInBuff[i]);
				break;
			}
		}

		NOISY(("\n\n\n"));
		if (errorOrigin.Y == 0)
			errorOrigin = GetCurrentCursorPosition();
		CNOISY(BR_WHITE, ("Press any key to stop the loopback"));

		++uiLoopCount;
	}

	// free the buffers
	if ( pcOutBuff )
		delete [] pcOutBuff;
	if ( pcInBuff )
		delete [] pcInBuff;

	// get the char that broke us out of the loop if there was no error or loop count restriction
	if (!bError && !uiLoops)
		getch();

	return nRetVal;
}

/******************************************************************************
Routine Description:

    Prints loopback error

Arguments:

    COORD origin -	coords to start printing at
	char *str -		string to print

Return Value:

    void

******************************************************************************/

void PrintLoopbackError(COORD origin, char * str)
{
	if (origin.Y != 0)
	{
		SetCurrentCursorPosition(origin);
		printf("                                  ");
	}
	SetCurrentCursorPosition(origin);
	ColorPrintf(BR_RED, str);
}