/*********************************************************************************

Copyright(c) 2005 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

#include <windows.h>
//#include <stdio.h>
#include <conio.h>
#include <setupapi.h>
#include "usbcmd.h"
#include "usbdriver.h"
#include "console.h"


/////////////////////////////////////////////////////
// extern globals
/////////////////////////////////////////////////////

extern DWORD g_dwTotalDevices;		// total devices detected
extern UINT g_unDeviceNumber;		// device number selected
extern char g_pszDeviceDesc[];		// device name string for display only
extern HANDLE g_hRead;				// USB device read handle
extern HANDLE g_hWrite;				// USB device write handle

/******************************************************************************
Routine Description:

Reads Blackfin memory

Arguments:

UINT uiAddress -	Indicates if we should loop random data sizes
UINT uiCount -		If random this indicates the maximum transfer size, else
indicates the transfer size of each loop

Return Value:

int - return status

******************************************************************************/

char* ReadDeviceMemory(const _USB_COMMAND readCommand, const UINT uiAddress, const UINT uiCount )
{
	//char *pcInBuff = NULL;			// input buffer
	//char *pcOutBuff = NULL;			// output buffer
	//char *pcTemp = NULL;					// temp pointer
	
	ULONG nCountRcvd = 0;			// count received
	ULONG nCountCurrent = 0;		// current count
	ULONG nCountRound = 0;		// current count
	UINT uiRemainder = 0;			// remainder
	//ULONG i = 0,  k = 0;		// indexes
	ULONG nActualBytes=0;				// actual bytes transferred
	//BOOL IoStatus = TRUE;			// IO status

	// allocate some storage for buffer
	char  *pcOutBuff = new  char [sizeof(USBCB)];
    char  *pcInBuff = new  char [uiCount];
	//USBCB *pusbcb = NULL;			// USB command block pointer
	
	// point usbcb to front of new buffer, point temp pointer to input buffer
	USBCB *pusbcb = (USBCB*)pcOutBuff;
	char *pcTemp = pcInBuff;
	//BOOL IoStatus = (int)NULL;
	
	
	// send packet telling we want to read
	pusbcb->ulCommand = readCommand;		// command
	pusbcb->ulCount = uiCount;				// number of bytes
	pusbcb->ulData = (ULONG)uiAddress;		// start address
	BOOL IoStatus = WritePipe(g_hWrite, pusbcb, sizeof(USBCB), &nActualBytes);
	// check for error
	if (!IoStatus)
	{
		//ColorPrintf(BR_RED, "\n\nError WRITING usbcb for MEMORY DUMP");

		// free the buffers
		if ( pcOutBuff )
			delete [] pcOutBuff;
		if ( pcInBuff )
			delete [] pcInBuff;
		return NULL;
	}
	
	// divide up tranfers to the maximum transfer size
	if( uiCount > MAX_DATA_BYTES_EZEXTENDER )
		nCountRound = MAX_DATA_BYTES_EZEXTENDER;
	else
		nCountRound = uiCount;
	// this loop breaks up the piperead to MAX_DATA_BYTES_EZEXTENDER
	do
	{
		//IoStatus = ReadPipe(g_hRead, pcTemp, sizeof(char) * (pusbcb->ulCount), &nCountCurrent);  david hexner
		IoStatus = ReadPipe(g_hRead, pcTemp, sizeof(char) * nCountRound, &nCountCurrent);
		// check for error
		if (!IoStatus)
		{
			//ColorPrintf(BR_RED, "\n\nError READING data for MEMORY DUMP");
			break;
		}

		// add to the total we've already received
		nCountRcvd += nCountCurrent;
		
		uiRemainder = uiCount - nCountRcvd;
		
		// check if the remainder is less then the max transfer size
		if( uiRemainder > MAX_DATA_BYTES_EZEXTENDER)
			nCountRound = MAX_DATA_BYTES_EZEXTENDER;
		else
			nCountRound = uiRemainder;
		
		// advance pointer on host
		pcTemp += sizeof(char) * nCountCurrent;

	} while ( nCountRcvd < uiCount );

	// check for error
	if (!IoStatus)
	{
		//ColorPrintf(BR_RED, "\n\nError READING data for MEMORY DUMP");

		// free the buffers
		if ( pcOutBuff )
			delete [] pcOutBuff;
		if ( pcInBuff )
			delete [] pcInBuff;
		return NULL;
	}

	// free the buffers
	if ( pcOutBuff )
		delete [] pcOutBuff;

	return pcInBuff;
}

/*
char* ReadExcelData(const UINT uiAddress, UINT& uiRows, UINT& uiColumns)
{
	
	char *pcInBuff = (char)NULL;			// input buffer
	char *pcOutBuff =(char) NULL;			// output buffer
	char *pcTemp = NULL;					// temp pointer
	USBCB *pusbcb = NULL;			// USB command block pointer
	ULONG nCountRcvd = 0;			// count received
	ULONG nCountCurrent = 0;		// current count
	
	//ULONG i = 0, j = 0, k = 0;		// indexes
	ULONG nActualBytes=0;				// actual bytes transferred
	BOOL IoStatus = TRUE;			// IO status

	// allocate some storage for buffer
	pcOutBuff = new char [sizeof(USBCB)];
	
	// point usbcb to front of new buffer, point temp pointer to input buffer
	pusbcb = (USBCB*)pcOutBuff;

	// send packet telling we want to read
	pusbcb->ulCommand = SAVE_EXCEL_FILE_SIZE;		// command
	pusbcb->ulCount = 0;				// number of bytes
	pusbcb->ulData = (ULONG)uiAddress;		// start address

	IoStatus = WritePipe(g_hWrite, pusbcb, sizeof(USBCB), &nActualBytes);

	// check for error
	if (!IoStatus)
	{
		//ColorPrintf(BR_RED, "\n\nError WRITING usbcb for MEMORY DUMP");

		// free the buffers
		if ( pcOutBuff )
			delete [] pcOutBuff;
		return NULL;
	}

	pcInBuff = new char [sizeof(int) * 2];
	pcTemp = pcInBuff;

	//Read number of rows and columns
	IoStatus = ReadPipe(g_hRead, pcTemp, sizeof(int) * 2, &nCountCurrent);
	
	uiRows = *(int*)pcTemp;
	uiColumns = *(int*)(pcTemp + 4);
	delete [] pcInBuff;

	const ULONG countData = sizeof(float) * uiRows * uiColumns;
	pcInBuff = new char [countData];
	pcTemp = pcInBuff;
	do
	{
		IoStatus = ReadPipe(g_hRead, pcTemp, sizeof(char) * countData, &nCountCurrent);

		// check for error
		if (!IoStatus)
		{
			//ColorPrintf(BR_RED, "\n\nError READING data for MEMORY DUMP");
			break;
		}

		// add to the total we've already received
		nCountRcvd += nCountCurrent;

		// advance pointer on host
		pcTemp += sizeof(char) * nCountCurrent;

	} while ( nCountRcvd < countData );

	// check for error
	if (!IoStatus)
	{
		//ColorPrintf(BR_RED, "\n\nError READING data for MEMORY DUMP");

		// free the buffers
		if ( pcOutBuff )
			delete [] pcOutBuff;
		if ( pcInBuff )
			delete [] pcInBuff;
		return NULL;
	}

	// free the buffers
	if ( pcOutBuff )
		delete [] pcOutBuff;
		
	return pcInBuff;
}

*/


short FillDeviceMemory(const _USB_COMMAND writeCommand, const UINT uiAddress, const UINT uiTotalCount,  char*  buffer)
{
	int uiTotalCountWritten = 0;
	bool writeJustCommand = (uiTotalCount == 0);
	USBCB usbcb;						// USB command block
	BOOL IoStatus = TRUE;				// IO status
	char *pcBuffer = buffer;						// buffer to store data
	// allocate and initialize a buffer of data
	//pcBuffer = buffer;

	
	while(uiTotalCount != uiTotalCountWritten || writeJustCommand)
	{
		writeJustCommand = false;
		const ULONG uiCount = (uiTotalCount - uiTotalCountWritten) < MAX_DATA_BYTES_EZEXTENDER ? 
						(uiTotalCount - uiTotalCountWritten) : MAX_DATA_BYTES_EZEXTENDER;
	    ULONG nCountSent = 0;				// count sent
		ULONG nCountCurrent = 0;			// current count sent
		// send packet telling we want to write
		usbcb.ulCommand = writeCommand;				// command
		usbcb.ulCount = (ULONG)uiCount;				// number of bytes
		usbcb.ulData = (ULONG)(uiAddress + uiTotalCountWritten);			// start address

		char *pcTemp = pcBuffer;		// temp pointer to current written chunk

		IoStatus = WritePipe(g_hWrite, &usbcb, sizeof(USBCB), &nCountCurrent);

		// check for error
		if (!IoStatus)
		{
			//ColorPrintf(BR_RED, "\n\nError WRITING usbcb for MEMORY SET");
			return IO_WRITE_USBCB_FAILED;
		}

		if(uiCount > 0)
		{
			do
			{
				IoStatus = WritePipe(g_hWrite, pcTemp, sizeof(char) * (usbcb.ulCount - nCountSent), &nCountCurrent);

				// check for error
				if (!IoStatus)
				{
					break;
				}

				// add to the total we've already sent
				nCountSent += nCountCurrent;

				// advance pointer on host
				pcTemp += sizeof(char) * nCountCurrent;

			} while ( nCountSent < uiCount );

			// check for error
			if (!IoStatus)
			{
				//ColorPrintf(BR_RED, "\n\nError WRITING data for MEMORY SET");
				return IO_WRITE_DATA_FAILED;
			}

			pcBuffer += uiCount;
			uiTotalCountWritten += uiCount;
		}
	}

	//ColorPrintf(BR_WHITE, "Memory has been set.");
	return OPERATION_PASSED;
}


/******************************************************************************
Routine Description:

Downloads a host file to the Blackfin

Arguments:

CHAR *pszFilename -	File on host we want to download.
UINT uiAddress -	Address file is downloaded to.

Return Value:

int - return status

******************************************************************************/
/*
int DownloadFile( CHAR  *pszFilename, const UINT uiAddress )
{
	FILE	*InFile=NULL;								// input file
	char	pcBuffer[MAX_DATA_BYTES_EZEXTENDER];	// buffer to store file data
	int		nActual = 0;							// count read in from file
	int		nTotal = 0;								// total count read in from file
	USBCB	usbcb;									// USB command block
	ULONG	ulCurrentAddr = uiAddress;				// address pointer
	ULONG	nActualBytes = 0;						// actual bytes passed to the driver
	BOOL	IoStatus = TRUE;						// iostatus
	int		nRetVal = OPERATION_PASSED;				// return value


	// open input file as binary
	if( (InFile = fopen( pszFilename, "rb" )) == NULL )
		//ColorPrintf(BR_RED, "The input file was not found, please check the path and try again.\n" );

	// check for error opening the file
	if (!InFile)
	{
		//ColorPrintf(BR_RED, "\nError opening input file, please make sure the file is accessible and try again.");
		return ERROR_OPENING_FILE;
	}

	// loop until the end of file is found
	while( !feof( InFile ) )
	{
		// attempt to read in MAX_DATA_BYTES_EZEXTENDER bytes
		nActual = fread( pcBuffer, sizeof( char ), MAX_DATA_BYTES_EZEXTENDER, InFile );
		if( ferror( InFile ) )
		{
			//ColorPrintf(BR_RED, "\nError reading input file, download will not complete." );
			nRetVal = ERROR_READING_FILE;
			break;
		}

		// send packet telling we want to do a MEMORY_WRITE
		usbcb.ulCommand = MEMORY_WRITE;				// command
		usbcb.ulCount = (ULONG)nActual;				// number of bytes
		usbcb.ulData = (ULONG)ulCurrentAddr;		// start address

		IoStatus = WritePipe(g_hWrite, &usbcb, sizeof(USBCB), &nActualBytes);

		// check for error
		if (!IoStatus)
		{
			///ColorPrintf(BR_RED, "\n\nError WRITING usbcb for FILE DOWNLOAD");
			nRetVal = IO_WRITE_USBCB_FAILED;
			break;
		}

		IoStatus = WritePipe(g_hWrite, pcBuffer, sizeof(char) * usbcb.ulCount, &nActualBytes);

		// check for error
		if (!IoStatus)
		{
			//ColorPrintf(BR_RED, "\n\nError WRITING data for FILE DOWNLOAD");
			nRetVal = IO_WRITE_DATA_FAILED;
			break;
		}

		// increment counters
		nTotal += nActual;
		ulCurrentAddr += nActual;
	}

	//ColorPrintf(BR_WHITE, " \nFile \"%s\" has been downloaded.", pszFilename );

	// close file
	if (InFile)
	{
		if( fclose( InFile ) )
			return 0; //ColorPrintf(BR_RED, "\nThe input file could not be closed." );
	}

	return nRetVal;
}*/
