/*********************************************************************************

Copyright(c) 2005 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

#ifndef _USBDRIVER_H_
#define _USBDRIVER_H_


#define MAX_IO_WAIT					8000		// timeout for USB transfers in msecs

// pipe types
enum _PIPE_TYPE
{
	READ_PIPE,
	WRITE_PIPE
};

//HANDLE OpenOneDevice( HDEVINFO, PSP_INTERFACE_DEVICE_DATA, char * );
//HANDLE OpenUsbDevice( LPGUID, char * );
//BOOL GetUsbDeviceFileName( LPGUID, char * );
DWORD QueryNumDevices( LPGUID pGuid );
HANDLE OpenDeviceHandle( LPGUID pGuid, _PIPE_TYPE nPipeType, DWORD dwDeviceNumber, BOOL bUseAsyncIo);
BOOL ReadPipe(HANDLE hRead, LPVOID lpBuffer, DWORD nNumberOfBytesToRead, LPDWORD lpNumberOfBytesRead);
BOOL WritePipe(HANDLE hWrite, LPCVOID lpBuffer, DWORD nNumberOfBytesToWrite, LPDWORD lpNumberOfBytesWritten);


#endif // _USBDRIVER_H_

