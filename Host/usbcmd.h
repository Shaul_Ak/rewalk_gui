/*********************************************************************************

Copyright(c) 2005 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

#ifndef _USBCMD_H_
#define _USBCMD_H_

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// defines for both host and device
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

enum _USB_COMMAND
{
	NO_COMMAND,				// nothing doing here...
	GET_FW_VERSION,			// get the firmware version
	QUERY_SUPPORT,			// query for support
	QUERY_REPLY,			// query reply
	LOOPBACK,				// run loopback on the device
	MEMORY_READ,			// read from specified memory on the device
	MEMORY_WRITE,			// write to specified memory on the device
	USBIO_START,			// run USB IO on this device
	USBIO_STOP,				// stop USB IO on this device
	USBIO_OPEN,				// open file on host
	USBIO_CLOSE,			// close file on host
	USBIO_READ,				// read file on host
	USBIO_READ_REPLY,		// read reply from host
	USBIO_WRITE,			// write file on host
	USBIO_WRITE_REPLY,		// write reply from host
	USBIO_SEEK_CUR,			// seek from current position of file on host
	USBIO_SEEK_END,			// seek from end of file on host
	USBIO_SEEK_SET,			// seek from beginning of file on host
	USBIO_SEEK_REPLY,		// seek reply from host
	USBIO_FILEPTR,			// sending file pointer,
	CUSTOM_COMMAND,
	
	EXIT_USB_INTERFACE,
	READ_REWALK_PARAMETERS,	// read parameters from the device
	WRITE_REWALK_PARAMETERS,// write parameters to the device
	RESTORE_FACTORY_PARAMETERS, // read factory parameters
	WRITE_INF_ADDR, 	
	WRITE_INF_ID,
 	DOWNLOAD_DSP_CODE,
    DOWNLOAD_DSP_CODE_COMPLETE,
    DOWNLOAD_MCU_CODE,
    DOWNLOAD_MCU_CODE_COMPLETE,
	DOWNLOAD_RC_CODE,
    DOWNLOAD_RC_CODE_COMPLETE,
	TEST_START,
	TEST_STATUS,
    START_CALIBRATE,
    CALIBRATE,
    CALBIRATE_STATUS,
    READ_ONLINE_DEVICE_DATA, 
    COMMAND1,
	COMMAND2,
	COMMAND3,
	COMMAND4,
	COMMAND5,
	COMMAND6,
	COMMAND7,
	RECEIVE_COMMAND_RESULT,
	WRITE_SERIAL_NUMBERS,
	SAVE_EXCEL_FILE_SIZE,
	SAVE_EXCEL_FILE_DATA,
	JUMP_TO_THIRD_APPLICATION,
	RF_TEST_START,
	RF_TEST_STATUS,
	WRITE_RC_ADDRESS_ID_STATUS,
	WRITE_RC_ADDRESS_ID, 	
	WRITE_INF_ADDRESS_ID,
	SAVE_LOG_FILE,
	GET_LOG_FILE_SIZE,
	GET_LOG_BUFF_ADDRESS,
	GET_GEN_BUFF_ADDRESS,
	GET_FLASH_LOG_FILE_SIZE,
	SAVE_FLASH_LOG_FILE,
	GET_SYSTEM_ERROR_STATUS,
	CLEAR_SYSTEM_ERROR_STATUS,
	RESET_STEP_COUNTER,
	READ_STEP_COUNTER,
	RESET_STAIR_COUNTER,
	READ_STAIR_COUNTER,
	RESET_SDRAM_LOG,
	SAVE_LOG_FILE_DONE,
	SEND_DEBUG_COMMAND,
};

typedef struct _USBCB		// USB command block
{
  ULONG ulCommand;			// command to execute
  ULONG ulData;				// generic data field
  ULONG ulCount;			// number of bytes to transfer
} USBCB, *PUSBCB;

/*enum _VERSION_STRINGS		// version string info
{
	FW_BUILD_DATE,			// build date of firmware
	FW_BUILD_TIME,			// build time of firmware
	FW_VERSION_NUMBER,		// version number of firmware
	FW_TARGET_PROC,			// target processor of firmware
	FW_APPLICATION_NAME,	// application name of firmware

	NUM_VERSION_STRINGS		// number of version strings
};
*/
#define	MAX_VERSION_STRING_LEN		32
#define VERSION_STRING_BLOCK_SIZE	(NUM_VERSION_STRINGS*MAX_VERSION_STRING_LEN)

#define LOOPBACK_HEADER_BYTES		4						// bytes in header of loopback data
#define MAX_DATA_BYTES_EZEXTENDER	0x10000					// max bytes to send
#define MIN_DATA_BYTES_EZEXTENDER	LOOPBACK_HEADER_BYTES	// min bytes to send

#define FILE_OPEN_MODE_OFFSET		0						// byte offset for mode for file open
#define FILE_OPEN_FILENAME_OFFSET	4						// byte offset for filename for file open

#define USBIO_STDIN_FD 	0		// file descriptor for stdin on Blackfin
#define USBIO_STDOUT_FD 1		// file descriptor for stdout on Blackfin
#define USBIO_STDERR_FD 2		// file descriptor for stderr on Blackfin



///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// defines for host only
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

#ifdef _HOSTAPP_

enum _ERROR_VALUES			// error values
{
	OPERATION_PASSED = 0,
	UNSUPPORTED_COMMAND,
	IO_WRITE_USBCB_FAILED,
	IO_READ_USBCB_FAILED,
	IO_READ_DATA_FAILED,
	IO_WRITE_DATA_FAILED,
	OUT_OF_MEMORY_ON_HOST,
	ERROR_OPENING_FILE,
	ERROR_READING_FILE,
	NO_AVAILABLE_FILE_PTRS,
	COULD_NOT_CONNECT,
};

#endif	// _HOSTAPP_


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// defines for device only
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

#ifndef _HOSTAPP_

#endif // ! _HOSTAPP_


#endif // _USBCMD_H_

