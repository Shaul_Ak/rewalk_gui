// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the HOST_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// HOST_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef HOST_EXPORTS
#define HOST_API __declspec(dllexport)
#else
#define HOST_API __declspec(dllimport)
#endif

#define DEFAULT_BUFF_ADDRESS 0x20000 


typedef enum
{
	GENERAL_BUFF,
	LOG_BUFF,
} BuffType;

typedef HOST_API struct _SerialNumbers
{
	//Serial numbers
	short sn1;
	short sn2;
	short sn3;
	short infSN1;
	short infSN2;
	short infSN3;
	short infSN4;
	short rcSN1;
	short rcSN2;
	short rcSN3;
	short rcSN4;
	short lhSN1;
	short lhSN2;
	short lhSN3;
	short lhSN4;
	short lkSN1;
	short lkSN2;
	short lkSN3;
	short lkSN4;
	short rhSN1;
	short rhSN2;
	short rhSN3;
	short rhSN4;
	short rkSN1;
	short rkSN2;
	short rkSN3;
	short rkSN4;
} SerialNumbers;

typedef HOST_API struct _DeviceParameters
{
	//Walk window
	short kneeAngle;
	short hipAngle;
	short backHipAngle;
	short maxVelocity;
	//Tilt window
	short tiltDelta;
	short tiltTimeout;
	//Safety window
	short safetyWhileStanding;
	short xThreshold;
	short yThreshold;
	//Training window
	short delayBetweenSteps;
	short buzzerBetweenSteps;
	short record;
	short sampleTime;

	//About window
	short rcAddress;
	short rcID;
	short softwareVersion;
	short dspVersion;
	short motorVersion;
	short rcVersion;
	short fpgaVersion;
	short bootVersion;
	//FSR window
	short switchFSRs;
	short fsrThreshold;
	short fsrWalkTimeout;
	//Stairs
	short lhAsc;
	short lkAsc;
	short rhAsc;
	short rkAsc;
	short lhDsc;
	short lkDsc;
	short rhDsc;
	short rkDsc;
	//Software Download
	short dspSoftwareProgrammed;
	//Calbiration
	short systemCalbirationStatus;
	short serialNumberStatus;
	
	SerialNumbers sn;
	short WalkCurrentThreshold;
	short StairsCurrentThreshold;

	short StairsEnabled;
	short SysType;
	//Log params
    short CANLogLevel;
    short MCULogLevel;
    short RFLogLevel;
    short RCLogLevel;
    short ADCLogLevel;
    short StairsLogLevel;
    short SitStnLogLevel;
    short WalkLogLevel;
	short USBLogLevel;
	short BITLogLevel;
	short OperationLogLevel ;
	short FlashLogLevel ;
	short FirstStepFlexion;
	short SystemErrorType;
	unsigned int StepCounter;
	unsigned int EnableSystemStuckOnError_int;
	short SwitchingTime;
	short AFTSitCounter;
    short AFTASCCounter;
    short AFTDSCCounter;
    short AFTSitStatus;
    short AFTASCStatus;
    short AFTDSCStatus;
	short AFTWalkStatus;
    short StairsASCSpeed;
	short StairsDSCSpeed;
	short Placing_RH_on_the_stair;
	short Placing_RK_on_the_stair;
	short StepLength;
	unsigned int StairCounter;
	short CurrentHandleAlgo;
	short UpperLegLength;
	short LowerLegLength;
	short SoleHeight;
	short ShoeSize;
	short StairHeight;
	short PicSWVersion;
	//New GUI6 Look and Feel design
	short  UnitsSelection;
	short  Gender;
	unsigned int User_ID;
	short  Weight;
	short  Height;
	short  UpperStrapHoldersSize;
	short  UpperStrapHoldersPosition;
	short  AboveKneeBracketSize;
	short  AboveKneeBracketPosition;
	short  FrontKneeBracketAssemble;
	short  FrontKneeBracketAnteriorPosition;
	short  FrontKneeBracketLateralPosition;
	short  PelvicSize;
	short  PelvicAnteriorPosition;
	short  PelvicVerticalPosition;
	short  UpperLegHightSize;
	short  LowerLegHightSize;
	short  FootPlateType1;
	short  FootPlateType2;
	unsigned int   FootPlateNoOfRotation;
    short  AFTStopsStatus;
    short  INFBatteriesLevel;
    short  Ack_Movement_Point;
	short Enable_First_Step_Rewalk6;
	short SystemResrvedParmeter5;
	short SystemResrvedParmeter6;
	short SystemResrvedParmeter7;
	short SystemResrvedParmeter8;
	short SystemResrvedParmeter9;
	short SystemResrvedParmeter10;
	short status;	//Keep this parameter last, used only for status communication to the application, not to the device
	char  UserName[255];
	char  ComputerName[15]; 
    char  WriteDataTime[22];
	char  UpperStrapHolderSizeString[10];
    char  UpperStrapHolderTypeString[10];
    char  AboveKneeBracketTypeString[10];
    char  AboveKneeBracketAnteriorString[10];
    char  KneeBracketTypeString[10];
    char  KneeBracketAnteriorPositionString[10];
    char  KneeBracketLateralPositionString[10];
    char  PelvicSizeString[10];
    char  PelvicAnteriorPositionString[10];
    char  PelvicVerticalPositionString[10];
    char  FootPlateTypeString[10];
    char  FootPlateSizeString[10];
    char  FootPlateDorsiFlexionPositionString[10]; 
    char  ASCSpeedString[10];
    char  DSCSpeedString[10];

} DeviceParameters;

typedef HOST_API struct _DeviceVersions
{
	byte deviceHigh;
	byte deviceLow;
	byte dspHigh;
	byte dspLow;
	byte motorHigh;
	byte motorLow;
	byte remoteHigh;
	byte remoteLow;
	byte fpgaHigh;
	byte fpgaLow;
	short status;	//Keep this parameter last, used only for status communication to the application, not to the device
} DeviceVersions;

typedef HOST_API struct _OnlineData
{
	short lhAngle;
	short lkAngle;
	short rhAngle;
	short rkAngle;
	short tiltXAngle;
	short tiltYAngle;
	short fsrRight1;
	short fsrRight2;
	short fsrRight3;
	short fsrLeft1;
	short fsrLeft2;
	short fsrLeft3;
	short mainBattery;
	short auxBattery;
	short custom1;
	short custom2;
	short custom3;
	short custom4;
	short custom5;
	short status;	//Keep this parameter last, used only for status communication to the application, not to the device
} OnlineData;

typedef HOST_API struct _CustomCommandResult
{
    byte id1;
    byte id2;
    byte byte1;
    byte byte2;
    byte byte3;
    byte byte4;
    byte byte5;
    byte byte6;
    byte byte7;
    byte byte8;
    short status;
} CustomCommandResult;

typedef HOST_API struct _TestData
{
	short inf;
	short rc;
	short lh;
	short lk;
	short rh;
	short rk;
	short status;	//Keep this parameter last, used only for status communication to the application, not to the device
} TestData;

typedef HOST_API struct _RFTestData
{
	short inf_BER;
	short rc_BER;
	short numOfPackets;
	short status;	//Keep this parameter last, used only for status communication to the application, not to the device
} RFTestData;

typedef HOST_API struct _SystemErrorData
{
	short SystemErrorStatus;
	short status;	//Keep this parameter last, used only for status communication to the application, not to the device
} SystemErrorData;

typedef HOST_API struct _StepCounterData
{
	unsigned int StepCounterData;
	short status;	//Keep this parameter last, used only for status communication to the application, not to the device
} StepCounterData;

typedef HOST_API struct _StairCounterData
{
	unsigned int StairCounterData;
	short status;	//Keep this parameter last, used only for status communication to the application, not to the device
} StairCounterData;


typedef HOST_API struct _RCAddressIDData
{
	short RC_Address;
	short RC_ID;
	short status;	//Keep this parameter last, used only for status communication to the application, not to the device
} RCAddressIDData;

HOST_API DeviceParameters ReadDeviceParameters();
HOST_API DeviceParameters RestoreDeviceParameters();
HOST_API short WriteDeviceParameters(char* const UserName, const int UserNameSize, char* const ComputerName, const int ComputerNameSize, char* const Date, const int DateSize, DeviceParameters dp );
HOST_API short WriteSystemMeausrmentsDeviceParameters(char* const UpperStrapHolderSizeString, const int UpperStrapHolderSizeString_length, 
	                                                  char* const UpperStrapHolderTypeString, const int UpperStrapHolderTypeString_length, 
													  char* const AboveKneeBracketTypeString, const int AboveKneeBracketTypeString_length,
													  char* const AboveKneeBracketAnteriorString, const int AboveKneeBracketAnteriorString_length, 
													  char* const KneeBracketTypeString, const int KneeBracketTypeString_length, 
													  char* const KneeBracketAnteriorPositionString, const int KneeBracketAnteriorPositionString_length,
													  char* const KneeBracketLateralPositionString, const int KneeBracketLateralPositionString_length,
													  char* const PelvicSizeString, const int PelvicSizeString_length,
													  char* const PelvicAnteriorPositionString, const int PelvicAnteriorPositionString_length,
													  char* const PelvicVerticalPositionString, const int PelvicVerticalPositionString_length,
													  char* const FootPlateTypeString, const int FootPlateTypeString_length,
													  char* const FootPlateSizeString, const int FootPlateSizeString_length,
													  char* const FootPlateDorsiFlexionPositionString, const int FootPlateDorsiFlexionPositionString_length,
											 		  char* const ASCSpeedString, const int ASCSpeedString_length,
													  char* const DSCSpeedString, const int DSCSpeedString_length,
									
												      DeviceParameters dp );
HOST_API short WriteRCAddressID(short address, short id);
HOST_API short WriteINFAddressID(short address, short id);
HOST_API short WriteINFAddress(short address);
HOST_API short WriteINFId(short id);
HOST_API short Download(char* data, int count, int command);
HOST_API CustomCommandResult RunCustomCommand(char* data, int count, int command);
HOST_API OnlineData ReadOnlineData();
HOST_API short StartTest();
HOST_API TestData GetTestStatus();
HOST_API short StartCalibration(bool lh, bool lk, bool rh, bool rk);
HOST_API short DoCalibration();
HOST_API TestData GetCalibrationStatus();
HOST_API short SaveExcelData(char* file);
HOST_API short SaveLogFile(char* file, bool SendDone);
HOST_API short WriteSerialNumbers(SerialNumbers sn);
HOST_API short ShutDown();
HOST_API short RFStartTest();
HOST_API RFTestData GetRFTestStatus();
HOST_API bool CheckUSBConnected();
HOST_API short SaveFlashLogFile(char* file);
HOST_API SystemErrorData GetSystemErrorStatus();
HOST_API short ClearSystemError();
HOST_API StepCounterData ReadStepCounter();
HOST_API StairCounterData ReadStairCounter();





