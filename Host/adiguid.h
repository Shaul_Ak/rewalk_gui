/*********************************************************************************

Copyright(c) 2005 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

// The below GUID is used to generate symbolic links to driver instances created from user mode.

#ifndef ADIGUID_H
#define ADIGUID_H

#include <initguid.h>

DEFINE_GUID(GUID_CLASS_BF_USB_EZEXTENDER,
0xeb8322c5, 0x8b49, 0x4feb, 0xae, 0x6e, 0xc9, 0x9b, 0x2b, 0x23, 0x20, 0x45);	// bulkadi

#endif // #ifndef ADIGUID_H

