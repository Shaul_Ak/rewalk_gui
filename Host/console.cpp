/*********************************************************************************

Copyright(c) 2005 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

#include "console.h"

CONSOLE_SCREEN_BUFFER_INFO gSavedCsbi;

/******************************************************************************
Routine Description:

    Saves the original screen settings

Arguments:

	None

Return Value:

    None

******************************************************************************/

void SaveConsoleAttributes()
{
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

	GetConsoleScreenBufferInfo(hStdOut, &gSavedCsbi);
}


/******************************************************************************
Routine Description:

    Restore the original screen settings

Arguments:

	None

Return Value:

    None

******************************************************************************/

void RestoreConsoleAttributes()
{
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

	// just restore the original text attributes
	SetConsoleTextAttribute(hStdOut, gSavedCsbi.wAttributes);
}


/******************************************************************************
Routine Description:

   Clears the screen

Arguments:

	None

Return Value:

    None

******************************************************************************/

void Clrscr()
{
	COORD coordScreen = { 0, 0 };
	DWORD dwCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD dwConSize;
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	// get info about console screen buffer
	GetConsoleScreenBufferInfo(hConsole, &csbi);

	// determine size of buffer
	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;

	// write character to console screen buffer
	FillConsoleOutputCharacter(hConsole, TEXT(' '), dwConSize, coordScreen, &dwCharsWritten);

	// get info about console screen buffer
	GetConsoleScreenBufferInfo(hConsole, &csbi);

	// set attributes
	FillConsoleOutputAttribute(hConsole, csbi.wAttributes, dwConSize, coordScreen, &dwCharsWritten);

	// set cursor position
	SetConsoleCursorPosition(hConsole, coordScreen);
}


/******************************************************************************
Routine Description:

   Moves the cursor to x, y in console window

Arguments:

	COORD point - xy point to move to

Return Value:

    None

******************************************************************************/

void SetCurrentCursorPosition(COORD point)
{
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), point);
}


/******************************************************************************
Routine Description:

   Moves the cursor to x, y in console window

Arguments:

	int x - x position
	int y - y position

Return Value:

    None

******************************************************************************/

void SetCurrentCursorPosition(int x, int y)
{
	COORD point;
	point.X = x; point.Y = y;
	// should protect against moving the cursor off the screen
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), point);
}


/******************************************************************************
Routine Description:

   Gets the current x, y position of the cursor within the console window

Arguments:

	None

Return Value:

    None

******************************************************************************/

COORD GetCurrentCursorPosition()
{
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	GetConsoleScreenBufferInfo(hStdOut, &csbi);

	return (csbi.dwCursorPosition);
}


/******************************************************************************
Routine Description:

   Sets the text and background colors

Arguments:

	int color - color

Return Value:

    None

******************************************************************************/

void SetRGB(int color)
{
	// if no background constant is specified, the background is black
	// if no foreground constant is specified, the text is black
	// intensity can be used to make foreground or background look BOLD

	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

	switch (color)
	{
	case WHITE:			// White on Black
		SetConsoleTextAttribute(hStdOut, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		break;
	case BR_WHITE:		// Bright White on Black
		SetConsoleTextAttribute(hStdOut, FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		break;
	case RED:			// Red on Black
		SetConsoleTextAttribute(hStdOut, FOREGROUND_RED);
		break;
	case BR_RED:		// Bright Red on Black
		SetConsoleTextAttribute(hStdOut, FOREGROUND_INTENSITY |	FOREGROUND_RED);
		break;
	case YELLOW:		// Yellow on Black
		SetConsoleTextAttribute(hStdOut, FOREGROUND_RED | FOREGROUND_GREEN);
		break;
	case BR_YELLOW:		// Bright Yellow on Black
		SetConsoleTextAttribute(hStdOut, FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN);
		break;
	case GREEN:			// Green on Black
		SetConsoleTextAttribute(hStdOut, FOREGROUND_GREEN);
		break;
	case BR_GREEN:		// Bright Green on Black
		SetConsoleTextAttribute(hStdOut, FOREGROUND_INTENSITY |	FOREGROUND_GREEN);
		break;
	case CYAN:			// Cyan on Black
		SetConsoleTextAttribute(hStdOut, FOREGROUND_GREEN | FOREGROUND_BLUE);
		break;
	case BR_CYAN:		// Bright Cyan on Black
		SetConsoleTextAttribute(hStdOut, FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_BLUE);
		break;
	case MAGENTA:		// Magenta on Black
		SetConsoleTextAttribute(hStdOut, FOREGROUND_RED | FOREGROUND_BLUE);
		break;
	case BR_MAGENTA:	// Bright Magenta on Black
		SetConsoleTextAttribute(hStdOut, FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_BLUE);
		break;
	default:			// White on Black
		SetConsoleTextAttribute(hStdOut, FOREGROUND_INTENSITY |	FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		break;
	}
}


/******************************************************************************
Routine Description:

   Prints in color

Arguments:

	int color - 		color
	const char *str - 	string

Return Value:

    None

******************************************************************************/

int ColorPrintf(int color, const char *str)
{
	SetRGB(color);
	int ret = printf(str);
	SetRGB(WHITE);
	return ret;
}


/******************************************************************************
Routine Description:

   Prints in color

Arguments:

	int color - 		color
	const char *str - 	string
	char *arg1 -		printf argument

Return Value:

    None

******************************************************************************/

int ColorPrintf(int color, const char *str, int arg1)
{
	SetRGB(color);
	int ret = printf(str, arg1);
	SetRGB(WHITE);
	return ret;
}


/******************************************************************************
Routine Description:

   Prints in color

Arguments:

	int color - 		color
	const char *str - 	string
	char *arg1 -		printf argument

Return Value:

    None

******************************************************************************/

int ColorPrintf(int color, const char *str, char *arg1)
{
	SetRGB(color);
	int ret = printf(str, arg1);
	SetRGB(WHITE);
	return ret;
}
