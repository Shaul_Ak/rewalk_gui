/*********************************************************************************

Copyright(c) 2005 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <setupapi.h>
#include "usbcmd.h"
#include "usbdriver.h"
#include "console.h"

// choose whether to display (1) or supress (0) printf debug information in this file
#if 1
	#define NOISY(_x_) printf _x_ ;
	#define CNOISY(c, _x_) SetRGB(c);printf _x_;SetRGB(WHITE);
#else
	#define NOISY(_x_) ;
	#define CNOISY(_x_);
#endif

/////////////////////////////////////////////////////
// globals
/////////////////////////////////////////////////////

const UINT g_uiMaxFiles = 512;		// maximum number of files we let be open at one time
FILE *g_pFiles[g_uiMaxFiles];		// file pointer array

extern DWORD g_dwTotalDevices;		// total devices detected
extern UINT g_unDeviceNumber;		// device number selected
extern char g_pszDeviceDesc[];		// device name string for display only
extern HANDLE g_hRead;				// USB device read handle
extern HANDLE g_hWrite;				// USB device write handle

/////////////////////////////////////////////////////
// prototypes
/////////////////////////////////////////////////////

int UsbioOpen(ULONG ulCount);
int UsbioClose(FILE *FilePtr);
int UsbioRead(FILE *FilePtr, size_t size);
int UsbioWrite(FILE *FilePtr, size_t size);
int UsbioSeek(FILE *FilePtr, ULONG offset, int whence);
FILE** GetAvailableFilePtr();
BOOL IsValidFilePtr(FILE *FilePtr);

/******************************************************************************
Routine Description:

    Serve file IO requests from the device

Arguments:

	None

Return Value:

    int - return status

******************************************************************************/

int ServeFileIoRequests()
{
	USBCB usbcb;									// USB control block
	ULONG nCountCurrent = 0x0;						// current byte count
	BOOL bKeepRunning = TRUE;						// flag to keep running
    BOOL IoStatus = TRUE;							// IO status
	int nRetVal = OPERATION_PASSED;					// return value
	UINT ui;										// temp index

	// init the global file pointer array
	for ( ui = 0; ui < g_uiMaxFiles; ui++ )
		g_pFiles[ui] = NULL;

	// send out a USBCB that tells the device we want to service file IO
	usbcb.ulCommand = USBIO_START;					// command
	usbcb.ulCount = 0x0;							// doesn't matter
	usbcb.ulData = 0x0;								// doesn't matter

	IoStatus = WritePipe(g_hWrite, &usbcb, sizeof(USBCB), &nCountCurrent);

	// check for error
	if (!IoStatus)
	{
		//ColorPrintf(BR_RED, "\n\nError WRITING usbcb for USBIO");
		nRetVal = IO_WRITE_USBCB_FAILED;
	}

	//ColorPrintf(BR_CYAN, "\n< HOST > Start servicing USBIO\n\n");

	// loop until we get a USBIO_STOP command from device,
	// or we get an error
	
	while(  bKeepRunning && (OPERATION_PASSED == nRetVal) )
	{
		// now we expect a USBCB before each packet of data
		IoStatus = ReadPipe(g_hRead, &usbcb, sizeof(usbcb), &nCountCurrent);

		// check for error
		if (!IoStatus)
		{
			//ColorPrintf(BR_RED, "\n\nError READING usbcb for USBIO");
			nRetVal = IO_READ_USBCB_FAILED;
			break;
		}

		// switch on command
		switch (usbcb.ulCommand)
		{
			// we should quit
		    case USBIO_STOP:
		    	bKeepRunning = FALSE;
				//ColorPrintf(BR_CYAN, "\n< HOST > Device terminated USBIO successfully\n");
		        break;

			// open file
			case USBIO_OPEN:
				nRetVal = UsbioOpen(usbcb.ulCount);
				break;

			// close file
			case USBIO_CLOSE:
				nRetVal = UsbioClose((FILE*)(usbcb.ulData));
				break;

			// read file
			case USBIO_READ:
				nRetVal = UsbioRead((FILE*)(usbcb.ulData), (size_t)(usbcb.ulCount));
				break;

			// write file
			case USBIO_WRITE:
				nRetVal = UsbioWrite((FILE*)(usbcb.ulData), (size_t)(usbcb.ulCount));
				break;

			// seek file
			case USBIO_SEEK_CUR:
			case USBIO_SEEK_END:
			case USBIO_SEEK_SET:
				nRetVal = UsbioSeek((FILE*)(usbcb.ulData), usbcb.ulCount, (int)usbcb.ulCommand);
				break;

			default:
				//ColorPrintf(BR_RED, "\n\nUnsupported command received for USBIO");
				nRetVal = UNSUPPORTED_COMMAND;
				break;
		}
	}

	// we should close all open files in case they were left open
	for (ui = 0; ui < g_uiMaxFiles; ui++)
	{
		if (NULL != g_pFiles[ui])
			UsbioClose(g_pFiles[ui]);
	}

	// return success
	return OPERATION_PASSED;
}


/******************************************************************************
Routine Description:

    Open a file on the host

Arguments:

	ULONG ulCount - number of bytes to receive for info block

Return Value:

    int - return status

******************************************************************************/
int UsbioOpen(ULONG ulCount)
{
	FILE **ppFile  = NULL;						// pointer into file pointer array
	char *pcData = NULL;				// data pointer
	int nRetVal = OPERATION_PASSED;		// return value
	ULONG nCountCurrent = 0x0;			// current byte count
	BOOL IoStatus = TRUE;				// IO status
	UINT *puiMode = NULL;						// mode UINT ptr received from device
	char pcMode[8];						// mode char array
	USBCB usbcb;						// USB control block

	char *pcFilename = NULL;					// filename ptr
	// get available file ptr
	ppFile = GetAvailableFilePtr();

	// if there are no available file pointers we must fail, this means the
	// user has already opened the maximum amount of files that we allow
	if (!ppFile)
	{
		//ColorPrintf(BR_RED, "\n\nMaximum files already open");
		nRetVal = NO_AVAILABLE_FILE_PTRS;
	}

	// allocate some storage
	pcData = new char[ulCount];
	if (!pcData)
	{
		//ColorPrintf(BR_RED, "\n\nError allocating memory for FILE OPEN");
		nRetVal = OUT_OF_MEMORY_ON_HOST;
	}

	// USBIO_OPEN requires receiving a block of data containing the mode and filename
	IoStatus = ReadPipe(g_hRead, pcData, ulCount, &nCountCurrent);

	// check for error
	if (!IoStatus)
	{
		//ColorPrintf(BR_RED, "\n\nError READING INFO for FILE OPEN");
		nRetVal = ERROR_OPENING_FILE;
	}

	// if all is still good we can attempt to open the file
	if (OPERATION_PASSED == nRetVal)
	{
		// at this point we should have received the mode and filename info
		puiMode = (UINT*)(pcData + FILE_OPEN_MODE_OFFSET);
		pcFilename = pcData + FILE_OPEN_FILENAME_OFFSET;

		// need to make a string for the mode
		switch (*puiMode)
		{
			case 0x4000: strcpy( pcMode, "r" ); break;	// read
			case 0x4301: strcpy( pcMode, "w" ); break;	// write
			case 0x4109: strcpy( pcMode, "a" ); break;	// write and append
			case 0x4002: strcpy( pcMode, "r+" ); break;	// read and write
			case 0x4302: strcpy( pcMode, "w+" ); break;	// empty file for read and write
			case 0x410a: strcpy( pcMode, "a+" ); break;	// read and append
			case 0x8000: strcpy( pcMode, "rb" ); break;	// binary
			case 0x8301: strcpy( pcMode, "wb" ); break;	// binary
			default:	 strcpy( pcMode, "r+" ); break;	// read and write
		}

		// open it
		*ppFile = fopen( pcFilename, pcMode );
	}

	// send the file ptr back to the device
	usbcb.ulCommand = USBIO_FILEPTR;
	if (ppFile && (OPERATION_PASSED == nRetVal) )
		usbcb.ulData = (ULONG)*ppFile;	// file ptr
	else
		usbcb.ulData = (ULONG)NULL;			// else there was a problem, set it to NULL
	IoStatus = WritePipe(g_hWrite, &usbcb, sizeof(USBCB), &nCountCurrent);

	// free the buffer
	if ( pcData )
		delete [] pcData;

	// return success
	return nRetVal;
}

/******************************************************************************
Routine Description:

    Close a file on the host

Arguments:

	FILE *FilePtr - file ptr of the file to close, this is the file ptr obtained
					in the call to UsbioOpen()

Return Value:

    int - return status

******************************************************************************/
int UsbioClose(FILE *FilePtr)
{
	// make sure it's a valid file ptr, if so close it
	if ( IsValidFilePtr(FilePtr) )
		fclose(FilePtr);

	// clear out this file ptr so it can be used again
	for (UINT ui = 0; ui < g_uiMaxFiles; ui++)
	{
		// find it and NULL it out
		if (FilePtr == g_pFiles[ui])
		{
			g_pFiles[ui] = NULL;
			break;
		}
	}

	// return success
	return OPERATION_PASSED;
}

/******************************************************************************
Routine Description:

    Read from a file

Arguments:

	FILE *FilePtr - file ptr of the file to read, this is the file ptr obtained
					in the call to UsbioOpen()
	size_t size -	size in bytes to read

Return Value:

    int - return status

******************************************************************************/
int UsbioRead(FILE *FilePtr, size_t size)
{
	int nActual = 0;				// actual number of bytes read
	char *pcData = NULL;			// data pointer
	ULONG nCountCurrent = 0x0;		// current byte count
	USBCB usbcb;					// USB control block
	BOOL IoStatus = TRUE;			// IO status
	int	nRetVal = OPERATION_PASSED;	// return value

	// allocate some storage, we add a few more bytes in case it's a stdin read
	pcData = new char[size + 4];
	if (!pcData)
	{
		//ColorPrintf(BR_RED, "\n\nError allocating memory for FILE READ");
		return OUT_OF_MEMORY_ON_HOST;
	}

	// see if it's a stdin read
	if ( USBIO_STDIN_FD == (int)FilePtr )
	{
		// do the read, up to size bytes and figure out actual length read
		fgets( pcData, size, stdin );
		nActual = strlen(pcData);				// don't include the NULL

		usbcb.ulCommand = USBIO_READ_REPLY;		// command
		usbcb.ulCount = nActual;				// use ulCount to store actual
		usbcb.ulData = nActual;					// no error
	}

	// else see if it's a valid file ptr, if so attempt the read
	else if ( IsValidFilePtr(FilePtr) )
	{
		// do the read
		nActual = fread( pcData, sizeof( char ), size, FilePtr );

		usbcb.ulCommand = USBIO_READ_REPLY;		// command
		usbcb.ulCount = nActual;				// use ulCount to store actual

		// use ulData to store EOF, error, or actual bytes
		if ( feof(FilePtr) )
			usbcb.ulData = 0x0;
		else if ( ferror(FilePtr) )
			usbcb.ulData = 0xffffffff;
		else
			usbcb.ulData = nActual;
	}

	// else it was an invalid FilePtr we must fail
	else
	{
		usbcb.ulCommand = USBIO_READ_REPLY;		// command
		usbcb.ulCount = 0x0;					// use ulCount to store actual
		usbcb.ulData = 0xffffffff;				// use ulData to store failure
	}

	// USBIO_READ requires sending back a USBCB with actual bytes, EOF, and error info
	IoStatus = WritePipe(g_hWrite, &usbcb, sizeof(USBCB), &nCountCurrent);

	// check for error
	if (!IoStatus)
	{
		//ColorPrintf(BR_RED, "\n\nError WRITING usbcb for FILE READ");
		nRetVal = IO_WRITE_USBCB_FAILED;
	}

	// if there's actual data to send and we don't have an error do it now
	if ( nActual && IoStatus )
	{
		IoStatus = WritePipe(g_hWrite, pcData, sizeof(char) * nActual, &nCountCurrent);

		// check for error
		if (!IoStatus)
		{
			//ColorPrintf(BR_RED, "\n\nError WRITING data for FILE READ");
			nRetVal = IO_WRITE_DATA_FAILED;
		}
	}

	// free the buffer
	if ( pcData )
		delete [] pcData;

	// return value
	return nRetVal;
}

/******************************************************************************
Routine Description:

    Write to a file

Arguments:

	FILE *FilePtr - file ptr of the file to write, this is the file ptr obtained
					in the call to UsbioOpen()
	size_t size -	size in bytes to write

Return Value:

    int - return status

******************************************************************************/
int UsbioWrite(FILE *FilePtr, size_t size)
{
	int nActual = 0;				// actual number of bytes read
	char *pcData = NULL;			// data pointer
	ULONG nCountCurrent = 0x0;		// current byte count
	USBCB usbcb;					// USB control block
	BOOL IoStatus = TRUE;			// IO status
	int	nRetVal = OPERATION_PASSED;	// return value

	// allocate some storage, we add a few more bytes because we add a NULL if
	// it's a stdout or stderr write
	pcData = new char[size + 4];
	if (!pcData)
	{
		//ColorPrintf(BR_RED, "\n\nError allocating memory for FILE WRITE");
		return OUT_OF_MEMORY_ON_HOST;
	}

	// receive the data
	IoStatus = ReadPipe(g_hRead, pcData, size, &nCountCurrent);

	// check for error
	if (!IoStatus)
	{
		//ColorPrintf(BR_RED, "\n\nError READING data for FILE WRITE");
		nRetVal = IO_READ_DATA_FAILED;
	}

	// see if it's a stdout write
	if ( USBIO_STDOUT_FD == (int)FilePtr )
	{
		pcData[size] = (char)NULL;					// add NULL to string
		//ColorPrintf(BR_GREEN, "<DEVICE> ");	// print host tag
		//ColorPrintf(BR_GREEN, pcData);			// print string

		usbcb.ulCommand = USBIO_WRITE_REPLY;	// command
		usbcb.ulCount = size;					// use ulCount to store actual
		usbcb.ulData = size;					// no error
	}

	// else see if it's a stderr write
	else if ( USBIO_STDERR_FD == (int)FilePtr )
	{
		pcData[size] = (char) NULL;					// add NULL to string
		//ColorPrintf(BR_RED, "<DEVICE> ");		// print host tag
		//ColorPrintf(BR_RED, pcData);			// print string

		usbcb.ulCommand = USBIO_WRITE_REPLY;	// command
		usbcb.ulCount = size;					// use ulCount to store actual
		usbcb.ulData = size;					// no error
	}

	// else see if it's a valid file ptr, if so attempt the write
	else if ( IsValidFilePtr(FilePtr) && IoStatus )
	{
		// we should now have the data to write to the file so do the write
		nActual = fwrite( pcData, sizeof( char ), size, FilePtr );

		usbcb.ulCommand = USBIO_WRITE_REPLY;	// command
		usbcb.ulCount = nActual;				// use ulCount to store actual

		// use ulData to store closed, error, or actual bytes
		if ( ferror(FilePtr) )
			usbcb.ulData = 0xffffffff;
		else
			usbcb.ulData = nActual;
	}

	// else it was an invalid FilePtr we must fail
	else
	{
		usbcb.ulCommand = USBIO_WRITE_REPLY;	// command
		usbcb.ulCount = 0x0;					// use ulCount to store actual
		usbcb.ulData = 0xffffffff;				// use ulData to store failure
	}

	// USBIO_WRITE requires sending back a USBCB with actual bytes, closed, and error info
	IoStatus = WritePipe(g_hWrite, &usbcb, sizeof(USBCB), &nCountCurrent);

	// check for error
	if (!IoStatus)
	{
		//ColorPrintf(BR_RED, "\n\nError WRITING usbcb for FILE WRITE");
		nRetVal = IO_WRITE_USBCB_FAILED;
	}

	// free the buffer
	if ( pcData )
		delete [] pcData;

	// return value
	return nRetVal;
}

/******************************************************************************
Routine Description:

    Seek to a location in a file

Arguments:

	FILE *FilePtr - file ptr of the file to seek, this is the file ptr obtained
					in the call to UsbioOpen()
	ULONG ofset -	number of bytes from origin
	int whence -	initial position

Return Value:

    int - return status

******************************************************************************/
int UsbioSeek(FILE *FilePtr, ULONG offset, int whence)
{
	ULONG nCountCurrent = 0x0;		// current byte count
	USBCB usbcb;					// USB control block
	BOOL IoStatus = TRUE;			// IO status
	int	nRetVal = OPERATION_PASSED;	// return value
	int origin = SEEK_SET;						// origin
	int nSeekResult = 0;			// seek result

	// make sure it's a valid file ptr, if so attempt the seek
	if ( IsValidFilePtr(FilePtr) )
	{
		// determine the origin needed by the host
		if ( USBIO_SEEK_SET == whence )
			origin = SEEK_SET;
		else if ( USBIO_SEEK_CUR == whence )
			origin = SEEK_CUR;
		else if ( USBIO_SEEK_END == whence )
			origin = SEEK_END;

		// do the seek, it returns zero for success, non-zero otherwise
		nSeekResult = fseek( FilePtr, offset, origin );

		// if successful
		if ( !nSeekResult )
		{
			usbcb.ulCommand = USBIO_SEEK_REPLY;		// command
			usbcb.ulCount = ftell(FilePtr);			// new file position
			usbcb.ulData = 0x0;						// status
		}

		// else there was an error
		else
		{
			usbcb.ulCommand = USBIO_SEEK_REPLY;		// command
			usbcb.ulCount = 0x0;					// new file position
			usbcb.ulData = 0xffffffff;				// status
		}
	}

	// else it was an invalid FilePtr we must fail
	else
	{
		usbcb.ulCommand = USBIO_SEEK_REPLY;		// command
		usbcb.ulCount = 0x0;					// new file position
		usbcb.ulData = 0xffffffff;				// status
	}

	// USBIO_SEEK_XXX requires sending back a USBCB with new file position and status
	IoStatus = WritePipe(g_hWrite, &usbcb, sizeof(USBCB), &nCountCurrent);

	// check for error
	if (!IoStatus)
	{
		//ColorPrintf(BR_RED, "\n\nError WRITING usbcb for FILE SEEK");
		nRetVal = IO_WRITE_USBCB_FAILED;
	}

	// return value
	return nRetVal;
}

/******************************************************************************
Routine Description:

    Find available file pointer

Arguments:

	None

Return Value:

    FILE** - pointer to store file pointer

******************************************************************************/
FILE** GetAvailableFilePtr()
{
	for (UINT ui = 0; ui < g_uiMaxFiles; ui++)
	{
		if (NULL == g_pFiles[ui])
			return &g_pFiles[ui];
	}

	// we didn't find a free location, return NULL
	return NULL;
}

/******************************************************************************
Routine Description:

    Check to see if the supplied file pointer is valid

Arguments:

	FILE *FilePtr - file pointer to check

Return Value:

    BOOL - TRUE if valid, FALSE if not valid

******************************************************************************/
BOOL IsValidFilePtr(FILE *FilePtr)
{
	for (UINT ui = 0; ui < g_uiMaxFiles; ui++)
	{
		if (FilePtr == g_pFiles[ui])
			return TRUE;
	}

	// we didn't find it
	return FALSE;
}