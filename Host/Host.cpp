// Host.cpp : Defines the exported functions for the DLL application.
//

#include <windows.h>
//#include <stdio.h>
#include <conio.h>
#include <setupapi.h>
#include "usbcmd.h"
#include "usbdriver.h"
#include "console.h"
#include "Host.h"
#include "stdlib.h"
//#include <string.h>

// guid for this device
EXTERN_C const GUID GUID_CLASS_BF_USB_EZEXTENDER;

// SYNC/ASYNC defines, used to determine which method of I/O you want to use
#define USE_SYNC	FALSE
#define USE_ASYNC	TRUE

/////////////////////////////////////////////////////
// globals
/////////////////////////////////////////////////////
DWORD g_dwTotalDevices = 0;									// total devices detected
UINT g_unDeviceNumber = 0;									// device number selected
char g_pszDeviceDesc[256] = "Argo Medical Devices USB Driver"; //"Blackfin USB-LAN EZ-EXTENDER"; // device name string for display only
HANDLE g_hRead = INVALID_HANDLE_VALUE;						// USB device read handle
HANDLE g_hWrite = INVALID_HANDLE_VALUE;						// USB device write handle

int GENERAL_BUFF_READ_ADDRESS = 0x900000;
int LOG_BUFF_READ_ADDRESS = 0x400000;


//Forward Declarations
BOOL ConnectToDevice( BOOL bUseAsyncIo );
BOOL IsSupportedCommand( UINT uiCommand );
char* ReadDeviceMemory(_USB_COMMAND readCommand, UINT uiAddress, UINT uiCount );
short FillDeviceMemory(_USB_COMMAND writeCommand, UINT uiAddress, UINT uiCount, char* buffer);
//char* ReadExcelData(UINT uiAddress, UINT& uiRows, UINT& uiColumns);
//int GetFirmwareVersionStrings(BYTE* pbyVersion);
void BufferToDeviceParameters(char* result, DeviceParameters& dp);


bool CheckUSBConnected( void )
{
	
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	 
    return TRUE == ConnectToDevice(USE_ASYNC) ? true : false;

    
}

int  GetBuffAddress(const BuffType BufferType)
{
	int* address = NULL;
	
	int retval = 0;
	
	switch(BufferType)
	{
		case GENERAL_BUFF:
			if (IsSupportedCommand(GET_GEN_BUFF_ADDRESS))
			{
				
				address = (int*)ReadDeviceMemory(GET_GEN_BUFF_ADDRESS, GENERAL_BUFF_READ_ADDRESS, sizeof(int));
				if( address != NULL )
					retval = *address;
			}
			else
			{
				retval = DEFAULT_BUFF_ADDRESS;
			}
			break;
		case LOG_BUFF:
			if (IsSupportedCommand(GET_LOG_BUFF_ADDRESS))
			{
				address = (int*)ReadDeviceMemory(GET_LOG_BUFF_ADDRESS, GENERAL_BUFF_READ_ADDRESS, sizeof(int));
				if( address != NULL )
					retval = *address;
			}

			break;
		default:
			break;
	}
	return retval;

}


HOST_API DeviceParameters ReadDeviceParameters()
{
	DeviceParameters dp;
	memset(&dp,0,sizeof(DeviceParameters));
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(READ_REWALK_PARAMETERS))
		{
			
			//Last short in the device parameters is "status" field, don't read it
			int const address =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == address )
			{
				dp.status = IO_READ_DATA_FAILED;
				return dp;
			}
			char* const result = ReadDeviceMemory(READ_REWALK_PARAMETERS, address, sizeof(DeviceParameters) - sizeof(short));
			if(result != NULL)
			{
				BufferToDeviceParameters(result, dp);
				dp.status = OPERATION_PASSED;
			}
			else
			{
				dp.status = IO_READ_DATA_FAILED;
			}
		}
		else
		{
			dp.status = UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		dp.status = COULD_NOT_CONNECT;
	}

	return dp;
}



HOST_API DeviceParameters RestoreDeviceParameters()
{
	DeviceParameters dp;
	memset(&dp,0,sizeof(DeviceParameters));
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(RESTORE_FACTORY_PARAMETERS))
		{
			const int address =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == address )
			{
				dp.status = IO_READ_DATA_FAILED;
				dp.serialNumberStatus = 0;
				return dp;
			}
			//Last short in the device parameters is "status" field, don't read it
			char* const result = ReadDeviceMemory(RESTORE_FACTORY_PARAMETERS, address, sizeof(DeviceParameters) - sizeof(short));
			if(result != NULL)
			{
				BufferToDeviceParameters(result, dp);
				dp.status = OPERATION_PASSED;
			}
			else
			{
				dp.status = IO_READ_DATA_FAILED;
			}
		}
		else
		{
			dp.status = UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		dp.status = COULD_NOT_CONNECT;
	}

	return dp;
}




HOST_API short WriteDeviceParameters(	char* const UserName, const int UserNameSize,
	                                    char* const ComputerName,const int ComputerNameSize,
										char* const Date, const int DateSize,
										char* const UpperStrapHolderSizeString, const int UpperStrapHolderSizeString_length, 
										char* const UpperStrapHolderTypeString, const int UpperStrapHolderTypeString_length, 
										char* const AboveKneeBracketTypeString, const int AboveKneeBracketTypeString_length,
										char* const AboveKneeBracketAnteriorString, const int AboveKneeBracketAnteriorString_length, 
										char* const KneeBracketTypeString, const int KneeBracketTypeString_length, 
										char* const KneeBracketAnteriorPositionString, const int KneeBracketAnteriorPositionString_length,
										char* const KneeBracketLateralPositionString, const int KneeBracketLateralPositionString_length,
										char* const PelvicSizeString, const int PelvicSizeString_length,
										char* const PelvicAnteriorPositionString, const int PelvicAnteriorPositionString_length,
										char* const PelvicVerticalPositionString, const int PelvicVerticalPositionString_length,
										char* const FootPlateTypeString, const int FootPlateTypeString_length,
										char* const FootPlateSizeString, const int FootPlateSizeString_length,
										char* const FootPlateDorsiFlexionPositionString, const int FootPlateDorsiFlexionPositionString_length,
										char* const ASCSpeedString, const int ASCSpeedString_length,
										char* const DSCSpeedString, const int DSCSpeedString_length,
									    DeviceParameters dp )
{
	
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(WRITE_REWALK_PARAMETERS))
		{
			const int address =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == address )
			{
				dp.status = IO_READ_DATA_FAILED;
				return dp.status;
			}
			
			memset(&dp.UserName[0],0,UserNameSize+1);
			memset(&dp.ComputerName[0],0,ComputerNameSize+1);
			memset(&dp.WriteDataTime[0],0,DateSize);
			memset(&dp.UpperStrapHolderSizeString[0],0,10);
			memset(&dp.UpperStrapHolderTypeString[0],0,10);
			memset(&dp.AboveKneeBracketTypeString[0],0,10);
			memset(&dp.AboveKneeBracketAnteriorString[0],0,10);
			memset(&dp.KneeBracketTypeString[0],0,10);
			memset(&dp.KneeBracketAnteriorPositionString[0],0,10);
			memset(&dp.KneeBracketLateralPositionString[0],0,10);
			memset(&dp.PelvicSizeString[0],0,10);
			memset(&dp.PelvicAnteriorPositionString[0],0,10);
			memset(&dp.PelvicVerticalPositionString[0],0,10);
			memset(&dp.FootPlateTypeString[0],0,10);
			memset(&dp.FootPlateSizeString[0],0,10);
			memset(&dp.FootPlateDorsiFlexionPositionString[0],0,10);
			memset(&dp.ASCSpeedString[0],0,10);
			memset(&dp.DSCSpeedString[0],0,10);


	        memcpy(&dp.UserName[0],UserName,UserNameSize);
			memcpy(&dp.ComputerName[0], ComputerName,ComputerNameSize);
			memcpy(&dp.WriteDataTime[0], Date,DateSize);
			memcpy(&dp.UpperStrapHolderSizeString[0], UpperStrapHolderSizeString,UpperStrapHolderSizeString_length);
			memcpy(&dp.UpperStrapHolderTypeString[0], UpperStrapHolderTypeString,UpperStrapHolderTypeString_length);
     		memcpy(&dp.AboveKneeBracketTypeString[0], AboveKneeBracketTypeString,AboveKneeBracketTypeString_length);
	   		memcpy(&dp.AboveKneeBracketAnteriorString[0], AboveKneeBracketAnteriorString,AboveKneeBracketAnteriorString_length);
	   		memcpy(&dp.KneeBracketTypeString[0], KneeBracketTypeString, KneeBracketTypeString_length);
	  	    memcpy(&dp.KneeBracketAnteriorPositionString[0], KneeBracketAnteriorPositionString, KneeBracketAnteriorPositionString_length);
	 	    memcpy(&dp.KneeBracketLateralPositionString[0], KneeBracketLateralPositionString, KneeBracketLateralPositionString_length);
 	        memcpy(&dp.PelvicSizeString[0], PelvicSizeString, PelvicSizeString_length);
            memcpy(&dp.PelvicAnteriorPositionString[0], PelvicAnteriorPositionString, PelvicAnteriorPositionString_length);
			memcpy(&dp.PelvicVerticalPositionString[0], PelvicVerticalPositionString, PelvicVerticalPositionString_length);
			memcpy(&dp.FootPlateTypeString[0], FootPlateTypeString, FootPlateTypeString_length);
			memcpy(&dp.FootPlateSizeString[0], FootPlateSizeString, FootPlateSizeString_length);
			memcpy(&dp.FootPlateDorsiFlexionPositionString[0], FootPlateDorsiFlexionPositionString, FootPlateDorsiFlexionPositionString_length);
			memcpy(&dp.ASCSpeedString[0], ASCSpeedString, ASCSpeedString_length);
			memcpy(&dp.DSCSpeedString[0], DSCSpeedString, DSCSpeedString_length);
			
		//Write the device parameters excluding last "status" field
			dp.status = FillDeviceMemory(WRITE_REWALK_PARAMETERS, address, sizeof(DeviceParameters), (char*)&dp);
		}
		else
		{
			dp.status = UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		dp.status = COULD_NOT_CONNECT;
	}

	return dp.status;
}

HOST_API short WriteSerialNumbers(SerialNumbers sn)
{
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(WRITE_SERIAL_NUMBERS))
		{
			const int address =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == address )
			{
				return IO_WRITE_DATA_FAILED;
			}
			//Write just serial number fields of the struct
			return FillDeviceMemory(WRITE_SERIAL_NUMBERS, address, sizeof(sn), (char*)&sn);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}

}

HOST_API short WriteRCAddressID(const short address, const short id)
{
	short data[2];
	data[0] = address;
	data[1] = id;
	
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(WRITE_RC_ADDRESS_ID))
		{
			const int BuffAddress =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == BuffAddress )
			{
				return IO_READ_DATA_FAILED;
			}
			//Write the device parameters excluding last "status" field
			return FillDeviceMemory(WRITE_RC_ADDRESS_ID, BuffAddress,sizeof(data), (char *)data);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
}

HOST_API short WriteINFAddressID(const short address, const short id)
{
	short data[2];
	data[0] = address;
	data[1] = id;
	
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(WRITE_INF_ADDRESS_ID))
		{
			const int BuffAddress =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == BuffAddress )
			{
				return IO_WRITE_DATA_FAILED;
			}
			//Write the device parameters excluding last "status" field
			return FillDeviceMemory(WRITE_INF_ADDRESS_ID, BuffAddress, sizeof(data), (char *)data);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
}


HOST_API RCAddressIDData GetWriteRCAddressIDStatus()
{
	RCAddressIDData td;
	memset(&td,0,sizeof(RCAddressIDData));
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		
		// check to see if the firmware supports this command
		if (IsSupportedCommand(WRITE_RC_ADDRESS_ID_STATUS))
			{
				const int address =  GetBuffAddress(GENERAL_BUFF);
				if( (int)NULL == address )
				{
					td.status = IO_READ_DATA_FAILED;
					return td;
				}
				char* result=NULL;
				
				//Last short in the device parameters is "status" field, don't read it
				result = ReadDeviceMemory(WRITE_RC_ADDRESS_ID_STATUS, address, sizeof(RCAddressIDData) - sizeof(short));
				
				if(result != NULL)
				{
					short* temp = (short*)result;
					td.RC_Address = *temp++;
					td.RC_ID = *temp;
					td.status = OPERATION_PASSED;
				}
				else
				{
					td.status = IO_READ_DATA_FAILED;
				}
					
			}
			else
			{
				td.status = UNSUPPORTED_COMMAND;
			}
				
	}
	else
	{
		td.status = COULD_NOT_CONNECT; 
	}
	return td;
}

HOST_API short WriteINFAddress(short address)
{
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(WRITE_INF_ADDR))
		{
			const int BuffAddress =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == BuffAddress )
			{
				
				return IO_WRITE_DATA_FAILED;
			}
			//Write the device parameters excluding last "status" field
			return FillDeviceMemory(WRITE_INF_ADDR, BuffAddress, sizeof(short), (char*)&address);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
}

HOST_API short WriteINFId(int id)
{
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(WRITE_INF_ID))
		{
			const int address =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == address )
			{
				
				return IO_WRITE_DATA_FAILED;
			}
			//Write the device parameters excluding last "status" field
			return FillDeviceMemory(WRITE_INF_ID, address, sizeof(short), (char*)&id);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
}


HOST_API short Download(char* const   data, const int count, const int command)
{
	const _USB_COMMAND downloadCommand = (_USB_COMMAND)command;
	const _USB_COMMAND downloadCommandComplete = (_USB_COMMAND)(command + 1);
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(downloadCommand))
		{
			const int address =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == address )
			{
				
				return IO_WRITE_DATA_FAILED;
			}
			const short result = FillDeviceMemory(downloadCommand, address, count, data);
			if(result == OPERATION_PASSED)
			{
				return FillDeviceMemory(downloadCommandComplete, 0x0, 0, NULL);
			}
			else
			{
				return result;
			}
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
}

HOST_API CustomCommandResult RunCustomCommand(char* const  data, const int count, const int command)
{
	const _USB_COMMAND customCommand = (_USB_COMMAND)command;
	CustomCommandResult result;
	memset(&result,0,sizeof(CustomCommandResult));
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(customCommand))
		{
			const int address =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == address )
			{
				result.status = IO_WRITE_DATA_FAILED;
				return result;
			}
			result.status = FillDeviceMemory(customCommand, address, count, data);
			if (result.status == OPERATION_PASSED)
			{
				if (count > 0)
				{
				if (IsSupportedCommand(RECEIVE_COMMAND_RESULT))
				{
					
					Sleep(50);
					char* const buffer = ReadDeviceMemory(RECEIVE_COMMAND_RESULT, address, sizeof(CustomCommandResult) - sizeof(short));
					if(buffer != NULL)
					{
					    result.id1 = buffer[0];
						result.id2 = buffer[1];
						result.byte1 = buffer[2];
						result.byte2 = buffer[3];
						result.byte3 = buffer[4];
						result.byte4 = buffer[5];
						result.byte5 = buffer[6];
						result.byte6 = buffer[7];
						result.byte7 = buffer[8];
						result.byte8 = buffer[9];
						result.status = OPERATION_PASSED;
					}
					else
					{
						result.status = IO_READ_DATA_FAILED;
					}
				}
				else
				{	
					result.status = UNSUPPORTED_COMMAND;
				}
				}
			}
		}
		else
		{
			result.status = UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		result.status = COULD_NOT_CONNECT;
	}

	return result;
}


HOST_API OnlineData ReadOnlineData()
{
	OnlineData od;
	memset(&od,0,sizeof(OnlineData));
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(READ_ONLINE_DEVICE_DATA))
		{
			const int address =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == address )
			{
				od.status = IO_READ_DATA_FAILED;
				return od;
			}
			//Last short in the device parameters is "status" field, don't read it
			char* const result = ReadDeviceMemory(READ_ONLINE_DEVICE_DATA, address, sizeof(OnlineData) - sizeof(short));
			if(result != NULL)
			{
				short* temp = (short*)result;

				od.lhAngle		= *temp++;
				od.lkAngle		= *temp++;
				od.rhAngle		= *temp++;
				od.rkAngle		= *temp++;
				od.tiltXAngle	= *temp++;
				od.tiltYAngle	= *temp++;
				od.fsrRight1	= *temp++;
				od.fsrRight2	= *temp++;
				od.fsrRight3	= *temp++;
				od.fsrLeft1		= *temp++;
				od.fsrLeft2		= *temp++;
				od.fsrLeft3		= *temp++;
				od.mainBattery	= *temp++;
				od.auxBattery	= *temp++;
				od.custom1		= *temp++;
				od.custom2		= *temp++;
				od.custom3		= *temp++;
				od.custom4		= *temp++;
				od.custom5		= *temp;
				od.status = OPERATION_PASSED;
			}
			else
			{
				od.status = IO_READ_DATA_FAILED;
			}
		}
		else
		{
			od.status = UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		od.status = COULD_NOT_CONNECT;
	}

	return od;
}



HOST_API short StartTest()
{
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(TEST_START))
		{
			return FillDeviceMemory(TEST_START, 0, 0, NULL);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
}


HOST_API TestData GetTestStatus()
{
	TestData td;
	memset(&td,0,sizeof(TestData));
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(TEST_STATUS))
		{
			const int address =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == address )
			{
				td.status = IO_READ_DATA_FAILED;
				return td;
			}
			//Last short in the device parameters is "status" field, don't read it
			char* const result = ReadDeviceMemory(TEST_STATUS, address, sizeof(TestData) - sizeof(short));
			if(result != NULL)
			{
				short* temp = (short*)result;

				td.inf = *temp++;
				td.rc  = *temp++;
				td.lh  = *temp++;
				td.lk  = *temp++;
				td.rh  = *temp++;
				td.rk  = *temp;
				td.status = OPERATION_PASSED;
			}
			else
			{
				td.status = IO_READ_DATA_FAILED;
			}
		}
		else
		{
			td.status = UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		td.status = COULD_NOT_CONNECT;
	}

	return td;
}


HOST_API short RFStartTest()
{
	
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(RF_TEST_START))
		{
			return FillDeviceMemory(RF_TEST_START, 0, 0, NULL);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
	
}

HOST_API  RFTestData GetRFTestStatus()
{
	RFTestData td;
	memset(&td,0,sizeof(RFTestData));
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		//RF_TEST_START,
	//RF_TEST_STATUS,
		// check to see if the firmware supports this command
		if (IsSupportedCommand(RF_TEST_STATUS))
			{
				char* result=NULL;
				const int address =  GetBuffAddress(GENERAL_BUFF);
				if( (int)NULL == address )
				{
					td.status = IO_READ_DATA_FAILED;
					return td;
				}
				td.numOfPackets = 0;
				//Last short in the device parameters is "status" field, don't read it
				result = ReadDeviceMemory(RF_TEST_STATUS, address, sizeof(RFTestData) - sizeof(short));
				
				if(result != NULL)
				{
					short* temp = (short*)result;
					td.inf_BER = *temp++;
					td.rc_BER = *temp++;
					td.numOfPackets = *temp;
					td.status = OPERATION_PASSED;
				}
				else
				{
					td.status = IO_READ_DATA_FAILED;
				}
					
			}
			else
			{
				td.status = UNSUPPORTED_COMMAND;
			}
				
	}
	else
	{
		td.status = COULD_NOT_CONNECT; 
	}
	return td;
}

HOST_API SystemErrorData GetSystemErrorStatus()
{
	SystemErrorData td;
	memset(&td,0,sizeof(SystemErrorData));
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		//RF_TEST_START,
	//RF_TEST_STATUS,
		// check to see if the firmware supports this command
		if (IsSupportedCommand(GET_SYSTEM_ERROR_STATUS))
			{
				char* result=NULL;
				const int address =  GetBuffAddress(GENERAL_BUFF);
				if( (int)NULL == address )
				{
					td.status = IO_READ_DATA_FAILED;
					return td;
				}
				
				//Last short in the device parameters is "status" field, don't read it
				result = ReadDeviceMemory(GET_SYSTEM_ERROR_STATUS, address, sizeof(SystemErrorData) - sizeof(short));
				
				if(result != NULL)
				{
					td.SystemErrorStatus = *(short *)result;
					td.status = OPERATION_PASSED;
				}
				else
				{
					td.status = IO_READ_DATA_FAILED;
				}
					
			}
			else
			{
				td.status = UNSUPPORTED_COMMAND;
			}
				
	}
	else
	{
		td.status = COULD_NOT_CONNECT; 
	}
	return td;
}

HOST_API short StartCalibration(const bool lh, const bool lk, const bool rh, const bool rk)
{
	short calib[4];
	calib[0] = lh ? 1 : 0;
	calib[1] = lk ? 1 : 0;
	calib[2] = rh ? 1 : 0;
	calib[3] = rk ? 1 : 0;

	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(START_CALIBRATE))
		{
			const int address =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == address )
			{
				
				return IO_READ_DATA_FAILED;
			}
			return FillDeviceMemory(START_CALIBRATE, address, 4*sizeof(short), (char*)calib);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
}


HOST_API short DoCalibration()
{
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(CALIBRATE))
		{
			return FillDeviceMemory(CALIBRATE, 0, 0, NULL);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
}

HOST_API short ClearSystemError()
{
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(CLEAR_SYSTEM_ERROR_STATUS))
		{
			return FillDeviceMemory(CLEAR_SYSTEM_ERROR_STATUS, 0, 0, NULL);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
}


HOST_API short ResetStairCounter()
{
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(RESET_STAIR_COUNTER))
		{
			return FillDeviceMemory(RESET_STAIR_COUNTER, 0, 0, NULL);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
}
HOST_API short ResetStepCounter()
{
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(RESET_STEP_COUNTER))
		{
			return FillDeviceMemory(RESET_STEP_COUNTER, 0, 0, NULL);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
}
HOST_API short ClearlogFile()
{
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(RESET_SDRAM_LOG))
		{
			return FillDeviceMemory(RESET_SDRAM_LOG, 0, 0, NULL);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
}

HOST_API StepCounterData ReadStepCounter()
{
	StepCounterData td;
	td.StepCounterData = 0;
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		//RF_TEST_START,
	//RF_TEST_STATUS,
		// check to see if the firmware supports this command
		if (IsSupportedCommand(READ_STEP_COUNTER))
			{
				char* result=NULL;
				const int address =  GetBuffAddress(GENERAL_BUFF);
				if( (int)NULL == address )
				{
					td.status = IO_READ_DATA_FAILED;
					return td;
				}
				
				//Last short in the device parameters is "status" field, don't read it
				result = ReadDeviceMemory(READ_STEP_COUNTER, address, sizeof(SystemErrorData) - sizeof(short));
				
				if(result != NULL)
				{
					td.StepCounterData = *(short *)result;
					td.status = OPERATION_PASSED;
				}
				else
				{
					td.status = IO_READ_DATA_FAILED;
				}
					
			}
			else
			{
				td.status = UNSUPPORTED_COMMAND;
			}
				
	}
	else
	{
		td.status = COULD_NOT_CONNECT; 
	}
	return td;
}


HOST_API StairCounterData ReadStairCounter()
{
	StairCounterData td;
	td.StairCounterData = 0;
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		//RF_TEST_START,
	//RF_TEST_STATUS,
		// check to see if the firmware supports this command
		if (IsSupportedCommand(READ_STAIR_COUNTER))
			{
				char* result=NULL;
				const int address =  GetBuffAddress(GENERAL_BUFF);
				if( (int)NULL == address )
				{
					td.status = IO_READ_DATA_FAILED;
					return td;
				}
				
				//Last short in the device parameters is "status" field, don't read it
				result = ReadDeviceMemory(READ_STAIR_COUNTER, address, sizeof(SystemErrorData) - sizeof(short));
				
				if(result != NULL)
				{
					td.StairCounterData = *(short *)result;
					td.status = OPERATION_PASSED;
				}
				else
				{
					td.status = IO_READ_DATA_FAILED;
				}
					
			}
			else
			{
				td.status = UNSUPPORTED_COMMAND;
			}
				
	}
	else
	{
		td.status = COULD_NOT_CONNECT; 
	}
	return td;
}



HOST_API  TestData  GetCalibrationStatus()
{
	TestData td;
	
	memset(&td,0,sizeof(TestData));
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(CALBIRATE_STATUS))
		{
			//Read just 4 shorts
			const int address =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == address )
			{
				td.status = IO_READ_DATA_FAILED;
				return td;
			}
			char* const  result = ReadDeviceMemory(CALBIRATE_STATUS, address, 4*sizeof(short));
			if(result != NULL)
			{
				short* temp = (short*)result;
				td.lh  = *temp++;
				td.lk  = *temp++;
				td.rh  = *temp++;
				td.rk  = *temp;
				td.status = OPERATION_PASSED;
			}
			else
			{
				td.status = IO_READ_DATA_FAILED;
			}
		}
		else
		{
			td.status = UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		td.status = COULD_NOT_CONNECT;
	}
	
	return td;
}

HOST_API short SaveExcelData(char* file)
{
	file = 0;
	return COULD_NOT_CONNECT;
	/*g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(SAVE_EXCEL_FILE_SIZE))
		{
			int address =  GetBuffAddress(GENERAL_BUFF);
			if( NULL == address )
			{
				
				return IO_READ_DATA_FAILED;
			}
			int* size = (int*)ReadDeviceMemory(SAVE_EXCEL_FILE_SIZE, address, 8);
			if(size != NULL)
			{
			int rows = size[0];
			int columns = size[1];

			float* result = (float*)ReadDeviceMemory(SAVE_EXCEL_FILE_DATA, address, rows*columns*sizeof(float));
			if(result != NULL)
			{
				FILE* f = fopen(file, "w");
				fprintf(f, "%s,%s,%s,%s,%s,%s,%s\n", "Time", "LH_Angle", "LK_Angle", "RH_Angle", "RK_Angle", "Tilt_X_Angle", "Tilt_Y_Angle");
				for(int r = 0; r < rows; r++)
				{
					for(int c = 0; c < columns; c++)
					{
						fprintf(f, "%f,", result[r+c*rows]);
					}

					fprintf(f,"\n");
				}
				fclose(f);
				
				return OPERATION_PASSED;
			}
			else
			{
				return IO_READ_DATA_FAILED;
			}
			}
			else
			{
				return IO_READ_DATA_FAILED;
			}
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}*/
}

HOST_API short SaveLogFile(char* const  file, const bool SendDone)
{
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	short DoneResult=0;
	if (ConnectToDevice(USE_ASYNC))
	{
		if (IsSupportedCommand(GET_LOG_FILE_SIZE))
		{
			int* size=NULL;
			int address =  GetBuffAddress(GENERAL_BUFF);
			if((int) NULL == address )
			{
				
				return IO_READ_DATA_FAILED;
			}
			size = (int*)ReadDeviceMemory(GET_LOG_FILE_SIZE, address, sizeof(size));
			if(size != NULL)
			{
				// check to see if the firmware supports this command
				if (IsSupportedCommand(SAVE_LOG_FILE))
				{
					address =  GetBuffAddress(LOG_BUFF);
					if( (int)NULL == address )
					{
				
						return IO_READ_DATA_FAILED;
					}
					unsigned char* const  result = (unsigned char*)ReadDeviceMemory(SAVE_LOG_FILE, address, *size);
					if(result != NULL)
					{
						if( SendDone )
						{
							DoneResult = FillDeviceMemory(SAVE_LOG_FILE_DONE, 0x0, 0, NULL);
							if( OPERATION_PASSED != DoneResult )
								return DoneResult; 
						}
						FILE* const  f = fopen(file, "wb");

						if(!f)  
							return IO_READ_DATA_FAILED;
                 

						for(int c = 0; c < *size; c++)
						{
							//fprintf(f, "%.2x\n", result[c]);
							fprintf(f, "%c", result[c]);
							//if( !((c+1) % 16) )
							//	fprintf(f,"\n");
						}

						fclose(f);
				
						return OPERATION_PASSED;
					}
					else
					{
						return IO_READ_DATA_FAILED;
					}
				}
			}
			else
			{
				return IO_READ_DATA_FAILED;
			}
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
	return UNSUPPORTED_COMMAND;
}

HOST_API short SaveFlashLogFile(char* const file)
{
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		if (IsSupportedCommand(GET_FLASH_LOG_FILE_SIZE))
		{
			int* size = NULL;
			const int address =  GetBuffAddress(GENERAL_BUFF);
			if( (int)NULL == address )
			{
				
				return IO_READ_DATA_FAILED;
			}
			size = (int*)ReadDeviceMemory(GET_FLASH_LOG_FILE_SIZE, address, sizeof(size));
			if(size != NULL)
			{
				// check to see if the firmware supports this command
				if (IsSupportedCommand(SAVE_LOG_FILE))
				{
					/*address =  GetBuffAddress(GENERAL_BUFF);
					if( NULL == address )
					{
				
						return IO_READ_DATA_FAILED;
					}*/
					const unsigned char* result = (const unsigned char*)ReadDeviceMemory(SAVE_FLASH_LOG_FILE, address, *size);
					if(result != NULL)
					{
					 FILE* f = fopen(file, "wb");
						if(!f)
							 return IO_READ_DATA_FAILED;
        

						for(int c = 0; c < *size; c++)
						{
							//fprintf(f, "%.2x\n", result[c]);
							fprintf(f, "%c", result[c]);
							//if( !((c+1) % 16) )
							//	fprintf(f,"\n");
						}

						fclose(f);
				
						return OPERATION_PASSED;
					}
					else
					{
						return IO_READ_DATA_FAILED;
					}
				}
			}
			else
			{
				return IO_READ_DATA_FAILED;
			}
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
	return UNSUPPORTED_COMMAND;
}


HOST_API short ShutDown()
{
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{
		// check to see if the firmware supports this command
		if (IsSupportedCommand(EXIT_USB_INTERFACE))
		{
			return FillDeviceMemory(EXIT_USB_INTERFACE, 0, 0, NULL);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}

}
/*
HOST_API short SendDebugCommand( const char *command)
{
	int temp = strlen (command);
	g_dwTotalDevices = QueryNumDevices((LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER);
	if (ConnectToDevice(USE_ASYNC))
	{

		const int address =  GetBuffAddress(GENERAL_BUFF);
		if((int) NULL == address )
		{
				
			return IO_WRITE_DATA_FAILED;
		}
			
		// check to see if the firmware supports this command
		if (IsSupportedCommand(SEND_DEBUG_COMMAND))
		{
			return FillDeviceMemory(SEND_DEBUG_COMMAND, address, strlen (command), command);
		}
		else
		{
			return UNSUPPORTED_COMMAND;
		}
	}
	else
	{
		return COULD_NOT_CONNECT;
	}
return UNSUPPORTED_COMMAND;
}
*/

/******************************************************************************
Routine Description:

Connect to a device based on the device number.

Arguments:

BOOL bUseAsyncIo - specifies if we should use ASYNC (TRUE) or SYNC (FALSE) IO

Return Value:

BOOL success (TRUE) or failure (FALSE)

******************************************************************************/
BOOL ConnectToDevice(const BOOL bUseAsync)
{
	// if no devices found print message and return
	if ( !g_dwTotalDevices )
	{
		//printf( "\nNo ");
		//ColorPrintf(BR_GREEN, "%s ", g_pszDeviceDesc);
		//printf("devices were found\n");
		return FALSE;
	}

	// open write handle
	g_hWrite = OpenDeviceHandle( (LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER, WRITE_PIPE, g_unDeviceNumber, bUseAsync );
	if( g_hWrite == INVALID_HANDLE_VALUE)
	{
		//ColorPrintf(BR_RED, "Error opening driver\n");
		return FALSE;
	}

	// open read handle
	g_hRead = OpenDeviceHandle( (LPGUID)&GUID_CLASS_BF_USB_EZEXTENDER, READ_PIPE, g_unDeviceNumber, bUseAsync );
	if( g_hRead == INVALID_HANDLE_VALUE)
	{
		//ColorPrintf(BR_RED, "Error opening driver\n");
		return FALSE;
	}

	// ok
	return TRUE;
}


/******************************************************************************
Routine Description:

Query the firmware to see if it supports a given command.

Arguments:

UINT uiCommand - command we want to query for

Return Value:

BOOL success (TRUE) or failure (FALSE)

******************************************************************************/
BOOL IsSupportedCommand(const  UINT uiCommand )
{
	USBCB usbcb;									// USB control block
	ULONG nCountCurrent = 0x0;						// current byte count
	//BOOL IoStatus = false ? false:true; //TRUE;							// IO status

	// send out a USBCB that tells the device we want to query for support
	usbcb.ulCommand = QUERY_SUPPORT;				// command
	usbcb.ulCount = 0x0;							// doesn't matter
	usbcb.ulData = uiCommand;						// command to query for

	BOOL IoStatus = WritePipe(g_hWrite, &usbcb, sizeof(USBCB), &nCountCurrent);

	// check for error
	if (!IoStatus)
	{
		//ColorPrintf(BR_RED, "\n\nError WRITING usbcb for QUERY FIRMWARE");
		return FALSE;
	}

	// now we expect a USBCB back indicating the query result
	IoStatus = ReadPipe(g_hRead, &usbcb, sizeof(usbcb), &nCountCurrent);

	// check for error
	if (!IoStatus)
	{
		//ColorPrintf(BR_RED, "\n\nError READING usbcb for QUERY FIRMWARE");
		return FALSE;
	}

	// ulData contains the result, either TRUE or FALSE
	return (BOOL)usbcb.ulData;
}


/******************************************************************************
Routine Description:

Get the firmware version strings.

Arguments:

BYTE *pbyVersion -	pointer to a buffer to store the strings in

Return Value:

int -				return status

******************************************************************************/
/*
int GetFirmwareVersionStrings(BYTE *pbyVersion)
{
	USBCB usbcb;								// command block
	ULONG ulActualBytes=0;						// track actual bytes
	BOOL IoStatus = false?false:true;//true;//TRUE;						// iostatus

	// send command for firmware version
	usbcb.ulCommand = GET_FW_VERSION;			// command
	usbcb.ulCount = VERSION_STRING_BLOCK_SIZE;	// number of bytes
	usbcb.ulData = 0x0;							// not used for this command

	IoStatus = WritePipe(g_hWrite, &usbcb, sizeof(USBCB), &ulActualBytes);

	// check for error
	if (!IoStatus)
	{
		//ColorPrintf(BR_RED, "\n\nError WRITING usbcb for FIRMWARE VERSION");
		return IO_WRITE_USBCB_FAILED;
	}

	// now get the firmware version
	IoStatus = ReadPipe(g_hRead, pbyVersion, VERSION_STRING_BLOCK_SIZE, &ulActualBytes);

	// check for error
	if (!IoStatus)
	{
		//ColorPrintf(BR_RED, "\n\nError READING data for FIRMWARE VERSION");
		return IO_READ_DATA_FAILED;
	}

	return OPERATION_PASSED;
}

*/
void BufferToDeviceParameters(char* result, DeviceParameters& dp)
{
	const short* temp = (const short*)result;

	dp.kneeAngle			= *temp++;
	dp.hipAngle				= *temp++;
	dp.backHipAngle			= *temp++;
	dp.maxVelocity			= *temp++;
	dp.tiltDelta			= *temp++;
	dp.tiltTimeout			= *temp++;
	dp.safetyWhileStanding  = *temp++;
	dp.xThreshold			= *temp++;
	dp.yThreshold			= *temp++;
	dp.delayBetweenSteps	= *temp++;
	dp.buzzerBetweenSteps	= *temp++;
	dp.record				= *temp++;
	dp.sampleTime			= *temp++;
	dp.rcAddress			= *temp++;
	dp.rcID					= *temp++;
	dp.softwareVersion		= *temp++;
	dp.dspVersion			= *temp++;
	dp.motorVersion			= *temp++;
	dp.rcVersion			= *temp++;
	dp.fpgaVersion			= *temp++;
	dp.bootVersion			= *temp++;
	dp.switchFSRs			= *temp++;
	dp.fsrThreshold 		= *temp++;
	dp.fsrWalkTimeout		= *temp++;
	dp.lhAsc				= *temp++;
	dp.lkAsc				= *temp++;
	dp.rhAsc				= *temp++;
	dp.rkAsc				= *temp++;
	dp.lhDsc				= *temp++;
	dp.lkDsc				= *temp++;
	dp.rhDsc				= *temp++;
	dp.rkDsc				= *temp++;
	dp.dspSoftwareProgrammed= *temp++;
	dp.systemCalbirationStatus    = *temp++;
	dp.serialNumberStatus	= *temp++;
	dp.sn.sn1				= *temp++;
	dp.sn.sn2				= *temp++;
	dp.sn.sn3				= *temp++;
	dp.sn.infSN1			= *temp++;
	dp.sn.infSN2			= *temp++;
	dp.sn.infSN3			= *temp++;
	dp.sn.infSN4			= *temp++;
	dp.sn.rcSN1				= *temp++;
	dp.sn.rcSN2				= *temp++;
	dp.sn.rcSN3				= *temp++;
	dp.sn.rcSN4				= *temp++;
	dp.sn.lhSN1				= *temp++;
	dp.sn.lhSN2				= *temp++;
	dp.sn.lhSN3				= *temp++;
	dp.sn.lhSN4				= *temp++;
	dp.sn.lkSN1				= *temp++;
	dp.sn.lkSN2				= *temp++;
	dp.sn.lkSN3				= *temp++;
	dp.sn.lkSN4				= *temp++;
	dp.sn.rhSN1				= *temp++;
	dp.sn.rhSN2				= *temp++;
	dp.sn.rhSN3				= *temp++;
	dp.sn.rhSN4				= *temp++;
	dp.sn.rkSN1				= *temp++;
	dp.sn.rkSN2				= *temp++;
	dp.sn.rkSN3				= *temp++;
	dp.sn.rkSN4				= *temp++;

	dp.WalkCurrentThreshold = *temp++;
	dp.StairsCurrentThreshold = *temp++;

	dp.StairsEnabled = *temp++;
	dp.SysType = *temp++;
	//Log params
    dp.CANLogLevel = *temp++;
    dp.MCULogLevel = *temp++;
    dp.RFLogLevel = *temp++;
    dp.RCLogLevel = *temp++;
    dp.ADCLogLevel = *temp++;
    dp.StairsLogLevel = *temp++;
    dp.SitStnLogLevel = *temp++;
    dp.WalkLogLevel = *temp++;
	dp.USBLogLevel = *temp++;
	dp.BITLogLevel = *temp++;
	dp.OperationLogLevel = *temp++;
	dp.FlashLogLevel = *temp++;
	dp.FirstStepFlexion = *temp++;
	dp.SystemErrorType = *temp++;
	dp.StepCounter     = *(unsigned int *)(temp);
	temp += 2;//sizeof(dp.StepCounter);
	dp.EnableSystemStuckOnError_int = *(unsigned int *)(temp);
	temp += 2;
	dp.SwitchingTime = *temp++;	
	//Tsn.ODO: Continue
	dp.AFTSitCounter = *temp++;
    dp.AFTASCCounter =  *temp++;
    dp. AFTDSCCounter = *temp++;
    dp. AFTSitStatus = *temp++;
    dp. AFTASCStatus = *temp++; 
    dp. AFTDSCStatus =  *temp++;
	dp. AFTWalkStatus = *temp++;
	dp.StairsASCSpeed =*temp++ ; 
	dp.StairsDSCSpeed = *temp++;
	dp.Placing_RH_on_the_stair = *temp++;
	dp.Placing_RK_on_the_stair = *temp++;
	dp.StepLength = *(unsigned int *)(temp);
	temp += 2;
	dp.StairCounter = *(unsigned int *)(temp);
	temp += 2;
	dp.CurrentHandleAlgo = *temp++;
	dp.UpperLegLength = *temp++;
	dp.LowerLegLength = *temp++;
	dp.SoleHeight= *temp++;
	dp.ShoeSize= *temp++;
	dp.StairHeight= *temp++;
	dp.PicSWVersion = *temp++;

	//New GUI6 Look and Feel design
	dp.UnitsSelection = *temp++;
	dp.Gender = *(unsigned int *)(temp);
	temp += 2;
	dp.User_ID    = *(unsigned int *)(temp);
	temp += 2;
	dp.Weight = *temp++;
	dp.Height = *temp++;
	dp.UpperStrapHoldersSize = *temp++;
	dp.UpperStrapHoldersPosition = *temp++;
	dp.AboveKneeBracketSize = *temp++;
	dp.AboveKneeBracketPosition = *temp++;
	dp.FrontKneeBracketAssemble = *temp++;
	dp.FrontKneeBracketAnteriorPosition = *temp++;
	dp.FrontKneeBracketLateralPosition  = *temp++;
	dp.PelvicSize = *temp++;
	dp.PelvicAnteriorPosition = *temp++;
	dp.PelvicVerticalPosition = *temp++;
	dp.UpperLegHightSize = *temp++;
	dp.LowerLegHightSize = *temp++;
	dp.FootPlateType1 = *temp++;
	dp.FootPlateType2 = *temp++;
	dp.FootPlateNoOfRotation = *(unsigned int *)(temp);
	temp += 2;
    dp.AFTStopsStatus = *temp++;
    dp.INFBatteriesLevel = *temp++;
    dp.Ack_Movement_Point = *temp++;
	dp.Enable_First_Step_Rewalk6 = *temp++;
	dp.SystemResrvedParmeter5 = *temp++;
	dp.SystemResrvedParmeter6 = *temp++;
	dp.SystemResrvedParmeter7 = *temp++;
	dp.SystemResrvedParmeter8 = *temp++;
	dp.SystemResrvedParmeter9 = *temp++;
    dp.SystemResrvedParmeter10 = *temp;

	


}


