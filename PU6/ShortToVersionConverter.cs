﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Rewalk.Device;
using System.Diagnostics;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using RewalkControls;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Win32;

using System.Threading;


namespace PU
{
    public class ShortToVersionConverter : IValueConverter
    {
        #region IValueConverter Members

       

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int version;
            //int version = int.Parse(string.Format("{0}", value));
            bool cc = int.TryParse(string.Format("{0}", value), out version);

            byte Mech_Ver,HW_Ver,SW_Ver;
           

            Mech_Ver = (byte)(version >> 24);
            HW_Ver = (byte)(version >> 16);
            SW_Ver = (byte)(version >> 8);
            /*
            if ((byte)(version >> 8) <= 23)
            {

                SW_Ver = (byte)(version >> 8);
            }
            else
            {

                SW_Ver = (byte)((byte)(version >> 8) - 23);
            }
            */
            
            if ((byte)(version >> 8) == 0xff)
                return string.Format("NO COMM"); //founf in RCconvertor
            else
            {
                if ((short)version == 0x0909) //check if Boot version is running
                    return string.Format(" BOOT Running");
                else
                    return string.Format(" {0}.{1}.{2}.{3}", Mech_Ver, HW_Ver, SW_Ver, (byte)version);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
