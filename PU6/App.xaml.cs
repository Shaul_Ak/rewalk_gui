﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Win32;
using Rewalk.Device;
using System.Diagnostics;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using RewalkControls;
using System.Threading;
using System.Windows.Input;
using System.Security;
using System.Text;
using System.Windows.Media;
using System.Windows.Documents;
using System.Net;
using System.Net.Mail;
using System.Windows.Markup;
using FileParser;
namespace PU
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
        public bool DeptName_Connected { get; set; }
        public short DeptName_AFTCONWALKST { get; set; }
        public short DeptName_AFTSTOPSSTATUS { get; set; }
        public short DeptName_AFTCONSITSTANDST { get; set; }
        public short DeptName_AFTCONASCST { get; set; }
        public short DeptName_AFTCONDSCST { get; set; }
        public short DeptName_AFTCONWALKCOUNTER { get; set; }
        public short DeptName_AFTCONSITSTANDCOUNTER { get; set; }
        public short DeptName_AFTCONASCCOUNTER { get; set; }
        public short DeptName_AFTCONDSCCOUNTER { get; set; }
        public short DeptName_CurrentTestRunning { get; set; }
        public string DeptName_TesterFirstName { get; set; }
        public bool DeptName_RunINFtoApplication { get; set; }


        //New GUI6
        public string DeptName_SysType { get; set; }
      
        
	}
}