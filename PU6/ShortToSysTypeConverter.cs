﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Collections.ObjectModel;
using System.Xml;


namespace PU
{
    
    public class ShortToSysTypeConverter : IValueConverter
    {
        
        #region IValueConverter Members
        public string returnedsystemtype;
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string[] str = new string[6];
            string[] retval = new string[6];
            //string str1;
            int i;
            
           

            if (value != null)
            {
               
                
                ReadOnlyCollection<XmlNode> nodes = value as ReadOnlyCollection<XmlNode>;
                if (nodes != null && nodes.Count > 0)
                {
                    str = nodes[0].InnerText.Split(new char[] { ',', ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                    //return str;
                    //retval[0] = "Rewalk-I";
                    //return retval;
                    //i = temp.GetType();
                    for (i = 0; i < str.Length; i++)
                    {
                        switch(str[i] )
                        {
                            
                            case "2":
                                retval[i] = "Rewalk-I";
                                break;
                            case "3":
                                retval[i] = "Rewalk-P0";
                                break;
                            case "4":
                                retval[i] = "Rewalk-R";
                                break;
                            case "5":
                                retval[i] = "Rewalk-P6"; 
                                break;
                            default:
                                retval[i] = "Error";
                                break;
                        }
                        
                    }
                  
                    return retval;
                    //return nodes[0].InnerText.Split(new char[] { ',', ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                }
            }

            return "Error!"; // null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
