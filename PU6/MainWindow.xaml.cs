﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Win32;
using Rewalk.Device;
using System.Diagnostics;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using RewalkControls;
using System.Threading;
using System.Windows.Input;
using System.Security;
using System.Text;
using System.Windows.Media;
using System.Windows.Documents;
using System.Net;
using System.Net.Mail;
using System.Windows.Markup;
using System.Collections.Generic;
using FileParser;

using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Windows.Navigation;

namespace PU
{
	/// <summary>
	/// Interaction logic for PU.xaml
	/// </summary>
	public partial class PU6 : Window
	{
        public PU6()
        {
               InitializeComponent();
            Detect_killRewalkRunningApllication();
            DataContext = deviceData;
           
           ReWalkStartUpConfiguration();
           UploadLanguageSelection();
        //    tbInterfaceVersion.Text = "Rewalk Interface " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            string InterfaceVersion =  "Rewalk Interface " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            System.Version GUIVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            Short_GUIVersion = (short)(GUIVersion.Major * 100 + GUIVersion.Build * 10 + GUIVersion.Revision);
         //   tbInterfaceVersion.Text = Logger.Validate(InterfaceVersion);


          
            deviceData.Parameters.UnitsSelection = 0;
            deviceData.Parameters.RCAddress = 0; deviceData.Parameters.RCId = 0;
            Events_Init();
            this.Closed += new EventHandler(OnMainClosed);
          
          //  AFTToolScreenSet();
            //    SetTitle(null);
           // btnUpdateINF.IsEnabled = false;
           // cbSysType.IsEnabled = true;
            //btnSetSN.IsEnabled = false; 
         //   btnUpdateRC.IsEnabled = false;//Must do this after InitializeComponent to run animation                 
            // Knee_Flexion.Items.CopyTo(array1, 0);
         //   btnBootBrowse.IsEnabled = false; btnBootDownload.IsEnabled = false;
            string temp = AppDomain.CurrentDomain.BaseDirectory; string[] temp1;
            DirectoryInfo dir_info;
            temp1 = temp.Split('\\'); LogPath = temp1[0] + "\\Argo\\logs"; EncoderTestPath = temp1[0] + "\\Argo\\Encoder_Plots"; string AFTFolderPath = temp1[0] + "\\Argo\\AFT";
            UserProfilesPath = temp1[0] + "\\Argo\\UsersProfiles";
            try
            {
                if (!Directory.Exists(AFTFolderPath))
                    dir_info = Directory.CreateDirectory(AFTFolderPath);
                if (!Directory.Exists(LogPath))
                    dir_info = Directory.CreateDirectory(LogPath);
                SysLogPath = temp1[0] + "\\Argo\\logs\\sys_logs";
                if (!Directory.Exists(SysLogPath))
                    dir_info = Directory.CreateDirectory(SysLogPath);
                if (!Directory.Exists(EncoderTestPath))
                    dir_info = Directory.CreateDirectory(EncoderTestPath);
                if (!Directory.Exists(UserProfilesPath))
                    dir_info = Directory.CreateDirectory(UserProfilesPath);
            }
            catch (Exception exception)
            {
                // MessageBox.Show(exception.Message);
                Logger.Error(null, exception.Message);
            }

            OnDisconenctUSB();


            (App.Current as App).DeptName_RunINFtoApplication = true;

      


            //New GUI 6
            XMLParamLoadCheckValid.GainMinMaxLengthValues();
        

         
        }
        private void UploadLanguageSelection()//This Function uploading Language Selection
        {

            if (Properties.Settings.Default.Language_Selection == "English")
                MuEnglish.IsChecked = true;
            if (Properties.Settings.Default.Language_Selection == "Deutsch")
                MuDeutsch.IsChecked = true;
            if (Properties.Settings.Default.Language_Selection == "Français")
                MuFrançais.IsChecked = true;
            if (Properties.Settings.Default.Language_Selection == "Italiano")
                MuItaliano.IsChecked = true;
            if (Properties.Settings.Default.Language_Selection == "Türkçe")
                MuTürkçe.IsChecked = true;
            if (Properties.Settings.Default.Language_Selection == "日本語")
                Mu日本語.IsChecked = true;
            if (Properties.Settings.Default.Language_Selection == "Español")
                Muespañol.IsChecked = true;
            if (Properties.Settings.Default.Language_Selection == "Pусский")
                русский.IsChecked = true;
            
            
        }
        private void ReWalkStartUpConfiguration() //this Function will read xml configration
        {
            try
            {
                XmlDocument deviceSettings = new XmlDocument();
                deviceSettings.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");
                string currenthanlderalgo = deviceSettings.SelectSingleNode("/DeviceSettingsValues/CurrentHandlerAlgo").OuterXml;
                string[] CurrentHandleStatus = currenthanlderalgo.Split('#');
                if (CurrentHandleStatus[1] == "1")
                {
                    CurrentHandleAlogMethod = 1;
                }
                else
                {
                    CurrentHandleAlogMethod = 0;
                }
                string SelectedArea = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RegionConfig").OuterXml;
                string[] name = SelectedArea.Split('#');
                if (name[1] == "USA" || name[1] == "Other")
                {
                    if (name[1] == "USA")
                    {

                        deviceData.Parameters.StairsEnabled = false;
                     //   StairsTotalText.Visibility = Visibility.Hidden;
                        StairsCounterValue.Visibility = Visibility.Hidden;
                        StairsBorderIcon.Visibility = Visibility.Hidden;
                        GUIVersionType = "USA";

                    }
                    else if (name[1] == "Other")
                    {
                        GUIVersionType = "Other";
                      //  StairsTotalText.Visibility = Visibility.Visible;
                        StairsCounterValue.Visibility = Visibility.Visible;
                        StairsBorderIcon.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    Close();
                }
            }
            catch (Exception exception)
            {
         //       Logger.Error_MessageBox_Advanced("Loading System Configuration Parameters Failed:", "Loading System Configuration", exception.Message);

            }

        }


        string InstallingAppDir = Directory.GetCurrentDirectory();
        private bool LogFileSent = false;
        [Flags]
        enum ReportType { ERROR, NORMAL , WARNING};
        public enum USER_MESSAGES : int
        {
            USER_MESSAGE_0 = 0,
            USER_MESSAGE_1 = 1,
            USER_MESSAGE_2 = 2,
            USER_MESSAGE_3 = 3,
            USER_MESSAGE_4 = 4,
            USER_MESSAGE_5 = 5,
            USER_MESSAGE_6 = 6,
            USER_MESSAGE_7 = 7,
            USER_MESSAGE_8 = 8,
            USER_MESSAGE_9 = 9,
            USER_MESSAGE_10 = 10,
            USER_MESSAGE_11 = 11,
            USER_MESSAGE_12 = 12,
            USER_MESSAGE_13 = 13,
        };

        List<string> TransStringslist = new List<string>();
        private string LogPath;
        private string SysLogPath;
        private string EncoderTestPath;
        private short Short_GUIVersion;
        private string UserProfilesPath;
     



        private void Events_Init()
        {
            onlineTimer.Tick += new EventHandler(OnCollectDataTimer);
            testTimer.Interval = TimeSpan.FromSeconds(10);
           
            //Trainning.OnChanged += new EventHandler(Trainingmode_OnChanged);
            //  FirstStep.OnChanged += new EventHandler(OnEnableDisableFirstStep);
            // safetyonoff.OnChanged += new EventHandler(OnOffBox_OnChanged);
            // collectOnline.Click += new EventHandler(OnCollectOnlineDataChanged);
            //collectOnline.Checked += new EventHandler(OnCollectOnlineDataChanged);
            //  collectOnline.Click += new RoutedEventHandler(Mytoogle);
           
            CheckUSBConnectedTime.Tick += new EventHandler(OnCheckUSBConnectedTimeTimer_Tick);
          
            BatteryStatusTimer.Tick += new EventHandler(BatteryStatusTimer_Tick);
            RCUpdateTimer.Interval = TimeSpan.FromSeconds(1);
            RFtestTimer.Interval = TimeSpan.FromSeconds(1);
            calibTimer.Interval = TimeSpan.FromSeconds(1);
            TestEncoderTimer.Interval = TimeSpan.FromSeconds(1);
            CheckUSBConnectedTime.Interval = TimeSpan.FromMilliseconds(200);//SamanUSB
            BatteryStatusTimer.Interval = TimeSpan.FromMilliseconds(500);

            //Timer Added 
            UpdateProgressBarTimer.Tick += new EventHandler(UpdateProgressBarTimer_Tick);
            UpdateProgressBarTimer.Interval = TimeSpan.FromMilliseconds(100);


            calibTimer.IsEnabled = false;
            TestEncoderTimer.IsEnabled = false;
            CheckUSBConnectedTime.IsEnabled = true;
            testTimer.IsEnabled = false;
            BatteryStatusTimer.IsEnabled = false;

           


        }
		
		
		
      private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                this.DragMove();
            }
            catch (Exception ex) 
            {
                ;
            }
		}

        private void WinClose(object sender, System.Windows.RoutedEventArgs e)
        {
        	this.Close();
        }
        void OnMainClosed(object sender, EventArgs e)
        {


            CheckUSBConnectedTime.IsEnabled = false;
            deviceData.Disconnect();
            progressform.Close();
            GenerateRCAddressIDForm.Close();

            MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
            MCUDownloadingProcessWindow.Close();

            //    Resolution.CResolution ChangeRes = new Resolution.CResolution((int)CurrentScreenWidth, (int)CurrentScreenHeight);//Set  screen resolution back

            System.Diagnostics.Process.GetCurrentProcess().Kill();
            Application.Current.Shutdown();


        }
        private void OnCaptionButton(object sender, RoutedEventArgs e)
        {
            CheckUSBConnectedTime.IsEnabled = false;
            deviceData.Disconnect();
            progressform.Close();
            GenerateRCAddressIDForm.Close();

            MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
            MCUDownloadingProcessWindow.Close();

            //    Resolution.CResolution ChangeRes = new Resolution.CResolution((int)CurrentScreenWidth, (int)CurrentScreenHeight);//Set  screen resolution back

            System.Diagnostics.Process.GetCurrentProcess().Kill();
            Application.Current.Shutdown();

        }
        private void RemoveTitle()
        {
            //  UserFileloadedName.Visibility = Visibility.Hidden;
            // UserFileloadedName.Text = "";
        }
   

        private void OnLoadConfiguration(object sender, RoutedEventArgs e) // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions" CS.MLC "Methods executing atomic functions"
        {
            OpenFileDialog ofd = new OpenFileDialog();
            Data deviceData_temp = new Data();
            ofd.CheckFileExists = true;
            ofd.Filter = "Argo Config files (*.xarg)|*.xarg|(*.*) |*.*";
            FileStream fs;
            try
            {
                if (ofd.ShowDialog().Value == true)
                {
                    using (fs = new FileStream(ofd.FileName, FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(DeviceParameters));
                        deviceData_temp.Parameters = (DeviceParameters)serializer.Deserialize(fs);
                        if (Temp_ParamData_StructureCopy(deviceData_temp) == false)
                            return;

                    }
                    if (deviceData_temp.Parameters.SampleTime >= Short_GUIVersion)
                    {
                        UserParamData_VerfiytoSave(deviceData_temp);
                        using (fs = new FileStream(ofd.FileName, FileMode.Create))
                        {
                            XmlSerializer serializer_save = new XmlSerializer(typeof(DeviceParameters));
                            deviceData.Parameters.SampleTime = Short_GUIVersion;
                            serializer_save.Serialize(fs, deviceData.Parameters);
                        }
                        fs.Close();
         
                    }
                    else
                    {
                       // MessageBox.Show(this, "This file was created by an older Rewalk interface version and is not compatible to the current version. In order to continue the operation it will be converted to a compatible format and saved.",
                       //                          "User file Loading", MessageBoxButton.OK, MessageBoxImage.Information);
                        Display_User_Message(USER_MESSAGES.USER_MESSAGE_9); 
                        
                        deviceData.RestoreParameters();
                        UserParamData_VerfiytoSave(deviceData_temp);
                        // Overwrite the configuration file 
                        using (fs = new FileStream(ofd.FileName, FileMode.Create))
                        {
                            XmlSerializer serializer_save = new XmlSerializer(typeof(DeviceParameters));
                            deviceData.Parameters.SampleTime = Short_GUIVersion;
                            serializer_save.Serialize(fs, deviceData.Parameters);
                        }
                        fs.Close();
                     }



          //          Write.IsEnabled = true;
                   /* if (deviceData.Parameters.SwitchFSRs == false) //disable Training mode fields if training button is OFF
                    {
                        hip_final.IsEnabled = false;
                        hip_max.IsEnabled = false;
                        t_steptime.IsEnabled = false;
                    }
                    else
                    {
                        hip_final.IsEnabled = true;
                        hip_max.IsEnabled = true;
                        t_steptime.IsEnabled = true;
                    }
                    if (deviceData.Parameters.SafetyWhileStanding == false) //disable safety if SafetyWhileStanding                                   
                        Y_Thr.IsEnabled = false;
                    else
                        Y_Thr.IsEnabled = true;
                    */
                }
            }
            catch (Exception exception)
            {
                //MessageBox.Show(this, "Load configuration failed: Corrupted configuration file.");// + exception.Message);
                Logger.Dummy_Error("Load configuration failed", exception.Message);
                Display_User_Message(USER_MESSAGES.USER_MESSAGE_4);    
            }
        }

        
        private bool Temp_ParamData_StructureCopy(Data deviceData_temp)
        {

            if (deviceData_temp.Parameters.SysType != deviceData.Parameters.SysType || deviceData_temp.Parameters.SysTypeString != deviceData.Parameters.SysTypeString)
            {
                MessageBox.Show((string)this.FindResource("Sorry the file can't be opened because it has different type of system"), "", MessageBoxButton.OK, MessageBoxImage.Hand);
                return false;
            }
            else
            {
                deviceData_temp.Parameters.RCAddress = deviceData.Parameters.RCAddress;
                deviceData_temp.Parameters.RCId = deviceData.Parameters.RCId;
                deviceData_temp.Parameters.RCBER = deviceData.Parameters.RCBER;
                deviceData_temp.Parameters.Remote = deviceData.Parameters.Remote;
                deviceData_temp.Parameters.RFProgress = deviceData.Parameters.RFProgress;
                deviceData_temp.Parameters.Sn1Char = deviceData.Parameters.Sn1Char;
                deviceData_temp.Parameters.Sn1 = deviceData.Parameters.Sn1;
                deviceData_temp.Parameters.Sn2 = deviceData.Parameters.Sn2;
                deviceData_temp.Parameters.Sn3 = deviceData.Parameters.Sn3;
                deviceData_temp.Parameters.StairsEnabled = deviceData.Parameters.StairsEnabled;
                deviceData_temp.Parameters.SysType = deviceData.Parameters.SysType;
                deviceData_temp.Parameters.SysTypeString = deviceData.Parameters.SysTypeString;
                deviceData_temp.Parameters.DSP = deviceData.Parameters.DSP;
                deviceData_temp.Parameters.Boot = deviceData.Parameters.Boot;
                deviceData_temp.Parameters.Motor = deviceData.Parameters.Motor;
                deviceData_temp.Parameters.FPGA = deviceData.Parameters.FPGA;
                deviceData_temp.Parameters.Device = deviceData.Parameters.Device;
                deviceData_temp.Parameters.RcSN = deviceData.Parameters.RcSN;
                deviceData_temp.Parameters.InfSN = deviceData.Parameters.InfSN;
                deviceData_temp.Parameters.LkSN = deviceData.Parameters.LkSN;
                deviceData_temp.Parameters.LhSN = deviceData.Parameters.LhSN;
                deviceData_temp.Parameters.RkSN = deviceData.Parameters.RkSN;
                deviceData_temp.Parameters.RhSN = deviceData.Parameters.RhSN;
                deviceData_temp.Parameters.EnableSystemStuckOnError_int = deviceData.Parameters.EnableSystemStuckOnError_int;
                deviceData_temp.Parameters.SwitchingTime = deviceData.Parameters.SwitchingTime;
                deviceData_temp.Parameters.StepCounter = deviceData.Parameters.StepCounter;
                deviceData_temp.Parameters.LogLevel.ADC = deviceData.Parameters.LogLevel.ADC;
                deviceData_temp.Parameters.LogLevel.CAN = deviceData.Parameters.LogLevel.CAN;
                deviceData_temp.Parameters.LogLevel.STAIRS = deviceData.Parameters.LogLevel.STAIRS;
                deviceData_temp.Parameters.LogLevel.USB = deviceData.Parameters.LogLevel.USB;
                deviceData_temp.Parameters.LogLevel.MCU = deviceData.Parameters.LogLevel.MCU;
                deviceData_temp.Parameters.LogLevel.RF = deviceData.Parameters.LogLevel.RF;
                deviceData_temp.Parameters.LogLevel.BIT = deviceData.Parameters.LogLevel.BIT;
                deviceData_temp.Parameters.LogLevel.RC = deviceData.Parameters.LogLevel.RC;
                deviceData_temp.Parameters.LogLevel.SIT_STN = deviceData.Parameters.LogLevel.SIT_STN;
                deviceData_temp.Parameters.LogLevel.WALK = deviceData.Parameters.LogLevel.WALK;
                deviceData_temp.Parameters.CurrentHandleAlgo = deviceData.Parameters.CurrentHandleAlgo;
                return true;
            }

        }

        byte[] MCUFileArray, INFDSPFileArray, INFBootFileArray;
        private void Delay_ms(double ms)
        {
            double _val = 0;
            Stopwatch stopwatch1 = new Stopwatch();
            stopwatch1.Start();

            while (stopwatch1.ElapsedMilliseconds < ms)
            {
                _val++;
            }
            stopwatch1.Reset();
            _val = 0;

        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        public static byte[] StrToByteArray(string str)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return encoding.GetBytes(str);
        }

        private MCU_ProgrammingWindow INFDownLoadinProcess = new MCU_ProgrammingWindow();
        private bool PIC_included_flag = false;
        private string downloadstatus = null;
        private string PIC_SW_VERSION = null;
        private string PIC_SW = null;
        
        private MCU_ProgrammingWindow MCUDownloadingProcessWindow = new MCU_ProgrammingWindow();
    
   
    
        private void OnSaveConfiguration(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "(*.xarg)|*.xarg|(*.*) |*.*";
            try
            {
                if (sfd.ShowDialog().Value == true)
                {
                    using (FileStream fs = new FileStream(sfd.FileName, FileMode.OpenOrCreate))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(DeviceParameters));
                        deviceData.Parameters.SampleTime = Short_GUIVersion;
                        serializer.Serialize(fs, deviceData.Parameters);
                        fs.Close();
               //         SetTitle(Path.GetFileNameWithoutExtension(sfd.FileName));

                    }
                }
            }
            catch (Exception exception)
            {
                //MessageBox.Show(this, "Save configuration failed: " + exception.Message);
                Logger.Error("Save configuration failed: ", exception.Message);
            }
        }

 

        private void OnUpdateSettings(object sender, RoutedEventArgs e)
        {
            try
            {

                deviceData.WriteParameters();
                RemoveTitle();
                // Thread.Sleep(1200);
                Delay_ms(1200); // replace Thread.Sleep
                MessageBox.Show((string)FindResource("Parameters were written successfully to the Rewalk"), (string)FindResource("Write"), MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            catch (DeviceOperationException exception)
            {
                // MessageBox.Show(this, "Device Operation failed: " + exception.Message);
                Logger.Error("Device Operation failed: ", exception.Message);
            }
        }
        private void OnReadDeviceSettings(object sender, RoutedEventArgs e)
        {

            ReadDeviceSettings();
            //Check_KneeFlexion_Selection();
            //Thread.Sleep(1200);
            //Check_StairsThreshold(); moved into ReadDeviceSettings 
        }
        private bool ReadDeviceSettings(bool throwException = true) // parasoft-suppress  CS.MLC "Methods executing atomic functions"
        {
            try
            {
               
                deviceData.ReadData();
                ReturnValueIndex();
                InitSystemStrings();

                if (deviceData.Parameters.UnitsSelection == 0)
                {
              //      Metric_Units.IsChecked = true;
                }
                if (deviceData.Parameters.UnitsSelection == 1)
                {
              //      US_Units.IsChecked = true;
                }
                RemoveTitle();
                if (GUIVersionType == "USA")
                {
                    StairsCounterValue.Visibility = Visibility.Hidden;
                    StepCounterValue.Visibility = Visibility.Visible;
                    //Enforce StairsEnable to be false in "INF"  -- Stairs is forbidden in USA
                    if (true == deviceData.Parameters.StairsEnabled)
                    {
                        deviceData.Parameters.StairsEnabled = false;
                        deviceData.WriteParameters();
                    }
                    else
                    {
                        deviceData.Parameters.StairsEnabled = false;
                    }
                }
                else
                {
                    StairsCounterValue.Visibility = Visibility.Visible;
                    StepCounterValue.Visibility = Visibility.Visible;
                }
				if (CurrentHandleAlogMethod == 0)
                { 
                    //0: Then old stop algo applied .
                    if (true == deviceData.Parameters.CurrentHandleAlgo)
                    {
                        deviceData.Parameters.CurrentHandleAlgo = false;
                        deviceData.WriteParameters();
                    }
                    else
                    {
                        deviceData.Parameters.CurrentHandleAlgo = false;
                    }
                
                }
                if (CurrentHandleAlogMethod == 1 )
                {
                    if (false == deviceData.Parameters.CurrentHandleAlgo)
                    {
                        deviceData.Parameters.CurrentHandleAlgo = true;
                        deviceData.WriteParameters();
                    }
                    else
                    {
                        deviceData.Parameters.CurrentHandleAlgo = true;
                    }
                }
              //  Write.IsEnabled = true;
                //Expander.IsEnabled = true; Expander.IsExpanded = true;

              //  Check_DSP_MOTOR_AppVer();

                if ((short)deviceData.Parameters.DSP != 0x0909) // dsp application version 
                {
                    Check_DSP_IfBeyondVer_030A();
                    Par_set_true_inRead();


                }

                else if ((short)deviceData.Parameters.DSP == 0x0909)//boot version ,0x0909 code of boot version
                {
                    Parm_set_false_inRead();
                    deviceData.Disconnect();
                    RunINFtoApplication = false;
                    /*
                    if ((App.Current as App).DeptName_RunINFtoApplication)
                    {
                        MessageBoxResult result = MessageBox.Show((string)this.FindResource("Rewalk is running in BOOT mode, limited operations are available!") + "\n" + (string)this.FindResource("Do you want to switch to operational mode?"), (string)this.FindResource("Warning_"), MessageBoxButton.YesNo, MessageBoxImage.Information);
                        if (MessageBoxResult.Yes == result)
                            deviceData.Disconnect();
                    }
                    else
                    {
                        deviceData.Disconnect();
                        RunINFtoApplication = false;

                    }*/
                }

                if ((short)deviceData.Parameters.Motor == 0x0909)//boot version
                {
             //       DSP_boot_ver_txtblock.Visibility = System.Windows.Visibility.Visible;
                    MessageBox.Show((string)this.FindResource("MCU is running in BOOT mode, Download new motors software or restart the device"),(string)this.FindResource( "Warning"), MessageBoxButton.OK, MessageBoxImage.Error);
                }
                Check_Safety_Trainning_Status();
               
                BatteryStatusTimer.IsEnabled = true;
                BatteryStatusTimer.Start();


                return true;
            }//end try



            catch (DeviceOperationException exception)
            {

                if (throwException)
                   Logger.Error("Device Operation failed: ", exception.Message);

                return false;
            }
        }
        private void Check_Safety_Trainning_Status()
        {
            /* if (safetyonoff.IsOn == false)
                 Y_Thr.IsEnabled = false;
             else if (safetyonoff.IsOn == true)
                 Y_Thr.IsEnabled = true;

             if (Trainning.IsOn == false)
             { hip_final.IsEnabled = false; hip_max.IsEnabled = false; t_steptime.IsEnabled = false; }
             else if (Trainning.IsOn == true)
             { hip_final.IsEnabled = true; hip_max.IsEnabled = true; t_steptime.IsEnabled = true; }
             */
        }
        private bool WarningMessageWasCalled;

        //new 2.9.2015
        private void Check_DSP_IfBeyondVer_030A()
        {

            if ((short)deviceData.Parameters.DSP > 0x030A)
            {
                short SysErrorStatus = deviceData.Parameters.SysErrorType;//deviceData.GetErrorStatus();
                UInt32 CheckWarningErrorStatus = deviceData.Parameters.EnableSystemStuckOnError_int;
                bool warningflag = (CheckWarningErrorStatus & (1 << SysErrorStatus)) != 0;
                WarningMessageWasCalled = warningflag;
                string ReportProcessWindow = (string)FindResource("System Error");
                if (SysErrorStatus > 0)//(-1 != SysErrorStatus)
                {
                    //  DisableScreen.Visibility = Visibility.Visible;

                    if (warningflag)
                    {
                        ReportProcessWindow = (string)FindResource("System Warning");//"System Error #";
                      //  progressform.btnClearError.Visibility = Visibility.Collapsed; //Hide button clear if it was warning
                    }
                    else
                    {
                        //   btnClearSysErr.IsEnabled = true;
                      //  btnFlashLogBrowse.IsEnabled = true;
                        //  tbSysErr.Text = SysErrorStatus.ToString();
                    }


                    if (!LogFileSent)
                    {

                        progressform = (Report_Progress)Application.LoadComponent(new Uri("Report_Progress.xaml", UriKind.Relative));
                        progressform.labelErrorReport.Content = ReportProcessWindow + " #" + SysErrorStatus;
                        //   progressform.AppendProgress(ReportProcessWindow + " occured\n");
                        //   progressform.AppendProgress("Please contact technical support before using the system again.\n");
                        progressform.AppendProgress((string)(FindResource("Please contact technical support.") + "\n"));
                        string output = string.Format("{0,0} ", (string)FindResource("Saving log files")); progressform.AppendProgress(output);

                        progressform.Visibility = System.Windows.Visibility.Visible;
                      //  ShowPYTH.IsEnabled = false;
                       // OpenUserfile.IsEnabled = false;
                       // Load.IsEnabled = false;
                      //  TopToolBar.IsEnabled = false;
                        progressform.Show();
                        progressform.Focus();
                        App tempAFT = App.Current as App;
                        if (tempAFT != null)
                        {
                      //      (tempAFT).DeptName_BrowseSaveAsFlashLogFile = false;
                        }
                      //  if (Technician.IsSelected == true)
                      //  {
                       //     if (WarningMessageWasCalled == false) //Error Message Not Warning
                       //         progressform.btnClearError.Visibility = Visibility.Visible;
                     //   }

                        //if(SysErrorStatus <= 30)
                        // SendEmailReport(ushort errtype, bool sendReport = false, bool reportProgress = true, ReportType reportTypeVal = ReportType.ERROR)
                        if (!warningflag)
                        {
                       //     btnFlashLogBrowse.IsEnabled = true;
                            SendEmailReport((ushort)SysErrorStatus);

                        }
                        else
                        {

                            SendEmailReport((ushort)SysErrorStatus, false, true, ReportType.WARNING); LogFileSent = true;
                        }

                    }
                }
                else
                {

                   // btnFlashLogBrowse.IsEnabled = false;
                    //btnClearSysErr.IsEnabled = false; tbSysErr.Text = "None";
                    //   progressform.btnClearError.Visibility = Visibility.Collapsed;


                }
            }
        }

        //private void Check_DSP_IfBeyondVer_030A()
        //{

        //    if ((short)deviceData.Parameters.DSP > 0x030A)
        //    {
        //        short SysErrorStatus = deviceData.Parameters.SysErrorType;//deviceData.GetErrorStatus();
        //        UInt32 CheckWarningErrorStatus = deviceData.Parameters.EnableSystemStuckOnError_int;
        //        bool warningflag = (CheckWarningErrorStatus & (1 << SysErrorStatus)) != 0;
        //        WarningMessageWasCalled = warningflag;
        //        string ReportProcessWindow = "System Error";// (string)FindResource("System Error");
        //        if (SysErrorStatus > 0)//(-1 != SysErrorStatus)
        //        {
        //            //  DisableScreen.Visibility = Visibility.Visible;

        //            if (warningflag)
        //            {
        //                ReportProcessWindow = "System Warning";//(string)FindResource("System Warning");//"System Error #";
        //             //   progressform.btnClearError.Visibility = Visibility.Collapsed; //Hide button clear if it was warning
        //            }
        //            else
        //            {
        //                //   btnClearSysErr.IsEnabled = true;
        //             //   btnFlashLogBrowse.IsEnabled = true;
        //                //  tbSysErr.Text = SysErrorStatus.ToString();
        //            }


        //            if (!LogFileSent)
        //            {

        //                progressform = (Report_Progress)Application.LoadComponent(new Uri("Report_Progress.xaml", UriKind.Relative));
        //                progressform.labelErrorReport.Content = ReportProcessWindow + " #" + SysErrorStatus;
        //                //   progressform.AppendProgress(ReportProcessWindow + " occured\n");
        //                //   progressform.AppendProgress("Please contact technical support before using the system again.\n");
        //               // progressform.AppendProgress((string)(FindResource("Please contact technical support.") + "\n"));
        //               // string output = string.Format("{0,0} ", (string)FindResource("Saving log files")); progressform.AppendProgress(output);
                      
        //                progressform.AppendProgress("Please contact technical support." + "\n");
        //                string output = string.Format("{0,0} ", "Saving log files"); progressform.AppendProgress(output);

        //                progressform.Visibility = System.Windows.Visibility.Visible;
                   
        //                progressform.Show();
        //                progressform.Focus();
        //                App tempAFT = App.Current as App;
                    

        //                //if(SysErrorStatus <= 30)
        //                // SendEmailReport(ushort errtype, bool sendReport = false, bool reportProgress = true, ReportType reportTypeVal = ReportType.ERROR)
        //                if (!warningflag)
        //                {
        //                  // btnFlashLogBrowse.IsEnabled = true;
        //                    SendEmailReport((ushort)SysErrorStatus);

        //                }
        //                else
        //                {

        //                    SendEmailReport((ushort)SysErrorStatus, false, true, ReportType.WARNING); LogFileSent = true;
        //                }

        //            }
        //        }
        //        else
        //        {

        //         //   btnFlashLogBrowse.IsEnabled = false;
        //            //btnClearSysErr.IsEnabled = false; tbSysErr.Text = "None";
        //            //   progressform.btnClearError.Visibility = Visibility.Collapsed;


        //        }
        //    }
        //}
        //private void Check_DSP_IfBeyondVer_030A()
        //{

        //    if ((short)deviceData.Parameters.DSP > 0x030A)
        //    {
        //        short SysErrorStatus = deviceData.Parameters.SysErrorType;//deviceData.GetErrorStatus();
        //                UInt32 CheckWarningErrorStatus = deviceData.Parameters.EnableSystemStuckOnError_int;
        //                bool warningflag = (CheckWarningErrorStatus & (1 << SysErrorStatus)) != 0;
        //        string ReportProcessWindow = "System Error";
        //                if (SysErrorStatus > 0 )//(-1 != SysErrorStatus)
        //            {


        //                    if (warningflag)
        //                {
        //                    ReportProcessWindow = "System Warning";//"System Error #";
        //                }
        //                    else
        //                    {
        //                     //   btnClearSysErr.IsEnabled = true;
        //                    //    btnFlashLogBrowse.IsEnabled = true;
        //                      //  tbSysErr.Text = SysErrorStatus.ToString();
        //                    }

        //                    if (!LogFileSent)
        //                    {

        //                progressform = (Report_Progress)Application.LoadComponent(new Uri("Report_Progress.xaml", UriKind.Relative));
        //                progressform.labelErrorReport.Content = ReportProcessWindow + " #" +SysErrorStatus;
        //                progressform.AppendProgress(ReportProcessWindow + " occured\n");
        //                progressform.AppendProgress("Please contact technical support before using the system again.\n");
        //                string output = string.Format("{0,0} ", "Saving log files......"); progressform.AppendProgress(output);

        //                progressform.Visibility = System.Windows.Visibility.Visible;
        //                progressform.Show();
        //                       //if(SysErrorStatus <= 30)
        //                        // SendEmailReport(ushort errtype, bool sendReport = false, bool reportProgress = true, ReportType reportTypeVal = ReportType.ERROR)
        //                        if (!warningflag)
        //                        {
        //                            SendEmailReport((ushort)SysErrorStatus);
                                  
        //                        }
        //                        else
        //                        {
        //                            SendEmailReport((ushort)SysErrorStatus, false, true, ReportType.WARNING); LogFileSent = true;
        //                        }

        //            }
        //        }
        //        else
        //        {

        //          //  btnFlashLogBrowse.IsEnabled = false;
        //            //btnClearSysErr.IsEnabled = false; tbSysErr.Text = "None";


        //        }
        //    }
        //}
  
        public void Par_set_true_inRead()
        {

            App temp = App.Current as App;
            if (temp != null)
            {

                temp.DeptName_Connected = true;
             
            }
           
            //New GUI6
           // SetFielsdsDisplayAsSysType();
        }
        UInt32 PrevStepCounter = 0;
        public void Parm_set_false_inRead()
        {
            App temp = App.Current as App;
            if (temp != null)
                //(App.Current as App).DeptName_Connected = false;
                temp.DeptName_Connected = false;
        }
        private void OnRestoreDeviceSettings(object sender, RoutedEventArgs e)
        {


            try
            {
                deviceData.RestoreParameters();
                if (GUIVersionType == "USA")
                {
                    deviceData.Parameters.StairsEnabled = false;
                }
                if (CurrentHandleAlogMethod == 0)
                {
                    deviceData.Parameters.CurrentHandleAlgo = false;
                }
                else
                {
                    deviceData.Parameters.CurrentHandleAlgo = true;
                }
            }
            catch (DeviceOperationException exception)
            {
                // MessageBox.Show(this, "Device Operation failed: " + exception.Message);
                Logger.Error("Device Operation failed: ", exception.Message);
            }
        }
   
        private GenerateRCAddressID GenerateRCAddressIDForm = new GenerateRCAddressID();
    
  
        private void OnDisconenctUSB()
        {
            tbConnected.Visibility = Visibility.Collapsed;
            tbDisConnected.Visibility = Visibility.Visible;


            BatteryFull.Visibility = Visibility.Collapsed;
            BatteryEmpty.Visibility = Visibility.Collapsed;
            BatteryHalf.Visibility = Visibility.Collapsed;
            BatteryThird.Visibility = Visibility.Collapsed;

            StairsCounterValue.Visibility = Visibility.Collapsed;
            StepCounterValue.Visibility = Visibility.Collapsed;

     
          
            USBConnectDelay = 0;
            USBDisConnectDelay = 0;
            
            LogFileSent = false;
            USBConnectState = false;
          
            // Expander.IsEnabled = false; Expander.IsExpanded = false;
            USBConnectDelay = 0;
            USBDisConnectDelay = 0;
            App temp = App.Current as App;
            if (temp != null)
                //(App.Current as App).DeptName_Connected = false;
                temp.DeptName_Connected = false;



        }
        void OnCheckUSBConnectedTimeTimer_Tick(object sender, EventArgs e) // parasoft-suppress  OOM.CYCLO "Coding style - method logic was verified"
        {

          //  Detect_killRewalkRunningApllication();
        
            try
            {
                USBConnected = deviceData.CheckUSBConnected();
                if (!USBConnected)
                {
                    USBDisConnectDelay++;
                }
            }
            catch (DeviceOperationException exception)
            {
                USBConnected = false;
                Logger.Dummy_Error("", exception.Message);
            }
            if (true == USBConnectState)
            {
                if (USBDisConnectDelay >= 2)
                    OnDisconenctUSB();
            }
            else
            {
                if (USBConnected)
                {
                    USBConnectDelay++;
                    if (USBConnectDelay >= 5)
                    {
                        bool EnableReadOnOpen = false;
                        XmlDocument doc = new XmlDocument();
                        doc.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");
                        try
                        {
                            XmlNodeList ReadOnOpenNode = doc.SelectNodes("//ReadOnOpen");
                            if (ReadOnOpenNode.Count == 1)
                                foreach (XmlNode value in ReadOnOpenNode)
                                { EnableReadOnOpen = value.Attributes["Values"].Value == "true" || value.Attributes["Values"].Value == "True"; }
                        }
                        catch (Exception exception) { Logger.Dummy_Error("", exception.Message); }
                        if (EnableReadOnOpen)
                            ReadDeviceSettings(false);
                        LogGUIConnections();
                        tbConnected.Visibility = Visibility.Visible;
                        tbDisConnected.Visibility = Visibility.Collapsed;
						USBDisConnectDelay = 0; USBConnectDelay = 0;
                        ControlsEnabled();




                    }
                }
            }
        }
        private void ControlsEnabled()
        {
          //  Read.IsEnabled = true;
          //  Load.IsEnabled = true;
          //  Restore.IsEnabled = true;
          //  Save.IsEnabled = true;
            //   Load_open.IsEnabled = true;
            //  CreateBtn.IsEnabled = true;
            //  NewBtn.IsEnabled = true;
         //   btnStairsCalc.IsEnabled = true;
          //  btnClearSysErr.IsEnabled = true;
            //  TechnicianRadioBtn.IsEnabled = true;
            //  ProductionRadioBtn.IsEnabled = true;
          //  collectOnline.IsEnabled = true;
            //  gbCustomCommands.IsEnabled = true;
           // btnCom1.IsEnabled = true;
            //btnCom2.IsEnabled = true;
           // btnCom3.IsEnabled = true;
          //  btnCom4.IsEnabled = true;
           // btnCom5.IsEnabled = true;
            //btnCom6.IsEnabled = true;
            // btnCom7.IsEnabled = true;
          //  btnDSPBrowse.IsEnabled = true;
         //   btnDSPDownload.IsEnabled = true;
         //   btnMCUBrowse.IsEnabled = true;
         //   btnMCUDownload.IsEnabled = true;
            USBConnectState = true;
         //   BtnRunEncoderTest.IsEnabled = true;

        }
 
        void OnCollectDataTimer(object sender, EventArgs e)
        {
            try
            {
                deviceData.ReadOnlineData();
                // tbOnlineError.Visibility = Visibility.Hidden;
            }
            catch (DeviceOperationException exception)
            {
                // tbOnlineError.Text = "Read online data failed: ";// +exception.Message;
                // tbOnlineError.Visibility = Visibility.Visible;
                Logger.Dummy_Error("", exception.Message);
            }
        }
        private void OnRunCustomCommand(FrameworkElement sender, byte[] data)
        {
            // int command = int.Parse(sender.Tag.ToString());
            int command;
            bool cc = int.TryParse(sender.Tag.ToString(), out  command);
            if (command == 5 && data[0] == 0x99 && data[1] == 0x99 && data[2] == 0x99)
            {
                //Report_Progress progressform = new Report_Progress();
                object s = Application.LoadComponent(new Uri("Report_Progress.xaml", UriKind.Relative));

                progressform = (Report_Progress)s; progressform.Visibility = System.Windows.Visibility.Visible; progressform.Show();
                progressform.rtfUserReport.AppendText("System error occured\n"); progressform.rtfUserReport.AppendText("Saving log files......");

                SendEmailReport(00);
            }

            try
            {
                if (command == 5 && data[0] == (byte)0x39) //BR window 
                {
                    data[0] = 0x30;
                    deviceData.RunCustomCommand(1, data);
                    byte[] result_LH = deviceData.RunCustomCommand(1, data); 

                    if(result_LH[0]== 0x30)
                      btnCom1_LH.SetResult(result_LH);


                    data[0] = 0x31;
                    deviceData.RunCustomCommand(2, data); 
                    byte[] result_LK = deviceData.RunCustomCommand(2, data); 
                    if (result_LK[0] == 0x31)
                        btnCom2_LK.SetResult(result_LK);


                    data[0] = 0x32;
                    deviceData.RunCustomCommand(3, data); 
                    byte[] result_RH = deviceData.RunCustomCommand(3, data); 
                    if(result_RH[0]== 0x32)
                        btnCom3_RH.SetResult(result_RH);


                    data[0] = 0x33;
                    deviceData.RunCustomCommand(4, data);
                    byte[] result_RK = deviceData.RunCustomCommand(4, data); 
                    if(result_RK[0]== 0x33)
                        btnCom4_RK.SetResult(result_RK);


                }
                else
                {
                    deviceData.RunCustomCommand(command, data);
                    byte[] result = deviceData.RunCustomCommand(command, data);
                    // (sender as CommandCtrl).SetResult(result);
                    CommandCtrl aa = sender as CommandCtrl;
                    if (aa != null) aa.SetResult(result);

                }
            }
            catch (DeviceOperationException exception)
            {
                Logger.Error("Custom Command Operation failed: ", exception.Message);
                //   deviceData.Disconnect();
                //      OnDisconenctUSB();
                //    CheckUSBConnectedTime.IsEnabled = false;
                //    Restore.IsEnabled = true;
            }

        }
        private bool RunINFtoApplication = false;
        private void OnParameterlessCustomCommandRun(object sender, RoutedEventArgs e)
        {

            //  int command = int.Parse((sender as FrameworkElement).Tag.ToString());


            try
            {
                int command;
                bool cc;

                FrameworkElement aa = sender as FrameworkElement;
                if (aa != null)
                {
                    cc = int.TryParse(aa.Tag.ToString(), out command);
                    deviceData.RunCustomCommand(command, null);
                    if (command == 6)//command 6 , INF exit_usb=0 and  command 7 setWDG to do reset
                    {
                        deviceData.Disconnect();
                        OnDisconenctUSB();
                        RunINFtoApplication = true;

                    }
                    else if (command == 7)
                    {
                        // RunINFtoApplication = true;
                        /*  USBConnected = false;
                          USBConnectState = false;
                          deviceData.Disconnect();
                          OnDisconenctUSB();*/


                    }


                }


            }
            catch (DeviceOperationException exception)
            {
                // MessageBox.Show(this, "Custom Command Operation failed: " + exception.Message);
                Logger.Error("Custom Command Operation failed: ", exception.Message);
            }
        }
        public void RunSystem()
        {
            deviceData.Disconnect();
            OnDisconenctUSB();
            RunINFtoApplication = true;
        }
        private int Joint_Being_Downloaded = 0x330;
        private int infdownloadcounter = 0;
        private int PICSWVERSION = 0;

        void UpdateProgressBarTimer_Tick(object sender, EventArgs e)
        {
            if (INFDownLoadinProcess.prgWorking.Value == 30000)
            {
                INFDownLoadinProcess.prgWorking.Value = 0;
            }
            INFDownLoadinProcess.prgWorking.Value += 350;
  
        }
         //Battery Status Indication 

        void BatteryStatusTimer_Tick(object sender, EventArgs e)
        {
            short batterylevel = deviceData.Parameters.INFBatteriesLevel;

            if (USBConnected)
            {
                switch (batterylevel)
                {
                    case 0x07: //Full Battery Green 

                        BatteryHalf.Visibility = Visibility.Collapsed;
                        BatteryThird.Visibility = Visibility.Collapsed;
                        BatteryEmpty.Visibility = Visibility.Collapsed;
                        BatteryFull.Visibility = Visibility.Visible;

                        break;
                    case 0x08: //Very Low battery blinking red 

                        BatteryHalf.Visibility = Visibility.Collapsed;
                        BatteryThird.Visibility = Visibility.Collapsed;
                        BatteryFull.Visibility = Visibility.Collapsed;
                        if (BatteryEmpty.Visibility == Visibility.Visible)
                        {

                            BatteryEmpty.Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            BatteryEmpty.Visibility = Visibility.Visible;
                        }

                        break;
                    case 0x9: //Low battery
                        BatteryFull.Visibility = Visibility.Collapsed;
                        BatteryEmpty.Visibility = Visibility.Visible;
                        BatteryThird.Visibility = Visibility.Collapsed;
                        BatteryHalf.Visibility = Visibility.Collapsed;

                        break;
                    case 0x0A://Med battery
                        BatteryFull.Visibility = Visibility.Collapsed;
                        BatteryEmpty.Visibility = Visibility.Collapsed;
                        BatteryThird.Visibility = Visibility.Collapsed;
                        BatteryHalf.Visibility = Visibility.Visible;

                        break;
                }
            }
        }

        //Encoder Test Check Status ; 
        bool EncoderTestInProcess;
        int JointCounter;
        bool LHJointUnderTest;
        bool RHJointUnderTest;
        bool LKJointUnderTest;
        bool RKJointUnderTest;
   
        DateTime CurrentTime;
     
     
        private string GetSysPass()
        {
            string st;
            st = "argoadmin";
            return st;
        }
        private string GetSysConfigPass()
        {
            string st;
            st = "kingradi";
            return st;
        }
    
    
     
        private Parser LogParser = new Parser();
        private XmlDataProvider deviceSettingsValuesDataProvider;
        public Data deviceData = new Data();
        private DispatcherTimer onlineTimer = new DispatcherTimer();
        private DispatcherTimer testTimer = new DispatcherTimer();
        private DispatcherTimer calibTimer = new DispatcherTimer();
        private DispatcherTimer TestEncoderTimer = new DispatcherTimer();
        private DispatcherTimer BatteryStatusTimer = new DispatcherTimer();
        private bool USBConnected = false;
        private bool USBConnectState = false;
        private int USBConnectDelay = 0;
        private int USBDisConnectDelay = 0;
        bool RFTestStarted = false;
        private DispatcherTimer RFtestTimer = new DispatcherTimer();
        private DispatcherTimer RCUpdateTimer = new DispatcherTimer();
        private DispatcherTimer CheckUSBConnectedTime = new DispatcherTimer();
        private DispatcherTimer UpdateProgressBarTimer = new DispatcherTimer();
        string[] array1 = new string[31];
        string[] array2 = new string[31];
        int[] array2_int = new int[31];
        private const int KNEE_MAX_SPEED = 80; // in degrees/sec
        private const int MAX_STAIRS_THRESHOLD = 19;

       
      
    
    
        private DateTime LastLogTime, FirstLogTime;
        private TimeSpan DeltaTime;
        private void LogGUIConnections() // parasoft-suppress  CS.MLC "Methods executing atomic functions" OOM.CYCLO "Coding style - method logic was verified"
        {
            CurrentTime = DateTime.Now; LastLogTime = CurrentTime; FirstLogTime = DateTime.MinValue; DeltaTime = TimeSpan.Zero;

            string path = SysLogPath; int LogSendDeltaTimeInDays = 100000; bool SendLog = false, cc = false; string FileName = "\\system_report_" + deviceData.Parameters.SN + ".rwk";
            try
            {
                using (StreamReader f = new StreamReader(File.Open(path + FileName, FileMode.Open)))
                {
                    string[] StringArray; string line = "-1"; bool LogSent = false, FoundLogSent = false;

                    do
                    {
                        if (line != "-1")
                        {
                            line = line.TrimStart('<'); line = line.TrimEnd('>'); StringArray = line.Split('|');
                            LogSent = System.Convert.ToBoolean(StringArray[2]);
                            if (!FoundLogSent) FoundLogSent = LogSent;
                            // if (LogSent) LastLogTime = DateTime.Parse(StringArray[0]);
                            if (LogSent)
                                cc = DateTime.TryParse(StringArray[0], out LastLogTime);
                            //if (FirstLogTime == DateTime.MinValue) FirstLogTime = DateTime.Parse(StringArray[0]);
                            if (FirstLogTime == DateTime.MinValue)
                                cc = DateTime.TryParse(StringArray[0], out FirstLogTime);

                        }
                        line = f.ReadLine();
                    } while (line != null);

                    if (!FoundLogSent) LastLogTime = FirstLogTime;

                    DeltaTime = CurrentTime.Subtract(LastLogTime);

                    f.Close();
                }

            }
            catch (Exception exception) { Logger.Dummy_Error("", exception.Message); }

            try
            {
                string LogSendDeltaTimeInDaysString = null;
                XmlDocument doc = new XmlDocument(); doc.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");
                XmlNodeList LogSendDeltaTimeNode = doc.SelectNodes("//ConnectionLogDeltaTime");
                if (LogSendDeltaTimeNode.Count == 1)
                    foreach (XmlNode value in LogSendDeltaTimeNode)
                    { LogSendDeltaTimeInDaysString = value.Attributes["Values"].Value; }
                LogSendDeltaTimeInDays = System.Convert.ToInt32(LogSendDeltaTimeInDaysString);
            }
            catch (Exception exception) { LogSendDeltaTimeInDays = 100000; Logger.Dummy_Error("", exception.Message); }



            if (DeltaTime.TotalDays >= LogSendDeltaTimeInDays) SendLog = true;

            try
            {
                using (StreamWriter f = new StreamWriter(File.Open(path + FileName, FileMode.Append)))

                { f.WriteLine("<{0,0} | {1,1} | {2,2}>", CurrentTime, deviceData.Parameters.StepCounter, SendLog); f.Close(); }

            }
            catch (Exception exception) { Logger.Dummy_Error("", exception.Message); }

            if (SendLog)
                SendEmailReport(0, true, false, ReportType.NORMAL);
        }
      
        private string GUIVersionType = null;
        private short CurrentHandleAlogMethod ;

        private string GetSecureString()
        {
            string st = String.Empty;
            /* Code for getting the password */
            st = "reports2013";
            return st;
        }
        private string GetGmailPassword()
        {
            string st = String.Empty;
            /* Code for getting the password */
            st = "argoadmin";
            return st;
        }
        private Attachment LogFileAttachement, FlashLogFileAttachement, PCLogFileAttachement;
        private MailMessage message;// = new MailMessage;
        public void AutoSaveLogfile(string filename)
        {

            string path = CheckDirectory();
            string datfilename = path + "\\" + deviceData.Parameters.SN + filename + ".dat";
            string txtfilename = path + "\\" + deviceData.Parameters.SN + filename + ".txt";


            deviceData.SaveLogFile(datfilename);
            LogParser.ParseFile(datfilename, false, txtfilename);
            //Process.Start("notepad", txtfilename);

        }
        private string CheckDirectory()
        {
            deviceData.ReadData();
            string temp = AppDomain.CurrentDomain.BaseDirectory; string[] temp1;
            DirectoryInfo dir_info;
            temp1 = temp.Split('\\'); string AFTFolderPath = temp1[0] + "\\Argo\\AFT"; string SystemSerialNumber = temp1[0] + "\\Argo\\AFT\\" + deviceData.Parameters.SN;
            try
            {
                if (!Directory.Exists(AFTFolderPath))
                    dir_info = Directory.CreateDirectory(AFTFolderPath);
                if (!Directory.Exists(SystemSerialNumber))
                    dir_info = Directory.CreateDirectory(SystemSerialNumber);
                return SystemSerialNumber;
            }
            catch (Exception exception)
            {
                // MessageBox.Show(exception.Message);
                Logger.Error(null, exception.Message);
                return null;
            }

        }
        public void AFTReport(string testername, string result, string status, string testname, string current, bool backlashflag, string backlashvalue) // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions" CS.MLC "Methods executing atomic functions" SEC.AUSD "Method inspected and no security violation found - only used for log record"
        {
            string path = CheckDirectory();
            DateTime CurrentTimeDate;
            CurrentTimeDate = DateTime.Now;
            string FileLocation = path + "\\" + deviceData.Parameters.SN + " Calbiration" + ".dat";
            string SystemParameterHeader = ReadSysParamsFromLog(FileLocation);

            string[] lines = SystemParameterHeader.Split('\n');
            int NumberOfLines = lines.Length;
            string filenamepath = path + "\\" + deviceData.Parameters.SN + "_AFT_Report" + ".txt";





            if (!File.Exists(filenamepath))
            {
                using (StreamWriter AFTFileReport = File.CreateText(filenamepath))
                {
                    AFTFileReport.WriteLine("Rewalk AFT Report Generated at :".PadRight(30) + CurrentTimeDate + "\n");
                    AFTFileReport.WriteLine("*********************************************************\n");
                    AFTFileReport.WriteLine("ReWalk Parameters :\n");
                    AFTFileReport.WriteLine(Environment.NewLine + " Tester Name:".PadRight(32) + testername + "\n");
                    for (int i = 3; i <= NumberOfLines - 22; i++)
                    {
                        AFTFileReport.WriteLine(lines[i]);
                    }

                    //AFTFileReport.WriteLine(Environment.NewLine + "____________ Test Name:- " + TestName + " _____________\n");
                    //AFTFileReport.WriteLine(Environment.NewLine + " Calibration Current:".PadRight(32) + current + "\n");
                    //AFTFileReport.WriteLine(Environment.NewLine + " Start Run 1 Test Time:".PadRight(32) + CurrentTimeDate + "\n");   

                }

            }

            else
            {
                using (StreamWriter AFTFileReport = File.AppendText(filenamepath))
                {
                    if (status == "Calibration")
                    {
                        AFTFileReport.WriteLine(Environment.NewLine + "__Test Name:- " + testname + " _____________\n");
                        AFTFileReport.WriteLine(Environment.NewLine + " Calibration Current:".PadRight(32) + current + "    Result:-  " + result + "\n");

                    }
                    if (status == "Start")
                    {

                        AFTFileReport.WriteLine(Environment.NewLine + " Start Test Time:".PadRight(32) + CurrentTimeDate + "  Test Name:- " + testname + "\n");
                        //   AFTFileReport.WriteLine(Environment.NewLine + " Run 1 Current:".PadRight(32) + current + "\n");
                        //   AFTFileReport.WriteLine(Environment.NewLine + " End Run 1 Test:".PadRight(32)  + CurrentTimeDate + "\n");
                        //   AFTFileReport.WriteLine(Environment.NewLine + " Start Run 2 Test Time:".PadRight(32) + CurrentTimeDate + "\n");

                    }
                    if (status == "End")
                    {
                        //    AFTFileReport.WriteLine(Environment.NewLine + " Start Current:".PadRight(32) + current + "\n");
                        if (backlashflag)
                            AFTFileReport.WriteLine(Environment.NewLine + " Backlash Result:".PadRight(32) + backlashvalue + "\n");
                        else
                            AFTFileReport.WriteLine(Environment.NewLine + " End Test:".PadRight(32) + CurrentTimeDate + " Test Name:- " + testname + "    Result:-  " + result + "\n".PadRight(32) + current + "\n");
                    }
                    /*    if (status == "Start_RUN_2")
                        {

                            AFTFileReport.WriteLine(Environment.NewLine + " Start Run 2 Test Time:".PadRight(32) + CurrentTimeDate + "\n");   
               
                        }

        */
                    if (status == "End_")
                    {
                        AFTFileReport.WriteLine(Environment.NewLine + " Current Test Result:".PadRight(32) + current + "\n");
                        AFTFileReport.WriteLine(Environment.NewLine + " End Run  Test:".PadRight(32) + CurrentTimeDate + "\n");
                        if (backlashflag)
                            AFTFileReport.WriteLine(Environment.NewLine + " Backlash Test Result:".PadRight(32) + "\n".PadRight(32) + backlashvalue + "\n");

                        // AFTFileReport.WriteLine(Environment.NewLine + " Test Result:".PadRight(32) + result + "\n");
                        //   AFTFileReport.WriteLine(Environment.NewLine + "Test Result:".PadRight(32) +"\n".PadRight(32)+ result + "\n");
                        //  AFTFileReport.WriteLine(Environment.NewLine + "______________________________________________________");

                    }
                    if (status == "End_All")
                    {

                        AFTFileReport.WriteLine(Environment.NewLine + " Test Result:".PadRight(32) + result + "\n");
                        AFTFileReport.WriteLine(Environment.NewLine + "______________________________________________________");

                    }

                }
            }

        }
        private string ReadSysParamsFromLog(string erroreilelocation)
        {
            try
            {
                using (BinaryReader b = new BinaryReader(File.Open(erroreilelocation, FileMode.Open)))
                {

                    int length = (int)b.BaseStream.Length;
                    char[] LUTData = new char[length];
                    int ret_int = b.Read(LUTData, 0, length);
                    string LUTStringData = new string(LUTData);
                    string SysParamsInfo = null;

                    bool SysParamsIncluded = LUTStringData.Contains("SYS_PARAMS");
                    if (SysParamsIncluded)
                    {
                        int index = LUTStringData.IndexOf("SYS_PARAMS");
                        // DateTime CurrentTimeLog = DateTime.Now;
                        string[] dataarray = new string[5000];
                        string[] ParamsArray = new string[1000];
                        string line2write;
                        dataarray = LUTStringData.Split('[');

                        LUTStringData = LUTStringData.Substring(index);//dataarray[4].Split(']');
                        ParamsArray = LUTStringData.Split('[');
                        ParamsArray = ParamsArray[0].Split(']');
                        ParamsArray = ParamsArray[1].Split('>');

                        SysParamsInfo = "\nRewalk parameters downloaded at " + CurrentTime + "\n";
                        SysParamsInfo += "***********************************************************\n";
                        SysParamsInfo += "\n";


                        foreach (string line in ParamsArray)
                        {
                            line2write = line.Replace('<', ' ');
                            SysParamsInfo += line2write + "\n";
                        }

                    }
                    b.Close();
                    return SysParamsInfo;
                }
            }
            catch (Exception exception)
            {
                Logger.Dummy_Error("", exception.Message);
                return null;
            }
        }
        private void SendEmailReport(ushort errtype, bool sendReport = false, bool reportProgress = true, ReportType reportTypeVal = ReportType.ERROR) // parasoft-suppress  CS.MLC "Methods executing atomic functions" OOM.CYCLO "Coding style - method logic was verified" METRICS.MCCC "Coding style - method logic was verified" METRICS.MLOC "Methods executing atomic functions"
        {

            CurrentTime = DateTime.Now;
            string SystemLogLocation;
            var fromAddress = new MailAddress("Rewalk.reports@argomedtec.com", "Rewalk Error Reports " + deviceData.Parameters.SN);
            var toAddress = new MailAddress("Rewalk.reports@argomedtec.com");

            // string fromPass = GetSecureString();// "reports2013";

            string subject;
            string body;


            if (reportTypeVal == ReportType.ERROR || reportTypeVal == ReportType.WARNING)
            {

            if (reportTypeVal == ReportType.ERROR)
            {
                subject = "Do Not Reply - Error report for system " + deviceData.Parameters.SN + " error #" + errtype;
                body = "This is an automatic report generated at " + CurrentTime + " for system " + deviceData.Parameters.SN + "\nerror #" + errtype + " occured, log and log flash file are attached.";
            }
            else
            {
                    subject = "Do Not Reply - Warning report for system " + deviceData.Parameters.SN + " warning #" + errtype;
                    body = "This is an automatic report generated at " + CurrentTime + " for system " + deviceData.Parameters.SN + "\nwarning #" + errtype + " occured, log file is attached";
                }
            }
            else
            {
                subject = "System Log Report System #" + deviceData.Parameters.SN;
                body = "This is an automatic report generated at " + CurrentTime + " for system " + deviceData.Parameters.SN + "log file is attached.";

            }

            string xmldata;
            string[] emails = null;
            string path = LogPath;//AppDomain.CurrentDomain.BaseDirectory;// Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData); //Directory.GetCurrentDirectory();

            bool DisableEmail = false;
            string output;
            string ErrorFileLocation;
            string ErrorFlashFileLocation = null;

            SystemLogLocation = path + "\\sys_logs\\system_report_" + deviceData.Parameters.SN + ".rwk";

            //MessageBoxResult result = MessageBox.Show("Saving log files...", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            if (reportTypeVal == ReportType.ERROR || reportTypeVal == ReportType.WARNING)
            {
            if (reportTypeVal == ReportType.ERROR)
            {
                ErrorFileLocation = path + "\\log_error_report_system_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
                ErrorFlashFileLocation = path + "\\flash_log_error_report_system_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
            }
            else
            {
                    ErrorFileLocation = path + "\\log_warning_report_system_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
                }
            }
            else
            {
                ErrorFileLocation = path + "\\log_file_report_system_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
                if (deviceData.Parameters.SysErrorType != -1 && reportTypeVal != ReportType.WARNING)
                {
                    ErrorFlashFileLocation = path + "\\flash_log_error_report_system_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
                }
                else
                    ErrorFlashFileLocation = null;
            }
            try
            {
                //Just to make sure that file might be created
                using (FileStream fs = new FileStream(ErrorFileLocation, FileMode.Create))
                {
                    fs.Close();
                }

                deviceData.SaveLogFile(ErrorFileLocation);
            }
            catch (Exception exception)
            {
                if (reportProgress)
                {
                    output = string.Format("{0,15} ", "Failed\n");
                    progressform.rtfUserReport.AppendText(output);
                    progressform.Close_Report();
                }
                Logger.Dummy_Error("", exception.Message);
                return;
            }

            try
            {
                if (reportTypeVal == ReportType.ERROR ||
                    deviceData.Parameters.SysErrorType != -1 && reportTypeVal != ReportType.WARNING)
                {
                    //Just to make sure that file might be created
                    using (FileStream fs = new FileStream(ErrorFlashFileLocation, FileMode.Create))
                    {
                        fs.Close();
                    }

                    deviceData.SaveFlashLogFile(ErrorFlashFileLocation);
                    deviceData.SaveLogFile(ErrorFileLocation);
                }
            }
            catch (Exception exception)
            {
                if (reportProgress)
                {
                    output = string.Format("{0,15} ", "Failed\n");
                    progressform.rtfUserReport.AppendText(output);
                    progressform.Close_Report();
                }
                Logger.Dummy_Error("", exception.Message);
                return;
            }
            if (reportProgress)
            {
               // output = string.Format("{0,15} ", "Done\n");
                output = string.Format("{0,15} ", (string)FindResource("Done_") + "\n");
                progressform.rtfUserReport.AppendText(output);
            }


            string EmailInfo = ReadSysParamsFromLog(ErrorFileLocation);
            body += EmailInfo;
            XmlDocument doc = new XmlDocument();
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");

            try
            {
                XmlNodeList DisableEmailNode = doc.SelectNodes("//DisableEmail");
                if (DisableEmailNode.Count == 1)
                    foreach (XmlNode value in DisableEmailNode)
                    {
                        DisableEmail = value.Attributes["Values"].Value == "true" || value.Attributes["Values"].Value == "True";


                    }
            }
            catch (Exception exception)
            {
                Logger.Dummy_Error("", exception.Message);
            }


            if (DisableEmail && false == sendReport)
            {
                //MessageBox.Show("Error Logs saved.", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                progressform.Close_Report();
                return;
            }
            if (reportProgress)
            {

                output = string.Format("{0,0} ", "Sending logs files......");
                progressform.rtfUserReport.AppendText(output);
                bool bb = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
                if (!bb)
                {
                    output = string.Format("{0,15} ", "No internet connection available\n");
                    progressform.rtfUserReport.AppendText(output);
                    progressform.Close_Report();
                    return;
                }
            }
            try
            {
                XmlNodeList EmailNode = doc.SelectNodes("//Email");
                foreach (XmlNode email in EmailNode)
                {
                    xmldata = email.Attributes["Values"].Value;
                    emails = xmldata.Split(',');

                }
            }
            catch (Exception exception)
            {
                Logger.Dummy_Error("", exception.Message);
            }



            /*var smtp = new SmtpClient
            {
                Host = "mail.argomedtec.com",
                Port = 25,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = true,//false,
                Credentials = new NetworkCredential(fromAddress.User, GetSecureString(), "argoext")// (fromAddress.Address, fromPassword)
       
            };*/
            string GmailUser = "rewalk.reports123";

            var gmail = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(GmailUser, GetGmailPassword())// (fromAddress.Address, fromPassword)

            };

            message = new MailMessage(fromAddress, toAddress);

            message.Subject = subject;
            message.Body = body;

            foreach (string email in emails)
            {
                // if (email != "" && email != null)
                if (!String.IsNullOrEmpty(email))
                {

                    try
                    {
                        message.CC.Add(email);
                    }
                    catch (Exception exception)
                    {
                        Logger.Dummy_Error("", exception.Message);
                    }

                }
            }

            try
            {

                if (File.Exists(SystemLogLocation))
                {
                    PCLogFileAttachement = new Attachment(SystemLogLocation);
                    message.Attachments.Add(PCLogFileAttachement);
                }
                if (ErrorFileLocation != null)
                {
                    LogFileAttachement = new Attachment(ErrorFileLocation);
                    message.Attachments.Add(new Attachment(ErrorFileLocation));
                }
                if (ErrorFlashFileLocation != null)
                {
                    FlashLogFileAttachement = new Attachment(ErrorFlashFileLocation);
                    message.Attachments.Add(new Attachment(ErrorFlashFileLocation));
                }

            }
            catch (Exception exception)
            {
                output = string.Format("{0,15} ", "Error PC Log file writing!\n");
                progressform.rtfUserReport.AppendText(output);
                progressform.Close_Report();
                Logger.Dummy_Error("", exception.Message);
                return;
            }
            try
            {

                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };
                if (reportProgress)
                    //smtp.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(Client_SendCompleted);
                    gmail.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(Client_SendCompleted);
                //smtp.SendAsync(message, null);  

                gmail.SendAsync(message, null);


            }
            catch (Exception exception)
            {
                if (reportProgress)
                {
                    output = string.Format("{0,15} ", "Check internet connection!\n");
                    progressform.rtfUserReport.AppendText(output);
                    progressform.Close_Report();
                    return;
                }
                Logger.Dummy_Error("", exception.Message);
            }


        }
        void Client_SendCompleted(object sender, AsyncCompletedEventArgs e) // parasoft-suppress  CS.MLC "Methods executing atomic functions"
        {
            string output;


            if (e.Error != null)
            {
                var fromAddress = new MailAddress("Rewalk.reports@argomedtec.com", "Rewalk Error Reports " + deviceData.Parameters.SN);
                var toAddress = new MailAddress("Rewalk.reports@argomedtec.com");

                var smtp = new SmtpClient
                {
                    Host = "mail.argomedtec.com",
                    Port = 25,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = true,//false,
                    Credentials = new NetworkCredential(fromAddress.User, GetSecureString(), "argoext")// (fromAddress.Address, fromPassword)

                };

                /*string GmailUser = "rewalk.reports";
                
                var gmail = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(GmailUser, GetGmailPassword())// (fromAddress.Address, fromPassword)

                };*/

                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };

                //gmail.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(GmailClient_SendCompleted);

                //gmail.SendAsync(message, null);
                smtp.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(SecondaryClient_SendCompleted);

                smtp.SendAsync(message, null);
            }
            else
            {
               // output = string.Format("{0,15} ", "Done\n");
                output = string.Format("{0,15} ", (string)FindResource("Done_") + "\n");
                progressform.rtfUserReport.AppendText(output);
                try
                {
                    message.Dispose();
                    PCLogFileAttachement.Dispose();
                    LogFileAttachement.Dispose();
                    FlashLogFileAttachement.Dispose();
                }
                catch (Exception exception)
                {
                    Logger.Dummy_Error("", exception.Message);
                }
                progressform.Close_Report();
            }


        }
        void SecondaryClient_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            string output;
            try
            {
                message.Dispose();
                PCLogFileAttachement.Dispose();
                LogFileAttachement.Dispose();
                FlashLogFileAttachement.Dispose();
            }
            catch (Exception exception)
            {
                Logger.Dummy_Error("", exception.Message);
            }

            if (e.Error != null)
            {

                output = string.Format("{0,15} ", "Failed\n");
                progressform.rtfUserReport.AppendText(output);
            }
            else
            {
             //   output = string.Format("{0,15} ", "Done\n");
                output = string.Format("{0,15} ", (string)FindResource("Done_") + "\n");
                progressform.rtfUserReport.AppendText(output);


            }
            progressform.Close_Report();

        }
        private void OnClearSystemError(object sender, RoutedEventArgs e)
        {
            deviceData.ClearSystemError();
            deviceData.Parameters.SysErrorType = deviceData.GetErrorStatus();
            // tbSysErr.Text = deviceData.Parameters.SysErrorType == -1 ? "None" : deviceData.Parameters.SysErrorType.ToString();
            MessageBox.Show((string)this.FindResource("System error cleared successfully !"), "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            //  btnClearSysErr.IsEnabled = false;
          //  btnFlashLogBrowse.IsEnabled = false;


        }
        private Report_Progress progressform = new Report_Progress();
        private void OnResetStepCounter(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show((string)this.FindResource("Are you sure you want to reset steps counter?"), (string)this.FindResource("Steps Counter Reset"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                UInt32 StepsCounterReset = deviceData.ResetStepsCounter();
                if (StepsCounterReset == 0)
                {
                    MessageBox.Show((string)this.FindResource("Reset was done successfully"), (string)this.FindResource("Confirmation"), MessageBoxButton.OK, MessageBoxImage.Warning);

                }
            }


        }
        private void OnSendLogFile(object sender, RoutedEventArgs e)
        {
            object s;
            s = Application.LoadComponent(new Uri("Report_Progress.xaml", UriKind.Relative));

            progressform = (Report_Progress)s;
            progressform.labelErrorReport.Content = "Sending system report";

            string output = string.Format("{0,0} ", "Saving log files......");
            progressform.AppendProgress(output);
            progressform.Visibility = System.Windows.Visibility.Visible;

            progressform.Show();


            SendEmailReport(0, true, true, ReportType.NORMAL);
        }
        public void BtnCom1_Loaded(object sender, RoutedEventArgs e)
        {
            btnCom1_LH = (CommandCtrl)sender;
        }
        private void BtnCom2_Loaded(object sender, RoutedEventArgs e)
        {
            btnCom2_LK = (CommandCtrl)sender;
        }
        private void BtnCom3_Loaded(object sender, RoutedEventArgs e)
        {
            btnCom3_RH = (CommandCtrl)sender;
        }
        private void BtnCom4_Loaded(object sender, RoutedEventArgs e)
        {
            btnCom4_RK = (CommandCtrl)sender;
        }
        CommandCtrl btnCom1_LH = new CommandCtrl();
        CommandCtrl btnCom2_LK = new CommandCtrl();
        CommandCtrl btnCom3_RH = new CommandCtrl();
        CommandCtrl btnCom4_RK = new CommandCtrl();
       
    
   
      
        private void btnClearLogFile_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show((string)this.FindResource("Are you sure you want to reset log file ?"), (string)this.FindResource("Log File Reset"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                string ClearLogFileStatus = deviceData.ClearLogFile();
               //MessageBox.Show(ClearLogFileStatus, "Confirmation", MessageBoxButton.OK, MessageBoxImage.Warning);

            }
        }
    

        //New GUI Rewalk 6
        private void ControlsIncDecButtonsEvent(object sender, RoutedEventArgs e)
        {
           
            switch ((string)((sender as Button).Name)) //button name was pressed
            {
                case "UpperStarpHolderSizeIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (deviceData.Parameters.UpperStrapHoldersSize < XMLParamLoadCheckValid.PUpperStrapHolderSizeLength_Value)
                        {
                            deviceData.Parameters.UpperStrapHoldersSize++;
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.UpperStrapHoldersSize < XMLParamLoadCheckValid.PUpperStrapHolderSizeLength_Value)
                        {
                            deviceData.Parameters.UpperStrapHoldersSize++;
                        }
                    }
                    break;
                case "UpperStarpHolderSizeDecrease":

                    if (deviceData.Parameters.UpperStrapHoldersSize > 0)
                    {
                        deviceData.Parameters.UpperStrapHoldersSize--;
                    }
                    break;
                case "UpperStarpHolderTypeIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (deviceData.Parameters.UpperStrapHoldersPosition < XMLParamLoadCheckValid.PUpperStrapHolderTypeLength_Value)
                        {
                            deviceData.Parameters.UpperStrapHoldersPosition++;
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.UpperStrapHoldersPosition < XMLParamLoadCheckValid.PUpperStrapHolderTypeLength_Value)
                        {
                            deviceData.Parameters.UpperStrapHoldersPosition++;
                        }
                    }
                    break;
                case "UpperStarpHolderTypeDecrease":

                    if (deviceData.Parameters.UpperStrapHoldersPosition > 0)
                    {
                        deviceData.Parameters.UpperStrapHoldersPosition--;
                    }
                    break;

                case "AboveKneeBrackeTypeIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (deviceData.Parameters.AboveKneeBracketSize < XMLParamLoadCheckValid.RAboveKneeBracketTypeLength_Value)
                        {
                            deviceData.Parameters.AboveKneeBracketSize++;
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.AboveKneeBracketSize < XMLParamLoadCheckValid.PAboveKneeBracketTypeLength_Value)
                        {
                            deviceData.Parameters.AboveKneeBracketSize++;
                        }
                    }
                    break;
                case "AboveKneeBrackeTypeDecrease":

                    if (deviceData.Parameters.AboveKneeBracketSize > 0)
                    {
                        deviceData.Parameters.AboveKneeBracketSize--;
                    }
                    break;

                case "AboveKneeBrackeAnteriorIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (deviceData.Parameters.AboveKneeBracketPosition < XMLParamLoadCheckValid.RAboveKneeBracketAnteriorLength_Value)
                        {
                            deviceData.Parameters.AboveKneeBracketPosition++;
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.AboveKneeBracketPosition < XMLParamLoadCheckValid.PAboveKneeBracketAnteriorLength_Value)
                        {
                            deviceData.Parameters.AboveKneeBracketPosition++;
                        }
                    }
                    break;
                case "AboveKneeBrackeAnteriorDecrease":

                    if (deviceData.Parameters.AboveKneeBracketPosition > 0)
                    {
                        deviceData.Parameters.AboveKneeBracketPosition--;
                    }
                    break;

                case "KneeBrackeTypeIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (deviceData.Parameters.FrontKneeBracketAssemble < XMLParamLoadCheckValid.RKneeBracketTypeLength_Value)
                        {
                            deviceData.Parameters.FrontKneeBracketAssemble++;
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.FrontKneeBracketAssemble < XMLParamLoadCheckValid.PKneeBracketTypeLength_Value)
                        {
                            deviceData.Parameters.FrontKneeBracketAssemble++;
                        }
                    }
                    break;
                case "KneeBrackeTypeDecrease":

                    if (deviceData.Parameters.FrontKneeBracketAssemble > 0)
                    {
                        deviceData.Parameters.FrontKneeBracketAssemble--;
                    }
                    break;
                case "KneeBrackeAnteriorIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (deviceData.Parameters.FrontKneeBracketAnteriorPosition < XMLParamLoadCheckValid.RKneeBracketAnteriorPositionLength_Value)
                        {
                            deviceData.Parameters.FrontKneeBracketAnteriorPosition++;
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.FrontKneeBracketAnteriorPosition < XMLParamLoadCheckValid.PKneeBracketAnteriorPositionLength_Value)
                        {
                            deviceData.Parameters.FrontKneeBracketAnteriorPosition++;
                        }
                    }
                    break;
                case "KneeBrackeAnteriorDecrease":

                    if (deviceData.Parameters.FrontKneeBracketAnteriorPosition > 0)
                    {
                        deviceData.Parameters.FrontKneeBracketAnteriorPosition--;
                    }
                    break;

                case "KneeBrackeLateralIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.FrontKneeBracketLateralPosition < XMLParamLoadCheckValid.PKneeBracketLateralPositionLength_Value)
                        {
                            deviceData.Parameters.FrontKneeBracketLateralPosition++;
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.FrontKneeBracketLateralPosition < 3)
                        {
                            deviceData.Parameters.FrontKneeBracketLateralPosition++;
                        }
                    }
                    break;
                case "KneeBrackeLateralDecrease":

                    if (deviceData.Parameters.FrontKneeBracketLateralPosition > 0)
                    {
                        deviceData.Parameters.FrontKneeBracketLateralPosition--;
                    }
                    break;
                case "PelvicSizeIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (deviceData.Parameters.PelvicSize < XMLParamLoadCheckValid.RPelvicSizeLength_Value)
                        {
                            deviceData.Parameters.PelvicSize++;
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.PelvicSize < XMLParamLoadCheckValid.PPelvicSizeLength_Value)
                        {
                            deviceData.Parameters.PelvicSize++;
                        }
                    }
                    break;
                case "PelvicSizeDecrease":

                    if (deviceData.Parameters.PelvicSize > 0)
                    {
                        deviceData.Parameters.PelvicSize--;
                    }
                    break;
                case "PelvicAnteriorPositionIncrease":

                   if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.PelvicAnteriorPosition < XMLParamLoadCheckValid.PPelvicAnteriorPositionLength_Value)
                        {
                            deviceData.Parameters.PelvicAnteriorPosition++;
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.PelvicAnteriorPosition < 3)
                        {
                            deviceData.Parameters.PelvicAnteriorPosition++;
                        }
                    }
                    break;
                case "PelvicAnteriorPositioDecrease":

                    if (deviceData.Parameters.PelvicAnteriorPosition > 0)
                    {
                        deviceData.Parameters.PelvicAnteriorPosition--;
                    }
                    break;

                case "PelvicVerticalPositionIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (deviceData.Parameters.PelvicVerticalPosition < 3)
                        {
                            deviceData.Parameters.PelvicVerticalPosition++;
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.PelvicVerticalPosition < XMLParamLoadCheckValid.PPelvicVerticalPositionLength_Value)
                        {
                            deviceData.Parameters.PelvicVerticalPosition++;
                        }
                    }
                    break;
                case "PelvicVerticalPositioDecrease":

                    if (deviceData.Parameters.PelvicVerticalPosition > 0)
                    {
                        deviceData.Parameters.PelvicVerticalPosition--;
                    }
                    break;
                case "UpperLegLengthIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (rup_leg_length_index < XMLParamLoadCheckValid.RUpperLegLengthLength_Value)
                        {
                            deviceData.Parameters.UpperLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.RUpLegLen[++rup_leg_length_index]);
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (pup_leg_length_index < XMLParamLoadCheckValid.PUpperLegLengthLength_Value)
                        {
                            deviceData.Parameters.UpperLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.PUpLegLen[++pup_leg_length_index]);
                        }
                    }
                    break;
                    
                case "UpperLegLengthDecrease":
                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (rup_leg_length_index > 0)
                        {
                            deviceData.Parameters.UpperLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.RUpLegLen[--rup_leg_length_index]);
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (pup_leg_length_index > 0)
                        {
                            deviceData.Parameters.UpperLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.PUpLegLen[--pup_leg_length_index]);
                        }
                    }
                    break;

                case "LowerLegLengthIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (rlow_leg_length_index < XMLParamLoadCheckValid.RLowerLegLengthLength_Value)
                        {
                            deviceData.Parameters.LowerLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.RLowLegLen[++rlow_leg_length_index]);
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (plow_leg_length_index < XMLParamLoadCheckValid.PLowerLegLengthLength_Value)
                        {
                            deviceData.Parameters.LowerLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.PLowLegLen[++plow_leg_length_index]);
                        }
                    }
                    break;
                case "LowerLegLengthDecrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (rlow_leg_length_index > 0)
                        {
                            deviceData.Parameters.LowerLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.RLowLegLen[--rlow_leg_length_index]);
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (plow_leg_length_index > 0)
                        {
                            deviceData.Parameters.LowerLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.PLowLegLen[--plow_leg_length_index]);
                        }
                    }
                    break;

                case "FootPlateTypeIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (deviceData.Parameters.FootPlateType1 < XMLParamLoadCheckValid.RFootPlateTypeLength_Value)
                        {
                            deviceData.Parameters.FootPlateType1++;
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.FootPlateType1 < XMLParamLoadCheckValid.PFootPlateTypeLength_Value)
                        {
                            deviceData.Parameters.FootPlateType1++;
                        }
                    }
                    break;
                case "FootPlateTypeDecrease":

                    if (deviceData.Parameters.FootPlateType1 > 0)
                    {
                        deviceData.Parameters.FootPlateType1--;
                    }
                    break;
                case "FootPlateSizeIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (deviceData.Parameters.FootPlateType2 < XMLParamLoadCheckValid.RFootPlateSizeLength_Value)
                        {
                            deviceData.Parameters.FootPlateType2++;
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.FootPlateType2 < XMLParamLoadCheckValid.PFootPlateSizeLength_Value)
                        {
                            deviceData.Parameters.FootPlateType2++;
                        }
                    }
                    break;
                case "FootPlatesizeDecrease":

                    if (deviceData.Parameters.FootPlateType2 > 0)
                    {
                        deviceData.Parameters.FootPlateType2--;
                    }
                    break;
                case "FootPlateDorsiFlextionIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (deviceData.Parameters.FootPlateNoOfRotation < XMLParamLoadCheckValid.RFootPlateDorsiFlexionPositionLength_Value)
                        {
                            deviceData.Parameters.FootPlateNoOfRotation++;
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        if (deviceData.Parameters.FootPlateNoOfRotation < XMLParamLoadCheckValid.PFootPlateDorsiFlexionPositionLength_Value)
                        {
                            deviceData.Parameters.FootPlateNoOfRotation++;
                        }
                    }
                    break;
                case "FootPlateDorsiFlextionDecrease":

                    if (deviceData.Parameters.FootPlateNoOfRotation > 0)
                    {
                        deviceData.Parameters.FootPlateNoOfRotation--;
                    }
                    break;

 //////////////////////////////////////  Standing tab item //////////////////////
                case "FallDetectionThresholdIncrease":

                    if (faldet_index < XMLParamLoadCheckValid.FallDetectionThrLength_Value)
                    {
                        deviceData.Parameters.YThreshold = System.Convert.ToInt16(XMLParamLoadCheckValid.faldet[++faldet_index]);
                    }
                    break;
                case "FallDetectionThresholdDecrease":

                    if (faldet_index > 0)
                    {
                        deviceData.Parameters.YThreshold= System.Convert.ToInt16(XMLParamLoadCheckValid.faldet[--faldet_index]);
                    }
                    break;
                case "StandCurrentThresholdIncrease":

                    if (stct_index < XMLParamLoadCheckValid.SystemCurrentThrLength_Value)
                    {
                        deviceData.Parameters.Stairs_Current_Threshold = System.Convert.ToInt16(XMLParamLoadCheckValid.stct[++stct_index]);
                    }
                    break;
                case "StandCurrentThresholdDecrease":

                    if (stct_index > 0)
                    {
                        deviceData.Parameters.Stairs_Current_Threshold = System.Convert.ToInt16(XMLParamLoadCheckValid.stct[--stct_index]);

                    }
                    break;
     ///////////////////////////////////////////////  Walking tab item //////////////////////

                case "HipFlextionIncrease":

                    if (hx_index < XMLParamLoadCheckValid.HipFlextionAngleLength_Value)
                    {
                        deviceData.Parameters.HipAngle = System.Convert.ToInt16(XMLParamLoadCheckValid.hx[++hx_index]) ;
                    }
                    break;
                case "HipFlextionDecrease":

                    if (hx_index > 0)
                    {
                        deviceData.Parameters.HipAngle = System.Convert.ToInt16(XMLParamLoadCheckValid.hx[--hx_index]);
                    }
                    break;

                case "KneeFlextionIncrease":

                    if (kx_index < XMLParamLoadCheckValid.KneeFlextionAngleLength_Value)
                    {
                        deviceData.Parameters.KneeAngle = System.Convert.ToInt16(XMLParamLoadCheckValid.kx[++kx_index]);
                    }
                    break;
                case "KneeFlextionDecrease":

                    if (kx_index > 0)
                    {
                        deviceData.Parameters.KneeAngle = System.Convert.ToInt16(XMLParamLoadCheckValid.kx[--kx_index]);
                    }
                    break;
                case "StepTimeIncrease":

                    if (spt_index < XMLParamLoadCheckValid.StepTimeLength_Value)
                    {
                        deviceData.Parameters.MaxVelocity = System.Convert.ToInt16(XMLParamLoadCheckValid.spt[++spt_index]);
                    }
                    break;
                case "StepTimeDecrease":
                
                    if (spt_index > 0)
                    {
                        deviceData.Parameters.MaxVelocity = System.Convert.ToInt16(XMLParamLoadCheckValid.spt[--spt_index]);
                    }
                    break;
                case "TiltDeltaIncrease":
                    if (tiltd_index < XMLParamLoadCheckValid.TiltDeltaLength_Value)
                    {
                        deviceData.Parameters.TiltDelta = System.Convert.ToInt16(XMLParamLoadCheckValid.tiltd[++tiltd_index]);
                    }
                    break;
                case "TiltDeltaDecrease":
                    if (tiltd_index > 0)
                    {
                        deviceData.Parameters.TiltDelta = System.Convert.ToInt16(XMLParamLoadCheckValid.tiltd[--tiltd_index]);
                    }
                    break;
                case "TiltTimeoutIncrease":
                    if (tiltt_index < XMLParamLoadCheckValid.TiltTimeOutLength_Value)
                    {
                        deviceData.Parameters.TiltTimeout = System.Convert.ToInt16(XMLParamLoadCheckValid.tiltt[++tiltt_index]);
                    }
                    break;
                case "TiltTimeoutDecrease":
                    if (tiltt_index > 0)
                    {
                        deviceData.Parameters.TiltTimeout = System.Convert.ToInt16(XMLParamLoadCheckValid.tiltt[--tiltt_index]);
                    }
                    break;
                case "BeginnersStepTimeIncrease":
                    if (tspt_index < XMLParamLoadCheckValid.BeginnersStepTimeLength_Value)
                    {
                        deviceData.Parameters.BackHipAngle = System.Convert.ToInt16(XMLParamLoadCheckValid.tspt[++tspt_index]);
                    }
                    break;
                case "BeginnersStepTimeDecrease":
                    if (tspt_index > 0)
                    {
                        deviceData.Parameters.BackHipAngle = System.Convert.ToInt16(XMLParamLoadCheckValid.tspt[--tspt_index]);
                    }
                    break;
                case "DelayBetweenStepsIncrease":
                    if (dbets_index < XMLParamLoadCheckValid.DelayBetweenStepsLength_Value)
                    {
                        deviceData.Parameters.DelayBetweenSteps = System.Convert.ToInt16(XMLParamLoadCheckValid.dbets[++dbets_index]);
                    }
                    break;
                case "DelayBetweenStepsDecrease":
                    if (dbets_index > 0)
                    {
                        deviceData.Parameters.DelayBetweenSteps = System.Convert.ToInt16(XMLParamLoadCheckValid.dbets[--dbets_index]);
                    }
                    break;
                case "WalkCurrentThresholdIncrease":
                    if (wact_index < XMLParamLoadCheckValid.WalkCurrentThrLength_Value)
                    {
                        deviceData.Parameters.Walk_Current_Threshold = System.Convert.ToInt16(XMLParamLoadCheckValid.wact[++wact_index]);
                    }
                    break;
                case "WalkCurrentThresholdDecrease":
                    if (wact_index > 0)
                    {
                        deviceData.Parameters.Walk_Current_Threshold = System.Convert.ToInt16(XMLParamLoadCheckValid.wact[--wact_index]);
                    }
                    break;
                case "FirstStepFlexionIncrease":
                    if (fsf_index < XMLParamLoadCheckValid.FirststepFlexionLength_Value)
                    {
                        deviceData.Parameters.FirstStepFlexion = System.Convert.ToInt16(XMLParamLoadCheckValid.fsf[++fsf_index]);
                    }
                    break;
                case "FirstStepFlexionDecrease":
                    if (fsf_index > 0)
                    {
                        deviceData.Parameters.FirstStepFlexion = System.Convert.ToInt16(XMLParamLoadCheckValid.fsf[--fsf_index]);
                    }
                    break;
                case "MaxHipFlextionIncrease":
                    if (hmaxf_index < XMLParamLoadCheckValid.HipMaxFlextionAngleLength_Value)
                    {
                        deviceData.Parameters.FSRThreshold = System.Convert.ToInt16(XMLParamLoadCheckValid.hmaxf[++hmaxf_index]);
                    }
                    break;
                case "MaxHipFlextionDecrease":
                    if (hmaxf_index > 0)
                    {
                        deviceData.Parameters.FSRThreshold = System.Convert.ToInt16(XMLParamLoadCheckValid.hmaxf[--hmaxf_index]);
                    }
                    break;
                case "HipFinalFlextionIncrease":
                    if (hfinf_index < XMLParamLoadCheckValid.HipFinalFlextionAngleLength_Value)
                    {
                        deviceData.Parameters.FSRWalkTimeout = System.Convert.ToInt16(XMLParamLoadCheckValid.hfinf[++hfinf_index]);
                    }
                    break;
                case "HipFinalFlextionDecrease":
                    if (hfinf_index > 0)
                    {
                        deviceData.Parameters.FSRWalkTimeout = System.Convert.ToInt16(XMLParamLoadCheckValid.hfinf[--hfinf_index]);
                    }
                    break;
                case "StairsASCSpeedIncrease":
                    if (ascsp_index < XMLParamLoadCheckValid.ASCSpeedLength_Value)
                    {
                        deviceData.Parameters.StairsASCSpeed = System.Convert.ToInt16(XMLParamLoadCheckValid.ascsp[++ascsp_index]);
                    }
                    break;
                case "StairsASCSpeedDecrease":
                    if (ascsp_index > 0)
                    {
                        deviceData.Parameters.StairsASCSpeed = System.Convert.ToInt16(XMLParamLoadCheckValid.ascsp[--ascsp_index]);
                    }
                    break;
                case "StairsDSCSpeedIncrease":
                    if (dscsp_index < XMLParamLoadCheckValid.DSCSpeedLength_Value)
                    {
                        deviceData.Parameters.StairsDSCSpeed = System.Convert.ToInt16(XMLParamLoadCheckValid.dscsp[++dscsp_index]);
                    }
                    break;
                case "StairsDSCSpeedDecrease":
                    if (dscsp_index > 0)
                    {
                        deviceData.Parameters.StairsDSCSpeed = System.Convert.ToInt16(XMLParamLoadCheckValid.dscsp[--dscsp_index]);
                    }
                    break;
                case "StairHeighIncrease":
                    if (stairheight_index < XMLParamLoadCheckValid.StairHeightLength_Value)
                    {
                        deviceData.Parameters.StairHeight = System.Convert.ToInt16(XMLParamLoadCheckValid.stairheight[++stairheight_index]);
                    }
                    break;
                case "StairHeighDecrease":
                    if (stairheight_index > 0)
                    {
                        deviceData.Parameters.StairHeight = System.Convert.ToInt16(XMLParamLoadCheckValid.stairheight[--stairheight_index]);
                    }
                    break;
                ///////////////////////////////////////////////  Stairs tab item //////////////////////


                case "LHASCIncrease":
                    if (asc_lh_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value)
                    {
                        deviceData.Parameters.LHAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[++asc_lh_index]);
                    }
                    break;
                case "LHDSCIncrease":
                    if (dsc_lh_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value)
                    {
                        deviceData.Parameters.LHDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[++dsc_lh_index]);
                    }
                    break;
                case "LKASCIncrease":
                    if (asc_lk_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value)
                    {
                        deviceData.Parameters.LKAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[++asc_lk_index]);
                    }
                    break;
                case "LKDSCIncrease":
                    if (dsc_lk_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value)
                    {
                        deviceData.Parameters.LKDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[++dsc_lk_index]);
                    }
                    break;

                case "LHASCDecrease":
                    if (asc_lh_index > 0)
                    {
                        deviceData.Parameters.LHAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[--asc_lh_index]);
                    }
                    break;
                case "LHDSCDecrease":
                    if (dsc_lh_index > 0)
                    {
                        deviceData.Parameters.LHDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[--dsc_lh_index]);
                    }
                    break;
                case "LKASCDecrease":
                    if (asc_lk_index > 0)
                    {
                        deviceData.Parameters.LKAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[--asc_lk_index]);
                    }
                    break;
                case "LKDSCDecrease":
                    if (dsc_lk_index > 0)
                    {
                        deviceData.Parameters.LKDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[--dsc_lk_index]);
                    }
                    break;
                    ///////////////////////////
                case "RHASCIncrease":
                    if (asc_rh_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value)
                    {
                        deviceData.Parameters.RHAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[++asc_rh_index]);
                    }
                    break;
                case "RHDSCIncrease":
                    if (dsc_rh_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value)
                    {
                        deviceData.Parameters.RHDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[++dsc_rh_index]);
                    }
                    break;
                case "RKASCIncrease":
                    if (asc_rk_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value)
                    {
                        deviceData.Parameters.RKAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[++asc_rk_index]);
                    }
                    break;
                case "RKDSCIncrease":
                    if (dsc_rk_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value)
                    {
                        deviceData.Parameters.RKDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[++dsc_rk_index]);
                    }
                    break;

                case "RHASCDecrease":
                    if (asc_rh_index > 0)
                    {
                        deviceData.Parameters.RHAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[--asc_rh_index]);
                    }
                    break;
                case "RHDSCDecrease":
                    if (dsc_rh_index > 0)
                    {
                        deviceData.Parameters.RHDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[--dsc_rh_index]);
                    }
                    break;
                case "RKASCDecrease":
                    if (asc_rk_index > 0)
                    {
                        deviceData.Parameters.RKAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[--asc_rk_index]);
                    }
                    break;
                case "RKDSCDecrease":
                    if (dsc_rk_index > 0)
                    {
                        deviceData.Parameters.RKDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.stairs[--dsc_rk_index]);
                    }
                    break;
                case "ShoeLengthIncrease":
                    if (shoe_length_index < XMLParamLoadCheckValid.ShoeLenghtLength_Value)
                    {
                        deviceData.Parameters.ShoeSize = System.Convert.ToInt16(XMLParamLoadCheckValid.ShoeLen[++shoe_length_index]);
                    }
                    break;
                case "ShoeLengthDecrease":
                    if (shoe_length_index > 0)
                    {
                        deviceData.Parameters.ShoeSize = System.Convert.ToInt16(XMLParamLoadCheckValid.ShoeLen[--shoe_length_index]);
                    }
                    break;

            }
            InitSystemStrings();



         
        }


       
        CheckPrametersValid XMLParamLoadCheckValid = new CheckPrametersValid();

        private void UserParamData_VerfiytoSave(Data deviceData_temp) // parasoft-suppress  METRICS.MCCC "Methods executing atomic functions" METRICS.MLOC "Methods executing atomic functions" CS.MLC "Methods executing atomic functions"
        {
           
           
            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.HipAngle, XMLParamLoadCheckValid.HipFlextionAngleMin_Value, XMLParamLoadCheckValid.HipFlextionAngleMax_Value) == true)
                deviceData.Parameters.HipAngle = deviceData_temp.Parameters.HipAngle;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.KneeAngle, XMLParamLoadCheckValid.KneeFlextionAngleMin_Value, XMLParamLoadCheckValid.KneeFlextionAngleMax_Value) == true)
                deviceData.Parameters.KneeAngle = deviceData_temp.Parameters.KneeAngle;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.BackHipAngle, XMLParamLoadCheckValid.BeginnersStepTimeMin_Value, XMLParamLoadCheckValid.BeginnersStepTimeMax_Value) == true)
                deviceData.Parameters.BackHipAngle = deviceData_temp.Parameters.BackHipAngle;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.MaxVelocity, XMLParamLoadCheckValid.StepTimeMin_Value, XMLParamLoadCheckValid.StepTimeMax_Value) == true)
                deviceData.Parameters.MaxVelocity = deviceData_temp.Parameters.MaxVelocity;//Step time

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.TiltDelta, XMLParamLoadCheckValid.TiltDeltaMin_Value, XMLParamLoadCheckValid.TiltDeltaMax_Value) == true)
                deviceData.Parameters.TiltDelta = deviceData_temp.Parameters.TiltDelta;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.TiltTimeout, XMLParamLoadCheckValid.TiltTimeOutMin_Value, XMLParamLoadCheckValid.TiltTimeOutMax_Value) == true)
                deviceData.Parameters.TiltTimeout = deviceData_temp.Parameters.TiltTimeout;

            if (deviceData_temp.Parameters.SafetyWhileStanding == true || deviceData_temp.Parameters.SafetyWhileStanding == false)
                deviceData.Parameters.SafetyWhileStanding = deviceData_temp.Parameters.SafetyWhileStanding;

            if (deviceData_temp.Parameters.XThreshold != 0)
                deviceData.Parameters.XThreshold = deviceData_temp.Parameters.XThreshold;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.YThreshold, XMLParamLoadCheckValid.FallDetectionThrMin_Value, XMLParamLoadCheckValid.FallDetectionThrMax_Value) == true)
                deviceData.Parameters.YThreshold = deviceData_temp.Parameters.YThreshold;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.DelayBetweenSteps, XMLParamLoadCheckValid.DelayBetweenStepsMin_Value, XMLParamLoadCheckValid.DelayBetweenStepsMax_Value) == true)
                deviceData.Parameters.DelayBetweenSteps = deviceData_temp.Parameters.DelayBetweenSteps;

            if (deviceData_temp.Parameters.BuzzerBetweenSteps == true || deviceData_temp.Parameters.BuzzerBetweenSteps == false)
                deviceData.Parameters.BuzzerBetweenSteps = deviceData_temp.Parameters.BuzzerBetweenSteps;

            if (deviceData_temp.Parameters.Record == true || deviceData_temp.Parameters.Record == false)
                deviceData.Parameters.Record = deviceData_temp.Parameters.Record;//Enable walk sfter stuck

            if (deviceData_temp.Parameters.SwitchFSRs == true || deviceData_temp.Parameters.SwitchFSRs == false)
                deviceData.Parameters.SwitchFSRs = deviceData_temp.Parameters.SwitchFSRs;//Beginner mode enable\disable

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FSRThreshold, XMLParamLoadCheckValid.HipMaxFlextionAngleMin_Value, XMLParamLoadCheckValid.HipMaxFlextionAngleMax_Value) == true)
                deviceData.Parameters.FSRThreshold = deviceData_temp.Parameters.FSRThreshold;//Beginner Hip max Flexion

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FSRWalkTimeout, XMLParamLoadCheckValid.HipFinalFlextionAngleMin_Value, XMLParamLoadCheckValid.HipFinalFlextionAngleMax_Value) == true)
                deviceData.Parameters.FSRWalkTimeout = deviceData_temp.Parameters.FSRWalkTimeout;//Beginner Hip final flexion

            if (deviceData_temp.Parameters.EnableFirstStep == true || deviceData_temp.Parameters.EnableFirstStep == false)
                deviceData.Parameters.EnableFirstStep = deviceData_temp.Parameters.EnableFirstStep;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FirstStepFlexion, XMLParamLoadCheckValid.FirststepFlexionMin_Value, XMLParamLoadCheckValid.FirststepFlexionMax_Value) == true)
                deviceData.Parameters.FirstStepFlexion = deviceData_temp.Parameters.FirstStepFlexion;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.Walk_Current_Threshold, XMLParamLoadCheckValid.WalkCurrentThrMin_Value, XMLParamLoadCheckValid.WalkCurrentThrMax_Value) == true)
                deviceData.Parameters.Walk_Current_Threshold = deviceData_temp.Parameters.Walk_Current_Threshold;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.Stairs_Current_Threshold, XMLParamLoadCheckValid.SystemCurrentThrMin_Value, XMLParamLoadCheckValid.SystemCurrentThrMax_Value) == true)
                deviceData.Parameters.Stairs_Current_Threshold = deviceData_temp.Parameters.Stairs_Current_Threshold;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.LHAsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.LHAsc = deviceData_temp.Parameters.LHAsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.LHDsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.LHDsc = deviceData_temp.Parameters.LHDsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.LKAsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.LKAsc = deviceData_temp.Parameters.LKAsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.LKDsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.LKDsc = deviceData_temp.Parameters.LKDsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.RHAsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.RHAsc = deviceData_temp.Parameters.RHAsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.RHDsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.RHDsc = deviceData_temp.Parameters.RHDsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.RKAsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.RKAsc = deviceData_temp.Parameters.RKAsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.RKDsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.RKDsc = deviceData_temp.Parameters.RKDsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.SwitchingTime, XMLParamLoadCheckValid.SwitchingTimeMin_Value, XMLParamLoadCheckValid.SwitchingTimeMax_Value) == true)
                deviceData.Parameters.SwitchingTime = deviceData_temp.Parameters.SwitchingTime;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.StairHeight, XMLParamLoadCheckValid.StairHeightMin_Value, XMLParamLoadCheckValid.StairHeightMax_Value) == true)
                deviceData.Parameters.StairHeight = deviceData_temp.Parameters.StairHeight;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.SoleHeight, XMLParamLoadCheckValid.ShoeLenghtMin_Value, XMLParamLoadCheckValid.ShoeLenghtMax_Value) == true)
                deviceData.Parameters.SoleHeight = deviceData_temp.Parameters.SoleHeight;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.Placing_RH_on_the_stair, XMLParamLoadCheckValid.PlacingRHMin_Value, XMLParamLoadCheckValid.PlacingRHMax_Value) == true)
                deviceData.Parameters.Placing_RH_on_the_stair = deviceData_temp.Parameters.Placing_RH_on_the_stair;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.Placing_RK_on_the_stair, XMLParamLoadCheckValid.PlacingRKMin_Value, XMLParamLoadCheckValid.PlacingRKMax_Value) == true)
                deviceData.Parameters.Placing_RK_on_the_stair = deviceData_temp.Parameters.Placing_RK_on_the_stair;

            if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.UpperStrapHoldersSize, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PUpperStrapHolderSizeLength_Value)) == true)
                    deviceData.Parameters.UpperStrapHoldersSize = deviceData_temp.Parameters.UpperStrapHoldersSize;
            }

            if (deviceData_temp.Parameters.SysTypeString == "Rewalk-P0" || deviceData_temp.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.UpperStrapHoldersPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PUpperStrapHolderTypeLength_Value)) == true)
                    deviceData.Parameters.UpperStrapHoldersPosition = deviceData_temp.Parameters.UpperStrapHoldersPosition;
            }

            if (deviceData_temp.Parameters.SysTypeString == "Rewalk-P0" || deviceData_temp.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.AboveKneeBracketSize, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PAboveKneeBracketTypeLength_Value)) == true)
                    deviceData.Parameters.AboveKneeBracketSize = deviceData_temp.Parameters.AboveKneeBracketSize;
            }
            else if (deviceData_temp.Parameters.SysTypeString == "Rewalk-R" || deviceData_temp.Parameters.SysTypeString == "Rewalk-I")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.AboveKneeBracketSize, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.RAboveKneeBracketTypeLength_Value)) == true)
                    deviceData.Parameters.AboveKneeBracketSize = deviceData_temp.Parameters.AboveKneeBracketSize;
            }

            if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.AboveKneeBracketPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PAboveKneeBracketAnteriorLength_Value)) == true)
                    deviceData.Parameters.AboveKneeBracketPosition = deviceData_temp.Parameters.AboveKneeBracketPosition;
            }
            else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.AboveKneeBracketPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.RAboveKneeBracketAnteriorLength_Value)) == true)
                    deviceData.Parameters.AboveKneeBracketPosition = deviceData_temp.Parameters.AboveKneeBracketPosition;
            }

            if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FrontKneeBracketAssemble, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PKneeBracketTypeLength_Value)) == true)
                    deviceData.Parameters.FrontKneeBracketAssemble = deviceData_temp.Parameters.FrontKneeBracketAssemble;
            }
            else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FrontKneeBracketAssemble, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.RKneeBracketTypeLength_Value)) == true)
                    deviceData.Parameters.FrontKneeBracketAssemble = deviceData_temp.Parameters.FrontKneeBracketAssemble;
            }

            if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FrontKneeBracketAnteriorPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PKneeBracketAnteriorPositionLength_Value)) == true)
                    deviceData.Parameters.FrontKneeBracketAnteriorPosition = deviceData_temp.Parameters.FrontKneeBracketAnteriorPosition;
            }
            else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FrontKneeBracketAnteriorPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.RKneeBracketAnteriorPositionLength_Value)) == true)
                    deviceData.Parameters.FrontKneeBracketAnteriorPosition = deviceData_temp.Parameters.FrontKneeBracketAnteriorPosition;
            }

            if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FrontKneeBracketLateralPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PKneeBracketLateralPositionLength_Value)) == true)
                    deviceData.Parameters.FrontKneeBracketLateralPosition = deviceData_temp.Parameters.FrontKneeBracketLateralPosition;
            }

            if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.PelvicSize, XMLParamLoadCheckValid.PPelvicSizeMin_Value,XMLParamLoadCheckValid.PPelvicSizeMax_Value) == true)
                    deviceData.Parameters.PelvicSize = deviceData_temp.Parameters.PelvicSize;
            }
            else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.PelvicSize, XMLParamLoadCheckValid.RPelvicSizeMin_Value, XMLParamLoadCheckValid.RPelvicSizeMax_Value) == true)
                    deviceData.Parameters.PelvicSize = deviceData_temp.Parameters.PelvicSize;
            }

            if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.PelvicAnteriorPosition, XMLParamLoadCheckValid.PPelvicAnteriorPositionMin_Value, XMLParamLoadCheckValid.PPelvicAnteriorPositionMax_Value) == true)
                    deviceData.Parameters.PelvicAnteriorPosition = deviceData_temp.Parameters.PelvicAnteriorPosition;
            }

            if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.PelvicVerticalPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PPelvicVerticalPositionLength_Value)) == true)
                    deviceData.Parameters.PelvicVerticalPosition = deviceData_temp.Parameters.PelvicVerticalPosition;
            }

            if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.UpperLegHightSize, XMLParamLoadCheckValid.PUpperLegLengthMin_Value, XMLParamLoadCheckValid.PUpperLegLengthMax_Value) == true)
                    deviceData.Parameters.UpperLegHightSize = deviceData_temp.Parameters.UpperLegHightSize;
            }
            else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.UpperLegHightSize, XMLParamLoadCheckValid.RUpperLegLengthMin_Value, XMLParamLoadCheckValid.RUpperLegLengthMax_Value) == true)
                    deviceData.Parameters.UpperLegHightSize = deviceData_temp.Parameters.UpperLegHightSize;
            }

            if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.LowerLegHightSize, XMLParamLoadCheckValid.PLowerLegLengthMin_Value, XMLParamLoadCheckValid.PLowerLegLengthMax_Value) == true)
                    deviceData.Parameters.LowerLegHightSize = deviceData_temp.Parameters.LowerLegHightSize;
            }
            else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.LowerLegHightSize, XMLParamLoadCheckValid.RLowerLegLengthMin_Value, XMLParamLoadCheckValid.RLowerLegLengthMax_Value) == true)
                    deviceData.Parameters.LowerLegHightSize = deviceData_temp.Parameters.LowerLegHightSize;
            }

            if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FootPlateType1,0, System.Convert.ToInt16(XMLParamLoadCheckValid.PFootPlateTypeLength_Value)) == true)
                    deviceData.Parameters.FootPlateType1 = deviceData_temp.Parameters.FootPlateType1;
            }
            else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FootPlateType1,0, System.Convert.ToInt16(XMLParamLoadCheckValid.RFootPlateTypeLength_Value)) == true)
                    deviceData.Parameters.FootPlateType1 = deviceData_temp.Parameters.FootPlateType1;
            }

            if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FootPlateType2, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PFootPlateSizeLength_Value)) == true)
                    deviceData.Parameters.FootPlateType2 = deviceData_temp.Parameters.FootPlateType2;
            }
            else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FootPlateType2, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.RFootPlateSizeLength_Value)) == true)
                    deviceData.Parameters.FootPlateType2 = deviceData_temp.Parameters.FootPlateType2;
            }

            if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(System.Convert.ToInt16(deviceData_temp.Parameters.FootPlateNoOfRotation), XMLParamLoadCheckValid.PFootPlateDorsiFlexionPositionMin_Value, XMLParamLoadCheckValid.PFootPlateDorsiFlexionPositionMax_Value) == true)
                    deviceData.Parameters.FootPlateNoOfRotation = deviceData_temp.Parameters.FootPlateNoOfRotation;
            }
            else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
            {
                if (XMLParamLoadCheckValid.ValidValueofInt16Pram(System.Convert.ToInt16(deviceData_temp.Parameters.FootPlateNoOfRotation), XMLParamLoadCheckValid.RFootPlateDorsiFlexionPositionMin_Value, XMLParamLoadCheckValid.RFootPlateDorsiFlexionPositionMax_Value) == true)
                    deviceData.Parameters.FootPlateNoOfRotation = deviceData_temp.Parameters.FootPlateNoOfRotation;
            }


            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.StairsASCSpeed, XMLParamLoadCheckValid.ASCSpeedMin_Value, XMLParamLoadCheckValid.ASCSpeedMax_Value) == true)
                deviceData.Parameters.StairsASCSpeed = deviceData_temp.Parameters.StairsASCSpeed;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.StairsDSCSpeed, XMLParamLoadCheckValid.DSCSpeedMin_Value, XMLParamLoadCheckValid.DSCSpeedMax_Value) == true)
                deviceData.Parameters.StairsDSCSpeed = deviceData_temp.Parameters.StairsDSCSpeed;

            InitSystemStrings();
        }

        private void TechnicianPasswordGo_Click(object sender, RoutedEventArgs e)
        {

         /*   string xword = TechnicianPasswordBox.Password;
            Technician.Visibility = (xword == GetSysPass()) ? Visibility.Visible : Visibility.Hidden;
            Technician.IsSelected = (xword == GetSysPass()) ? true : false;
            TbRightMenu.IsChecked = (xword == GetSysPass()) ? true : false;
            TbInnerMenu.Visibility= (xword == GetSysPass()) ? Visibility.Visible : Visibility.Hidden;
            TbRightMenu.IsChecked = false;
            
            
            
            TechnicianRadioBtn.IsEnabled = (xword == GetSysPass()) ? true : false;

            //  DiagnosticsGrid.Visibility = toolbar.Visibility = technicianGrid.Visibility = debugGrid.Visibility = advancedGrid.Visibility;

       
            //<!--<i:Interaction.Triggers>
            //                <i:EventTrigger EventName="Click">
            //                    <im:ControlStoryboardAction Storyboard="{StaticResource TechnicianEntranceClose}"/>
            //                    <im:ControlStoryboardAction Storyboard="{StaticResource RightMenuClose}"/>
            //                    <ic:ChangePropertyAction TargetName="Technician" PropertyName="IsSelected" Value="True"/>
            //                    <ic:ChangePropertyAction TargetName="TbRightMenu" PropertyName="IsChecked" Value="False"/>
            //                    <ic:ChangePropertyAction TargetName="TbInnerMenu" PropertyName="Visibility"/>
            //                </i:EventTrigger>
            //            </i:Interaction.Triggers>-->
            
            
            //Sync password for two panes
            /*  if (pbTechnician.Password != xword)
                  pbTechnician.Password = xword;
              if (pbAdvanced.Password != xword)
                  pbAdvanced.Password = xword;
              if (pbDebug.Password != xword)
                  pbDebug.Password = xword;
              /* if (pbAFT.Password != xword)
                   pbAFT.Password = xword;*/
        }

     


        private void ContWalkRadBut_Checked(object sender, RoutedEventArgs e)
        {

        }

       
            int hx_index;
            int faldet_index;
            int stct_index;
            int spt_index;
            int dbets_index;
            int wact_index;
            int fsf_index;
            int kx_index;
            int tiltd_index;
            int tiltt_index;
            int hmaxf_index;
            int hfinf_index;
            int tspt_index;
            int ascsp_index;
            int dscsp_index;
            int asc_lh_index;
            int asc_lk_index;
            int asc_rh_index;
            int asc_rk_index;
            int dsc_lh_index;
            int dsc_lk_index;
            int dsc_rh_index;
            int dsc_rk_index;
            int stairheight_index;
            int pup_leg_length_index;
            int rup_leg_length_index;
            int plow_leg_length_index;
            int rlow_leg_length_index;
            int shoe_length_index;


            public void ReturnValueIndex()
            {
                try
                {
                    hx_index = Array.IndexOf(XMLParamLoadCheckValid.hx, (deviceData.Parameters.HipAngle).ToString());
                    faldet_index = Array.IndexOf(XMLParamLoadCheckValid.faldet,(deviceData.Parameters.YThreshold).ToString());
                    stct_index = Array.IndexOf(XMLParamLoadCheckValid.stct, (deviceData.Parameters.Stairs_Current_Threshold).ToString());
                    spt_index = Array.IndexOf(XMLParamLoadCheckValid.spt, (deviceData.Parameters.MaxVelocity).ToString());
                    dbets_index = Array.IndexOf(XMLParamLoadCheckValid.dbets,(deviceData.Parameters.DelayBetweenSteps).ToString());
                    wact_index = Array.IndexOf(XMLParamLoadCheckValid.wact,(deviceData.Parameters.Walk_Current_Threshold).ToString());
                    fsf_index = Array.IndexOf(XMLParamLoadCheckValid.fsf, (deviceData.Parameters.FirstStepFlexion).ToString());
                    kx_index = Array.IndexOf(XMLParamLoadCheckValid.kx, (deviceData.Parameters.KneeAngle).ToString());
                    tiltd_index = Array.IndexOf(XMLParamLoadCheckValid.tiltd, (deviceData.Parameters.TiltDelta).ToString());
                    tiltt_index = Array.IndexOf(XMLParamLoadCheckValid.tiltt, (deviceData.Parameters.TiltTimeout).ToString());
                    hmaxf_index = Array.IndexOf(XMLParamLoadCheckValid.hmaxf, (deviceData.Parameters.FSRThreshold).ToString());
                    hfinf_index = Array.IndexOf(XMLParamLoadCheckValid.hfinf, (deviceData.Parameters.FSRWalkTimeout).ToString());
                    tspt_index = Array.IndexOf(XMLParamLoadCheckValid.tspt, (deviceData.Parameters.BackHipAngle).ToString());
                    ascsp_index = Array.IndexOf(XMLParamLoadCheckValid.ascsp, (deviceData.Parameters.StairsASCSpeed).ToString());
                    dscsp_index = Array.IndexOf(XMLParamLoadCheckValid.dscsp, (deviceData.Parameters.StairsDSCSpeed).ToString());
                    stairheight_index = Array.IndexOf(XMLParamLoadCheckValid.stairheight,(deviceData.Parameters.StairHeight).ToString());
                    asc_lh_index = Array.IndexOf(XMLParamLoadCheckValid.stairs, (deviceData.Parameters.LHAsc).ToString());
                    asc_lk_index = Array.IndexOf(XMLParamLoadCheckValid.stairs, (deviceData.Parameters.LKAsc).ToString());
                    asc_rh_index = Array.IndexOf(XMLParamLoadCheckValid.stairs, (deviceData.Parameters.RHAsc).ToString());
                    asc_rk_index = Array.IndexOf(XMLParamLoadCheckValid.stairs, (deviceData.Parameters.RKAsc).ToString());
                    dsc_lh_index = Array.IndexOf(XMLParamLoadCheckValid.stairs, (deviceData.Parameters.LHDsc).ToString());
                    dsc_lk_index = Array.IndexOf(XMLParamLoadCheckValid.stairs, (deviceData.Parameters.LKDsc).ToString());
                    dsc_rh_index = Array.IndexOf(XMLParamLoadCheckValid.stairs, (deviceData.Parameters.RHDsc).ToString());
                    dsc_rk_index = Array.IndexOf(XMLParamLoadCheckValid.stairs, (deviceData.Parameters.RKDsc).ToString());
                    pup_leg_length_index = Array.IndexOf(XMLParamLoadCheckValid.PUpLegLen, (deviceData.Parameters.UpperLegHightSize).ToString());
                    rup_leg_length_index = Array.IndexOf(XMLParamLoadCheckValid.RUpLegLen,(deviceData.Parameters.UpperLegHightSize).ToString());
                    plow_leg_length_index = Array.IndexOf(XMLParamLoadCheckValid.PLowLegLen,(deviceData.Parameters.LowerLegHightSize).ToString());
                    rlow_leg_length_index = Array.IndexOf(XMLParamLoadCheckValid.RLowLegLen, (deviceData.Parameters.LowerLegHightSize).ToString());
                    shoe_length_index = Array.IndexOf(XMLParamLoadCheckValid.ShoeLen, (deviceData.Parameters.ShoeSize).ToString());
                }
                catch (Exception exception)
                {

                    Logger.Error_MessageBox_Advanced("Finding Current Index was failed", "Finding Current Index", exception.Message);

                }

            }

            private void InitSystemStrings()
            {
                try
                {
                    deviceData.Parameters.UpperStrapHolderSizeString = XMLParamLoadCheckValid.PUpStrapSize[deviceData.Parameters.UpperStrapHoldersSize];

                    deviceData.Parameters.UpperStrapHolderTypeString = XMLParamLoadCheckValid.PUpStrapType[deviceData.Parameters.UpperStrapHoldersPosition];

                    if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        deviceData.Parameters.AboveKneeBracketTypeString = XMLParamLoadCheckValid.PAKBType[deviceData.Parameters.AboveKneeBracketSize];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        deviceData.Parameters.AboveKneeBracketTypeString = XMLParamLoadCheckValid.RAKBType[deviceData.Parameters.AboveKneeBracketSize];
                    }

                    if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        deviceData.Parameters.AboveKneeBracketAnteriorString = XMLParamLoadCheckValid.PAKBAnterior[deviceData.Parameters.AboveKneeBracketPosition];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        deviceData.Parameters.AboveKneeBracketAnteriorString = XMLParamLoadCheckValid.RAKBAnterior[deviceData.Parameters.AboveKneeBracketPosition];
                    }

                    if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        deviceData.Parameters.KneeBracketTypeString = XMLParamLoadCheckValid.PKBType[deviceData.Parameters.FrontKneeBracketAssemble];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        deviceData.Parameters.KneeBracketTypeString = XMLParamLoadCheckValid.RKBType[deviceData.Parameters.FrontKneeBracketAssemble];
                    }

                    if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        deviceData.Parameters.KneeBracketAnteriorPositionString = XMLParamLoadCheckValid.PKBAnteriorPos[deviceData.Parameters.FrontKneeBracketAnteriorPosition];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        deviceData.Parameters.KneeBracketAnteriorPositionString = XMLParamLoadCheckValid.RKBAnteriorPos[deviceData.Parameters.FrontKneeBracketAnteriorPosition];
                    }

                    if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        deviceData.Parameters.KneeBracketLateralPositionString = XMLParamLoadCheckValid.PKBLateralPos[deviceData.Parameters.FrontKneeBracketLateralPosition];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        deviceData.Parameters.KneeBracketLateralPositionString = "N/A";
                    }

                    if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        deviceData.Parameters.PelvicSizeString = XMLParamLoadCheckValid.PPelvicSize[deviceData.Parameters.PelvicSize];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        deviceData.Parameters.PelvicSizeString = XMLParamLoadCheckValid.RPelvicSize[deviceData.Parameters.PelvicSize];
                    }

                    if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        deviceData.Parameters.PelvicAnteriorPositionString = XMLParamLoadCheckValid.PPelvicAnteriorPos[deviceData.Parameters.PelvicAnteriorPosition];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        deviceData.Parameters.PelvicAnteriorPositionString = "N/A";
                    }

                    if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        deviceData.Parameters.PelvicVerticalPositionString = XMLParamLoadCheckValid.PPelvicVerticalPos[deviceData.Parameters.PelvicVerticalPosition];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        deviceData.Parameters.PelvicVerticalPositionString = "N/A";
                    }

                    if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        deviceData.Parameters.FootPlateTypeString = XMLParamLoadCheckValid.PFPlateType[deviceData.Parameters.FootPlateType1];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        deviceData.Parameters.FootPlateTypeString = XMLParamLoadCheckValid.RFPlateType[deviceData.Parameters.FootPlateType1];
                    }

                    if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        deviceData.Parameters.FootPlateSizeString = XMLParamLoadCheckValid.PFPlateSize[deviceData.Parameters.FootPlateType2];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        deviceData.Parameters.FootPlateSizeString = XMLParamLoadCheckValid.RFPlateSize[deviceData.Parameters.FootPlateType2];
                    }

                    if (deviceData.Parameters.SysTypeString == "Rewalk-P0" || deviceData.Parameters.SysTypeString == "Rewalk-P6")
                    {
                        deviceData.Parameters.FootPlateDorsiFlexionPositionString = XMLParamLoadCheckValid.PFPDorsiFlexPos[deviceData.Parameters.FootPlateNoOfRotation];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        deviceData.Parameters.FootPlateDorsiFlexionPositionString = XMLParamLoadCheckValid.RFPDorsiFlexPos[deviceData.Parameters.FootPlateNoOfRotation];
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error_MessageBox_Advanced("System measurements initialization has failed." +"\n"+ "Perform 'Restore' and then 'Load to Rewalk' to continue.","Intialiazing System Configuration", exception.Message);
                }
                //For stairs strings
                try
                {
                    int ASCSpeed = Array.IndexOf(XMLParamLoadCheckValid.ascsp, deviceData.Parameters.StairsASCSpeed.ToString());
                    deviceData.Parameters.ASCSpeedString = XMLParamLoadCheckValid.ASCSpeedStrings[ASCSpeed];

                    int DSCSpeed = Array.IndexOf(XMLParamLoadCheckValid.dscsp, deviceData.Parameters.StairsDSCSpeed.ToString());
                    deviceData.Parameters.DSCSpeedString = XMLParamLoadCheckValid.DSCSpeedStrings[DSCSpeed];
                }
                catch (Exception exception)
                {
                    Logger.Error_MessageBox_Advanced("System measurements initialization has failed." + "\n" + "Perform 'Restore' and then 'Load to Rewalk' to continue.", "Intialiazing System Configuration", exception.Message);
                }

            }
            private void Detect_killRewalkRunningApllication()
            {
                System.Array Instances_Rewalk6;
                // System.Array Instances_Rewalk;
                //  Instances = System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location));
                Instances_Rewalk6 = System.Diagnostics.Process.GetProcessesByName("MyReWalk");// (System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location));
                //      Instances_Rewalk = System.Diagnostics.Process.GetProcessesByName("Rewalk");// (System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location));

                if (Instances_Rewalk6.Length > 1)// || Instances_Rewalk.Length >= 1)
                {
                //    MessageBoxResult result = MessageBox.Show(this, (string)this.FindResource("Another instance of the Rewalk application is already running.") + "\n", (string)this.FindResource("Warning_"), MessageBoxButton.OK, MessageBoxImage.Hand);
                    MessageBoxResult result = MessageBox.Show(this, "Another instance of the MyRewalk application is already running."+ "\n", "Warning", MessageBoxButton.OK, MessageBoxImage.Hand);
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
            }

     /////////////////////////  Translation ///////////////////////////////////////

            private void ReBuildStrings()
            {
                deviceData.Parameters.StairsASCSpeed = deviceData.Parameters.StairsASCSpeed;
                deviceData.Parameters.StairsDSCSpeed = deviceData.Parameters.StairsDSCSpeed;
                deviceData.Parameters.UpperStrapHoldersSize = deviceData.Parameters.UpperStrapHoldersSize;
                deviceData.Parameters.UpperStrapHoldersPosition = deviceData.Parameters.UpperStrapHoldersPosition;
                deviceData.Parameters.AboveKneeBracketSize = deviceData.Parameters.AboveKneeBracketSize;
                deviceData.Parameters.AboveKneeBracketPosition = deviceData.Parameters.AboveKneeBracketPosition;
                deviceData.Parameters.FrontKneeBracketAssemble = deviceData.Parameters.FrontKneeBracketAssemble;
                deviceData.Parameters.FrontKneeBracketAnteriorPosition = deviceData.Parameters.FrontKneeBracketAnteriorPosition;
                deviceData.Parameters.FrontKneeBracketLateralPosition = deviceData.Parameters.FrontKneeBracketLateralPosition;
                deviceData.Parameters.PelvicSize = deviceData.Parameters.PelvicSize;
                deviceData.Parameters.PelvicAnteriorPosition = deviceData.Parameters.PelvicAnteriorPosition;
                deviceData.Parameters.PelvicVerticalPosition = deviceData.Parameters.PelvicVerticalPosition;
                deviceData.Parameters.FootPlateType1 = deviceData.Parameters.FootPlateType1;
                deviceData.Parameters.FootPlateType2 = deviceData.Parameters.FootPlateType2;
                deviceData.Parameters.FootPlateNoOfRotation = deviceData.Parameters.FootPlateNoOfRotation;

            }

            private void English_Checked(object sender, RoutedEventArgs e)
            {
                var myResourceDictionary = new ResourceDictionary();
                myResourceDictionary.Source = new Uri("..\\Translation\\StringResources.xaml", UriKind.Relative);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                Properties.Settings.Default.Language_Selection = "English";
                Properties.Settings.Default.Save();
              //  MuEnglish.IsChecked = false;
                MuDeutsch.IsChecked = false;
                MuFrançais.IsChecked = false;
                MuItaliano.IsChecked = false;
                MuTürkçe.IsChecked = false;
                Mu日本語.IsChecked = false;
                Muespañol.IsChecked = false;
                русский.IsChecked = false;

            }

            private void Deutsch_Checked(object sender, RoutedEventArgs e)
            {

                var myResourceDictionary = new ResourceDictionary();
                myResourceDictionary.Source = new Uri("..\\Translation\\StringResources-deDE.xaml", UriKind.Relative);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                Properties.Settings.Default.Language_Selection = "Deutsch";
                Properties.Settings.Default.Save();
                MuEnglish.IsChecked = false;
                //MuDeutsch.IsChecked = false;
                MuFrançais.IsChecked = false;
                MuItaliano.IsChecked = false;
                MuTürkçe.IsChecked = false;
                Mu日本語.IsChecked = false;
                Muespañol.IsChecked = false;
                русский.IsChecked = false;


            }

            private void Français_Checked(object sender, RoutedEventArgs e)
            {
                var myResourceDictionary = new ResourceDictionary();
                myResourceDictionary.Source = new Uri("..\\Translation\\StringResources-frFR.xaml", UriKind.Relative);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                Properties.Settings.Default.Language_Selection = "Français";
                Properties.Settings.Default.Save();
                MuEnglish.IsChecked = false;
                MuDeutsch.IsChecked = false;
                //MuFrançais.IsChecked = false;
                MuItaliano.IsChecked = false;
                MuTürkçe.IsChecked = false;
                Mu日本語.IsChecked = false;
                Muespañol.IsChecked = false;
                русский.IsChecked = false;

            }

            private void Italiano_Checked(object sender, RoutedEventArgs e)
            {
                var myResourceDictionary = new ResourceDictionary();
                myResourceDictionary.Source = new Uri("..\\Translation\\StringResources-itIT.xaml", UriKind.Relative);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                Properties.Settings.Default.Language_Selection = "Italiano";
                Properties.Settings.Default.Save();
                MuEnglish.IsChecked = false;
                MuDeutsch.IsChecked = false;
                MuFrançais.IsChecked = false;
                //MuItaliano.IsChecked = false;
                MuTürkçe.IsChecked = false;
                Mu日本語.IsChecked = false;
                Muespañol.IsChecked = false;
                русский.IsChecked = false;


            }

            private void Türkçe_Checked(object sender, RoutedEventArgs e)
            {
                var myResourceDictionary = new ResourceDictionary();
                myResourceDictionary.Source = new Uri("..\\Translation\\StringResources-trTR.xaml", UriKind.Relative);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                Properties.Settings.Default.Language_Selection = "Türkçe";
                Properties.Settings.Default.Save();
                MuEnglish.IsChecked = false;
                MuDeutsch.IsChecked = false;
                MuFrançais.IsChecked = false;
                MuItaliano.IsChecked = false;
                //MuTürkçe.IsChecked = false;
                Mu日本語.IsChecked = false;
                Muespañol.IsChecked = false;
                русский.IsChecked = false;

            }

            private void 日本語_Checked(object sender, RoutedEventArgs e)
            {
                var myResourceDictionary = new ResourceDictionary();
                myResourceDictionary.Source = new Uri("..\\Translation\\StringResources-jpJA.xaml", UriKind.Relative);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                Properties.Settings.Default.Language_Selection = "日本語";
                Properties.Settings.Default.Save();
                MuEnglish.IsChecked = false;
                MuDeutsch.IsChecked = false;
                MuFrançais.IsChecked = false;
                MuItaliano.IsChecked = false;
                MuTürkçe.IsChecked = false;
                //Mu日本語.IsChecked = false;
                Muespañol.IsChecked = false;
                русский.IsChecked = false;

            }


            private void Español_Checked(object sender, RoutedEventArgs e)
            {
                var myResourceDictionary = new ResourceDictionary();
                myResourceDictionary.Source = new Uri("..\\Translation\\StringResources-esES.xaml", UriKind.Relative);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                Properties.Settings.Default.Language_Selection = "Español";
                Properties.Settings.Default.Save();
                MuEnglish.IsChecked = false;
                MuDeutsch.IsChecked = false;
                MuFrançais.IsChecked = false;
                MuItaliano.IsChecked = false;
                MuTürkçe.IsChecked = false;
                Mu日本語.IsChecked = false;
                //Muespañol.IsChecked = false;
                русский.IsChecked = false;

            }
            private void Pусский_Checked(object sender, RoutedEventArgs e)
            {
                var myResourceDictionary = new ResourceDictionary();
                myResourceDictionary.Source = new Uri("..\\Translation\\StringResources-ruRU.xaml", UriKind.Relative);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                Properties.Settings.Default.Language_Selection = "Pусский";
                Properties.Settings.Default.Save();
                MuEnglish.IsChecked = false;
                MuDeutsch.IsChecked = false;
                MuFrançais.IsChecked = false;
                MuItaliano.IsChecked = false;
                MuTürkçe.IsChecked = false;
                Mu日本語.IsChecked = false;
                Muespañol.IsChecked = false;
                //русский.IsChecked = false;
    
            }

            public MessageBoxResult MyMessageBox_Show(USER_MESSAGES message_num, List<string> messages, int optioncode, string message_title, MessageBoxButton button, MessageBoxImage image)
            {
                string ss = "", s = "";
                string Trans_message_title = "";

                try
                {
                    if (!string.IsNullOrEmpty(message_title))
                    {
                        Trans_message_title = (string)this.FindResource(message_title);
                    }
                }
                catch (Exception exception)
                {

                    Logger.Show_Selected_MessageBox_Exception("", "(" + message_num.ToString() + "): ", exception.Message);

                    return MessageBoxResult.None;

                }
                foreach (string d in messages)
                {

                    try
                    {

                        if (d != "\n")
                        {

                            s = (string)this.FindResource(d);
                            ss = ss + s;
                        }
                        else
                        {
                            ss = ss + d;
                        }
                    }
                    catch (Exception exception)
                    {

                        Logger.Show_Selected_MessageBox_Exception("", "(" + message_num.ToString() + "): ", exception.Message);
                        return MessageBoxResult.None;
                    }

                }
                if (optioncode != 0)
                {
                    return (MessageBox.Show(ss + " " + optioncode, "(" + message_num.ToString() + "): " + Trans_message_title, button, image));
                }
                else
                {
                    return (MessageBox.Show(ss, "(" + message_num.ToString() + "): " + Trans_message_title, button, image));
                }
            }




            public MessageBoxResult Display_User_Message(USER_MESSAGES message_num)
            {
                MessageBoxResult mess_result = MessageBoxResult.None;

                switch (message_num)
                {
                    case USER_MESSAGES.USER_MESSAGE_0:
                        break;
                    case USER_MESSAGES.USER_MESSAGE_1:
                        TransStringslist.Add("Illegal input please input only digits");
                        TransStringslist.Add("!");
                        mess_result = MyMessageBox_Show(USER_MESSAGES.USER_MESSAGE_1, TransStringslist, 0, "Input Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        TransStringslist.Clear();
                        break;
                    case USER_MESSAGES.USER_MESSAGE_2:
                        TransStringslist.Add("SN inputed is different from system SN");
                        TransStringslist.Add("\n");
                        TransStringslist.Add("Do you want to proceed");
                        TransStringslist.Add("?");
                        mess_result = MyMessageBox_Show(USER_MESSAGES.USER_MESSAGE_2, TransStringslist, 0, "System SN Alert", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                        TransStringslist.Clear();
                        break;
                    case USER_MESSAGES.USER_MESSAGE_3:
                        TransStringslist.Add("Another instance of the application is already running");
                        mess_result = MyMessageBox_Show(USER_MESSAGES.USER_MESSAGE_3, TransStringslist, 0, null, MessageBoxButton.OK, MessageBoxImage.Hand);
                        TransStringslist.Clear();
                        break;
                    case USER_MESSAGES.USER_MESSAGE_4:
                        TransStringslist.Add("Load configuration failed");
                        TransStringslist.Add(".");             
                        MyMessageBox_Show(USER_MESSAGES.USER_MESSAGE_4, TransStringslist, 0, null, MessageBoxButton.OK, MessageBoxImage.Information);
                        TransStringslist.Clear();
                        break;
                    case USER_MESSAGES.USER_MESSAGE_5:
                        TransStringslist.Add("The current boot version is not compatible with this DSP version");
                        TransStringslist.Add("!");
                        TransStringslist.Add("\n");
                        TransStringslist.Add("Please update the boot version first");
                        TransStringslist.Add(".");
                        TransStringslist.Add("\n");
                        TransStringslist.Add("Downloading this DSP version might cause the device to mulfunction.Do you want to continue?");
                        MyMessageBox_Show(USER_MESSAGES.USER_MESSAGE_5, TransStringslist, 0, "INF app Downloading...", MessageBoxButton.YesNoCancel, MessageBoxImage.Error);
                        TransStringslist.Clear();
                        break;
                    case USER_MESSAGES.USER_MESSAGE_6:
                        TransStringslist.Add("You are about to update the INF SW version");
                        TransStringslist.Add("\n");
                        TransStringslist.Add("This will take several seconds");
                        TransStringslist.Add(",");
                        TransStringslist.Add("please do not turn off the system");
                        TransStringslist.Add("!");
                        mess_result = MyMessageBox_Show(USER_MESSAGES.USER_MESSAGE_6, TransStringslist, 0, "INF app Downloading...", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
                        TransStringslist.Clear();
                        break;
                    case USER_MESSAGES.USER_MESSAGE_7:
                        TransStringslist.Add("INF Download finished successfully");
                        TransStringslist.Add("\n");
                        TransStringslist.Add("Please, Restart the system");
                        TransStringslist.Add("!");
                        MyMessageBox_Show(USER_MESSAGES.USER_MESSAGE_7, TransStringslist, 0, "INF app Downloading...", MessageBoxButton.YesNoCancel, MessageBoxImage.Error);
                        TransStringslist.Clear();
                        break;
                    case USER_MESSAGES.USER_MESSAGE_8:
                        TransStringslist.Add("INF application downloading has failed");
                        TransStringslist.Add("!");
                        TransStringslist.Add("\n");
                        TransStringslist.Add("Error Code:");

                        MyMessageBox_Show(USER_MESSAGES.USER_MESSAGE_8, TransStringslist, 0, "INF app Downloading...", MessageBoxButton.YesNoCancel, MessageBoxImage.Error);
                        TransStringslist.Clear();
                        break;
                    case USER_MESSAGES.USER_MESSAGE_9:
                        TransStringslist.Add("This file was created by an older Rewalk interface version and is not compatible to the current version. In order to continue the operation it will be converted to a compatible format and saved.");
                        TransStringslist.Add(".");
                  
                        MyMessageBox_Show(USER_MESSAGES.USER_MESSAGE_9, TransStringslist, 0, "User file Loading" , MessageBoxButton.YesNoCancel, MessageBoxImage.Error);
                        TransStringslist.Clear();
                        break;
                    default:
                        break;
                }

                return mess_result;
            }


          /*  change Save , Close , Open ,load, Stop, Warning to 1"*/

           

            private void RadioButton_Click(object sender, RoutedEventArgs e)
            {
                USER_MESSAGES u = new USER_MESSAGES();

                while ((int)u < 100)
                {
                    Display_User_Message(u);
                    u++;
                }

            }

		
	   private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
       {
           Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        
		}

       private void btnLanguageSelection_Click(object sender, RoutedEventArgs e)
       {
           (sender as Button).ContextMenu.IsEnabled = true;
           (sender as Button).ContextMenu.PlacementTarget = (sender as Button);
           (sender as Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
           (sender as Button).ContextMenu.IsOpen = true;
       }




	}
}

