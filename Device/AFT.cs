﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Rewalk.Device
{
    public enum AFTTestStatus { Unknown, NOTCOMPLETED, PASS, FAIL };
    public class AFTData : INotifyPropertyChanged
    {
        public AFT  CONTWALKPASS
        {
            get { return contwalkpass; }
            set { contwalkpass = value; RaisePropertyChanged("CONTWALKPASS"); }
        }

        public AFT CONTWALKFAIL
        {
            get { return contwalkfail; }
            set { contwalkfail = value; RaisePropertyChanged("CONTWALKFAIL"); }
        }

        public AFT CONTWALKNOTCOMPLETED
        {
            get { return contwalknotcompleted; }
            set { contwalknotcompleted = value; RaisePropertyChanged("CONTWALKNOTCOMPLETED"); }
        }
        public AFT CONTSITSTANDPASS
        {
            get { return contsitstandpass; }
            set { contsitstandpass = value; RaisePropertyChanged("CONTSITSTANDPASS"); }
        }

        public AFT CONTSITSTANDFAIL
        {
            get { return contsitstandfail; }
            set { contsitstandfail = value; RaisePropertyChanged("CONTSITSTANDFAIL"); }
        }

        public AFT CONTSITSTANDNOTCOMPLETED
        {
            get { return contsitstandnotcompleted; }
            set { contsitstandnotcompleted = value; RaisePropertyChanged("CONTSITSTANDNOTCOMPLETED"); }
        }

        public AFT CONTASCPASS
        {
            get { return contASCpass; }
            set { contASCpass = value; RaisePropertyChanged("CONTASCPASS"); }
        }

        public AFT CONTASCFAIL
        {
            get { return contASCfail; }
            set { contASCfail = value; RaisePropertyChanged("CONTASCFAIL"); }
        }

        public AFT CONTASCNOTCOMPLETED
        {
            get { return contASCnotcompleted; }
            set { contASCnotcompleted = value; RaisePropertyChanged("CONTASCNOTCOMPLETED"); }
        }
        public AFT CONTDSCPASS
        {
            get { return contDSCpass; }
            set { contASCpass = value; RaisePropertyChanged("CONTDSCPASS"); }
        }

        public AFT CONTDSCFAIL
        {
            get { return contDSCfail; }
            set { contDSCfail = value; RaisePropertyChanged("CONTDSCFAIL"); }
        }

        public AFT CONTDSCNOTCOMPLETED
        {
            get { return contDSCnotcompleted; }
            set { contDSCnotcompleted = value; RaisePropertyChanged("CONTDSCNOTCOMPLETED"); }
        }

       


    #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion

        private AFT contwalkpass = new AFT();
        private AFT contwalkfail = new AFT();
        private AFT contwalknotcompleted = new AFT();
        private AFT contsitstandpass = new AFT();
        private AFT contsitstandfail = new AFT();
        private AFT contsitstandnotcompleted = new AFT();
        private AFT contASCpass = new AFT();
        private AFT contASCfail = new AFT();
        private AFT contASCnotcompleted = new AFT();
        private AFT contDSCpass = new AFT();
        private AFT contDSCfail = new AFT();
        private AFT contDSCnotcompleted = new AFT();
    }

    public class AFT : INotifyPropertyChanged
    {
        public AFTTestStatus Status
        {
            get { return status; }
            set { status = value; RaisePropertyChanged("Status"); }
        }

    
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion

       
        private AFTTestStatus status = AFTTestStatus.Unknown;
        
    }


        
    
}
