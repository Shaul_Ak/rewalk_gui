﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Rewalk.Device
{
    public enum  LogLevel_Mask { DATA=0x4, WARNINIG=0x2, ERROR=0x1 };
    public class LogLevel : INotifyPropertyChanged
    {
        public LogModule CAN
        {
            get { return can; }
            set { can = value; RaisePropertyChanged("CAN"); }
        }

        public LogModule MCU
        {
            get { return mcu; }
            set { mcu = value; RaisePropertyChanged("MCU"); }
        }

        public LogModule RF
        {
            get { return rf; }
            set { rf = value; RaisePropertyChanged("RF"); }
        }

        public LogModule RC
        {
            get { return rc; }
            set { rc = value; RaisePropertyChanged("RC"); }
        }

        public LogModule ADC
        {
            get { return adc; }
            set { adc = value; RaisePropertyChanged("ADC"); }
        }

        public LogModule STAIRS
        {
            get { return stairs; }
            set { stairs = value; RaisePropertyChanged("ASC_DSC"); }
        }

        public LogModule SIT_STN
        {
            get { return sit_stn; }
            set { sit_stn = value; RaisePropertyChanged("SIT_STN"); }
        }

        public LogModule WALK
        {
            get { return walk; }
            set { walk = value; RaisePropertyChanged("WALK"); }
        }

        public LogModule USB
        {
            get { return usb; }
            set { usb = value; RaisePropertyChanged("USB"); }
        }

        public LogModule BIT
        {
            get { return bit; }
            set { bit = value; RaisePropertyChanged("BIT"); }
        }

        public bool ALL_DATA
        {
            get { return all_data; }
            set {
                    all_data = value;
                    can.DataLevel = value;
                    mcu.DataLevel = value;
                    rf.DataLevel = value;
                    rc.DataLevel = value;
                    adc.DataLevel = value;
                    stairs.DataLevel = value;
                    sit_stn.DataLevel = value;
                    walk.DataLevel = value;
                    usb.DataLevel = value;
                    bit.DataLevel = value;
                    RaisePropertyChanged("ALL_DATA"); 
                }
        }

        public bool ALL_WARNING
        {
            get { return all_warning; }
            set
            {
                all_warning = value;
                can.WarningLevel = value;
                mcu.WarningLevel = value;
                rf.WarningLevel = value;
                rc.WarningLevel = value;
                adc.WarningLevel = value;
                stairs.WarningLevel = value;
                sit_stn.WarningLevel = value;
                walk.WarningLevel = value;
                usb.WarningLevel = value;
                bit.WarningLevel = value;
                RaisePropertyChanged("ALL_WARNING");
            }
        }

        public bool ALL_ERROR
        {
            get { return all_error; }
            set
            {
                all_error = value;
                can.ErrorLevel = value;
                mcu.ErrorLevel = value;
                rf.ErrorLevel = value;
                rc.ErrorLevel = value;
                adc.ErrorLevel = value;
                stairs.ErrorLevel = value;
                sit_stn.ErrorLevel = value;
                walk.ErrorLevel = value;
                usb.ErrorLevel = value;
                bit.ErrorLevel = value;
                RaisePropertyChanged("ALL_ERROR");
            }
        }
      
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion
        private LogModule can = new LogModule();
        private LogModule mcu = new LogModule();
        private LogModule rf = new LogModule();
        private LogModule rc = new LogModule();
        private LogModule adc = new LogModule();
        private LogModule stairs = new LogModule();
        private LogModule sit_stn = new LogModule();
        private LogModule walk = new LogModule();
        private LogModule usb = new LogModule();
        private LogModule bit = new LogModule();
        private bool all_data;
        private bool all_warning;
        private bool all_error;
    }

    public class LogModule : INotifyPropertyChanged
    {
        public bool DataLevel
        {
            get { return dataLevel; }
            set { dataLevel = value; RaisePropertyChanged("DataLevel"); }
        }

        public bool WarningLevel
        {
            get { return warningLevel; }
            set { warningLevel = value; RaisePropertyChanged("WarningLevel"); }
        }

        public bool ErrorLevel
        {
            get { return errorLevel; }
            set { errorLevel = value; RaisePropertyChanged("ErrorLevel"); }
        }

        public short LogLevel
        {
            get {
                    logLevel = 0;
                    logLevel = dataLevel ? (short)((short)LogLevel_Mask.DATA | logLevel) : logLevel;
                    logLevel = warningLevel ? (short)((short)LogLevel_Mask.WARNINIG | logLevel) : logLevel;
                    logLevel = errorLevel ? (short)((short)LogLevel_Mask.ERROR | logLevel) : logLevel;
                    return logLevel; 
                }
            set { 
                    logLevel = value;
                    dataLevel = (short)((short)LogLevel_Mask.DATA & logLevel) > 0; 
                    warningLevel = (short)((short)LogLevel_Mask.WARNINIG & logLevel) > 0;
                    errorLevel = (short)((short)LogLevel_Mask.ERROR & logLevel) > 0;
                    RaisePropertyChanged("LogLevel"); 
                }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion

        //private short errorCode = 0;
        //private TestStatus status = TestStatus.Unknown;
        private bool dataLevel = false;
        private bool warningLevel = false;
        private bool errorLevel = false;
        private short logLevel = 0;
        
    }
}
