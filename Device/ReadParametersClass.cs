﻿using System;
using System.Runtime.InteropServices;

namespace Rewalk.Device
{
    public  class ReadParametersClass
    {

        private static ReadParametersClass ReadInstance = new ReadParametersClass();

        public static ReadParametersClass READINSTANCE
        {
            get { return ReadInstance; }
            set { ReadInstance = value; }
        }


        private ReadParametersClass()
        {
        }

        public DeviceParameters ReadParameters()
        {
            return OnReadParameters(DeviceInterop.ReadDeviceParameters());
        }

        

        public DeviceParameters RestoreParameters()
        {
            return OnReadParameters(DeviceInterop.RestoreDeviceParameters());
        }

        
       private static long ConvertShortToLong(short s1, short s2, short s3, short s4)
        {
            return ((long)(ushort)s1) + ((long)(ushort)s2 << 16) + ((long)(ushort)s3 << 32) + ((long)(ushort)s4 << 48);
        }

      
        private DeviceParameters OnReadParameters(DeviceInterop.DeviceParameters dp) // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions"
        {
            if (dp.DEV_STATUS == Status.Operation_passed)
            {
                DeviceParameters ds = new DeviceParameters();
                ds.KneeAngle = dp.KNEEANGLE;
                ds.HipAngle = dp.HIPANGLE;
                ds.MaxVelocity = dp.MAXVELOCITY;
                ds.BackHipAngle = dp.BACKHIPANGLE;
                ds.TiltDelta = dp.TILTDELTA;
                ds.TiltTimeout = dp.TILTTIMEOUT;
                ds.SwitchFSRs = dp.SWITCHFSRS == 1;
                ds.FSRThreshold = dp.FSRTHRESHOLD;
                ds.FSRWalkTimeout = dp.FSRWALKTIMEOUT;
                ds.DelayBetweenSteps = dp.DELAYBETWEENSTEPS;
                ds.BuzzerBetweenSteps = dp.BUZZERBETWEENSTEPS == 1;
                ds.Record = dp.RECORD == 1;
                ds.SampleTime = dp.SAMPLETIME;
                ds.SafetyWhileStanding = dp.SAFETYWHILESTANDING == 1;
                ds.XThreshold = dp.XTHRESHOLD; ds.YThreshold = dp.YTHRESHOLD;             
                ds.RCAddress = dp.RCADDRESS; ds.RCId = dp.RCID;            
                //Log settings
                ds.LogLevel.CAN.LogLevel = dp.DEV_CANLOGLEVEL; ds.LogLevel.MCU.LogLevel = dp.DEV_MCULOGLEVEL; ds.LogLevel.RF.LogLevel = dp.DEV_RFLOGLEVEL; ds.LogLevel.RC.LogLevel = dp.DEV_RCLOGLEVEL; ds.LogLevel.BIT.LogLevel = dp.DEV_BITLOGLEVEL; ds.LogLevel.OPS.LogLevel = dp.DEV_OperationLogLevel; ds.LogLevel.MEM.LogLevel = dp.DEV_FlashLogLevel;
                ds.LogLevel.ADC.LogLevel = dp.DEV_ADCLOGLEVEL; ds.LogLevel.STAIRS.LogLevel = dp.DEV_STAIRSLOGLEVEL; ds.LogLevel.SIT_STN.LogLevel = dp.DEV_SITSTNLOGLEVEL; ds.LogLevel.WALK.LogLevel = dp.DEV_WALKLOGLEVEL; ds.LogLevel.USB.LogLevel = dp.DEV_USBLOGLEVEL;
                ds.LHAsc = dp.DEV_LHASC; ds.RHAsc = dp.DEV_RHASC; ds.LKAsc = dp.DEV_LKASC; ds.RKAsc = dp.DEV_RKASC; ds.LHDsc = dp.DEV_LHDSC; ds.RHDsc = dp.DEV_RHDSC; ds.LKDsc = dp.DEV_LKDSC; ds.RKDsc = dp.DEV_RKDSC;
                //Versions
                ds.Device = dp.SOFTWAREVERSION; ds.DSP = dp.DSPVERSION; ds.Motor = dp.MOTORVERSION; ds.Remote = dp.RCVERSION; ds.FPGA = dp.FPGAVERSION; ds.Boot = dp.BOOTVERSION;
                //Serial numbers
                ds.Sn1 = dp.DEV_SN1; ds.Sn2 = dp.DEV_SN2; ds.Sn3 = dp.DEV_SN3;
                ds.InfSN = ConvertShortToLong(dp.DEV_INFSN1, dp.DEV_INFSN2, dp.DEV_INFSN3, dp.DEV_INFSN4);
                ds.RcSN = ConvertShortToLong(dp.DEV_RC1, dp.DEV_RC2, dp.DEV_RC3, dp.DEV_RC4);
                ds.LhSN = ConvertShortToLong(dp.DEV_LH1, dp.DEV_LH2, dp.DEV_LH3, dp.DEV_LH4); ds.LkSN = ConvertShortToLong(dp.DEV_LK1, dp.DEV_LK2, dp.DEV_LK3, dp.DEV_LK4);
                ds.RhSN = ConvertShortToLong(dp.DEV_RH1, dp.DEV_RH2, dp.DEV_RH3, dp.DEV_RH4); ds.RkSN = ConvertShortToLong(dp.DEV_RK1, dp.DEV_RK2, dp.DEV_RK3, dp.DEV_RK4);      
                ds.DspSoftwareProgrammed = (dp.DEV_DSPSOFTWAREPROGRAMMED == 0) ? TestStatus.Fail : TestStatus.Success;
                ds.SystemCalbirationStatus = (dp.DEV_SYSTEMCALBIRATIONSTATUS == 0) ? TestStatus.Fail : TestStatus.Success;
                ds.SerialNumberStatus = (dp.DEV_SERIALNUMBERSTATUS == 0) ? TestStatus.Fail : TestStatus.Success;
                ds.Walk_Current_Threshold = dp.DEV_WALKCURRENTTHRESHOLD; ds.Stairs_Current_Threshold = dp.DEV_STAIRSCURRENTTHRESHOLD;                              
                ds.StairsEnabled = 1 == dp.DEV_STAIRSENABLED;
                ds.SysType = (short)dp.DEV_SYSTEMTYPE;
                ds.FirstStepFlexion = dp.DEV_FIRSTSTEPFLEXION;
                ds.SysErrorType = dp.DEV_SYSTEMERRORTYPE;
                ds.StepCounter = dp.DEV_STEPCOUNTER;
                ds.EnableSystemStuckOnError_int = dp.DEV_ENABLESYSTEMSTUCKONERROR;
                ds.SwitchingTime = dp.DEV_SWITCHINGTIME;
                ds.AFTSitCounter = dp.DEV_AFTSitCounter;
                ds.AFTASCCounter = dp.DEV_AFTASCCounter;
                ds.AFTDSCCounter = dp.DEV_AFTDSCCounter;
                ds.AFTSitStatus = dp.DEV_AFTSitStatus;
                ds.AFTASCStatus = dp.DEV_AFTASCStatus;
                ds.AFTDSCStatus = dp.DEV_AFTDSCStatus;
                ds.AFTWalkStatus = dp.DEV_AFTWalkStatus;
                ds.StairsASCSpeed = dp.DEV_StairsASCSpeed;
                ds.StairsDSCSpeed = dp.DEV_StairsDSCSpeed ; 
                ds.Placing_RH_on_the_stair = dp.DEV_Placing_RH_on_the_stair ;
                ds.Placing_RK_on_the_stair = dp.DEV_Placing_RK_on_the_stair;
                ds.StepLength = dp.DEV_StepLength;
                ds.StairCounter = dp.DEV_StairCounter;
                ds.CurrentHandleAlgo = 1 == dp.DEV_CurrentHandleAlgo;//Saman
                ds.UpperLegLength = dp.DEV_UpperLegLength;
                ds.LowerLegLength = dp.DEV_LowerLegLength;
                ds.SoleHeight     =dp.DEV_SoleHeight;
                ds.ShoeSize       = dp.DEV_ShoeSize;
                ds.StairHeight    =  dp.DEV_StairHeight;
                ds.PicSWVersion = dp.DEV_PicSWVersion;
                //New GUI6 Look and Feel design
                ds.UnitsSelection = dp.DEV_UnitsSelection;
                ds.Gender = dp.DEV_Gender == (short) 1;
                ds.User_ID = dp.DEV_User_ID;
                ds.Weight = dp.DEV_Weight;
                ds.Height = dp.DEV_Height;
                ds.UnitsSelection = dp.DEV_UnitsSelection;
                ds.UpperStrapHoldersSize = dp.DEV_UpperStrapHoldersSize;
                ds.UpperStrapHoldersPosition = dp.DEV_UpperStrapHoldersPosition;
                ds.AboveKneeBracketSize = dp.DEV_AboveKneeBracketSize;
                ds.AboveKneeBracketPosition = dp.DEV_AboveKneeBracketPosition;
                ds.FrontKneeBracketAssemble = dp.DEV_FrontKneeBracketAssemble;
                ds.FrontKneeBracketAnteriorPosition = dp.DEV_FrontKneeBracketAnteriorPosition;
                ds.FrontKneeBracketLateralPosition = dp.DEV_FrontKneeBracketLateralPosition;
                ds.PelvicSize = dp.DEV_PelvicSize;
                ds.PelvicAnteriorPosition = dp.DEV_PelvicAnteriorPosition;
                ds.PelvicVerticalPosition = dp.DEV_PelvicVerticalPosition;
                ds.UpperLegHightSize = dp.DEV_UpperLegHightSize;
                ds.LowerLegHightSize = dp.DEV_LowerLegHightSize;
                ds.FootPlateType1 = dp.DEV_FootPlateType1;
                ds.FootPlateType2 = dp.DEV_FootPlateType2;
                ds.FootPlateNoOfRotation = dp.DEV_FootPlateNoOfRotation;
                ds.AFTStopsStatus = dp.DEV_AFTStopsStatus;
                ds.INFBatteriesLevel = dp.DEV_INFBatteriesLevel;
                ds.Ack_Movement_Point = dp.DEV_Ack_Movement_Point;
                ds.Enable_First_Step_Rewalk6 = dp.DEV_Enable_First_Step_Rewalk6 == (short)1;
                ds.SystemResrvedParmeter5 = dp.DEV_SystemResrvedParmeter5;
                ds.SystemResrvedParmeter6 = dp.DEV_SystemResrvedParmeter6;
                ds.SystemResrvedParmeter7 = dp.DEV_SystemResrvedParmeter7;
                ds.SystemResrvedParmeter8 = dp.DEV_SystemResrvedParmeter8;
                ds.SystemResrvedParmeter9 = dp.DEV_SystemResrvedParmeter9;
                ds.SystemResrvedParmeter10 = dp.DEV_SystemResrvedParmeter10;


                return ds;
            }
            else

            { 
                throw new DeviceOperationException(DeviceManager.INSTANCE.Status2String(dp.DEV_STATUS)); }


        }

        
       
        

    }
}

