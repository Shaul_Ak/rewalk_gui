﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rewalk.Device
{

    public class SerialNumbers_Class
    {
        private static SerialNumbers_Class SerialNumInstance = new SerialNumbers_Class();

        public static SerialNumbers_Class SN_INSTANCE
        {
            get { return SerialNumInstance; }
            set { SerialNumInstance = value; }
        }
        private SerialNumbers_Class()
        {
        }
        public void WriteSerialNumbers(DeviceParameters ds)
        {
            DeviceInterop.SerialNumbers sn = new DeviceInterop.SerialNumbers();

            //DeviceManagerServicesPart2.DEV_SERVICES2.WriteSysSerialNumbers(ds,sn);
            sn.M_SN1 = ds.Sn1;
            sn.M_SN2 = ds.Sn2;
            sn.M_SN3 = ds.Sn3;

            sn.M_INFSN1 = (short)ds.InfSN;
            sn.M_INFSN2 = (short)(ds.InfSN >> 16);
            sn.M_INFSN3 = (short)(ds.InfSN >> 32);
            sn.M_INFSN4 = (short)(ds.InfSN >> 48);

            sn.M_RCSN1 = (short)ds.RcSN;
            sn.M_RCSN2 = (short)(ds.RcSN >> 16);
            sn.M_RCSN3 = (short)(ds.RcSN >> 32);
            sn.M_RCSN4 = (short)(ds.RcSN >> 48);

            sn.M_LHSN1 = (short)ds.LhSN;
            sn.M_LHSN2 = (short)(ds.LhSN >> 16);
            sn.M_LHSN3 = (short)(ds.LhSN >> 32);
            sn.M_LHSN4 = (short)(ds.LhSN >> 48);

            sn.M_RHSN1 = (short)ds.RhSN;
            sn.M_RHSN2 = (short)(ds.RhSN >> 16);
            sn.M_RHSN3 = (short)(ds.RhSN >> 32);
            sn.M_RHSN4 = (short)(ds.RhSN >> 48);

            sn.M_LKSN1 = (short)ds.LkSN;
            sn.M_LKSN2 = (short)(ds.LkSN >> 16);
            sn.M_LKSN3 = (short)(ds.LkSN >> 32);
            sn.M_LKSN4 = (short)(ds.LkSN >> 48);

            sn.M_RKSN1 = (short)ds.RkSN;
            sn.M_RKSN2 = (short)(ds.RkSN >> 16);
            sn.M_RKSN3 = (short)(ds.RkSN >> 32);
            sn.M_RKSN4 = (short)(ds.RkSN >> 48);


            Status status = DeviceInterop.WriteSerialNumbers(sn);
            if (status != Status.Operation_passed)
            {
            
                throw new DeviceOperationException(DeviceManager.INSTANCE.Status2String(status));
            }
        }
    }
}
