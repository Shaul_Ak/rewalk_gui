﻿using System;
using System.Runtime.Serialization;

namespace Rewalk.Device
{
    [Serializable]
    public class DeviceOperationException : Exception
    {
        public  DeviceOperationException(string message)
            : base(message)
        {
          
        }

        public DeviceOperationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected DeviceOperationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
