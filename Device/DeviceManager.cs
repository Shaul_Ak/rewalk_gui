﻿using System;
using System.Runtime.InteropServices;

namespace Rewalk.Device
{
    internal sealed class DeviceManager
    {

        private static DeviceManager Instance = new DeviceManager();

        public static DeviceManager INSTANCE
        {
            get { return Instance; }
            set { Instance = value; }
        }


        private DeviceManager()
        {
        }


       
        

        public void WriteRCAddressandID(ushort address, ushort id)
        {
            Status status = DeviceInterop.WriteRCAddressID(address, id);
            if (status != Status.Operation_passed)
            {
                throw new DeviceOperationException(Status2String(status));
            }
        }

        public DeviceInterop.RCAddressIDData RequestWriteRCAddressandIDStatus()
        {
            DeviceClassInterface MessageWindow = new DeviceClassInterface();
            DeviceInterop.RCAddressIDData td = DeviceInterop.GetWriteRCAddressIDStatus();
            if (td.STATUS != Status.Operation_passed)
            {
                throw new DeviceOperationException(Status2String(td.STATUS));
            }

            return td;
        }

        public void WriteINFAddressandID(ushort address, ushort id)
        {
            Status status = DeviceInterop.WriteINFAddressID(address, id);
            if (status != Status.Operation_passed)
            {
                throw new DeviceOperationException(Status2String(status));
            }
        }

        public void WriteINFAddress(ushort address)
        {
            Status status = DeviceInterop.WriteINFAddress(address);
            if (status != Status.Operation_passed)
            {
               throw new DeviceOperationException(Status2String(status));
            }
        }

        public void WriteINFId(ushort id)
        {
            Status status = DeviceInterop.WriteINFId(id);
            if (status != Status.Operation_passed)
            {
                throw new DeviceOperationException(Status2String(status));

            }
        }

        public void DownloadDSP(byte[] data)
        {
            Status status = DeviceInterop.Download(data, data.Length, DeviceInterop.DOWNLOAD_DSP);
            if (status != Status.Operation_passed)
            {
                throw new DeviceOperationException(Status2String(status));
            };
        }

        public void DownloadMCU(byte[] data)
        {
            Status status = DeviceInterop.Download(data, data.Length, DeviceInterop.DOWNLOAD_MCU);
            if (status != Status.Operation_passed)
            {
               throw new DeviceOperationException(Status2String(status));

            };
        }


        public void DownloadRC(byte[] data)
        {
            Status status = DeviceInterop.Download(data, data.Length, DeviceInterop.DOWNLOAD_RC);
            if (status != Status.Operation_passed)
            {
                throw new DeviceOperationException(Status2String(status));
            };
        }

        public byte[] RunCustomCommand(int command, byte[] data)
        {

            try
            {
                DeviceInterop.CustomCommandResult result = DeviceInterop.RunCustomCommand(data, data == null ? 0 : data.Length, DeviceInterop.COMMAND1 + command - 1);
                if (result.STATUS != Status.Operation_passed)
                {
                    throw new DeviceOperationException(Status2String(result.STATUS));
                }
                else
                {
                    return new byte[10] { result.ID1, result.ID2, result.BYTE1, result.BYTE2, result.BYTE3, result.BYTE4, result.BYTE5, result.BYTE6, result.BYTE7, result.BYTE8 };
                }
            }
            catch(Exception exception)
            {
               
                
                return new byte[10];
                throw new DeviceOperationException(exception.Message);
            }

        }


        


        public void RFStartTest()
        {
            Status status = DeviceInterop.RFStartTest();
            if (status != Status.Operation_passed)
            {
               throw new DeviceOperationException(Status2String(status));
            }
        }

        public void ResetStepCounter()
        {
            Status status = DeviceInterop.ResetStepCounter();
            if (status != Status.Operation_passed)
            { 
               throw new DeviceOperationException(Status2String(status));
            }
        }
        public string ClearlogFile()//Saman
        {
            DeviceClassInterface MessageWindow = new DeviceClassInterface();
            Status status = DeviceInterop.ClearlogFile();
            if (status != Status.Operation_passed)
            {
                throw new DeviceOperationException(Status2String(status));
            }
        
            return Status2String(status);
        }
        

        public void ResetStairCounter()
        {
            Status status = DeviceInterop.ResetStairCounter();
            if (status != Status.Operation_passed)
            {
                throw new DeviceOperationException(Status2String(status));
            }
        }
        public UInt32 ReadStepCounter()
        {
            DeviceInterop.StepCounterData td = DeviceInterop.ReadStepCounter();
            if (td.STATUS != Status.Operation_passed)
            {
                throw new DeviceOperationException(Status2String(td.STATUS));
            }
            return td.STEPCOUNTER;
        }
        public UInt32 ReadStairCounter()
        {
            DeviceInterop.StairCounterData td = DeviceInterop.ReadStairCounter();
            if (td.STATUS != Status.Operation_passed)
            {
                throw new DeviceOperationException(Status2String(td.STATUS));
            }
            return td.STAIRCOUNTER;
        }



        public DeviceInterop.RFTestData RequestRFTestStatus()
        {
            DeviceInterop.RFTestData td = DeviceInterop.GetRFTestStatus();
            if (td.STATUS != Status.Operation_passed)
            {
                throw new DeviceOperationException(Status2String(td.STATUS));
   
            }
            return td;//saman
        }

       

      

       

        
        public void SaveLogFile(string file, bool sendDone)
        {
            IntPtr ptrFile = Marshal.StringToCoTaskMemAnsi(file);
            try
            {

                Status status = DeviceInterop.SaveLogFile(ptrFile, sendDone);
                if (status != Status.Operation_passed)
                {
                   throw new DeviceOperationException(Status2String(status));
                }
            }
            finally
            {
                Marshal.FreeCoTaskMem(ptrFile);
            }
        }
        public void ClearSystemError()
        {

            Status status = DeviceInterop.ClearSystemError();
            if (status != Status.Operation_passed)
            {
               throw new DeviceOperationException(Status2String(status));
            }

        }


        public void SaveFlashLogFile(string file)
        {
            IntPtr ptrFile = Marshal.StringToCoTaskMemAnsi(file);
            try
            {
                Status status = DeviceInterop.SaveFlashLogFile(ptrFile);
                if (status != Status.Operation_passed)
                {
                    throw new DeviceOperationException(Status2String(status));
                }
            }
            finally
            {
                Marshal.FreeCoTaskMem(ptrFile);
            }
        }

        public void ShutDown()
        {
            DeviceInterop.ShutDown();
        }

        public bool CheckUSBConnected()
        {
            return DeviceInterop.CheckUSBConnected();// DeviceManager.Instance.RequestWriteRCAddressandIDStatus();

        }

       /* public bool SendDebugCommand(string command)
        {

            return true;
        }

        */



        public DeviceInterop.SystemErrorData RequestSystemErrorStatus()
        {
            DeviceInterop.SystemErrorData td = DeviceInterop.GetSystemErrorStatus();
            if (td.STATUS != Status.Operation_passed)
            {
               throw new DeviceOperationException(Status2String(td.STATUS));
            }
            return td;//saman
        }
       
       
        public string Status2String(Status status)
        {
            string st = Enum.GetName(typeof(Status), status);
            return st.Replace('_', ' ');
        }
    } 
}
