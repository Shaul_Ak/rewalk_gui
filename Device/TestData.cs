﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Rewalk.Device
{
    public enum TestStatus { Unknown, Fail, InProgress, Success };
    public enum SystemType { Sys_Unknown = 0,  Rewalk_I = 2, Rewalk_P0 = 3, Rewalk_R = 4, Rewalk_P6 = 5 };
    public class TestData : INotifyPropertyChanged
    {
        public TestStatus INF
        {
            get { return inf; }
            set { inf = value; RaisePropertyChanged("INF"); }
        }

        public short ErrorCodeINF
        {
            get { return errorCodeINF; }
            set
            {
                INF = CodeShortToTestStatus(value);
                errorCodeINF = (INF == TestStatus.Fail) ? value : (short)0; 
                RaisePropertyChanged("ErrorCodeINF"); 
            }
        }

        public TestStatus RC
        {
            get { return rc; }
            set { rc = value; RaisePropertyChanged("RC"); }
        }

        public short ErrorCodeRC
        {
            get { return errorCodeRC; }
            set
            {
                RC = CodeShortToTestStatus(value);
                errorCodeRC = (RC == TestStatus.Fail) ? value : (short)0;
                RaisePropertyChanged("ErrorCodeRC");
            }
        }

        public TestStatus LH
        {
            get { return lh; }
            set { lh = value; RaisePropertyChanged("LH"); }
        }

        public short ErrorCodeLH
        {
            get { return errorCodeLH; }
            set
            {
                LH = CodeShortToTestStatus(value);
                errorCodeLH = (LH == TestStatus.Fail) ? value : (short)0;
                RaisePropertyChanged("ErrorCodeLH");
            }
        }

        public TestStatus LK
        {
            get { return lk; }
            set { lk = value; RaisePropertyChanged("LK"); }
        }

        public short ErrorCodeLK
        {
            get { return errorCodeLK; }
            set
            {
                LK = CodeShortToTestStatus(value);
                errorCodeLK = (LK == TestStatus.Fail) ? value : (short)0;
                RaisePropertyChanged("ErrorCodeLK");
            }
        }

        public TestStatus RH
        {
            get { return rh; }
            set { rh = value; RaisePropertyChanged("RH"); }
        }

        public short ErrorCodeRH
        {
            get { return errorCodeRH; }
            set
            {
                RH = CodeShortToTestStatus(value);
                errorCodeRH = (RH == TestStatus.Fail) ? value : (short)0; 
                RaisePropertyChanged("ErrorCodeRH"); 
            }
        }

        public TestStatus RK
        {
            get { return rk; }
            set { rk = value; RaisePropertyChanged("RK"); }
        }

        public short ErrorCodeRK
        {
            get { return errorCodeRK; }
            set
            {
                RK = CodeShortToTestStatus(value);
                errorCodeRK = (RK == TestStatus.Fail) ? value : (short)0;
                RaisePropertyChanged("ErrorCodeRK");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion

        internal static TestStatus CodeShortToTestStatus(short code)
        {
            if (code == 0)
                return TestStatus.InProgress;
            else if ((ushort)code == 0xFFFF)
            {
                return TestStatus.Success;
            }
            else
                return TestStatus.Fail;
        }

        private TestStatus inf = TestStatus.Unknown;
        private TestStatus rc = TestStatus.Unknown;
        private TestStatus lh = TestStatus.Unknown;
        private TestStatus lk = TestStatus.Unknown;
        private TestStatus rh = TestStatus.Unknown;
        private TestStatus rk = TestStatus.Unknown;

        private short errorCodeINF = 0;
        private short errorCodeRC = 0;
        private short errorCodeLH = 0;
        private short errorCodeLK = 0;
        private short errorCodeRH = 0;
        private short errorCodeRK = 0;
    }
}
