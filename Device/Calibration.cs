﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Rewalk.Device
{
    public class CalibrationData : INotifyPropertyChanged
    {
        public Calibration LH
        {
            get { return lh; }
            set { lh = value; RaisePropertyChanged("LH"); }
        }

        public Calibration LK
        {
            get { return lk; }
            set { lk = value; RaisePropertyChanged("LK"); }
        }

        public Calibration RH
        {
            get { return rh; }
            set { rh = value; RaisePropertyChanged("RH"); }
        }

        public Calibration RK
        {
            get { return rk; }
            set { rk = value; RaisePropertyChanged("RK"); }
        }

       


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion

        private Calibration lh = new Calibration();
        private Calibration lk = new Calibration();
        private Calibration rh = new Calibration();
        private Calibration rk = new Calibration();
     
    }

    public class Calibration : INotifyPropertyChanged
    {
        public TestStatus Status
        {
            get { return status; }
            set { status = value; RaisePropertyChanged("Status"); }
        }

        public short ErrorCode
        {
            get { return errorCode; }
            set
            {
                Status = TestData.CodeShortToTestStatus(value);
                errorCode = (Status == TestStatus.Fail) ? value : (short)0;
                RaisePropertyChanged("ErrorCode");
            }
        }

        public bool IsChecked
        {
            get { return isChecked; }
            set { isChecked = value; RaisePropertyChanged("IsChecked"); }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion

        private short errorCode = 0;
        private TestStatus status = TestStatus.Unknown;
        private bool isChecked = false;
    }
}
