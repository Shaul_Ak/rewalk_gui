﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Rewalk.Device
{
    enum Status : short		// error values
    {
        Operation_passed = 0,
        Unsupported_command,
        IO_write_USBCB_failed,
        IO_read_USBCB_failed,
        IO_read_data_failed,
        IO_write_data_failed,
        Out_of_memory_on_host,
        Error_opening_file,
        Error_reading_file,
        No_available_file_ptrs,
        Could_not_connect,
      
    };

    public enum System_Components
    {
        INFRASTRUCTURE,
        REMOTE_CONTROL,
        LEFT_HIP,
        LEFT_KNEE,
        RIGHT_HIP,
        RIGHT_KNEE,
        SYSTEM_SN,
    };

    
   
    internal sealed class DeviceInterop
    {
        
        [StructLayout(LayoutKind.Sequential)]
        public struct SerialNumbers
        {
            private  short sn1;
            private  short sn2;
            private  short sn3;
            private  short infSN1;
            private  short infSN2;
            private  short infSN3;
            private  short infSN4;
            private short rcSN1;
            private short rcSN2;
            private short rcSN3;
            private short rcSN4;
            private short lhSN1;
            private short lhSN2;
            private short lhSN3;
            private short lhSN4;
            private short lkSN1;
            private short lkSN2;
            private short lkSN3;
            private short lkSN4;
            private short rhSN1;
            private short rhSN2;
            private short rhSN3;
            private short rhSN4;
            private short rkSN1;
            private short rkSN2;
            private short rkSN3;
            private short rkSN4;

            public short M_SN1
             {
               get { return sn1;  }
               set { sn1 = value; }
             }
            public short M_SN2
             {
               get { return sn2;  }
               set { sn2 = value; }
             }
            public short M_SN3
            {
                get { return sn3; }
                set { sn3 = value; }
            }
            public short   M_INFSN1
            {
                get { return ( infSN1); }
                set { ( infSN1) = value; }
            }
            public short M_INFSN2
            {
                get { return infSN2; }
                set { infSN2 = value; }
            }
            public short M_INFSN3
            {
                get { return infSN3; }
                set { infSN3 = value; }
            }
            public short M_INFSN4
            {
                get { return infSN4; }
                set { infSN4 = value; }
            }
            public short M_RCSN1
            {
                get { return rcSN1; }
                set { rcSN1 = value; }
            }
            public short M_RCSN2
            {
                get { return rcSN2; }
                set { rcSN2 = value; }
            }
            public short M_RCSN3
            {
                get { return rcSN3; }
                set { rcSN3 = value; }
            }
            public short M_RCSN4
            {
                get { return rcSN4; }
                set { rcSN4 = value; }
            }
            public short M_LHSN1
            {
                get { return lhSN1; }
                set { lhSN1 = value; }
            }
            public short M_LHSN2
            {
                get { return lhSN2; }
                set { lhSN2 = value; }
            }
            public short M_LHSN3
            {
                get { return lhSN3; }
                set { lhSN3 = value; }
            }
            public short M_LHSN4
            {
                get { return lhSN4; }
                set { lhSN4 = value; }
            }
            public short M_LKSN1
            {
                get { return lkSN1; }
                set { lkSN1 = value; }
            }
            public short M_LKSN2
            {
                get { return lkSN2; }
                set { lkSN2 = value; }
            }
            public short M_LKSN3
            {
                get { return lkSN3; }
                set { lkSN3 = value; }
            }
            public short M_LKSN4
            {
                get { return lkSN4; }
                set { lkSN4 = value; }
            }
           
            public short M_RHSN1
            {
                get { return rhSN1; }
                set { rhSN1 = value; }
            }
            public short M_RHSN2
            {
                get { return rhSN2; }
                set { rhSN2 = value; }
            }
            public short M_RHSN3
            {
                get { return rhSN3; }
                set { rhSN3 = value; }
            }
            public short M_RHSN4
            {
                get { return rhSN4; }
                set { rhSN4 = value; }
            }
            public short M_RKSN1
            {
                get { return rkSN1; }
                set { rkSN1 = value; }
            }
            public short M_RKSN2
            {
                get { return rkSN2; }
                set { rkSN2 = value; }
            }
            public short M_RKSN3
            {
                get { return rkSN3; }
                set { rkSN3 = value; }
            }
            public short M_RKSN4
            {
                get { return rkSN4; }
                set { rkSN4 = value; }
            } 
        }





        [StructLayout(LayoutKind.Sequential)]
        public struct DeviceParameters
        {
            private  short kneeAngle;
            private  short hipAngle;
            private  short backHipAngle;
            private  short maxVelocity;
            private  short tiltDelta;
            private  short tiltTimeout;
            private  short safetyWhileStanding;
            private  short xThreshold;
            private  short yThreshold;
            private  short delayBetweenSteps;
            private  short buzzerBetweenSteps;
            private  short record;
            private  short sampleTime;
            private  ushort rcAddress;
            private  ushort rcID;
            private  short softwareVersion;
            private  short dspVersion;
            private  short motorVersion;
            private  short rcVersion;
            private  short fpgaVersion;
            private  short bootVersion;
            private  short switchFSRs;
            private  short fsrThreshold;
            private  short fsrWalkTimeout;
            private  short lhAsc;
            private  short lkAsc;
            private  short rhAsc;
            private  short rkAsc;
            private  short lhDsc;
            private  short lkDsc;
            private  short rhDsc;
            private  short rkDsc;
            private  short dspSoftwareProgrammed;
            private  short systemCalbirationStatus;
            private  short serialNumberStatus;
            private  short sn1;
            private  short sn2;
            private  short sn3;
            private short infSN1;
            private short infSN2;
            private short infSN3;
            private short infSN4;
            private short rcSN1;
            private short rcSN2;
            private short rcSN3;
            private short rcSN4;
            private short lhSN1;
            private short lhSN2;
            private short lhSN3;
            private short lhSN4;
            private short lkSN1;
            private short lkSN2;
            private short lkSN3;
            private short lkSN4;
            private short rhSN1;
            private short rhSN2;
            private short rhSN3;
            private short rhSN4;
            private short rkSN1;
            private short rkSN2;
            private short rkSN3;
            private short rkSN4;
            private short WalkCurrentThreshold;
            private short StairsCurrentThreshold;
            private short StairsEnabled;
            private short SysType;
            private short CANLogLevel;
            private short MCULogLevel;
            private short RFLogLevel;
            private short RCLogLevel;
            private short ADCLogLevel;
            private short StairsLogLevel;
            private short SitStnLogLevel;
            private short WalkLogLevel;
            private short USBLogLevel;
            private short BITLogLevel;
            private short OperationLogLevel;
            private short FlashLogLevel;
            private short FirstStepFlexion;
            private short SystemErrorType;
	        private UInt32 StepCounter;
            private UInt32 EnableSystemStuckOnError_int;
            private short SwitchingTime;
            private short AFTSitCounter;
            private short AFTASCCounter;
            private short AFTDSCCounter;
            private short AFTSitStatus;
            private short AFTASCStatus;
            private short AFTDSCStatus;
            private short AFTWalkStatus;
            private short StairsASCSpeed;
            private short StairsDSCSpeed;
            private short Placing_RH_on_the_stair;
            private short Placing_RK_on_the_stair;
            private short StepLength;
            private UInt32 StairCounter;
            private short CurrentHandleAlgo;
            private short UpperLegLength;
            private short LowerLegLength;
            private short SoleHeight;
            private short ShoeSize;
            private short StairHeight;
            private short PicSWVersion;

	        //New GUI6 Look and Feel design
	        private short  UnitsSelection;
	        private short  Gender;
	        private UInt32 User_ID;
	        private short  Weight;
	        private short  Height;
	        private short  UpperStrapHoldersSize;
	        private short  UpperStrapHoldersPosition;
	        private short  AboveKneeBracketSize;
	        private short  AboveKneeBracketPosition;
	        private short  FrontKneeBracketAssemble;
	        private short  FrontKneeBracketAnteriorPosition;
	        private short  FrontKneeBracketLateralPosition;
	        private short  PelvicSize;
	        private short  PelvicAnteriorPosition;
	        private short  PelvicVerticalPosition;
	        private short  UpperLegHightSize;
	        private short  LowerLegHightSize;
	        private short  FootPlateType1;
	        private short  FootPlateType2;
            private UInt32 FootPlateNoOfRotation;
            private short AFTStopsStatus;
            private short INFBatteriesLevel;
            private short Ack_Movement_Point;
            private short Enable_First_Step_Rewalk6;
            private short SystemResrvedParmeter5;
            private short SystemResrvedParmeter6;
            private short SystemResrvedParmeter7;
            private short SystemResrvedParmeter8;
            private short SystemResrvedParmeter9;
            private short SystemResrvedParmeter10;
           
            private Status status;


            public short KNEEANGLE
            {
                get { return kneeAngle; }
                set { kneeAngle = value; }
            }
            public short HIPANGLE
            {
                get { return hipAngle; }
                set { hipAngle = value; }
            }
            public short BACKHIPANGLE
            {
                get { return backHipAngle; }
                set { backHipAngle = value; }
            }
            public short MAXVELOCITY
            {
                get { return maxVelocity; }
                set { maxVelocity = value; }
            }
            public short TILTDELTA
            {
                get { return tiltDelta; }
                set { tiltDelta = value; }
            }
            public short TILTTIMEOUT
            {
                get { return tiltTimeout; }
                set { tiltTimeout = value; }
            }
            public short SAFETYWHILESTANDING
            {
                get { return safetyWhileStanding; }
                set { safetyWhileStanding = value; }
            }
            public short XTHRESHOLD
            {
                get { return xThreshold; }
                set { xThreshold = value; }
            }
            public short YTHRESHOLD
            {
                get { return yThreshold; }
                set { yThreshold = value; }
            }
            public short DELAYBETWEENSTEPS
            {
                get { return delayBetweenSteps; }
                set { delayBetweenSteps = value; }
            }
            public short BUZZERBETWEENSTEPS
            {
                get { return buzzerBetweenSteps; }
                set { buzzerBetweenSteps = value; }
            }
            public short RECORD
            {
                get { return record; }
                set { record = value; }
            }
            public short SAMPLETIME
            {
                get { return sampleTime; }
                set { sampleTime = value; }
            }
            public ushort RCADDRESS
            {
                get { return rcAddress; }
                set { rcAddress = value; }
            }
            public ushort RCID
            {
                get { return rcID; }
                set { rcID = value; }
            }
            public short SOFTWAREVERSION
            {
                get { return softwareVersion; }
                set { softwareVersion = value; }
            }
            public short DSPVERSION
            {
                get { return dspVersion; }
                set { dspVersion = value; }
            }
            public short MOTORVERSION
            {
                get { return motorVersion; }
                set { motorVersion = value; }
            }
            public short RCVERSION
            {
                get { return rcVersion; }
                set { rcVersion = value; }
            }
            public short FPGAVERSION
            {
                get { return fpgaVersion; }
                set { fpgaVersion = value; }
            }
            public short BOOTVERSION
            {
                get { return bootVersion; }
                set { bootVersion = value; }
            }
            public short SWITCHFSRS
            {
                get { return switchFSRs; }
                set { switchFSRs = value; }
            }
            public short FSRTHRESHOLD
            {
                get { return fsrThreshold; }
                set { fsrThreshold = value; }
            }
            public short FSRWALKTIMEOUT
            {
                get { return fsrWalkTimeout; }
                set { fsrWalkTimeout = value; }
            }
            public short DEV_LHASC
            {
                get { return lhAsc; }
                set { lhAsc = value; }
            }
            public short DEV_RHASC
            {
                get { return rhAsc; }
                set { rhAsc = value; }
            }
            public short DEV_LKASC
            {
                get { return lkAsc; }
                set { lkAsc = value; }
            }
            public short DEV_RKASC
            {
                get { return rkAsc; }
                set { rkAsc = value; }
            }
            public short DEV_LHDSC
            {
                get { return lhDsc; }
                set { lhDsc = value; }
            }
            public short DEV_RHDSC
            {
                get { return rhDsc; }
                set { rhDsc = value; }
            }
            public short DEV_LKDSC
            {
                get { return lkDsc; }
                set { lkDsc = value; }
            }
            public short DEV_RKDSC
            {
                get { return rkDsc; }
                set { rkDsc = value; }
            }
            public short DEV_DSPSOFTWAREPROGRAMMED
            {
                get { return dspSoftwareProgrammed; }
                set { dspSoftwareProgrammed = value; }
            }
            public short DEV_SYSTEMCALBIRATIONSTATUS
            {
                get { return systemCalbirationStatus; }
                set { systemCalbirationStatus = value; }
            }
            public short DEV_SERIALNUMBERSTATUS
            {
                get { return serialNumberStatus; }
                set { serialNumberStatus = value; }
            }       
            public short DEV_WALKCURRENTTHRESHOLD
            {
                get { return WalkCurrentThreshold; }
                set { WalkCurrentThreshold = value; }
            }
            public short DEV_STAIRSCURRENTTHRESHOLD
            {
                get { return StairsCurrentThreshold; }
                set { StairsCurrentThreshold = value; }
            }
            public short DEV_STAIRSENABLED
            {
                get { return StairsEnabled; }
                set { StairsEnabled = value; }
            }
            public short DEV_SYSTEMTYPE
            {
                get { return SysType; }
                set { SysType = value; }
            }
            public short DEV_CANLOGLEVEL
            {
                get { return CANLogLevel; }
                set { CANLogLevel = value; }
            }
            public short DEV_MCULOGLEVEL
            {
                get { return MCULogLevel; }
                set { MCULogLevel = value; }
            }
            public short DEV_RFLOGLEVEL
            {
                get { return RFLogLevel; }
                set { RFLogLevel = value; }
            }
            public short DEV_RCLOGLEVEL
            {
                get { return RCLogLevel; }
                set { RCLogLevel = value; }
            }
            public short DEV_ADCLOGLEVEL
            {
                get { return ADCLogLevel; }
                set { ADCLogLevel = value; }
            }
            public short DEV_STAIRSLOGLEVEL
            {
                get { return StairsLogLevel; }
                set { StairsLogLevel = value; }
            }
            public short DEV_SITSTNLOGLEVEL
            {
                get { return SitStnLogLevel; }
                set { SitStnLogLevel = value; }
            }
            public short DEV_WALKLOGLEVEL
            {
                get { return WalkLogLevel; }
                set { WalkLogLevel = value; }
            }
            public short DEV_USBLOGLEVEL
            {
                get { return USBLogLevel; }
                set { USBLogLevel = value; }
            }
            public short DEV_BITLOGLEVEL
            {
                get { return BITLogLevel; }
                set { BITLogLevel = value; }
            }
            public short DEV_OperationLogLevel
            {
                get { return OperationLogLevel; }
                set { OperationLogLevel = value; }
            }
            public short DEV_FlashLogLevel
            {
                get { return FlashLogLevel; }
                set { FlashLogLevel = value; }
            }
            public short DEV_FIRSTSTEPFLEXION
            {
                get { return FirstStepFlexion; }
                set { FirstStepFlexion = value; }
            }
            public short DEV_SYSTEMERRORTYPE
            {
                get { return SystemErrorType; }
                set { SystemErrorType = value; }
            }
            public UInt32 DEV_STEPCOUNTER
            {
                get { return StepCounter; }
                set { StepCounter = value; }
            }
            public UInt32 DEV_ENABLESYSTEMSTUCKONERROR
            {
                get { return EnableSystemStuckOnError_int; }
                set { EnableSystemStuckOnError_int = value; }
            }
            public short DEV_SWITCHINGTIME
            {
                get { return SwitchingTime; }
                set { SwitchingTime = value; }
            }
            public Status DEV_STATUS
            {
                get { return status; }
                set { status = value; }
            }
            public short DEV_INFSN1
            {
                get { return (infSN1); }
                set { (infSN1) = value; }
            }
            public short DEV_INFSN2
            {
                get { return infSN2; }
                set { infSN2 = value; }
            }
            public short DEV_INFSN3
            {
                get { return infSN3; }
                set { infSN3 = value; }
            }
            public short DEV_INFSN4
            {
                get { return infSN4; }
                set { infSN4 = value; }
            }
            public short DEV_SN1
            {
                get { return sn1; }
                set { sn1 = value; }
            }
            public short DEV_SN2
            {
                get { return sn2; }
                set { sn2 = value; }
            }
            public short DEV_SN3
            {
                get { return sn3; }
                set { sn3 = value; }
            }
            public short DEV_RC1
            {
                get { return rcSN1; }
                set { rcSN1 = value;}
            }
            public short DEV_RC2
            {
                get { return rcSN2; }
                set { rcSN2 = value;}
            }
            public short DEV_RC3
            {
                get { return rcSN3;  }
                set { rcSN3 = value; }
            }
            public short DEV_RC4
             {
                get { return rcSN4; }
                set { rcSN4 = value; }
            }
            public short DEV_LH1
            {
                get { return lhSN1; }
                set { lhSN1 = value; }
            }
            public short DEV_LH2
            {
                get { return lhSN2; }
                set { lhSN2 = value; }
            }
            public short DEV_LH3
            {
                get { return lhSN3; }
                set { lhSN3 = value; }
            }
            public short DEV_LH4
            {
                get { return lhSN4; }
                set { lhSN4 = value; }
            }
            public short DEV_LK1
            {
                get { return lkSN1; }
                set { lkSN1 = value; }
            }
            public short DEV_LK2
            {
                get { return lkSN2; }
                set { lkSN2 = value; }
            }
            public short DEV_LK3
            {
                get { return lkSN3; }
                set { lkSN3 = value; }
            }
            public short DEV_LK4
            {
                get { return lkSN4; }
                set { lkSN4 = value; }
            }
            public short DEV_RH1
            {
                get { return rhSN1; }
                set { rhSN1 = value; }
            }
            public short DEV_RH2
            {
                get { return rhSN2; }
                set { rhSN2 = value; }
            }
            public short DEV_RH3
            {
                get { return rhSN3; }
                set { rhSN3 = value; }
            }
            public short DEV_RH4
            {
                get { return rhSN4; }
                set { rhSN4 = value; }
            }
            public short DEV_RK1
            {
                get { return rkSN1; }
                set { rkSN1 = value; }
            }
            public short DEV_RK2
            {
                get { return rkSN2; }
                set { rkSN2 = value; }
            }
            public short DEV_RK3
            {
                get { return rkSN3; }
                set { rkSN3 = value; }
            }
            public short DEV_RK4
            {
                get { return rkSN4; }
                set { rkSN4 = value; }
            }
            public short DEV_AFTSitCounter
            {
                get { return AFTSitCounter; }
                set { AFTSitCounter = value; }
            }
            public short DEV_AFTASCCounter
            {
                get { return AFTASCCounter; }
                set { AFTASCCounter = value; }
            }
            public short DEV_AFTDSCCounter
            {
                get { return AFTDSCCounter; }
                set { AFTASCCounter = value; }
            }
            public short DEV_AFTSitStatus
            {
                get { return AFTSitStatus; }
                set { AFTASCCounter = value; }
            }
            public short DEV_AFTASCStatus
            {
                get { return AFTASCStatus; }
                set { AFTASCCounter = value; }
            }
            public short DEV_AFTDSCStatus
            {
                get { return AFTDSCStatus; }
                set { AFTDSCStatus = value; }
            }

            public short DEV_AFTWalkStatus
            {
                get { return AFTWalkStatus; }
                set { AFTWalkStatus = value; }
            }
            public short DEV_StairsASCSpeed
            {
                get { return StairsASCSpeed; }
                set { StairsASCSpeed = value; }
            }
            public short DEV_StairsDSCSpeed
            {
                get { return StairsDSCSpeed; }
                set { StairsDSCSpeed = value; }
            }
            public short DEV_Placing_RH_on_the_stair
            {
                get { return Placing_RH_on_the_stair; }
                set { Placing_RH_on_the_stair = value; }
            }
            public short DEV_Placing_RK_on_the_stair
            {
                get { return Placing_RK_on_the_stair; }
                set { Placing_RK_on_the_stair = value; }
            }
            public UInt32 DEV_StairCounter
            {
                get { return StairCounter; }
                set { StairCounter = value; }
            }
            public short DEV_StepLength
            {
                get { return StepLength; }
                set { StepLength = value; }
            }
            public short DEV_CurrentHandleAlgo
            {
                get { return CurrentHandleAlgo; }
                set { CurrentHandleAlgo = value; }
            }
            public short DEV_UpperLegLength
            {
                get { return UpperLegLength; }
                set { UpperLegLength = value; }
            }
            public short DEV_LowerLegLength
            {
                get { return LowerLegLength; }
                set { LowerLegLength = value; }
            }
            public short DEV_SoleHeight
            {
                get { return SoleHeight; }
                set { SoleHeight = value; }
            }
            public short DEV_ShoeSize
            {
                get { return ShoeSize; }
                set { ShoeSize = value; }
            }
            public short DEV_StairHeight
            {
                get { return StairHeight; }
                set { StairHeight = value; }
            }
            public short DEV_PicSWVersion
            {
                get { return PicSWVersion; }
                set { PicSWVersion = value; }
            }

            //New GUI6 Look and Feel design 
            public short DEV_UnitsSelection
            {
                get { return UnitsSelection; }
                set { UnitsSelection = value; }
            }

            public short DEV_Gender
            {
                get { return Gender; }
                set { Gender = value; }
            }
            public UInt32 DEV_User_ID
            {
                get { return User_ID; }
                set { User_ID = value; }
            }
            public short DEV_Weight
            {
                get { return Weight; }
                set { Weight = value; }
            }
            public short DEV_Height
            {
                get { return Height; }
                set { Height = value; }
            }
            public short DEV_UpperStrapHoldersSize
            {
                get { return UpperStrapHoldersSize; }
                set { UpperStrapHoldersSize = value; }
            }
            public short DEV_UpperStrapHoldersPosition
            {
                get { return UpperStrapHoldersPosition; }
                set { UpperStrapHoldersPosition = value; }
            }
            public short DEV_AboveKneeBracketSize
            {
                get { return AboveKneeBracketSize; }
                set { AboveKneeBracketSize = value; }
            }
            public short DEV_AboveKneeBracketPosition
            {
                get { return AboveKneeBracketPosition; }
                set { AboveKneeBracketPosition = value; }
            }
            public short DEV_FrontKneeBracketAssemble
            {
                get { return FrontKneeBracketAssemble; }
                set { FrontKneeBracketAssemble = value; }
            }
            public short DEV_FrontKneeBracketAnteriorPosition 
            {
                get { return FrontKneeBracketAnteriorPosition; }
                set { FrontKneeBracketAnteriorPosition = value; }
            }
            public short DEV_FrontKneeBracketLateralPosition
            {
                get { return FrontKneeBracketLateralPosition; }
                set { FrontKneeBracketLateralPosition = value; }
            }
            public short DEV_PelvicSize
            {
                get { return PelvicSize; }
                set { PelvicSize = value; }
            }
            public short DEV_PelvicAnteriorPosition
            {
                get { return PelvicAnteriorPosition; }
                set { PelvicAnteriorPosition = value; }
            }
            public short DEV_PelvicVerticalPosition
            {
                get { return PelvicVerticalPosition; }
                set { PelvicVerticalPosition = value; }
            }
            public short DEV_UpperLegHightSize
            {
                get { return UpperLegHightSize; }
                set { UpperLegHightSize = value; }
            }
            public short DEV_LowerLegHightSize
            {
                get { return LowerLegHightSize; }
                set { LowerLegHightSize = value; }
            }
            public short DEV_FootPlateType1
            {
                get { return FootPlateType1; }
                set { FootPlateType1 = value; }
            }
            public short DEV_FootPlateType2
            {
                get { return FootPlateType2; }
                set { FootPlateType2 = value; }
            }
            public UInt32 DEV_FootPlateNoOfRotation
            {
                get { return FootPlateNoOfRotation; }
                set { FootPlateNoOfRotation = value; }
            }
            public short DEV_AFTStopsStatus
            {
                get { return AFTStopsStatus; }
                set { AFTStopsStatus = value; } 
            }
            public short DEV_INFBatteriesLevel
            {
                get { return INFBatteriesLevel; }
                set { INFBatteriesLevel = value; } 
            }
            public short DEV_Ack_Movement_Point
            {
                get { return Ack_Movement_Point; }
                set { Ack_Movement_Point = value; } 
            }
            public short DEV_Enable_First_Step_Rewalk6
            {
                get { return Enable_First_Step_Rewalk6; }
                set { Enable_First_Step_Rewalk6 = value; }             
            }
            public short DEV_SystemResrvedParmeter5
            {
                get { return SystemResrvedParmeter5; }
                set { SystemResrvedParmeter5 = value; } 
            }
            public short DEV_SystemResrvedParmeter6
            {
                get { return SystemResrvedParmeter6; }
                set { SystemResrvedParmeter6 = value; } 
            }
            public short DEV_SystemResrvedParmeter7
            {
                get { return SystemResrvedParmeter7; }
                set { SystemResrvedParmeter7 = value; } 
            }
            public short DEV_SystemResrvedParmeter8
            {
                get { return SystemResrvedParmeter8; }
                set { SystemResrvedParmeter8 = value; } 
            }
            public short DEV_SystemResrvedParmeter9
            {
                 get { return SystemResrvedParmeter9; }
                set { SystemResrvedParmeter9 = value; } 
            }
            public short DEV_SystemResrvedParmeter10
            {
                 get { return SystemResrvedParmeter10; }
                set { SystemResrvedParmeter10 = value; } 
            }
        };


        [StructLayout(LayoutKind.Sequential)]
        public struct OnlineData
        {
            private  short lhAngle;
            private  short lkAngle;
            private  short rhAngle;
            private  short rkAngle;
            private  short tiltXAngle;
            private  short tiltYAngle;
            private  short fsrRight1;
            private  short fsrRight2;
            private  short fsrRight3;
            private  short fsrLeft1;
            private  short fsrLeft2;
            private  short fsrLeft3;
            private  short mainBattery;
            private  short auxBattery;
            private  short custom1;
            private  short custom2;
            private  short custom3;
            private  short custom4;
            private  short custom5;
            private  Status status;

            public short LHANGLE
            {
                get { return lhAngle; }
                set { lhAngle = value; }
            }
            public short RHANGLE
            {
                get { return rhAngle; }
                set { rhAngle = value; }
            }
            public short LKANGLE
            {
                get { return lkAngle; }
                set { lkAngle = value; }
            }
            public short RKANGLE
            {
                get { return rkAngle; }
                set { rkAngle = value; }
            }
            public short TILTXANGLE
            {
                get { return tiltXAngle; }
                set { tiltXAngle = value; }
            }
            public short TILTYANGLE
            {
                get { return tiltYAngle; }
                set { tiltYAngle = value; }
            }
            public short FsrRight1
            {
                get { return fsrRight1; }
                set { fsrRight1 = value; }
            }

            public short FsrRight2
            {
                get { return fsrRight2; }
                set { fsrRight2 = value;  }
            }

            public short FsrRight3
            {
                get { return fsrRight3; }
                set { fsrRight3 = value;  }
            }

            public short FsrLeft1
            {
                get { return fsrLeft1; }
                set { fsrLeft1 = value;  }
            }

            public short FsrLeft2
            {
                get { return fsrLeft2; }
                set { fsrLeft2 = value;  }
            }

            public short FsrLeft3
            {
                get { return fsrLeft3; }
                set { fsrLeft3 = value;  }
            }
            public short MAINBATTERY
            {
                get { return mainBattery; }
                set { mainBattery = value; }
            }
            public short AUXBATTERY
            {
                get { return auxBattery; }
                set { auxBattery = value; }
            }
            public short CUSTOM1
            {
                get { return custom1; }
                set { custom1 = value; }
            }
            public short CUSTOM2
            {
                get { return custom2; }
                set { custom2 = value; }
            }
            public short CUSTOM3
            {
                get { return custom3; }
                set { custom3 = value; }
            }
            public short CUSTOM4
            {
                get { return custom4; }
                set { custom4 = value; }
            }
            public short CUSTOM5
            {
                get { return custom5; }
                set { custom5 = value; }
            }
            public Status  STATUS
            {
                get { return status; }
                set { status = value; }
            }
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct CustomCommandResult
        {
            private byte id1;
            private byte id2;
            private byte byte1;
            private byte byte2;
            private byte byte3;
            private byte byte4;
            private byte byte5;
            private byte byte6;
            private byte byte7;
            private  byte byte8;
            private  Status status;
            public byte ID1
            {
                get { return id1; }
                set { id1 = value; }
            }
            public byte ID2
            {
                get { return id2; }
                set { id2 = value; }
            }

            public byte BYTE1
            {
                get { return byte1; }
                set { byte1 = value; }
            }
            public byte BYTE2
            {
                get { return byte2; }
                set { byte2 = value; }
            }
            public byte BYTE3
            {
                get { return byte3; }
                set { byte3 = value; }
            }
            public byte BYTE4
            {
                get { return byte4; }
                set { byte4 = value; }
            }
            public byte BYTE5
            {
                get { return byte5; }
                set { byte5 = value; }
            }
            public byte BYTE6
            {
                get { return byte6; }
                set { byte6 = value; }
            }
            public byte BYTE7
            {
                get { return byte7; }
                set { byte7 = value; }
            }
            public byte BYTE8
            {
                get { return byte8; }
                set { byte8 = value; }
            }
            public Status STATUS
            {
                get { return status; }
                set { status = value; }
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct TestData
        {
            private  short inf;
            private  short rc;
            private  short lh;
            private  short lk;
            private  short rh;
            private  short rk;
            private  Status status;

            public short INF
            {
                get { return inf; }
                set { inf = value; }
            }
            public short RC
            {
                get { return rc; }
                set { rc = value; }
            }
            public short LH
            {
                get { return lh; }
                set { lh = value; }
            }
            public short LK
            {
                get { return lk; }
                set { lk = value; }
            }
            public short RH
            {
                get { return rh; }
                set { rh = value; }
            }
            public short RK
            {
                get { return rk; }
                set { rk = value; }
            }
            public Status STATUS
            {
                get { return status; }
                set { status = value; }
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RFTestData
        {
            private short inf_BER;
            private short rc_BER;
            private short numOfPackets;
            private Status status;

            public short INF_BER
            {
                get { return inf_BER; }
                set { inf_BER = value; }
            }
            public short RC_BER
            {
                get { return rc_BER; }
                set { rc_BER = value; }
            }
            public short NUMOFPACKETS
            {
                get { return numOfPackets; }
                set { numOfPackets = value; }
            }
            public Status STATUS
            {
                get { return status; }
                set { status = value; }
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RCAddressIDData
        {
            private  short RC_Address;
            private  ushort RC_ID;
            private  Status status;

            public short RC_ADDRESS
            {
                get { return RC_Address; }
                set { RC_Address = value; }
            }
            public ushort RC_ID__
            {
                get { return RC_ID; }
                set { RC_ID = value; }
            }
            public Status STATUS
            {
                get { return status; }
                set { status = value; }
            }
        }

        public struct SystemErrorData
        {
            private  short ErrorType;
            private  Status status;

            public short ERRORTYPE
            {
                get { return ErrorType; }
                set { ErrorType = value; }
            }
            public Status STATUS
            {
                get { return status; }
                set { status = value; }
            }
        }

        public struct StepCounterData
        {
            private  UInt32 StepCounter;
            private  Status status;

            public UInt32 STEPCOUNTER
            {
                get { return StepCounter; }
                set { StepCounter = value; }
            }
            public Status STATUS
            {
                get { return status; }
                set { status = value; }
            }
        }

        public struct StairCounterData
        {
            private UInt32 StairCounter;
            private Status status;

            public UInt32 STAIRCOUNTER
            {
                get { return StairCounter; }
                set { StairCounter = value; }
            }
            public Status STATUS
            {
                get { return status; }
                set { status = value; }
            }
        }
        

        public const int DOWNLOAD_DSP = 27;
        public const int DOWNLOAD_MCU = 29;
        public const int DOWNLOAD_RC  = 31;
        public const int COMMAND1     = 39;

        [DllImport("Host.dll")]
      //  [DllImport("kernel32.dll", SetLastError=true)]
        public static extern DeviceParameters ReadDeviceParameters();

        [DllImport("Host.dll")]
        public static extern DeviceParameters RestoreDeviceParameters();

  
        [DllImport("Host.dll")]
        public static extern Status WriteDeviceParameters(
                                                      IntPtr userName, int userNameSize,
                                                      IntPtr computerName, int computerNameSize,
                                                      IntPtr date, int dateSize,
                                                      IntPtr upperStrapHolderSizeString, int upperStrapHolderSizeString_length,
                                                      IntPtr upperStrapHolderTypeString, int upperStrapHolderTypeString_length,
                                                      IntPtr aboveKneeBracketTypeString, int aboveKneeBracketTypeString_length,
                                                      IntPtr aboveKneeBracketAnteriorString, int aboveKneeBracketAnteriorString_length,
                                                      IntPtr kneeBracketTypeString, int kneeBracketTypeString_length,
                                                      IntPtr kneeBracketAnteriorPositionString, int kneeBracketAnteriorPositionString_length,
                                                      IntPtr kneeBracketLateralPositionString, int kneeBracketLateralPositionString_length,
                                                      IntPtr pelvicSizeString, int pelvicSizeString_length,
                                                      IntPtr pelvicAnteriorPositionString, int pelvicAnteriorPositionString_length,
                                                      IntPtr pelvicVerticalPositionString, int pelvicVerticalPositionString_length,
                                                      IntPtr footPlateTypeString, int footPlateTypeString_length,
                                                      IntPtr footPlateSizeString, int footPlateSizeString_length,
                                                      IntPtr footPlateDorsiFlexionPositionString, int footPlateDorsiFlexionPositionString_length,
                                                      IntPtr aSCSpeedString, int aSCSpeedString_length,
                                                      IntPtr dSCSpeedString, int dSCSpeedString_length,

                                                      DeviceParameters dp);
        //[DllImport("Host.dll")]
        //public static extern Status WriteRCAddress(short address);

        //[DllImport("Host.dll")]
        //public static extern Status WriteINFId(short id);
        
        [DllImport("Host.dll")]
        public static extern Status WriteRCAddressID(ushort address, ushort id);

        [DllImport("Host.dll")]
        public static extern RCAddressIDData GetWriteRCAddressIDStatus();

        [DllImport("Host.dll")]
        public static extern Status WriteINFAddressID(ushort address, ushort id);

		[DllImport("Host.dll")]
        public static extern Status WriteINFAddress(ushort address);

        [DllImport("Host.dll")]
        public static extern Status WriteINFId(ushort id);

        [DllImport("Host.dll")]
        public static extern Status Download(byte[] data, int count, int command);

        [DllImport("Host.dll")]
        public static extern OnlineData ReadOnlineData();

        [DllImport("Host.dll")]
        public static extern CustomCommandResult RunCustomCommand(byte[] data, int count, int command);

        [DllImport("Host.dll")]
        public static extern Status StartTest();

        [DllImport("Host.dll")]
        public static extern TestData GetTestStatus();

        [DllImport("Host.dll")]
        public static extern Status RFStartTest();

        [DllImport("Host.dll")]
        public static extern  RFTestData GetRFTestStatus();      

        [DllImport("Host.dll")]
        public static extern Status StartCalibration(bool lh, bool lk, bool rh, bool rk);

        [DllImport("Host.dll")]
        public static extern Status DoCalibration();

        [DllImport("Host.dll")]
        public static extern  TestData GetCalibrationStatus();

        [DllImport("Host.dll")]
        public static extern Status SaveExcelData(IntPtr file);
        
        [DllImport("Host.dll")]
        public static extern Status SaveLogFile(IntPtr file, bool sendDone);

        [DllImport("Host.dll")]
        public static extern Status SaveFlashLogFile(IntPtr file);

        [DllImport("Host.dll")]
        public static extern Status ClearSystemError();      

        [DllImport("Host.dll")]
        public static extern Status WriteSerialNumbers(SerialNumbers sn);


        [DllImport("Host.dll")]
        public static extern Status ShutDown();

        [DllImport("Host.dll")]
        public static extern bool CheckUSBConnected();

       /* [DllImport("Host.dll")]
        public static extern Status SendDebugCommand(char[] command);*/
        
        [DllImport("Host.dll")]
        public static extern SystemErrorData GetSystemErrorStatus(); 
        
        [DllImport("Host.dll")]
        public static extern Status ResetStepCounter();

        [DllImport("Host.dll")]
        public static extern Status ClearlogFile();
 
        [DllImport("Host.dll")]
        public static extern StepCounterData ReadStepCounter();

        [DllImport("Host.dll")]
        public static extern Status ResetStairCounter();

        [DllImport("Host.dll")]
        public static extern StairCounterData ReadStairCounter();
        
        
        
    }
}
