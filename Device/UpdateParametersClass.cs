﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Rewalk.Device
{
    public class UpdateParametersClass // parasoft-suppress  CS.OOM.MI "Coding style - method logic was verified"
    {
        private static UpdateParametersClass UpdateInstance = new UpdateParametersClass();

        public static UpdateParametersClass UpdateINSTANCE
        {
            get { return UpdateInstance; }
            set { UpdateInstance = value; }
        }
        private UpdateParametersClass()
        {
        }

        public void UpdateParameters(DeviceParameters ds) // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions"
        {
           
            DeviceInterop.DeviceParameters dp = new DeviceInterop.DeviceParameters();

            dp.KNEEANGLE = ds.KneeAngle;
            dp.HIPANGLE = ds.HipAngle;
            dp.MAXVELOCITY = ds.MaxVelocity;
            dp.BACKHIPANGLE = ds.BackHipAngle;
            dp.TILTDELTA = ds.TiltDelta;
            dp.TILTTIMEOUT = ds.TiltTimeout;
            dp.SWITCHFSRS = Convert.ToInt16(ds.SwitchFSRs);
            dp.FSRTHRESHOLD = ds.FSRThreshold;
            dp.FSRWALKTIMEOUT = ds.FSRWalkTimeout;
            dp.DELAYBETWEENSTEPS = ds.DelayBetweenSteps;
            dp.BUZZERBETWEENSTEPS = Convert.ToInt16(ds.BuzzerBetweenSteps);
            dp.RECORD = Convert.ToInt16(ds.Record);
            dp.SAMPLETIME = ds.SampleTime;
            dp.SAFETYWHILESTANDING = Convert.ToInt16(ds.SafetyWhileStanding);
            dp.XTHRESHOLD = ds.XThreshold;
            dp.YTHRESHOLD = ds.YThreshold;
            dp.DEV_LHASC = ds.LHAsc; dp.DEV_RHASC = ds.RHAsc; dp.DEV_LKASC = ds.LKAsc; dp.DEV_RKASC = ds.RKAsc; dp.DEV_LHDSC = ds.LHDsc; dp.DEV_RHDSC = ds.RHDsc; dp.DEV_LKDSC = ds.LKDsc; dp.DEV_RKDSC = ds.RKDsc;
            dp.RCADDRESS = ds.RCAddress; dp.RCID = ds.RCId;
            dp.SOFTWAREVERSION = ds.Device; dp.DSPVERSION = (short)(ds.DSP); dp.MOTORVERSION = (short)(ds.Motor); dp.RCVERSION = (short)(ds.Remote); dp.FPGAVERSION = ds.FPGA; dp.BOOTVERSION = (short)ds.Boot;
           
            //Serial numbers
            dp.DEV_SN1 = ds.Sn1; dp.DEV_SN2 = ds.Sn2; dp.DEV_SN3 = ds.Sn3;    
            dp.DEV_INFSN1 = (short)ds.InfSN; dp.DEV_INFSN2 = (short)(ds.InfSN >> 16); dp.DEV_INFSN3 = (short)(ds.InfSN >> 32); dp.DEV_INFSN4 = (short)(ds.InfSN >> 48);
            dp.DEV_RC1 = (short)ds.RcSN; dp.DEV_RC2 = (short)(ds.RcSN >> 16); dp.DEV_RC3 = (short)(ds.RcSN >> 32); dp.DEV_RC4 = (short)(ds.RcSN >> 48);
            dp.DEV_LH1 = (short)ds.LhSN; dp.DEV_LH2 = (short)(ds.LhSN >> 16); dp.DEV_LH3 = (short)(ds.LhSN >> 32); dp.DEV_LH4 = (short)(ds.LhSN >> 48);
            dp.DEV_RH1 = (short)ds.RhSN; dp.DEV_RH2 = (short)(ds.RhSN >> 16); dp.DEV_RH3 = (short)(ds.RhSN >> 32); dp.DEV_RH4 = (short)(ds.RhSN >> 48);
            dp.DEV_LK1 = (short)ds.LkSN; dp.DEV_LK2 = (short)(ds.LkSN >> 16); dp.DEV_LK3 = (short)(ds.LkSN >> 32); dp.DEV_LK4 = (short)(ds.LkSN >> 48);
            dp.DEV_RK1 = (short)ds.RkSN; dp.DEV_RK2 = (short)(ds.RkSN >> 16); dp.DEV_RK3 = (short)(ds.RkSN >> 32); dp.DEV_RK4 = (short)(ds.RkSN >> 48);
            dp.DEV_WALKCURRENTTHRESHOLD = ds.Walk_Current_Threshold;
            dp.DEV_STAIRSCURRENTTHRESHOLD = ds.Stairs_Current_Threshold;
            dp.DEV_SYSTEMTYPE = ds.SysType;
            dp.DEV_STAIRSENABLED = ds.StairsEnabled ? (short)1 : (short)0;
            //lOG
            dp.DEV_CANLOGLEVEL = ds.LogLevel.CAN.LogLevel; dp.DEV_MCULOGLEVEL = ds.LogLevel.MCU.LogLevel; dp.DEV_RFLOGLEVEL = ds.LogLevel.RF.LogLevel; dp.DEV_RCLOGLEVEL = ds.LogLevel.RC.LogLevel; dp.DEV_ADCLOGLEVEL = ds.LogLevel.ADC.LogLevel;
            dp.DEV_STAIRSLOGLEVEL = ds.LogLevel.STAIRS.LogLevel; dp.DEV_SITSTNLOGLEVEL = ds.LogLevel.SIT_STN.LogLevel; dp.DEV_WALKLOGLEVEL = ds.LogLevel.WALK.LogLevel; dp.DEV_USBLOGLEVEL = ds.LogLevel.USB.LogLevel; dp.DEV_BITLOGLEVEL = ds.LogLevel.BIT.LogLevel;        
            dp.DEV_FlashLogLevel = ds.LogLevel.MEM.LogLevel; dp.DEV_OperationLogLevel = ds.LogLevel.OPS.LogLevel;
            dp.DEV_FIRSTSTEPFLEXION = ds.FirstStepFlexion;
            dp.DEV_ENABLESYSTEMSTUCKONERROR = ds.EnableSystemStuckOnError_int;
            dp.DEV_SWITCHINGTIME = ds.SwitchingTime;
            dp.DEV_StairsASCSpeed = ds.StairsASCSpeed;
            dp.DEV_StairsDSCSpeed = ds.StairsDSCSpeed;
            dp.DEV_Placing_RH_on_the_stair = ds.Placing_RH_on_the_stair;
            dp.DEV_Placing_RK_on_the_stair = ds.Placing_RK_on_the_stair;
            dp.DEV_StepLength = ds.StepLength;
            dp.DEV_StairCounter = ds.StairCounter;
            dp.DEV_CurrentHandleAlgo = (ds.CurrentHandleAlgo) ? (short)1 : (short)0;
            dp.DEV_UpperLegLength = ds.UpperLegLength;
            dp.DEV_LowerLegLength = ds.LowerLegLength;
            dp.DEV_SoleHeight = ds.SoleHeight;
            dp.DEV_ShoeSize =  ds.ShoeSize;
            dp.DEV_StairHeight = ds.StairHeight;
            dp.DEV_PicSWVersion = ds.PicSWVersion;
            //New GUI6 Look and Feel design
            dp.DEV_UnitsSelection = ds.UnitsSelection;
            dp.DEV_Gender = Convert.ToInt16(ds.Gender);
            dp.DEV_User_ID = ds.User_ID;
            dp.DEV_Weight = ds.Weight;
            dp.DEV_Height = ds.Height;
            dp.DEV_UpperStrapHoldersSize = ds.UpperStrapHoldersSize;
            dp.DEV_UpperStrapHoldersPosition = ds.UpperStrapHoldersPosition;
            dp.DEV_AboveKneeBracketSize = ds.AboveKneeBracketSize;
            dp.DEV_AboveKneeBracketPosition = ds.AboveKneeBracketPosition;
            dp.DEV_FrontKneeBracketAssemble = ds.FrontKneeBracketAssemble;
            dp.DEV_FrontKneeBracketAnteriorPosition = ds.FrontKneeBracketAnteriorPosition;
            dp.DEV_FrontKneeBracketLateralPosition = ds.FrontKneeBracketLateralPosition;
            dp.DEV_PelvicSize = ds.PelvicSize;
            dp.DEV_PelvicAnteriorPosition = ds.PelvicAnteriorPosition;
            dp.DEV_PelvicVerticalPosition = ds.PelvicVerticalPosition;
            dp.DEV_UpperLegHightSize = ds.UpperLegHightSize;
            dp.DEV_LowerLegHightSize = ds.LowerLegHightSize;
            dp.DEV_FootPlateType1 = ds.FootPlateType1;
            dp.DEV_FootPlateType2 = ds.FootPlateType2;
            dp.DEV_FootPlateNoOfRotation = ds.FootPlateNoOfRotation;
            dp.DEV_AFTStopsStatus = ds.AFTStopsStatus;
            dp.DEV_INFBatteriesLevel = ds.INFBatteriesLevel;
            dp.DEV_Ack_Movement_Point = ds.Ack_Movement_Point;
            dp.DEV_Enable_First_Step_Rewalk6 = ds.Enable_First_Step_Rewalk6 ? (short)1 : (short)0; 
            dp.DEV_SystemResrvedParmeter5 = ds.SystemResrvedParmeter5;
            dp.DEV_SystemResrvedParmeter6 = ds.SystemResrvedParmeter6;
            dp.DEV_SystemResrvedParmeter7 = ds.SystemResrvedParmeter7;
            dp.DEV_SystemResrvedParmeter8 = ds.SystemResrvedParmeter8;
            dp.DEV_SystemResrvedParmeter9 = ds.SystemResrvedParmeter9;
            dp.DEV_SystemResrvedParmeter10 = ds.SystemResrvedParmeter10;



         
            string  UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            IntPtr ptrUserName = Marshal.StringToCoTaskMemAnsi(UserName);
            string ComputerName = System.Environment.MachineName;
            IntPtr ptrComputerName = Marshal.StringToCoTaskMemAnsi(ComputerName);
            string DateTimeString = DateTime.Now.ToString();
            IntPtr ptrDateTimeString = Marshal.StringToCoTaskMemAnsi(DateTimeString);

            try
            {
               // GC.SuppressFinalize(this);
                Status status = DeviceInterop.WriteDeviceParameters(
                ptrUserName, UserName.Length,
                ptrComputerName,ComputerName.Length,
                ptrDateTimeString, DateTimeString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.UpperStrapHolderSizeString), ds.UpperStrapHolderSizeString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.UpperStrapHolderTypeString), ds.UpperStrapHolderTypeString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.AboveKneeBracketTypeString), ds.AboveKneeBracketTypeString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.AboveKneeBracketAnteriorString), ds.AboveKneeBracketAnteriorString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.KneeBracketTypeString), ds.KneeBracketTypeString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.KneeBracketAnteriorPositionString), ds.KneeBracketAnteriorPositionString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.KneeBracketLateralPositionString), ds.KneeBracketLateralPositionString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.PelvicSizeString), ds.PelvicSizeString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.PelvicAnteriorPositionString), ds.PelvicAnteriorPositionString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.PelvicVerticalPositionString), ds.PelvicVerticalPositionString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.FootPlateTypeString), ds.FootPlateTypeString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.FootPlateSizeString), ds.FootPlateSizeString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.FootPlateDorsiFlexionPositionString), ds.FootPlateDorsiFlexionPositionString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.ASCSpeedString), ds.ASCSpeedString.Length,
                Marshal.StringToCoTaskMemAnsi(ds.DSCSpeedString), ds.DSCSpeedString.Length,
                dp);
                if (status != Status.Operation_passed)
                {
                    throw new DeviceOperationException(DeviceManager.INSTANCE.Status2String(status));
                }
            }
            catch (Exception exception)
            {
                
                throw new DeviceOperationException(exception.Message);
                          
            }
           finally
            {
               Marshal.FreeCoTaskMem(ptrUserName);
            }
                
            
        /* 


            try
            {

                Status status = DeviceInterop.WriteDeviceParameters(ptrUserName, UserName.Length, ptrComputerName, ComputerName.Length, ptrDateTimeString, DateTimeString.Length,dp);
                if (status != Status.Operation_passed)
                {
                    DeviceClassInterface MessageWindow = new DeviceClassInterface();
                    //MessageWindow.ShowMessageBox(DeviceManager.INSTANCE.Status2String(status));
                    throw new DeviceOperationException(DeviceManager.INSTANCE.Status2String(status));
                }
            }
            catch(Exception exception)
            {
                string a = exception.Message;    
                
            }
            finally
            {
                Marshal.FreeCoTaskMem(ptrUserName);
            }

            //Write measurments strings
       


       /*  try
         {

                 Status status = DeviceInterop.WriteSystemMeausrmentsDeviceParameters(


                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 Marshal.StringToCoTaskMemAnsi("rasem12"), "rasem12".Length,
                 dp);
             if (status != Status.Operation_passed)
             {
                 DeviceClassInterface MessageWindow = new DeviceClassInterface();
                 //MessageWindow.ShowMessageBox(DeviceManager.INSTANCE.Status2String(status));
                 throw new DeviceOperationException(DeviceManager.INSTANCE.Status2String(status));
             }
         }
         catch (Exception exception)
         {
             string a = exception.Message;

         }
         finally
         {
             Marshal.FreeCoTaskMem(ptrUserName);
         }
                
         */

        }



    }     
}
