﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;


namespace Rewalk.Device
{
    
    public partial class DeviceParameters : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion

        #region Public Properties
      

        public short SysType
        {
            get { return sysType; }
            set { sysType = value; RaisePropertyChanged("SysType");    }
        }

        public string SysTypeString
        {
            get 
            {
                SystemType sys_Type = (SystemType)sysType;
                switch (sys_Type)
                {
                    case SystemType.Sys_Unknown:
                        return "";
                    case SystemType.Rewalk_P0:
                        return "Rewalk-P1.0";
                    case SystemType.Rewalk_I:
                        return "Rewalk-I";
                    case SystemType.Rewalk_R:
                        return "Rewalk-R";
                    case SystemType.Rewalk_P6:
                        return "Rewalk-P6.0";
                    default:
                        return "Error";
                }   
                
            }
            set 
            {
                switch (value)
                {
                    case "Rewalk-P1.0" :
                        sysType = (short)SystemType.Rewalk_P0;
                        break;
                    case "Rewalk-P":
                        sysType = (short)SystemType.Rewalk_P0;
                        break;
                    case "Rewalk-R":
                        sysType = (short)SystemType.Rewalk_R;
                        break;
                    case "Rewalk-I":
                        sysType = (short)SystemType.Rewalk_I;
                        break;
                    case "Rewalk-P6.0":
                        sysType = (short)SystemType.Rewalk_P6;
                        break;
                    default:
                        sysType = (short)SystemType.Sys_Unknown;
                        break;
                      
                }

                RaisePropertyChanged("SysTypeString"); 
            }
        }

        public bool StairsEnabled
        {
            get { return stairsEnabled; }
            set { stairsEnabled = value; RaisePropertyChanged("StairsEnabled"); }
        }

        public short KneeAngle
        {
            get { return kneeAngle; }
            set { kneeAngle = value; RaisePropertyChanged("KneeAngle"); }
        }

        public short HipAngle
        {
            get { return hipAngle; }
            set { hipAngle = value; RaisePropertyChanged("HipAngle"); }
        }

        public short BackHipAngle
        {
            get { return backHipAngle; }
            set { backHipAngle = value; RaisePropertyChanged("BackHipAngle"); }
        }

        public short MaxVelocity
        {
            get { return maxVelocity; }
            set { maxVelocity = value; RaisePropertyChanged("MaxVelocity"); }
        }

        public short TiltDelta
        {
            get { return tiltDelta; }
            set { tiltDelta = value; RaisePropertyChanged("TiltDelta"); }
        }

        public short TiltTimeout
        {
            get { return tiltTimeout; }
            set { tiltTimeout = value; RaisePropertyChanged("TiltTimeout"); }
        }

        public bool SwitchFSRs
        {
            get { return switchFSRs; }
            set { switchFSRs = value; RaisePropertyChanged("SwitchFSRs"); }
        }

        public short FSRThreshold
        {
            get { return fSRThreshold; }
            set { fSRThreshold = value; RaisePropertyChanged("FSRThreshold"); }
        }

        public short FSRWalkTimeout
        {
            get { return fSRWalkTimeout; }
            set { fSRWalkTimeout = value; RaisePropertyChanged("FSRWalkTimeout"); }
        }

        public short DelayBetweenSteps
        {
            get { return delayBetweenSteps; }
            set { delayBetweenSteps = value; RaisePropertyChanged("DelayBetweenSteps"); }
        }

        public bool BuzzerBetweenSteps
        {
            get { return buzzerBetweenSteps; }
            set { buzzerBetweenSteps = value; RaisePropertyChanged("BuzzerBetweenSteps"); }
        }

        public bool Record
        {
            get { return record; }
            set { record = value; RaisePropertyChanged("Record"); }
        }

        public short SampleTime
        {
            get { return sampleTime; }
            set { sampleTime = value; RaisePropertyChanged("SampleTime"); }
        }

        public bool SafetyWhileStanding
        {
            get { return safetyWhileStanding; }
            set { safetyWhileStanding = value; RaisePropertyChanged("SafetyWhileStanding"); }
        }

        public short XThreshold
        {
            get { return xThreshold; }
            set { xThreshold = value; RaisePropertyChanged("XThreshold"); }
        }

        public short YThreshold
        {
            get { return yThreshold; }
            set { yThreshold = value; RaisePropertyChanged("YThreshold"); }
        }

        public short LHAsc
        {
            get { return lhAsc; }
            set { lhAsc = value; RaisePropertyChanged("LHAsc"); }
        }

        public short LKAsc
        {
            get { return lkAsc; }
            set { lkAsc = value; RaisePropertyChanged("LKAsc"); }
        }

        public short RHAsc
        {
            get { return rhAsc; }
            set { rhAsc = value; RaisePropertyChanged("RHAsc"); }
        }

        public short RKAsc
        {
            get { return rkAsc; }
            set { rkAsc = value; RaisePropertyChanged("RKAsc"); }
        }

        public short LHDsc
        {
            get { return lhDsc; }
            set { lhDsc = value; RaisePropertyChanged("LHDsc"); }
        }

        public short LKDsc
        {
            get { return lkDsc; }
            set { lkDsc = value; RaisePropertyChanged("LKDsc"); }
        }

        public short RHDsc
        {
            get { return rhDsc; }
            set { rhDsc = value; RaisePropertyChanged("RHDsc"); }
        }

        public short RKDsc
        {
            get { return rkDsc; }
            set { rkDsc = value; RaisePropertyChanged("RKDsc"); }
        }

        [XmlIgnore]
        public short Device
        {
            get { return device; }
            set { device = value; RaisePropertyChanged("Device"); }
        }

        [XmlIgnore]
        public int DSP
        {
            get 
            {   // sysType is Device hardawre version 
                // device Low byte is INF board hardawre version 
               
                    int retval = sysType << 24 | (byte)(device) << 16 | (dsp & 0xffffff);
                
                return retval; 
            }
            set { dsp = value; RaisePropertyChanged("DSP"); }
        }

        [XmlIgnore]
        public int Motor
        {
            get 
            {
                // fpga Low byte is MCU board hardawre version 
                // fpga High byte is RC board hardawre version 
                int retval = sysType << 24 | (byte)(fpga) << 16 | (motor & 0xffffff);

                return retval; 
            }
            set { motor = value; RaisePropertyChanged("Motor"); }
        }

        [XmlIgnore]
        public int Remote
        {
            get 
            {
                //int retval = sysType << 24 |  (byte)(fpga>>8) << 16 |(remote & 0xffffff);
               // int rem = ((dsp &0xff00)>>8) | (remote&0xff00); //Saman Remove 
                int rem = (remote & 0xffff);
                int retval = sysType << 24 | (byte)(0x03) << 16 | rem; //Must be updated in INF 
                return retval; 
            }
            set { remote = value; RaisePropertyChanged("Remote"); }
        }

        [XmlIgnore]
        public short FPGA
        {
            get { return fpga; }
            set { fpga = value; RaisePropertyChanged("FPGA"); }
        }

        [XmlIgnore]
        public int Boot
        {
            get
            {
                int retval = sysType << 24 | (byte)(device) << 16 | (boot & 0xffffff);

                return retval;
            }
            set { boot = value; RaisePropertyChanged("Boot"); }
        }

        public ushort RCAddress
        {
            get { return rcAddress; }
            set { rcAddress = value; RaisePropertyChanged("RCAddress"); }
        }

        public ushort RCId
        {
            get { return rcId; }
            set { rcId = value; RaisePropertyChanged("RCId"); }
        }

        [XmlIgnore]
        public TestStatus DspSoftwareProgrammed
        {
            get { return dspSoftwareProgrammed; }
            set { dspSoftwareProgrammed = value; RaisePropertyChanged("DspSoftwareProgrammed"); }
        }

        [XmlIgnore]
        public TestStatus SystemCalbirationStatus
        {
            get { return systemCalbirationStatus; }
            set { systemCalbirationStatus = value; RaisePropertyChanged("SystemCalbirationStatus"); }
        }

        [XmlIgnore]
        public TestStatus SerialNumberStatus
        {
            get { return serialNumberStatus; }
            set { serialNumberStatus = value; RaisePropertyChanged("SerialNumberStatus"); }
        }

        [XmlIgnore]
        public string SN
        {
            get { return string.Format("{0}-{1}-{2}", Sn1Char, Sn2, Sn3); }
        }

        [XmlIgnore]
        public short Sn1
        {
            get { return sn1; }
            set { sn1 = value; RaisePropertyChanged("Sn1"); RaisePropertyChanged("Sn1Char"); RaisePropertyChanged("SN"); }
        }

        [XmlIgnore]
        public string Sn1Char
        {
            get
            { //return ((char)sn1).ToString(); }
                if (SysType == 3)
                    return ("P");
                else if (SysType == 2)
                    return ("I");
                else if (SysType == 4)
                    return ("R");
                else if (SysType == 5)
                    return ("P");

                else
                    return ("N/A");
            }
            set { sn1 = (short)(byte)value[0]; RaisePropertyChanged("SN"); }
        }

        [XmlIgnore]
        public short Sn2
        {
            get { return sn2; }
            set { sn2 = value; RaisePropertyChanged("Sn2"); RaisePropertyChanged("SN"); }
        }

        [XmlIgnore]
        public short Sn3
        {
            get { return sn3; }
            set { sn3 = value; RaisePropertyChanged("Sn3"); RaisePropertyChanged("SN"); }
        }

        [XmlIgnore]
        public long InfSN
        {
            get { return infSN; }
            set { infSN = value; RaisePropertyChanged("InfSN"); }
        }

        [XmlIgnore]
        public long RcSN
        {
            get { return rcSN; }
            set { rcSN = value; RaisePropertyChanged("RcSN"); }
        }

        [XmlIgnore]
        public long LhSN
        {
            get { return lhSN; }
            set { lhSN = value; RaisePropertyChanged("LhSN"); }
        }

        [XmlIgnore]
        public long LkSN
        {
            get { return lkSN; }
            set { lkSN = value; RaisePropertyChanged("LkSN"); }
        }

        [XmlIgnore]
        public long RhSN
        {
            get { return rhSN; }
            set { rhSN = value; RaisePropertyChanged("RhSN"); }
        }

        [XmlIgnore]
        public long RkSN
        {
            get { return rkSN; }
            set { rkSN = value; RaisePropertyChanged("RkSN"); }
        }

        public short RCBER
        {
            get { return RC_BER; }
            set { RC_BER = value; RaisePropertyChanged("RCBER"); }
        }

        public short INFBER
        {
            get { return INF_BER; }
            set { INF_BER = value; RaisePropertyChanged("INFBER"); }
        }

        public short RFProgress
        {
            get { return RF_progress; }
            set { RF_progress = value; RaisePropertyChanged("RFProgress"); }
        }

        public short Walk_Current_Threshold
        {
            get { return WalkCurrentThreshold; }
            set { WalkCurrentThreshold = value; RaisePropertyChanged("Walk_Current_Threshold"); }
        }
        public short Stairs_Current_Threshold
        {
            get { return StairsCurrentThreshold; }
            set { StairsCurrentThreshold = value; RaisePropertyChanged("Stairs_Current_Threshold"); }
        }

        public LogLevel LogLevel
        {
            get { return logLevel; }
            set { logLevel = value; RaisePropertyChanged("LogLevel"); }
        }

        public short FirstStepFlexion
        {
            get { return firstStepFlexion; }
            set 
            { 
                firstStepFlexion = value;
                if (value > 0)
                {
                    enableFirstStep = true;
                }
                else
                {
                    enableFirstStep = false;
                }
                RaisePropertyChanged("FirstStepFlexion"); 
            }
        }

        public bool EnableFirstStep
        {
            get { return enableFirstStep; }
            set { enableFirstStep = value; RaisePropertyChanged("EnableFirstStep"); }
        }

     

        public UInt32 StepCounter
        {
            get { return stepCounter; }
            set { stepCounter = value; RaisePropertyChanged("StepCounter"); }
        }

        public short SysErrorType
        {
            get { return sysErrorType; }
            set { sysErrorType = value; RaisePropertyChanged("SysErrorType"); }
        }

        public UInt32 EnableSystemStuckOnError_int
        {
            get { return enableSystemStuckOnError_int; }
            set { enableSystemStuckOnError_int = value; RaisePropertyChanged("EnableSystemStuckOnError_int"); }
        }


        public short SwitchingTime
        {
            get { return switchingTime; }
            set { switchingTime = value; RaisePropertyChanged("SwitchingTime"); }
        }

        public short AFTSitCounter
        {
            get { return aFTSitCounter; }
            set { aFTSitCounter = value; RaisePropertyChanged("AFTSitCounter"); }
        }

        public short AFTASCCounter
        {
            get { return aFTASCCounter; }
            set { aFTASCCounter = value; RaisePropertyChanged("AFTASCCounter"); }
        }

        public short AFTDSCCounter
        {
            get { return aFTDSCCounter; }
            set { aFTDSCCounter = value; RaisePropertyChanged("AFTDSCCounter"); }
        }

        public short AFTSitStatus
        {
            get { return aFTSitStatus; }
            set { aFTSitStatus = value; RaisePropertyChanged("AFTSitStatus"); }
        }

        public short AFTASCStatus
        {
            get { return aFTASCStatus; }
            set { aFTASCStatus = value; RaisePropertyChanged("AFTASCStatus"); }
        }

        public short AFTDSCStatus
        {
            get { return aFTDSCStatus; }
            set { aFTDSCStatus = value; RaisePropertyChanged("AFTDSCStatus"); }
        }

        public short AFTWalkStatus
        {
            get { return aFTWalkStatus; }
            set { aFTWalkStatus = value; RaisePropertyChanged("AFTWalkStatus"); }

        }
        public short StairsASCSpeed
        {
            get { return stairsASCSpeed; }
            set { stairsASCSpeed = value; RaisePropertyChanged("StairsASCSpeed"); }
        }
        public short StairsDSCSpeed
        {
            get { return stairsDSCSpeed; }
            set { stairsDSCSpeed = value; RaisePropertyChanged("StairsDSCSpeed"); }
        }
        public short Placing_RH_on_the_stair
        {
            get { return placing_RH_on_the_stair; }
            set { placing_RH_on_the_stair = value; RaisePropertyChanged("Placing_RH_on_the_stair"); }
        }
        public short Placing_RK_on_the_stair
        {
            get { return placing_RK_on_the_stair; }
            set { placing_RK_on_the_stair = value; RaisePropertyChanged("Placing_RH_on_the_stair"); }
        }
        public short StepLength
        {
            get { return stepLength; }
            set { stepLength = value; RaisePropertyChanged("StepLength"); }
        }
        public UInt32 StairCounter
        {
            get { return stairCounter; }
            set { stairCounter = value; RaisePropertyChanged("StairCounter"); }
        }
        public bool CurrentHandleAlgo
        {
            get { return currentHandleAlgo; }
            set { currentHandleAlgo = value; RaisePropertyChanged("CurrentHandleAlgo"); }
        }
        public short UpperLegLength
        {
            get { return upperLegLength; }
            set { upperLegLength = value; RaisePropertyChanged("UpperLegLength"); }
        }
        public short LowerLegLength
        {
            get { return lowerLegLength; }
            set { lowerLegLength = value; RaisePropertyChanged("LowerLegLength"); }
        }
        public short SoleHeight
        {
            get { return soleHeight; }
            set { soleHeight = value; RaisePropertyChanged("SoleHeight"); }
        }
        public short ShoeSize
        {
            get { return shoeSize; }
            set { shoeSize = value; RaisePropertyChanged("ShoeSize"); }
        }
        public short StairHeight
        {
            get { return stairHeight; }
            set { stairHeight = value; RaisePropertyChanged("StairHeight"); }
        }

        public short PicSWVersion
        {
            get { return picSWVersion; }
            set { picSWVersion = value; RaisePropertyChanged("PicSWVersion"); }
        }
        //New GUI6 Look and Feel design
        public short UnitsSelection
        {
            get { return unitsSelection; }
            set { unitsSelection = value; RaisePropertyChanged("UnitsSelection"); }
        }

        public bool Gender
        {
            get { return gender; }
            set { gender = value; RaisePropertyChanged("Gender"); }
        }
        public UInt32 User_ID
        {
            get { return user_ID; }
            set { user_ID = value; RaisePropertyChanged("User_ID"); }
        }
        public short Weight
        {
            get { return weight; }
            set { weight = value; RaisePropertyChanged("Weight"); }
        }
        public short Height
        {
            get { return height; }
            set { height = value; RaisePropertyChanged("Height"); }
        } 
        public short UpperStrapHoldersSize
        {
            get { return upperStrapHoldersSize; }
            set { upperStrapHoldersSize = value; RaisePropertyChanged("UpperStrapHoldersSize"); }
        }
        public short UpperStrapHoldersPosition
        {
            get { return upperStrapHoldersPosition; }
            set { upperStrapHoldersPosition = value; RaisePropertyChanged("UpperStrapHoldersPosition"); }
        }
        public short AboveKneeBracketSize
        {
            get { return aboveKneeBracketSize; }
            set { aboveKneeBracketSize = value; RaisePropertyChanged("AboveKneeBracketSize"); }
        }
        public short AboveKneeBracketPosition
        {
            get { return aboveKneeBracketPosition; }
            set { aboveKneeBracketPosition = value; RaisePropertyChanged("AboveKneeBracketPosition"); }
        }
        public short FrontKneeBracketAssemble
        {
            get { return frontKneeBracketAssemble; }
            set { frontKneeBracketAssemble = value; RaisePropertyChanged("FrontKneeBracketAssemble"); }
        }
        public short FrontKneeBracketAnteriorPosition
        {
            get { return frontKneeBracketAnteriorPosition; }
            set { frontKneeBracketAnteriorPosition = value; RaisePropertyChanged("FrontKneeBracketAnteriorPosition"); }
        }
        public short FrontKneeBracketLateralPosition
        {
            get { return frontKneeBracketLateralPosition; }
            set { frontKneeBracketLateralPosition = value; RaisePropertyChanged("FrontKneeBracketLateralPosition"); }
        }
        public short PelvicSize
        {
            get { return pelvicSize; }
            set { pelvicSize = value; RaisePropertyChanged("PelvicSize"); }
        }
        public short PelvicAnteriorPosition
        {
            get { return pelvicAnteriorPosition; }
            set { pelvicAnteriorPosition = value; RaisePropertyChanged("PelvicAnteriorPosition"); }
        }
        public short PelvicVerticalPosition
        {
            get { return pelvicVerticalPosition; }
            set { pelvicVerticalPosition = value; RaisePropertyChanged("PelvicVerticalPosition"); }
        }
        public short UpperLegHightSize
        {
            get { return upperLegHightSize; }
            set { upperLegHightSize = value; RaisePropertyChanged("UpperLegHightSize"); }
        }
        public short LowerLegHightSize
        {
            get { return lowerLegHightSize; }
            set { lowerLegHightSize = value; RaisePropertyChanged("LowerLegHightSize"); }
        }
        public short FootPlateType1
        {
            get { return footPlateType1; }
            set { footPlateType1 = value; RaisePropertyChanged("FootPlateType1"); }
        }
        public short FootPlateType2
        {
            get { return footPlateType2; }
            set { footPlateType2 = value; RaisePropertyChanged("FootPlateType2"); }
        }
        public UInt32 FootPlateNoOfRotation
        {
            get { return footPlateNoOfRotation; }
            set { footPlateNoOfRotation = value; RaisePropertyChanged("FootPlateNoOfRotation"); }
        }
        public short AFTStopsStatus
        {
            get { return aFTStopsStatus; }
            set { aFTStopsStatus = value; RaisePropertyChanged("AFTStopsStatus"); }  
        }
        public short INFBatteriesLevel
        {
            get { return iNFBatteriesLevel; }
            set { iNFBatteriesLevel = value; RaisePropertyChanged("INFBatteriesLevel"); }         
        }
        public short Ack_Movement_Point
        {
            get { return ack_Movement_Point; }
            set { ack_Movement_Point = value; RaisePropertyChanged("Ack_Movement_Point"); }  
        }
        public bool Enable_First_Step_Rewalk6
        {
            get { return enable_first_step_rewalk6; }
            set { enable_first_step_rewalk6 = value; RaisePropertyChanged("Enable_First_Step_Rewalk6"); }  
        }
        public short SystemResrvedParmeter5
        {
            get { return systemResrvedParmeter5; }
            set { systemResrvedParmeter5 = value; RaisePropertyChanged("SystemResrvedParmeter5"); }  
        }
        public short SystemResrvedParmeter6
        {
            get { return systemResrvedParmeter6; }
            set { systemResrvedParmeter6 = value; RaisePropertyChanged("SystemResrvedParmeter6"); }  
        }
        public short SystemResrvedParmeter7
        {
            get { return systemResrvedParmeter7; }
            set { systemResrvedParmeter7 = value; RaisePropertyChanged("SystemResrvedParmeter7"); }  
        }
        public short SystemResrvedParmeter8
        {
            get { return systemResrvedParmeter8; }
            set { systemResrvedParmeter8 = value; RaisePropertyChanged("SystemResrvedParmeter8"); }  
        }
        public short SystemResrvedParmeter9
        {
            get { return systemResrvedParmeter9; }
            set { systemResrvedParmeter9 = value; RaisePropertyChanged("SystemResrvedParmeter9"); }  
        }
        public short SystemResrvedParmeter10
        {
            get { return systemResrvedParmeter10; }
            set { systemResrvedParmeter10 = value; RaisePropertyChanged("SystemResrvedParmeter10"); }  
        
        }
        /// <summary>
        /// ////strings 
        /// </summary>
        /// 
        
        public string UpperStrapHolderSizeString
        {
            get { return upperStrapHolderSizeString; }
            set { upperStrapHolderSizeString = value; RaisePropertyChanged("UpperStrapHolderSizeString"); }
        }

        public string UpperStrapHolderTypeString
        {
            get { return upperStrapHolderTypeString; }
            set { upperStrapHolderTypeString = value; RaisePropertyChanged("UpperStrapHolderTypeString"); }
        }
        public string AboveKneeBracketTypeString
        {
            get { return aboveKneeBracketTypeString; }
            set { aboveKneeBracketTypeString = value; RaisePropertyChanged("AboveKneeBracketTypeString"); }
        }
        public string AboveKneeBracketAnteriorString
        {
            get { return aboveKneeBracketAnteriorString; }
            set { aboveKneeBracketAnteriorString = value; RaisePropertyChanged("AboveKneeBracketAnteriorString"); }
        }
        public string KneeBracketTypeString
        {
            get { return kneeBracketTypeString; }
            set { kneeBracketTypeString = value; RaisePropertyChanged("KneeBracketTypeString"); }
        }
        public string KneeBracketAnteriorPositionString
        {
            get { return kneeBracketAnteriorPositionString; }
            set { kneeBracketAnteriorPositionString = value; RaisePropertyChanged("KneeBracketAnteriorPositionString"); }
        }
        public string KneeBracketLateralPositionString
        {
            get { return kneeBracketLateralPositionString; }
            set { kneeBracketLateralPositionString = value; RaisePropertyChanged("KneeBracketLateralPositionString"); }
        }
        public string PelvicSizeString
        {
            get { return pelvicSizeString; }
            set { pelvicSizeString = value; RaisePropertyChanged("PelvicSizeString"); }
        }

        public string PelvicAnteriorPositionString
        {
            get { return pelvicAnteriorPositionString; }
            set { pelvicAnteriorPositionString = value; RaisePropertyChanged("PelvicAnteriorPositionString"); }
        }
        public string PelvicVerticalPositionString
        {
            get { return pelvicVerticalPositionString; }
            set {pelvicVerticalPositionString = value; RaisePropertyChanged("PelvicVerticalPositionString"); }
        }
        public string FootPlateTypeString
        {
            get { return footPlateTypeString; }
            set { footPlateTypeString = value; RaisePropertyChanged("FootPlateTypeString"); }
        }

        public string FootPlateSizeString
        {
            get { return footPlateSizeString; }
            set { footPlateSizeString = value; RaisePropertyChanged("FootPlateSizeString"); }
        }
        public string FootPlateDorsiFlexionPositionString
        {
            get { return footPlateDorsiFlexionPositionString; }
            set { footPlateDorsiFlexionPositionString = value; RaisePropertyChanged("FootPlateDorsiFlexionPositionString"); }
        }
        public string ASCSpeedString
        {
            get { return aSCSpeedString; }
            set { aSCSpeedString = value; RaisePropertyChanged("ASCSpeedString"); }
        }
        public string DSCSpeedString
        {
            get { return dSCSpeedString; }
            set { dSCSpeedString = value; RaisePropertyChanged("DSCSpeedString"); }
        }      
               
            
        #endregion

        public void OnDeserialization()
        {
            return;
        }

        #region Private Fields

        private short kneeAngle;
        private short hipAngle;
        private short backHipAngle;
        private short maxVelocity;
        private short tiltDelta;
        private short tiltTimeout;
        private bool switchFSRs;
        private short fSRThreshold;
        private short fSRWalkTimeout;
        private short delayBetweenSteps;
        private bool buzzerBetweenSteps;
        private bool record;
        private short sampleTime;
        private bool safetyWhileStanding;
        private short xThreshold;
        private short yThreshold;

        private short lhAsc;
        private short lkAsc;
        private short rhAsc;
        private short rkAsc;
        private short lhDsc;
        private short lkDsc;
        private short rhDsc;
        private short rkDsc;

        private short device;
        private int dsp;
        private int motor;
        private int remote;
        private short fpga;
        private int boot;
        private ushort rcAddress = 0xffff;
        private ushort rcId = 0xffff;
        private short INF_BER = -1;
        private short RC_BER = -1;
        private short RF_progress = 0;

        private TestStatus dspSoftwareProgrammed;
        private TestStatus systemCalbirationStatus;
        private TestStatus serialNumberStatus;

        private short sn1;
        private short sn2;
        private short sn3;

        private long infSN;
        private long rcSN;
        private long lhSN;
        private long lkSN;
        private long rhSN;
        private long rkSN;
        private short WalkCurrentThreshold;
        private short StairsCurrentThreshold;
        private short sysType;
        private bool stairsEnabled;
        [NonSerialized]
        private LogLevel logLevel = new LogLevel();
        private short firstStepFlexion;
        private bool enableFirstStep = false;

        private UInt32 stepCounter;
        private short sysErrorType;
        private UInt32 enableSystemStuckOnError_int;
        private short switchingTime;
        private short aFTSitCounter;
        private short aFTASCCounter;
        private short aFTDSCCounter;
        private short aFTSitStatus;
        private short aFTASCStatus;
        private short aFTDSCStatus;
        private short aFTWalkStatus;
        private short stairsASCSpeed;
	    private short stairsDSCSpeed;
	    private short placing_RH_on_the_stair;
        private short placing_RK_on_the_stair;
	    private UInt32 stairCounter;
	    private short stepLength;
        private bool currentHandleAlgo;
        private short upperLegLength;
        private short lowerLegLength;
        private short soleHeight;
        private short shoeSize;
        private short stairHeight;
        private short picSWVersion;
        //New GUI6 Look and Feel design
        private short unitsSelection;
        private bool gender;
        private UInt32 user_ID;
        private short weight;
        private short height;
        private short upperStrapHoldersSize;
        private short upperStrapHoldersPosition;
        private short aboveKneeBracketSize;
        private short aboveKneeBracketPosition;
        private short frontKneeBracketAssemble;
        private short frontKneeBracketAnteriorPosition;
        private short frontKneeBracketLateralPosition;
        private short pelvicSize;
        private short pelvicAnteriorPosition;
        private short pelvicVerticalPosition;
        private short upperLegHightSize;
        private short lowerLegHightSize;
        private short footPlateType1;
        private short footPlateType2;
        private UInt32 footPlateNoOfRotation;
        private short aFTStopsStatus;
        private short iNFBatteriesLevel;
        private short ack_Movement_Point;
        private bool enable_first_step_rewalk6;
        private short systemResrvedParmeter5;
        private short systemResrvedParmeter6;
        private short systemResrvedParmeter7;
        private short systemResrvedParmeter8;
        private short systemResrvedParmeter9;
        private short systemResrvedParmeter10;

        //write strings to INF
        
        private string upperStrapHolderSizeString="NOT SET";
        private string upperStrapHolderTypeString = "NO TSET";
        private string aboveKneeBracketTypeString = "NOT SET";
        private string aboveKneeBracketAnteriorString = "NOT SET";
        private string kneeBracketTypeString = "NOT SET";
        private string kneeBracketAnteriorPositionString = "NOT SET";
        private string kneeBracketLateralPositionString = "NOT SET";
        private string pelvicSizeString = "NOT SET";
        private string pelvicAnteriorPositionString = "NOT SET";
        private string pelvicVerticalPositionString = "NOT SET";
        private string footPlateTypeString = "NOT SET";
        private string footPlateSizeString = "NOT SET";
        private string footPlateDorsiFlexionPositionString = "NOT SET";
        private string aSCSpeedString = "NOT SET";
        private string dSCSpeedString = "NOT SET";
        #endregion
    }

     

}


