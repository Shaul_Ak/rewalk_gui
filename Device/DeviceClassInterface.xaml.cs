﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rewalk.Device
{
    /// <summary>
    /// Interaction logic for DeviceClassInterface.xaml
    /// </summary>
    public partial class DeviceClassInterface : UserControl
    {
        public DeviceClassInterface()
        {
            InitializeComponent();
        }
        public void ShowMessageBox(string detecion)
        {
            MessageBox.Show(detecion, "Confirmation", MessageBoxButton.OK, MessageBoxImage.Warning);
        
        }
    }
}
