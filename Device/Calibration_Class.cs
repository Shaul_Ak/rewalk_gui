﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rewalk.Device
{
    public class Calibration_Class
    {
        private static Calibration_Class CalibInstance = new Calibration_Class();

        public static Calibration_Class CALIBINSTANCE
        {
            get { return CalibInstance; }
            set { CalibInstance = value; }
        }
        private Calibration_Class()
        {
        }


        public void RequestCalibrationStatus(CalibrationData data)
        {
            DeviceInterop.TestData td = DeviceInterop.GetCalibrationStatus();
            if (td.STATUS == Status.Operation_passed)
            {
                if (data.LH.IsChecked)
                    data.LH.ErrorCode = td.LH;

                if (data.LK.IsChecked)
                    data.LK.ErrorCode = td.LK;

                if (data.RH.IsChecked)
                    data.RH.ErrorCode = td.RH;

                if (data.RK.IsChecked)
                    data.RK.ErrorCode = td.RK;
            }
            else
            {
                throw new DeviceOperationException(DeviceManager.INSTANCE.Status2String(td.STATUS));
            }
        }
        public void StartCalibration(CalibrationData data)
        {
            Status status = DeviceInterop.StartCalibration(data.LH.IsChecked, data.LK.IsChecked, data.RH.IsChecked, data.RK.IsChecked);
            if (status != Status.Operation_passed)
            {
                throw new DeviceOperationException(DeviceManager.INSTANCE.Status2String(status));
                
            }

        }
       

      
    }
}
