﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Rewalk.Device
{
    public class OnlineData : INotifyPropertyChanged
    {
        public short LHAngle
        {
            get { return lhAngle; }
            set { lhAngle = value; RaisePropertyChanged("LHAngle"); }
        }

        public short LKAngle
        {
            get { return lkAngle; }
            set { lkAngle = value; RaisePropertyChanged("LKAngle"); }
        }

        public short RHAngle
        {
            get { return rhAngle; }
            set { rhAngle = value; RaisePropertyChanged("RHAngle"); }
        }

        public short RKAngle
        {
            get { return rkAngle; }
            set { rkAngle = value; RaisePropertyChanged("RKAngle"); }
        }

        public short TiltXAngle
        {
            get { return tiltXAngle; }
            set { tiltXAngle = value; RaisePropertyChanged("TiltXAngle"); }
        }

        public short TiltYAngle
        {
            get { return tiltYAngle; }
            set { tiltYAngle = value; RaisePropertyChanged("TiltYAngle"); }
        }

        public short FsrRight1
        {
            get { return fsrRight1; }
            set { fsrRight1 = value; RaisePropertyChanged("FsrRight1"); }
        }

        public short FsrRight2
        {
            get { return fsrRight2; }
            set { fsrRight2 = value; RaisePropertyChanged("FsrRight2"); }
        }

        public short FsrRight3
        {
            get { return fsrRight3; }
            set { fsrRight3 = value; RaisePropertyChanged("FsrRight3"); }
        }

        public short FsrLeft1
        {
            get { return fsrLeft1; }
            set { fsrLeft1 = value; RaisePropertyChanged("FsrLeft1"); }
        }

        public short FsrLeft2
        {
            get { return fsrLeft2; }
            set { fsrLeft2 = value; RaisePropertyChanged("FsrLeft2"); }
        }

        public short FsrLeft3
        {
            get { return fsrLeft3; }
            set { fsrLeft3 = value; RaisePropertyChanged("FsrLeft3"); }
        }

        public short MainBattery
        {
            get { return mainBattery; }
            set { mainBattery = value; RaisePropertyChanged("MainBattery"); }
        }

        public short AuxBattery
        {
            get { return auxBattery; }
            set { auxBattery = value; RaisePropertyChanged("AuxBattery"); }
        }

        public short Custom1
        {
            get { return custom1; }
            set { custom1 = value; RaisePropertyChanged("Custom1"); }
        }

        public short Custom2
        {
            get { return custom2; }
            set { custom2 = value; RaisePropertyChanged("Custom2"); }
        }

        public short Custom3
        {
            get { return custom3; }
            set { custom3 = value; RaisePropertyChanged("Custom3"); }
        }

        public short Custom4
        {
            get { return custom4; }
            set { custom4 = value; RaisePropertyChanged("Custom4"); }
        }

        public short Custom5
        {
            get { return custom5; }
            set { custom5 = value; RaisePropertyChanged("Custom5"); }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion

        private short lhAngle;
        private short lkAngle;
        private short rhAngle;
        private short rkAngle;
        private short tiltXAngle;
        private short tiltYAngle;
        private short fsrRight1;
        private short fsrRight2;
        private short fsrRight3;
        private short fsrLeft1;
        private short fsrLeft2;
        private short fsrLeft3;
        private short mainBattery;
        private short auxBattery;
        private short custom1;
        private short custom2;
        private short custom3;
        private short custom4;
        private short custom5;
    }
}
