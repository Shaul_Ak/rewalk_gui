﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Rewalk.Device
{
    public class Data : INotifyPropertyChanged
    {
        #region Public Methods

        public void Disconnect()
        {
            DeviceManager.INSTANCE.ShutDown();
        }

        public void WriteParameters()
        {

            SerialNumbers_Class.SN_INSTANCE.WriteSerialNumbers(Parameters);
            UpdateParametersClass.UpdateINSTANCE.UpdateParameters(Parameters);
			
           
        }

        public void SetSerialNumbers()
        {
         SerialNumbers_Class.SN_INSTANCE.WriteSerialNumbers(Parameters);
        }

        public void ReadData()
        {
            Parameters = ReadParametersClass.READINSTANCE.ReadParameters();
            
        }

        public void SetRCAddressID()
        {
            RCNewID = parameters.RCId;
            RCNewAddress = parameters.RCAddress;
            DeviceManager.INSTANCE.WriteRCAddressandID(parameters.RCAddress, parameters.RCId);
        }

        public bool RequestRCAddressIDStatus()
        {
            DeviceInterop.RCAddressIDData td;
            td = DeviceManager.INSTANCE.RequestWriteRCAddressandIDStatus();
            if ((RCNewAddress == td.RC_ADDRESS) && (RCNewID == td.RC_ID__))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CheckUSBConnected()
        {
            return DeviceManager.INSTANCE.CheckUSBConnected();
            
        }

        public void SetINFAddressID()
        {
            DeviceManager.INSTANCE.WriteINFAddressandID(parameters.RCAddress, parameters.RCId);
        }

        public void SetINFAddress()
        {
            DeviceManager.INSTANCE.WriteINFAddress(parameters.RCAddress);
        }

		public void SetINFId()
        {
            DeviceManager.INSTANCE.WriteINFId(parameters.RCId);
        }

        public void RestoreParameters()
        {
            Parameters =  ReadParametersClass.READINSTANCE.RestoreParameters();
        }

        public void DownloadDSP(byte[] data)
        {
            DeviceManager.INSTANCE.DownloadDSP(data);
        }

        public void DownloadMCU(byte[] data)
        {
            DeviceManager.INSTANCE.DownloadMCU(data);
        }

        public void DownloadRC(byte[] data)
        {
            DeviceManager.INSTANCE.DownloadRC(data);
        }

        public void ReadOnlineData()
        {
            Online = Test_Class.TESTINSTANCE.ReadOnlineData();
        }

        public byte[] RunCustomCommand(int command, byte[] data)
        {
            return DeviceManager.INSTANCE.RunCustomCommand(command, data);
        }
        public void StartTest()
        {           
            Test_Class.TESTINSTANCE.StartTest();
        }

        public bool RequestTestStatus()
        {
            Test_Class.TESTINSTANCE.RequestTestStatus(testData);
          
            
            return testData.INF != TestStatus.InProgress && 
                   testData.RC  != TestStatus.InProgress &&
                   testData.LH  != TestStatus.InProgress &&
                   testData.LK  != TestStatus.InProgress &&
                   testData.RH  != TestStatus.InProgress &&
                   testData.RK  != TestStatus.InProgress;
        }

        public void StartCalibration()
        {
            Calibration_Class.CALIBINSTANCE.StartCalibration(calibrationData);
        }



        public TestStatus RequestCalibrationStatus(bool lh, bool lk, bool rh, bool rk) // parasoft-suppress  OOM.CYCLO "Coding style - method logic was verified"
        {
            Calibration_Class.CALIBINSTANCE.RequestCalibrationStatus(calibrationData);
            if ((calibrationData.LH.Status == TestStatus.Fail && lh ) ||
                (calibrationData.LK.Status == TestStatus.Fail && lk )||
                (calibrationData.RH.Status == TestStatus.Fail && rh )||
                (calibrationData.RK.Status == TestStatus.Fail && rk) )
                return TestStatus.Fail;
            else if ( (calibrationData.LH.Status == TestStatus.Success || !lh) &&
                      (calibrationData.LK.Status == TestStatus.Success || !lk) &&
                      (calibrationData.RH.Status == TestStatus.Success || !rh) &&
                      (calibrationData.RK.Status == TestStatus.Success || !rk))
                return TestStatus.Success;
            else
                return TestStatus.InProgress;
             
        }

        public bool RequestTestEncoderStatus()
        {
            Calibration_Class.CALIBINSTANCE.RequestCalibrationStatus(calibrationData);
            return calibrationData.LH.Status != TestStatus.InProgress &&
                calibrationData.LK.Status != TestStatus.InProgress &&
                calibrationData.RH.Status != TestStatus.InProgress &&
                calibrationData.RK.Status != TestStatus.InProgress;
        }

        public bool EncoderTestStatus()
        {
            Calibration_Class.CALIBINSTANCE.RequestCalibrationStatus(calibrationData);
            

            return calibrationData.LH.Status == TestStatus.Success ||
                calibrationData.LK.Status == TestStatus.Success ||
                calibrationData.RH.Status == TestStatus.Success ||
                calibrationData.RK.Status == TestStatus.Success;
        }

       

        public void SaveLogFile(string file)
        {
            if( (short)parameters.DSP >= 0x311 )
                DeviceManager.INSTANCE.SaveLogFile(file, true);
            else
                DeviceManager.INSTANCE.SaveLogFile(file, false);
        }

        public void SaveFlashLogFile(string file)
        {
            DeviceManager.INSTANCE.SaveFlashLogFile(file);
        }

        public void ClearSystemError()
        {
            DeviceManager.INSTANCE.ClearSystemError();
        }

        public void StartRFTest()
        {
            DeviceManager.INSTANCE.RFStartTest();
        }

        public void ResetRFTest()
        {
            parameters.RFProgress = 0;
        }

        public short GetRCErrRate()
        {
            return parameters.RCBER;
        }

        public short GetINFErrRate()
        {
            return parameters.INFBER;
        }

        public UInt32 ResetStepsCounter()
        {
            DeviceManager.INSTANCE.ResetStepCounter();
            return parameters.StepCounter = DeviceManager.INSTANCE.ReadStepCounter();
        }
        public string ClearLogFile()//saman
        {
            string ClearLogFilesStatus = DeviceManager.INSTANCE.ClearlogFile(); 
            return ClearLogFilesStatus;//ClearLogFilesStatus;
            //parameters.StepCounter = DeviceManager.INSTANCE.ReadStepCounter();
        }
        public UInt32 ResetStairsCounter()
        {
            DeviceManager.INSTANCE.ResetStairCounter();
            return parameters.StairCounter = DeviceManager.INSTANCE.ReadStairCounter();

        }
        
        public bool RequestRFTestStatus()
        {
            DeviceInterop.RFTestData td;
            td = DeviceManager.INSTANCE.RequestRFTestStatus();
            if (td.NUMOFPACKETS >= 50 )
            {
                parameters.RCBER = td.RC_BER;
                parameters.INFBER = td.INF_BER;
                parameters.RFProgress = (short)(2*td.NUMOFPACKETS); // in percentage
                return true;
            }
            else
            {
                parameters.RFProgress = (short)(2 * td.NUMOFPACKETS); // in percentage
                return false;
            }
        }

        public short GetErrorStatus()
        {
            DeviceInterop.SystemErrorData td;
            td = DeviceManager.INSTANCE.RequestSystemErrorStatus();
            return td.ERRORTYPE;
            
           
        }

     

      /*  public void SendDebugCommand(string command)
        {
            DeviceManager.INSTANCE.SendDebugCommand(command);
        }
        
        */
        #endregion

        #region Public Properties

        public DeviceParameters Parameters
        {
            get { return parameters; }
            set { parameters = value; RaisePropertyChanged("Parameters"); }
        }


        public TestData TestData
        {
            get { return testData; }
            set { testData = value; RaisePropertyChanged("TestData"); }
        }



        public CalibrationData CalibrationData
        {
            get { return calibrationData; }
            set { calibrationData = value; RaisePropertyChanged("CalibrationData"); }
        }

   /*     public AFTData AFTData
        {
            get { return aftData; }
            set { aftData = value; RaisePropertyChanged("AftData"); }
        }
        */
        public OnlineData Online
        {
            get { return online; }
            set { online = value; RaisePropertyChanged("Online"); }
        }

        
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

              
        #endregion


        private DeviceParameters parameters = new DeviceParameters();
        private TestData testData = new TestData();
        private CalibrationData calibrationData = new CalibrationData();
        private OnlineData online = new OnlineData();
       // private  AFTData aftData = new AFTData();
        private ushort RCNewID, RCNewAddress;

    }
}
