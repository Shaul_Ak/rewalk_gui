﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rewalk.Device
{
    public class Test_Class
    {
        private static Test_Class TestInstance = new Test_Class();

        public static Test_Class TESTINSTANCE
        {
            get { return TestInstance; }
            set { TestInstance = value; }
        }
        private Test_Class()
        {
        }

     

        public OnlineData ReadOnlineData()
        {
            DeviceInterop.OnlineData od = DeviceInterop.ReadOnlineData();
            if (od.STATUS == Status.Operation_passed)
            {
                OnlineData data = new OnlineData();
                data.LHAngle = od.LHANGLE;
                data.LKAngle = od.LKANGLE;
                data.RHAngle = od.RHANGLE;
                data.RKAngle = od.RKANGLE;
                data.TiltXAngle = od.TILTXANGLE;
                data.TiltYAngle = od.TILTYANGLE;
                data.FsrRight1 = od.FsrRight1;
                data.FsrRight2 = od.FsrRight2;
                data.FsrRight3 = od.FsrRight3;
                data.FsrLeft1 = od.FsrLeft1;
                data.FsrLeft2 = od.FsrLeft2;
                data.FsrLeft3 = od.FsrLeft3;
                data.MainBattery = od.MAINBATTERY;
                data.AuxBattery = od.AUXBATTERY;
                data.Custom1 = od.CUSTOM1;
                data.Custom2 = od.CUSTOM2;
                data.Custom3 = od.CUSTOM3;
                data.Custom4 = od.CUSTOM4;
                data.Custom5 = od.CUSTOM5;

                return data;
            }
            else
            {
               throw new DeviceOperationException(DeviceManager.INSTANCE.Status2String(od.STATUS));
            }
        }
      

        public void StartTest()
        {
            Status status = DeviceInterop.StartTest();
            if (status != Status.Operation_passed)
            {
              throw new DeviceOperationException(DeviceManager.INSTANCE.Status2String(status));
            }
        }

        public void RequestTestStatus(TestData testData)
        {
            DeviceInterop.TestData td = DeviceInterop.GetTestStatus();
            if (td.STATUS == Status.Operation_passed)
            {
                testData.ErrorCodeINF = td.INF;
                testData.ErrorCodeRC = td.RC;
                testData.ErrorCodeLH = td.LH;
                testData.ErrorCodeLK = td.LK;
                testData.ErrorCodeRH = td.RH;
                testData.ErrorCodeRK = td.RK;
            }
            else
            {
              throw new DeviceOperationException(DeviceManager.INSTANCE.Status2String(td.STATUS));
            }
        }
    }
}
