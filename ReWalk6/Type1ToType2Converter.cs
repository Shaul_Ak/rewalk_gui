﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Collections.ObjectModel;
using System.Xml;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Xml.Serialization;
using Microsoft.Win32;
using Rewalk.Device;
using System.Diagnostics;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using RewalkControls;
using System.Threading;
using System.Windows.Input;
using System.Security;
using System.Windows.Media;
using System.Windows.Documents;
using System.Net;
using System.Net.Mail;
using System.Windows.Markup;
using FileParser;

namespace ReWalk6
{
    /************
     * Multi value converter for MultiBinding Implementation 
     * Written by rasem fadela 6.2.2015
     * ////////////////////////////////////////////
     */
    public sealed class Type1ToType2Converter : IMultiValueConverter  
    {
              
        #region IValueConverter Members
    
       
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture) // parasoft-suppress  METRICS.MCCC "Methods executing atomic functions" METRICS.MLOC "Methods executing atomic functions"
        {


            int FieldIndex;
            bool cc = int.TryParse(string.Format("{0}", value[0]), out FieldIndex);
           
            string systype = value[1] as string;
            string FieldTag = parameter as string;
            string[] FieldArray = { "" };
          
            XmlDocument deviceSettings = new XmlDocument();
            try
            {
                deviceSettings.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");
            }
            catch (Exception exception)
            {
                throw new DeviceOperationException(exception.Message);
               
            }
           
        
            
            try
            {

               

                    //STAIRSDSCSPEED
                    if (FieldTag == "STAIRSASCSPEED")
                    {
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/ASCSpeed").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        if (FieldIndex >= 0 )
                        {
                        int ASCSpeed = Array.IndexOf(FieldArray, FieldIndex.ToString());
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/ASCSpeedStrings").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        return (string)Application.Current.FindResource(FieldArray[ASCSpeed]);
                        }
                        else
                        {
                            return ((string)Application.Current.FindResource("NOT SET"));
                        }
                    }


                    //STAIRSDSCSPEED
                    if (FieldTag == "STAIRSDSCSPEED")
                    {

                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/DSCSpeed").Attributes["Values"].Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                        if (FieldIndex >= 0)
                        {
                        int DSCSpeed = Array.IndexOf(FieldArray, FieldIndex.ToString());
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/DSCSpeedStrings").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        return (string)Application.Current.FindResource(FieldArray[DSCSpeed]);
                        }
                        else
                        {
                            return ((string)Application.Current.FindResource("NOT SET"));
                        }
                    }

                    if (FieldTag == "STEPSCOUNTER" || FieldTag == "STAIRSCOUNTER")
                    {
                       // int counter = (int)value[1];
                       // if ( (string)value[0] != "Can receive data.")
                            return (string)value[0];
                  
                       // else
                         //   return FieldIndex.ToString();
                    }
                if (FieldTag == "STROKESYSTEMTYPEVERSION" )
                {
                    // int counter = (int)value[1];
                    if ((string)value[1] != "Can receive data.")
                        return "";

                    else
                        return ("STROKEV" + FieldIndex.ToString());
                }

                if (FieldTag == "STROKE_SESIONTIME")
                {
                    // int counter = (int)value[1];
                    if ((string)value[1] != "Can receive data.")
                        return "";

                   
                }
                if (FieldTag == "STROKE_WALKINGSPEEDP")
                {
                    // int counter = (int)value[1];
                    if ((string)value[1] != "Can receive data.")
                        return "";

                    else
                        return (FieldIndex.ToString());
                }
                //Select system type
                if (systype == "Rewalk-R" || systype == "Rewalk-I" || systype == "Rewalk-P1.0" || systype == "Rewalk-P")
                {
                //Select field to display
                switch (FieldTag)
                {
                    case "UPPERSTRAPHOLDERSSIZE":
                        return "N/A";
                        // break;
                    case "UPPERSTRAPHOLDERSTYPE":
                        return "N/A";
                       // break;
                    case "ABOVEKNEEBRACKETTYPE":
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RAboveKneeBracketType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        break;
                    case "ABOVEKNEEBRACKETANTERIOR":
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RAboveKneeBracketAnterior").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        break;
                    case "KNEEBRACKETTYPE":
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RKneeBracketType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        break;
                    case "KNEEBRACKETANTERIOR":
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RKneeBracketAnteriorPosition").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        break;
                    case "KNEEBRACKETLATERAL":
                        return "N/A";
                        
                    case "PELVICSIZE":
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RPelvicSize").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        break;
                    case "PELVICANTERIOR":
                        return "N/A";
                    case "PELVICVERTICAL":
                        return "N/A";
                    case "FOOTPLATETYPE":
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RFootPlateType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        break;
                    case "FOOTPLATESIZE":
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RFootPlateSize").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        break;
                    case "FOOTPLATEDORSIFLEXION":
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RFootPlateDorsiFlexionPosition").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        break;
                    default:
                        break;
                }
         
            }
            else if (systype == "Rewalk-P6.0")
            {
                //Select field to display
                switch (FieldTag)
                {
                    case "UPPERSTRAPHOLDERSSIZE":
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PUpperStrapHolderSize").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        break;
                    case "UPPERSTRAPHOLDERSTYPE":
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PUpperStrapHolderType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        break;
                    case "ABOVEKNEEBRACKETTYPE":
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PAboveKneeBracketType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        break;
                    case "ABOVEKNEEBRACKETANTERIOR":
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PAboveKneeBracketAnterior").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                       break;
                    case "KNEEBRACKETTYPE":
                       FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PKneeBracketType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                       break;
                    case "KNEEBRACKETANTERIOR":
                       FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PKneeBracketAnteriorPosition").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                       break;
                    case "KNEEBRACKETLATERAL":
                       FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PKneeBracketLateralPosition").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                       break;
                    case "PELVICSIZE":
                       FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PPelvicSize").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                       break;
                    case "PELVICANTERIOR":
                       FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PPelvicAnteriorPosition").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                       break;
                    case "PELVICVERTICAL":
                       FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PPelvicVerticalPosition").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                       break;
                  case "FOOTPLATETYPE":
                       FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PFootPlateType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                       break;
                    case "FOOTPLATESIZE":
                       FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PFootPlateSize").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                       break;
                    case "FOOTPLATEDORSIFLEXION":
                       FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PFootPlateDorsiFlexionPosition").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                       break;
                   default:
                       break;

                }
                
             
            }
            else
            {

                return ((string)Application.Current.FindResource("NOT SET"));
            }

                if (systype == "Rewalk-R" || systype == "Rewalk-I")
                {
                    if (FieldTag == "UPPERLEGLENGTH")
                    {
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RUpperLegLength").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    }
                }
                else if (systype == "Rewalk-P6.0" || systype == "Rewalk-P1.0" || systype == "Rewalk-P")
                {
                    if (FieldTag == "UPPERLEGLENGTH")
                    {
                        FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PUpperLegLength").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    }
                }
                else
                {

                    return ((string)Application.Current.FindResource("NOT SET"));
                }



            if (systype == "Rewalk-R" || systype == "Rewalk-I")
            {
                if (FieldTag == "LOWERLEGLENGTH")
                {
                    FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RLowerLegLength").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                }
            }
            else if (systype == "Rewalk-P6.0" || systype == "Rewalk-P1.0" || systype == "Rewalk-P")
            {
                if (FieldTag == "LOWERLEGLENGTH")
                {
                    FieldArray = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PLowerLegLength").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);                 
                }
            }
            else
            {

                return ((string)Application.Current.FindResource("NOT SET"));
            }

           //Select value

            if (FieldIndex >= 0 && FieldIndex <= (FieldArray.Length - 1))
            {
                return (string)Application.Current.FindResource(FieldArray[(int)FieldIndex]);
               // return (FieldArray[(int)FieldIndex]);
            }

            else if (FieldIndex >= (FieldArray.Length - 1))
            {

                return (string)Application.Current.FindResource(FieldArray[FieldArray.Length - 1]);
              //  return (FieldArray[FieldArray.Length - 1]);
            }
         
            else
            {
                return ((string)Application.Current.FindResource("NOT SET"));
            }

            }
            catch (Exception exception)
            {
                string ex = exception.Message;
             //   throw new DeviceOperationException(exception.Message);
                return ((string)Application.Current.FindResource("NOT SET"));
            }
           
           
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
