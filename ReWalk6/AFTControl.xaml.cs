﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Win32;
using Rewalk.Device;
using System.Diagnostics;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using RewalkControls;
using System.Threading;
using System.Windows.Input;
using System.Security;
using System.Text;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Windows.Media;

using System.Windows.Documents;

using System.Net;
using System.Net.Mail;
using System.Windows.Markup;
using FileParser;
using ReWalk6;

namespace ReWalk6
{
    /// <summary>
    /// Interaction logic for AFTControl.xaml
    /// </summary>
    ///         
    public enum AFTTestStatus { Unknown, NOTCOMPLETED, PASS, FAIL };
    public enum AFTTests { CONTWALK, CONTSITSTAND, CONTASC, CONTDSC };

    public partial class AFTControl : UserControl
    {
        Data AFTDeviceData = new Data();
      
      
        static AFTControl()
        {
        }
        public   AFTControl() // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions"
        {
            this.InitializeComponent();
            CurrentRunningTestTimer = new DispatcherTimer(TimeSpan.FromMilliseconds(1000), DispatcherPriority.Background, CurrentRunningTest_SM, this.Dispatcher);
            CurrentRunningTestTimer.IsEnabled = false;
            try
            {
              
                XmlDocument deviceSettings = new XmlDocument();
                deviceSettings.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");

                string WalkCalCounts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTWalkCalibrationCounts").Attributes["Values"].Value;
                WalkCalibrationCounts = System.Convert.ToInt16(WalkCalCounts);
                string ASCCalCounts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTASCCalibrationCounts").Attributes["Values"].Value;
                ASCCalibrationCounts = System.Convert.ToInt16(ASCCalCounts);
                string DSCCalCounts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTDSCCalibrationCounts").Attributes["Values"].Value;
                DSCCalibrationCounts = System.Convert.ToInt16(DSCCalCounts);
                string SitStandCalCounts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTSitStandCalibrationCounts").Attributes["Values"].Value;
                SitStandCalibrationCounts = System.Convert.ToInt16(SitStandCalCounts);

                string WalkR1Counts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTWalkRun1Counts").Attributes["Values"].Value;
                WalkRun1Counts = System.Convert.ToInt16(WalkR1Counts);
                string ASCR1Counts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTASCRun1Counts").Attributes["Values"].Value;
                ASCRun1Counts = System.Convert.ToInt16(ASCR1Counts);
                string DSCR1Counts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTDSCRun1Counts").Attributes["Values"].Value;
                DSCRun1Counts = System.Convert.ToInt16(DSCR1Counts);
                string SitStandR1Counts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTSitStandRun1Counts").Attributes["Values"].Value;
                SitStandRun1Counts = System.Convert.ToInt16(SitStandR1Counts);


                string HipMax = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTHipMaxAngle").Attributes["Values"].Value;
                HipMaxAngle = System.Convert.ToInt16(HipMax);
                string HipMin = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTHipMinAngle").Attributes["Values"].Value;
                HipMinAngle = System.Convert.ToInt16(HipMin);

                string KneeMax = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTKneeMaxAngle").Attributes["Values"].Value;
                KneeMaxAngle = System.Convert.ToInt16(KneeMax);
                string KneeMin = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTKneeMinAngle").Attributes["Values"].Value;
                KneeMinAngle = System.Convert.ToInt16(KneeMin);

                string StopsTestCounts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTWalkStopsCounts").Attributes["Values"].Value;
                StopsTestRun1Counts = System.Convert.ToInt16(StopsTestCounts);

            }
            catch (Exception exception)
            {
             //   Logger.Error_MessageBox_AFT("Loading System Configuration Parameters Failed:", "Loading System Configuration", exception.Message);
              // MessageBox.Show("Loading System Configuration Parameters Failed:", "Loading System Configuration");
            }
        }

      

       
      
      

        private int TimeOut_Cnt = 0;

        //Main Proccess
        private void CurrentRunningTest_SM(object sender, EventArgs e) // parasoft-suppress  METRICS.MCCC "Methods executing atomic functions" METRICS.MLOC "Methods executing atomic functions"
        {
            string mmm = "";

            AFTTestStatus status;

            App tempAFT = App.Current as App;
            if (tempAFT != null)
            {
                if (tempAFT.DeptName_Connected)
                {

                    if (AFTCALBIRATE)
                    {

                        ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read actual current from MCU
                        if (enablerecordcurrent)
                        {
                            RecordCurrentsEnable();
                        }
                        string testname = GetTestName();
                    if ((AFTToolConCurrentthr <= (ActualAverageCurrent + 0.5)) && ((AFTToolConCurrentthr >= ActualAverageCurrent - 0.5)) && ActualAverageCurrent !=0 )
                        {
                            stRun.IsEnabled = true;

                            MainWindow MainWindow = new MainWindow();
                            MainWindow.AutoSaveLogfile(" Calbiration");
                            MainWindow.AFTReport((tempAFT.DeptName_TesterFirstName), "", "", "", "", false, "");
                            MainWindow.AFTReport((tempAFT.DeptName_TesterFirstName), AFTTestStatus.PASS.ToString(), "Calibration", testname, "I: " + motors_current, false, "");


                            AFTCALBIRATE = false;
                            Starttool.IsEnabled = true;
                            ActualSavedCurrent = ActualAverageCurrent;
                            mmm = "";

                            MessageBox.Show(("Success" + "\nThe measured average current is: " + ActualAverageCurrent.ToString() + "\n Press Start to run test." + mmm), "AFT Calibration", MessageBoxButton.OK, MessageBoxImage.Information);

                        }
                        else
                        {
                          

                            AFTCALBIRATE = false;
                            if (ActualAverageCurrent < AFTToolConCurrentthr)
                                mmm = "Increase Load!";
                            else if (ActualAverageCurrent > AFTToolConCurrentthr)
                                mmm = "Decrease Load!";


                            MessageBox.Show("Failed " + "\nThe measured current is: " + ActualAverageCurrent.ToString() + "\n" + mmm, "AFT Calibration", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        return;
                    }//AFTCALbirate
                    if (StartWalkTestRunning)
                    {

                        if (AFT_All_in_process || AFT_WALK_in_process)
                        {

                            AFT_WALK_in_process = true;
                            ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read actual current from MCU
                            if ((ActualAverageCurrent <= (ActualSavedCurrent + 0.5)) && (ActualAverageCurrent >= (ActualSavedCurrent - 0.5)) && (tempAFT).DeptName_AFTCONWALKST == 2 && ActualAverageCurrent !=0)
                            {


                                SetTestStatus(AFTTestStatus.PASS, true);

                                MainWindow MainWindow = new MainWindow();
                                MainWindow.AFTReport(((tempAFT).DeptName_TesterFirstName), AFTTestStatus.PASS.ToString(), "End", "Continuous Walk", "I: " + motors_current + "  Steps: " + (tempAFT).DeptName_AFTCONWALKCOUNTER, false, "");

                            }
                            else
                            {

                                SetTestStatus(AFTTestStatus.FAIL, true);
                                Motors_release();
                                MainWindow MainWindow = new MainWindow();
                                MainWindow.AFTReport(((tempAFT).DeptName_TesterFirstName), AFTTestStatus.FAIL.ToString(), "End", "Continuous Walk", "I: " + motors_current + "  Steps: " + (tempAFT).DeptName_AFTCONWALKCOUNTER, false, "");
                                Write_final_status();
                            }
                            if (AFT_WALK_in_process == true && AFT_All_in_process == false)
                            {
                                AFT_WALK_in_process = false;
                                StartWalkTestRunning = false;
                                CurrentRunningTestTimer.IsEnabled = false;
                                return;
                            }
                            //go to DSC
                            if (AFT_All_in_process)
                            {

                                trContinuousStairDSC.SmallLoading.Visibility = Visibility.Visible;
                                Storyboard stContinuousStairDSC = (Storyboard)trContinuousStairDSC.FindResource("SmallLoading");
                                stContinuousStairDSC.Begin();

                                MainWindow MainWindow1 = new MainWindow();

                                MainWindow1.AFTReport(((tempAFT).DeptName_TesterFirstName), "", "Start", "Continuous DSC", ActualAverageCurrent.ToString(), false, "");
                                //send custom command 
                                byte[] data = { 0x5b, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
                                Int16 run1Ccounts = DSCRun1Counts; //DSC
                                data[4] = (byte)(run1Ccounts & 0xFF);
                                data[3] = (byte)(run1Ccounts >> 8);
                                data[5] = 1;
                                data[9] = 100; data[8] = 100;
                                byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                                byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
                                MainWindow1.RunSystem();
                                StartDSCTestRunning = true;
                                AFT_DSC_in_process = true;
                                AFT_WALK_in_process = false;
                                StartWalkTestRunning = false;
                                return;
                            }
                        }

                    } //End Walk
                    if (StartDSCTestRunning)
                    {

                        if (AFT_All_in_process || AFT_DSC_in_process)
                        {
                            //go to ASC


                            ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read actual current from MCU
                            if ((tempAFT).DeptName_AFTCONDSCST == 2 && ActualAverageCurrent !=0) // + 0.5)) && ((ActualAverageCurrent >= ActualSavedCurrent - 0.5)))
                            {

                                SetTestStatus(AFTTestStatus.PASS, true);


                                MainWindow MainWindow = new MainWindow();
                                MainWindow.AFTReport(((tempAFT).DeptName_TesterFirstName), AFTTestStatus.PASS.ToString(), "End", "Continuous DSC", "I: " + motors_current + "  Steps: " + (tempAFT).DeptName_AFTCONDSCCOUNTER, false, "");

                            }
                            else
                            {

                                SetTestStatus(AFTTestStatus.FAIL, true);
                                Motors_release();
                                MainWindow MainWindow = new MainWindow();
                                MainWindow.AFTReport(((tempAFT).DeptName_TesterFirstName), AFTTestStatus.FAIL.ToString(), "End", "Continuous DSC", "I: " + motors_current + "  Steps: " + (tempAFT).DeptName_AFTCONDSCCOUNTER, false, "");
                                Write_final_status();
                            }
                            if (AFT_DSC_in_process == true && AFT_All_in_process == false)
                            {

                                AFT_DSC_in_process = false;
                                StartDSCTestRunning = false;
                                CurrentRunningTestTimer.IsEnabled = false;
                                return;
                            }
                            //go to ASC     
                            trContinuousStairASC.SmallLoading.Visibility = Visibility.Visible;
                            Storyboard stContinuousStairASC = (Storyboard)trContinuousStairASC.FindResource("SmallLoading");
                            stContinuousStairASC.Begin();
                            MainWindow MainWindow2 = new MainWindow();
                            MainWindow2.AFTReport(((tempAFT).DeptName_TesterFirstName), "", "Start", "Continuous ASC", ActualAverageCurrent.ToString(), false, "");
                            //send custom command 
                            byte[] data = { 0x5a, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
                            Int16 run1Ccounts = ASCRun1Counts; //ASC
                            data[4] = (byte)(run1Ccounts & 0xFF);
                            data[3] = (byte)(run1Ccounts >> 8);
                            data[5] = 1;
                            data[9] = 100; data[8] = 100;
                            byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                            byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
                            MainWindow2.RunSystem();
                            StartASCTestRunning = true;
                            StartDSCTestRunning = false;
                            AFT_DSC_in_process = false;
                            AFT_ASC_in_process = true;

                            return;
                        }//end DSC
                    }
                    if (StartASCTestRunning)
                    {

                        //Check ASC

                        if (AFT_All_in_process || AFT_ASC_in_process)
                        {


                            ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read actual current from MCU
                            if ((tempAFT).DeptName_AFTCONASCST == 2 && ActualAverageCurrent !=0)// + 0.5)) && ((ActualAverageCurrent >= ActualSavedCurrent - 0.5)))
                            {

                                SetTestStatus(AFTTestStatus.PASS, true);

                                MainWindow MainWindow = new MainWindow();
                                MainWindow.AFTReport(((tempAFT).DeptName_TesterFirstName), AFTTestStatus.PASS.ToString(), "End", "Continuous ASC", "I: " + motors_current + "  Steps: " + (tempAFT).DeptName_AFTCONASCCOUNTER, false, "");

                            }
                            else
                            {

                                SetTestStatus(AFTTestStatus.FAIL, true);
                                Motors_release();
                                MainWindow MainWindow = new MainWindow();
                                MainWindow.AFTReport(((tempAFT).DeptName_TesterFirstName), AFTTestStatus.FAIL.ToString(), "End", "Continuous ASC", "I: " + motors_current + "  Steps: " + (tempAFT).DeptName_AFTCONASCCOUNTER, false, "");
                                Write_final_status();
                            }


                            if (AFT_ASC_in_process == true && AFT_All_in_process == false)
                            {

                                AFT_ASC_in_process = false;
                                StartASCTestRunning = false;
                                CurrentRunningTestTimer.IsEnabled = false;
                                return;
                            }

                            //go to Sit Stand

                            trContinuousSitStand.SmallLoading.Visibility = Visibility.Visible;
                            Storyboard stContinuousSitStand = (Storyboard)trContinuousSitStand.FindResource("SmallLoading");
                            stContinuousSitStand.Begin();
                            MainWindow MainWindow2 = new MainWindow();
                            MainWindow2.AFTReport(((tempAFT).DeptName_TesterFirstName), "", "Start", "Continuous Sit_Stand", ActualAverageCurrent.ToString(), false, "");
                            //send custom command 
                            byte[] data = { 0x4e, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
                            Int16 run1Ccounts = SitStandRun1Counts;//SitStand
                            data[4] = (byte)(run1Ccounts & 0xFF);
                            data[3] = (byte)(run1Ccounts >> 8);
                            data[5] = 1;
                            data[9] = 100; data[8] = 100;
                            byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                            byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
                            MainWindow2.RunSystem();
                            StartASCTestRunning = false;
                            StartSitStandTestRunning = true;

                            AFT_DSC_in_process = false;
                            AFT_ASC_in_process = false;
                            AFT_SITSTAND_in_process = true;

                            return;
                        } //end Asc
                    }
                    if (StartSitStandTestRunning || AFT_SITSTAND_in_process)
                    {

                        if (AFT_All_in_process || AFT_SITSTAND_in_process)
                        {


                            ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read actual current from MCU
                            if ((tempAFT).DeptName_AFTCONSITSTANDST == 2 && ActualAverageCurrent !=0 )// + 0.5)) && ((ActualAverageCurrent >= ActualSavedCurrent - 0.5)))
                            {

                                SetTestStatus(AFTTestStatus.PASS, true);

                                MainWindow MainWindow = new MainWindow();
                                MainWindow.AFTReport(((tempAFT).DeptName_TesterFirstName), AFTTestStatus.PASS.ToString(), "End", "Continuous Sit_Stand", "I: " + motors_current + "   Steps: " + (tempAFT).DeptName_AFTCONSITSTANDCOUNTER, false, "");

                            }
                            else
                            {

                                SetTestStatus(AFTTestStatus.FAIL, true);
                                Motors_release();
                                MainWindow MainWindow = new MainWindow();
                                MainWindow.AFTReport(((tempAFT).DeptName_TesterFirstName), AFTTestStatus.FAIL.ToString(), "End", "Continuous Sit_Stand", "I: " + motors_current + "  Steps: " + (tempAFT).DeptName_AFTCONSITSTANDCOUNTER, false, "");
                                Write_final_status();
                            }


                            if (AFT_SITSTAND_in_process == true && AFT_All_in_process == false)
                            {

                                AFT_SITSTAND_in_process = false;
                                StartSitStandTestRunning = false;
                                CurrentRunningTestTimer.IsEnabled = false;
                                return;
                            }
                            //go to check current final

                            FinalCurrenttestinWalk();
                            //  Main MainWindow2 = new Main();
                            //   MainWindow2.AFTReport(((App.Current as App).DeptName_TesterFirstName), "", "Start", "Backlash", ActualAverageCurrent.ToString(), false, "");
                            /*  MessageBox.Show("Measure Backlash of each joint and then press Backlash button.", "Backlash", MessageBoxButton.OK, MessageBoxImage.Information);
                  

                              CurrentRunningTestTimer.IsEnabled = true;
                              backlash.IsEnabled = true;
                              ReadEncoders1.IsEnabled = true;
                              ReadEncoders2.IsEnabled = true;*/


                            //send custom command 

                            StartASCTestRunning = false;
                            StartSitStandTestRunning = false;

                            AFT_DSC_in_process = false;
                            AFT_ASC_in_process = false;
                            AFT_SITSTAND_in_process = false;
                            AFT_WALK_in_process = false;
                            AFT_MotorPerformance_in_process = false;
                            AFT_Backlash_in_process = false;

                            return;
                        } //end SitStand  
                    }
                    if (AFT_MotorPerformance_in_process)
                    {


                        if (AFT_All_in_process)
                        {
                            MainWindow MainWindow2 = new MainWindow();
                            MainWindow2.AFTReport(((tempAFT).DeptName_TesterFirstName), "", "Start", "Motors Performance", "", false, "");


                            SetTestStatus(AFTTestStatus.PASS, true);
                            if (CheckMotorPerformance())
                            {
                                SetTestStatus(AFTTestStatus.PASS, true);

                                MainWindow2.AFTReport(((tempAFT).DeptName_TesterFirstName), AFTTestStatus.PASS.ToString(), "End", "Motors Performance", (AFTTestStatus.PASS.ToString() + " -> " + " LH: " + Motors_perf[0] + " LK: " + Motors_perf[1] + " RH: " + Motors_perf[2] + " RK: " + Motors_perf[3]), false, "");

                            }
                            else
                            {
                                SetTestStatus(AFTTestStatus.FAIL, true);
                                Motors_release();
                                MainWindow2.AFTReport(((tempAFT).DeptName_TesterFirstName), AFTTestStatus.FAIL.ToString(), "End", "Motors Performance", (AFTTestStatus.FAIL.ToString() + " -> " + " LH: " + Motors_perf[0] + " LK: " + Motors_perf[1] + " RH: " + Motors_perf[2] + " RK: " + Motors_perf[3]), false, "");

                            }
                        ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read actual current from MCU // Lilach????
                            AFT_MotorPerformance_in_process = false;

                            Motors_release_running();
                            Write_final_status();
                            MessageBox.Show("Test finished.", "Test", MessageBoxButton.OK, MessageBoxImage.Information);
                            AFT_All_in_process = false;
                            CurrentRunningTestTimer.IsEnabled = false;
                            Calibratetool.IsEnabled = false;
                            Backlash_TestRunning = false;







                        }
                    }
                    if (AFT_StopsTest_in_process)
                    {


                        if ((tempAFT).DeptName_AFTSTOPSSTATUS == 2)
                        {
                            trStopsTest.SmallLoading.Visibility = Visibility.Visible;
                            Storyboard stStopsTest = (Storyboard)trStopsTest.FindResource("SmallLoading");
                            stStopsTest.Begin();

                            MainWindow MainWindow2 = new MainWindow();
                            MessageBoxResult MSGresults = MessageBox.Show("Please tie the strap to left leg", "Instruction", MessageBoxButton.YesNo, MessageBoxImage.Question);
                            if (MSGresults == MessageBoxResult.Yes)
                            {
                                MainWindow2.AFTReport(((tempAFT).DeptName_TesterFirstName), "", "Start", "Stops test", ActualAverageCurrent.ToString(), false, "");
                                //send custom command 
                                byte[] data = { 0x4f, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
                                Int16 run1Ccounts = StopsTestRun1Counts; //DSC
                                data[4] = (byte)(run1Ccounts & 0xFF);
                                data[3] = (byte)(run1Ccounts >> 8);
                                data[5] = 1;
                                data[6] = 1;
                                data[9] = 100; data[8] = 100;
                                byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                                byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
                                MainWindow2.RunSystem();
                                //   StopsTest_TestRunning = true;
                                AFT_StopsTest_in_process = false;
                                AFT_WALK_in_process = false;
                                StartWalkTestRunning = false;
                                AFT_StopsTest_in_processLG = true;
                                // CurrentRunningTestTimer.IsEnabled = false;
                                CurrentRunningTestTimer.IsEnabled = true;
                                (tempAFT).DeptName_AFTSTOPSSTATUS = 1;

                            }


                        }


                    }
                    if (AFT_StopsTest_in_processLG)
                    {
                        if ((tempAFT).DeptName_AFTSTOPSSTATUS == 2)
                        {


                            trStopsTest.SmallLoading.Visibility = Visibility.Collapsed;
                            Storyboard stStopsTest = (Storyboard)trStopsTest.FindResource("SmallLoading");
                            stStopsTest.Stop();

                            trStopsTest.OK.Visibility = Visibility.Visible;

                            //    Motors_release_running();
                            // SystemStopsTestPassEllipse.Fill = successBrush;
                            //     Write_final_status();
                            //     MessageBox.Show("Test finished.", "Test", MessageBoxButton.OK, MessageBoxImage.Information);
                            //     AFT_All_in_process = false;
                            //     CurrentRunningTestTimer.IsEnabled = false;
                            //     Calibratetool.IsEnabled = false;
                            //     Backlash_TestRunning = false;

                            MessageBoxResult MSGresults = MessageBox.Show("Please remove the bands", "Instruction", MessageBoxButton.YesNo, MessageBoxImage.Question);
                            if (MSGresults == MessageBoxResult.Yes)
                            {


                                trMotorPerformance.SmallLoading.Visibility = Visibility.Visible;
                                Storyboard stMotorPerformance = (Storyboard)trMotorPerformance.FindResource("SmallLoading");
                                stMotorPerformance.Begin();

                                AFT_StopsTest_in_process = false;
                                AFT_StopsTest_in_processLG = false;
                                AFT_MotorPerformance_in_process = true;
                                // MotorPerformancePassEllipse.Fill = inProgressBrush;
                                SystemStopsTestCheckBox.IsChecked = true;

                            }

                        }


                    }
                    if (Backlash_TestRunning)
                    {


                        if (AFT_All_in_process)
                        {
                            RecordCurrentsDisable();
                            ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read final actual current from MCU

                            string testname = GetTestName();
                        if ((ActualSavedCurrent <= (ActualAverageCurrent + 0.5)) && ((ActualSavedCurrent >= ActualAverageCurrent - 0.5)) && ActualAverageCurrent != 0)
                            {

                                CurrentStatus = AFTTestStatus.PASS;
                                Current_Final_Test_Result = AFTTestStatus.PASS;
                            }
                            else
                            {
                                CurrentStatus = AFTTestStatus.FAIL;
                                Current_Final_Test_Result = AFTTestStatus.FAIL;
                            }

                            if (CurrentStatus == AFTTestStatus.PASS && Backlash_Test_Result == AFTTestStatus.PASS)
                            {
                                status = AFTTestStatus.PASS;
                            }
                            else
                            {
                                status = AFTTestStatus.FAIL;
                            }


                            MainWindow MainWindow = new MainWindow();

                            ResetINFTest();
                            MainWindow.AFTReport((tempAFT).DeptName_TesterFirstName, status.ToString(), "End_", "Test", (CurrentStatus.ToString() + " I: " + motors_current), false, (Backlash_Test_Result.ToString() + " -> LH: " + lh_b.Text + " LK: " + lk_b.Text + " RH: " + rh_b.Text + " RK: " + rk_b.Text + " "));
                            MainWindow.AutoSaveLogfile(" RunTest");
                            AFT_MotorPerformance_in_process = false;

                            Backlash_TestRunning = false;


                            trBacklash.SmallLoading.Visibility = Visibility.Visible;
                            Storyboard sttrBacklash = (Storyboard)trBacklash.FindResource("SmallLoading");
                            sttrBacklash.Begin();

                            Storyboard myStoryboard = (Storyboard)(this.FindName("sbbacklashstart"));
                            myStoryboard.Begin();

                            MessageBox.Show("Measure Backlash of each joint and then press Backlash button.", "Backlash", MessageBoxButton.OK, MessageBoxImage.Information);


                            CurrentRunningTestTimer.IsEnabled = true;
                            btnSaveBacklash.IsEnabled = true;
                            ReadEncoders1.IsEnabled = true;
                            ReadEncoders2.IsEnabled = true;
                            return;
                        }
                    } //end backlash

                }
            }
        }


        void Write_final_status()
        {
            AFTTestStatus status;
            MainWindow MainWindow = new MainWindow();
            App tempAFT = App.Current as App;
            if (tempAFT != null)
            {
                if (aftcontWalktreportstatus == AFTTestStatus.PASS && aftcontSitStandreportstatus == AFTTestStatus.PASS && aftcontASCreportstatus == AFTTestStatus.PASS && aftcontDSCreportstatus == AFTTestStatus.PASS && Backlash_Test_Result == AFTTestStatus.PASS && Current_Final_Test_Result == AFTTestStatus.PASS)
                {
                    status = AFTTestStatus.PASS;
                }
                else
                {
                    status = AFTTestStatus.FAIL;
                }

                MainWindow.AFTReport((tempAFT).DeptName_TesterFirstName, status.ToString(), "End_All", "Test", (CurrentStatus.ToString() + " I: " + motors_current), false, (Backlash_Test_Result.ToString() + " -> LH: " + lh_b.Text + " LK: " + lk_b.Text + " RH: " + rh_b.Text + " RK: " + rk_b.Text + " "));
            }
        }



        byte[] motors_unrelease = { 0x30, 3, 0x24, 0, 0, 0, 0, 0, 0, 0 };
        byte[] motors_release = { 0x30, 3, 0x23, 0, 0, 0, 0, 0, 0, 0 };
       



       


        void Motors_release_running()
        {

            motors_release[0] = 0x30;
            AFTDeviceData.RunCustomCommand(1, motors_release);
            motors_release[0] = 0x31;
            AFTDeviceData.RunCustomCommand(2, motors_release);
            motors_release[0] = 0x32;
            AFTDeviceData.RunCustomCommand(3, motors_release);
            motors_release[0] = 0x33;
            AFTDeviceData.RunCustomCommand(4, motors_release);
        }
        void Motors_release()
        {

            motors_release[0] = 0x30;
            AFTDeviceData.RunCustomCommand(1, motors_release);
            motors_release[0] = 0x31;
            AFTDeviceData.RunCustomCommand(2, motors_release);
            motors_release[0] = 0x32;
            AFTDeviceData.RunCustomCommand(3, motors_release);
            motors_release[0] = 0x33;
            AFTDeviceData.RunCustomCommand(4, motors_release);
            MessageBox.Show("Test was failed!");

         
            ReadEncoders1.IsEnabled = false;
            ReadEncoders2.IsEnabled = false;
            btnSaveBacklash.IsEnabled = false;

            AFT_Backlash_in_process = false;
            AFT_WALK_in_process = false;
            AFT_SITSTAND_in_process = false;
            AFT_ASC_in_process = false;
            AFT_DSC_in_process = false;
            AFT_All_in_process = false;
            AFT_MotorPerformance_in_process = false;
            AllRadBut.IsEnabled = false;
            ContWalkRadBut.IsEnabled = false;
            ContDSCRadBut.IsEnabled = false;
            ContASCRadBut.IsEnabled = false;
            BacklashRadBut.IsEnabled = false;
            ContSitStandRadBut.IsEnabled = false;
            MotorPerformanceRadBut.IsEnabled = false;
            Calibratetool.IsEnabled = false;
            Starttool.IsEnabled = false;
            SystemStopsTestRadBut.IsEnabled = false;



        }

        private DispatcherTimer CurrentRunningTestTimer;


        private bool AFT_All_in_process = false;
        private bool AFT_WALK_in_process = false;
        private bool AFT_SITSTAND_in_process = false;
        private bool AFT_ASC_in_process = false;
        private bool AFT_DSC_in_process = false;
        private bool AFT_StopsTest_in_process = false;
        private bool AFT_StopsTest_in_processLG = false;
        bool AFT_MotorPerformance_in_process = false;
        bool AFT_Backlash_in_process = false;
        private double AFTToolConCurrentthr;
        private double AFTWalkToolContCurrentThr;
   
        private bool AFTCALBIRATE = false;
        private AFTTestStatus aftcontWalktreportstatus;
        private AFTTestStatus aftcontSitStandreportstatus;
        private AFTTestStatus aftcontASCreportstatus;
        private AFTTestStatus aftcontDSCreportstatus;
        private AFTTestStatus aftbacklashreportstatus;
        private AFTTestStatus aftmotorperfreportstatus;
        private Int16 WalkCalibrationCounts;
        private Int16 ASCCalibrationCounts;
        private Int16 DSCCalibrationCounts;
        private Int16 SitStandCalibrationCounts;
        private Int16 WalkRun1Counts;
        private Int16 ASCRun1Counts;
        private Int16 DSCRun1Counts;
        private Int16 SitStandRun1Counts;
        private Int16 StopsTestRun1Counts;

        private bool enablerecordcurrent = false;
        double ActualAverageCurrent;
        double ActualSavedCurrent;

        bool StartWalkTestRunning = false;
        bool StartDSCTestRunning = false;
        bool StartASCTestRunning = false;
        bool StartSitStandTestRunning = false;
        bool Backlash_TestRunning = false;
     //   bool StopsTest_TestRunning = false;

        private void AllRadBut_Checked(object sender, RoutedEventArgs e)
        {
            AFT_All_in_process = true;
            AFT_WALK_in_process = false;
            AFT_SITSTAND_in_process = false;
            AFT_ASC_in_process = false;
            AFT_DSC_in_process = false;
            AFT_Backlash_in_process = false;
            AFT_MotorPerformance_in_process = false;
            Calibratetool.IsEnabled = true;
            try
            {
                XmlDocument deviceSettings = new XmlDocument();
                deviceSettings.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");
                string WalkCurrent = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTContWalkCurrent").Attributes["Values"].Value;
                AFTWalkToolContCurrentThr = System.Convert.ToDouble(WalkCurrent);
                AFTToolConCurrentthr = AFTWalkToolContCurrentThr;
                App tempAFT = App.Current as App;
                if (tempAFT != null)
                {
                    (tempAFT).DeptName_RunINFtoApplication = true;
                }
            }
            catch (Exception exception)
            {
            //    Logger.Error_MessageBox_AFT("Loading System Configuration Parameters Failed:", "Loading System Configuration", exception.Message);

            }

        }

        private void ContDSCRadBut_Checked(object sender, RoutedEventArgs e)
        {
            App tempAFT = App.Current as App;
            if (tempAFT != null)
            {
                (tempAFT).DeptName_RunINFtoApplication = true;


                MainWindow MainWindow1 = new MainWindow();

                MainWindow1.AFTReport(((tempAFT).DeptName_TesterFirstName), "", "Start", "Continuous DSC", ActualAverageCurrent.ToString(), false, "");
                //send custom command 
                byte[] data = { 0x5b, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
                Int16 run1Ccounts = DSCRun1Counts; //DSC
                data[4] = (byte)(run1Ccounts & 0xFF);
                data[3] = (byte)(run1Ccounts >> 8);
                data[5] = 1;
                data[9] = 100; data[8] = 100;
                byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
                MainWindow1.RunSystem();
                StartDSCTestRunning = true;
                AFT_DSC_in_process = true;
                AFT_WALK_in_process = false;
                StartWalkTestRunning = false;
                AFT_ASC_in_process = false;
                AFT_All_in_process = false;
                AFT_MotorPerformance_in_process = false;
                AFT_Backlash_in_process = false;
                CurrentRunningTestTimer.IsEnabled = true;
            }
        }

        private void ContWalkRadBut_Checked(object sender, RoutedEventArgs e)
        {

            Calibratetool.IsEnabled = true;
            AFT_WALK_in_process = true;
            AFT_SITSTAND_in_process = false;
            AFT_ASC_in_process = false;
            AFT_DSC_in_process = false;
            AFT_All_in_process = false;
            AFT_MotorPerformance_in_process = false;
            AFT_Backlash_in_process = false;

            try
            {
                XmlDocument deviceSettings = new XmlDocument();
                deviceSettings.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");
                string WalkCurrent = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTContWalkCurrent").Attributes["Values"].Value;
                AFTWalkToolContCurrentThr = System.Convert.ToDouble(WalkCurrent);
                AFTToolConCurrentthr = AFTWalkToolContCurrentThr;
                 App tempAFT = App.Current as App;
                 if (tempAFT != null)
                 {
                     (tempAFT).DeptName_RunINFtoApplication = true;
                 }

            }
            catch (Exception exception)
            {
        //        Logger.Error_MessageBox_AFT("Loading System Configuration Parameters Failed:", "Loading System Configuration", exception.Message);

            }
        }


        //private static SolidColorBrush unknownBrush = new SolidColorBrush(Color.FromRgb(0xBB, 0xBB, 0xBB));
        //private static SolidColorBrush failBrush = new SolidColorBrush(Color.FromRgb(0xDD, 0x11, 0x11));
        //private static SolidColorBrush inProgressBrush = new SolidColorBrush(Color.FromRgb(0xDD, 0xDD, 0x11));
        //private static SolidColorBrush inProgressBrushBlink = new SolidColorBrush(Color.FromRgb(0xFF, 0xFF, 0x00));
        //private static SolidColorBrush successBrush = new SolidColorBrush(Color.FromRgb(0x11, 0xBB, 0x11));

        private void Calibratetool_Click(object sender, RoutedEventArgs e) // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions"
        {
             App tempAFT = App.Current as App;
             if (tempAFT != null)
             {
                 if (AFT_All_in_process)
                 {
                     (tempAFT).DeptName_RunINFtoApplication = true;
                     AFTCALBIRATE = true;


                     byte[] data = { 0x4f, 0, 1, 0, 5, 1, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier


                     if (AFT_All_in_process == true)
                     {
                         data[0] = 0x4f;
                         Int16 calcounts = WalkCalibrationCounts;
                         data[4] = (byte)(calcounts & 0xFF);
                         data[3] = (byte)(calcounts >> 8);

                     }
                     else if (AFT_WALK_in_process == true)
                     {
                         data[0] = 0x4f;
                         Int16 calcounts = WalkCalibrationCounts;
                         data[4] = (byte)(calcounts & 0xFF);
                         data[3] = (byte)(calcounts >> 8);
                         // timer.IsEnabled = true;
                     }
                     else if (AFT_SITSTAND_in_process == true)
                     {
                         data[0] = 0x4e;
                         Int16 calcounts = SitStandCalibrationCounts;
                         data[4] = (byte)(calcounts & 0xFF);
                         data[3] = (byte)(calcounts >> 8);
                     }
                     else if (AFT_ASC_in_process == true)
                     {
                         Int16 calcounts = ASCCalibrationCounts;
                         data[4] = (byte)(calcounts & 0xFF);
                         data[3] = (byte)(calcounts >> 8);
                         data[0] = 0x5a;
                     }
                     else if (AFT_DSC_in_process == true)
                     {
                         Int16 calcounts = DSCCalibrationCounts;
                         data[4] = (byte)(calcounts & 0xFF);
                         data[3] = (byte)(calcounts >> 8);
                         data[0] = 0x5b;
                     }
                     else if (AFT_StopsTest_in_process == true)
                     {
                         Int16 calcounts = StopsTestRun1Counts;
                         data[4] = (byte)(calcounts & 0xFF);//note 
                         data[3] = (byte)(calcounts >> 8);
                         data[6] = 0x01;
                         data[0] = 0x4f;

                     }
                     data[5] = 1;
                     data[8] = 100;//data module count
                     data[9] = 100;//data module count
                     byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                     byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);

                     MainWindow MainWindow = new MainWindow();
                     MainWindow.RunSystem();
                     CurrentRunningTestTimer.IsEnabled = true;
                 }
                 else
                 {
                     MessageBox.Show("Please select test", "Instruction", MessageBoxButton.OK, MessageBoxImage.Warning);
                 }
             }

        }
        byte[] WDG_Enable = { 0x6a, 0, 0, 0, 0, 0, 0, 0, 0 };
        private void RecordCurrentsEnable()
        {
            byte[] data = { 0x30, 0x03, 0x30, 1, 0, 10, 0, 0, 0, 0 }; //Read average current
            byte[] result;

            data[0] = 0x30;
            result = AFTDeviceData.RunCustomCommand(1, data);

            data[0] = 0x31;
            result = AFTDeviceData.RunCustomCommand(2, data);

            data[0] = 0x32;
            result = AFTDeviceData.RunCustomCommand(3, data);

            data[0] = 0x33;
            result = AFTDeviceData.RunCustomCommand(4, data);

        }
        private void RecordCurrentsDisable()
        {
            byte[] data = { 0x30, 0x03, 0x30, 2, 0, 4, 0, 0, 0, 0 }; //Read average current
            byte[] result;

            data[0] = 0x30;
            result = AFTDeviceData.RunCustomCommand(1, data);

            data[0] = 0x31;
            result = AFTDeviceData.RunCustomCommand(2, data);

            data[0] = 0x32;
            result = AFTDeviceData.RunCustomCommand(3, data);

            data[0] = 0x33;
            result = AFTDeviceData.RunCustomCommand(4, data);


        }
        private string GetTestName()
        {
            string testname = "";
            if (AFT_All_in_process == true)
            {
                testname = "All";
            }
            if (AFT_WALK_in_process == true)
            {
                testname = "Continuous Walk";
            }
            else if (AFT_SITSTAND_in_process == true)
            {
                testname = "Continuous Sit-Stand";
            }
            else if (AFT_ASC_in_process == true)
            {
                testname = "Continuous ASC";
            }
            else if (AFT_DSC_in_process == true)
            {
                testname = "Continuous DSC";
            }
            else if (AFT_StopsTest_in_process == true)
            {
                testname = "Stops Test";
            }
            return testname;
        }
        AFTTestStatus CurrentStatus = AFTTestStatus.Unknown;
        private void SetTestStatus(AFTTestStatus status, bool fill) // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions"
        {

            if (AFT_WALK_in_process == true)
            {
                aftcontWalktreportstatus = status;
                CurrentStatus = status;
             
                if (fill)
                    FillContWalkEllipse(aftcontWalktreportstatus);
                WalkCheckBox.IsEnabled = true;
                WalkCheckBox.IsChecked = true;
                WalkCheckBox.IsEnabled = false;


            }
            else if (AFT_SITSTAND_in_process == true)
            {
                aftcontSitStandreportstatus = status;

                CurrentStatus = status;
              
                if (fill) FillContSitStandEllipse(aftcontSitStandreportstatus);
                SitStandCheckBox.IsEnabled = true;
                SitStandCheckBox.IsChecked = true;
                SitStandCheckBox.IsEnabled = false;
            }
            else if (AFT_ASC_in_process == true)
            {
                aftcontASCreportstatus = status;

                CurrentStatus = status;
               
                if (fill) FillContASCEllipse(aftcontASCreportstatus);
                ASCCheckBox.IsEnabled = true;
                ASCCheckBox.IsChecked = true;
                ASCCheckBox.IsEnabled = false;

            }
            else if (AFT_DSC_in_process == true)
            {
                aftcontDSCreportstatus = status;

                CurrentStatus = status;
               
                if (fill) FillContDSCEllipse(aftcontDSCreportstatus);
                DSCCheckBox.IsEnabled = true;
                DSCCheckBox.IsChecked = true;
                DSCCheckBox.IsEnabled = false;
            }
            else if (AFT_Backlash_in_process == true)
            {
                aftbacklashreportstatus = status;

                CurrentStatus = status;
              
                if (fill) FillContBacklashEllipse(aftbacklashreportstatus);
                BacklashCheckBox.IsEnabled = true;
                BacklashCheckBox.IsChecked = true;
                BacklashCheckBox.IsEnabled = false;
            }
            else if (AFT_MotorPerformance_in_process == true)
            {
                aftmotorperfreportstatus = status;

                CurrentStatus = status;
               
                if (fill) FillContMotorPerfotmanceEllipse(aftmotorperfreportstatus);
                MotorPerformanceCheckBox.IsEnabled = true;
                MotorPerformanceCheckBox.IsChecked = true;
                MotorPerformanceCheckBox.IsEnabled = false;
            }


        }
        string motors_current = "";
        private float ReadAverageToolCalibMCUCurrent()
        {
            byte[] data = { 0x30, 0x03, 0x81, 0, 0, 0, 0, 0, 0, 0 }; //Read average current
            byte[] result={0};
            Int32 LH_AvgCurrent=0, LK_AvgCurrent=0, RH_AvgCurrent=0, RK_AvgCurrent=0;
            float fLH_AvgCurrent=0, fLK_AvgCurrent=0, fRH_AvgCurrent=0, fRK_AvgCurrent=0;
            float AverageCurrent=0;
            bool retry = true;
            short i;


            data[0] = 0x30;
            for (i = 1; i < 4 && retry == true; i++)
            {
            result = AFTDeviceData.RunCustomCommand(1, data);
                if (result[1] == 0x03 && result[0] == 0x30)
                    if (result[2] == 0x81)
                    {
            LH_AvgCurrent = result[4];
            LH_AvgCurrent = (LH_AvgCurrent << 8) + result[5];
                        if (LH_AvgCurrent != 0)
                            retry = false;
                    }
                    else
                    {
                        LH_AvgCurrent = 0;
                    }
                else
                    i--;
            }
            retry = true;

            data[0] = 0x31;
            for (i = 1; i < 4 && retry == true; i++)
            {
            result = AFTDeviceData.RunCustomCommand(2, data);
                if (result[1] == 0x03 && result[0] == 0x31)
                    if (result[2] == 0x81)
                    {
            LK_AvgCurrent = result[4];
            LK_AvgCurrent = (LK_AvgCurrent << 8) + result[5];
                        if (LK_AvgCurrent != 0)
                            retry = false;
                    }
                    else
                    {
                        LK_AvgCurrent = 0;
                    }
                else
                    i--;
            }
            retry = true;

            data[0] = 0x32;
            for (i = 1; i < 4 && retry == true; i++)
            {
            result = AFTDeviceData.RunCustomCommand(3, data);
                if (result[1] == 0x03 && result[0] == 0x32)
                    if (result[2] == 0x81)
                    {
            RH_AvgCurrent = result[4];
            RH_AvgCurrent = (RH_AvgCurrent << 8) + result[5];
                        if (RH_AvgCurrent != 0)
                            retry = false;
                    }
                    else
                    {
                        RH_AvgCurrent = 0;
                    }
                else
                    i--;
            }
            retry = true;

            data[0] = 0x33;
            for (i = 1; i < 4 && retry == true; i++)
            {
            result = AFTDeviceData.RunCustomCommand(4, data);
                if (result[1] == 0x03 && result[0] == 0x33)
                    if (result[2] == 0x81)
                    {
            RK_AvgCurrent = result[4];
            RK_AvgCurrent = (RK_AvgCurrent << 8) + result[5];
                        if (RK_AvgCurrent != 0)
                            retry = false;
                    }
                    else
                    {
                        RK_AvgCurrent = 0;
                    }
                else
                    i--;
            }
            retry = true;

            if (LH_AvgCurrent != 0 && LK_AvgCurrent != 0 && RH_AvgCurrent != 0 && RK_AvgCurrent != 0)
            {

            AverageCurrent = LH_AvgCurrent + LK_AvgCurrent + RH_AvgCurrent + RK_AvgCurrent;
            AverageCurrent = (AverageCurrent / 416);
            }
            fLH_AvgCurrent = LH_AvgCurrent;
            fLH_AvgCurrent = (fLH_AvgCurrent / 104);

            fRH_AvgCurrent = RH_AvgCurrent;
            fRH_AvgCurrent = (fRH_AvgCurrent / 104);

            fLK_AvgCurrent = LK_AvgCurrent;
            fLK_AvgCurrent = (fLK_AvgCurrent / 104);

            fRK_AvgCurrent = RK_AvgCurrent;
            fRK_AvgCurrent = (fRK_AvgCurrent / 104);





            motors_current = (AverageCurrent.ToString() + " -> " + " LH: " + fLH_AvgCurrent.ToString() + " LK: " + fLK_AvgCurrent.ToString() + " RH: " + fRH_AvgCurrent.ToString() + " RK: " + fRK_AvgCurrent.ToString());
            return (AverageCurrent); //4*104=416 , average of four joints and 104 is current mcu divider
        }


        private string ResetINFTest() //it was with elsif loop .
        {
            string testname = "";
            if (AFT_WALK_in_process == true)
            {
                testname = "Continuous Walk";
                byte[] data = { 0x4f, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
            }
            if (AFT_SITSTAND_in_process == true)
            {
                testname = "Continuous Sit-Stand";
                byte[] data = { 0x4e, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
            }
            if (AFT_ASC_in_process == true)
            {
                testname = "Continuous ASC";
                byte[] data = { 0x5a, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
            }
            if (AFT_DSC_in_process == true)
            {
                testname = "Continuous DSC";
                byte[] data = { 0x5b, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
            }
            if (AFT_StopsTest_in_process == true)
            {
                testname = "Sopts Tests";
                byte[] data = { 0x4f, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
            }
            return testname;
        }

        Int32 Backlash_lh, Backlash_lk, Backlash_rh, Backlash_rk;
        AFTTestStatus Backlash_Test_Result, Current_Final_Test_Result;
        private void Backlash_Click(object sender, RoutedEventArgs e) // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions"
        {

             App tempAFT = App.Current as App;
             if (tempAFT != null)
             {

                 Storyboard myStoryboard = (Storyboard)(this.FindName("sbbacklashstart"));
                 myStoryboard.Stop();
                 AFTTestStatus status;
                 string testname = "";

                 testname = GetTestName();


                 if (!string.IsNullOrEmpty(lh_b.Text) && !string.IsNullOrEmpty(lk_b.Text) && !string.IsNullOrEmpty(rh_b.Text) && !string.IsNullOrEmpty(rk_b.Text))
                 {
                     try
                     {
                         //  Backlash_lh = (Convert.ToInt16(lh_b.Text));
                         //  Backlash_lk = (Convert.ToInt16(lk_b.Text));
                         //  Backlash_rh = (Convert.ToInt16(rh_b.Text));
                         //  Backlash_rk = (Convert.ToInt16(rk_b.Text));


                         if ((Backlash_lh > 45) || (Backlash_lk > 45) || (Backlash_rh > 45) || (Backlash_rk > 45))
                         {
                             status = AFTTestStatus.FAIL;

                         }
                         else
                         {
                             status = AFTTestStatus.PASS;

                         }



                         Backlash_Test_Result = status;
                         btnSaveBacklash.IsEnabled = false;
                         lh_b.IsEnabled = false;
                         lk_b.IsEnabled = false;
                         rh_b.IsEnabled = false;
                         rk_b.IsEnabled = false;
                         //      lh.Text = "";
                         //     lk.Text = "";
                         ///     rh.Text = "";
                         //    rk.Text = "";

                         MainWindow MainWindow = new MainWindow();
                         MainWindow.AFTReport(((tempAFT).DeptName_TesterFirstName), "", "Start", "Backlash", ActualAverageCurrent.ToString(), false, "");

                         if (AFT_All_in_process)
                         {
                             AFT_Backlash_in_process = true;
                             SetTestStatus(status, true);
                             // Backlash_TestRunning = true;
                             AFT_Backlash_in_process = false;
                             //AFT_MotorPerformance_in_process = true;
                             AFT_MotorPerformance_in_process = false;
                             MainWindow.AFTReport((tempAFT).DeptName_TesterFirstName, status.ToString(), "End", "Backlash", (CurrentStatus.ToString() + "I: " + ActualAverageCurrent.ToString()), true, (Backlash_Test_Result.ToString() + "-> LH: " + lh_b.Text + " C " + lh_b_d.Text + " deg," + " LK: " + lk_b.Text + " C " + lk_b_d.Text + " deg,"
                      + " RH: " + rh_b.Text + " C " + rh_b_d.Text + " deg," + " RK: " + rk_b.Text + " C " + rk_b_d.Text + " deg"));
                             // FinalCurrenttestinWalk();

                             //begin Stop test
                             //   Main MainWindow1 = new Main();
                             MainWindow MainWindow2 = new MainWindow();
                             MessageBoxResult MSGresults = MessageBox.Show("Please tie the strap to right leg", "Instruction", MessageBoxButton.YesNo, MessageBoxImage.Question);
                             if (MSGresults == MessageBoxResult.Yes)
                             {
                                 // SystemStopsTestPassEllipse.Fill = inProgressBrush;
                                 MainWindow2.AFTReport(((tempAFT).DeptName_TesterFirstName), "", "Start", "Stops test", ActualAverageCurrent.ToString(), false, "");
                                 //send custom command 
                                 byte[] data = { 0x4f, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
                                 Int16 run1Ccounts = StopsTestRun1Counts; //DSC
                                 data[4] = (byte)(run1Ccounts & 0xFF);
                                 data[3] = (byte)(run1Ccounts >> 8);
                                 data[5] = 1;
                                 data[6] = 1;
                                 data[9] = 100; data[8] = 100;
                                 byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                                 byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
                                 MainWindow2.RunSystem();
                                 //StopsTest_TestRunning = true;
                                 AFT_StopsTest_in_process = true;
                                 AFT_WALK_in_process = false;
                                 StartWalkTestRunning = false;
                                 // CurrentRunningTestTimer.IsEnabled = false;
                                 CurrentRunningTestTimer.IsEnabled = true;
                             }
                             else if (MSGresults == MessageBoxResult.No)
                             {
                                 AFT_StopsTest_in_process = false;
                                 CurrentRunningTestTimer.IsEnabled = false;
                             }
                             /////////////////////end stop begin



                         }
                         else if (AFT_Backlash_in_process == true)
                         {
                             SetTestStatus(status, true);
                             AFT_Backlash_in_process = false;

                             MainWindow.AFTReport((tempAFT).DeptName_TesterFirstName, status.ToString(), "End", "Backlash", (CurrentStatus.ToString() + "I: " + ActualAverageCurrent.ToString()), true, (Backlash_Test_Result.ToString() + "-> LH: " + lh_b.Text + " C " + lh_b_d.Text + " deg," + " LK: " + lk_b.Text + " C " + lk_b_d.Text + " deg,"
                                 + " RH: " + rh_b.Text + " C " + rh_b_d.Text + " deg," + " RK: " + rk_b.Text + " C " + rk_b_d.Text + " deg"));

                         }
                     }

                     catch (Exception exception)
                     {
                         string exc = exception.Message;
                         Logger.Error_MessageBox_AFT("Enter numbers only!", "", exception.Message);

                         //  MessageBox.Show(" " + "Enter numbers only!");
                     }
                 }
                 else
                 {
                     MessageBox.Show("Backlash value should be inserted for all joints!", "Backlash", MessageBoxButton.OK, MessageBoxImage.Information);
                 }
             }

        }


        void FinalCurrenttestinWalk()
        {
            byte[] data = { 0x4f, 0, 1, 0, 5, 1, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier



            data[0] = 0x4f;
            Int16 calcounts = WalkCalibrationCounts;
            data[4] = (byte)(calcounts & 0xFF);
            data[3] = (byte)(calcounts >> 8);
            data[5] = 1;
            data[8] = 100;//data module count
            data[9] = 100;//data module count
            byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
            byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
            Backlash_TestRunning = true;
            MainWindow MainWindow = new MainWindow();
            MainWindow.RunSystem();


        }
        public void FillContWalkEllipse(AFTTestStatus aftstatus)
        {
            Storyboard stContinousWalk = (Storyboard)trContinuousWalk.FindResource("SmallLoading");
            stContinousWalk.Stop();
            trContinuousWalk.SmallLoading.Visibility = Visibility.Collapsed; 

                    switch (aftstatus)
                    {

                        case AFTTestStatus.PASS:
                           // ContWalkPassEllipse.Fill = successBrush;
                            trContinuousWalk.OK.Visibility = Visibility.Visible;
                           break;
                        //case AFTTestStatus.NOTCOMPLETED:
                           // ContWalkPassEllipse.Fill = inProgressBrush;
                          //  break;
                        case AFTTestStatus.FAIL:
                           trContinuousWalk.Fail.Visibility = Visibility.Visible;
                            //ContWalkPassEllipse.Fill = failBrush;
                            break;
                        default:
                        //    ContWalkPassEllipse.Fill = unknownBrush;
                            break;
                    }
             
        }
        private void FillContSitStandEllipse(AFTTestStatus aftstatus)
        {
            trContinuousSitStand.SmallLoading.Visibility = Visibility.Collapsed;
            Storyboard stContinuousSitStand = (Storyboard)trContinuousSitStand.FindResource("SmallLoading");
            stContinuousSitStand.Stop();
              switch (aftstatus)
               {
                   case AFTTestStatus.PASS:
                       //ContSitStandPassEllipse.Fill = successBrush;
                       trContinuousSitStand.OK.Visibility = Visibility.Visible;
                       break;
                   //case AFTTestStatus.NOTCOMPLETED:
                    //   ContSitStandPassEllipse.Fill = inProgressBrush;
                    //   break;
                   case AFTTestStatus.FAIL:
                       trContinuousSitStand.Fail.Visibility = Visibility.Visible;     
                  //ContSitStandPassEllipse.Fill = failBrush;
                       break;
                   default:
                       
                       break;
               }
           
            
        }
        private void FillContASCEllipse(AFTTestStatus aftstatus)
        {

            trContinuousStairASC.SmallLoading.Visibility = Visibility.Collapsed;
            Storyboard stContinuousStairASC = (Storyboard)trContinuousStairASC.FindResource("SmallLoading");
            stContinuousStairASC.Stop();
             switch (aftstatus)
             {
                 case AFTTestStatus.PASS:
                     trContinuousStairASC.OK.Visibility = Visibility.Visible;
                    // ContASCPassEllipse.Fill = successBrush;
                     break;
                 //case AFTTestStatus.NOTCOMPLETED:
                 //    ContASCPassEllipse.Fill = inProgressBrush;
                 //    break;
                 case AFTTestStatus.FAIL:

                     trContinuousStairASC.Fail.Visibility = Visibility.Visible;
                 // ContASCPassEllipse.Fill = failBrush;
                     break;
                 default:
                 //    ContASCPassEllipse.Fill = unknownBrush;
                    break;
             }
        }
        private void FillContDSCEllipse(AFTTestStatus aftstatus)
        {

            trContinuousStairDSC.SmallLoading.Visibility = Visibility.Collapsed;
            Storyboard stContinuousStairDSC = (Storyboard)trContinuousStairDSC.FindResource("SmallLoading");
            stContinuousStairDSC.Stop();
             switch (aftstatus)
             {
                 case AFTTestStatus.PASS:
                     trContinuousStairDSC.OK.Visibility = Visibility.Visible;
                     //ContDSCPassEllipse.Fill = successBrush;
                     break;
                 //case AFTTestStatus.NOTCOMPLETED:
                 //    ContDSCPassEllipse.Fill = inProgressBrush;
                   //  break;
                 case AFTTestStatus.FAIL:
                     trContinuousStairDSC.Fail.Visibility = Visibility.Visible;
                     break;
                 default:
                 //    ContDSCPassEllipse.Fill = unknownBrush;
                    break;
             }
        }
        private void FillContBacklashEllipse(AFTTestStatus aftstatus)
        {

            trBacklash.SmallLoading.Visibility = Visibility.Collapsed;
            Storyboard sttrBacklash = (Storyboard)trBacklash.FindResource("SmallLoading");
            sttrBacklash.Stop();
            switch (aftstatus)
             {
                 case AFTTestStatus.PASS:
                    // BacklashPassEllipse.Fill = successBrush;
                     trBacklash.OK.Visibility = Visibility.Visible; 
                    break;
                 //case AFTTestStatus.NOTCOMPLETED:
                 //    BacklashPassEllipse.Fill = inProgressBrush;
                 //    break;
                 case AFTTestStatus.FAIL:
                     //BacklashPassEllipse.Fill = failBrush;
                    trBacklash.Fail.Visibility = Visibility.Visible; 
                     break;
                 default:
                 //    BacklashPassEllipse.Fill = unknownBrush;
                 break;
             }
        }
        private void FillContMotorPerfotmanceEllipse(AFTTestStatus aftstatus)
        {
            trMotorPerformance.SmallLoading.Visibility = Visibility.Collapsed;
            Storyboard stMotorPerformance = (Storyboard)trMotorPerformance.FindResource("SmallLoading");
            stMotorPerformance.Stop();

             switch (aftstatus)
              {
                  case AFTTestStatus.PASS:
                      trMotorPerformance.OK.Visibility = Visibility.Visible;
                   //   MotorPerformancePassEllipse.Fill = successBrush;
                      break;
                  //case AFTTestStatus.NOTCOMPLETED:
                  //    MotorPerformancePassEllipse.Fill = inProgressBrush;
                  //    break;
                  case AFTTestStatus.FAIL:
                      trMotorPerformance.Fail.Visibility = Visibility.Visible;
                      //MotorPerformancePassEllipse.Fill = failBrush;
                      break;
                  default:
                  //    MotorPerformancePassEllipse.Fill = unknownBrush;
                   break;
              }
        }

        private void Starttool_Click(object sender, RoutedEventArgs e)
        {
            Starttool.IsChecked = true;
            if (AFT_All_in_process == true || AFT_WALK_in_process == true)
            {
                 App tempAFT = App.Current as App;
                 if (tempAFT != null)
                 {
                     (tempAFT).DeptName_RunINFtoApplication = true;
                     MainWindow MainWindow = new MainWindow();

                     MainWindow.AFTReport(((tempAFT).DeptName_TesterFirstName), "", "Start", "Continuous Walk", ActualAverageCurrent.ToString(), false, "");
                     //send custom command 
                     byte[] data = { 0x4f, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier

                     Int16 run1Ccounts = WalkRun1Counts; //Walk
                     data[4] = (byte)(run1Ccounts & 0xFF);
                     data[3] = (byte)(run1Ccounts >> 8);
                     data[5] = 1;
                     data[8] = 100;
                     data[9] = 100;
                     byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                     byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);

                     MainWindow.RunSystem();
                     StartWalkTestRunning = true;
                     AFT_WALK_in_process = true;

                     trContinuousWalk.SmallLoading.Visibility = Visibility.Visible;
                     Storyboard stContinousWalk = (Storyboard)trContinuousWalk.FindResource("SmallLoading");
                     stContinousWalk.Begin();
                 }


            }

        }

        short HipMaxAngle;
        short HipMinAngle;
        short KneeMaxAngle;
        short KneeMinAngle;



        private void Delay_ms(double ms)
        {
            double _val = 0;
            Stopwatch stopwatch1 = new Stopwatch();
            stopwatch1.Start();

            while (stopwatch1.ElapsedMilliseconds < ms)
            {
                _val++;
            }
            stopwatch1.Reset();
            _val = 0;

        }
        bool WaitUntillHipsReachDestination(byte joint, int j, short des)
        {

            byte[] data = { joint, 0x03, 0x01, 0, 0, 0, 0, 0, 0, 0 }; //Read average current
            byte[] result;
            short encoder;
            data[0] = joint;
            result = AFTDeviceData.RunCustomCommand(j, data);
            encoder = result[5];
            encoder = (short)((encoder << 8) + result[6]);
            if ((des <= (encoder + 46)) && (des > (encoder - 46)))//2 degrees window
                return true;



            return false;
        }
        bool WaitUntillKneesReachDestination(byte joint, int j, short des)
        {

            byte[] data = { joint, 0x03, 0x01, 0, 0, 0, 0, 0, 0, 0 }; //Read average current
            byte[] result;
            short encoder;
            data[0] = joint;
            result = AFTDeviceData.RunCustomCommand(j, data);
            encoder = result[5];
            encoder = (short)((encoder << 8) + result[6]);

            des = Math.Abs(des);
            encoder = Math.Abs(encoder);

            if ((des <= (encoder + 46)) && (des > (encoder - 46)))//2 degrees window
                return true;



            return false;
        }
        string[] Motors_perf = new string[4];
        private bool CheckMotorPerformance()
        {
            Motors_perf[0] = "FAIL"; Motors_perf[1] = "FAIL"; Motors_perf[2] = "FAIL"; Motors_perf[3] = "FAIL";
            Motors_release_running();
            if (Run_hipmotors_test_performance(0x330))
            {
                Motors_perf[0] = "PASS";
                if (Run_kneemotors_test_performance(0x331))
                {
                    Motors_perf[1] = "PASS";
                    if (Run_hipmotors_test_performance(0x332))
                    {
                        Motors_perf[2] = "PASS";
                        if (Run_kneemotors_test_performance(0x333))
                        {
                            Motors_perf[3] = "PASS";
                            return true;
                        }
                    }
                }
            }

            return false;
        }
        private bool Run_hipmotors_test_performance(short motor) // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions"
        {
            short angle;
            byte Joint = 0;
            int J = 0;
            string m = "";
            if (motor == 0x330)
            {
                Joint = 0x30;
                J = 1;
                m = "LH";
                motors_unrelease[0] = 0x30;
                AFTDeviceData.RunCustomCommand(J, motors_unrelease);
            }
            else if (motor == 0x332)
            {
                Joint = 0x32;
                J = 3;
                m = "RH";
                motors_unrelease[0] = 0x32;
                AFTDeviceData.RunCustomCommand(J, motors_unrelease);
            }

            try
            {
                byte[] result_LH;
                byte[] data_move = { Joint, 3, 0, 0, 0, 0, 0, 3, 0, 0 }; //move Bytes 4&5=angle and 6&7=velocity ='0x0300'   
                byte[] motor_release = { Joint, 3, 0x23, 0, 0, 0, 0, 3, 0, 0 };
                short HIP_MINMUM_ANGLE = (short)(HipMinAngle * 23);
                short HIP_MAXIMUM_ANGLE = (short)(HipMaxAngle * 23);
                angle = HIP_MINMUM_ANGLE;
                bool res = true;


                while (angle <= HIP_MAXIMUM_ANGLE && res)
                {

                    data_move[6] = (byte)(angle);
                    data_move[5] = (byte)(angle >> 8);
                    result_LH = AFTDeviceData.RunCustomCommand(J, data_move);
                    Delay_ms(1000);
                    TimeOut_Cnt = 0;
                    res = false;
                    while (!res && TimeOut_Cnt < 10)
                    {
                        res = WaitUntillHipsReachDestination(Joint, J, angle);
                        Delay_ms(500);
                        TimeOut_Cnt++;
                    }
                    angle += 46 * 2;
                }
                if (!res || TimeOut_Cnt >= 10)
                {
                    MessageBox.Show(m + " has stopped unexpectedly");
                    return false;
                }

                angle -= 46 * 2;
                while (angle >= HIP_MINMUM_ANGLE && res)
                {

                    data_move[6] = (byte)(angle);
                    data_move[5] = (byte)(angle >> 8);
                    result_LH = AFTDeviceData.RunCustomCommand(J, data_move);
                    Delay_ms(500);
                    TimeOut_Cnt = 0;
                    res = false;
                    while (!res && TimeOut_Cnt < 10)
                    {
                        res = WaitUntillHipsReachDestination(Joint, J, angle);
                        Delay_ms(500);
                        TimeOut_Cnt++;

                    }
                    angle -= 46 * 2;
                }

                if (res && TimeOut_Cnt < 10)
                {
                    //    MessageBox.Show(m+" ran all over his spectrum successfully!");

                    //move to 30 degree
                    angle = 0x2c0;
                    data_move[6] = (byte)(angle);
                    data_move[5] = (byte)(angle >> 8);
                    result_LH = AFTDeviceData.RunCustomCommand(J, data_move);
                    Delay_ms(2000);
                    result_LH = AFTDeviceData.RunCustomCommand(J, motor_release);
                    Delay_ms(2000);
                    return true;
                }
                else
                {
                    MessageBox.Show(m + " LH has stopped unexpectedly");
                    return false;
                }
            }
            catch (Exception exception)
            {
                string exc = exception.Message;
             //   MessageBox.Show("Stopped unexpectedly:  " +"");
                Logger.Error_MessageBox_AFT("Stopped unexpectedly:  ", "", exception.Message);
                return false;
            }
        }
        private bool Run_kneemotors_test_performance(short motor) // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions"
        {
            short angle;
            byte Joint = 0;
            int J = 0;
            string m = "";
            if (motor == 0x331)
            {
                Joint = 0x31;
                J = 2;
                m = "LK";
                motors_unrelease[0] = 0x31;
                AFTDeviceData.RunCustomCommand(J, motors_unrelease);
            }
            else if (motor == 0x333)
            {
                Joint = 0x33;
                J = 4;
                m = "RK";
                motors_unrelease[0] = 0x33;
                AFTDeviceData.RunCustomCommand(J, motors_unrelease);
            }

            try
            {
                byte[] result_LH;
                byte[] data_move = { Joint, 3, 0, 0, 0, 0, 0, 3, 0, 0 }; //move Bytes 4&5=angle and 6&7=velocity ='0x0300'   
                byte[] motor_release = { Joint, 3, 0x23, 0, 0, 0, 0, 3, 0, 0 };
                short KNEE_MINMUM_ANGLE = (short)(-KneeMinAngle * 23);
                short KNEE_MAXIMUM_ANGLE = (short)(-KneeMaxAngle * 23);
                angle = KNEE_MINMUM_ANGLE;
                bool res = true;


                while (angle >= KNEE_MAXIMUM_ANGLE && res)
                {

                    data_move[6] = (byte)(angle);
                    data_move[5] = (byte)(angle >> 8);
                    result_LH = AFTDeviceData.RunCustomCommand(J, data_move);
                    Delay_ms(500);
                    TimeOut_Cnt = 0;
                    res = false;
                    while (!res && TimeOut_Cnt < 10)
                    {
                        res = WaitUntillKneesReachDestination(Joint, J, angle);
                        Delay_ms(500);
                        TimeOut_Cnt++;
                    }
                    angle -= 46 * 2;
                }
                if (!res || TimeOut_Cnt >= 10)
                {
                    MessageBox.Show(m + " has stopped unexpectedly");
                    return false;
                }

                angle += 46 * 2;
                while (angle <= KNEE_MINMUM_ANGLE && res)
                {

                    data_move[6] = (byte)(angle);
                    data_move[5] = (byte)(angle >> 8);
                    result_LH = AFTDeviceData.RunCustomCommand(J, data_move);
                    Delay_ms(500);
                    TimeOut_Cnt = 0;
                    res = false;
                    while (!res && TimeOut_Cnt < 10)
                    {
                        res = WaitUntillKneesReachDestination(Joint, J, angle);
                        Delay_ms(500);
                        TimeOut_Cnt++;

                    }
                    angle += 46 * 2;
                }

                if (res && TimeOut_Cnt < 10)
                {
                    // MessageBox.Show(m+" ran all over his spectrum successfully!");
                    //move to 0

                    angle = -136;
                    data_move[6] = (byte)(angle);
                    data_move[5] = (byte)(angle >> 8);
                    result_LH = AFTDeviceData.RunCustomCommand(J, data_move);
                    Delay_ms(2000);
                    result_LH = AFTDeviceData.RunCustomCommand(J, motor_release);
                    return true;
                }
                else
                {
                    MessageBox.Show(m + " LH has stopped unexpectedly");
                    return false;
                }
            }
            catch (Exception exception)
            {
                string exc = exception.Message;
                MessageBox.Show("Stopped unexpectedly:  " + "");
                Logger.Error_MessageBox_AFT("Stopped unexpectedly:  " + "","",exception.Message);
                return false;
            }
        }


        private void MotorPerformanceRadBut_Checked(object sender, RoutedEventArgs e)
        {
            App tempAFT = App.Current as App;
            if (tempAFT != null)
            {
                (tempAFT).DeptName_RunINFtoApplication = true;
            }
            Calibratetool.IsEnabled = true;
            AFT_WALK_in_process = false;
            AFT_SITSTAND_in_process = false;
            AFT_ASC_in_process = false;
            AFT_DSC_in_process = false;
            AFT_MotorPerformance_in_process = true;
            AFT_All_in_process = false;
            AFT_Backlash_in_process = false;
            MainWindow MainWindow2 = new MainWindow();
            MainWindow2.AFTReport(((tempAFT).DeptName_TesterFirstName), "", "Start", "Motors Performance", "", false, "");
            
            if (CheckMotorPerformance())
            {
                SetTestStatus(AFTTestStatus.PASS, true);
                MainWindow2.AFTReport(((tempAFT).DeptName_TesterFirstName), AFTTestStatus.PASS.ToString(), "End", "Motors Performance", (AFTTestStatus.PASS.ToString() + " -> " + " LH: " + Motors_perf[0] + " LK: " + Motors_perf[1] + " RH: " + Motors_perf[2] + " RK: " + Motors_perf[3]), false, "");

            }
            else
            {
                SetTestStatus(AFTTestStatus.FAIL, true);
                MainWindow2.AFTReport(((tempAFT).DeptName_TesterFirstName), AFTTestStatus.FAIL.ToString(), "End", "Motors Performance", (AFTTestStatus.FAIL.ToString() + " -> " + " LH: " + Motors_perf[0] + " LK: " + Motors_perf[1] + " RH: " + Motors_perf[2] + " RK: " + Motors_perf[3]), false, "");

            }

        }
        short LH_encoder, LK_encoder, RH_encoder, RK_encoder;
        private void ReadAEncodersMax() // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions"
        {
            byte[] data = { 0x30, 0x03, 0x01, 0, 0, 0, 0, 0, 0, 0 }; //Read average current
            byte[] result;



            if (LH.IsChecked == true)
            {
                data[0] = 0x30;
                result = AFTDeviceData.RunCustomCommand(1, data);
                result = AFTDeviceData.RunCustomCommand(1, data);
                LH_encoder = result[5];
                LH_encoder = (short)((LH_encoder << 8) + result[6]);
                lh.Text = LH_encoder.ToString();
                if (!string.IsNullOrEmpty(lh.Text) && !string.IsNullOrEmpty(lh2.Text))
                {
                    Backlash_lh = (Int32)(Math.Abs(Math.Abs(System.Convert.ToInt16(lh.Text)) - Math.Abs(System.Convert.ToInt16(lh2.Text))));
                    lh_b.Text = Backlash_lh.ToString();
                    fBacklash_lh = Backlash_lh;
                    fBacklash_lh = (float)(fBacklash_lh / 22.75);
                    lh_b_d.Text = fBacklash_lh.ToString("0.0###");
                }
            }
            if (LK.IsChecked == true)
            {
                data[0] = 0x31;
                result = AFTDeviceData.RunCustomCommand(2, data);
                result = AFTDeviceData.RunCustomCommand(2, data);
                LK_encoder = result[5];
                LK_encoder = (short)((LK_encoder << 8) + result[6]);
                lk.Text = LK_encoder.ToString();
                if (!string.IsNullOrEmpty(lk.Text) && !string.IsNullOrEmpty(lk2.Text))
                {
                    Backlash_lk = (Int16)(Math.Abs(Math.Abs(System.Convert.ToInt16(lk.Text)) - Math.Abs(System.Convert.ToInt16(lk2.Text))));
                    lk_b.Text = Backlash_lk.ToString();
                    fBacklash_lk = Backlash_lk;
                    fBacklash_lk = (float)(fBacklash_lk / 22.755);
                    lk_b_d.Text = fBacklash_lk.ToString("0.0###");
                }
            }
            if (RH.IsChecked == true)
            {
                data[0] = 0x32;
                result = AFTDeviceData.RunCustomCommand(3, data);
                result = AFTDeviceData.RunCustomCommand(3, data);
                RH_encoder = result[5];
                RH_encoder = (short)((RH_encoder << 8) + result[6]);
                rh.Text = RH_encoder.ToString();
                if (!string.IsNullOrEmpty(rh.Text) && !string.IsNullOrEmpty(rh2.Text))
                {
                    Backlash_rh = (Int16)(Math.Abs(Math.Abs(System.Convert.ToInt16(rh.Text)) - Math.Abs(System.Convert.ToInt16(rh2.Text))));
                    rh_b.Text = Backlash_rh.ToString();
                    fBacklash_rh = Backlash_rh;
                    fBacklash_rh = (float)(fBacklash_rh / 22.755);
                    rh_b_d.Text = fBacklash_rh.ToString("0.0###");
                }
            }
            if (RK.IsChecked == true)
            {
                data[0] = 0x33;
                result = AFTDeviceData.RunCustomCommand(4, data);
                result = AFTDeviceData.RunCustomCommand(4, data);
                RK_encoder = result[5];
                RK_encoder = (short)((RK_encoder << 8) + result[6]);
                rk.Text = RK_encoder.ToString();
                if (!string.IsNullOrEmpty(rk.Text) && !string.IsNullOrEmpty(rk2.Text))
                {
                    Backlash_rk = (Int16)(Math.Abs(Math.Abs(System.Convert.ToInt16(rk.Text)) - Math.Abs(System.Convert.ToInt16(rk2.Text))));
                    rk_b.Text = Backlash_rk.ToString();
                    fBacklash_rk = Backlash_rk;
                    fBacklash_rk = (float)(fBacklash_rk / 22.755);
                    rk_b_d.Text = fBacklash_rk.ToString("0.0###");
                }
            }



        }

        float fBacklash_lh, fBacklash_lk, fBacklash_rh, fBacklash_rk;
        private void ReadAEncodersMin() // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions"
        {
            byte[] data = { 0x30, 0x03, 0x01, 0, 0, 0, 0, 0, 0, 0 }; //Read average current
            byte[] result;



            if (LH.IsChecked == true)
            {
                data[0] = 0x30;
                result = AFTDeviceData.RunCustomCommand(1, data);
                result = AFTDeviceData.RunCustomCommand(1, data);
                LH_encoder = result[5];
                LH_encoder = (short)((LH_encoder << 8) + result[6]);
                lh2.Text = LH_encoder.ToString();
                if (!string.IsNullOrEmpty(lh.Text) && !string.IsNullOrEmpty(lh2.Text))
                {
                    Backlash_lh = (Int32)(Math.Abs(Math.Abs(System.Convert.ToInt16(lh.Text)) - Math.Abs(System.Convert.ToInt16(lh2.Text))));
                    lh_b.Text = Backlash_lh.ToString();

                    fBacklash_lh = Backlash_lh;
                    fBacklash_lh = (float)(fBacklash_lh / 22.755);
                    lh_b_d.Text = fBacklash_lh.ToString("0.0###");

                }
            }
            if (LK.IsChecked == true)
            {
                data[0] = 0x31;
                result = AFTDeviceData.RunCustomCommand(2, data);
                result = AFTDeviceData.RunCustomCommand(2, data);
                LK_encoder = result[5];
                LK_encoder = (short)((LK_encoder << 8) + result[6]);
                lk2.Text = LK_encoder.ToString();
                if (!string.IsNullOrEmpty(lk.Text) && !string.IsNullOrEmpty(lk2.Text))
                {
                    Backlash_lk = (Int16)(Math.Abs(Math.Abs(System.Convert.ToInt16(lk.Text)) - Math.Abs(System.Convert.ToInt16(lk2.Text))));
                    lk_b.Text = Backlash_lk.ToString();
                    fBacklash_lk = Backlash_lk;
                    fBacklash_lk = (float)(fBacklash_lk / 22.755);
                    lk_b_d.Text = fBacklash_lk.ToString("0.0###");
                }
            }
            if (RH.IsChecked == true)
            {
                data[0] = 0x32;
                result = AFTDeviceData.RunCustomCommand(3, data);
                result = AFTDeviceData.RunCustomCommand(3, data);
                RH_encoder = result[5];
                RH_encoder = (short)((RH_encoder << 8) + result[6]);
                rh2.Text = RH_encoder.ToString();
                if (!string.IsNullOrEmpty(rh.Text) && !string.IsNullOrEmpty(rh2.Text))
                {
                    Backlash_rh = (Int16)(Math.Abs(Math.Abs(System.Convert.ToInt16(rh.Text)) - Math.Abs(System.Convert.ToInt16(rh2.Text))));
                    rh_b.Text = Backlash_rh.ToString();
                    fBacklash_rh = Backlash_rh;
                    fBacklash_rh = (float)(fBacklash_rh / 22.755);
                    rh_b_d.Text = fBacklash_rh.ToString("0.0###");
                }
            }
            if (RK.IsChecked == true)
            {
                data[0] = 0x33;
                result = AFTDeviceData.RunCustomCommand(4, data);
                result = AFTDeviceData.RunCustomCommand(4, data);
                RK_encoder = result[5];
                RK_encoder = (short)((RK_encoder << 8) + result[6]);
                rk2.Text = RK_encoder.ToString();
                if (!string.IsNullOrEmpty(rk.Text) && !string.IsNullOrEmpty(rk2.Text))
                {
                    Backlash_rk = (Int16)(Math.Abs(Math.Abs(System.Convert.ToInt16(rk.Text)) - Math.Abs(System.Convert.ToInt16(rk2.Text))));
                    rk_b.Text = Backlash_rk.ToString();
                    fBacklash_rk = Backlash_rk;
                    fBacklash_rk = (float)(fBacklash_rk / 22.755);
                    rk_b_d.Text = fBacklash_rk.ToString("0.0###");
                }
            }



        }

        private void BacklashRadBut_Checked(object sender, RoutedEventArgs e)
        {
            Storyboard myStoryboard = (Storyboard)(this.FindName("sbbacklashstart"));
            myStoryboard.Begin();
            ReadEncoders1.IsEnabled = true;
            ReadEncoders2.IsEnabled = true;
            btnSaveBacklash.IsEnabled = true;
            AFT_Backlash_in_process = true;
            AFT_WALK_in_process = false;
            AFT_SITSTAND_in_process = false;
            AFT_ASC_in_process = false;
            AFT_DSC_in_process = false;
            AFT_All_in_process = false;
            AFT_MotorPerformance_in_process = false;
            MainWindow MainWindow2 = new MainWindow();
            App tempAFT = App.Current as App;
              if (tempAFT != null)
              {
                  MainWindow2.AFTReport(((tempAFT).DeptName_TesterFirstName), "", "Start", "Backlash", ActualAverageCurrent.ToString(), false, "");
              }
          
           
        }

        private void ContSitStandRadBut_Checked(object sender, RoutedEventArgs e)
        {
              App tempAFT = App.Current as App;
              if (tempAFT != null)
              {
                  (tempAFT).DeptName_RunINFtoApplication = true;
                  MainWindow MainWindow2 = new MainWindow();
                  MainWindow2.AFTReport(((tempAFT).DeptName_TesterFirstName), "", "Start", "Continuous Sit_Stand", ActualAverageCurrent.ToString(), false, "");

                  //send custom command 
                  byte[] data = { 0x4e, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
                  Int16 run1Ccounts = SitStandRun1Counts;//SitStand
                  data[4] = (byte)(run1Ccounts & 0xFF);
                  data[3] = (byte)(run1Ccounts >> 8);
                  data[5] = 1;
                  data[9] = 100; data[8] = 100;
                  byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                  byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
                  MainWindow2.RunSystem();
                  StartASCTestRunning = false;
                  StartSitStandTestRunning = true;
                  AFT_DSC_in_process = false;
                  AFT_ASC_in_process = false;
                  AFT_SITSTAND_in_process = true;
                  AFT_All_in_process = false;
                  AFT_WALK_in_process = false;
                  AFT_Backlash_in_process = false;
                  AFT_MotorPerformance_in_process = false;
                  CurrentRunningTestTimer.IsEnabled = true;
              }
        }

        private void ContASCRadBut_Checked(object sender, RoutedEventArgs e)
        {
            //go to ASC  
              App tempAFT = App.Current as App;
              if (tempAFT != null)
              {
                  (tempAFT).DeptName_RunINFtoApplication = true;
                  MainWindow MainWindow2 = new MainWindow();
                  MainWindow2.AFTReport(((tempAFT).DeptName_TesterFirstName), "", "Start", "Continuous ASC", ActualAverageCurrent.ToString(), false, "");

                  //send custom command 
                  byte[] data = { 0x5a, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
                  Int16 run1Ccounts = ASCRun1Counts; //ASC
                  data[4] = (byte)(run1Ccounts & 0xFF);
                  data[3] = (byte)(run1Ccounts >> 8);
                  data[5] = 1;
                  data[9] = 100; data[8] = 100;
                  byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                  byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
                  MainWindow2.RunSystem();
                  StartASCTestRunning = true;
                  StartDSCTestRunning = false;
                  AFT_DSC_in_process = false;
                  AFT_ASC_in_process = true;
                  AFT_All_in_process = false;
                  AFT_SITSTAND_in_process = false;
                  AFT_WALK_in_process = false;
                  AFT_MotorPerformance_in_process = false;
                  CurrentRunningTestTimer.IsEnabled = true;
              }

        }

        private void Reports_Click(object sender, RoutedEventArgs e)
        {

            if (Directory.Exists("\\Argo\\AFT"))
            {
                Process.Start(@"C:\Argo\AFT");
            }
            else
            {
                MessageBox.Show("The folder 'AFT' doesn't exist yet!");
            }

        }

        private void ReadEncodersMAx_Click(object sender, RoutedEventArgs e)
        {
              App tempAFT = App.Current as App;
              if (tempAFT != null)
              {
                  if ((tempAFT).DeptName_Connected)
                  {
                      if (LH.IsChecked == true || LK.IsChecked == true || RH.IsChecked == true || RK.IsChecked == true)
                      {
                          ReadAEncodersMax();
                      }
                      else
                      {
                          MessageBox.Show("One joint should be checked at least first!", "User", MessageBoxButton.OK, MessageBoxImage.Information);
                      }
                  }
                  else
                  {
                      MessageBox.Show("The USB should be connected first!", "User", MessageBoxButton.OK, MessageBoxImage.Information);

                  }
              }

        }

        private void ReadEncodersMIN_Click(object sender, RoutedEventArgs e)
        {
              App tempAFT = App.Current as App;
              if (tempAFT != null)
              {
                  if ((tempAFT).DeptName_Connected)
                  {
                      if (LH.IsChecked == true || LK.IsChecked == true || RH.IsChecked == true || RK.IsChecked == true)
                      {
                          ReadAEncodersMin();
                      }
                      else
                      {
                          MessageBox.Show("One joint should be checked at least first!", "User", MessageBoxButton.OK, MessageBoxImage.Information);
                      }
                  }
                  else
                  {
                      MessageBox.Show("The USB should be connected first!", "User", MessageBoxButton.OK, MessageBoxImage.Information);

                  }
              }
        }





        private void LHRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            lh.Text = "";
            lh2.Text = "";
            lh_b.IsEnabled = false;
        }

        private void RHRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            rh.Text = "";
            rh2.Text = "";
            rh_b.IsEnabled = false;
        }

        private void RKRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            rk.Text = "";
            rk2.Text = "";
            rk_b.IsEnabled = false;
        }

        private void LKRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            lk.Text = "";
            lk2.Text = "";
            lk_b.IsEnabled = false;
        }

        private void LH_Checked(object sender, RoutedEventArgs e)
        {
            LK.IsChecked = false;
            RH.IsChecked = false;
            RK.IsChecked = false;

        }

        private void LK_Checked(object sender, RoutedEventArgs e)
        {
            LH.IsChecked = false;
            RH.IsChecked = false;
            RK.IsChecked = false;
        }

        private void RH_Checked(object sender, RoutedEventArgs e)
        {
            LH.IsChecked = false;
            LK.IsChecked = false;
            RK.IsChecked = false;
        }

        private void RK_Checked(object sender, RoutedEventArgs e)
        {
            LH.IsChecked = false;
            RH.IsChecked = false;
            LK.IsChecked = false;
        }

        private void SystemStopsTestRadBut_Checked(object sender, RoutedEventArgs e)
        {
              App tempAFT = App.Current as App;
              if (tempAFT != null)
              {
                  (tempAFT).DeptName_RunINFtoApplication = true;


                  MainWindow MainWindow1 = new MainWindow();

                  MainWindow1.AFTReport(((tempAFT).DeptName_TesterFirstName), "", "Start", "Stops Test", ActualAverageCurrent.ToString(), false, "");

                  //send custom command 
                  byte[] data = { 0x4f, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
                  Int16 run1Ccounts = StopsTestRun1Counts; //DSC
                  data[4] = (byte)(run1Ccounts & 0xFF);
                  data[3] = (byte)(run1Ccounts >> 8);
                  data[5] = 1;
                  data[6] = 1;
                  data[9] = 100; data[8] = 100;
                  byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                  byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
                  MainWindow1.RunSystem();
                  StartDSCTestRunning = false;
                  AFT_DSC_in_process = false;
                  AFT_WALK_in_process = false;
                  StartWalkTestRunning = false;
                  AFT_ASC_in_process = false;
                  AFT_All_in_process = false;
                  AFT_MotorPerformance_in_process = false;
                  AFT_Backlash_in_process = false;

                  AFT_StopsTest_in_process = true;
                  //StopsTest_TestRunning = true;

                  CurrentRunningTestTimer.IsEnabled = true;
              }
        }

        private void BtnSkipAFT_Click(object sender, RoutedEventArgs e)
        {

            AFT_WALK_in_process = true;
            AFT_SITSTAND_in_process = true;
            AFT_ASC_in_process = true;
            AFT_DSC_in_process = true;
            AFT_StopsTest_in_process = true;
            ResetINFTest();

        }


        private void EditAFTBtn_Checked(object sender, RoutedEventArgs e)
        {
            

           if (!string.IsNullOrEmpty(tbTesterName.Text))
            {
               try
                {
                     App tempAFT = App.Current as App;
                     if (tempAFT != null)
                     {
                         if ((tempAFT).DeptName_Connected)
                         {
                             XmlDocument deviceSettings = new XmlDocument();
                             deviceSettings.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");
                             string en = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTContRecordCurrent").Attributes["Values"].Value;
                             if (en == "1")
                             {
                                 RecordCurrentsEnable();
                                 enablerecordcurrent = true;
                             }

                             string end = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTDefaultParameters").Attributes["Values"].Value;
                             if (end == "1")
                             {
                                 AFTDeviceData.RestoreParameters();
                                 AFTDeviceData.WriteParameters();

                             }
                             MessageBox.Show("Tester registration completed.", "Tester name created", MessageBoxButton.OK, MessageBoxImage.Information);
                             tbTesterName.IsEnabled = false;
                             (tempAFT).DeptName_TesterFirstName = tbTesterName.Text;


                             stCalibrate.IsEnabled = true;
                             Calibrate_the_system.IsEnabled = true;

                             AllRadBut.IsEnabled = true;
                             ContWalkRadBut.IsEnabled = true;
                             ContDSCRadBut.IsEnabled = true; ContASCRadBut.IsEnabled = true;
                             BacklashRadBut.IsEnabled = true; ContSitStandRadBut.IsEnabled = true;
                             MotorPerformanceRadBut.IsEnabled = true; SystemStopsTestRadBut.IsEnabled = true;


                         }
                         else
                         {
                             MessageBox.Show("The USB should be connected first!", "User", MessageBoxButton.OK, MessageBoxImage.Information);
                         }

                     }

                }

                catch (Exception exception)
                {
           //         Logger.Error_MessageBox_AFT("Loading System Configuration Parameters Failed:", "Loading System Configuration", exception.Message);

                }


            }
            else
            {
                MessageBox.Show("Tester name should be filled", "Tester name created",
                MessageBoxButton.OK, MessageBoxImage.Error);
                tbTesterName.IsEnabled = true;

            }
        }














    }
}