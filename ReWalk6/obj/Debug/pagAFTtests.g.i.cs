﻿#pragma checksum "..\..\pagAFTtests.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "9FAD7C653B6C3BECCB17CAF1788C0533"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Expression.Blend.SampleData.SampleDataSource;
using Rewalk;
using RewalkControls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Extensions;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Rewalk {
    
    
    /// <summary>
    /// pagAFTtests
    /// </summary>
    public partial class pagAFTtests : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Rewalk.pagAFTtests Page;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox testerfirstname;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Calibratetool;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Starttool;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Reports;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox WalkCheckBox;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox DSCCheckBox;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox ASCCheckBox;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox SitStandCheckBox;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox BacklashCheckBox;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox MotorPerformanceCheckBox;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton AllRadBut;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton ContWalkRadBut;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton ContSitStandRadBut;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton ContASCRadBut;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton ContDSCRadBut;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton BacklashRadBut;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton MotorPerformanceRadBut;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Ellipse ContWalkPassEllipse;
        
        #line default
        #line hidden
        
        
        #line 135 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Ellipse ContSitStandPassEllipse;
        
        #line default
        #line hidden
        
        
        #line 191 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Ellipse ContASCPassEllipse;
        
        #line default
        #line hidden
        
        
        #line 247 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Ellipse ContDSCPassEllipse;
        
        #line default
        #line hidden
        
        
        #line 303 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Ellipse BacklashPassEllipse;
        
        #line default
        #line hidden
        
        
        #line 359 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Ellipse MotorPerformancePassEllipse;
        
        #line default
        #line hidden
        
        
        #line 415 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ReadEncoders1;
        
        #line default
        #line hidden
        
        
        #line 416 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ReadEncoders2;
        
        #line default
        #line hidden
        
        
        #line 417 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button backlash;
        
        #line default
        #line hidden
        
        
        #line 419 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox lh;
        
        #line default
        #line hidden
        
        
        #line 420 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox rh;
        
        #line default
        #line hidden
        
        
        #line 421 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox lk;
        
        #line default
        #line hidden
        
        
        #line 422 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox rk;
        
        #line default
        #line hidden
        
        
        #line 423 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox lh2;
        
        #line default
        #line hidden
        
        
        #line 424 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox rh2;
        
        #line default
        #line hidden
        
        
        #line 425 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox rk2;
        
        #line default
        #line hidden
        
        
        #line 426 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox lk2;
        
        #line default
        #line hidden
        
        
        #line 431 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox lk_b;
        
        #line default
        #line hidden
        
        
        #line 432 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox rk_b;
        
        #line default
        #line hidden
        
        
        #line 433 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox rk_b_d;
        
        #line default
        #line hidden
        
        
        #line 434 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox rh_b;
        
        #line default
        #line hidden
        
        
        #line 435 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox rh_b_d;
        
        #line default
        #line hidden
        
        
        #line 436 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox lh_b;
        
        #line default
        #line hidden
        
        
        #line 437 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox lh_b_d;
        
        #line default
        #line hidden
        
        
        #line 438 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox lk_b_d;
        
        #line default
        #line hidden
        
        
        #line 443 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton LH;
        
        #line default
        #line hidden
        
        
        #line 446 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton LK;
        
        #line default
        #line hidden
        
        
        #line 449 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton RH;
        
        #line default
        #line hidden
        
        
        #line 452 "..\..\pagAFTtests.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton RK;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ReWalk6;component/pagafttests.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\pagAFTtests.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Page = ((Rewalk.pagAFTtests)(target));
            return;
            case 2:
            this.testerfirstname = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            
            #line 32 "..\..\pagAFTtests.xaml"
            ((System.Windows.Controls.Image)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.btnOk_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.Calibratetool = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\pagAFTtests.xaml"
            this.Calibratetool.Click += new System.Windows.RoutedEventHandler(this.Calibratetool_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.Starttool = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\pagAFTtests.xaml"
            this.Starttool.Click += new System.Windows.RoutedEventHandler(this.Starttool_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.Reports = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\pagAFTtests.xaml"
            this.Reports.Click += new System.Windows.RoutedEventHandler(this.Reports_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.WalkCheckBox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 8:
            this.DSCCheckBox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 9:
            this.ASCCheckBox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 10:
            this.SitStandCheckBox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 11:
            this.BacklashCheckBox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 12:
            this.MotorPerformanceCheckBox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 13:
            this.AllRadBut = ((System.Windows.Controls.RadioButton)(target));
            
            #line 61 "..\..\pagAFTtests.xaml"
            this.AllRadBut.Checked += new System.Windows.RoutedEventHandler(this.AllRadBut_Checked);
            
            #line default
            #line hidden
            return;
            case 14:
            this.ContWalkRadBut = ((System.Windows.Controls.RadioButton)(target));
            
            #line 62 "..\..\pagAFTtests.xaml"
            this.ContWalkRadBut.Checked += new System.Windows.RoutedEventHandler(this.ContWalkRadBut_Checked);
            
            #line default
            #line hidden
            return;
            case 15:
            this.ContSitStandRadBut = ((System.Windows.Controls.RadioButton)(target));
            
            #line 63 "..\..\pagAFTtests.xaml"
            this.ContSitStandRadBut.Checked += new System.Windows.RoutedEventHandler(this.ContSitStandRadBut_Checked);
            
            #line default
            #line hidden
            return;
            case 16:
            this.ContASCRadBut = ((System.Windows.Controls.RadioButton)(target));
            
            #line 64 "..\..\pagAFTtests.xaml"
            this.ContASCRadBut.Checked += new System.Windows.RoutedEventHandler(this.ContASCRadBut_Checked);
            
            #line default
            #line hidden
            return;
            case 17:
            this.ContDSCRadBut = ((System.Windows.Controls.RadioButton)(target));
            
            #line 65 "..\..\pagAFTtests.xaml"
            this.ContDSCRadBut.Checked += new System.Windows.RoutedEventHandler(this.ContDSCRadBut_Checked);
            
            #line default
            #line hidden
            return;
            case 18:
            this.BacklashRadBut = ((System.Windows.Controls.RadioButton)(target));
            
            #line 66 "..\..\pagAFTtests.xaml"
            this.BacklashRadBut.Checked += new System.Windows.RoutedEventHandler(this.BacklashRadBut_Checked);
            
            #line default
            #line hidden
            return;
            case 19:
            this.MotorPerformanceRadBut = ((System.Windows.Controls.RadioButton)(target));
            
            #line 67 "..\..\pagAFTtests.xaml"
            this.MotorPerformanceRadBut.Checked += new System.Windows.RoutedEventHandler(this.MotorPerformanceRadBut_Checked);
            
            #line default
            #line hidden
            return;
            case 20:
            this.ContWalkPassEllipse = ((System.Windows.Shapes.Ellipse)(target));
            return;
            case 21:
            this.ContSitStandPassEllipse = ((System.Windows.Shapes.Ellipse)(target));
            return;
            case 22:
            this.ContASCPassEllipse = ((System.Windows.Shapes.Ellipse)(target));
            return;
            case 23:
            this.ContDSCPassEllipse = ((System.Windows.Shapes.Ellipse)(target));
            return;
            case 24:
            this.BacklashPassEllipse = ((System.Windows.Shapes.Ellipse)(target));
            return;
            case 25:
            this.MotorPerformancePassEllipse = ((System.Windows.Shapes.Ellipse)(target));
            return;
            case 26:
            this.ReadEncoders1 = ((System.Windows.Controls.Button)(target));
            
            #line 415 "..\..\pagAFTtests.xaml"
            this.ReadEncoders1.Click += new System.Windows.RoutedEventHandler(this.ReadEncodersMAx_Click);
            
            #line default
            #line hidden
            return;
            case 27:
            this.ReadEncoders2 = ((System.Windows.Controls.Button)(target));
            
            #line 416 "..\..\pagAFTtests.xaml"
            this.ReadEncoders2.Click += new System.Windows.RoutedEventHandler(this.ReadEncodersMIN_Click);
            
            #line default
            #line hidden
            return;
            case 28:
            this.backlash = ((System.Windows.Controls.Button)(target));
            
            #line 417 "..\..\pagAFTtests.xaml"
            this.backlash.Click += new System.Windows.RoutedEventHandler(this.backlash_Click);
            
            #line default
            #line hidden
            return;
            case 29:
            this.lh = ((System.Windows.Controls.TextBox)(target));
            return;
            case 30:
            this.rh = ((System.Windows.Controls.TextBox)(target));
            return;
            case 31:
            this.lk = ((System.Windows.Controls.TextBox)(target));
            return;
            case 32:
            this.rk = ((System.Windows.Controls.TextBox)(target));
            return;
            case 33:
            this.lh2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 34:
            this.rh2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 35:
            this.rk2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 36:
            this.lk2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 37:
            this.lk_b = ((System.Windows.Controls.TextBox)(target));
            return;
            case 38:
            this.rk_b = ((System.Windows.Controls.TextBox)(target));
            return;
            case 39:
            this.rk_b_d = ((System.Windows.Controls.TextBox)(target));
            return;
            case 40:
            this.rh_b = ((System.Windows.Controls.TextBox)(target));
            return;
            case 41:
            this.rh_b_d = ((System.Windows.Controls.TextBox)(target));
            return;
            case 42:
            this.lh_b = ((System.Windows.Controls.TextBox)(target));
            return;
            case 43:
            this.lh_b_d = ((System.Windows.Controls.TextBox)(target));
            return;
            case 44:
            this.lk_b_d = ((System.Windows.Controls.TextBox)(target));
            return;
            case 45:
            this.LH = ((System.Windows.Controls.RadioButton)(target));
            
            #line 443 "..\..\pagAFTtests.xaml"
            this.LH.Unchecked += new System.Windows.RoutedEventHandler(this.LHRadioButton_Unchecked);
            
            #line default
            #line hidden
            
            #line 443 "..\..\pagAFTtests.xaml"
            this.LH.Checked += new System.Windows.RoutedEventHandler(this.LH_Checked);
            
            #line default
            #line hidden
            return;
            case 46:
            this.LK = ((System.Windows.Controls.RadioButton)(target));
            
            #line 446 "..\..\pagAFTtests.xaml"
            this.LK.Unchecked += new System.Windows.RoutedEventHandler(this.LKRadioButton_Unchecked);
            
            #line default
            #line hidden
            
            #line 446 "..\..\pagAFTtests.xaml"
            this.LK.Checked += new System.Windows.RoutedEventHandler(this.LK_Checked);
            
            #line default
            #line hidden
            return;
            case 47:
            this.RH = ((System.Windows.Controls.RadioButton)(target));
            
            #line 449 "..\..\pagAFTtests.xaml"
            this.RH.Unchecked += new System.Windows.RoutedEventHandler(this.RHRadioButton_Unchecked);
            
            #line default
            #line hidden
            
            #line 449 "..\..\pagAFTtests.xaml"
            this.RH.Checked += new System.Windows.RoutedEventHandler(this.RH_Checked);
            
            #line default
            #line hidden
            return;
            case 48:
            this.RK = ((System.Windows.Controls.RadioButton)(target));
            
            #line 452 "..\..\pagAFTtests.xaml"
            this.RK.Unchecked += new System.Windows.RoutedEventHandler(this.RKRadioButton_Unchecked);
            
            #line default
            #line hidden
            
            #line 452 "..\..\pagAFTtests.xaml"
            this.RK.Checked += new System.Windows.RoutedEventHandler(this.RK_Checked);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

