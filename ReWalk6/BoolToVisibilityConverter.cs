﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Collections.ObjectModel;
using System.Xml;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Xml.Serialization;
using Microsoft.Win32;
using Rewalk.Device;
using System.Diagnostics;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using RewalkControls;
using System.Threading;
using System.Windows.Input;
using System.Security;
using System.Windows.Media;
using System.Windows.Documents;
using System.Net;
using System.Net.Mail;
using System.Windows.Markup;
using FileParser;


namespace ReWalk6
{

    public class BoolToVisibilityConverter : IValueConverter  
    {

        
        #region IValueConverter Members
 		private Visibility valueWhenTrue = Visibility.Visible;
		public Visibility ValueWhenTrue
		{
			get { return valueWhenTrue; }
			set { valueWhenTrue = value; }
		}
 
		private Visibility valueWhenFalse = Visibility.Collapsed;
		public Visibility ValueWhenFalse
		{
			get { return valueWhenFalse; }
			set { valueWhenFalse = value; }
		}
 
		private Visibility valueWhenNull = Visibility.Hidden;
		public Visibility ValueWhenNull
		{
			get { return valueWhenNull; }
			set { valueWhenNull = value; }
		}
 
		private object GetVisibility(object value)
		{
			if (!(value is bool) || value == null)
				return ValueWhenNull;
 
			if ((bool)value)
				return valueWhenTrue;
 
			return valueWhenFalse;
		}
 
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return GetVisibility(value);
		}
 
		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	

        #endregion
    }
}
