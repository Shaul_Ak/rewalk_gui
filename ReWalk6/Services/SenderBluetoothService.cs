﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SenderBluetoothService.cs" company="saramgsilva">
//   Copyright (c) 2014 saramgsilva. All rights reserved.
// </copyright>
// <summary>
//   The Sender bluetooth service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ReWalk6.Shared.Model;
using InTheHand.Net;
using InTheHand.Net.Sockets;

namespace ReWalk6.Services
{
    /// <summary>
    /// The Sender bluetooth service.
    /// </summary>
    public sealed class SenderBluetoothService : ISenderBluetoothService
    {
         private readonly Guid _serviceClassId;

        /// <summary>
        /// Initializes a new instance of the <see cref="SenderBluetoothService"/> class. 
        /// </summary>
        public SenderBluetoothService()
        {
            // this guid is random, only need to match in Sender & Receiver
            // this is like a "key" for the connection!
            _serviceClassId = new Guid("00001101-0000-1000-8000-00805F9B34FB");
        }

        /// <summary>
        /// Gets the devices.
        /// </summary>
        /// <returns>The list of the devices.</returns>
        public async Task<IList<Device>> GetDevices()
        {
            // for not block the UI it will run in a different threat
            var task = Task.Run(() =>
            {
                var devices = new List<Device>();
                using (var bluetoothClient = new BluetoothClient())
                {
                    var array = bluetoothClient.DiscoverDevices();
                    var count = array.Length;
                    for (var i = 0; i < count; i++)
                    {
                        devices.Add(new Device(array[i]));
                    }
                }
                return devices;
            });
            return await task;
        }

        /// <summary>
        /// Sends the data to the Receiver.
        /// </summary>
        /// <param name="device">The device.</param>
        /// <param name="content">The content.</param>
        /// <returns>If was sent or not.</returns>
        public async Task<bool> Send(Device device, string content)
        {
            if (device == null)

                device.ToString();
            

            if (string.IsNullOrEmpty(content))
            {
                throw new ArgumentNullException("content");
            }
            
            // for not block the UI it will run in a different threat
            var task = Task.Run(() =>
            {
           // using (ReceiverBluetoothService.client)  //var bluetoothClient = new BluetoothClient())
              //  {
                 //   while (true)
                  //  {
                        try
                        {
                            var ep = new BluetoothEndPoint(device.DeviceInfo.DeviceAddress, _serviceClassId);
                            BluetoothClient client = new BluetoothClient();


                            client = ReceiverBluetoothService.pre_client;

                             



                            // bluetoothClient.LingerState.Enabled = true;
                            // bluetoothClient.LingerState.LingerTime = 100;

                    //  bluetoothClient.BeginConnect(ep, null, null);





                    // get stream for send the data
                         var bluetoothStream = client.GetStream();

                            // if all is ok to send
                          if (client.Connected && bluetoothStream != null)
                            {
                                // write the data in the stream
                                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                                bluetoothStream.Write(buffer, 0, buffer.Length);
                              //  bluetoothStream.Flush();
                             //   bluetoothStream.Close();
                                return true;
                            }
                         
                           return false;
                        }
                        catch (Exception cc)
                        {
                            string r = cc.Message;
                            // the error will be ignored and the send data will report as not sent
                            // for understood the type of the error, handle the exception
                        }
                 //   }//end while
            //    }
               return false;
            });
            return await task;
        }
    }
}
