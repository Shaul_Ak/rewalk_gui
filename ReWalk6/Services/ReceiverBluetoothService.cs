﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReceiverBluetoothService.cs" company="saramgsilva">
//   Copyright (c) 2014 saramgsilva. All rights reserved.
// </copyright>
// <summary>
//   The Receiver bluetooth service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using InTheHand.Net.Sockets;
using InTheHand.Net;
using ReWalk6.Shared.Model;
using System.Globalization;
using ReWalk6.ViewModel;
using System.Text;
namespace ReWalk6.Services
{
    /// <summary>
    /// The Receiver bluetooth service.
    /// </summary>
    public class ReceiverBluetoothService : ObservableObject, IDisposable, IReceiverBluetoothService
    {
        private readonly Guid _serviceClassId;
        private Action<string> _responseAction;
        private BluetoothListener _listener;
        private CancellationTokenSource _cancelSource;
        private bool _wasStarted;
        private string _status;
        private Device _selectDevice;
        private IAsyncResult result;
        /// <summary>
        /// Initializes a new instance of the <see cref="ReceiverBluetoothService" /> class.
        /// </summary>
        public ReceiverBluetoothService()
        {
            _serviceClassId = new Guid("00001101-0000-1000-8000-00805F9B34FB");
        }

        /// <summary>
        /// Gets or sets a value indicating whether was started.
        /// </summary>
        /// <value>
        /// The was started.
        /// </value>
        public bool WasStarted
        {
            get { return _wasStarted; }
            set { Set(() => WasStarted, ref _wasStarted, value); }
        }

        /// <summary>
        /// Starts the listening from Senders.
        /// </summary>
        /// <param name="reportAction">
        /// The report Action.
        /// </param>
        /// 

        public static BluetoothClient pre_client = null;
        public void Start(Action<string> reportAction, string conn_state)
        {
            DateTime CurrentTimeDate;
            CurrentTimeDate = DateTime.Now;

            try
            {
                if (pre_client == null && BlueView1.SenderViewModel.SelectDevice != null)
                {
                    WasStarted = true;
                    BluetoothAddress addr = BlueView1.SenderViewModel.SelectDevice.DeviceInfo.DeviceAddress;
                    var endPoint = new BluetoothEndPoint(addr, _serviceClassId);
                    BluetoothClient client = new BluetoothClient();
                    client.Connect(endPoint);
                    pre_client = client;
                    _responseAction = reportAction;
                    if (_cancelSource != null && _listener != null)
                    {
                        Dispose(true);
                    }
                    _listener = new BluetoothListener(_serviceClassId)
                    {
                        ServiceName = "MyService"
                    };
                 

                    _listener.Start();
                    BlueView1.ReceiverViewModel.Status = "CONNECTED";
                    BlueView1.ReceiverViewModel.Data = "Can Receive Data!";
                    _cancelSource = new CancellationTokenSource();
                    Task.Run(() => Listener(_cancelSource, client));

                }




                if (conn_state == "CLOSECONN")
                {
                    WasStarted = false;
                    _cancelSource = new CancellationTokenSource();
                    Listener(_cancelSource, pre_client);
                }




            }
            catch (Exception ee)
            {
                string d;
                d = ee.Message;
                WasStarted = false;
                pre_client = null;
                BlueView1.ReceiverViewModel.Status = "NOTCONNECTED";
                BlueView1.ReceiverViewModel.Data =   ee.Message;//"Fail to connect device after timeout or device wasn't found!";



            }


        }

        /// <summary>
        /// Stops the listening from Senders.
        /// </summary>
        public void Stop()
        {
            WasStarted = false;

            Start(SetDataOnce, "CLOSECONN");
            BlueView1.ReceiverViewModel.Data = "Cann't Receive Data!";

        }
        public void SetDataOnce(string data)
        {
            data = "cann't open because ";
        }
        /// <summary>
        /// Listeners the accept bluetooth client.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// 
        /// 
        public Device SelectDevice
        {
            get { return _selectDevice; }
            set { Set(() => SelectDevice, ref _selectDevice, value); }
        }


        public static ViewModelLocator BlueView1 = new ViewModelLocator();
        string[] text_to_message;
        StreamReader eee;
      
        string text_ = "", text_line = "";
        
       
        byte[] bd = new byte[300];
        int i;
        
        private void Listener(CancellationTokenSource token , BluetoothClient client)
        {

            try
            {
                while (true && WasStarted)
                {

                    
                      StreamReader sr = new StreamReader(client.GetStream());


                     while (sr.Peek() >= 0 && !text_.EndsWith("%"))
                       {
                           text_line = sr.ReadLine();
                           text_ += text_line;                 

                     }
                     

                    /********************** binary **********************************************************************
                                    BinaryReader sr = new BinaryReader(client.GetStream());

                                    i = 0;
                                    bd[0] = sr.ReadByte();
                                    while (bd[i]!=37)  // ==  ("%"))
                                                {
                                                    i++;
                                                    bd[i]= sr.ReadByte();                                                                            

                                            }


                    byte[] bf = new byte[4] { 0x3c, 0xa7, 0xfd, 0x17 };
                    _responseAction(BitConverter.ToString(bd, 0));
                    float fs = BitConverter.ToSingle(bf, 0);
                    float fs1 = BitConverter.ToSingle(bd, 5);
                    /************************************************************************************************/

                    if (text_.EndsWith("%"))
                    {
                        _responseAction(text_line);
                        
                        text_to_message = text_.Split('#');
                        if (text_to_message[0] == "ENCY" || text_to_message[0] == "ENCX")
                        {
                                                   
                            uint num = uint.Parse(text_to_message[1], System.Globalization.NumberStyles.AllowHexSpecifier);

                            byte[] floatVals = BitConverter.GetBytes(num);
                            float f = BitConverter.ToSingle(floatVals, 0);


                            _responseAction(text_line +  "  ==Value: " + f.ToString() );
                        }
                        
                    }

                    else
                        _responseAction(text_);



                

                    text_ = "";
                    text_line = "";



              } //end while 


            if (WasStarted == false )
            {

                if (client.Connected == true)
                {

                    client.Close();                                          
                    _listener.Stop();                     
                     pre_client = null;                  
                    _cancelSource.Cancel();
                    Thread.Sleep(100);

                }

                    return;

                }
} //end try


    catch (Exception exception)
    {
        _responseAction(exception.Message);
    }               
}

/// <summary>
/// The dispose.
/// </summary>
public void Dispose()
{
Dispose(true);
GC.SuppressFinalize(this);
}

/// <summary>
/// The dispose.
/// </summary>
/// <param name="disposing">
/// The disposing.
/// </param>
protected virtual void Dispose(bool disposing)
{
if (disposing)
{
if (_cancelSource != null)
{
_listener.Stop();
_listener = null;
_cancelSource.Dispose();
_cancelSource = null;

}
}
}
}
}
