﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading;

namespace ReWalk6
{
    /// <summary>
    /// Interaction logic for MCU_ProgrammingWindow.xaml
    /// </summary>
    public partial class MCU_ProgrammingWindow : Window
    {
        
        public MCU_ProgrammingWindow()
        {
              InitializeComponent();

            // Prepare the BackgroundWorker.
              m_BackgroundWorker = new BackgroundWorker();
              m_BackgroundWorker.WorkerReportsProgress = true;
              m_BackgroundWorker.WorkerSupportsCancellation = true;
              
              m_BackgroundWorker.DoWork += BackgroundWorker_DoWork;
              m_BackgroundWorker.ProgressChanged += BackgroundWorker_ProgressChanged;
            //  m_BackgroundWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;


        }

        
        Stopwatch stopwatch = new Stopwatch(); 
        
        // Simulate a long task.
        private void BackgroundWorker_DoWork(Object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            int _val = 0;
            // Random rand = new Random();
             while (pvalue < 30000 && (mcu_downloading_in_process==1))
             {
                 // Wait a little while.
                // Thread.Sleep(200);
                 stopwatch.Start();
                 while (stopwatch.ElapsedMilliseconds < 200)
                 {
                     _val++;
                 }
                 stopwatch.Reset();
                 _val = 0;
                
                 // Add a bit to the progress.
                 pvalue +=150;
                 
                 // Update the UI thread.
                 //lblWorking.Content = " LH change"+ value.ToString();
                 m_BackgroundWorker.ReportProgress(pvalue);
             }
            
        }
        // The BackgroundWorker that will perform the long task.
        private BackgroundWorker m_BackgroundWorker;
        private int pvalue = 0;
        private int mcu_downloading_in_process= 0;
        private int inf_downloading_in_process = 0;
        private bool Timer_downloading_inprocess = false;

        public BackgroundWorker M_BACKGROUNDWORKER
        {
            get { return m_BackgroundWorker; }
            set { m_BackgroundWorker = value; }
        }

        public int MCUDOWNLOADINGINPROCESS
        {
            get { return mcu_downloading_in_process; }
            set { mcu_downloading_in_process = value; }
        }
        public int INFDOWNLOADINGPROCESS
        {
            get { return inf_downloading_in_process; }
            set { inf_downloading_in_process = value; }
        }
        public bool TIMERDOWNLOADINPROCESS
        {
            get { return Timer_downloading_inprocess; }
            set { Timer_downloading_inprocess = value; }
        }

        public int VALUE
        {
            get { return pvalue; }
            set { pvalue = value; }
        }


        // Start the long task.
        public  void Show_progressbar()
        {
            // Display the progress controls.
            prgWorking.Value = 0;
            prgWorking.Visibility = Visibility.Visible;
            lblWorking.Visibility = Visibility.Visible;

            // Asynchronously start the routine
            // that does the long calculation.
          //  m_BackgroundWorker.RunWorkerAsync();
        }
        // Display the progress.
        private void BackgroundWorker_ProgressChanged(Object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            prgWorking.Value = e.ProgressPercentage;
        }

        // Reset the UI.
     /*   private void BackgroundWorker_RunWorkerCompleted(Object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
           // prgWorking.Visibility = Visibility.Hidden;
           // lblWorking.Visibility = Visibility.Hidden;
           
        }*/
    
         
    }
}
