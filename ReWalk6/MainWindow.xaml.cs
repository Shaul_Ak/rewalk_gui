﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Win32;
using Rewalk.Device;
using System.Diagnostics;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using RewalkControls;
using System.Threading;
using System.Windows.Input;
using System.Security;
using System.Text;
using System.Windows.Media;
using System.Windows.Documents;
using System.Net;
using System.Net.Mail;
using System.Windows.Markup;
using System.Collections.Generic;
using FileParser;
using System.Windows.Media.Animation;
using System.Configuration;
using Resolution;

using ReWalk6.ViewModel;
using ReWalk6.Services;
using ReWalk6.Shared.ViewModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.ObjectModel;
using ReWalk6.Shared.Model;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using ReWalk6.Shared.Model;
using InTheHand.Net;
using InTheHand.Net.Sockets;

namespace ReWalk6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        string InstallingAppDir = Directory.GetCurrentDirectory();
        private bool LogFileSent = false;
        [Flags]
        enum ReportType { ERROR, NORMAL , WARNING};

        string[] Default_Intial_Graph_Parameters = { "Index","lc1","lc2", "lGyrZ", "rGyrZ", "Pos1", "Pos2", "Bat1Vo", "Bat1Cu", "Bat2Vo", "Bat2Cu","Bat1Te","Bat2Te", "Bat1RC", "Bat2RC", "Bat1RTE", "Bat2RTE" };
        string[] Default_Intial_Graph_Parameters_Units = { "msec", "N", "N", "mm", "mm","mm","mm", "mV","mA", "mV", "mA","C","C","mAh","mAh","min","min"};

        
        private string LogPath;
        private string SysLogPath;
        private string EncoderTestPath;
        private short Short_GUIVersion;
        private string UserProfilesPath;
        public  string StrokeFolder;
        string KSTGraphsAppFolder;
        public string KST_Config_Data_File_Name;
        string KST_App_Run_File_File_Name;
        string InstalledKSTFolder;
        string Installed_KST_Config_Data_File_Name;
        string Installed_KST_App_Run_File_File_Name;
        private bool TechnicianScreenWasEntered = false;
        private  bool ProductionVersionAFT;
        // double CurrentScreenHeight;
        // double CurrentScreenWidth;

    
     
        public MainWindow()
        {

            this.Dispatcher.UnhandledException += OnDispatcherUnhandledException;

            InitializeComponent();
            Detect_killRewalkRunningApllication();
            DataContext = deviceData;
            
          
          //  ReWalkStartUpConfiguration();
            UploadLanguageSelection();
            TranslationMessagesTestVisibilty();
          //  tbInterfaceVersion.Text = "Rewalk Interface " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            string InterfaceVersion =  "Rewalk Interface " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            System.Version GUIVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            Short_GUIVersion = (short)(GUIVersion.Major * 100 + GUIVersion.Build * 10 + GUIVersion.Revision);
            tbInterfaceVersion.Text = Logger.Validate(InterfaceVersion);
            deviceData.Parameters.SampleTime = Short_GUIVersion;
            deviceData.Parameters.SampleTime = Short_GUIVersion;
            deviceData.Parameters.SampleTime = Short_GUIVersion;
            deviceData.Parameters.StairsEnabled = true;

            deviceData.Parameters.RCAddress = 0; deviceData.Parameters.RCId = 0;
            Events_Init();
            this.Closed += new EventHandler(OnMainClosed);
            if (DesignerProperties.GetIsInDesignMode(this) == false)
            {
                deviceSettingsValuesDataProvider = (XmlDataProvider)FindResource("deviceSettingsValues");
                deviceSettingsValuesDataProvider.Document = new XmlDocument();
                deviceSettingsValuesDataProvider.Document.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml"); deviceSettingsValuesDataProvider.XPath = "DeviceSettingsValues";

            }
           
            //    SetTitle(null);
            btnUpdateINF.IsEnabled = false;
            cbSysType.IsEnabled = true;
            //btnSetSN.IsEnabled = false; 
            btnUpdateRC.IsEnabled = false;//Must do this after InitializeComponent to run animation                 
            // Knee_Flexion.Items.CopyTo(array1, 0);
            btnBootBrowse.IsEnabled = false; btnBootDownload.IsEnabled = false;
            string temp = AppDomain.CurrentDomain.BaseDirectory; string[] temp1;
            DirectoryInfo dir_info;
            temp1 = temp.Split('\\');
            LogPath = temp1[0] + "\\Argo\\logs";
            EncoderTestPath = temp1[0] + "\\Argo\\Encoder_Plots";
            string AFTFolderPath = temp1[0] + "\\Argo\\AFT";
            UserProfilesPath = temp1[0] + "\\Argo\\UsersProfiles";
            StrokeFolder = temp1[0] + "\\ReWalk Robotic\\Stroke";
            KSTGraphsAppFolder = temp1[0] + "\\ReWalk Robotic\\KST";
            KST_Config_Data_File_Name = temp1[0] + "\\ReWalk Robotic\\KST\\Kst_Graph_Data.txt";
            /*
            BlueView.ReceiverViewModel.Blue_strings_Data_File_Name = StrokeFolder  + "BlueReceivedData_Debug.txt";

            using (StreamWriter Blue_strings_data_file = File.CreateText(BlueView.ReceiverViewModel.Blue_strings_Data_File_Name))
            {

                Blue_strings_data_file.WriteLine("Start Writing :- " + "\n");
            }
            */
            KST_App_Run_File_File_Name = temp1[0] + "\\ReWalk Robotic\\KST\\Kst_Draw.kst";
            InstalledKSTFolder = AppDomain.CurrentDomain.BaseDirectory + "\\KST";
            Installed_KST_Config_Data_File_Name = AppDomain.CurrentDomain.BaseDirectory +  "\\KST\\Kst_Graph_Data.txt";
            Installed_KST_App_Run_File_File_Name = AppDomain.CurrentDomain.BaseDirectory + "\\KST\\Kst_Draw.kst";

            try
            {
                if (!Directory.Exists(AFTFolderPath))
                    dir_info = Directory.CreateDirectory(AFTFolderPath);
                if (!Directory.Exists(LogPath))
                    dir_info = Directory.CreateDirectory(LogPath);
                SysLogPath = temp1[0] + "\\Argo\\logs\\sys_logs";
                if (!Directory.Exists(SysLogPath))
                    dir_info = Directory.CreateDirectory(SysLogPath);
                if (!Directory.Exists(EncoderTestPath))
                    dir_info = Directory.CreateDirectory(EncoderTestPath);
                if (!Directory.Exists(UserProfilesPath))
                    dir_info = Directory.CreateDirectory(UserProfilesPath);
                if (!Directory.Exists(StrokeFolder))
                    dir_info = Directory.CreateDirectory(StrokeFolder);
                if (!Directory.Exists(KSTGraphsAppFolder))
                    dir_info = Directory.CreateDirectory(KSTGraphsAppFolder);


               
                if (File.Exists(Installed_KST_Config_Data_File_Name) && !File.Exists(KST_Config_Data_File_Name))
                    File.Copy(Installed_KST_Config_Data_File_Name , KST_Config_Data_File_Name);
                                    
                if (File.Exists(Installed_KST_App_Run_File_File_Name) && !File.Exists(KST_App_Run_File_File_Name))
                    File.Copy(Installed_KST_App_Run_File_File_Name, KST_App_Run_File_File_Name);
                         
            //    File.OpenWrite(KST_Config_Data_File_Name);
                Write_session_graphs_data_init(Default_Intial_Graph_Parameters, Default_Intial_Graph_Parameters_Units);

            }
            catch (Exception exception)
            {
                Logger.Error("", exception.Message);//doesnt show error excpted of not connect
            }

          //  ProductionVersionAFT =  AFTToolScreenSet();
          //  OnDisconenctUSB();
         //   ProductionVersionAFT = AFTToolScreenSet();
          /******** for debugging ******************/
          ShowPYTH.IsEnabled = true;
            //Test_Messages_Translation();
            //Read.IsEnabled = true;
        //    deviceData.Parameters.StairsEnabled = true;
         /******************************************/
            //New GUI 6
            XMLParamLoadCheckValid.GainMinMaxLengthValues();
            XMLParamLoadCheckValid.kx.CopyTo(array2, 0);
            deviceData.Parameters.StairsASCSpeed = System.Convert.ToInt16(XMLParamLoadCheckValid.Ascsp[0]);
            deviceData.Parameters.StairsDSCSpeed = System.Convert.ToInt16(XMLParamLoadCheckValid.Dscsp[0]);
         //   NoteBtn.Visibility = Visibility.Visible;
            TopToolBarButtons.Visibility = Visibility.Hidden;
            PasswordCheckingMessage.Text = "";
          //  PT.Visibility = Visibility.Visible;
            PT.IsSelected = false;
         //   UD.Visibility = Visibility.Visible;
          //  UserDetails.Visibility = Visibility.Visible;
          //  deviceData.Parameters.Gender = false;
          //  TopToolBarButtons.Visibility = Visibility.Visible;
          //  EditBtn.Visibility = Visibility.Visible;
          //  TopToolBar.Visibility = Visibility.Visible;
            EditBtn.IsChecked = false;
            NoteBtn.Visibility = Visibility.Collapsed;
            F.IsChecked = false;
            M.IsChecked = false;
            Write.IsEnabled = true;
            OpenUserfile.IsEnabled = true;
            _CloseButton.IsEnabled = true;
            Save.IsEnabled = true;
            InkInputHelper.DisableWPFTabletSupport();
            //  Debug.WriteLine("Hello Rasem!!!!");
            BatteryFull.Visibility = Visibility.Collapsed;
            BatteryEmpty.Visibility = Visibility.Collapsed;
            BatteryHalf.Visibility = Visibility.Collapsed;
            BatteryThird.Visibility = Visibility.Collapsed;

            SystemInfo.Visibility = Visibility.Collapsed;

            BlueView.ReceiverViewModel.KST_Config_Data_File_Name_Global = KST_Config_Data_File_Name;
       
        }

        /******************************** End Graphs ******************************************/

        private DispatcherTimer GraphTimerUpdate = new DispatcherTimer();


        private int graph_refresh_time = 0;

        private void GraphTimerUpdateHandler(object sender, EventArgs e)
        {
            string[] ss = new string[10];
            graph_refresh_time = graph_refresh_time + 1;

            ss[0]=(graph_refresh_time).ToString();
            ss[1] = BlueView.ReceiverViewModel.IMU_L0.ToString();
          //  ss[2] = BlueView.ReceiverViewModel.IMU_L1.ToString();
      
            StrokeGrapghReport(ss);
        }

    
        public void StrokeGrapghReport(string[] graph_data)
        {

            using (StreamWriter kst_constant_configuration_data_file = File.AppendText(KST_Config_Data_File_Name))
            {
                kst_constant_configuration_data_file.WriteLine(graph_data[0] + " ; " + graph_data[1] + " ; ");// + graph_data[2]);
           }

         
        }



        private void Events_Init()
        {
         //   GraphTimerUpdate.Interval = TimeSpan.FromMilliseconds(10);
           // GraphTimerUpdate.Tick += new EventHandler(GraphTimerUpdateHandler);
            GraphTimerUpdate.IsEnabled = false;

            onlineTimer.Tick += new EventHandler(OnCollectDataTimer);
            testTimer.Interval = TimeSpan.FromSeconds(10);
            testTimer.Tick += new EventHandler(OnTestTimer_Tick);
           // Trainning.OnChanged += new EventHandler(Trainingmode_OnChanged);
            //  FirstStep.OnChanged += new EventHandler(OnEnableDisableFirstStep);
            // safetyonoff.OnChanged += new EventHandler(OnOffBox_OnChanged);
            // collectOnline.Click += new EventHandler(OnCollectOnlineDataChanged);
            //collectOnline.Checked += new EventHandler(OnCollectOnlineDataChanged);
            //  collectOnline.Click += new RoutedEventHandler(Mytoogle);
         //   RFtestTimer.Tick += new EventHandler(OnRFTestTimer_Tick);
         //   RCUpdateTimer.Tick += new EventHandler(OnUpdateRCAddressIDTimer_Tick);
         //   calibTimer.Tick += new EventHandler(OnCalibrationTimer_Tick);
            CheckUSBConnectedTime.Tick += new EventHandler(OnCheckUSBConnectedTimeTimer_Tick);
        //    TestEncoderTimer.Tick += new EventHandler(OnTestEncoderTimer_Tick);
            BatteryStatusTimer.Tick += new EventHandler(BatteryStatusTimer_Tick);
        //    RCUpdateTimer.Interval = TimeSpan.FromSeconds(1);
        //    RFtestTimer.Interval = TimeSpan.FromSeconds(1);
       //     calibTimer.Interval = TimeSpan.FromSeconds(1);
       //     TestEncoderTimer.Interval = TimeSpan.FromSeconds(1);
            CheckUSBConnectedTime.Interval = TimeSpan.FromMilliseconds(200);//SamanUSB
            BatteryStatusTimer.Interval = TimeSpan.FromMilliseconds(500);

            //Timer Added 
            UpdateProgressBarTimer.Tick += new EventHandler(UpdateProgressBarTimer_Tick);
            UpdateProgressBarTimer.Interval = TimeSpan.FromMilliseconds(100);


            calibTimer.IsEnabled = false;
            TestEncoderTimer.IsEnabled = false;
            CheckUSBConnectedTime.IsEnabled = true;
            testTimer.IsEnabled = false;
            BatteryStatusTimer.IsEnabled = false;

            BatteryStatusTimer.IsEnabled = true;
            BatteryStatusTimer.Start();


        }
        void OnDispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
           // string errorMessage = "";// e.Exception.Message;// string.Format("An unhandled exception occurred: {0}", e.Exception.Message);
            MessageBox.Show(e.Exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
         //   Resolution.CResolution ChangeRes = new Resolution.CResolution((int)CurrentScreenWidth,(int)CurrentScreenHeight);//Set  screen resolution back
  
            e.Handled = true;
            return;
         //   Application.Current.Shutdown();
           // throw new DeviceOperationException("");                      
        }
        

        void OnMainClosed(object sender, EventArgs e)
        {

            CheckUSBConnectedTime.IsEnabled = false;
            deviceData.Disconnect();
            progressform.Close();
            GenerateRCAddressIDForm.Close();
            MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
            MCUDownloadingProcessWindow.Close();
            //  Resolution.CResolution ChangeRes = new Resolution.CResolution((int)CurrentScreenWidth, (int)CurrentScreenHeight);//Set  screen resolution back

            try
            {

                Process[] cc = System.Diagnostics.Process.GetProcessesByName("Kst2");

                foreach (Process i in cc)
                    i.Kill();
            }
            catch (Exception ex)
            {
                //   MessageBox.Show("KST was closed before ReWalk :- "+ ex.Message);
            }

            Application.Current.Shutdown();
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            Process.GetCurrentProcess().Kill();

        }
        private void UploadLanguageSelection()//This Function uploading Language Selection
        {
            string appPath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
            if (GUIVersionType == "USA")
            {
                HelpFileName = appPath + "\\TherapistManual_English\\TherapistManualUSA.pdf";
            }
            else if (GUIVersionType == "Other")
            {
                HelpFileName = appPath + "\\TherapistManual_English\\TherapistManualOther.pdf";
            }

            if (Properties.Settings.Default.Language_Selection == "English")
               English.IsChecked = true;
            if (Properties.Settings.Default.Language_Selection == "Deutsch")
               Deutsch.IsChecked = true;
            if (Properties.Settings.Default.Language_Selection == "Français")
               Français.IsChecked = true;
            if (Properties.Settings.Default.Language_Selection == "Italiano")
               Italiano.IsChecked = true;
            if (Properties.Settings.Default.Language_Selection == "Türkçe")
                Türkçe.IsChecked = true;
            if (Properties.Settings.Default.Language_Selection == "日本語")
                日本語.IsChecked = true;
            if (Properties.Settings.Default.Language_Selection == "Español")
               español.IsChecked = true;
            if (Properties.Settings.Default.Language_Selection == "Pусский")
               русский.IsChecked = true;


        }
        private void OnCaptionButton(object sender, RoutedEventArgs e)
        {
            
            CheckUSBConnectedTime.IsEnabled = false;
            deviceData.Disconnect();
            progressform.Close();
            GenerateRCAddressIDForm.Close();

            MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
            MCUDownloadingProcessWindow.Close();
            try
            {

                Process[] cc = System.Diagnostics.Process.GetProcessesByName("Kst2");

                foreach (Process i in cc)
                    i.Kill();
            }
            catch(Exception ex)
            {
             //   MessageBox.Show("KST was closed before ReWalk :- "+ ex.Message);
            }

           
            Process.GetCurrentProcess().Kill();
            Application.Current.Shutdown();

        

        }
      
        private void SetTitle(string file) // parasoft-suppress  METRICS.MCCC "Methods executing atomic functions"
        {

            if ((User1profile.Content).ToString() == "File Name 1") User1profile.Content = file;
             
         else if ((string)User2profile.Content == "File Name 2") User2profile.Content = file;
            
         else if ((string)User3profile.Content == "File Name 3") User3profile.Content = file;
            
         else if ((string)User4profile.Content == "File Name 4") User4profile.Content = file;
            
         else if ((string)User5profile.Content == "File Name 5") User5profile.Content = file;
           
        else if ((string)User6profile.Content == "File Name 6") User6profile.Content = file;
           
         else if ((string)User7profile.Content == "File Name 7") User7profile.Content = file;
            
         else if ((string)User8profile.Content == "File Name 8") User8profile.Content = file;
            
         else if ((string)User9profile.Content == "File Name 9") User9profile.Content = file;
            
         else if ((string)User10profile.Content == "File Name 10") User10profile.Content = file;
            
         else if ((string)User11profile.Content == "File Name 11") User11profile.Content = file;
           
         else if ((string)User12profile.Content == "File Name 12") User12profile.Content = file;
            
         else if ((string)User13profile.Content == "File Name 13") User13profile.Content = file;
            
         else if ((string)User14profile.Content == "File Name 14") User14profile.Content = file;
            
         else if ((string)User15profile.Content == "File Name 15") User15profile.Content = file;
          
             
        }

  

        private bool CreateUserStringAsDefualt = false;

        private void OnLoadConfiguration(object sender, RoutedEventArgs e) // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions" CS.MLC "Methods executing atomic functions"
        {
            TbRightMenu.IsChecked = false;
            OpenFileDialog ofd = new OpenFileDialog();
            Data deviceData_temp = new Data();
            ofd.CheckFileExists = true;
            ofd.Filter = "Argo Config files (*.xarg)|*.xarg|(*.*) |*.*";
            FileStream fs;
           
            try
            {
                
                if (ofd.ShowDialog().Value == true)
                {
                    using (fs = new FileStream(ofd.FileName, FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(DeviceParameters));
                        deviceData_temp.Parameters = (DeviceParameters)serializer.Deserialize(fs);
                        if (Temp_ParamData_StructureCopy(deviceData_temp) == false)
                            return;

                    }
                    if (deviceData_temp.Parameters.SampleTime >= Short_GUIVersion)
                    {
                        UserParamData_VerfiytoSave(deviceData_temp);
                        using (fs = new FileStream(ofd.FileName, FileMode.Create))
                        {
                            XmlSerializer serializer_save = new XmlSerializer(typeof(DeviceParameters));
                            deviceData.Parameters.SampleTime = Short_GUIVersion;
                            // Overwrite the configuration file 
                            serializer_save.Serialize(fs, deviceData.Parameters);
                        }
                        fs.Close();
                        SetTitle(Path.GetFileNameWithoutExtension(ofd.FileName));


                        Button but = sender as Button;

                        if (but != null)
                        {
                            if ((string)(but.Name) == "Load")
                            {
                                PT.IsSelected = true;
                                TbRightMenu.IsChecked = false;
                                TItemSystemMeasure.IsSelected = true;
                                TopToolBar.Visibility = Visibility.Visible;
                                TopToolBarButtons.Visibility = Visibility.Visible;
                                UD.Visibility = Visibility.Visible;
                                EditBtn.Visibility = Visibility.Visible;
                                EditBtn.IsChecked = false;
                                SystemInfo.Visibility = Visibility.Visible;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show(this, (string)this.FindResource("This file was created by an older Rewalk interface version and is not compatible to the current version. In order to continue the operation it will be converted to a compatible format and saved."),
                                                (string)this.FindResource("User file Loading"), MessageBoxButton.OK, MessageBoxImage.Information);
                        deviceData.RestoreParameters();
                        if (deviceData_temp.Parameters.SampleTime < 400) //this last released GUI 3.5.xx which has no strings      
                            CreateUserStringAsDefualt = true;
                        deviceData.Parameters.Enable_First_Step_Rewalk6 = deviceData_temp.Parameters.EnableFirstStep;
                        UserParamData_VerfiytoSave(deviceData_temp);
                        CreateUserStringAsDefualt = false;
                        // Overwrite the configuration file 
                        using (fs = new FileStream(ofd.FileName, FileMode.Create))
                        {
                            XmlSerializer serializer_save = new XmlSerializer(typeof(DeviceParameters));
                            deviceData.Parameters.SampleTime = Short_GUIVersion;
                            serializer_save.Serialize(fs, deviceData.Parameters);
                        }
                        fs.Close();
                        SetTitle(Path.GetFileNameWithoutExtension(ofd.FileName));
                        Button but = sender as Button;

                        if (but != null)
                        {
                            if ((string)(but.Name) == "Load")
                            {
                                PT.IsSelected = true;
                                TbRightMenu.IsChecked = false;
                                TItemSystemMeasure.IsSelected = true;
                                TopToolBar.Visibility = Visibility.Visible;
                                TopToolBarButtons.Visibility = Visibility.Visible;
                                UD.Visibility = Visibility.Visible;
                                EditBtn.Visibility = Visibility.Visible;
                                EditBtn.IsChecked = false;
                            }
                        }
                    }
                    InitParamWithUnitsConv();
                    //Open radion button in Walk and Stand screens to prevent buttons overlapping
                    StandingUpFallDetection.IsChecked = true;
                    FirstStepAssistance.IsChecked = true;
                    //Accoording to parameters reading update texts and toggle buttons
                    Write.IsEnabled = true;
                    //Accoording to parameters reading update texts and toggle buttons
                    if (deviceData.Parameters.Enable_First_Step_Rewalk6 == true)
                    {
                        FirstStepAssistance.IsChecked = true;
                        OFF1Sts.Visibility = Visibility.Collapsed;
                        ON1Sts.Visibility = Visibility.Visible;
                        F1sSBtns.Visibility = Visibility.Visible;
                    }
                    else if (deviceData.Parameters.Enable_First_Step_Rewalk6 == false)
                    {
                        OFF1Sts.Visibility = Visibility.Visible;
                        ON1Sts.Visibility = Visibility.Collapsed;
                        F1sSBtns.Visibility = Visibility.Collapsed;
                    }
                    if (deviceData.Parameters.SafetyWhileStanding == true)
                    {
                        StandingUpFallDetection.IsChecked = true;
                        OFF.Visibility = Visibility.Collapsed;
                        ON.Visibility = Visibility.Visible;
                        FDTBtns.Visibility = Visibility.Visible;
                    }
                    else if (deviceData.Parameters.SafetyWhileStanding == false)
                    {

                        OFF.Visibility = Visibility.Visible;
                        ON.Visibility = Visibility.Collapsed;
                        FDTBtns.Visibility = Visibility.Collapsed;
                    }

                    Write4.Foreground = Brushes.Red;
                    Write.IsChecked = true;
                    Write2.Foreground = Brushes.Red;
                    Write3.Foreground = Brushes.Red;
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Load configuration failed", exception.Message);
            }
        }

        
        private bool Temp_ParamData_StructureCopy(Data deviceData_temp)
        {
            
            if (deviceData_temp.Parameters.SysType != deviceData.Parameters.SysType )
            {
                MessageBox.Show((string)this.FindResource("Sorry the file can't be opened because it has different type of system"), "", MessageBoxButton.OK, MessageBoxImage.Hand);
                return false;
            }
            else
            {
            deviceData_temp.Parameters.RCAddress = deviceData.Parameters.RCAddress;
            deviceData_temp.Parameters.RCId = deviceData.Parameters.RCId;
            deviceData_temp.Parameters.RCBER = deviceData.Parameters.RCBER;
            deviceData_temp.Parameters.Remote = deviceData.Parameters.Remote;
            deviceData_temp.Parameters.RFProgress = deviceData.Parameters.RFProgress;
            deviceData_temp.Parameters.Sn1Char = deviceData.Parameters.Sn1Char;
            deviceData_temp.Parameters.Sn1 = deviceData.Parameters.Sn1;
            deviceData_temp.Parameters.Sn2 = deviceData.Parameters.Sn2;
            deviceData_temp.Parameters.Sn3 = deviceData.Parameters.Sn3;
            deviceData_temp.Parameters.StairsEnabled = deviceData.Parameters.StairsEnabled;
            deviceData_temp.Parameters.SysType = deviceData.Parameters.SysType;
            deviceData_temp.Parameters.SysTypeString = deviceData.Parameters.SysTypeString;
            deviceData_temp.Parameters.DSP = deviceData.Parameters.DSP;
            deviceData_temp.Parameters.Boot = deviceData.Parameters.Boot;
            deviceData_temp.Parameters.Motor = deviceData.Parameters.Motor;
            deviceData_temp.Parameters.FPGA = deviceData.Parameters.FPGA;
            deviceData_temp.Parameters.Device = deviceData.Parameters.Device;
            deviceData_temp.Parameters.RcSN = deviceData.Parameters.RcSN;
            deviceData_temp.Parameters.InfSN = deviceData.Parameters.InfSN;
            deviceData_temp.Parameters.LkSN = deviceData.Parameters.LkSN;
            deviceData_temp.Parameters.LhSN = deviceData.Parameters.LhSN;
            deviceData_temp.Parameters.RkSN = deviceData.Parameters.RkSN;
            deviceData_temp.Parameters.RhSN = deviceData.Parameters.RhSN;
            deviceData_temp.Parameters.EnableSystemStuckOnError_int = deviceData.Parameters.EnableSystemStuckOnError_int;
            deviceData_temp.Parameters.SwitchingTime = deviceData.Parameters.SwitchingTime;
            deviceData_temp.Parameters.StepCounter = deviceData.Parameters.StepCounter;
            deviceData_temp.Parameters.LogLevel.ADC = deviceData.Parameters.LogLevel.ADC;
            deviceData_temp.Parameters.LogLevel.CAN = deviceData.Parameters.LogLevel.CAN;
            deviceData_temp.Parameters.LogLevel.STAIRS = deviceData.Parameters.LogLevel.STAIRS;
            deviceData_temp.Parameters.LogLevel.USB = deviceData.Parameters.LogLevel.USB;
            deviceData_temp.Parameters.LogLevel.MCU = deviceData.Parameters.LogLevel.MCU;
            deviceData_temp.Parameters.LogLevel.RF = deviceData.Parameters.LogLevel.RF;
            deviceData_temp.Parameters.LogLevel.BIT = deviceData.Parameters.LogLevel.BIT;
            deviceData_temp.Parameters.LogLevel.RC = deviceData.Parameters.LogLevel.RC;
            deviceData_temp.Parameters.LogLevel.SIT_STN = deviceData.Parameters.LogLevel.SIT_STN;
            deviceData_temp.Parameters.LogLevel.WALK = deviceData.Parameters.LogLevel.WALK;
            deviceData_temp.Parameters.CurrentHandleAlgo = deviceData.Parameters.CurrentHandleAlgo;
            deviceData_temp.Parameters.LogLevel.MEM = deviceData.Parameters.LogLevel.MEM;
            deviceData_temp.Parameters.LogLevel.OPS = deviceData.Parameters.LogLevel.OPS;
            return true;
            }

        }
        byte[] MCUFileArray, INFDSPFileArray, INFBootFileArray;
        private void Delay_ms(double ms)
        {
            double _val = 0;
            Stopwatch stopwatch1 = new Stopwatch();
            stopwatch1.Start();

            while (stopwatch1.ElapsedMilliseconds < ms)
            {
                _val++;
            }
            stopwatch1.Reset();
            _val = 0;

        }

      
        public static byte[] StrToByteArray(string str)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return encoding.GetBytes(str);
        }

        private MCU_ProgrammingWindow INFDownLoadinProcess = new MCU_ProgrammingWindow();
        private bool PIC_included_flag = false;
        private string downloadstatus = null;
        private string PIC_SW_VERSION = null;
        private string PIC_SW = null;
        private void OnDownloadDspFile(object sender, RoutedEventArgs e)
        {
            if (ISINFAPPFileWasSelected)
            {
                testTimer.IsEnabled = true;
                //  MCUDownloadingProcessWindow.Title = "INF Download in process"; 
                //  MCUDownloadingProcessWindow.lblWorking.Content = "INF Download in process \nPlease don't shutdown the system";
                INFDownLoadinProcess.Title = (string)FindResource("INF App Download");
                INFDownLoadinProcess.lblWorking.Content = (string)FindResource("INF App Download") + "\n" + (string)FindResource("please do not turn off the system");

                string inf_app = null;



                byte[] INFDSPFileArray_WithoutSignature;
                string OverAllSource_INF_PIC = File.ReadAllText(DspCodeFile).Replace("\r\n", "\n");
                if (OverAllSource_INF_PIC.Contains("REVPIC"))
                {
                    PIC_included_flag = true;
                    string[] SplitFile = OverAllSource_INF_PIC.Split('$'); //Split the Code Source by $ First Section for INF and the Second for PIC  
                    INFDSPFileArray = StrToByteArray(SplitFile[0]);
                    inf_app = SplitFile[0];
                    PIC_SW_VERSION = SplitFile[2];
                    PIC_SW = SplitFile[4];
                    INFDSPFileArray_WithoutSignature = new byte[INFDSPFileArray.Length - inf_app.IndexOf('6')];
                }
                else
                {


                    INFDSPFileArray = File.ReadAllBytes(DspCodeFile);
                    inf_app = File.ReadAllText(DspCodeFile);
                    INFDSPFileArray_WithoutSignature = new byte[INFDSPFileArray.Length - inf_app.IndexOf('6')];
                }






                Array.Copy(INFDSPFileArray, inf_app.IndexOf('6'), INFDSPFileArray_WithoutSignature, 0, INFDSPFileArray_WithoutSignature.Length);
                if (inf_app.Contains("#INF Successful Generated App#") || (short)deviceData.Parameters.DSP <= 0x0221) //from version INF DSP 2.33 this signature was added
                {
                    if ((short)deviceData.Parameters.Boot < 0x0202)
                    {
                        MessageBoxResult continue_download = MessageBox.Show(this, (string)this.FindResource("The current boot version is not compatible with this DSP version!") + "\n" + (string)this.FindResource("Please update boot version first.") + "\n" + (string)this.FindResource("Downloading this DSP version might cause the device to mulfunction.") + "\n" + (string)this.FindResource("Do you want to continue?"), (string)this.FindResource("INF app Downloading..."), MessageBoxButton.YesNoCancel, MessageBoxImage.Error);
                        if ((MessageBoxResult.No == continue_download) || (MessageBoxResult.Cancel == continue_download)) return;
                    }
                    MessageBoxResult result = MessageBox.Show(this, (string)this.FindResource("You are about to update INF app.") + "\n" + (string)this.FindResource("This will take several seconds, please do not turn off the system!"), (string)this.FindResource("INF app Downloading..."), MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);

                    if (MessageBoxResult.OK == result)
                    {

                        if ((short)deviceData.Parameters.DSP <= 0x0221) //without status request 
                        {
                            Mouse.OverrideCursor = true ? Cursors.Wait : null;
                            deviceData.DownloadDSP(INFDSPFileArray_WithoutSignature);
                            Delay_ms(5000);
                            Mouse.OverrideCursor = this.Cursor;
                            MessageBox.Show(this, (string)this.FindResource("Downloading new INF's software was finished successfully") + "\n" + (string)this.FindResource("Please, restart the system!!"), (string)this.FindResource("INF app Downloading..."), MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            INFDownLoadinProcess.prgWorking.Value = 0;
                            //  INFDownLoadinProcess.TIMERDOWNLOADINPROCESS = true;
                            INFDownLoadinProcess.INFDOWNLOADINGPROCESS = 1;
                            INFDownLoadinProcess.Show();
                            INFDownLoadinProcess.Focus();
                            INFDownLoadinProcess.M_BACKGROUNDWORKER.RunWorkerAsync();

                            testTimer.Start();
                            UpdateProgressBarTimer.Start();

                            Mouse.OverrideCursor = true ? Cursors.Wait : null;
                            deviceData.DownloadDSP(INFDSPFileArray_WithoutSignature);
                            Debug.WriteLine("A debug message.");
                            //Rasem change 8.11.2015 download bug
                            Delay_ms(5000);
                            
                        }

                    }
                }
                else //user preesed cancel
                {
                    MessageBox.Show((string)this.FindResource("The selected file is not an upgrading INF App file !") + "\n" + (string)this.FindResource("Please select another file and try again."), (string)FindResource("INF app  Downloading..."), MessageBoxButton.OK, MessageBoxImage.Hand);
                    return;
                }
            }
            else
            {
                MessageBox.Show((string)this.FindResource("No INF App file was selected!") + "\n" + (string)this.FindResource("Please select another file and try again."), (string)FindResource("INF app  Downloading..."), MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;

            }
        }
        
            
           

        
        private MCU_ProgrammingWindow MCUDownloadingProcessWindow = new MCU_ProgrammingWindow();
        private void OnDownloadMcuFile(object sender, RoutedEventArgs e)
        {
            MCUDownloadingProcessWindow.Title =(string)FindResource("MCU Downloading...");
            MCUDownloadingProcessWindow.lblWorking.Content = (string)FindResource("Starting LH Dowloading...");
            //Content="Starting LH Dowloading..."

            try
            {
                if (ISMCUFileWasSelected)
                {
                    MCUFileArray = File.ReadAllBytes(MCUCodeFile);
                    string mcu = File.ReadAllText((MCUCodeFile));
                    if (mcu.Contains("#MCU Successful Generated File#") && mcu.Contains("$A"))
                    {
                        MessageBoxResult result = MessageBox.Show(this, (string)FindResource("You are about to update the MCU SW version") + "\n" + (string)FindResource("This will take several seconds, please do not turn off the system!"), (string)FindResource("MCU app Downloading..."), MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);

                        if (MessageBoxResult.OK == result)
                        {
                            //start downloading - restart downloading parameters
                            Mouse.OverrideCursor = true ? Cursors.Wait : null;
                            Joint_Being_Downloaded = 0x330;
                            MCUDownloadingProcessWindow.MCUDOWNLOADINGINPROCESS = 1;//start bar filling
                            MCUDownloadingProcessWindow.VALUE = 0;
                            MCUDownloadingProcessWindow.lblWorking.Content =(string)FindResource("Starting LH Dowloading...");
                            deviceData.DownloadMCU(MCUFileArray);
                            //reset status
                            deviceData.TestData.ErrorCodeLH = 0; deviceData.TestData.ErrorCodeLK = 0; deviceData.TestData.ErrorCodeRH = 0; deviceData.TestData.ErrorCodeRK = 0;
                            btnMCUDownload.IsEnabled = false;
                            testTimer.Start();
                            MCUDownloadingProcessWindow.Show();
                            MCUDownloadingProcessWindow.Focus();
                            MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.RunWorkerAsync();
                        }
                        else //cancel button was pressed
                            return;

                    }
                    else

                    { 
                        MessageBox.Show((string)FindResource("The selected file is not an upgrading MCU file !") + "\n" + (string)FindResource("Please select another file and try again."),(string)FindResource("MCU app Downloading..."), MessageBoxButton.OK, MessageBoxImage.Hand); }
                    }
                else
                {
                    MessageBoxResult result = MessageBox.Show(this, (string)FindResource("No MCU file was selected!") + "\n" + (string)FindResource("Press the Browse button first and select an appropriate MCU file."),(string)FindResource("MCU app Downloading..."), MessageBoxButton.OKCancel, MessageBoxImage.Exclamation); 
                }

            }
            catch (Exception exception)
            {
                Logger.Error_MessageBox_Advanced("MCU file download failed:", "MCU Download Error", exception.Message);
                btnMCUDownload.IsEnabled = true;
                MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
                MCUDownloadingProcessWindow.Hide();
            }

        }
        private void OnDownloadBootFile(object sender, RoutedEventArgs e)
        {


            if (ISINFBOOTFileWasSelected)
            {
                try
                {
                    INFBootFileArray = File.ReadAllBytes(BootCodeFile);
                    string inf_boot = File.ReadAllText(BootCodeFile);
                    byte[] INFBootFileArray_WithoutSignature = new byte[INFBootFileArray.Length - inf_boot.IndexOf('6')];
                    Array.Copy(INFBootFileArray, inf_boot.IndexOf('6'), INFBootFileArray_WithoutSignature, 0, INFBootFileArray_WithoutSignature.Length);
                    if (inf_boot.Contains("#INF Successful Generated Boot#") || (short)deviceData.Parameters.Boot <= 0x0203)//Last version of boot 2.3
                    {
                        MessageBoxResult result = MessageBox.Show(this, (string)FindResource("You are about to update the INF Boot SW version") + "\n" + (string)FindResource("This will take several seconds, please do not turn off the system!"), (string)FindResource("INF Boot Downloading..."), MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
                        if (MessageBoxResult.OK == result)
                        {
                            if ((short)deviceData.Parameters.DSP <= 0x0221)
                            {
                                Mouse.OverrideCursor = true ? Cursors.Wait : null;
                                deviceData.DownloadRC(INFBootFileArray_WithoutSignature);//tbRcFileLocation.Text));
                                // Thread.Sleep(3000);
                                Delay_ms(3000); // replace Thread.Sleep
                                Mouse.OverrideCursor = this.Cursor;
                                MessageBox.Show(this, (string)FindResource("Downloading new INF's Boot  software was finished successfully") + "\n" + (string)FindResource("Please, Restart the system!!"), (string)FindResource("INF Boot Downloading..."), MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            else
                            {
                                Mouse.OverrideCursor = true ? Cursors.Wait : null;
                                deviceData.DownloadRC(INFBootFileArray_WithoutSignature);//tbRcFileLocation.Text));
                                // Thread.Sleep(3000);
                                Delay_ms(3000); // replace Thread.Sleep
                                //reset status
                                deviceData.TestData.ErrorCodeRC = 0;
                                deviceData.RequestTestStatus();
                                Mouse.OverrideCursor = this.Cursor;
                                if (deviceData.TestData.ErrorCodeRC == (short)0x7fff) //success download
                                    MessageBox.Show(this, (string)FindResource("Downloading new INF's Boot  software was finished successfully") + "\n" + (string)FindResource("Please, Restart the system!!"), (string)FindResource("INF Boot Downloading..."), MessageBoxButton.OK, MessageBoxImage.Information);
                                else
                                    MessageBox.Show((string)FindResource("INF boot downloading has failed!") + "\n" + (string)FindResource("Error Code:") + deviceData.TestData.ErrorCodeRC.ToString(), (string)FindResource("INF Boot Downloading..."), MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show((string)FindResource("The selected file is not an upgrading INF Boot file !") + "\n" + (string)FindResource("Please select another file and try again."), (string)FindResource("INF Boot Downloading..."), MessageBoxButton.OK, MessageBoxImage.Hand);
                        return;
                    }
                }

                catch (Exception exception)
                {
                    Logger.Error("Boot file download failed:", exception.Message);
                }

            }
            else
            {
                MessageBox.Show((string)this.FindResource("No INF Boot file was selected!") + "\n" + (string)this.FindResource("Please select another file and try again."), (string)FindResource("INF Boot Downloading..."), MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

        }
        string DspCodeFile, BootCodeFile, MCUCodeFile;
        private void OnBrowseDspFile(object sender, RoutedEventArgs e)
        {
            //string file = BrowseTextFile();
            string file = BrowseINFAppFile();
            DspCodeFile = file;
            if (((short)deviceData.Parameters.Boot < 0x0202) && ((short)deviceData.Parameters.Boot != 0))
            {
                MessageBoxResult continue_download = MessageBox.Show(this, (string)this.FindResource("The current INF boot version is not compatible with this INF app version!") + "\n" + (string)this.FindResource("Please update the boot version first.") + "\n" + (string)this.FindResource("Downloading this INF app version might cause the device to mulfunction.") + "\n" + (string)this.FindResource("Do you want to continue?"), (string)this.FindResource("INF app Downloading..."), MessageBoxButton.YesNoCancel, MessageBoxImage.Error);

                if ((MessageBoxResult.No == continue_download) || (MessageBoxResult.Cancel == continue_download))
                {
                    return;
                }
            }

            if (!string.IsNullOrEmpty(file))
            {
                if (file.Length > 33)
                {
                    tbDspFileLocation.Text = file.Remove(33);
                    tbDspFileLocation.Text = tbDspFileLocation.Text + "...";
                }
                else
                {
                    tbDspFileLocation.Text = file;
                }
                ISINFAPPFileWasSelected = true;
            }
            else
            {
                ISINFAPPFileWasSelected = false;
            }
        }
        private bool ISMCUFileWasSelected = false;
        private bool ISINFAPPFileWasSelected = false;
        private bool ISINFBOOTFileWasSelected = false;
        private void OnBrowseMcuFile(object sender, RoutedEventArgs e)
        {


            //string file = BrowseTextFile();
            string file = BrowseMCUFile();
            MCUCodeFile = file;
            if (!string.IsNullOrEmpty(file))
            {

                if (file.Length > 33)
                {
                    tbMcuFileLocation.Text = file.Remove(33);
                    tbMcuFileLocation.Text = tbMcuFileLocation.Text + "...";

                }
                else
                {
                    tbMcuFileLocation.Text = file;
                }

                ISMCUFileWasSelected = true;



            }
            else
            {
                ISMCUFileWasSelected = false;
            }
        }
        private void OnBrowseBootFile(object sender, RoutedEventArgs e)
        {
            
            string file = BrowseINFBootFile();
            BootCodeFile = file;
            if (!string.IsNullOrEmpty(file))
            {
                if (file.Length > 33)
                {
                    tbRcFileLocation.Text = file.Remove(33);
                    tbRcFileLocation.Text = tbRcFileLocation.Text + "...";
                }
                else
                {
                    tbRcFileLocation.Text = file;
                }
                ISINFBOOTFileWasSelected = true;
            }
           else
           {
            ISINFBOOTFileWasSelected = false;
           }
            
        }
        private string BrowseMCUFile()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.CheckFileExists = true;
            ofd.RestoreDirectory = true;
            ofd.Filter = "MCU Files (*.mcu)|*.mcu";
            if (ofd.ShowDialog().Value == true)
            {
                return ofd.FileName;
            }
            else
            {
                return null;
            }
        }
        private string BrowseINFAppFile()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.CheckFileExists = true;
            ofd.RestoreDirectory = true;
            ofd.Filter = "INF App Files (*.ainf)|*.ainf";
            if (ofd.ShowDialog().Value == true)
            {
               
                return ofd.FileName;
            }
            else
            {
               
                return null;
            }
        }
        private string BrowseINFBootFile()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.CheckFileExists = true;
            ofd.RestoreDirectory = true;
            ofd.Filter = "INF Boot Files (*.binf)|*.binf";
            if (ofd.ShowDialog().Value == true)
            {
                return ofd.FileName;
            }
            else
            {
                return null;
            }
        }

        private void OnSaveConfiguration(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "(*.xarg)|*.xarg|(*.*) |*.*";
            try
            {
                if (sfd.ShowDialog().Value == true)
                {
                    using (FileStream fs = new FileStream(sfd.FileName, FileMode.OpenOrCreate))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(DeviceParameters));
                        deviceData.Parameters.SampleTime = Short_GUIVersion;
                        serializer.Serialize(fs, deviceData.Parameters);
                        fs.Close();
                       // SetTitle(Path.GetFileNameWithoutExtension(sfd.FileName));
                        MessageBox.Show(this, (string)this.FindResource("Save configuration file finished"),(string)this.FindResource("Save As"),MessageBoxButton.OK,MessageBoxImage.Information);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Save configuration failed:",exception.Message);
            }
        }

   


        private void OnUpdateSettings(object sender, RoutedEventArgs e)
        {
            try
            {
               
                deviceData.WriteParameters();
            
                Write.IsChecked = false;

                Write4.Foreground = Brushes.Black;
                Write2.Foreground = Brushes.Black;
                Write3.Foreground = Brushes.Black;
                Delay_ms(1200); // replace Thread.Sleep
                MessageBox.Show((string)this.FindResource("Parameters were written successfully to the Rewalk"), (string)this.FindResource("Write"), MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            catch (DeviceOperationException exception)
            {
                Logger.Error("Device Operation failed:", exception.Message);
            }
        }
        //private void OnReadDeviceSettings(object sender, RoutedEventArgs e)
        //{
        //   ReadDeviceSettings();  
        //}

       
     
        private  bool ReadDeviceSettings(bool throwException) // parasoft-suppress  CS.MLC "Methods executing atomic functions"
        {
            try
            {
               
                deviceData.ReadData();
                //Open radion button in Walk and Stand screens to prevent buttons overlapping
                StandingUpFallDetection.IsChecked = true;
                FirstStepAssistance.IsChecked = true;
                //Accoording to parameters reading update texts and toggle buttons
                if (deviceData.Parameters.Enable_First_Step_Rewalk6 == true)
                {
                    FirstStepAssistance.IsChecked = true;
                    OFF1Sts.Visibility = Visibility.Collapsed;
                    ON1Sts.Visibility = Visibility.Visible;
                    F1sSBtns.Visibility = Visibility.Visible;
                }
                else if (deviceData.Parameters.Enable_First_Step_Rewalk6 == false)
                {
                    OFF1Sts.Visibility = Visibility.Visible;
                    ON1Sts.Visibility = Visibility.Collapsed;
                    F1sSBtns.Visibility = Visibility.Collapsed;
                }
                if (deviceData.Parameters.SafetyWhileStanding == true)
                {
                    StandingUpFallDetection.IsChecked = true;
                    OFF.Visibility = Visibility.Collapsed;
                    ON.Visibility = Visibility.Visible;
                    FDTBtns.Visibility = Visibility.Visible;
                }
                else if (deviceData.Parameters.SafetyWhileStanding == false)
                {
                    
                    OFF.Visibility = Visibility.Visible;
                    ON.Visibility = Visibility.Collapsed;
                    FDTBtns.Visibility = Visibility.Collapsed;
                }
              

                ReturnValueIndex();
                HipFlexion_SelectionChanged();
                InitSystemStrings();
                InitParamWithUnitsConv();                           
                if (GUIVersionType == "USA")
                {
                    //Enforce StairsEnable to be false in "INF"  -- Stairs is forbidden in USA
                    if (true == deviceData.Parameters.StairsEnabled)
                    {
                        deviceData.Parameters.StairsEnabled = false;
                        deviceData.WriteParameters();
                    }
                    else
                    {
                        deviceData.Parameters.StairsEnabled = false;
                    }
                }
				if (CurrentHandleAlogMethod == 0)
                { 
                    //0: Then old stop algo applied .
                    if (true == deviceData.Parameters.CurrentHandleAlgo)
                    {
                        deviceData.Parameters.CurrentHandleAlgo = false;
                        deviceData.WriteParameters();
                    }
                    else
                    {
                        deviceData.Parameters.CurrentHandleAlgo = false;
                    }
                
                }
                if (CurrentHandleAlogMethod == 1 )
                {
                    if (false == deviceData.Parameters.CurrentHandleAlgo)
                    {
                        deviceData.Parameters.CurrentHandleAlgo = true;
                        deviceData.WriteParameters();
                    }
                    else
                    {
                        deviceData.Parameters.CurrentHandleAlgo = true;
                    }
                }
                Write.IsEnabled = true;
                

                Check_DSP_MOTOR_AppVer();

                deviceData.Parameters.SampleTime = Short_GUIVersion;

                if ((short)deviceData.Parameters.DSP != 0x0909) // dsp application version 
                {
                    Par_set_true_inRead();
                    Check_DSP_IfBeyondVer_030A();
                   


                }

                else if ((short)deviceData.Parameters.DSP == 0x0909)//boot version ,0x0909 code of boot version
                {
                    Parm_set_false_inRead();
                     App tempAFT = App.Current as App;
                     if (tempAFT != null)
                     {
                         if (!(tempAFT).DeptName_RunINFtoApplication)
                         {
                             MessageBoxResult result = MessageBox.Show((string)this.FindResource("Rewalk is running in BOOT mode, limited operations are available!") + "\n" + (string)this.FindResource("Do you want to switch to operational mode?"), (string)this.FindResource("Warning_"), MessageBoxButton.YesNo, MessageBoxImage.Information);
                             if (MessageBoxResult.Yes == result)
                                 deviceData.Disconnect();
                         }
                         else
                         {
                             deviceData.Disconnect();
                             //RunINFtoApplication = false;

                         }
                     }
                }
                if ((short)deviceData.Parameters.Motor == 0x0909)//boot version
                {
                    DSP_boot_ver_txtblock.Visibility = System.Windows.Visibility.Visible;
                    INFBOOT_Text.Visibility = Visibility.Visible;
                    MessageBox.Show((string)this.FindResource("MCU is running in BOOT mode, Download new motors software or restart the device"),(string)this.FindResource( "Warning_"), MessageBoxButton.OK, MessageBoxImage.Error);
                }
               
               
                BatteryStatusTimer.IsEnabled = true;
                BatteryStatusTimer.Start();


                return true;
            }//end try



            catch (DeviceOperationException exception)
            {

                if (throwException)
                   Logger.Error("Device Operation failed:", exception.Message);
           
                return false;
            }
        }
    

        private  bool WarningMessageWasCalled;
        private void Check_DSP_IfBeyondVer_030A()
        {

            if ((short)deviceData.Parameters.DSP > 0x030A)
            {
                        short SysErrorStatus = deviceData.Parameters.SysErrorType;//deviceData.GetErrorStatus();
                        UInt32 CheckWarningErrorStatus = deviceData.Parameters.EnableSystemStuckOnError_int;
                        bool warningflag = (CheckWarningErrorStatus & (1 << SysErrorStatus)) != 0;
                        WarningMessageWasCalled = warningflag;
                        string ReportProcessWindow = (string)FindResource("System Error");
                        if (SysErrorStatus > 0 )//(-1 != SysErrorStatus)
                        {
                         //  DisableScreen.Visibility = Visibility.Visible;
                         
                           if (warningflag)
                           {
                            ReportProcessWindow = (string)FindResource("System Warning");//"System Error #";
                            progressform.btnClearError.Visibility = Visibility.Collapsed; //Hide button clear if it was warning
                           }
                            else
                            {
                             //   btnClearSysErr.IsEnabled = true;
                                btnFlashLogBrowse.IsEnabled = true;
                              //  tbSysErr.Text = SysErrorStatus.ToString();
                            }

                       
                            if (!LogFileSent)
                            {

                        progressform = (Report_Progress)Application.LoadComponent(new Uri("Report_Progress.xaml", UriKind.Relative));
                        progressform.labelErrorReport.Content = ReportProcessWindow + " #" +SysErrorStatus;
                     //   progressform.AppendProgress(ReportProcessWindow + " occured\n");
                     //   progressform.AppendProgress("Please contact technical support before using the system again.\n");
                        progressform.AppendProgress((string)(FindResource("Please contact technical support.")+"\n"));
                        string output = string.Format("{0,0} ", (string)FindResource("Saving log files")); progressform.AppendProgress(output);

                        progressform.Visibility = System.Windows.Visibility.Visible;
                        ShowPYTH.IsEnabled = false;
                        OpenUserfile.IsEnabled = false;
                        Load.IsEnabled = false;
                        TopToolBar.IsEnabled = false;
                        progressform.Show();
                        progressform.Focus();
                        App tempAFT = App.Current as App;
                        if (tempAFT != null)
                        {
                            (tempAFT).DeptName_BrowseSaveAsFlashLogFile = false;
                        }
                        if (Technician.IsSelected == true)
                        {
                            if (WarningMessageWasCalled == false) //Error Message Not Warning
                                progressform.btnClearError.Visibility = Visibility.Visible;
                        }
                       
                               //if(SysErrorStatus <= 30)
                                // SendEmailReport(ushort errtype, bool sendReport = false, bool reportProgress = true, ReportType reportTypeVal = ReportType.ERROR)
                                if (!warningflag)
                                {
                                    btnFlashLogBrowse.IsEnabled = true;
                                    SendEmailReport((ushort)SysErrorStatus);
                                  
                                }
                                else
                                {
                                   
                                    SendEmailReport((ushort)SysErrorStatus, false, true, ReportType.WARNING); LogFileSent = true;
                                }

                    }
                }
                else
                {
                           
                    btnFlashLogBrowse.IsEnabled = false;
                    //btnClearSysErr.IsEnabled = false; tbSysErr.Text = "None";
                 //   progressform.btnClearError.Visibility = Visibility.Collapsed;


                }
            }
        }
        private void Check_DSP_MOTOR_AppVer()
        {
            if (((short)deviceData.Parameters.Motor == 0x0909) && ((short)deviceData.Parameters.DSP != 0x0909)) // dsp application version 
            {
                    string temp = DSP_boot_ver_txtblock.Text;
                string[] temp_arr = new string[8];
                    temp_arr = temp.Split(' ');
                string version = temp_arr[temp_arr.Length - 1];
                    DSP_boot_ver_txtblock.Text = "MCU Boot: " + version;
            }
            else
            {
                    string temp = DSP_boot_ver_txtblock.Text;
                string[] temp_arr = new string[8];
                    temp_arr = temp.Split(' ');
                string version = temp_arr[temp_arr.Length - 1];
                //   DSP_boot_ver_txtblock.Text = "INF Boot: " + version;
            }
        }
        public void Par_set_true_inRead()
        {

            if (deviceData.Parameters.Gender == true)
            {
                M.IsChecked = true;
                F.IsChecked = false;
            }
            if (deviceData.Parameters.Gender == false)
            {
                F.IsChecked = true;
                M.IsChecked = false;
            }
            tbStepsCounter.Visibility = Visibility.Visible;
          //  tbStairCounter.Visibility = Visibility.Visible;
            btnBootBrowse.IsEnabled = true;
            btnBootDownload.IsEnabled = true;
            Write.IsEnabled = true;
            Write2.IsEnabled = true;
            Write3.IsEnabled = true;
            Write4.IsEnabled = true;
            Write4.Foreground = Brushes.Black;
            Write2.Foreground = Brushes.Black;
            Write3.Foreground = Brushes.Black;
            Load.IsEnabled = true;
            OpenUserfile.IsEnabled = true;
            TopToolBar.IsEnabled = true;
            ShowPYTH.IsEnabled = true;
            Restore.IsEnabled = true;
            Save.IsEnabled = true;
            Restore2.IsEnabled = true;
            btnBootBrowse.IsEnabled = true;
            btnBootDownload.IsEnabled = true;
            btnFlashLogBrowse.IsEnabled = false;
            btnFlashLogSend.IsEnabled = true;
            btnBrowseLog.IsEnabled = true;
            btnSaveSendLog.IsEnabled = true;
            btnClearLogFile.IsEnabled = true;
            SendReportFromMenu.IsEnabled = true;
            SaveReportFromMenu.IsEnabled = true;
            CustomLogHolder.Visibility = Visibility.Visible;
            CustomLogHolder.IsEnabled = true;
            cbSysType.IsEnabled = true;
            cbEnableStairs.IsEnabled = true;
            RcPairingGrid.IsEnabled = true;
            collectOnline.IsEnabled = true;
            btnStairsCalc.IsEnabled = true;
            btnBrowseLog.IsEnabled = true;
            btnSaveSendLog.IsEnabled = true;
            btnFlashLogBrowse.IsEnabled = false;
            btnFlashLogSend.IsEnabled = true;
            btnCom1.IsEnabled = true;
            btnCom2.IsEnabled = true;
            btnCom3.IsEnabled = true;
            btnCom4.IsEnabled = true;
            btnCom5.IsEnabled = true;
            //BtnRunEncoderTest.IsEnabled = true;
            btnDSPBrowse.IsEnabled = true;
            btnResetStepCounter.IsEnabled = true;
            btnResetStairCounter.IsEnabled = true;
            btnDSPDownload.IsEnabled = true;
            btnMCUBrowse.IsEnabled = true;
            btnMCUDownload.IsEnabled = true;
            SNGridName.IsEnabled = true;
            //MenuTechEnter.IsEnabled = true;
           // MenuProductionHome.IsEnabled = true; only in AFT
            SoftwareUpgradeGrid.IsEnabled = true;
            SelectMotorsGrid.IsEnabled = true;
            INFAPPVER.Visibility = System.Windows.Visibility.Visible;
            MCUAPPVER.Visibility = System.Windows.Visibility.Visible;
            RCAPPVER.Visibility = System.Windows.Visibility.Visible;
            INFBOOT_Text.Visibility = Visibility.Visible;
            DSP_App_ver_txtblock.Visibility = System.Windows.Visibility.Visible;
            MOTOR_ver_txtblock.Visibility = System.Windows.Visibility.Visible;
            RC_ver_txtblock.Visibility = System.Windows.Visibility.Visible;
            DSP_boot_ver_txtblock.Visibility = System.Windows.Visibility.Visible;
        
            App temp = App.Current as App;
            if (temp != null)
            {

                temp.DeptName_Connected = true;
                temp.DeptName_AFTCONWALKST = deviceData.Parameters.AFTWalkStatus;
                temp.DeptName_AFTSTOPSSTATUS = deviceData.Parameters.AFTStopsStatus;
                temp.DeptName_AFTCONWALKCOUNTER = (short)(deviceData.Parameters.StepCounter - PrevStepCounter);
                PrevStepCounter = deviceData.Parameters.StepCounter;
                temp.DeptName_AFTCONASCST = deviceData.Parameters.AFTASCStatus;
                temp.DeptName_AFTCONASCCOUNTER = deviceData.Parameters.AFTASCCounter;
                temp.DeptName_AFTCONDSCST = deviceData.Parameters.AFTDSCStatus;
                temp.DeptName_AFTCONDSCCOUNTER = deviceData.Parameters.AFTDSCCounter;
                temp.DeptName_AFTCONSITSTANDST = deviceData.Parameters.AFTSitStatus;
                temp.DeptName_AFTCONSITSTANDCOUNTER = deviceData.Parameters.AFTSitCounter;
            }

            //New GUI6
            SetFielsdsDisplayAsSysType();
        }
        UInt32 PrevStepCounter = 0;
        public void Parm_set_false_inRead()
        {
            tbStepsCounter.Visibility = Visibility.Visible;
           // tbStairCounter.Visibility = Visibility.Visible;
            Write.IsEnabled = false;
            Write2.IsEnabled = false;
            Write3.IsEnabled = false;
            Write4.IsEnabled = false;
            Write4.Foreground = Brushes.Black;
            Write2.Foreground = Brushes.Black;
            Write3.Foreground = Brushes.Black;
            Load.IsEnabled = false;
            OpenUserfile.IsEnabled = false;
            ShowPYTH.IsEnabled = false;
            Restore.IsEnabled = false;
            Save.IsEnabled = false;
            Restore2.IsEnabled = false;
            //MenuTechEnter.IsEnabled = true;
            //MenuProductionHome.IsEnabled = false;only in AFT
            btnStairsCalc.IsEnabled = false;
            BtnRunEncoderTest.IsEnabled = false;
            SNGridName.IsEnabled = false;
            INFAPPVER.Visibility = System.Windows.Visibility.Hidden;
            MCUAPPVER.Visibility = System.Windows.Visibility.Hidden;
            RCAPPVER.Visibility = System.Windows.Visibility.Hidden;
            INFBOOT_Text.Visibility = Visibility.Hidden;
            DSP_App_ver_txtblock.Visibility = System.Windows.Visibility.Hidden;
            MOTOR_ver_txtblock.Visibility = System.Windows.Visibility.Hidden;
            RC_ver_txtblock.Visibility = System.Windows.Visibility.Hidden;    
            DSP_boot_ver_txtblock.Visibility = System.Windows.Visibility.Visible;
            INFBOOT_Text.Visibility = Visibility.Visible;
            btnBootBrowse.IsEnabled = false;
            btnBootDownload.IsEnabled = false;
            btnFlashLogBrowse.IsEnabled = false;
            btnFlashLogSend.IsEnabled = false;
            btnBrowseLog.IsEnabled = false;
            btnSaveSendLog.IsEnabled = false;
            btnClearLogFile.IsEnabled = false;
            SendReportFromMenu.IsEnabled = false;
            SaveReportFromMenu.IsEnabled = false;
            CustomLogHolder.Visibility = Visibility.Collapsed;
            CustomLogHolder.IsEnabled = false;
            cbSysType.IsEnabled = false;
            cbEnableStairs.IsEnabled = false;
            RcPairingGrid.IsEnabled = false;
            SelectMotorsGrid.IsEnabled = true;
            SoftwareUpgradeGrid.IsEnabled = true;

            App temp = App.Current as App;
            if (temp != null)
                //(App.Current as App).DeptName_Connected = false;
                temp.DeptName_Connected = false;
        }
        private void OnRestoreDeviceSettings(object sender, RoutedEventArgs e)
        {


            try
            {
                string CurrentSystemTypeStringSelected = deviceData.Parameters.SysTypeString;
                short CurrentSystemTypeSelected = deviceData.Parameters.SysType;
                bool CurrentEnableDisableStairsSelected = deviceData.Parameters.StairsEnabled;
                deviceData.RestoreParameters();
                deviceData.Parameters.SysTypeString = CurrentSystemTypeStringSelected;
                deviceData.Parameters.SysType = CurrentSystemTypeSelected;
                deviceData.Parameters.StairsEnabled = CurrentEnableDisableStairsSelected;
                deviceData.Parameters.User_ID = 0;
                deviceData.Parameters.Height = 0;
                deviceData.Parameters.Weight = 0;
                deviceData.Parameters.Gender = false;
                deviceData.Parameters.SampleTime = Short_GUIVersion;
                ReturnValueIndex();
                HipFlexion_SelectionChanged();
                InitSystemStrings();
                InitParamWithUnitsConv();

                Write.IsChecked = true;
                Write4.Foreground = Brushes.Red;              
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;

                if (GUIVersionType == "USA")
                {
                    deviceData.Parameters.StairsEnabled = false;
                }
                if (CurrentHandleAlogMethod == 0)
                {
                    deviceData.Parameters.CurrentHandleAlgo = false;
                }
                else
                {
                    deviceData.Parameters.CurrentHandleAlgo = true;
                }
            }
            catch (DeviceOperationException exception)
            {
                Logger.Error("Device Operation failed:",exception.Message);
            }
        }
        private void OnEditRemoteAddress(object sender, RoutedEventArgs e)
        {
           // btnSetRCId.IsEnabled = true;
            MessageBoxResult Result;
            Result = MessageBox.Show((string)this.FindResource("Are you sure you want to change address/ID Manually?") + "\n" + (string)this.FindResource("It is recommended to use the generate button."), (string)this.FindResource("Address ID Alert"), MessageBoxButton.YesNoCancel, MessageBoxImage.Exclamation);
        
            if (Result != MessageBoxResult.Yes)
                return;
            tbRCId.IsEnabled = true;
            btnUpdateINF.IsEnabled = true;
            btnUpdateRC.IsEnabled = true;
            tbRCAddress.IsEnabled = true;

        }
        private void OnCalculateRemoteAddressID(object sender, RoutedEventArgs e)
        {


            GenerateRCAddressIDForm = (GenerateRCAddressID)Application.LoadComponent(new Uri("GenerateRCAddressID.xaml", UriKind.Relative)); 
            GenerateRCAddressIDForm.Visibility = System.Windows.Visibility.Visible;
            GenerateRCAddressIDForm.Init(SN_ComBox.SelectedIndex, deviceData.Parameters.Sn2.ToString(), deviceData.Parameters.Sn3.ToString());
            //GenerateRCAddressIDForm.Show();
            GenerateRCAddressIDForm.ShowDialog();

            if (!GenerateRCAddressIDForm.IsDone)
                return;
            btnUpdateINF.IsEnabled = true;
            btnUpdateRC.IsEnabled = true;
            deviceData.Parameters.RCAddress = GenerateRCAddressIDForm.Address;// (short)((deviceData.Parameters.Sn3 - deviceData.Parameters.Sn3 / 1000 * 1000) % 255);
            deviceData.Parameters.RCId = GenerateRCAddressIDForm.ID; //(short)(deviceData.Parameters.Sn3 + 10000 * deviceData.Parameters.SysType);

        }
        private GenerateRCAddressID GenerateRCAddressIDForm = new GenerateRCAddressID();
        private void OnUpdateINFAddressID(object sender, RoutedEventArgs e)
        {
            //btnSetRCId.IsEnabled = false;
            tbRCId.IsEnabled = false;
            btnUpdateINF.IsEnabled = false;
            tbRCAddress.IsEnabled = false;
            if ((deviceData.Parameters.RCAddress <= 0) || (deviceData.Parameters.RCId <= 0) || (deviceData.Parameters.RCAddress > 255) || (deviceData.Parameters.RCId > 65535))
            {
                MessageBox.Show((string)this.FindResource("RC address or ID out of range") + "\n" + (string)this.FindResource("Please input address from 1 to 255, ID from 1 to 32767."), (string)this.FindResource("INF Address and ID"), MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return;
            }

            try
            {

                deviceData.SetINFAddress();
                try
                {
                    deviceData.SetINFId();
                }
                catch (DeviceOperationException exception)
                {
                    Logger.Error("Device Operation failed:", exception.Message);

                }

            }
            catch (DeviceOperationException exception)
            {
                Logger.Error("Device Operation failed:", exception.Message);
            }
            //Thread.Sleep(1200);
            Delay_ms(1200); // replace Thread.Sleep
            MessageBox.Show((string)this.FindResource("INF address and ID were updated successfully!") + "\n" + (string)this.FindResource("Please reset the system (by turning it off and on)."), (string)this.FindResource("INF Address and ID"), MessageBoxButton.OK, MessageBoxImage.Exclamation);

        }
        private void OnUpdateRCAddressID(object sender, RoutedEventArgs e)
        {
            //btnSetRCId.IsEnabled = false;
            tbRCId.IsEnabled = false;
            btnUpdateRC.IsEnabled = false;
            tbRCAddress.IsEnabled = false;

            if ((deviceData.Parameters.RCAddress <= 0) || (deviceData.Parameters.RCId <= 0) || (deviceData.Parameters.RCAddress > 255) || (deviceData.Parameters.RCId > ushort.MaxValue))
            {
                MessageBox.Show(this, (string)this.FindResource("RC address or ID out of range") + "\n" + (string)this.FindResource("Please input address from 1 to 255, ID from 1 to 32767."));
                return;
            }
            try
            {

                MessageBoxResult result = MessageBox.Show(this, (string)this.FindResource("Do you want to change RC address and ID?"), (string)this.FindResource("INF Address and ID"), MessageBoxButton.OKCancel);



                if (MessageBoxResult.OK == result)
                {
                    MessageBox.Show((string)this.FindResource("Please turn on RC and select hand icon in technician mode"));
                    //   tbRCStartTest.Text = "Updating RC address and ID...";
                    RCUpdateTimer.Start();
                    deviceData.SetRCAddressID();
                }
                else
                {
                    btnUpdateINF.IsEnabled = false;
                    return;
                }

            }
            catch (DeviceOperationException exception)
            {
                Logger.Error("Device Operation failed:", exception.Message);
                //tbRCStartTest.Text = "";
                btnUpdateINF.IsEnabled = false;
            }

        }
        void OnUpdateRCAddressIDTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (deviceData.RequestRCAddressIDStatus())
                {
                    RCUpdateTimer.Stop();
                   // tbRCStartTest.Text = "";
                    string message = (string)this.FindResource("RC address and ID were updated successfully!") + "\n" + (string)this.FindResource("Please reset the system (by turning it off and on).");
                    MessageBoxResult result = MessageBox.Show(this, message);

                }
            }
            catch (DeviceOperationException exception)
            {
               // tbRCStartTest.Text = "";
                RCUpdateTimer.Stop();
                Logger.Error("Update RC address and ID Operation failed:", exception.Message);

            }
        }
        private void OnDisconenctUSB()
        {

         //   tbConnected.Visibility = Visibility.Collapsed;
        //    tbDisConnected.Visibility = Visibility.Visible;
            TechnicianScreenWasEntered = false;
            TechnicianPasswordBox.Password = SetPassBoxEmpty();
            progressform.Close();
            TopToolBar.IsEnabled = false;
           // Read.IsEnabled = false;
            Write.IsEnabled = false;
            Write4.IsEnabled = false;
            Write2.IsEnabled = false;
            Write3.IsEnabled = false;
            Write.IsChecked = false;
            Write4.Foreground = Brushes.Black;
            Write2.Foreground = Brushes.Black;
            Write3.Foreground = Brushes.Black;
            Load.IsEnabled = false;
            OpenUserfile.IsEnabled = false;
            Restore.IsEnabled = false;
            Save.IsEnabled = false;         
            Write2.IsEnabled = false;
            Load.IsEnabled = false;
            ShowPYTH.IsEnabled = false;
            Restore.IsEnabled = false;
            Save.IsEnabled = false;
            Restore2.IsEnabled = false;
         //   if(!ProductionVersionAFT)
         //    MenuTechEnter.Visibility = Visibility.Visible;
            TbRightMenu.IsChecked = false;
          //  MenuProductionHome.IsEnabled = false;
            btnStairsCalc.IsEnabled = false;
            BtnRunEncoderTest.IsEnabled = false;
            SNGridName.IsEnabled = false;
            btnStairsCalc.IsEnabled = false;
            btnBootBrowse.IsEnabled = false;
            btnBootDownload.IsEnabled = false;
            btnFlashLogBrowse.IsEnabled = false;
            btnFlashLogSend.IsEnabled = false;
            btnBrowseLog.IsEnabled = false;
            btnSaveSendLog.IsEnabled = false;
            btnClearLogFile.IsEnabled = false;
            SendReportFromMenu.IsEnabled = false;
            SaveReportFromMenu.IsEnabled = false;
            CustomLogHolder.Visibility = Visibility.Collapsed;
            CustomLogHolder.IsEnabled = false;
            cbSysType.IsEnabled = false;
            cbEnableStairs.IsEnabled = false;
            RcPairingGrid.IsEnabled = false;
            collectOnline.IsEnabled = false;
            btnStartCalib.IsEnabled = false;
            btnBrowseLog.IsEnabled = false;
            btnSaveSendLog.IsEnabled = false;
            SoftwareUpgradeGrid.IsEnabled = false;
            btnCom1.IsEnabled = false;
            btnCom2.IsEnabled = false;
            btnCom3.IsEnabled = false;
            btnCom4.IsEnabled = false;
            btnCom5.IsEnabled = false;
         
            btnBootBrowse.IsEnabled = false;
            btnBootDownload.IsEnabled = false;
            btnDSPBrowse.IsEnabled = false;
            btnDSPDownload.IsEnabled = false;
            btnResetStepCounter.IsEnabled = false;
            btnResetStairCounter.IsEnabled = false;
            btnMCUBrowse.IsEnabled = false;
            btnMCUDownload.IsEnabled = false;
         
            LogFileSent = false;
            USBConnectState = false;
 
            USBConnectDelay = 0;
            USBDisConnectDelay = 0;
            App temp = App.Current as App;
            if (temp != null)
                //(App.Current as App).DeptName_Connected = false;
                temp.DeptName_Connected = false;

            BatteryFull.Visibility = Visibility.Collapsed;
            BatteryEmpty.Visibility = Visibility.Collapsed;
            BatteryHalf.Visibility = Visibility.Collapsed;
            BatteryThird.Visibility = Visibility.Collapsed;
            if (!ProductionVersionAFT)
            {
                Physiotherapist.IsSelected = true;
                TopToolBar.Visibility = Visibility.Visible;
            }
            TopToolBarButtons.Visibility = Visibility.Collapsed;
            UD.Visibility = Visibility.Collapsed;
            EditBtn.IsChecked = false;
            EditBtn.Visibility = Visibility.Collapsed;
            tbStepsCounter.Visibility = Visibility.Collapsed;
          //  tbStairCounter.Visibility = Visibility.Collapsed;
            deviceData.Parameters.Height = 0;
            deviceData.Parameters.Weight = 0;
            deviceData.Parameters.User_ID = 0;
           // DisableScreen.Visibility = Visibility.Visible;
            deviceData.Parameters.StairCounter = 0;
            deviceData.Parameters.StepCounter = 0;
            deviceData.Parameters.SysTypeString = "";
            SelectMotorsGrid.IsEnabled = false;
            Sn2_TextBox.Text = "";
            Sn3_TextBox.Text = "";
            InfSN_TextBox.Text = "";
            LHSN_TextBox.Text = "";
            LKSN_TextBox.Text = "";
            RHSN_TextBox.Text = "";
            RKSN_TextBox.Text = "";
            tbRCId.Text = "";
            tbRCAddress.Text = "";
            RCSN_TextBox.Text = "";
            SelectAllMotors.IsChecked = false;
            cbCalibLH.IsChecked = false;
            cbCalibLK.IsChecked = false;
            cbCalibRH.IsChecked = false;
            cbCalibRK.IsChecked = false;
            tbRCErrorRate.Text = "--";
            tbINFErrorRate.Text = "--";
          
            //All log setting unchecked
            cbCANLogLevelData.IsChecked = false;
            cbCANLogLevelWarning.IsChecked = false;
            cbCANLogLevelError.IsChecked = false;
            cbMCULogLevelData.IsChecked = false;
            cbMCULogLevelWarning.IsChecked = false;
            cbMCULogLevelError.IsChecked = false;
            cbRFLogLevelData.IsChecked = false;
            cbRFLogLevelWarning.IsChecked = false;
            cbRFLogLevelError.IsChecked = false;
            cbRCLogLevelData.IsChecked = false;
            cbRCLogLevelWarning.IsChecked = false;
            cbRCLogLevelError.IsChecked = false;
            cbADCLogLevelData.IsChecked = false;
            cbADCLogLevelWarning.IsChecked = false;
            cbADCLogLevelError.IsChecked = false;
            cbSTAIRSLogLevelData.IsChecked = false;
            cbSTAIRSLogLevelWarning.IsChecked = false;
            cbSTAIRSLogLevelError.IsChecked = false;
            cbSITLogLevelData.IsChecked = false;
            cbSITLogLevelWarning.IsChecked = false;
            cbSITLogLevelError.IsChecked = false;
            cbWALKLogLevelData.IsChecked = false;
            cbWALKLogLevelWarning.IsChecked = false;
            cbWALKLogLevelError.IsChecked = false;
            cbUSBLogLevelData.IsChecked = false;
            cbUSBLogLevelWarning.IsChecked = false;
            cbUSBLogLevelError.IsChecked = false;
            cbBITLogLevelData.IsChecked = false;
            cbBITLogLevelWarning.IsChecked = false;
            cbBITLogLevelError.IsChecked = false;
            cbMEMLogLevelData.IsChecked = false;
            cbMEMLogLevelWarning.IsChecked = false;
            cbMEMLogLevelError.IsChecked = false;
            cbOPSLogLevelData.IsChecked = false;
            cbOPSLogLevelWarning.IsChecked = false;
            cbOPSLogLevelError.IsChecked = false;

            btnCom1_LH.ZeroSendBox = "0";
            btnCom1_LH.ClearRecvBox = "";
            btnCom2_LK.ZeroSendBox = "0";
            btnCom2_LK.ClearRecvBox = "";
            btnCom3_RH.ZeroSendBox = "0";
            btnCom3_RH.ClearRecvBox = "";
            btnCom4_RK.ZeroSendBox = "0";
            btnCom4_RK.ClearRecvBox = "";
            btnCom5.ZeroSendBox = "0";
            btnCom5.ClearRecvBox = "";
            INFAPPVER.Visibility = System.Windows.Visibility.Hidden;
            MCUAPPVER.Visibility = System.Windows.Visibility.Hidden;
            RCAPPVER.Visibility =  System.Windows.Visibility.Hidden;
            INFBOOT_Text.Visibility = Visibility.Hidden;
            DSP_App_ver_txtblock.Visibility = System.Windows.Visibility.Hidden;
            MOTOR_ver_txtblock.Visibility = System.Windows.Visibility.Hidden;
            RC_ver_txtblock.Visibility = System.Windows.Visibility.Hidden;
            DSP_boot_ver_txtblock.Visibility = System.Windows.Visibility.Hidden;
        }
        void OnCheckUSBConnectedTimeTimer_Tick(object sender, EventArgs e) // parasoft-suppress  OOM.CYCLO "Coding style - method logic was verified"
        {

            App tempAFT = App.Current as App;
            if (tempAFT != null)
            {

                if ((tempAFT).DeptName_Connected == false && (tempAFT).DeptName_RunINFtoApplication == true)
                {
                    ReadDeviceSettings(false);
                    Delay_ms(1000);
                    return;
                }
            }

            try
            {
                USBConnected = deviceData.CheckUSBConnected();
                if (!USBConnected)
                {
                    USBDisConnectDelay++;
                }
            }
            catch (DeviceOperationException exception)
            {
                USBConnected = false;
                Logger.Dummy_Error("", exception.Message);
            }
            if (true == USBConnectState)
            {
                if (USBDisConnectDelay >= 2)
                    OnDisconenctUSB();
            }
            else
            {
                if (USBConnected)
                {
                    USBConnectDelay++;
                    if (USBConnectDelay >= 5)
                    {
                        bool EnableReadOnOpen = false;
                        XmlDocument doc = new XmlDocument();
                        doc.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");
                        try
                        {
                            XmlNodeList ReadOnOpenNode = doc.SelectNodes("//ReadOnOpen");
                            if (ReadOnOpenNode.Count == 1)
                                foreach (XmlNode value in ReadOnOpenNode)
                                { EnableReadOnOpen = value.Attributes["Values"].Value == "true" || value.Attributes["Values"].Value == "True"; }
                        }
                        catch (Exception exception) { Logger.Dummy_Error("", exception.Message); }
                        if (EnableReadOnOpen)
                            ReadDeviceSettings(false);
                        LogGUIConnections();

                   //     tbConnected.Visibility = Visibility.Visible;
                   //     tbDisConnected.Visibility = Visibility.Collapsed;
                        USBDisConnectDelay = 0;
                        USBConnectDelay = 0;
                        ControlsEnabled();

                    }
                }
            }
        }
        private void ControlsEnabled()
        {
        //    Read.IsEnabled = true;
         //   Load.IsEnabled = true;
         //   Restore.IsEnabled = true;
          //  Save.IsEnabled = true;
         //   Load_open.IsEnabled = true;
          //  CreateBtn.IsEnabled = true;
          //  NewBtn.IsEnabled = true;
         //   btnStairsCalc.IsEnabled = true;
         //   btnClearSysErr.IsEnabled = true;
          //  TechnicianRadioBtn.IsEnabled = true;
          //  ProductionRadioBtn.IsEnabled = true;
            collectOnline.IsEnabled = true;
            //  gbCustomCommands.IsEnabled = true;
            btnCom1.IsEnabled = true;
            btnCom2.IsEnabled = true;
            btnCom3.IsEnabled = true;
            btnCom4.IsEnabled = true;
            btnCom5.IsEnabled = true;
          
            btnDSPBrowse.IsEnabled = true;
            btnDSPDownload.IsEnabled = true;
            btnMCUBrowse.IsEnabled = true;
            btnMCUDownload.IsEnabled = true;
            USBConnectState = true;
           
            
        }
        private void OnStartRFTest(object sender, RoutedEventArgs e)
        {
            btnStartRCTest.IsChecked = true;//remain Run text on button
            if (!RFTestStarted)
            {
                try
                {
                    btnStartRCTest.IsChecked = true;
                    tbRCErrorRate.Text = "--";
                    tbINFErrorRate.Text = "--";

                    //btnStartRFTest.IsEnabled = false;
                    RFTestStarted = true;
                 //   btnStartRCTest.Content = "Stop";// (string)FindResource("Stop");
                    //  tbRCStartTest.Text = "Please Turn on RC !";
                    MessageBoxResult result = MessageBox.Show(this, (string)this.FindResource("Please Turn on RC !"), (string)this.FindResource("RC Test"));

                    deviceData.StartRFTest();
                    RFtestTimer.Start();
                    deviceData.ResetRFTest();
                    //tbRCStartTest.Visibility = "Visible";

                }
                catch (DeviceOperationException exception)
                {
                    // tbRCStartTest.Text = "";
                    btnStartRCTest.Content = "Start RC Test";
                    Logger.Error("Start RF test Operation failed:", exception.Message);
                }
            }
            else
            {
                RFtestTimer.Stop();
                btnStartRCTest.Content = "Start RC Test";
                // tbRCStartTest.Text = "";
                RFTestStarted = false;
                deviceData.ResetRFTest();
            }
        }
        void OnRFTestTimer_Tick(object sender, EventArgs e)
        {
            
            try
            {
                if (deviceData.RequestRFTestStatus())
                {
                    btnStartRCTest.IsChecked = true;
                    short RCerrRate = deviceData.GetRCErrRate();
                    short INFerrRate = deviceData.GetINFErrRate();
                    RFtestTimer.Stop();
                    btnStartRCTest.Content = "Start RC Test";
                    RFTestStarted = false;
                    //tbRCStartTest.Text = "";
                    string message = (string)this.FindResource("RC Test Finished !") + "\n" + "\n" + (string)this.FindResource("Results:") + "\n" + (string)this.FindResource( "RC error rate:") + RCerrRate.ToString() + "%" + "\n" + (string)this.FindResource("INF error rate:") + INFerrRate.ToString() + "%";
                    MessageBoxResult result = MessageBox.Show(this, message, "RC Test Results");
                    deviceData.ResetRFTest();
                    tbRCErrorRate.Text = RCerrRate.ToString() + "%";
                    tbINFErrorRate.Text = INFerrRate.ToString() + "%";

                }
            }
            catch (DeviceOperationException exception)
            {
                Logger.Error("Get RF Test Status Operation failed:", exception.Message);
                RFtestTimer.Stop();

           }
        }
        void OnCollectDataTimer(object sender, EventArgs e)
        {
            try
            {
                deviceData.ReadOnlineData();
                // tbOnlineError.Visibility = Visibility.Hidden;
            }
            catch (DeviceOperationException exception)
            {
                // tbOnlineError.Text = "Read online data failed: ";// +exception.Message;
                // tbOnlineError.Visibility = Visibility.Visible;
                Logger.Dummy_Error("", exception.Message);
            }
        }
        private void OnRunCustomCommand(FrameworkElement sender, byte[] data)
        {
            // int command = int.Parse(sender.Tag.ToString());
            int command;
            bool cc = int.TryParse(sender.Tag.ToString(), out  command);
            if (command == 5 && data[0] == 0x99 && data[1] == 0x99 && data[2] == 0x99)
            {
                //Report_Progress progressform = new Report_Progress();
                object s = Application.LoadComponent(new Uri("Report_Progress.xaml", UriKind.Relative));

                progressform = (Report_Progress)s; progressform.Visibility = System.Windows.Visibility.Visible; progressform.Show();
                progressform.rtfUserReport.AppendText("System error occured\n"); progressform.rtfUserReport.AppendText((string)FindResource("Saving log files"));

                SendEmailReport(00);
            }
          

            try
            {
                if (command == 5 && data[0] == (byte)0x39) //BR window 
                {
                    data[0] = 0x30;
                    deviceData.RunCustomCommand(1, data);
                    byte[] result_LH = deviceData.RunCustomCommand(1, data); 

                    if(result_LH[0]== 0x30)
                      btnCom1_LH.SetResult(result_LH);


                    data[0] = 0x31;
                    deviceData.RunCustomCommand(2, data); 
                    byte[] result_LK = deviceData.RunCustomCommand(2, data); 
                    if (result_LK[0] == 0x31)
                        btnCom2_LK.SetResult(result_LK);


                    data[0] = 0x32;
                    deviceData.RunCustomCommand(3, data); 
                    byte[] result_RH = deviceData.RunCustomCommand(3, data); 
                    if(result_RH[0]== 0x32)
                        btnCom3_RH.SetResult(result_RH);


                    data[0] = 0x33;
                    deviceData.RunCustomCommand(4, data);
                    byte[] result_RK = deviceData.RunCustomCommand(4, data); 
                    if(result_RK[0]== 0x33)
                        btnCom4_RK.SetResult(result_RK);


                }
                else
                {
                    deviceData.RunCustomCommand(command, data);
                    byte[] result = deviceData.RunCustomCommand(command, data);
                    // (sender as CommandCtrl).SetResult(result);
                    CommandCtrl aa = sender as CommandCtrl;
                    if (aa != null) aa.SetResult(result);

                }
            }
            catch (DeviceOperationException exception)
            {
                Logger.Error("Custom Command Operation failed:", exception.Message);
                //   deviceData.Disconnect();
                //      OnDisconenctUSB();
                //    CheckUSBConnectedTime.IsEnabled = false;
                //    Restore.IsEnabled = true;
            }

        }
     //    private bool RunINFtoApplication = false;
      
        public void RunSystem()
        {
            deviceData.Disconnect();
            OnDisconenctUSB();
            //RunINFtoApplication = true;
        }
        private int Joint_Being_Downloaded = 0x330;
        private int infdownloadcounter = 0;
        private int PICSWVERSION = 0;

        void UpdateProgressBarTimer_Tick(object sender, EventArgs e)
        {
            if ((int)INFDownLoadinProcess.prgWorking.Value == 30000)
            {
                INFDownLoadinProcess.prgWorking.Value = 0;
            }
            INFDownLoadinProcess.prgWorking.Value += 350;
  
        }
        void OnTestTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (MCUDownloadingProcessWindow.MCUDOWNLOADINGINPROCESS == 1)
                {
                testTimer.Stop();

                deviceData.RequestTestStatus();

                switch (Joint_Being_Downloaded)
                {
                    case 0x330:

                        LH_Download();

                        break;
                    case 0x331:

                        LK_Download();

                        break;
                    case 0x332:

                        RH_Download();

                        break;


                    case 0x333:

                        RK_Download();

                        break;

                    default:
                        break;


                }




            }
                if (INFDownLoadinProcess.INFDOWNLOADINGPROCESS == 1)
                {
                    infdownloadcounter += 1;
                    Debug.WriteLine("A debug message." + (infdownloadcounter * 10).ToString()); 
                    INFDownLoadinProcess.lblWorking.Content = "";
                    INFDownLoadinProcess.lblWorking.Content = "infdownloadcounter = " + (infdownloadcounter * 10).ToString() + " sec" + "\n";


                    deviceData.RequestTestStatus();
                    if (PIC_included_flag)
                    {
                        bool cc = Int32.TryParse(PIC_SW_VERSION, System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out PICSWVERSION);
                                             
                    }
                    if (infdownloadcounter == 1)
                    {
                        INFDownLoadinProcess.lblWorking.Content = "infdownloadcounter = " + (infdownloadcounter * 10).ToString() + " sec" + "\n";

                            Mouse.OverrideCursor = this.Cursor;
                            deviceData.TestData.ErrorCodeINF = 0;
                            Debug.WriteLine("Enter Request status 1");
                            deviceData.RequestTestStatus();
                            Debug.WriteLine("Exit Request status 1");
                            if (deviceData.TestData.ErrorCodeINF == (short)0x7fff) //success download  
                            {
                                if ((deviceData.Parameters.Device == 3) || !PIC_included_flag)//old INF 
                                {
                                    PIC_included_flag = false;
                                   // Mouse.OverrideCursor = this.Cursor;
                                    INFDownLoadinProcess.INFDOWNLOADINGPROCESS = 0;
                                    INFDownLoadinProcess.prgWorking.Value = 30000;
                                    INFDownLoadinProcess.Hide();
                                    testTimer.Stop();
                                    //MessageBox.Show(this, "Downloading new INF's software was finished successfully\nPlease, Restart the system!!\n", "INF app Downloading...", MessageBoxButton.OK, MessageBoxImage.Information);
                                }
                                else
                                {
                                    downloadstatus = "PASSED";
                                }
                            }
                            else
                            {
                                MessageBox.Show((string)this.FindResource("INF application downloading has failed")+"\n"+(string)this.FindResource("Error Code:") + deviceData.TestData.ErrorCodeINF.ToString(), (string)this.FindResource("INF app Downloading..."), MessageBoxButton.OK, MessageBoxImage.Error);
                               
                                downloadstatus = "FAILED";
                            }



                       

                            if (downloadstatus == "PASSED" && (PICSWVERSION != (int)deviceData.Parameters.PicSWVersion) ) //if INF passed download then start PIC Download
                            {
                                Debug.WriteLine("Enter PIC download ");
                                testTimer.Stop();
                                testTimer.IsEnabled = true;
                                Mouse.OverrideCursor = true ? Cursors.Wait : null;
                                byte[] PICSWDownload = StrToByteArray(PIC_SW);
                                deviceData.DownloadDSP(PICSWDownload);
                                Debug.WriteLine("Exit PIC download ");

                            }

                            else
                            {

                                UpdateProgressBarTimer.Stop();
                                testTimer.Stop();
                                infdownloadcounter = 0;
                                INFDownLoadinProcess.Hide();
                                Mouse.OverrideCursor = this.Cursor;
                                INFDownLoadinProcess.INFDOWNLOADINGPROCESS = 0;
                                INFDownLoadinProcess.M_BACKGROUNDWORKER.CancelAsync();
                                MessageBox.Show(this, (string)this.FindResource("Downloading new INF's software was finished successfully") + "\n" + (string)this.FindResource("Please, restart the system!!"), (string)this.FindResource("INF app Downloading..."), MessageBoxButton.OK, MessageBoxImage.Information);
                            
                            }
                    
                    
                    }
               /*     if (infdownloadcounter == 2 && PIC_included_flag && (PICSWVERSION == (int)deviceData.Parameters.PicSWVersion))
                    {
                        UpdateProgressBarTimer.Stop();
                         testTimer.Stop();
                         infdownloadcounter = 0;
                         INFDownLoadinProcess.Hide();
                         Mouse.OverrideCursor = this.Cursor;
                         MessageBox.Show(this, "Downloading new INF's software was finished successfully\nPlease, Restart the system!!\n", "INF app Downloading...", MessageBoxButton.OK, MessageBoxImage.Information);
                    }*/
                    if (infdownloadcounter == 9) //10 second wait for 90 min 
                    {
                        UpdateProgressBarTimer.Stop();
                        infdownloadcounter = 0;
                        testTimer.Stop();
                        testTimer.IsEnabled = false;
                        Mouse.OverrideCursor = this.Cursor;
                        INFDownLoadinProcess.INFDOWNLOADINGPROCESS = 0;
                        INFDownLoadinProcess.M_BACKGROUNDWORKER.CancelAsync();
                        INFDownLoadinProcess.Hide();
                        deviceData.TestData.ErrorCodeINF = 0;
                        Debug.WriteLine("Enter Request status 2");
                        deviceData.RequestTestStatus();
                        Debug.WriteLine("Exit Request status 2");
                        if (deviceData.TestData.ErrorCodeINF == (short)0x7fff) //success download  
                        {
                            MessageBox.Show(this, (string)this.FindResource("Downloading new INF's software was finished successfully") + "\n" + (string)this.FindResource("Please, restart the system!!"), (string)this.FindResource("INF app Downloading..."), MessageBoxButton.OK, MessageBoxImage.Information);
                        
                        }
                    }

                }
                
            }
            catch (DeviceOperationException exception)
            {
                Logger.Error("Get Test Status Operation failed:", exception.Message);
                MessageBox.Show("Reason: " + exception.Message + " " + infdownloadcounter.ToString());
                btnMCUDownload.IsEnabled = true;
                MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
                MCUDownloadingProcessWindow.Hide();
                UpdateProgressBarTimer.Stop();
                infdownloadcounter = 0;
                testTimer.Stop();
                testTimer.IsEnabled = false;
                Mouse.OverrideCursor = this.Cursor;
                INFDownLoadinProcess.INFDOWNLOADINGPROCESS = 0;
                INFDownLoadinProcess.M_BACKGROUNDWORKER.CancelAsync();
                INFDownLoadinProcess.Hide();
            }
        }
        private void OnStartCalibration(object sender, RoutedEventArgs e)
        {
            try
            {
                CalibrationInProcess = true;

                Storyboard CalibrationStatus = (Storyboard)FindResource("LoadingOrange");
               // CalibrationStatus.RepeatBehavior = RepeatBehavior.Forever;
                CalibrationStatus.Begin();
                

             
                deviceData.StartCalibration();
                btnStartCalib.IsEnabled = false;
                calibTimer.IsEnabled = true;
                // btnDoCalib.IsEnabled = true;
                // cbCalibLH.IsEnabled = cbCalibLK.IsEnabled = cbCalibRH.IsEnabled = cbCalibRK.IsEnabled = true;
                calibTimer.Start(); 
            }
            catch (DeviceOperationException exception)
            {
                CalibrationInProcess = false;
                Logger.Error("Start Calibration Operation failed:", exception.Message);
            }
        }
        private int cal_timeout_cnt = 0;
        void OnCalibrationTimer_Tick(object sender, EventArgs e) // parasoft-suppress  OOM.CYCLO "Coding style - method logic was verified"
        {
            Storyboard CalibrationStatus = (Storyboard)FindResource("LoadingOrange");
            cal_timeout_cnt++;
            TestStatus CalibStatus;
            try
            {
                CalibStatus = deviceData.RequestCalibrationStatus((bool)cbCalibLH.IsChecked, (bool)cbCalibLK.IsChecked, (bool)cbCalibRH.IsChecked, (bool)cbCalibRK.IsChecked);
                //if  ==     deviceData.RequestCalibrationStatus( )
                if (TestStatus.Success == CalibStatus)
                {
                    calibTimer.Stop();
                    calibTimer.IsEnabled = false;

                    if (cbCalibLH.IsChecked == true)
                    { cbCalibLH.IsEnabled = true; cbCalibLH.IsChecked = false; }


                    if (cbCalibRH.IsChecked == true)
                    { cbCalibRH.IsEnabled = true; cbCalibRH.IsChecked = false; }

                    if (cbCalibLK.IsChecked == true)
                    { cbCalibLK.IsEnabled = true; cbCalibLK.IsChecked = false; }


                    if (cbCalibRK.IsChecked == true)
                    { cbCalibRK.IsEnabled = true; cbCalibRK.IsChecked = false; }

                    Delay_ms(2000);
                    CalibrationStatus.Stop();
                   
                    MessageBox.Show(this, (string)this.FindResource("Calibration Finished.") + "\n" + (string)this.FindResource("Please restart the system."), (string)this.FindResource("Calibration"), MessageBoxButton.OK, MessageBoxImage.Information);
                    CalibrationInProcess = false;

                }
                else if (TestStatus.Fail == CalibStatus)
                {
                    CalibrationStatus.Stop();
                    calibTimer.Stop();
                    calibTimer.IsEnabled = false;
                    MessageBox.Show(this, (string)this.FindResource("Calibration Failed!") + "\n" + (string)this.FindResource("Please restart and recalibrate the system."), (string)FindResource("Calibration"), MessageBoxButton.OK, MessageBoxImage.Error);
                    CalibrationInProcess = false;
                }


            }
            catch (DeviceOperationException exception)
            {
                CalibrationStatus.Stop();
                Logger.Error("Get Calibration Status Operation failed:", exception.Message);
                calibTimer.Stop();
                btnStartCalib.IsEnabled = true;
                cbCalibLH.IsEnabled = cbCalibLK.IsEnabled = cbCalibRH.IsEnabled = cbCalibRK.IsEnabled = false;
            }
        }
        //Battery Status Indication 

        void BatteryStatusTimer_Tick(object sender, EventArgs e)
        {
            //  short batterylevel =  deviceData.Parameters.INFBatteriesLevel;

            short batterylevel = System.Convert.ToInt16(BlueView.ReceiverViewModel.BATTERY_LEVEL);

           
           /*  App tempAFT = App.Current as App;
             if (tempAFT != null)
             {
                 if ((tempAFT).DeptName_BrowseSaveAsFlashLogFile == true)
                     btnFlashLogBrowse.IsEnabled = false;

                 if (USBConnected)
                 { */
                     switch (batterylevel)
                     {
                         case 0x07: //Full Battery Green 

                             BatteryHalf.Visibility = Visibility.Collapsed;
                             BatteryThird.Visibility = Visibility.Collapsed;
                             BatteryEmpty.Visibility = Visibility.Collapsed;
                             BatteryFull.Visibility = Visibility.Visible;

                             break;
                         case 0x08: //Very Low battery blinking red 

                             BatteryHalf.Visibility = Visibility.Collapsed;
                             BatteryThird.Visibility = Visibility.Collapsed;
                             BatteryFull.Visibility = Visibility.Collapsed;
                             if (BatteryEmpty.Visibility == Visibility.Visible)
                             {

                                 BatteryEmpty.Visibility = Visibility.Collapsed;
                             }
                             else
                             {
                                 BatteryEmpty.Visibility = Visibility.Visible;
                             }

                             break;
                         case 0x9: //Low battery
                             BatteryFull.Visibility = Visibility.Collapsed;
                             BatteryEmpty.Visibility = Visibility.Visible;
                             BatteryThird.Visibility = Visibility.Collapsed;
                             BatteryHalf.Visibility = Visibility.Collapsed;

                             break;
                         case 0x0A://Med battery
                             BatteryFull.Visibility = Visibility.Collapsed;
                             BatteryEmpty.Visibility = Visibility.Collapsed;
                             BatteryThird.Visibility = Visibility.Collapsed;
                             BatteryHalf.Visibility = Visibility.Visible;

                             break;
                         default:
                             break;
                     }
            //     }
            // }
        }

        //Encoder Test Check Status ; 
        bool EncoderTestInProcess;
        int JointCounter;
        bool LHJointUnderTest;
        bool RHJointUnderTest;
        bool LKJointUnderTest;
        bool RKJointUnderTest;
        void OnTestEncoderTimer_Tick(object sender, EventArgs e) // parasoft-suppress  OOM.CYCLO "Coding style - method logic was verified"
        {
            try
            {
                byte[] result = { }; int command = 1;

                if (cbCalibLH.IsChecked == true && EncoderTestInProcess)//
                {
                    byte[] DataArray2 = { 0x30, 0x3, 0x30, 0, 0, 1, 0, 2, 0, 0 }; result = deviceData.RunCustomCommand(command, DataArray2); EncoderTestInProcess = false; LHJointUnderTest = true;
                }

                if (cbCalibLK.IsChecked == true && EncoderTestInProcess) // 
                {
                    byte[] DataArray2 = { 0x31, 0x3, 0x30, 0, 0, 1, 0, 2, 0, 0 }; result = deviceData.RunCustomCommand(command, DataArray2); EncoderTestInProcess = false; LKJointUnderTest = true;
                }

                if (cbCalibRH.IsChecked == true && EncoderTestInProcess)//
                {
                    byte[] DataArray3 = { 0x32, 0x3, 0x30, 0, 0, 1, 0, 2, 0, 0 }; result = deviceData.RunCustomCommand(command, DataArray3); EncoderTestInProcess = false; RHJointUnderTest = true;
                }

                if (cbCalibRK.IsChecked == true && EncoderTestInProcess)//
                {
                    byte[] DataArray4 = { 0x33, 0x3, 0x30, 0, 0, 1, 0, 2, 0, 0 }; result = deviceData.RunCustomCommand(command, DataArray4); EncoderTestInProcess = false; RKJointUnderTest = true;
                }


                //check status 
                if (deviceData.RequestTestEncoderStatus())
                {
                    HipsCheck();
                    KneesCheck();
                }

                IfJointCounterEqualZero();
            }


            catch (DeviceOperationException exception)
            {
                Logger.Error("Encoder Test Status Operation failed:", exception.Message);
                BtnRunEncoderTest.IsEnabled = true; EncoderTestInProcess = false; TestEncoderTimer.Stop(); cbCalibLH.IsEnabled = cbCalibLK.IsEnabled = cbCalibRH.IsEnabled = cbCalibRK.IsEnabled = false;
                EncoderTestInProcessForALL = false;
            }

        }
        public void HipsCheck()
        {
            if (cbCalibLH.IsChecked == true && LHJointUnderTest)
            {
                cbCalibLH.IsEnabled = true; cbCalibLH.IsChecked = false; EncoderTestInProcess = true; LHJointUnderTest = false; JointCounter--;
            }

            if (cbCalibRH.IsChecked == true && RHJointUnderTest)//
            {
                cbCalibRH.IsEnabled = true; cbCalibRH.IsChecked = false; EncoderTestInProcess = true; RHJointUnderTest = false; JointCounter--;
            }
        }
        public void KneesCheck()
        {
            if (cbCalibLK.IsChecked == true && LKJointUnderTest)//
            {
                cbCalibLK.IsEnabled = true; cbCalibLK.IsChecked = false; EncoderTestInProcess = true; LKJointUnderTest = false; JointCounter--;
            }

            if (cbCalibRK.IsChecked == true && RKJointUnderTest)//
            {
                cbCalibRK.IsEnabled = true; cbCalibRK.IsChecked = false; EncoderTestInProcess = true; RKJointUnderTest = false; JointCounter--;
            }
        }
        DateTime CurrentTime;
        public void IfJointCounterEqualZero()
        {
            if (JointCounter == 0)
            {
                TestEncoderTimer.Stop();
                CurrentTime = DateTime.Now;
                string EncoderPlotPath = EncoderTestPath +
                "\\Encoder_Plot_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
                deviceData.SaveLogFile(EncoderPlotPath);
                string EncoderPlotPathText = EncoderPlotPath.TrimEnd("dat".ToCharArray());
                EncoderPlotPathText = EncoderPlotPathText + "txt";

                LogParser.ParseFile(EncoderPlotPath, false, EncoderPlotPathText);
                bool TimeStampParserFlag = LogParser.TimeStampFlagIncluded();
                //string LogFileName = tbBrowseViewPlot.Text;
                string TimeStampVariable;
                string Path_plot_encoder_readings = System.AppDomain.CurrentDomain.BaseDirectory;// Directory.GetCurrentDirectory();
                string plot_encoder_readings = @Path_plot_encoder_readings + "\\EncoderPlot_py_ver8\\plot_encoder_readings_REV08.exe";
                // string plot_encoder_readings = @"C:\\plot_encoder_readings_REV03.py";

                //string plot_encoder_readings = @Path_plot_encoder_readings + "\\plot_encoder_readings_REV03.py";
                //string LogFileNameCheck = Path.GetFileNameWithoutExtension(LogFileName);
                //string LogFileNameExtention = Path.GetExtension(LogFileNameCheck);


                if (!TimeStampParserFlag)
                {
                    TimeStampVariable = "3";
                }
                else
                {
                    TimeStampVariable = "4";
                }
                Storyboard EncoderTestStatus = (Storyboard)FindResource("LoadingEncoderTest");
                EncoderTestStatus.Stop();
                Process.Start(plot_encoder_readings, "\"" + EncoderPlotPathText + "\"" + " " + TimeStampVariable);
                BtnRunEncoderTest.IsEnabled = true;
                EncoderTestInProcessForALL = false;

            }
        }
   
        private string GetSysPass()
        {
            string st;
            //  4HXzxMQC
            st = "4HXzxMQC";// "RWLK30$$11a";
            return st;
        }

        private string SetPassBoxEmpty()
        {
            string st;
            st = "";
            return st;
        }
        //private string GetSysConfigPass()
        //{
        //    string st;
        //    st = "kingradi";
        //    return st;
        //}
        //private void OnSysConfigPasswordChanged(object sender, RoutedEventArgs e)
        //{
        //    string xxssword;  //= (sender as PasswordBox).Password;
        //    PasswordBox aa = sender as PasswordBox;
        //    if (aa != null)
        //    {
        //        xxssword = aa.Password;
        //        cbSysType.IsEnabled = (xxssword == GetSysConfigPass()) ? true : false;
        //        cbEnableStairs.IsEnabled = cbSysType.IsEnabled;
        //        // SerialNumbers_Expander.IsEnabled = cbSysType.IsEnabled;
        //        //  cbSwitchingTime.IsEnabled = cbSysType.IsEnabled;
        //        /* if ((short)deviceData.Parameters.DSP != 0x0909)
        //             btnResetStepCounter.IsEnabled = cbSysType.IsEnabled;
        //         btnResetStairCounter.IsEnabled = cbSysType.IsEnabled;

        //         //Sync password for two panes
        //         if (pbSysConfig.Password != xxssword)
        //             pbSysConfig.Password = xxssword;*/
        //    }

        //}
     
 
        private void OnBrowseSaveLogFile(object sender, RoutedEventArgs e)
        {
            TbRightMenu.IsChecked = false;
            SaveFileDialog ofd = new SaveFileDialog();
            ofd.CheckFileExists = false;
            ofd.RestoreDirectory = true;
            ofd.Filter = "DAT files (*.dat)|*.dat|(*.*) |*.*";

            if (ofd.ShowDialog().Value == true)
            {
                tbSaveLogFileLocation.Text = ofd.FileName;

                try
                {
                    //Just to make sure that file might be created
                    using (FileStream fs = new FileStream(tbSaveLogFileLocation.Text, FileMode.Create))
                    {
                        fs.Close();
                    }
                    deviceData.SaveLogFile(tbSaveLogFileLocation.Text);
                    try
                    {
                        string LogFileLocationText = tbSaveLogFileLocation.Text.TrimEnd("dat".ToCharArray());
                        LogFileLocationText = LogFileLocationText + "txt";

                        LogParser.ParseFile(tbSaveLogFileLocation.Text, false, LogFileLocationText);
                        MessageBoxResult Result = MessageBox.Show((string)this.FindResource("Log File Download Complete.") + "\n" + (string)this.FindResource("Open?"), (string)this.FindResource("Save Log File"), MessageBoxButton.YesNo);
                        if (Result == MessageBoxResult.Yes)
                        {
                            Process.Start("notepad", LogFileLocationText);
                        }
                    }
                    catch (DeviceOperationException exception)
                    {
                        MessageBox.Show((string)this.FindResource("Log Parser Failed"), (string)this.FindResource("Save Flash Log File"), MessageBoxButton.OK, MessageBoxImage.Error);
                        Logger.Dummy_Error("", exception.Message);
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error_MessageBox_Advanced("Save Log File failed:","Save Flash Log File",exception.Message);
                }
            }
        }
        //private void OnSaveLogFile(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        //Just to make sure that file might be created
        //        using (FileStream fs = new FileStream(tbSaveLogFileLocation.Text, FileMode.Create))
        //        {
        //            fs.Close();
        //        }

        //        deviceData.SaveLogFile(tbSaveLogFileLocation.Text);
        //        MessageBox.Show((string)FindResource("Log File Download Succeeded !"), (string)FindResource("Save Log File"), MessageBoxButton.OK, MessageBoxImage.Information);
        //    }
        //    catch (Exception exception)
        //    {
        //        Logger.Error_MessageBox_Advanced("Save Log File failed:","Save Flash Log File",exception.Message);
        //    }
        //}
        private void OnBrowseSaveFlashLogFile(object sender, RoutedEventArgs e)
        {
            SaveFileDialog ofd = new SaveFileDialog();
            ofd.CheckFileExists = false;
            ofd.RestoreDirectory = true;
            ofd.Filter = "DAT files (*.dat)|*.dat|(*.*) |*.*";
             App tempAFT = App.Current as App;
             if (tempAFT != null)
             {
                 if ((tempAFT).DeptName_BrowseSaveAsFlashLogFile == true)
                 {

                 //    MessageBox.Show("Flash Error already has been downloded!", "", MessageBoxButton.OK);
                     btnFlashLogBrowse.IsEnabled = false;
                     return;
                 }
             }

            if (ofd.ShowDialog().Value == true)
            {

                tbSaveFlashLogFileLocation.Text = ofd.FileName;

                try
                {
                    //Just to make sure that file might be created
                    using (FileStream fs = new FileStream(tbSaveFlashLogFileLocation.Text, FileMode.Create))
                    {
                        fs.Close();
                    }

                    deviceData.SaveFlashLogFile(tbSaveFlashLogFileLocation.Text);
                    try
                    {
                        string LogFileLocationText = tbSaveFlashLogFileLocation.Text.TrimEnd("dat".ToCharArray());
                        LogFileLocationText = LogFileLocationText + "txt";
                        //deviceData.SaveLogFile(tbSaveLogFileLocation.Text);
                        LogParser.ParseFile(tbSaveFlashLogFileLocation.Text, false, LogFileLocationText);
                       MessageBoxResult Result = MessageBox.Show((string)this.FindResource("Flash Error Log File Download Complete.") + "\n" + (string)this.FindResource("Open?"), (string)this.FindResource("Save Log File"), MessageBoxButton.YesNo);
                       if (Result == MessageBoxResult.Yes)
                       {
                           Process.Start("notepad", LogFileLocationText);
                       }
                       //else
                       //{

                       //    MessageBox.Show((string)this.FindResource("Flash Log File Download Succeeded !"), (string)this.FindResource("Save Log File"), MessageBoxButton.OK, MessageBoxImage.Information);
                       //}
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show((string)this.FindResource("Log Parser Failed !"), (string)this.FindResource("Save Flash Log File"), MessageBoxButton.OK, MessageBoxImage.Error);
                        Logger.Dummy_Error("", exception.Message);
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error_MessageBox_Advanced("Save Flash Log File failed:", "Save Flash Log File",exception.Message);

                }
            }
        }

        string HelpFileName = "";
        private void OnShowHelp(object sender, RoutedEventArgs e)
        {
    
            
            string appPath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
           
            HelpFileName = appPath + "\\TherapistManual_English\\TherapistManualUSA.pdf";
           

            try
            {
               
             TbRightMenu.IsChecked = false;
             Process help = new Process();
             ProcessStartInfo psi = new ProcessStartInfo(HelpFileName);
             psi.Verb = "open";
             help.StartInfo = psi;
             help.Start();
                       
            }
            catch (Exception exception)
            {
                TbRightMenu.IsChecked = false;
                Logger.Error_MessageBox_Advanced("Open therapist guide failed!", "Help", exception.Message);
            }

            /*  Process help = new Process();
              ProcessStartInfo psi = new ProcessStartInfo("Help.chm");//("UserManual.pdf");("Online_Help.chm")
              psi.Verb = "open";
              help.StartInfo = psi;
              help.Start();*/

        }
   
        private Parser LogParser = new Parser();
        private XmlDataProvider deviceSettingsValuesDataProvider;
        private Data deviceData = new Data();
        private DispatcherTimer onlineTimer = new DispatcherTimer();
        private DispatcherTimer testTimer = new DispatcherTimer();
        private DispatcherTimer calibTimer = new DispatcherTimer();
        private DispatcherTimer TestEncoderTimer = new DispatcherTimer();
        private DispatcherTimer BatteryStatusTimer = new DispatcherTimer();
        private bool USBConnected = false;
        private bool USBConnectState = false;
        private int USBConnectDelay = 0;
        private int USBDisConnectDelay = 0;
        bool RFTestStarted = false;
        private DispatcherTimer RFtestTimer = new DispatcherTimer();
        private DispatcherTimer RCUpdateTimer = new DispatcherTimer();
        private DispatcherTimer CheckUSBConnectedTime = new DispatcherTimer();
        private DispatcherTimer UpdateProgressBarTimer = new DispatcherTimer();
      
        string[] array2 = new string[31];
        Int16[] array2_int = new Int16[31];
        private const int KNEE_MAX_SPEED = 80; // in degrees/sec
        private const int MAX_STAIRS_THRESHOLD = 19;

        private void HipFlexion_SelectionChanged()
        {
               int i;
            
               for (i = 0; i <= XMLParamLoadCheckValid.KneeFlextionAngleLength_Value; i++)
               {
                   array2_int[i] = (Int16) (deviceData.Parameters.HipAngle + 8 + i - 1);
               }
               for (i = 0; i <= XMLParamLoadCheckValid.KneeFlextionAngleLength_Value; i++)
               {
                   array2[i] = Convert.ToString(array2_int[i]);
               }


               hx_index = Array.IndexOf(XMLParamLoadCheckValid.hx, (deviceData.Parameters.HipAngle).ToString());
               kx_index = Array.IndexOf(array2, (deviceData.Parameters.KneeAngle).ToString());
               if ((Convert.ToInt16(deviceData.Parameters.HipAngle) + 8) > Convert.ToInt16(deviceData.Parameters.KneeAngle))
               {
                   kx_index = 0;
                   deviceData.Parameters.KneeAngle = System.Convert.ToInt16(array2[kx_index]);
               }
            
            
               
        }
      
      
     
     
     
        private void CbSysType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetFielsdsDisplayAsSysTypeComboSelect();

            if (cbSysType.SelectedIndex == 1 || cbSysType.SelectedIndex == 3)//select P in case of P0 or P6
                SN_ComBox.SelectedIndex = 1;
            else
                SN_ComBox.SelectedIndex = cbSysType.SelectedIndex;

            if (USBConnectState == true)
            {
                Write4.Foreground = Brushes.Red;
                Write4.IsEnabled = true;
                Write.IsChecked  = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;

            }
        }
        private void CbCalibLH_Checked(object sender, RoutedEventArgs e)
        {
            btnStartCalib.IsEnabled = true;
            if ((short)deviceData.Parameters.DSP == 0x0909)
            {
                BtnRunEncoderTest.IsEnabled = false;
            }
            else
            {
                BtnRunEncoderTest.IsEnabled = true;
            }
        }
        private void CbCalibLK_Checked(object sender, RoutedEventArgs e)
        {
            btnStartCalib.IsEnabled = true;
            if ((short)deviceData.Parameters.DSP == 0x0909)
            {
                BtnRunEncoderTest.IsEnabled = false;
            }
            else
            {
                BtnRunEncoderTest.IsEnabled = true;
            }

        }
        private void CbCalibRH_Checked(object sender, RoutedEventArgs e)
        {
            btnStartCalib.IsEnabled = true;
            if ((short)deviceData.Parameters.DSP == 0x0909)
            {
                BtnRunEncoderTest.IsEnabled = false;
            }
            else
            {
                BtnRunEncoderTest.IsEnabled = true;
            }

        }
        private void CbCalibRK_Checked(object sender, RoutedEventArgs e)
        {
            btnStartCalib.IsEnabled = true;
            if ((short)deviceData.Parameters.DSP == 0x0909)
            {
                BtnRunEncoderTest.IsEnabled = false;
            }
            else
            {
                BtnRunEncoderTest.IsEnabled = true;
            }
        }
        private DateTime LastLogTime, FirstLogTime;
        private TimeSpan DeltaTime;
        private void LogGUIConnections() // parasoft-suppress  CS.MLC "Methods executing atomic functions" OOM.CYCLO "Coding style - method logic was verified"
        {
            CurrentTime = DateTime.Now; LastLogTime = CurrentTime; FirstLogTime = DateTime.MinValue; DeltaTime = TimeSpan.Zero;

            string path = SysLogPath; int LogSendDeltaTimeInDays = 100000; bool SendLog = false, cc = false; string FileName = "\\system_report_" + deviceData.Parameters.SN + ".rwk";
            try
            {
                using (StreamReader f = new StreamReader(File.Open(path + FileName, FileMode.Open)))
                {
                    string[] StringArray; string line = "-1"; bool LogSent = false, FoundLogSent = false;

                    do
                    {
                        if (line != "-1")
                        {
                            line = line.TrimStart('<'); line = line.TrimEnd('>'); StringArray = line.Split('|');
                            LogSent = System.Convert.ToBoolean(StringArray[2]);
                            if (!FoundLogSent) FoundLogSent = LogSent;
                            // if (LogSent) LastLogTime = DateTime.Parse(StringArray[0]);
                            if (LogSent)
                                cc = DateTime.TryParse(StringArray[0], out LastLogTime);
                            //if (FirstLogTime == DateTime.MinValue) FirstLogTime = DateTime.Parse(StringArray[0]);
                            if (FirstLogTime == DateTime.MinValue)
                                cc = DateTime.TryParse(StringArray[0], out FirstLogTime);

                        }
                        line = f.ReadLine();
                    } while (line != null);

                    if (!FoundLogSent) LastLogTime = FirstLogTime;

                    DeltaTime = CurrentTime.Subtract(LastLogTime);

                    f.Close();
                }

            }
            catch (Exception exception) { Logger.Dummy_Error("", exception.Message); }

            try
            {
                string LogSendDeltaTimeInDaysString = null;
                XmlDocument doc = new XmlDocument(); doc.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");
                XmlNodeList LogSendDeltaTimeNode = doc.SelectNodes("//ConnectionLogDeltaTime");
                if (LogSendDeltaTimeNode.Count == 1)
                    foreach (XmlNode value in LogSendDeltaTimeNode)
                    { LogSendDeltaTimeInDaysString = value.Attributes["Values"].Value; }
                LogSendDeltaTimeInDays = System.Convert.ToInt32(LogSendDeltaTimeInDaysString);
            }
            catch (Exception exception) { LogSendDeltaTimeInDays = 100000; Logger.Dummy_Error("", exception.Message); }



            if (DeltaTime.TotalDays >= LogSendDeltaTimeInDays) SendLog = true;

            try
            {
                using (StreamWriter f = new StreamWriter(File.Open(path + FileName, FileMode.Append)))

                { f.WriteLine("<{0,0} | {1,1} | {2,2}>", CurrentTime, deviceData.Parameters.StepCounter, SendLog); f.Close(); }

            }
            catch (Exception exception) { Logger.Dummy_Error("", exception.Message); }

            if (SendLog)
                SendEmailReport(0, true, false, ReportType.NORMAL);
        }
        private bool AFTToolScreenSet()
            {
                XmlDocument deviceSettings = new XmlDocument();
                deviceSettings.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");
                string yesno = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTToolScreenEnable").OuterXml;
                string[] yesno_answer = yesno.Split('#');

                if (yesno_answer[1] == "YES")
                {
                   // AFT.Visibility = Visibility.Visible;
                    MenuProductionHome.Visibility = Visibility.Visible;
                    MenuProductionHome.IsEnabled = true;
                    Production.Visibility = Visibility.Visible;
                    LanguageSelectButton.Visibility = Visibility.Collapsed;
                    MenuTechEnter.Visibility = Visibility.Collapsed;
                    HomeButton.Visibility = Visibility.Collapsed;

                    Production.IsSelected = true;
                    TopToolBar.Visibility = Visibility.Collapsed;
                    UD.Visibility = Visibility.Collapsed;
                    EditBtn.Visibility = Visibility.Collapsed;
                    return true;
                }
                else
                {
                  //  AFT.Visibility = Visibility.Hidden;
                     MenuProductionHome.Visibility = Visibility.Collapsed;
                     MenuProductionHome.IsEnabled = false;
                     Production.Visibility = Visibility.Hidden;
              
                    return false;
                }
            }
        private bool TranslationMessagesTestVisibilty()
        {
            XmlDocument deviceSettings = new XmlDocument();
            deviceSettings.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");
            string yesno = deviceSettings.SelectSingleNode("/DeviceSettingsValues/TestMessagesTranslation").OuterXml;
            string[] yesno_answer = yesno.Split('#');

            if (yesno_answer[1] == "YES")
            {
                TranMessTest.Visibility = Visibility.Visible;

                return true;
            }
            else
            {

                TranMessTest.Visibility = Visibility.Collapsed;
                return false;
            }
        }
        private string GUIVersionType = null;
        private short CurrentHandleAlogMethod ;

        private void ReWalkStartUpConfiguration() //this Function will read xml configration
        {
            try
            {
                XmlDocument deviceSettings = new XmlDocument();
                deviceSettings.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");
                string currenthanlderalgo = deviceSettings.SelectSingleNode("/DeviceSettingsValues/CurrentHandlerAlgo").OuterXml;
                string[] CurrentHandleStatus = currenthanlderalgo.Split('#');
                if (CurrentHandleStatus[1] == "1")
                {
                    CurrentHandleAlogMethod = 1;                                                         
                }
                else
                {
                    CurrentHandleAlogMethod = 0;
                }
                string SelectedArea = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RegionConfig").OuterXml;
                string[] name = SelectedArea.Split('#');
                if (name[1] == "USA" || name[1] == "Other")
                {
                    if (name[1] == "USA")
                    {

                        deviceData.Parameters.StairsEnabled = false;
                        TItemStairs.Visibility = Visibility.Collapsed;
                        btnStairsCalc.Visibility = Visibility.Collapsed;
                        StairsLogGroupBox.Visibility = Visibility.Collapsed;
                    //    StiarsCounterStackPanelDisplay.Visibility = Visibility.Collapsed;
                        cbEnableStairs.Visibility = Visibility.Collapsed;
                        GUIVersionType = "USA";

                    }
                    else if (name[1] == "Other")
                    {
                        GUIVersionType = "Other";
                    }
                }
                else
                {
                    Close();
                }
            }
            catch (Exception exception)
            {
          //      Logger.Error_MessageBox_Advanced("Loading System Configuration Parameters Failed:", "Loading System Configuration", exception.Message);

            }
           
        }
        private string GetSecureString()
        {
            string st = String.Empty;
            /* Code for getting the password */
            st = "reports2013";
            return st;
        }
        private string GetGmailPassword()
        {
            string st = String.Empty;
            /* Code for getting the password */
            st = "argoadmin";
            return st;
        }
        private Attachment LogFileAttachement, FlashLogFileAttachement, PCLogFileAttachement;
        private MailMessage message;// = new MailMessage;
        public void AutoSaveLogfile(string filename)
        {

            string path = CheckDirectory();
            string datfilename = path + "\\" + deviceData.Parameters.SN + filename + ".dat";
            string txtfilename = path + "\\" + deviceData.Parameters.SN + filename + ".txt";


            deviceData.SaveLogFile(datfilename);
            LogParser.ParseFile(datfilename, false, txtfilename);
            //Process.Start("notepad", txtfilename);

        }
        private string CheckDirectory()
        {
            deviceData.ReadData();
            string temp = AppDomain.CurrentDomain.BaseDirectory; string[] temp1;
            DirectoryInfo dir_info;
            temp1 = temp.Split('\\'); string AFTFolderPath = temp1[0] + "\\Argo\\AFT"; string SystemSerialNumber = temp1[0] + "\\Argo\\AFT\\" + deviceData.Parameters.SN;
            try
            {
                if (!Directory.Exists(AFTFolderPath))
                    dir_info = Directory.CreateDirectory(AFTFolderPath);
                if (!Directory.Exists(SystemSerialNumber))
                    dir_info = Directory.CreateDirectory(SystemSerialNumber);
                return SystemSerialNumber;
            }
            catch (Exception exception)
            {
                Logger.Error("", exception.Message);
                return null;
            }

        }
        public void AFTReport(string testername, string result, string status, string testname, string current, bool backlashflag, string backlashvalue) // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions" CS.MLC "Methods executing atomic functions" SEC.AUSD "Method inspected and no security violation found - only used for log record"
        {
            string path = CheckDirectory();
            DateTime CurrentTimeDate;
            CurrentTimeDate = DateTime.Now;
            string FileLocation = path + "\\" + deviceData.Parameters.SN + " Calbiration" + ".dat";
            string SystemParameterHeader = ReadSysParamsFromLog(FileLocation);

            string[] lines = SystemParameterHeader.Split('\n');
            int NumberOfLines = lines.Length;
            string filenamepath = path + "\\" + deviceData.Parameters.SN + "_AFT_Report" + ".txt";





            if (!File.Exists(filenamepath))
            {
                using (StreamWriter AFTFileReport = File.CreateText(filenamepath))
                {
                    AFTFileReport.WriteLine("Rewalk AFT Report Generated at :".PadRight(30) + CurrentTimeDate + "\n");
                    AFTFileReport.WriteLine("*********************************************************\n");
                    AFTFileReport.WriteLine("ReWalk Parameters :\n");
                    AFTFileReport.WriteLine(Environment.NewLine + " Tester Name:".PadRight(32) + testername + "\n");
                    for (int i = 3; i <= NumberOfLines - 22; i++)
                    {
                        AFTFileReport.WriteLine(lines[i]);
                    }

                    //AFTFileReport.WriteLine(Environment.NewLine + "____________ Test Name:- " + TestName + " _____________\n");
                    //AFTFileReport.WriteLine(Environment.NewLine + " Calibration Current:".PadRight(32) + current + "\n");
                    //AFTFileReport.WriteLine(Environment.NewLine + " Start Run 1 Test Time:".PadRight(32) + CurrentTimeDate + "\n");   

                }

            }

            else
            {
                using (StreamWriter AFTFileReport = File.AppendText(filenamepath))
                {
                    if (status == "Calibration")
                    {
                        AFTFileReport.WriteLine(Environment.NewLine + "__Test Name:- " + testname + " _____________\n");
                        AFTFileReport.WriteLine(Environment.NewLine + " Calibration Current:".PadRight(32) + current + "    Result:-  " + result + "\n");

                    }
                    if (status == "Start")
                    {

                        AFTFileReport.WriteLine(Environment.NewLine + " Start Test Time:".PadRight(32) + CurrentTimeDate + "  Test Name:- " + testname + "\n");
                        //   AFTFileReport.WriteLine(Environment.NewLine + " Run 1 Current:".PadRight(32) + current + "\n");
                        //   AFTFileReport.WriteLine(Environment.NewLine + " End Run 1 Test:".PadRight(32)  + CurrentTimeDate + "\n");
                        //   AFTFileReport.WriteLine(Environment.NewLine + " Start Run 2 Test Time:".PadRight(32) + CurrentTimeDate + "\n");

                    }
                    if (status == "End")
                    {
                        //    AFTFileReport.WriteLine(Environment.NewLine + " Start Current:".PadRight(32) + current + "\n");
                        if (backlashflag)
                            AFTFileReport.WriteLine(Environment.NewLine + " Backlash Result:".PadRight(32) + backlashvalue + "\n");
                        else
                            AFTFileReport.WriteLine(Environment.NewLine + " End Test:".PadRight(32) + CurrentTimeDate + " Test Name:- " + testname + "    Result:-  " + result + "\n".PadRight(32) + current + "\n");
                    }
                    /*    if (status == "Start_RUN_2")
                        {

                            AFTFileReport.WriteLine(Environment.NewLine + " Start Run 2 Test Time:".PadRight(32) + CurrentTimeDate + "\n");   
               
                        }

        */
                    if (status == "End_")
                    {
                        AFTFileReport.WriteLine(Environment.NewLine + " Current Test Result:".PadRight(32) + current + "\n");
                        AFTFileReport.WriteLine(Environment.NewLine + " End Run  Test:".PadRight(32) + CurrentTimeDate + "\n");
                        if (backlashflag)
                            AFTFileReport.WriteLine(Environment.NewLine + " Backlash Test Result:".PadRight(32) + "\n".PadRight(32) + backlashvalue + "\n");

                        // AFTFileReport.WriteLine(Environment.NewLine + " Test Result:".PadRight(32) + result + "\n");
                        //   AFTFileReport.WriteLine(Environment.NewLine + "Test Result:".PadRight(32) +"\n".PadRight(32)+ result + "\n");
                        //  AFTFileReport.WriteLine(Environment.NewLine + "______________________________________________________");

                    }
                    if (status == "End_All")
                    {

                        AFTFileReport.WriteLine(Environment.NewLine + " Test Result:".PadRight(32) + result + "\n");
                        AFTFileReport.WriteLine(Environment.NewLine + "______________________________________________________");

                    }

                }
            }

        }
        private string ReadSysParamsFromLog(string erroreilelocation)
        {
            try
            {
                using (BinaryReader b = new BinaryReader(File.Open(erroreilelocation, FileMode.Open)))
                {

                    int length = (int)b.BaseStream.Length;
                    char[] LUTData = new char[length];
                    int ret_int = b.Read(LUTData, 0, length);
                    string LUTStringData = new string(LUTData);
                    string SysParamsInfo = null;

                    bool SysParamsIncluded = LUTStringData.Contains("SYS_PARAMS");
                    if (SysParamsIncluded)
                    {
                        int index = LUTStringData.IndexOf("SYS_PARAMS");
                        // DateTime CurrentTimeLog = DateTime.Now;
                        string[] dataarray = new string[5000];
                        string[] ParamsArray = new string[1000];
                        string line2write;
                        dataarray = LUTStringData.Split('[');

                        LUTStringData = LUTStringData.Substring(index);//dataarray[4].Split(']');
                        ParamsArray = LUTStringData.Split('[');
                        ParamsArray = ParamsArray[0].Split(']');
                        ParamsArray = ParamsArray[1].Split('>');

                        SysParamsInfo = "\nRewalk parameters downloaded at " + CurrentTime + "\n";
                        SysParamsInfo += "***********************************************************\n";
                        SysParamsInfo += "\n";


                        foreach (string line in ParamsArray)
                        {
                            line2write = line.Replace('<', ' ');
                            SysParamsInfo += line2write + "\n";
                        }

                    }
                    b.Close();
                    return SysParamsInfo;
                }
            }
            catch (Exception exception)
            {
                Logger.Dummy_Error("", exception.Message);
                return null;
            }
        }
        private void SendEmailReport(ushort errtype, bool sendReport = false, bool reportProgress = true, ReportType reportTypeVal = ReportType.ERROR) // parasoft-suppress  CS.MLC "Methods executing atomic functions" OOM.CYCLO "Coding style - method logic was verified" METRICS.MCCC "Coding style - method logic was verified" METRICS.MLOC "Methods executing atomic functions"
        {

            CurrentTime = DateTime.Now;
            string SystemLogLocation;
            var fromAddress = new MailAddress("Rewalk.reports@argomedtec.com", "Rewalk Error Reports " + deviceData.Parameters.SN);
            var toAddress = new MailAddress("Rewalk.reports@argomedtec.com");

            // string fromPass = GetSecureString();// "reports2013";

            string subject;
            string body;


            if (reportTypeVal == ReportType.ERROR || reportTypeVal == ReportType.WARNING)
            {

            if (reportTypeVal == ReportType.ERROR)
            {
                subject = "Do Not Reply - Error report for system " + deviceData.Parameters.SN + " error #" + errtype;
                body = "This is an automatic report generated at " + CurrentTime + " for system " + deviceData.Parameters.SN + "\nerror #" + errtype + " occured, log and log flash file are attached.";
            }
            else
            {
                    subject = "Do Not Reply - Warning report for system " + deviceData.Parameters.SN + " warning #" + errtype;
                    body = "This is an automatic report generated at " + CurrentTime + " for system " + deviceData.Parameters.SN + "\nwarning #" + errtype + " occured, log file is attached";
                }
            }
            else
            {
                subject = "System Log Report System #" + deviceData.Parameters.SN;
                body = "This is an automatic report generated at " + CurrentTime + " for system " + deviceData.Parameters.SN + "log file is attached.";

            }

            string xmldata;
            string[] emails = null;
            string path = LogPath;//AppDomain.CurrentDomain.BaseDirectory;// Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData); //Directory.GetCurrentDirectory();

            bool DisableEmail = false;
            string output;
            string ErrorFileLocation;
            string ErrorFlashFileLocation = null;

            SystemLogLocation = path + "\\sys_logs\\system_report_" + deviceData.Parameters.SN + ".rwk";

            if (reportTypeVal == ReportType.ERROR || reportTypeVal == ReportType.WARNING)
            {
            if (reportTypeVal == ReportType.ERROR)
            {
                ErrorFileLocation = path + "\\log_error_report_system_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
                ErrorFlashFileLocation = path + "\\flash_log_error_report_system_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
            }
            else
            {
                    ErrorFileLocation = path + "\\log_warning_report_system_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
                }
            }
            else
            {
                ErrorFileLocation = path + "\\log_file_report_system_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
                if (deviceData.Parameters.SysErrorType != -1 && reportTypeVal != ReportType.WARNING)
                {
                    ErrorFlashFileLocation = path + "\\flash_log_error_report_system_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
                }
                else
                    ErrorFlashFileLocation = null;
            }
            try
            {
                //Just to make sure that file might be created
                using (FileStream fs = new FileStream(ErrorFileLocation, FileMode.Create))
                {
                    fs.Close();
                }

                deviceData.SaveLogFile(ErrorFileLocation);
            }
            catch (Exception exception)
            {
                if (reportProgress)
                {
                    output = string.Format("{0,15} ", " "+"Failed"+"\n");
                    progressform.rtfUserReport.AppendText(output);
                   
                }
                Logger.Dummy_Error("", exception.Message);
                return;
            }

            try
            {
                if (reportTypeVal == ReportType.ERROR ||
                    deviceData.Parameters.SysErrorType != -1 && reportTypeVal != ReportType.WARNING)
                {
                    //Just to make sure that file might be created
                    using (FileStream fs = new FileStream(ErrorFlashFileLocation, FileMode.Create))
                    {
                        fs.Close();
                    }

                    deviceData.SaveFlashLogFile(ErrorFlashFileLocation);
                    deviceData.SaveLogFile(ErrorFileLocation);
                }
            }
            catch (Exception exception)
            {
                if (reportProgress)
                {
                    output = string.Format("{0,15} "," "+ (string)FindResource("Failed") + "\n");
                    progressform.rtfUserReport.AppendText(output);
                   
                }
                Logger.Dummy_Error("", exception.Message);
                return;
            }
            if (reportProgress)
            {
                output = string.Format("{0,15} ", (string)FindResource("Done_")+"\n");
                progressform.rtfUserReport.AppendText(output);
            }


            string EmailInfo = ReadSysParamsFromLog(ErrorFileLocation);
            body += EmailInfo;
            XmlDocument doc = new XmlDocument();
            doc.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");

            try
            {
                XmlNodeList DisableEmailNode = doc.SelectNodes("//DisableEmail");
                if (DisableEmailNode.Count == 1)
                    foreach (XmlNode value in DisableEmailNode)
                    {
                        DisableEmail = value.Attributes["Values"].Value == "true" || value.Attributes["Values"].Value == "True";


                    }
            }
            catch (Exception exception)
            {
                Logger.Dummy_Error("", exception.Message);
            }


            if (DisableEmail && false == sendReport)
            {
               
                return;
            }
            if (reportProgress)
            {

                output = string.Format("{0,0} ", (string)FindResource("Sending error logs"));
                progressform.rtfUserReport.AppendText(output);
                bool bb = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
                if (!bb)
                {
                    output = string.Format("{0,15} ", "No internet connection available\n");
                    progressform.rtfUserReport.AppendText(output);
                  
                    return;
                }
            }
            try
            {
                XmlNodeList EmailNode = doc.SelectNodes("//Email");
                foreach (XmlNode email in EmailNode)
                {
                    xmldata = email.Attributes["Values"].Value;
                    emails = xmldata.Split(',');

                }
            }
            catch (Exception exception)
            {
                Logger.Dummy_Error("", exception.Message);
            }



            /*var smtp = new SmtpClient
            {
                Host = "mail.argomedtec.com",
                Port = 25,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = true,//false,
                Credentials = new NetworkCredential(fromAddress.User, GetSecureString(), "argoext")// (fromAddress.Address, fromPassword)
       
            };*/
            string GmailUser = "rewalk.reports123";

            var gmail = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(GmailUser, GetGmailPassword())// (fromAddress.Address, fromPassword)

            };

            message = new MailMessage(fromAddress, toAddress);

            message.Subject = subject;
            message.Body = body;

            foreach (string email in emails)
            {
                // if (email != "" && email != null)
                if (!String.IsNullOrEmpty(email))
                {

                    try
                    {
                        message.CC.Add(email);
                    }
                    catch (Exception exception)
                    {
                        Logger.Dummy_Error("", exception.Message);
                    }

                }
            }

            try
            {

                if (File.Exists(SystemLogLocation))
                {
                    PCLogFileAttachement = new Attachment(SystemLogLocation);
                    message.Attachments.Add(PCLogFileAttachement);
                }
                if (ErrorFileLocation != null)
                {
                    LogFileAttachement = new Attachment(ErrorFileLocation);
                    message.Attachments.Add(new Attachment(ErrorFileLocation));
                }
                if (ErrorFlashFileLocation != null)
                {
                    FlashLogFileAttachement = new Attachment(ErrorFlashFileLocation);
                    message.Attachments.Add(new Attachment(ErrorFlashFileLocation));
                }

            }
            catch (Exception exception)
            {
                output = string.Format("{0,15} ", "Error PC Log file writing!\n");
                progressform.rtfUserReport.AppendText(output);
                Logger.Dummy_Error("", exception.Message);
                return;
            }
            try
            {

                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };
                if (reportProgress)
                    //smtp.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(Client_SendCompleted);
                    gmail.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(Client_SendCompleted);
                //smtp.SendAsync(message, null);  

                gmail.SendAsync(message, null);


            }
            catch (Exception exception)
            {
                if (reportProgress)
                {
                    output = string.Format("{0,15} ", "Check internet connection!\n");
                    progressform.rtfUserReport.AppendText(output);
                   
                    return;
                }
                Logger.Dummy_Error("", exception.Message);
            }


        }
        void Client_SendCompleted(object sender, AsyncCompletedEventArgs e) // parasoft-suppress  CS.MLC "Methods executing atomic functions"
        {
            string output;


            if (e.Error != null)
            {
                var fromAddress = new MailAddress("Rewalk.reports@argomedtec.com", "Rewalk Error Reports " + deviceData.Parameters.SN);
                var toAddress = new MailAddress("Rewalk.reports@argomedtec.com");

                var smtp = new SmtpClient
                {
                    Host = "mail.argomedtec.com",
                    Port = 25,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = true,//false,
                    Credentials = new NetworkCredential(fromAddress.User, GetSecureString(), "argoext")// (fromAddress.Address, fromPassword)

                };

                /*string GmailUser = "rewalk.reports";
                
                var gmail = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(GmailUser, GetGmailPassword())// (fromAddress.Address, fromPassword)

                };*/

                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };

                //gmail.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(GmailClient_SendCompleted);

                //gmail.SendAsync(message, null);
                smtp.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(SecondaryClient_SendCompleted);

                smtp.SendAsync(message, null);
            }
            else
            {
                output = string.Format("{0,15} ", (string)FindResource("Done_") + "\n");
                progressform.rtfUserReport.AppendText(output);
                try
                {
                    message.Dispose();
                    PCLogFileAttachement.Dispose();
                    LogFileAttachement.Dispose();
                    FlashLogFileAttachement.Dispose();
                }
                catch (Exception exception)
                {
                    Logger.Dummy_Error("", exception.Message);
                }
              
            }


        }
        void SecondaryClient_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            string output;
            try
            {
                message.Dispose();
                PCLogFileAttachement.Dispose();
                LogFileAttachement.Dispose();
                FlashLogFileAttachement.Dispose();
            }
            catch (Exception exception)
            {
                Logger.Dummy_Error("", exception.Message);
            }

            if (e.Error != null)
            {

                output = string.Format("{0,15} "," "+ (string)FindResource("Failed") + "\n");
                progressform.rtfUserReport.AppendText(output);
            }
            else
            {
                output = string.Format("{0,15} ", (string)FindResource("Done_") + "\n");
                progressform.rtfUserReport.AppendText(output);


            }
            

        }
      
     
        private Report_Progress progressform = new Report_Progress();
        private void OnResetStepCounter(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show((string)this.FindResource("Are you sure you want to reset steps counter?"), (string)this.FindResource("Steps Counter Reset"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                UInt32 StepsCounterReset = deviceData.ResetStepsCounter();
                if (StepsCounterReset == 0)
                {
                    MessageBox.Show((string)this.FindResource("Reset was done successfully"), (string)this.FindResource("Confirmation"), MessageBoxButton.OK, MessageBoxImage.Warning);

                }
            }
         

        }
        private void OnSendLogFile(object sender, RoutedEventArgs e)
        {
            object s;

            TbRightMenu.IsChecked = false;
            s = Application.LoadComponent(new Uri("Report_Progress.xaml", UriKind.Relative));

            progressform = (Report_Progress)s;
            progressform.labelErrorReport.Content = (string)FindResource("Sending system report");

            string output = string.Format("{0,0} ", (string)FindResource("Saving log files"));
            progressform.AppendProgress(output);
            progressform.Visibility = System.Windows.Visibility.Visible;

            progressform.Show();
            SendEmailReport(0, true, true, ReportType.NORMAL);
            progressform.Close();
        }
        public void BtnCom1_Loaded(object sender, RoutedEventArgs e)
        {
            btnCom1_LH = (CommandCtrl)sender;
        }
        private void BtnCom2_Loaded(object sender, RoutedEventArgs e)
        {
            btnCom2_LK = (CommandCtrl)sender;
        }
        private void BtnCom3_Loaded(object sender, RoutedEventArgs e)
        {
            btnCom3_RH = (CommandCtrl)sender;
        }
        private void BtnCom4_Loaded(object sender, RoutedEventArgs e)
        {
            btnCom4_RK = (CommandCtrl)sender;
        }
        CommandCtrl btnCom1_LH = new CommandCtrl();
        CommandCtrl btnCom2_LK = new CommandCtrl();
        CommandCtrl btnCom3_RH = new CommandCtrl();
        CommandCtrl btnCom4_RK = new CommandCtrl();

        private void OnBrowseViewLogFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.CheckFileExists = false;
            ofd.RestoreDirectory = true;
            ofd.Filter = "(*.dat)|*.dat|(*.*) |*.* | (*.txt)|*.txt";

            if (ofd.ShowDialog().Value == true)
            {
                
                tbSaveLogFileLocation.Text = ofd.FileName;

                try
                {
                     string LogFileLocationText = ofd.FileName;// tbViewLogFileLocation.Text;
                     string LogFileNameExtention = Path.GetExtension(LogFileLocationText);
                     if (LogFileNameExtention == ".dat")
                      {
                       LogFileLocationText = tbSaveLogFileLocation.Text.TrimEnd("dat".ToCharArray());
                       LogFileLocationText = LogFileLocationText + "txt";

                       LogParser.ParseFile(tbSaveLogFileLocation.Text, false, LogFileLocationText);

                      }

                         Process.Start("notepad", LogFileLocationText);

                }
                catch (Exception exception)
                {
                    MessageBox.Show((string)this.FindResource("Log Parser Failed"), (string)this.FindResource("Save Flash Log File"), MessageBoxButton.OK, MessageBoxImage.Error);
                    Logger.Dummy_Error("", exception.Message);
                }
            }
        }
     
        private void BtnRunEncoderTest_Click(object sender, RoutedEventArgs e) // parasoft-suppress  OOM.CYCLO "Coding style - method logic was verified"
        {
            BtnRunEncoderTest.IsChecked = true;
            try
            {
                EncoderTestInProcessForALL = true;
                JointCounter = 0;

                if (cbCalibLH.IsChecked == true)
                    JointCounter++;
                if (cbCalibLK.IsChecked == true)
                    JointCounter++;
                if (cbCalibRH.IsChecked == true)
                    JointCounter++;
                if (cbCalibRK.IsChecked == true)
                    JointCounter++;

                if (JointCounter != 0)
                {
                    Storyboard EncoderTestStatus = (Storyboard)FindResource("LoadingEncoderTest");
                    //EncoderTestStatus.RepeatBehavior = RepeatBehavior.Forever;
                    EncoderTestStatus.Begin();
                    BtnRunEncoderTest.IsEnabled = false;
                    TestEncoderTimer.Start();
                    EncoderTestInProcess = true;
                }


            }
            catch (Exception exception)
            { 
                Logger.Error("Encoder Test Failed", exception.Message);
                EncoderTestInProcessForALL = false;
            }


        }

        private void LH_Download()
        {
            if (deviceData.TestData.ErrorCodeLH == (short)0x7fff) //success
            {
                MCUDownloadingProcessWindow.lblWorking.Content = (string)this.FindResource("LH Downloading Finished Successfully.") + "\n" + (string)this.FindResource("Starting LK Downloading...");
                deviceData.DownloadMCU(MCUFileArray);
                Joint_Being_Downloaded = 0x331;
                testTimer.Start();
            }
            else  //failed   
            {
                btnMCUDownload.IsEnabled = true;
                MCUDownloadingProcessWindow.MCUDOWNLOADINGINPROCESS = 0;//stop bar filling                                   
                MCUDownloadingProcessWindow.lblWorking.Content += (string)FindResource("LH Downloading has failed!");
                Mouse.OverrideCursor = this.Cursor;
                //   MCUDownloadingProcessWindow.m_BackgroundWorker.CancelAsync();
                MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
                if (deviceData.TestData.ErrorCodeLH == (short)10)
                {
                    MessageBox.Show((string)this.FindResource("LH downloading has failed:- Check Power or COM of LH") + "\n" + (string)this.FindResource("Error Code:" )+ deviceData.TestData.ErrorCodeLH.ToString(), (string)this.FindResource("MCU LH Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (deviceData.TestData.ErrorCodeLH == (short)9)
                {
                    MessageBox.Show((string)this.FindResource("LH downloading has failed:- Can't run LH in boot mode") + "\n" + (string)this.FindResource("Check Power or COM of LH") + "\n" + (string)this.FindResource("Error Code:") + deviceData.TestData.ErrorCodeLH.ToString(), (string)this.FindResource("MCU LH Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show((string)this.FindResource("LH downloading has failed:") + "\n" + (string)this.FindResource("Check Power or COM of LH") + "\n" + (string)this.FindResource("Error Code:") + deviceData.TestData.ErrorCodeLH.ToString(), (string)this.FindResource("MCU LH Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
                }

                MessageBox.Show(this, (string)this.FindResource("Downloading new MCU's software was canceled") + "\n" + (string)this.FindResource("Please restart the system and try again!!"), (string)this.FindResource("MCU's Downloading..."), MessageBoxButton.OK, MessageBoxImage.Stop);
                //Thread.Sleep(2000);
                Delay_ms(2000); // replace Thread.Sleep
                MCUDownloadingProcessWindow.Hide();


                return;
            }
        }
        private void LK_Download()
        {
            if (deviceData.TestData.ErrorCodeLK == (short)0x7fff) //succsees
            {
                MCUDownloadingProcessWindow.lblWorking.Content = (string)this.FindResource("LH Downloading Finished Successfully.") + "\n" + (string)this.FindResource("LK Downloading Finished Successfully.") + "\n" + (string)this.FindResource("Starting RH Downloading...");
                deviceData.DownloadMCU(MCUFileArray);
                Joint_Being_Downloaded = 0x332;
                testTimer.Start();
            }
            else  //failed   
            {
                btnMCUDownload.IsEnabled = true;
                MCUDownloadingProcessWindow.MCUDOWNLOADINGINPROCESS = 0;//stop bar filling                                   
                MCUDownloadingProcessWindow.lblWorking.Content += (string)this.FindResource("LK Downloading has failed!");
                Mouse.OverrideCursor = this.Cursor;
                MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
                if (deviceData.TestData.ErrorCodeLH == (short)10)
                {
                    MessageBox.Show((string)this.FindResource("LK downloading has failed:- Check Power or COM of LK") + "\n" + (string)this.FindResource("Error Code:") + deviceData.TestData.ErrorCodeLK.ToString(), (string)this.FindResource("MCU LK Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (deviceData.TestData.ErrorCodeLH == (short)9)
                {
                    MessageBox.Show((string)this.FindResource("LK downloading has failed:- Can't run LK in boot mode") + "\n" + (string)this.FindResource("Check Power or COM of LK") + "\n" + (string)this.FindResource("Error Code:") + deviceData.TestData.ErrorCodeLK.ToString(), (string)this.FindResource("MCU LK Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show((string)this.FindResource("LK downloading has failed:") + "\n" + (string)this.FindResource("Check Power or COM of LK") + "\n" + (string)this.FindResource("Error Code:") + deviceData.TestData.ErrorCodeLK.ToString(), (string)this.FindResource("MCU LK Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
                }

                MessageBox.Show(this, (string)this.FindResource("Downloading new MCU's software was canceled") + "\n" + (string)this.FindResource("Please restart the system and try again!!"), (string)this.FindResource("MCU's Downloading..."), MessageBoxButton.OK, MessageBoxImage.Stop);
                //Thread.Sleep(2000);
                Delay_ms(2000); // replace Thread.Sleep
                MCUDownloadingProcessWindow.Hide();


                return;
            }
        }
        private void RH_Download()
        {
            if (deviceData.TestData.ErrorCodeRH == (short)0x7fff) //success
            {
                MCUDownloadingProcessWindow.lblWorking.Content = (string)this.FindResource("LH Downloading Finished Successfully.") + "\n" + (string)this.FindResource("LK Downloading Finished Successfully.") + "\n" + (string)this.FindResource("RH Downloading Finished Successfully.") + "\n" +(string)this.FindResource("Starting RK Downloading...");
                deviceData.DownloadMCU(MCUFileArray);
                Joint_Being_Downloaded = 0x333;
                testTimer.Start();
            }
            else  //failed   
            {

                btnMCUDownload.IsEnabled = true;
                MCUDownloadingProcessWindow.MCUDOWNLOADINGINPROCESS = 0;//stop bar filling                                   
                MCUDownloadingProcessWindow.lblWorking.Content +=(string)this.FindResource("RH Downloading has failed!");
                Mouse.OverrideCursor = this.Cursor;
                MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
                if (deviceData.TestData.ErrorCodeRH == (short)10)
                {
                    MessageBox.Show((string)this.FindResource("RH downloading has failed:- Check Power or COM of RH") + "\n" + (string)this.FindResource("Error Code:") + deviceData.TestData.ErrorCodeRH.ToString(), (string)this.FindResource("MCU RH Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (deviceData.TestData.ErrorCodeRH == (short)9)
                {
                    MessageBox.Show((string)this.FindResource("RH downloading has failed:- Can't run RH in boot mode") + "\n" + (string)this.FindResource("Check Power or COM of RH") + "\n" + (string)this.FindResource("Error Code:") + deviceData.TestData.ErrorCodeRH.ToString(), (string)this.FindResource("MCU RH Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show((string)this.FindResource("RH downloading has failed:") + "\n" + (string)this.FindResource("Check Power or COM of RH") + "\n" + (string)this.FindResource("Error Code:") + deviceData.TestData.ErrorCodeRH.ToString(), (string)this.FindResource("MCU RH Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
                }

                MessageBox.Show(this, (string)this.FindResource("Downloading new MCU's software was canceled") + "\n" + (string)this.FindResource("Please restart the system and try again!!"), (string)this.FindResource("MCU's Downloading..."), MessageBoxButton.OK, MessageBoxImage.Stop);
                //Thread.Sleep(2000);
                Delay_ms(2000); // replace Thread.Sleep
                MCUDownloadingProcessWindow.Hide();


                return;
            }
        }
        private void RK_Download()
        {
            if (deviceData.TestData.ErrorCodeRK == (short)0x7fff) //success
            {
                MCUDownloadingProcessWindow.lblWorking.Content = (string)this.FindResource("LH Downloading Finished Successfully.") + "\n" + (string)this.FindResource("LK Downloading Finished Successfully.") + "\n" + (string)this.FindResource("RH Downloading Finished Successfully.") + "\n" + (string)this.FindResource("RK Downloading Finished Successfully.");
                Joint_Being_Downloaded = 0xfff;
                testTimer.Stop();
            }
            else  //failed   
            {
                btnMCUDownload.IsEnabled = true;
                MCUDownloadingProcessWindow.MCUDOWNLOADINGINPROCESS = 0;//stop bar filling                                   
                MCUDownloadingProcessWindow.lblWorking.Content +=(string)this.FindResource("RK Downloading has failed!");
                Mouse.OverrideCursor = this.Cursor;
                MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
                if (deviceData.TestData.ErrorCodeRK == (short)10)
                {
                    MessageBox.Show((string)this.FindResource("RK downloading has failed:- Check Power or COM of RK") + "\n" + (string)this.FindResource("Error Code:") + deviceData.TestData.ErrorCodeRK.ToString(), (string)this.FindResource("MCU RK Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (deviceData.TestData.ErrorCodeRK == (short)9)
                {
                    MessageBox.Show((string)this.FindResource("RK downloading has failed:- Can't run RK in boot mode") + "\n" + (string)this.FindResource("Check Power or COM of RK") + "\n" + (string)this.FindResource("Error Code:") + deviceData.TestData.ErrorCodeRK.ToString(), (string)this.FindResource("MCU RK Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show((string)this.FindResource("RK downloading has failed:") + "\n" + (string)this.FindResource("Check Power or COM of RK") + "\n" + (string)this.FindResource("Error Code:") + deviceData.TestData.ErrorCodeRH.ToString(), (string)this.FindResource("MCU RH Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
             }

                MessageBox.Show(this, (string)this.FindResource("Downloading new MCU's software was canceled") + "\n" + (string)this.FindResource("Please restart the system and try again!!"), (string)this.FindResource("MCU's Downloading..."), MessageBoxButton.OK, MessageBoxImage.Stop);
                //Thread.Sleep(2000);
                Delay_ms(2000); // replace Thread.Sleep
                MCUDownloadingProcessWindow.Hide();
                testTimer.Stop();
                return;
            }
            Mouse.OverrideCursor = this.Cursor;
            //Thread.Sleep(1000);          
            Delay_ms(1000); // replace Thread.Sleep
            MessageBox.Show(this, (string)this.FindResource("Downloading new MCU's software was finished successfully") + "\n" + (string)this.FindResource("Please, Restart the system!!"), (string)this.FindResource("MCU Downloading..."), MessageBoxButton.OK, MessageBoxImage.Information);
            //Thread.Sleep(2000);
            Delay_ms(2000); // replace Thread.Sleep
            btnMCUDownload.IsEnabled = true;
            MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
            MCUDownloadingProcessWindow.Hide();
        }
  
        private void OnResetStairCounter(object sender, RoutedEventArgs e)
        {

            MessageBoxResult result = MessageBox.Show((string)this.FindResource("Are you sure you want to reset stairs counter?"), (string)this.FindResource("Stairs Counter Reset"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                
                UInt32 StairCounterReset = deviceData.ResetStairsCounter();
                if (StairCounterReset == 0)
                {
                    MessageBox.Show((string)this.FindResource("Reset was done successfully"),(string)this.FindResource( "Confirmation"), MessageBoxButton.OK, MessageBoxImage.Warning);

                }
            }


          

        }
        private void BtnClearLogFile_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show((string)this.FindResource("Are you sure you want to reset log file ?"), (string)this.FindResource("Log File Reset"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                string ClearLogFileStatus = deviceData.ClearLogFile();
                string xx = Logger.ValidateClearError(ClearLogFileStatus);
                MessageBox.Show((string)this.FindResource(xx), (string)this.FindResource("Confirmation"), MessageBoxButton.OK, MessageBoxImage.Warning);

            }
        }
        private void BtnStairsCalc_Click(object sender, RoutedEventArgs e)
        {
            

                 //ASCRightHip, ASCRightKnee, PlacingRighHip, PlacingRightKnee, DSCLeftHip, DSCLeftKnee, DSCRightHip 
                 StairAnglesCalculator StCalc = new StairAnglesCalculator();
                 StairAnglesCalculator.StairsFlexions StairsFlexions = new StairAnglesCalculator.StairsFlexions();
                 double FootPlateSize = 0;
                 switch (deviceData.Parameters.FootPlateType2)
                 {
                     case 0: FootPlateSize = 82;//small
                         break;
                     case 1: FootPlateSize = 87;//medium
                         break;
                     case 2: FootPlateSize = 92;//large
                         break;
                     default: FootPlateSize = 0;//N/A
                         break;
                 }
                StairsFlexions = StCalc.CalculateStarisAngle(Convert.ToDouble(deviceData.Parameters.UpperLegHightSize), Convert.ToDouble(deviceData.Parameters.LowerLegHightSize)+FootPlateSize, Convert.ToDouble(deviceData.Parameters.ShoeSize), Convert.ToDouble(deviceData.Parameters.StairHeight));
                deviceData.Parameters.LHAsc = (short) StairsFlexions.ASC_LH;
                deviceData.Parameters.LKAsc = (short) StairsFlexions.ASC_LK;
                deviceData.Parameters.RHAsc = (short) StairsFlexions.ASC_RH;
                deviceData.Parameters.RKAsc = (short) StairsFlexions.ASC_RK;
                deviceData.Parameters.LHDsc = (short) StairsFlexions.DSC_LH;
                deviceData.Parameters.LKDsc = (short) StairsFlexions.DSC_Lk;
                deviceData.Parameters.RHDsc = (short) StairsFlexions.DSC_RH;
                deviceData.Parameters.RKDsc = (short) StairsFlexions.DSC_RK;
                deviceData.Parameters.Placing_RH_on_the_stair = (short)StairsFlexions.PLACING_RH;
                deviceData.Parameters.Placing_RK_on_the_stair = (short)StairsFlexions.PLACING_RK;
				deviceData.Parameters.Ack_Movement_Point = (short)StairsFlexions.Ack_Movement_Point;
            if(StairsFlexions.EXCEEDLIMITS)
            {
                StairsFlexions.EXCEEDLIMITS = false;
                MessageBox.Show((string)FindResource("Stairs flexion exceed the allowed limits")+"\n" + StairsFlexions.EXCEEDFLEXIONS, (string)FindResource("Stairs Calculation Warning"), MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            Write.IsChecked = true;
            Write.IsChecked = true;
            Write4.Foreground = Brushes.Red;
            Write.IsChecked = true;
            Write2.Foreground = Brushes.Red;
            Write3.Foreground = Brushes.Red;

        }



        //New GUI Rewalk 6
        private void ControlsIncDecButtonsEvent(object sender, RoutedEventArgs e) // parasoft-suppress  METRICS.MCCC "Methods executing atomic functions"
        {
            Write.IsChecked = true;
            Write4.Foreground = Brushes.Red;
            Write2.Foreground = Brushes.Red;
            Write3.Foreground = Brushes.Red;
            Button but = sender as Button;
            if(but != null)
            {
            switch ((string)(but.Name)) //button name was pressed
            {
               
                case "UpperStarpHolderSizeIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        if (deviceData.Parameters.UpperStrapHoldersSize < XMLParamLoadCheckValid.PUpperStrapHolderSizeLength_Value)
                        {
                            deviceData.Parameters.UpperStrapHoldersSize++;
                        }
                    }
                    else if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        if (deviceData.Parameters.UpperStrapHoldersSize < XMLParamLoadCheckValid.PUpperStrapHolderSizeLength_Value)
                        {
                            deviceData.Parameters.UpperStrapHoldersSize++;
                        }
                    }
                    break;
                case "UpperStarpHolderSizeDecrease":

                    if (deviceData.Parameters.UpperStrapHoldersSize > 0)
                    {
                        deviceData.Parameters.UpperStrapHoldersSize--;
                    }
                    break;
                case "UpperStarpHolderTypeIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        if (deviceData.Parameters.UpperStrapHoldersPosition < XMLParamLoadCheckValid.PUpperStrapHolderTypeLength_Value)
                        {
                            deviceData.Parameters.UpperStrapHoldersPosition++;
                        }
                    }
                    else if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        if (deviceData.Parameters.UpperStrapHoldersPosition < XMLParamLoadCheckValid.PUpperStrapHolderTypeLength_Value)
                        {
                            deviceData.Parameters.UpperStrapHoldersPosition++;
                        }
                    }
                    break;
                case "UpperStarpHolderTypeDecrease":

                    if (deviceData.Parameters.UpperStrapHoldersPosition > 0)
                    {
                        deviceData.Parameters.UpperStrapHoldersPosition--;
                    }
                    break;

                case "AboveKneeBrackeTypeIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        if (deviceData.Parameters.AboveKneeBracketSize < XMLParamLoadCheckValid.RAboveKneeBracketTypeLength_Value)
                        {
                            deviceData.Parameters.AboveKneeBracketSize++;
                        }
                    }
                    else if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        if (deviceData.Parameters.AboveKneeBracketSize < XMLParamLoadCheckValid.PAboveKneeBracketTypeLength_Value)
                        {
                            deviceData.Parameters.AboveKneeBracketSize++;
                        }
                    }
                    break;
                case "AboveKneeBrackeTypeDecrease":

                    if (deviceData.Parameters.AboveKneeBracketSize > 0)
                    {
                        deviceData.Parameters.AboveKneeBracketSize--;
                    }
                    break;

                case "AboveKneeBrackeAnteriorIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        if (deviceData.Parameters.AboveKneeBracketPosition < XMLParamLoadCheckValid.RAboveKneeBracketAnteriorLength_Value)
                        {
                            deviceData.Parameters.AboveKneeBracketPosition++;
                        }
                    }
                    else if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        if (deviceData.Parameters.AboveKneeBracketPosition < XMLParamLoadCheckValid.PAboveKneeBracketAnteriorLength_Value)
                        {
                            deviceData.Parameters.AboveKneeBracketPosition++;
                        }
                    }
                    break;
                case "AboveKneeBrackeAnteriorDecrease":

                    if (deviceData.Parameters.AboveKneeBracketPosition > 0)
                    {
                        deviceData.Parameters.AboveKneeBracketPosition--;
                    }
                    break;

                case "KneeBrackeTypeIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        if (deviceData.Parameters.FrontKneeBracketAssemble < XMLParamLoadCheckValid.RKneeBracketTypeLength_Value)
                        {
                            deviceData.Parameters.FrontKneeBracketAssemble++;
                        }
                    }
                    else if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        if (deviceData.Parameters.FrontKneeBracketAssemble < XMLParamLoadCheckValid.PKneeBracketTypeLength_Value)
                        {
                            deviceData.Parameters.FrontKneeBracketAssemble++;
                        }
                    }
                    break;
                case "KneeBrackeTypeDecrease":

                    if (deviceData.Parameters.FrontKneeBracketAssemble > 0)
                    {
                        deviceData.Parameters.FrontKneeBracketAssemble--;
                    }
                    break;
                case "KneeBrackeAnteriorIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        if (deviceData.Parameters.FrontKneeBracketAnteriorPosition < XMLParamLoadCheckValid.RKneeBracketAnteriorPositionLength_Value)
                        {
                            deviceData.Parameters.FrontKneeBracketAnteriorPosition++;
                        }
                    }
                    else if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        if (deviceData.Parameters.FrontKneeBracketAnteriorPosition < XMLParamLoadCheckValid.PKneeBracketAnteriorPositionLength_Value)
                        {
                            deviceData.Parameters.FrontKneeBracketAnteriorPosition++;
                        }
                    }
                    break;
                case "KneeBrackeAnteriorDecrease":

                    if (deviceData.Parameters.FrontKneeBracketAnteriorPosition > 0)
                    {
                        deviceData.Parameters.FrontKneeBracketAnteriorPosition--;
                    }
                    break;

                case "KneeBrackeLateralIncrease":

                    if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        if (deviceData.Parameters.FrontKneeBracketLateralPosition < XMLParamLoadCheckValid.PKneeBracketLateralPositionLength_Value)
                        {
                            deviceData.Parameters.FrontKneeBracketLateralPosition++;
                        }
                    }
                  
                    break;
                case "KneeBrackeLateralDecrease":

                    if (deviceData.Parameters.FrontKneeBracketLateralPosition > 0)
                    {
                        deviceData.Parameters.FrontKneeBracketLateralPosition--;
                    }
                    break;
                case "PelvicSizeIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        if (deviceData.Parameters.PelvicSize < XMLParamLoadCheckValid.RPelvicSizeLength_Value)
                        {
                            deviceData.Parameters.PelvicSize++;
                        }
                    }
                    else if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        if (deviceData.Parameters.PelvicSize < XMLParamLoadCheckValid.PPelvicSizeLength_Value)
                        {
                            deviceData.Parameters.PelvicSize++;
                        }
                    }
                    break;
                case "PelvicSizeDecrease":

                    if (deviceData.Parameters.PelvicSize > 0)
                    {
                        deviceData.Parameters.PelvicSize--;
                    }
                    break;
                case "PelvicAnteriorPositionIncrease":

                   if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        if (deviceData.Parameters.PelvicAnteriorPosition < XMLParamLoadCheckValid.PPelvicAnteriorPositionLength_Value)
                        {
                            deviceData.Parameters.PelvicAnteriorPosition++;
                        }
                    }
                   
                    break;
                case "PelvicAnteriorPositioDecrease":

                    if (deviceData.Parameters.PelvicAnteriorPosition > 0)
                    {
                        deviceData.Parameters.PelvicAnteriorPosition--;
                    }
                    break;

                case "PelvicVerticalPositionIncrease":
                   
                   if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        if (deviceData.Parameters.PelvicVerticalPosition < XMLParamLoadCheckValid.PPelvicVerticalPositionLength_Value)
                        {
                            deviceData.Parameters.PelvicVerticalPosition++;
                        }
                    }
                    break;
                case "PelvicVerticalPositioDecrease":

                    if (deviceData.Parameters.PelvicVerticalPosition > 0)
                    {
                        deviceData.Parameters.PelvicVerticalPosition--;
                    }
                    break;
                case "UpperLegLengthIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (rup_leg_length_index < XMLParamLoadCheckValid.RUpperLegLengthLength_Value)
                        {
                            rup_leg_length_index++;
                            deviceData.Parameters.UpperLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.RUpLegLen[rup_leg_length_index]);
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        if (pup_leg_length_index < XMLParamLoadCheckValid.PUpperLegLengthLength_Value)
                        {
                            pup_leg_length_index++;
                            deviceData.Parameters.UpperLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.PUpLegLen[pup_leg_length_index]);
                        }
                    }
                    break;
                    
                case "UpperLegLengthDecrease":
                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (rup_leg_length_index > 0)
                        {
                            rup_leg_length_index--;
                            deviceData.Parameters.UpperLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.RUpLegLen[rup_leg_length_index]);
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        if (pup_leg_length_index > 0)
                        {
                            pup_leg_length_index--;
                            deviceData.Parameters.UpperLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.PUpLegLen[pup_leg_length_index]);
                        }
                    }
                    break;

                case "LowerLegLengthIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" )
                    {
                        if (rlow_leg_length_index < XMLParamLoadCheckValid.RLowerLegLengthLength_Value)
                        {
                            rlow_leg_length_index++;
                            deviceData.Parameters.LowerLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.RLowLegLen[rlow_leg_length_index]);
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        if (plow_leg_length_index < XMLParamLoadCheckValid.PLowerLegLengthLength_Value)
                        {
                            plow_leg_length_index++;
                            deviceData.Parameters.LowerLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.PLowLegLen[plow_leg_length_index]);
                        }
                    }
                    break;
                case "LowerLegLengthDecrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                    {
                        if (rlow_leg_length_index > 0)
                        {
                            rlow_leg_length_index--;
                            deviceData.Parameters.LowerLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.RLowLegLen[rlow_leg_length_index]);
                        }
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        if (plow_leg_length_index > 0)
                        {
                            --plow_leg_length_index;
                            deviceData.Parameters.LowerLegHightSize = System.Convert.ToInt16(XMLParamLoadCheckValid.PLowLegLen[plow_leg_length_index]);
                        }
                    }
                    break;

                case "FootPlateTypeIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        if (deviceData.Parameters.FootPlateType1 < XMLParamLoadCheckValid.RFootPlateTypeLength_Value)
                        {
                            deviceData.Parameters.FootPlateType1++;
                        }
                    }
                    else if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        if (deviceData.Parameters.FootPlateType1 < XMLParamLoadCheckValid.PFootPlateTypeLength_Value)
                        {
                            deviceData.Parameters.FootPlateType1++;
                        }
                    }
                    break;
                case "FootPlateTypeDecrease":

                    if (deviceData.Parameters.FootPlateType1 > 0)
                    {
                        deviceData.Parameters.FootPlateType1--;
                    }
                    break;
                case "FootPlateSizeIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        if (deviceData.Parameters.FootPlateType2 < XMLParamLoadCheckValid.RFootPlateSizeLength_Value)
                        {
                            deviceData.Parameters.FootPlateType2++;
                        }
                    }
                    else if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        if (deviceData.Parameters.FootPlateType2 < XMLParamLoadCheckValid.PFootPlateSizeLength_Value)
                        {
                            deviceData.Parameters.FootPlateType2++;
                        }
                    }
                    break;
                case "FootPlatesizeDecrease":

                    if (deviceData.Parameters.FootPlateType2 > 0)
                    {
                        deviceData.Parameters.FootPlateType2--;
                    }
                    break;
                case "FootPlateDorsiFlextionIncrease":

                    if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        if (deviceData.Parameters.FootPlateNoOfRotation < XMLParamLoadCheckValid.RFootPlateDorsiFlexionPositionLength_Value)
                        {
                            deviceData.Parameters.FootPlateNoOfRotation++;
                        }
                    }
                    else if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        if (deviceData.Parameters.FootPlateNoOfRotation < XMLParamLoadCheckValid.PFootPlateDorsiFlexionPositionLength_Value)
                        {
                            deviceData.Parameters.FootPlateNoOfRotation++;
                        }
                    }
                    break;
                case "FootPlateDorsiFlextionDecrease":

                    if (deviceData.Parameters.FootPlateNoOfRotation > 0)
                    {
                        deviceData.Parameters.FootPlateNoOfRotation--;
                    }
                    break;

 //////////////////////////////////////  Standing tab item //////////////////////
                case "FallDetectionThresholdIncrease":

                    if (faldet_index < XMLParamLoadCheckValid.FallDetectionThrLength_Value)
                    {
                        faldet_index++;
                        deviceData.Parameters.YThreshold = System.Convert.ToInt16(XMLParamLoadCheckValid.Faldet[faldet_index]);
                    }
                    break;
                case "FallDetectionThresholdDecrease":

                    if (faldet_index > 0)
                    {
                        faldet_index--;
                        deviceData.Parameters.YThreshold= System.Convert.ToInt16(XMLParamLoadCheckValid.Faldet[faldet_index]);
                    }
                    break;
                case "StandCurrentThresholdIncrease":

                    if (stct_index < XMLParamLoadCheckValid.SystemCurrentThrLength_Value)
                    {
                        stct_index++;
                        deviceData.Parameters.Stairs_Current_Threshold = System.Convert.ToInt16(XMLParamLoadCheckValid.Stct[stct_index]);
                    }
                    break;
                case "StandCurrentThresholdDecrease":

                    if (stct_index > 0)
                    {
                        stct_index--;
                        deviceData.Parameters.Stairs_Current_Threshold = System.Convert.ToInt16(XMLParamLoadCheckValid.Stct[stct_index]);

                    }
                    break;
     ///////////////////////////////////////////////  Walking tab item //////////////////////

                case "HipFlextionIncrease":

                    if (hx_index < XMLParamLoadCheckValid.HipFlextionAngleLength_Value)
                    {
                        hx_index++;
                        deviceData.Parameters.HipAngle = System.Convert.ToInt16(XMLParamLoadCheckValid.hx[hx_index]) ;
                    }
                    HipFlexion_SelectionChanged();
                    break;
                case "HipFlextionDecrease":

                    if (hx_index > 0)
                    {
                        hx_index--;
                        deviceData.Parameters.HipAngle = System.Convert.ToInt16(XMLParamLoadCheckValid.hx[hx_index]);
                    }
                    HipFlexion_SelectionChanged();
                    break;

                case "KneeFlextionIncrease":

                    if (kx_index < XMLParamLoadCheckValid.KneeFlextionAngleLength_Value)
                    {
                       // deviceData.Parameters.KneeAngle = System.Convert.ToInt16(XMLParamLoadCheckValid.kx[++kx_index]);
                        kx_index++;
                        deviceData.Parameters.KneeAngle = System.Convert.ToInt16(array2[kx_index]);
                        
                    }
                    break;
                case "KneeFlextionDecrease":

                    if (kx_index > 0)
                    {
                      //  deviceData.Parameters.KneeAngle = System.Convert.ToInt16(XMLParamLoadCheckValid.kx[--kx_index]);
                        kx_index--;
                        deviceData.Parameters.KneeAngle = System.Convert.ToInt16(array2[kx_index]);
                    }
                    break;
                case "StepTimeIncrease":

                    if (spt_index < XMLParamLoadCheckValid.StepTimeLength_Value)
                    {
                        spt_index++;
                        deviceData.Parameters.MaxVelocity = System.Convert.ToInt16(XMLParamLoadCheckValid.Spt[spt_index]);
                    }
                    break;
                case "StepTimeDecrease":
                
                    if (spt_index > 0)
                    {
                        spt_index--;
                        deviceData.Parameters.MaxVelocity = System.Convert.ToInt16(XMLParamLoadCheckValid.Spt[spt_index]);
                    }
                    break;
                case "TiltDeltaIncrease":
                    if (tiltd_index < XMLParamLoadCheckValid.TiltDeltaLength_Value)
                    {
                        tiltd_index++;
                        deviceData.Parameters.TiltDelta = System.Convert.ToInt16(XMLParamLoadCheckValid.Tiltd[tiltd_index]);
                    }
                    break;
                case "TiltDeltaDecrease":
                    if (tiltd_index > 0)
                    {
                        tiltd_index--;
                        deviceData.Parameters.TiltDelta = System.Convert.ToInt16(XMLParamLoadCheckValid.Tiltd[tiltd_index]);
                    }
                    break;
                case "TiltTimeoutIncrease":
                    if (tiltt_index < XMLParamLoadCheckValid.TiltTimeOutLength_Value)
                    {
                        tiltt_index++;
                        deviceData.Parameters.TiltTimeout = System.Convert.ToInt16(XMLParamLoadCheckValid.Tiltt[tiltt_index]);
                    }
                    break;
                case "TiltTimeoutDecrease":
                    if (tiltt_index > 0)
                    {
                        tiltt_index--;
                        deviceData.Parameters.TiltTimeout = System.Convert.ToInt16(XMLParamLoadCheckValid.Tiltt[tiltt_index]);
                    }
                    break;
                case "BeginnersStepTimeIncrease":
                    if (tspt_index < XMLParamLoadCheckValid.BeginnersStepTimeLength_Value)
                    {
                        tspt_index++;
                        deviceData.Parameters.BackHipAngle = System.Convert.ToInt16(XMLParamLoadCheckValid.Tspt[tspt_index]);
                    }
                    break;
                case "BeginnersStepTimeDecrease":
                    if (tspt_index > 0)
                    {
                        tspt_index--;
                        deviceData.Parameters.BackHipAngle = System.Convert.ToInt16(XMLParamLoadCheckValid.Tspt[tspt_index]);
                    }
                    break;
                case "DelayBetweenStepsIncrease":
                    if (dbets_index < XMLParamLoadCheckValid.DelayBetweenStepsLength_Value)
                    {
                        dbets_index++;
                        deviceData.Parameters.DelayBetweenSteps = System.Convert.ToInt16(XMLParamLoadCheckValid.Dbets[dbets_index]);
                    }
                    break;
                case "DelayBetweenStepsDecrease":
                    if (dbets_index > 0)
                    {
                        dbets_index--;
                        deviceData.Parameters.DelayBetweenSteps = System.Convert.ToInt16(XMLParamLoadCheckValid.Dbets[dbets_index]);
                    }
                    break;
                case "WalkCurrentThresholdIncrease":
                    if (wact_index < XMLParamLoadCheckValid.WalkCurrentThrLength_Value)
                    {
                        wact_index++;
                        deviceData.Parameters.Walk_Current_Threshold = System.Convert.ToInt16(XMLParamLoadCheckValid.Wact[wact_index]);
                    }
                    break;
                case "WalkCurrentThresholdDecrease":
                    if (wact_index > 0)
                    {
                        wact_index--;
                        deviceData.Parameters.Walk_Current_Threshold = System.Convert.ToInt16(XMLParamLoadCheckValid.Wact[wact_index]);
                    }
                    break;
                case "FirstStepFlexionIncrease":
                    if (fsf_index < XMLParamLoadCheckValid.FirststepFlexionLength_Value)
                    {
                        fsf_index++;
                        deviceData.Parameters.FirstStepFlexion = System.Convert.ToInt16(XMLParamLoadCheckValid.Fsf[fsf_index]);
                    }
                    break;
                case "FirstStepFlexionDecrease":
                    if (fsf_index > 0)
                    {
                        fsf_index--;
                        deviceData.Parameters.FirstStepFlexion = System.Convert.ToInt16(XMLParamLoadCheckValid.Fsf[fsf_index]);
                    }
                    break;
                case "MaxHipFlextionIncrease":
                    if (hmaxf_index < XMLParamLoadCheckValid.HipMaxFlextionAngleLength_Value)
                    {
                        hmaxf_index++;
                        deviceData.Parameters.FSRThreshold = System.Convert.ToInt16(XMLParamLoadCheckValid.Hmaxf[hmaxf_index]);
                    }
                    break;
                case "MaxHipFlextionDecrease":
                    if (hmaxf_index > 0)
                    {
                        hmaxf_index--;
                        deviceData.Parameters.FSRThreshold = System.Convert.ToInt16(XMLParamLoadCheckValid.Hmaxf[hmaxf_index]);
                    }
                    break;
                case "HipFinalFlextionIncrease":
                    if (hfinf_index < XMLParamLoadCheckValid.HipFinalFlextionAngleLength_Value)
                    {
                        hfinf_index++;
                        deviceData.Parameters.FSRWalkTimeout = System.Convert.ToInt16(XMLParamLoadCheckValid.Hfinf[hfinf_index]);
                    }
                    break;
                case "HipFinalFlextionDecrease":
                    if (hfinf_index > 0)
                    {
                        hfinf_index--;
                        deviceData.Parameters.FSRWalkTimeout = System.Convert.ToInt16(XMLParamLoadCheckValid.Hfinf[hfinf_index]);
                    }
                    break;
                case "StairsASCSpeedIncrease":
                    if (ascsp_index < XMLParamLoadCheckValid.ASCSpeedLength_Value)
                    {
                        ascsp_index++;
                        deviceData.Parameters.StairsASCSpeed = System.Convert.ToInt16(XMLParamLoadCheckValid.Ascsp[ascsp_index]);
                    }
                    break;
                case "StairsASCSpeedDecrease":
                    if (ascsp_index > 0)
                    {
                        ascsp_index--;
                        deviceData.Parameters.StairsASCSpeed = System.Convert.ToInt16(XMLParamLoadCheckValid.Ascsp[ascsp_index]);
                    }
                    break;
                case "StairsDSCSpeedIncrease":
                    if (dscsp_index < XMLParamLoadCheckValid.DSCSpeedLength_Value)
                    {
                        dscsp_index++;
                        deviceData.Parameters.StairsDSCSpeed = System.Convert.ToInt16(XMLParamLoadCheckValid.Dscsp[dscsp_index]);
                    }
                    break;
                case "StairsDSCSpeedDecrease":
                    if (dscsp_index > 0)
                    {
                        dscsp_index--;
                        deviceData.Parameters.StairsDSCSpeed = System.Convert.ToInt16(XMLParamLoadCheckValid.Dscsp[dscsp_index]);
                    }
                    break;
                case "StairHeighIncrease":
                    if (stairheight_index < XMLParamLoadCheckValid.StairHeightLength_Value)
                    {
                        stairheight_index++;
                        deviceData.Parameters.StairHeight = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairheight[stairheight_index]);
                    }
                    break;
                case "StairHeighDecrease":
                    if (stairheight_index > 0)
                    {
                        stairheight_index--;
                        deviceData.Parameters.StairHeight = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairheight[stairheight_index]);
                    }
                    break;
                ///////////////////////////////////////////////  Stairs tab item //////////////////////


                case "LHASCIncrease":
                    if (asc_lh_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value && deviceData.Parameters.LHAsc < 100)//saman
                    {
                        asc_lh_index++;
                        deviceData.Parameters.LHAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[asc_lh_index]);
                    }
                    break;
                case "LHDSCIncrease":
                    if (dsc_lh_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value && deviceData.Parameters.LHDsc < 80)//saman
                    {
                        dsc_lh_index++;
                        deviceData.Parameters.LHDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[dsc_lh_index]);
                    }
                    break;
                case "LKASCIncrease":
                    if (asc_lk_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value)
                    {
                        asc_lk_index++;
                        deviceData.Parameters.LKAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[asc_lk_index]);
                    }
                    break;
                case "LKDSCIncrease":
                    if (dsc_lk_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value)
                    {
                        dsc_lk_index++;
                        deviceData.Parameters.LKDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[dsc_lk_index]);
                    }
                    break;

                case "LHASCDecrease":
                    if (asc_lh_index > 0)
                    {
                        asc_lh_index--;
                        deviceData.Parameters.LHAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[asc_lh_index]);
                    }
                    break;
                case "LHDSCDecrease":
                    if (dsc_lh_index > 0)
                    {
                        dsc_lh_index--;
                        deviceData.Parameters.LHDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[dsc_lh_index]);
                    }
                    break;
                case "LKASCDecrease":
                    if (asc_lk_index > 0)
                    {
                        asc_lk_index--;
                        deviceData.Parameters.LKAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[asc_lk_index]);
                    }
                    break;
                case "LKDSCDecrease":
                    if (dsc_lk_index > 0)
                    {
                        dsc_lk_index--;
                        deviceData.Parameters.LKDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[dsc_lk_index]);
                    }
                    break;
                    ///////////////////////////
                case "RHASCIncrease":
                    if (asc_rh_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value && deviceData.Parameters.RHAsc < 100)
                    {
                        asc_rh_index++;
                        deviceData.Parameters.RHAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[asc_rh_index]);
                    }
                    break;
                case "RHDSCIncrease":
                    if (dsc_rh_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value && deviceData.Parameters.RHDsc <80 )
                    {
                        dsc_rh_index++;
                        deviceData.Parameters.RHDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[dsc_rh_index]);
                    }
                    break;
                case "RKASCIncrease":
                    if (asc_rk_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value)
                    {
                        asc_rk_index++;
                        deviceData.Parameters.RKAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[asc_rk_index]);
                    }
                    break;
                case "RKDSCIncrease":
                    if (dsc_rk_index < XMLParamLoadCheckValid.ASCDSCStairsLength_Value)
                    {
                        dsc_rk_index++;
                        deviceData.Parameters.RKDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[dsc_rk_index]);
                    }
                    break;

                case "RHASCDecrease":
                    if (asc_rh_index > 0)
                    {
                        asc_rh_index--;
                        deviceData.Parameters.RHAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[asc_rh_index]);
                    }
                    break;
                case "RHDSCDecrease":
                    if (dsc_rh_index > 0)
                    {
                        dsc_rh_index--;
                        deviceData.Parameters.RHDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[dsc_rh_index]);
                    }
                    break;
                case "RKASCDecrease":
                    if (asc_rk_index > 0)
                    {
                        asc_rk_index--;
                        deviceData.Parameters.RKAsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[asc_rk_index]);
                    }
                    break;
                case "RKDSCDecrease":
                    if (dsc_rk_index > 0)
                    {
                        dsc_rk_index--;
                        deviceData.Parameters.RKDsc = System.Convert.ToInt16(XMLParamLoadCheckValid.Stairs[dsc_rk_index]);
                    }
                    break;
                case "ShoeLengthIncrease":
                    if (shoe_length_index < XMLParamLoadCheckValid.ShoeLenghtLength_Value)
                    {
                        shoe_length_index++;
                        deviceData.Parameters.ShoeSize = System.Convert.ToInt16(XMLParamLoadCheckValid.ShoeLen[shoe_length_index]);
                    }
                    break;
                case "ShoeLengthDecrease":
                    if (shoe_length_index > 0)
                    {
                        shoe_length_index--;
                        deviceData.Parameters.ShoeSize = System.Convert.ToInt16(XMLParamLoadCheckValid.ShoeLen[shoe_length_index]);
                    }
                    break;

                        /**************************************************Stroke *****************************/
                    case "PFFORCEInc":
                        if (BlueView.ReceiverViewModel.PF_FORCE == 2)
                            BlueView.ReceiverViewModel.PF_FORCE = -1;

                        BlueView.ReceiverViewModel.PF_FORCE++;

                        if (BlueView.ReceiverViewModel.PF_FORCE == 0)
                        {
                            BlueView.ReceiverViewModel.PF_FORCE_String_For_Display = "INACTIVE_MODE(0)";
                        }
                        else if (BlueView.ReceiverViewModel.PF_FORCE == 1)
                        {
                            BlueView.ReceiverViewModel.PF_FORCE_String_For_Display = "PRETENSION_(1)";
                        }
                        else if (BlueView.ReceiverViewModel.PF_FORCE == 2)
                        {
                            BlueView.ReceiverViewModel.PF_FORCE_String_For_Display = "ACTIVE_MODE(2)";
                        }
                        break;
                    case "PFFORCEDec":
                        if (BlueView.ReceiverViewModel.PF_FORCE == 0)
                            BlueView.ReceiverViewModel.PF_FORCE = 1;

                        BlueView.ReceiverViewModel.PF_FORCE--;
                        if (BlueView.ReceiverViewModel.PF_FORCE == 0)
                        {
                            BlueView.ReceiverViewModel.PF_FORCE_String_For_Display = "INACTIVE_MODE(0)";
                        }
                        else if (BlueView.ReceiverViewModel.PF_FORCE == 1)
                        {
                            BlueView.ReceiverViewModel.PF_FORCE_String_For_Display = "PRETENSION_(1)";
                        }
                        else if (BlueView.ReceiverViewModel.PF_FORCE == 2)
                        {
                            BlueView.ReceiverViewModel.PF_FORCE_String_For_Display = "ACTIVE_MODE(2)";
                        }
                        break;
                    case "PFTIMINGInc":
                        if (BlueView.ReceiverViewModel.PF_TIMING == 9999)
                            BlueView.ReceiverViewModel.PF_TIMING = -1;

                        BlueView.ReceiverViewModel.PF_TIMING++;
                        break;
                    case "PFTIMINGDec":
                        if (BlueView.ReceiverViewModel.PF_TIMING == -9999)
                            BlueView.ReceiverViewModel.PF_TIMING = 1;

                        BlueView.ReceiverViewModel.PF_TIMING--;
                        break;
                    case "PFPRETELEVELInc":
                        if (BlueView.ReceiverViewModel.PF_PRETENSION_LEVEL == 9999)
                            BlueView.ReceiverViewModel.PF_PRETENSION_LEVEL = -1;

                        BlueView.ReceiverViewModel.PF_PRETENSION_LEVEL++;

                        break;
                    case "PFPRETELEVELDec":
                        if (BlueView.ReceiverViewModel.PF_PRETENSION_LEVEL == -9999)
                            BlueView.ReceiverViewModel.PF_PRETENSION_LEVEL = 1;

                        BlueView.ReceiverViewModel.PF_PRETENSION_LEVEL--;
                        break;
                    case "DFPULLLENGTHInc":
                        if (BlueView.ReceiverViewModel.DF_PULL_LENGTH == 2)
                            BlueView.ReceiverViewModel.DF_PULL_LENGTH = -1;

                        BlueView.ReceiverViewModel.DF_PULL_LENGTH++;

                       if(BlueView.ReceiverViewModel.DF_PULL_LENGTH == 0)
                        {
                            BlueView.ReceiverViewModel.DF_PULL_LENGTH_String_For_Display = "LEFT_PARIETIC(0)";
                        }
                       else if (BlueView.ReceiverViewModel.DF_PULL_LENGTH == 1)
                        {
                            BlueView.ReceiverViewModel.DF_PULL_LENGTH_String_For_Display = "RIGHT_PARIETIC(1)";
                        }
                       else if (BlueView.ReceiverViewModel.DF_PULL_LENGTH == 2)
                        {
                            BlueView.ReceiverViewModel.DF_PULL_LENGTH_String_For_Display = "NOT_SPECIFIED(2)";
                        }
                        break;
                    case "DFPULLLENGTHDec":
                        if (BlueView.ReceiverViewModel.DF_PULL_LENGTH == 0)
                            BlueView.ReceiverViewModel.DF_PULL_LENGTH = 1;

                        BlueView.ReceiverViewModel.DF_PULL_LENGTH--;
                        if (BlueView.ReceiverViewModel.DF_PULL_LENGTH == 0)
                        {
                            BlueView.ReceiverViewModel.DF_PULL_LENGTH_String_For_Display = "LEFT_PARIETIC(0)";
                        }
                        else if (BlueView.ReceiverViewModel.DF_PULL_LENGTH == 1)
                        {
                            BlueView.ReceiverViewModel.DF_PULL_LENGTH_String_For_Display = "RIGHT_PARIETIC(1)";
                        }
                        else if (BlueView.ReceiverViewModel.DF_PULL_LENGTH == 2)
                        {
                            BlueView.ReceiverViewModel.DF_PULL_LENGTH_String_For_Display = "NOT_SPECIFIED(2)";
                        }
                        break;
                    case "DFTIMINGInc":
                        if (BlueView.ReceiverViewModel.DF_TIMING == 9999)
                            BlueView.ReceiverViewModel.DF_TIMING = -1;

                        BlueView.ReceiverViewModel.DF_TIMING++;
                        break;
                    case "DFTIMINGDec":
                        if (BlueView.ReceiverViewModel.DF_TIMING == -9999)
                            BlueView.ReceiverViewModel.DF_TIMING = 1;

                        BlueView.ReceiverViewModel.DF_TIMING--;
                        break;
                    case "DFPRETELEVELInc":
                        if (BlueView.ReceiverViewModel.DF_PRETENSION_LEVEL == 9999)
                            BlueView.ReceiverViewModel.DF_PRETENSION_LEVEL = -1;

                        BlueView.ReceiverViewModel.DF_PRETENSION_LEVEL++;
                        break;
                    case "DFPRETELEVELDec":
                        if (BlueView.ReceiverViewModel.DF_PRETENSION_LEVEL == -9999)
                            BlueView.ReceiverViewModel.DF_PRETENSION_LEVEL = 1;

                        BlueView.ReceiverViewModel.DF_PRETENSION_LEVEL--;
                        break;



                    default:
                    break;
                   
            }
            InitSystemStrings();
            }


         
        }


        private void BtnViewPlot_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.CheckFileExists = false;
                ofd.RestoreDirectory = true;
                ofd.Filter = "(*.dat)|*.dat|(*.*) |*.* | (*.txt)|*.txt";

                if (ofd.ShowDialog().Value == true)
                {
                  //  tbBrowseViewPlot.Text = ofd.FileName;
                    string retportfile = ofd.FileName;
                    string ReportFileName = ofd.FileName;
                    if (ReportFileName.Length > 33)
                    {
                        tbBrowseViewPlot.Text = ReportFileName.Remove(35);
                        tbBrowseViewPlot.Text = tbBrowseViewPlot.Text + "...";
                    }
                    else
                    {
                        tbBrowseViewPlot.Text = ReportFileName;
                    }
                    string LogFileLocationText = retportfile;// tbBrowseViewPlot.Text;
                    string LogFileNameExtention = Path.GetExtension(LogFileLocationText);
                    if (LogFileNameExtention == ".dat")
                    {
                        LogFileLocationText = LogFileLocationText.TrimEnd("dat".ToCharArray());
                        LogFileLocationText = LogFileLocationText + "txt";

                        LogParser.ParseFile(retportfile, false, LogFileLocationText);


                    }

                    bool TimeStampParserFlag = LogParser.TimeStampFlagIncluded();
                    string LogFileName =  LogFileLocationText;//tbBrowseViewPlot.Text;
                    string TimeStampVariable;
                    string Path_plot_encoder_readings = System.AppDomain.CurrentDomain.BaseDirectory; //Directory.GetCurrentDirectory();
                    string plot_encoder_readings = @Path_plot_encoder_readings + "\\EncoderPlot_py_ver8\\plot_encoder_readings_REV08.exe";

                    if (!TimeStampParserFlag)
                    {
                        TimeStampVariable = "3";
                    }
                    else
                    {
                        TimeStampVariable = "4";
                    }

                    Process.Start(plot_encoder_readings, "\"" + LogFileName + "\"" + " " + TimeStampVariable);
                }

            }

            catch (Exception exception)
            {
                Logger.Error("Encoder Readings Failed", exception.Message);
            }

        }
/*
        private void BtnViewPlot_Click(object sender, RoutedEventArgs e)
        {
             try
              {
                  OpenFileDialog ofd = new OpenFileDialog();
                  ofd.CheckFileExists = false;
                  ofd.RestoreDirectory = true;
                  ofd.Filter = "(*.dat)|*.dat|(*.*) |*.* | (*.txt)|*.txt";

                  if (ofd.ShowDialog().Value == true)
                  {
                      string ReportFileName = ofd.FileName;
                      if (ReportFileName.Length > 33)
                      {
                          tbBrowseViewPlot.Text = ReportFileName.Remove(33);
                          tbBrowseViewPlot.Text = tbBrowseViewPlot.Text + "...";
                      }
                      else
                      {
                          tbBrowseViewPlot.Text = ReportFileName;
                      }





                      string LogFileLocationText = ofd.FileName;// tbBrowseViewPlot.Text;
                      string LogFileNameExtention = Path.GetExtension(LogFileLocationText);
                      if (LogFileNameExtention == ".dat")
                      {
                          LogFileLocationText = LogFileLocationText.TrimEnd("dat".ToCharArray());    //tbBrowseViewPlot.Text.TrimEnd("dat".ToCharArray());
                          LogFileLocationText = LogFileLocationText + "txt";

                          LogParser.ParseFile(tbBrowseViewPlot.Text, false, LogFileLocationText);


                      }

                      bool TimeStampParserFlag = LogParser.TimeStampFlagIncluded();
                      string LogFileName = LogFileLocationText;//tbBrowseViewPlot.Text;
                      string TimeStampVariable;
                      string Path_plot_encoder_readings = System.AppDomain.CurrentDomain.BaseDirectory; //Directory.GetCurrentDirectory();
                      string plot_encoder_readings = @Path_plot_encoder_readings + "\\EncoderPlot_py_ver8\\plot_encoder_readings_REV08.exe";

                      if (!TimeStampParserFlag)
                      {
                          TimeStampVariable = "3";
                      }
                      else
                      {
                          TimeStampVariable = "4";
                      }

                      Process.Start(plot_encoder_readings, "\"" + LogFileName + "\"" + " " + TimeStampVariable);
                  }

              }

              catch (Exception exception)
              {
                  //MessageBox.Show(this, "Encoder Readings Failed " + exception.Message);
                  Logger.Error("Encoder Readings Failed ", exception.Message);
              }
              
        }
*/
        private void SetFielsdsDisplayAsSysType()
        {
            if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
            {

                ChangeSystemTypePIC.IsChecked = true;
                //Hide the follwoing controls
            //    UpperStrapHolders.Visibility = Visibility.Hidden;
            //    KNEEBRACKETLATERAL_stackpanel.Visibility = Visibility.Collapsed;
            //    KNEEBRACKETLATERAL_border.Visibility = Visibility.Hidden;
             //   KNEEBRACKETLATERALButtons.Visibility = Visibility.Hidden;
                //FrontKneeBracketExtraBorder.Visibility = Visibility.Hidden;
                //AboveKneeBracketExtraBorder.Visibility = Visibility.Hidden;
                //UpperStrapHoldersExtraBorder.Visibility = Visibility.Hidden;

            
               
             
            }
            else if(deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
            {
                //Visible the follwoing controls
                ChangeSystemTypePIC.IsChecked = false;
                //UpperStrapHolders.Visibility = Visibility.Visible;
                //KNEEBRACKETLATERAL_stackpanel.Visibility = Visibility.Visible;
                //KNEEBRACKETLATERAL_border.Visibility = Visibility.Visible;
                //KNEEBRACKETLATERALButtons.Visibility = Visibility.Visible;
              //  KNEEBRACKETLATERALColumn2.Visibility = Visibility.Visible;
                //FrontKneeBracketExtraBorder.Visibility = Visibility.Visible;
                //AboveKneeBracketExtraBorder.Visibility = Visibility.Visible;
                //UpperStrapHoldersExtraBorder.Visibility = Visibility.Visible;

               
            }
        }

        private void SetFielsdsDisplayAsSysTypeComboSelect()
        {
            if ((string)cbSysType.SelectedValue == "Rewalk-R" || (string)cbSysType.SelectedValue == "Rewalk-I" || (string)cbSysType.SelectedValue == "Rewalk-P1.0" || (string)cbSysType.SelectedValue == "Rewalk-P")
            {
                //Hide the follwoing controls
                ChangeSystemTypePIC.IsChecked = true;//Change system pictures
               // UpperStrapHolders.Visibility = Visibility.Hidden;
              //  KNEEBRACKETLATERAL_stackpanel.Visibility = Visibility.Hidden;
              //  KNEEBRACKETLATERAL_border.Visibility = Visibility.Hidden;
              //  KNEEBRACKETLATERALButtons.Visibility = Visibility.Hidden;
                //FrontKneeBracketExtraBorder.Visibility = Visibility.Hidden;
                //AboveKneeBracketExtraBorder.Visibility = Visibility.Hidden;
                //UpperStrapHoldersExtraBorder.Visibility = Visibility.Hidden;
                ReturnValueIndex();
                HipFlexion_SelectionChanged();
                InitSystemStringAsSystemTypecomboxSelection();
                InitParamWithUnitsConv();
                
            }
            else if ((string)cbSysType.SelectedValue == "Rewalk-P6.0")
            {
                //Visible the follwoing controls
                ChangeSystemTypePIC.IsChecked = false; //Change system pictures
                //UpperStrapHolders.Visibility = Visibility.Visible;
                //KNEEBRACKETLATERAL_stackpanel.Visibility = Visibility.Visible;
                //KNEEBRACKETLATERAL_border.Visibility = Visibility.Visible;
                //KNEEBRACKETLATERALButtons.Visibility = Visibility.Visible;
                //FrontKneeBracketExtraBorder.Visibility = Visibility.Visible;
                //AboveKneeBracketExtraBorder.Visibility = Visibility.Visible;
                //UpperStrapHoldersExtraBorder.Visibility = Visibility.Visible;
                ReturnValueIndex();
                HipFlexion_SelectionChanged();
                InitSystemStringAsSystemTypecomboxSelection();
                InitParamWithUnitsConv();
            }
        }

        private void OnCollectOnlineDataChanged(object sender, RoutedEventArgs e)
        {

            if (collectOnline.IsChecked == true)
            {
                onlineTimer.Interval = new TimeSpan(0, 0, 0, 0, 200);//fix 200msec
                onlineTimer.Start();
            }
            else
            {
                onlineTimer.Stop();
            }
        }

        private void MetricUnitsSelection(object sender, RoutedEventArgs e)
        {
           // tbWeight_Unit.Text = "kg";
           // tbHeight_Unit.Text = "cm";
          //  tbWeight_Units_2.Text = "kg";
         //   tbHeight_Units_2.Text = "cm";
            deviceData.Parameters.UnitsSelection = 0;
            deviceData.Parameters.Weight = (short)Weight_Metric_On;
            deviceData.Parameters.Height = (short)Height_Metric_On;  
            UnitsSelectButton.Content = (string)FindResource("Units (Metric)");
          
        }

        private void USUnitsSelection(object sender, RoutedEventArgs e)
        {
            tbWeight_Unit.Text = "lb";
            tbHeight_Unit.Text = "inch";
            tbWeight_Units_2.Text = "lb";
        //    tbHeight_Units_2.Text = "inch";
            deviceData.Parameters.UnitsSelection = 1;
            deviceData.Parameters.Weight = (short)Weight_US_On;
            deviceData.Parameters.Height = (short)Height_US_On;
            UnitsSelectButton.Content = (string)FindResource("US units");
        }
       
        
        CheckPrametersValid XMLParamLoadCheckValid = new CheckPrametersValid();

        private void UserParamData_VerfiytoSave(Data deviceData_temp) // parasoft-suppress  METRICS.MCCC "Methods executing atomic functions" METRICS.MLOC "Methods executing atomic functions" CS.MLC "Methods executing atomic functions"
        {
          
           
            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.HipAngle, XMLParamLoadCheckValid.HipFlextionAngleMin_Value, XMLParamLoadCheckValid.HipFlextionAngleMax_Value) == true)
                deviceData.Parameters.HipAngle = deviceData_temp.Parameters.HipAngle;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.KneeAngle, XMLParamLoadCheckValid.KneeFlextionAngleMin_Value, XMLParamLoadCheckValid.KneeFlextionAngleMax_Value) == true)
                deviceData.Parameters.KneeAngle = deviceData_temp.Parameters.KneeAngle;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.BackHipAngle, XMLParamLoadCheckValid.BeginnersStepTimeMin_Value, XMLParamLoadCheckValid.BeginnersStepTimeMax_Value) == true)
                deviceData.Parameters.BackHipAngle = deviceData_temp.Parameters.BackHipAngle;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.MaxVelocity, XMLParamLoadCheckValid.StepTimeMin_Value, XMLParamLoadCheckValid.StepTimeMax_Value) == true)
                deviceData.Parameters.MaxVelocity = deviceData_temp.Parameters.MaxVelocity;//Step time

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.TiltDelta, XMLParamLoadCheckValid.TiltDeltaMin_Value, XMLParamLoadCheckValid.TiltDeltaMax_Value) == true)
                deviceData.Parameters.TiltDelta = deviceData_temp.Parameters.TiltDelta;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.TiltTimeout, XMLParamLoadCheckValid.TiltTimeOutMin_Value, XMLParamLoadCheckValid.TiltTimeOutMax_Value) == true)
                deviceData.Parameters.TiltTimeout = deviceData_temp.Parameters.TiltTimeout;

            if (deviceData_temp.Parameters.SafetyWhileStanding == true || deviceData_temp.Parameters.SafetyWhileStanding == false)
                deviceData.Parameters.SafetyWhileStanding = deviceData_temp.Parameters.SafetyWhileStanding;

            if (deviceData_temp.Parameters.XThreshold != 0)
                deviceData.Parameters.XThreshold = deviceData_temp.Parameters.XThreshold;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.YThreshold, XMLParamLoadCheckValid.FallDetectionThrMin_Value, XMLParamLoadCheckValid.FallDetectionThrMax_Value) == true)
                deviceData.Parameters.YThreshold = deviceData_temp.Parameters.YThreshold;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.DelayBetweenSteps, XMLParamLoadCheckValid.DelayBetweenStepsMin_Value, XMLParamLoadCheckValid.DelayBetweenStepsMax_Value) == true)
                deviceData.Parameters.DelayBetweenSteps = deviceData_temp.Parameters.DelayBetweenSteps;

            if (deviceData_temp.Parameters.BuzzerBetweenSteps == true || deviceData_temp.Parameters.BuzzerBetweenSteps == false)
                deviceData.Parameters.BuzzerBetweenSteps = deviceData_temp.Parameters.BuzzerBetweenSteps;

            if (deviceData_temp.Parameters.Record == true || deviceData_temp.Parameters.Record == false)
                deviceData.Parameters.Record = deviceData_temp.Parameters.Record;//Enable walk sfter stuck

            if (deviceData_temp.Parameters.SwitchFSRs == true || deviceData_temp.Parameters.SwitchFSRs == false)
                deviceData.Parameters.SwitchFSRs = deviceData_temp.Parameters.SwitchFSRs;//Beginner mode enable\disable

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FSRThreshold, XMLParamLoadCheckValid.HipMaxFlextionAngleMin_Value, XMLParamLoadCheckValid.HipMaxFlextionAngleMax_Value) == true)
                deviceData.Parameters.FSRThreshold = deviceData_temp.Parameters.FSRThreshold;//Beginner Hip max Flexion

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FSRWalkTimeout, XMLParamLoadCheckValid.HipFinalFlextionAngleMin_Value, XMLParamLoadCheckValid.HipFinalFlextionAngleMax_Value) == true)
                deviceData.Parameters.FSRWalkTimeout = deviceData_temp.Parameters.FSRWalkTimeout;//Beginner Hip final flexion

             /* Rasem this need special handlinhg **********/        
            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FirstStepFlexion, XMLParamLoadCheckValid.FirststepFlexionMin_Value, XMLParamLoadCheckValid.FirststepFlexionMax_Value) == true)
                deviceData.Parameters.FirstStepFlexion = deviceData_temp.Parameters.FirstStepFlexion;

            if (deviceData_temp.Parameters.Enable_First_Step_Rewalk6 == true || deviceData_temp.Parameters.Enable_First_Step_Rewalk6 == false)
            {
                deviceData.Parameters.Enable_First_Step_Rewalk6 = deviceData_temp.Parameters.Enable_First_Step_Rewalk6;
                if (deviceData.Parameters.Enable_First_Step_Rewalk6 == true)
                {
                    if (deviceData.Parameters.FirstStepFlexion == 0)
                    {
                        deviceData.Parameters.FirstStepFlexion = 7;
                    }
                }
            }
            ///////////////////////// End ////////////////////////////////////////////////
            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.Walk_Current_Threshold, XMLParamLoadCheckValid.WalkCurrentThrMin_Value, XMLParamLoadCheckValid.WalkCurrentThrMax_Value) == true)
                deviceData.Parameters.Walk_Current_Threshold = deviceData_temp.Parameters.Walk_Current_Threshold;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.Stairs_Current_Threshold, XMLParamLoadCheckValid.SystemCurrentThrMin_Value, XMLParamLoadCheckValid.SystemCurrentThrMax_Value) == true)
                deviceData.Parameters.Stairs_Current_Threshold = deviceData_temp.Parameters.Stairs_Current_Threshold;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.LHAsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.LHAsc = deviceData_temp.Parameters.LHAsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.LHDsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.LHDsc = deviceData_temp.Parameters.LHDsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.LKAsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.LKAsc = deviceData_temp.Parameters.LKAsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.LKDsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.LKDsc = deviceData_temp.Parameters.LKDsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.RHAsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.RHAsc = deviceData_temp.Parameters.RHAsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.RHDsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.RHDsc = deviceData_temp.Parameters.RHDsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.RKAsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.RKAsc = deviceData_temp.Parameters.RKAsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.RKDsc, XMLParamLoadCheckValid.ASCDSCStairsMin_Value, XMLParamLoadCheckValid.ASCDSCStairsMax_Value) == true)
                deviceData.Parameters.RKDsc = deviceData_temp.Parameters.RKDsc;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.SwitchingTime, XMLParamLoadCheckValid.SwitchingTimeMin_Value, XMLParamLoadCheckValid.SwitchingTimeMax_Value) == true)
                deviceData.Parameters.SwitchingTime = deviceData_temp.Parameters.SwitchingTime;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.StairHeight, XMLParamLoadCheckValid.StairHeightMin_Value, XMLParamLoadCheckValid.StairHeightMax_Value) == true)
                deviceData.Parameters.StairHeight = deviceData_temp.Parameters.StairHeight;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.ShoeSize, XMLParamLoadCheckValid.ShoeLenghtMin_Value, XMLParamLoadCheckValid.ShoeLenghtMax_Value) == true)
                deviceData.Parameters.ShoeSize = deviceData_temp.Parameters.ShoeSize;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.Placing_RH_on_the_stair, XMLParamLoadCheckValid.PlacingRHMin_Value, XMLParamLoadCheckValid.PlacingRHMax_Value) == true)
                deviceData.Parameters.Placing_RH_on_the_stair = deviceData_temp.Parameters.Placing_RH_on_the_stair;

            if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.Placing_RK_on_the_stair, XMLParamLoadCheckValid.PlacingRKMin_Value, XMLParamLoadCheckValid.PlacingRKMax_Value) == true)
                deviceData.Parameters.Placing_RK_on_the_stair = deviceData_temp.Parameters.Placing_RK_on_the_stair;


            deviceData.Parameters.User_ID = deviceData_temp.Parameters.User_ID;
            deviceData.Parameters.Height = deviceData_temp.Parameters.Height;
            deviceData.Parameters.Weight = deviceData_temp.Parameters.Weight;
            deviceData.Parameters.UnitsSelection = deviceData_temp.Parameters.UnitsSelection;

            //Updating strings of user accroding to indexes

            if (CreateUserStringAsDefualt == false)
            {


                if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.UpperStrapHoldersSize, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PUpperStrapHolderSizeLength_Value)) == true)
                        deviceData.Parameters.UpperStrapHoldersSize = deviceData_temp.Parameters.UpperStrapHoldersSize;
                }

                if (deviceData_temp.Parameters.SysTypeString == "Rewalk-P6.0")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.UpperStrapHoldersPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PUpperStrapHolderTypeLength_Value)) == true)
                        deviceData.Parameters.UpperStrapHoldersPosition = deviceData_temp.Parameters.UpperStrapHoldersPosition;
                }

                if (deviceData_temp.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData_temp.Parameters.SysTypeString == "Rewalk-P6.0")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.AboveKneeBracketSize, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PAboveKneeBracketTypeLength_Value)) == true)
                        deviceData.Parameters.AboveKneeBracketSize = deviceData_temp.Parameters.AboveKneeBracketSize;
                }
                else if (deviceData_temp.Parameters.SysTypeString == "Rewalk-R" || deviceData_temp.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.AboveKneeBracketSize, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.RAboveKneeBracketTypeLength_Value)) == true)
                        deviceData.Parameters.AboveKneeBracketSize = deviceData_temp.Parameters.AboveKneeBracketSize;
                }

                if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.AboveKneeBracketPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PAboveKneeBracketAnteriorLength_Value)) == true)
                        deviceData.Parameters.AboveKneeBracketPosition = deviceData_temp.Parameters.AboveKneeBracketPosition;
                }
                else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.AboveKneeBracketPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.RAboveKneeBracketAnteriorLength_Value)) == true)
                        deviceData.Parameters.AboveKneeBracketPosition = deviceData_temp.Parameters.AboveKneeBracketPosition;
                }

                if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FrontKneeBracketAssemble, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PKneeBracketTypeLength_Value)) == true)
                        deviceData.Parameters.FrontKneeBracketAssemble = deviceData_temp.Parameters.FrontKneeBracketAssemble;
                }
                else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FrontKneeBracketAssemble, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.RKneeBracketTypeLength_Value)) == true)
                        deviceData.Parameters.FrontKneeBracketAssemble = deviceData_temp.Parameters.FrontKneeBracketAssemble;
                }

                if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FrontKneeBracketAnteriorPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PKneeBracketAnteriorPositionLength_Value)) == true)
                        deviceData.Parameters.FrontKneeBracketAnteriorPosition = deviceData_temp.Parameters.FrontKneeBracketAnteriorPosition;
                }
                else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FrontKneeBracketAnteriorPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.RKneeBracketAnteriorPositionLength_Value)) == true)
                        deviceData.Parameters.FrontKneeBracketAnteriorPosition = deviceData_temp.Parameters.FrontKneeBracketAnteriorPosition;
                }

                if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FrontKneeBracketLateralPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PKneeBracketLateralPositionLength_Value)) == true)
                        deviceData.Parameters.FrontKneeBracketLateralPosition = deviceData_temp.Parameters.FrontKneeBracketLateralPosition;
                }

                if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.PelvicSize, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PPelvicSizeLength_Value)) == true)
                        deviceData.Parameters.PelvicSize = deviceData_temp.Parameters.PelvicSize;
                }
                else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.PelvicSize, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PPelvicSizeLength_Value)) == true)
                        deviceData.Parameters.PelvicSize = deviceData_temp.Parameters.PelvicSize;
                }

                if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.PelvicAnteriorPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PPelvicAnteriorPositionLength_Value)) == true)
                        deviceData.Parameters.PelvicAnteriorPosition = deviceData_temp.Parameters.PelvicAnteriorPosition;
                }

                if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.PelvicVerticalPosition, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PPelvicVerticalPositionLength_Value)) == true)
                        deviceData.Parameters.PelvicVerticalPosition = deviceData_temp.Parameters.PelvicVerticalPosition;
                }

                if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.UpperLegHightSize, XMLParamLoadCheckValid.PUpperLegLengthMin_Value, XMLParamLoadCheckValid.PUpperLegLengthMax_Value) == true)
                        deviceData.Parameters.UpperLegHightSize = deviceData_temp.Parameters.UpperLegHightSize;
                }
                else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.UpperLegHightSize, XMLParamLoadCheckValid.RUpperLegLengthMin_Value, XMLParamLoadCheckValid.RUpperLegLengthMax_Value) == true)
                        deviceData.Parameters.UpperLegHightSize = deviceData_temp.Parameters.UpperLegHightSize;
                }

                if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.LowerLegHightSize, XMLParamLoadCheckValid.PLowerLegLengthMin_Value, XMLParamLoadCheckValid.PLowerLegLengthMax_Value) == true)
                        deviceData.Parameters.LowerLegHightSize = deviceData_temp.Parameters.LowerLegHightSize;
                }
                else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.LowerLegHightSize, XMLParamLoadCheckValid.RLowerLegLengthMin_Value, XMLParamLoadCheckValid.RLowerLegLengthMax_Value) == true)
                        deviceData.Parameters.LowerLegHightSize = deviceData_temp.Parameters.LowerLegHightSize;
                }

                if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FootPlateType1, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PFootPlateTypeLength_Value)) == true)
                        deviceData.Parameters.FootPlateType1 = deviceData_temp.Parameters.FootPlateType1;
                }
                else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FootPlateType1, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.RFootPlateTypeLength_Value)) == true)
                        deviceData.Parameters.FootPlateType1 = deviceData_temp.Parameters.FootPlateType1;
                }

                if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FootPlateType2, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PFootPlateSizeLength_Value)) == true)
                        deviceData.Parameters.FootPlateType2 = deviceData_temp.Parameters.FootPlateType2;
                }
                else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(deviceData_temp.Parameters.FootPlateType2, 0, System.Convert.ToInt16(XMLParamLoadCheckValid.RFootPlateSizeLength_Value)) == true)
                        deviceData.Parameters.FootPlateType2 = deviceData_temp.Parameters.FootPlateType2;
                }

                if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(System.Convert.ToInt16(deviceData_temp.Parameters.FootPlateNoOfRotation), 0, System.Convert.ToInt16(XMLParamLoadCheckValid.PFootPlateDorsiFlexionPositionLength_Value)) == true)
                        deviceData.Parameters.FootPlateNoOfRotation = deviceData_temp.Parameters.FootPlateNoOfRotation;
                }
                else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                {
                    if (XMLParamLoadCheckValid.ValidValueofInt16Pram(System.Convert.ToInt16(deviceData_temp.Parameters.FootPlateNoOfRotation), 0, System.Convert.ToInt16(XMLParamLoadCheckValid.RFootPlateDorsiFlexionPositionLength_Value)) == true)
                        deviceData.Parameters.FootPlateNoOfRotation = deviceData_temp.Parameters.FootPlateNoOfRotation;
                }

            }

            if (XMLParamLoadCheckValid.ValidValueofStairsASCInt16Pram(deviceData_temp.Parameters.StairsASCSpeed) == true)
                deviceData.Parameters.StairsASCSpeed = deviceData_temp.Parameters.StairsASCSpeed;

            if (XMLParamLoadCheckValid.ValidValueofStairsDSCInt16Pram(deviceData_temp.Parameters.StairsDSCSpeed) == true)
                deviceData.Parameters.StairsDSCSpeed = deviceData_temp.Parameters.StairsDSCSpeed;

            ReturnValueIndex();
            HipFlexion_SelectionChanged();
            InitSystemStrings();
        }

        private void TechnicianPasswordGo_Click(object sender, RoutedEventArgs e)
        {

            string xword = TechnicianPasswordBox.Password;
  
            if (xword == GetSysPass())
            {
             
                
                if ((short)deviceData.Parameters.DSP == 0x0909)
                {
                    Load.IsEnabled = false;
                    OpenUserfile.IsEnabled = false;
                    ShowPYTH.IsEnabled = false;
                }
                else
                {
                    ShowPYTH.IsEnabled = false;
                    if (USBConnectState == true)
                    {
                        ShowPYTH.IsEnabled = true;
                        Load.IsEnabled = true;
                        OpenUserfile.IsEnabled = true;
                    }
                }
              

                TopToolBar.IsEnabled = true;
                Technician.Visibility = Visibility.Visible;
                Technician.IsSelected = true;
                TopToolBar.Visibility = Visibility.Collapsed;
                UD.Visibility = Visibility.Collapsed;
                EditBtn.Visibility = Visibility.Collapsed;
                TbRightMenu.IsChecked = false;
                if(WarningMessageWasCalled == false) //Error Message Not Warning
                 progressform.btnClearError.Visibility = Visibility.Visible;
                progressform.Focus();
                TechnicianScreenWasEntered = true;
                TechnicianEntrance.Visibility = Visibility.Collapsed;
                PasswordCheckingMessage.Text = "";
                PasswordCheckingMessageStack.Visibility = Visibility.Collapsed;
              //  MenuTechEnter.Visibility = Visibility.Collapsed;
            }
            else
            {
                TechnicianPasswordBox.Password = SetPassBoxEmpty();
                PasswordCheckingMessage.Text = (string)FindResource("Incorrect Password");
                PasswordCheckingMessageStack.Visibility = Visibility.Visible;
                Technician.Visibility = Visibility.Collapsed;
                Technician.IsSelected = false;
                TopToolBar.Visibility = Visibility.Visible;
                TbRightMenu.IsChecked = true;
            //    TechnicianEntrance.Visibility = Visibility.Visible;
            //    MenuTechEnter.Visibility = Visibility.Visible;
            }

            if (Production.IsSelected == true)
            {
                TopToolBar.Visibility = Visibility.Collapsed;
            }
         



           // TechnicianRadioBtn.IsEnabled = (xword == GetSysPass()) ? true : false;

            //  DiagnosticsGrid.Visibility = toolbar.Visibility = technicianGrid.Visibility = debugGrid.Visibility = advancedGrid.Visibility;

       
            //<!--<i:Interaction.Triggers>
            //                <i:EventTrigger EventName="Click">
            //                    <im:ControlStoryboardAction Storyboard="{StaticResource TechnicianEntranceClose}"/>
            //                    <im:ControlStoryboardAction Storyboard="{StaticResource RightMenuClose}"/>
            //                    <ic:ChangePropertyAction TargetName="Technician" PropertyName="IsSelected" Value="True"/>
            //                    <ic:ChangePropertyAction TargetName="TbRightMenu" PropertyName="IsChecked" Value="False"/>
            //                    <ic:ChangePropertyAction TargetName="TbInnerMenu" PropertyName="Visibility"/>
            //                </i:EventTrigger>
            //            </i:Interaction.Triggers>-->
            
            
            //Sync password for two panes
            /*  if (pbTechnician.Password != xword)
                  pbTechnician.Password = xword;
              if (pbAdvanced.Password != xword)
                  pbAdvanced.Password = xword;
              if (pbDebug.Password != xword)
                  pbDebug.Password = xword;
              /* if (pbAFT.Password != xword)
                   pbAFT.Password = xword;*/
        }

        private void Userprofile_Click(object sender, RoutedEventArgs e)
        {


            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = UserProfilesPath;
            Button but = sender as Button;
            if(but !=null)
             ofd.FileName = UserProfilesPath + "\\" + (string)((but).Content) + ".xarg";
            Data deviceData_temp = new Data();
            ofd.CheckFileExists = true;
            ofd.Filter = "Argo Config files (*.xarg)|*.xarg|(*.*) |*.*";
            FileStream fs;
            try
            {
                // if (ofd.ShowDialog().Value == true)
                // {
                using (fs = new FileStream(ofd.FileName, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DeviceParameters));
                    deviceData_temp.Parameters = (DeviceParameters)serializer.Deserialize(fs);
                    if (Temp_ParamData_StructureCopy(deviceData_temp) == false)
                        return;

                }
                if (deviceData_temp.Parameters.SampleTime >= Short_GUIVersion)
                {
                    UserParamData_VerfiytoSave(deviceData_temp);
                    using (fs = new FileStream(ofd.FileName, FileMode.Create))
                    {
                        XmlSerializer serializer_save = new XmlSerializer(typeof(DeviceParameters));
                        deviceData.Parameters.SampleTime = Short_GUIVersion;
                        serializer_save.Serialize(fs, deviceData.Parameters);
                    }
                    fs.Close();
                    SetTitle(Path.GetFileNameWithoutExtension(ofd.FileName));

                }
                else
                {
                    MessageBox.Show(this,(string)this.FindResource("This file was created by an older Rewalk interface version and is not compatible to the current version. In order to continue the operation it will be converted to a compatible format and saved."),
                                           (string)this.FindResource("User file Loading"), MessageBoxButton.OK, MessageBoxImage.Information);
                    deviceData.RestoreParameters();
                    UserParamData_VerfiytoSave(deviceData_temp);
                    // Overwrite the configuration file 
                    using (fs = new FileStream(ofd.FileName, FileMode.Create))
                    {
                        XmlSerializer serializer_save = new XmlSerializer(typeof(DeviceParameters));
                        deviceData.Parameters.SampleTime = Short_GUIVersion;
                        serializer_save.Serialize(fs, deviceData.Parameters);
                    }
                    fs.Close();
                    SetTitle(Path.GetFileNameWithoutExtension(ofd.FileName));
                    // }



                    Write.IsEnabled = true;
                    /*
                    if (deviceData.Parameters.SwitchFSRs == false) //disable Training mode fields if training button is OFF
                    {
                        hip_final.IsEnabled = false;
                        hip_max.IsEnabled = false;
                        t_steptime.IsEnabled = false;
                    }
                    else
                    {
                        hip_final.IsEnabled = true;
                        hip_max.IsEnabled = true;
                        t_steptime.IsEnabled = true;
                    }
                    if (deviceData.Parameters.SafetyWhileStanding == false) //disable safety if SafetyWhileStanding                                   
                        Y_Thr.IsEnabled = false;
                    else
                        Y_Thr.IsEnabled = true;
                    */
                }
            }
            catch (Exception exception)
            {
          
                Logger.Error_MessageBox_Advanced("Load configuration failed: Corrupted configuration file.","",exception.Message);
            }
        }


      

       
            int hx_index ;
            int faldet_index;
            int stct_index ;
            int spt_index;
            int dbets_index;
            int wact_index;
            int fsf_index;
            int kx_index;
            int tiltd_index;
            int tiltt_index;
            int hmaxf_index;
            int hfinf_index;
            int tspt_index;
            int ascsp_index;
            int dscsp_index;
            int asc_lh_index;
            int asc_lk_index;
            int asc_rh_index;
            int asc_rk_index;
            int dsc_lh_index;
            int dsc_lk_index;
            int dsc_rh_index;
            int dsc_rk_index;
            int stairheight_index;
            int pup_leg_length_index;
            int rup_leg_length_index;
            int plow_leg_length_index;
            int rlow_leg_length_index;
            int shoe_length_index;


            public void ReturnValueIndex()
            {
                try
                {
                    hx_index = Array.IndexOf(XMLParamLoadCheckValid.hx, (deviceData.Parameters.HipAngle).ToString());
                    faldet_index = Array.IndexOf(XMLParamLoadCheckValid.Faldet,(deviceData.Parameters.YThreshold).ToString());
                    stct_index = Array.IndexOf(XMLParamLoadCheckValid.Stct, (deviceData.Parameters.Stairs_Current_Threshold).ToString());
                    spt_index = Array.IndexOf(XMLParamLoadCheckValid.Spt, (deviceData.Parameters.MaxVelocity).ToString());
                    dbets_index = Array.IndexOf(XMLParamLoadCheckValid.Dbets,(deviceData.Parameters.DelayBetweenSteps).ToString());
                    wact_index = Array.IndexOf(XMLParamLoadCheckValid.Wact,(deviceData.Parameters.Walk_Current_Threshold).ToString());
                    fsf_index = Array.IndexOf(XMLParamLoadCheckValid.Fsf, (deviceData.Parameters.FirstStepFlexion).ToString());
                    kx_index = Array.IndexOf(XMLParamLoadCheckValid.kx, (deviceData.Parameters.KneeAngle).ToString());
                    tiltd_index = Array.IndexOf(XMLParamLoadCheckValid.Tiltd, (deviceData.Parameters.TiltDelta).ToString());
                    tiltt_index = Array.IndexOf(XMLParamLoadCheckValid.Tiltt, (deviceData.Parameters.TiltTimeout).ToString());
                    hmaxf_index = Array.IndexOf(XMLParamLoadCheckValid.Hmaxf, (deviceData.Parameters.FSRThreshold).ToString());
                    hfinf_index = Array.IndexOf(XMLParamLoadCheckValid.Hfinf, (deviceData.Parameters.FSRWalkTimeout).ToString());
                    tspt_index = Array.IndexOf(XMLParamLoadCheckValid.Tspt, (deviceData.Parameters.BackHipAngle).ToString());
                    ascsp_index = Array.IndexOf(XMLParamLoadCheckValid.Ascsp, (deviceData.Parameters.StairsASCSpeed).ToString());
                    dscsp_index = Array.IndexOf(XMLParamLoadCheckValid.Dscsp, (deviceData.Parameters.StairsDSCSpeed).ToString());
                    stairheight_index = Array.IndexOf(XMLParamLoadCheckValid.Stairheight,(deviceData.Parameters.StairHeight).ToString());
                    asc_lh_index = Array.IndexOf(XMLParamLoadCheckValid.Stairs, (deviceData.Parameters.LHAsc).ToString());
                    asc_lk_index = Array.IndexOf(XMLParamLoadCheckValid.Stairs, (deviceData.Parameters.LKAsc).ToString());
                    asc_rh_index = Array.IndexOf(XMLParamLoadCheckValid.Stairs, (deviceData.Parameters.RHAsc).ToString());
                    asc_rk_index = Array.IndexOf(XMLParamLoadCheckValid.Stairs, (deviceData.Parameters.RKAsc).ToString());
                    dsc_lh_index = Array.IndexOf(XMLParamLoadCheckValid.Stairs, (deviceData.Parameters.LHDsc).ToString());
                    dsc_lk_index = Array.IndexOf(XMLParamLoadCheckValid.Stairs, (deviceData.Parameters.LKDsc).ToString());
                    dsc_rh_index = Array.IndexOf(XMLParamLoadCheckValid.Stairs, (deviceData.Parameters.RHDsc).ToString());
                    dsc_rk_index = Array.IndexOf(XMLParamLoadCheckValid.Stairs, (deviceData.Parameters.RKDsc).ToString());
                    pup_leg_length_index = Array.IndexOf(XMLParamLoadCheckValid.PUpLegLen, (deviceData.Parameters.UpperLegHightSize).ToString());
                    rup_leg_length_index = Array.IndexOf(XMLParamLoadCheckValid.RUpLegLen,(deviceData.Parameters.UpperLegHightSize).ToString());
                    plow_leg_length_index = Array.IndexOf(XMLParamLoadCheckValid.PLowLegLen,(deviceData.Parameters.LowerLegHightSize).ToString());
                    rlow_leg_length_index = Array.IndexOf(XMLParamLoadCheckValid.RLowLegLen, (deviceData.Parameters.LowerLegHightSize).ToString());
                    shoe_length_index = Array.IndexOf(XMLParamLoadCheckValid.ShoeLen, (deviceData.Parameters.ShoeSize).ToString());
                }
                catch (Exception exception)
                {

                    Logger.Error_MessageBox_Advanced("Finding Current Index was failed","Finding Current Index", exception.Message);

                }

            }

            private void InitSystemStrings() // parasoft-suppress  METRICS.MCCC "Methods executing atomic functions"
            {
      
                try
                {
                    if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.UpperStrapHolderSizeString = XMLParamLoadCheckValid.PUpStrapSize[deviceData.Parameters.UpperStrapHoldersSize];
                        deviceData.Parameters.UpperStrapHolderTypeString = XMLParamLoadCheckValid.PUpStrapType[deviceData.Parameters.UpperStrapHoldersPosition];
                    }
                    else if(deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        deviceData.Parameters.UpperStrapHolderSizeString = "N/A";
                        deviceData.Parameters.UpperStrapHolderTypeString = "N/A";
                    }

                    if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.AboveKneeBracketTypeString = XMLParamLoadCheckValid.PAKBType[deviceData.Parameters.AboveKneeBracketSize];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        deviceData.Parameters.AboveKneeBracketTypeString = XMLParamLoadCheckValid.RAKBType[deviceData.Parameters.AboveKneeBracketSize];
                    }

                    if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.AboveKneeBracketAnteriorString = XMLParamLoadCheckValid.PAKBAnterior[deviceData.Parameters.AboveKneeBracketPosition];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        deviceData.Parameters.AboveKneeBracketAnteriorString = XMLParamLoadCheckValid.RAKBAnterior[deviceData.Parameters.AboveKneeBracketPosition];
                    }

                    if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.KneeBracketTypeString = XMLParamLoadCheckValid.PKBType[deviceData.Parameters.FrontKneeBracketAssemble];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        deviceData.Parameters.KneeBracketTypeString = XMLParamLoadCheckValid.RKBType[deviceData.Parameters.FrontKneeBracketAssemble];
                    }

                    if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.KneeBracketAnteriorPositionString = XMLParamLoadCheckValid.PKBAnteriorPos[deviceData.Parameters.FrontKneeBracketAnteriorPosition];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        deviceData.Parameters.KneeBracketAnteriorPositionString = XMLParamLoadCheckValid.RKBAnteriorPos[deviceData.Parameters.FrontKneeBracketAnteriorPosition];
                    }

                    if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.KneeBracketLateralPositionString = XMLParamLoadCheckValid.PKBLateralPos[deviceData.Parameters.FrontKneeBracketLateralPosition];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        deviceData.Parameters.KneeBracketLateralPositionString = "N/A";
                    }

                    if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.PelvicSizeString = XMLParamLoadCheckValid.PPelvicSize[deviceData.Parameters.PelvicSize];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        deviceData.Parameters.PelvicSizeString = XMLParamLoadCheckValid.RPelvicSize[deviceData.Parameters.PelvicSize];
                    }

                    if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.PelvicAnteriorPositionString = XMLParamLoadCheckValid.PPelvicAnteriorPos[deviceData.Parameters.PelvicAnteriorPosition];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        deviceData.Parameters.PelvicAnteriorPositionString = "N/A";
                    }

                    if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.PelvicVerticalPositionString = XMLParamLoadCheckValid.PPelvicVerticalPos[deviceData.Parameters.PelvicVerticalPosition];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        deviceData.Parameters.PelvicVerticalPositionString = "N/A";
                    }

                    if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.FootPlateTypeString = XMLParamLoadCheckValid.PFPlateType[deviceData.Parameters.FootPlateType1];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        deviceData.Parameters.FootPlateTypeString = XMLParamLoadCheckValid.RFPlateType[deviceData.Parameters.FootPlateType1];
                    }

                    if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.FootPlateSizeString = XMLParamLoadCheckValid.PFPlateSize[deviceData.Parameters.FootPlateType2];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        deviceData.Parameters.FootPlateSizeString = XMLParamLoadCheckValid.RFPlateSize[deviceData.Parameters.FootPlateType2];
                    }

                    if ( deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.FootPlateDorsiFlexionPositionString = XMLParamLoadCheckValid.PFPDorsiFlexPos[deviceData.Parameters.FootPlateNoOfRotation];
                    }
                    else if (deviceData.Parameters.SysTypeString == "Rewalk-R" || deviceData.Parameters.SysTypeString == "Rewalk-I" || deviceData.Parameters.SysTypeString == "Rewalk-P1.0" || deviceData.Parameters.SysTypeString == "Rewalk-P")
                    {
                        deviceData.Parameters.FootPlateDorsiFlexionPositionString = XMLParamLoadCheckValid.RFPDorsiFlexPos[deviceData.Parameters.FootPlateNoOfRotation];
                    }
              
                  
                    int ASCSpeed = Array.IndexOf(XMLParamLoadCheckValid.Ascsp, deviceData.Parameters.StairsASCSpeed.ToString());
                    deviceData.Parameters.ASCSpeedString = XMLParamLoadCheckValid.ASCSpeedStrings[ASCSpeed];

                    int DSCSpeed = Array.IndexOf(XMLParamLoadCheckValid.Dscsp, deviceData.Parameters.StairsDSCSpeed.ToString());
                    deviceData.Parameters.DSCSpeedString = XMLParamLoadCheckValid.DSCSpeedStrings[DSCSpeed];
                }
                catch (Exception exception)
                {
                    Logger.Error_MessageBox_Advanced("System measurements initialization has failed." + "\n" + "Perform Restore and then Load to Rewalk to continue.", "Intialiazing System Configuration", exception.Message);
                
                }
                ReBuildStrings();
            }
            private void InitSystemStringAsSystemTypecomboxSelection() // parasoft-suppress  METRICS.MCCC "Methods executing atomic functions"
            {

                try
                {
                    if ((string)cbSysType.SelectedValue == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.UpperStrapHolderSizeString = XMLParamLoadCheckValid.PUpStrapSize[deviceData.Parameters.UpperStrapHoldersSize];
                        deviceData.Parameters.UpperStrapHolderTypeString = XMLParamLoadCheckValid.PUpStrapType[deviceData.Parameters.UpperStrapHoldersPosition];
                    }
                    else if ((string)cbSysType.SelectedValue == "Rewalk-R" || (string)cbSysType.SelectedValue == "Rewalk-I" || (string)cbSysType.SelectedValue == "Rewalk-P1.0" || (string)cbSysType.SelectedValue == "Rewalk-P")
                    {
                        deviceData.Parameters.UpperStrapHolderSizeString = "N/A";
                        deviceData.Parameters.UpperStrapHolderTypeString = "N/A";
                    }

                    if ((string)cbSysType.SelectedValue == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.AboveKneeBracketTypeString = XMLParamLoadCheckValid.PAKBType[deviceData.Parameters.AboveKneeBracketSize];
                    }
                    else if ((string)cbSysType.SelectedValue == "Rewalk-R" || (string)cbSysType.SelectedValue == "Rewalk-I" || (string)cbSysType.SelectedValue == "Rewalk-P1.0" || (string)cbSysType.SelectedValue == "Rewalk-P")
                    {
                        deviceData.Parameters.AboveKneeBracketTypeString = XMLParamLoadCheckValid.RAKBType[deviceData.Parameters.AboveKneeBracketSize];
                    }

                    if ((string)cbSysType.SelectedValue == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.AboveKneeBracketAnteriorString = XMLParamLoadCheckValid.PAKBAnterior[deviceData.Parameters.AboveKneeBracketPosition];
                    }
                    else if ((string)cbSysType.SelectedValue == "Rewalk-R" || (string)cbSysType.SelectedValue == "Rewalk-I" || (string)cbSysType.SelectedValue == "Rewalk-P1.0" || (string)cbSysType.SelectedValue == "Rewalk-P")
                    {
                        deviceData.Parameters.AboveKneeBracketAnteriorString = XMLParamLoadCheckValid.RAKBAnterior[deviceData.Parameters.AboveKneeBracketPosition];
                    }

                    if ((string)cbSysType.SelectedValue == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.KneeBracketTypeString = XMLParamLoadCheckValid.PKBType[deviceData.Parameters.FrontKneeBracketAssemble];
                    }
                    else if ((string)cbSysType.SelectedValue == "Rewalk-R" || (string)cbSysType.SelectedValue == "Rewalk-I" || (string)cbSysType.SelectedValue == "Rewalk-P1.0" || (string)cbSysType.SelectedValue == "Rewalk-P")
                    {
                        deviceData.Parameters.KneeBracketTypeString = XMLParamLoadCheckValid.RKBType[deviceData.Parameters.FrontKneeBracketAssemble];
                    }

                    if ((string)cbSysType.SelectedValue == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.KneeBracketAnteriorPositionString = XMLParamLoadCheckValid.PKBAnteriorPos[deviceData.Parameters.FrontKneeBracketAnteriorPosition];
                    }
                    else if ((string)cbSysType.SelectedValue == "Rewalk-R" || (string)cbSysType.SelectedValue == "Rewalk-I" || (string)cbSysType.SelectedValue == "Rewalk-P1.0" || (string)cbSysType.SelectedValue == "Rewalk-P")
                    {
                        deviceData.Parameters.KneeBracketAnteriorPositionString = XMLParamLoadCheckValid.RKBAnteriorPos[deviceData.Parameters.FrontKneeBracketAnteriorPosition];
                    }

                    if ((string)cbSysType.SelectedValue == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.KneeBracketLateralPositionString = XMLParamLoadCheckValid.PKBLateralPos[deviceData.Parameters.FrontKneeBracketLateralPosition];
                    }
                    else if ((string)cbSysType.SelectedValue == "Rewalk-R" || (string)cbSysType.SelectedValue == "Rewalk-I" || (string)cbSysType.SelectedValue == "Rewalk-P1.0" || (string)cbSysType.SelectedValue == "Rewalk-P")
                    {
                        deviceData.Parameters.KneeBracketLateralPositionString = "N/A";
                    }

                    if ((string)cbSysType.SelectedValue == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.PelvicSizeString = XMLParamLoadCheckValid.PPelvicSize[deviceData.Parameters.PelvicSize];
                    }
                    else if ((string)cbSysType.SelectedValue == "Rewalk-R" || (string)cbSysType.SelectedValue == "Rewalk-I" || (string)cbSysType.SelectedValue == "Rewalk-P1.0" || (string)cbSysType.SelectedValue == "Rewalk-P")
                    {
                        deviceData.Parameters.PelvicSizeString = XMLParamLoadCheckValid.RPelvicSize[deviceData.Parameters.PelvicSize];
                    }

                    if ((string)cbSysType.SelectedValue == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.PelvicAnteriorPositionString = XMLParamLoadCheckValid.PPelvicAnteriorPos[deviceData.Parameters.PelvicAnteriorPosition];
                    }
                    else if ((string)cbSysType.SelectedValue == "Rewalk-R" || (string)cbSysType.SelectedValue == "Rewalk-I" || (string)cbSysType.SelectedValue == "Rewalk-P1.0" || (string)cbSysType.SelectedValue == "Rewalk-P")
                    {
                        deviceData.Parameters.PelvicAnteriorPositionString = "N/A";
                    }

                    if ((string)cbSysType.SelectedValue == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.PelvicVerticalPositionString = XMLParamLoadCheckValid.PPelvicVerticalPos[deviceData.Parameters.PelvicVerticalPosition];
                    }
                    else if ((string)cbSysType.SelectedValue == "Rewalk-R" || (string)cbSysType.SelectedValue == "Rewalk-I" || (string)cbSysType.SelectedValue == "Rewalk-P1.0" || (string)cbSysType.SelectedValue == "Rewalk-P")
                    {
                        deviceData.Parameters.PelvicVerticalPositionString = "N/A";
                    }

                    if ((string)cbSysType.SelectedValue == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.FootPlateTypeString = XMLParamLoadCheckValid.PFPlateType[deviceData.Parameters.FootPlateType1];
                    }
                    else if ((string)cbSysType.SelectedValue == "Rewalk-R" || (string)cbSysType.SelectedValue == "Rewalk-I" || (string)cbSysType.SelectedValue == "Rewalk-P1.0" || (string)cbSysType.SelectedValue == "Rewalk-P")
                    {
                        deviceData.Parameters.FootPlateTypeString = XMLParamLoadCheckValid.RFPlateType[deviceData.Parameters.FootPlateType1];
                    }

                    if ((string)cbSysType.SelectedValue == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.FootPlateSizeString = XMLParamLoadCheckValid.PFPlateSize[deviceData.Parameters.FootPlateType2];
                    }
                    else if ((string)cbSysType.SelectedValue == "Rewalk-R" || (string)cbSysType.SelectedValue == "Rewalk-I" || (string)cbSysType.SelectedValue == "Rewalk-P1.0" || (string)cbSysType.SelectedValue == "Rewalk-P")
                    {
                        deviceData.Parameters.FootPlateSizeString = XMLParamLoadCheckValid.RFPlateSize[deviceData.Parameters.FootPlateType2];
                    }

                    if (deviceData.Parameters.SysTypeString == "Rewalk-P6.0")
                    {
                        deviceData.Parameters.FootPlateDorsiFlexionPositionString = XMLParamLoadCheckValid.PFPDorsiFlexPos[deviceData.Parameters.FootPlateNoOfRotation];
                    }
                    else if ((string)cbSysType.SelectedValue == "Rewalk-R" || (string)cbSysType.SelectedValue == "Rewalk-I" || (string)cbSysType.SelectedValue == "Rewalk-P1.0" || (string)cbSysType.SelectedValue == "Rewalk-P")
                    {
                        deviceData.Parameters.FootPlateDorsiFlexionPositionString = XMLParamLoadCheckValid.RFPDorsiFlexPos[deviceData.Parameters.FootPlateNoOfRotation];
                    }
               

                    int ASCSpeed = Array.IndexOf(XMLParamLoadCheckValid.Ascsp, deviceData.Parameters.StairsASCSpeed.ToString());
                    deviceData.Parameters.ASCSpeedString = XMLParamLoadCheckValid.ASCSpeedStrings[ASCSpeed];

                    int DSCSpeed = Array.IndexOf(XMLParamLoadCheckValid.Dscsp, deviceData.Parameters.StairsDSCSpeed.ToString());
                    deviceData.Parameters.DSCSpeedString = XMLParamLoadCheckValid.DSCSpeedStrings[DSCSpeed];
                }
                catch (Exception exception)
                {
                    Logger.Error_MessageBox_Advanced("System measurements initialization has failed." + "\n" + "Perform Restore and then Load to Rewalk to continue.", "Intialiazing System Configuration", exception.Message);

                }
                ReBuildStrings();
            }

            private void Detect_killRewalkRunningApllication()
            {
                System.Array Instances_Rewalk6;
                System.Array Instances_MyRewalk;
              //  System.Array Instances_Rewalk;
                //  Instances = System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location));
                Instances_Rewalk6 = System.Diagnostics.Process.GetProcessesByName("ReWalk");// (System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location));
                Instances_MyRewalk = System.Diagnostics.Process.GetProcessesByName("MyReWalk");// (System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location));
            //    Instances_Rewalk = System.Diagnostics.Process.GetProcessesByName("Rewalk");// (System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location));

                if (Instances_Rewalk6.Length > 1 || Instances_MyRewalk.Length >= 1 )
                {
                    MessageBoxResult result = MessageBox.Show(this, (string)this.FindResource("Another instance of the Rewalk application is already running.") + "\n", (string)this.FindResource("Warning_"), MessageBoxButton.OK, MessageBoxImage.Hand);
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
            }



     /////////////////////////  Translation ///////////////////////////////////////

            private void ReBuildStrings()
            {
                deviceData.Parameters.StairsASCSpeed = deviceData.Parameters.StairsASCSpeed;
                deviceData.Parameters.StairsDSCSpeed = deviceData.Parameters.StairsDSCSpeed;
                deviceData.Parameters.UpperStrapHoldersSize = deviceData.Parameters.UpperStrapHoldersSize;
                deviceData.Parameters.UpperStrapHoldersPosition = deviceData.Parameters.UpperStrapHoldersPosition;
                deviceData.Parameters.AboveKneeBracketSize = deviceData.Parameters.AboveKneeBracketSize;
                deviceData.Parameters.AboveKneeBracketPosition = deviceData.Parameters.AboveKneeBracketPosition;
                deviceData.Parameters.FrontKneeBracketAssemble = deviceData.Parameters.FrontKneeBracketAssemble;
                deviceData.Parameters.FrontKneeBracketAnteriorPosition = deviceData.Parameters.FrontKneeBracketAnteriorPosition;
                deviceData.Parameters.FrontKneeBracketLateralPosition = deviceData.Parameters.FrontKneeBracketLateralPosition;
                deviceData.Parameters.PelvicSize = deviceData.Parameters.PelvicSize;
                deviceData.Parameters.PelvicAnteriorPosition = deviceData.Parameters.PelvicAnteriorPosition;
                deviceData.Parameters.PelvicVerticalPosition = deviceData.Parameters.PelvicVerticalPosition;
                deviceData.Parameters.FootPlateType1 = deviceData.Parameters.FootPlateType1;
                deviceData.Parameters.FootPlateType2 = deviceData.Parameters.FootPlateType2;
                deviceData.Parameters.FootPlateNoOfRotation = deviceData.Parameters.FootPlateNoOfRotation;

            }

            private void English_Checked(object sender, RoutedEventArgs e)
            {
                ResourceDictionary myResourceDictionary = new ResourceDictionary();
            //    myResourceDictionary.Source = new Uri(("..\\Translation\\StringResources.xaml"), UriKind.Relative);
                myResourceDictionary.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "/Translation/StringResources-enEN.xaml", UriKind.RelativeOrAbsolute);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                LanguageSelectButton.Content = "Language (English)";
                Properties.Settings.Default.Language_Selection = "English";
                Properties.Settings.Default.Save();
                if(deviceData.Parameters.UnitsSelection == 0)
                    UnitsSelectButton.Content = (string)FindResource("Units (Metric)");
                else if(deviceData.Parameters.UnitsSelection == 1)
                    UnitsSelectButton.Content = (string)FindResource("US units");
                TbRightMenu.IsChecked = false;
                ReBuildStrings();
                EditBtn.Content = (string)FindResource("Edit");
                MAX_HIP_FLEXION.FontSize = 14;
                KNEE_FLEXION.FontSize = 14;
                HIP_FLEXION.FontSize = 14;
                HIP_FINAL_FLEXION.FontSize = 14;
                CL.FontSize = 16;
                CL_Open.FontSize = 16;
                PelvicVerPos.FontSize = 15;
                KneeAnteriorPos.FontSize = 15;
                AboveKneeBracketValuString.FontSize = 18;
                cbEnableStairs.FontSize = 14;
                FootPlatesize.FontSize = 18;
                DorseFlexxionText.FontSize = 14;
                string appPath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                if (GUIVersionType == "USA")
                {
                    HelpFileName = appPath + "\\TherapistManual_English\\TherapistManualUSA.pdf";
                }
                else if (GUIVersionType == "Other")
                {
                    HelpFileName = appPath + "\\TherapistManual_English\\TherapistManualOther.pdf";
                }

               
              
                Thickness margin = LowerLegLength.Margin;
                margin.Right = 144;
                LowerLegLength.Margin = margin;
            }

            private void Deutsch_Checked(object sender, RoutedEventArgs e)
            {

                ResourceDictionary myResourceDictionary = new ResourceDictionary();
             //   myResourceDictionary.Source = new Uri("..\\Translation\\StringResources-deDE.xaml", UriKind.Relative);
                myResourceDictionary.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "/Translation/StringResources-deDE.xaml", UriKind.RelativeOrAbsolute);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                LanguageSelectButton.Content = "Language (Deutsch)";
                Properties.Settings.Default.Language_Selection = "Deutsch";
                Properties.Settings.Default.Save();
                if (deviceData.Parameters.UnitsSelection == 0)
                    UnitsSelectButton.Content = (string)FindResource("Units (Metric)");
                else if (deviceData.Parameters.UnitsSelection == 1)
                    UnitsSelectButton.Content = (string)FindResource("US units");
                TbRightMenu.IsChecked = false;
                ReBuildStrings();
                EditBtn.Content = (string)FindResource("Edit");
                MAX_HIP_FLEXION.FontSize = 14;
                KNEE_FLEXION.FontSize = 14;
                HIP_FLEXION.FontSize = 14;
                HIP_FINAL_FLEXION.FontSize = 14;
                CL.FontSize = 14;
                CL_Open.FontSize = 14;
                CL.FontSize = 11;
                CL_Open.FontSize = 11;
                PelvicVerPos.FontSize = 15;
                KneeAnteriorPos.FontSize = 14;
                AboveKneeBracketValuString.FontSize = 18;
                cbEnableStairs.FontSize = 14;
                FootPlatesize.FontSize = 18;
                DorseFlexxionText.FontSize = 14;
                string appPath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                if (GUIVersionType == "USA")
                {
                    HelpFileName = appPath + "\\TherapistManual_German\\TherapistManualUSA.pdf";
                }
                else if (GUIVersionType == "Other")
                {
                    HelpFileName = appPath + "\\TherapistManual_German\\TherapistManualOther.pdf";
                }
                Thickness margin = LowerLegLength.Margin;
                margin.Right = 144;
                LowerLegLength.Margin = margin;
            }

            private void Français_Checked(object sender, RoutedEventArgs e)
            {
                ResourceDictionary myResourceDictionary = new ResourceDictionary();
             //   myResourceDictionary.Source = new Uri("..\\Translation\\StringResources-frFR.xaml", UriKind.Relative);
                myResourceDictionary.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "/Translation/StringResources-frFR.xaml", UriKind.RelativeOrAbsolute);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                LanguageSelectButton.Content = "Language (Français)";
                Properties.Settings.Default.Language_Selection = "Français";
                Properties.Settings.Default.Save();
                if (deviceData.Parameters.UnitsSelection == 0)
                    UnitsSelectButton.Content = (string)FindResource("Units (Metric)");
                else if (deviceData.Parameters.UnitsSelection == 1)
                    UnitsSelectButton.Content = (string)FindResource("US units");
                TbRightMenu.IsChecked = false;
                ReBuildStrings();
                EditBtn.Content = (string)FindResource("Edit");
                MAX_HIP_FLEXION.FontSize = 13;
                KNEE_FLEXION.FontSize = 13;
                HIP_FLEXION.FontSize = 13;
                HIP_FINAL_FLEXION.FontSize = 13;
                CL.FontSize = 12;
                CL_Open.FontSize = 12;
                PelvicVerPos.FontSize = 15;
                KneeAnteriorPos.FontSize = 15;
                AboveKneeBracketValuString.FontSize = 18;
                cbEnableStairs.FontSize = 14;
                FootPlatesize.FontSize = 18;
                DorseFlexxionText.FontSize = 14;
                string appPath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                if (GUIVersionType == "USA")
                {
                    HelpFileName = appPath + "\\TherapistManual_France\\TherapistManualUSA.pdf";
                }
                else if (GUIVersionType == "Other")
                {
                    HelpFileName = appPath + "\\TherapistManual_France\\TherapistManualOther.pdf";
                }
                Thickness margin = LowerLegLength.Margin;
                margin.Right = 70;
                LowerLegLength.Margin = margin;
            }

            private void Italiano_Checked(object sender, RoutedEventArgs e)
            {
                ResourceDictionary myResourceDictionary = new ResourceDictionary();
            //    myResourceDictionary.Source = new Uri("..\\Translation\\StringResources-itIT.xaml", UriKind.Relative);
                myResourceDictionary.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "/Translation/StringResources-itIT.xaml", UriKind.RelativeOrAbsolute);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                LanguageSelectButton.Content = "Language (Italiano)";
                Properties.Settings.Default.Language_Selection = "Italiano";
                Properties.Settings.Default.Save();
                if (deviceData.Parameters.UnitsSelection == 0)
                    UnitsSelectButton.Content = (string)FindResource("Units (Metric)");
                else if (deviceData.Parameters.UnitsSelection == 1)
                    UnitsSelectButton.Content = (string)FindResource("US units");
                TbRightMenu.IsChecked = false;
                ReBuildStrings();
                EditBtn.Content = (string)FindResource("Edit");
                MAX_HIP_FLEXION.FontSize = 14;
                KNEE_FLEXION.FontSize = 14;
                HIP_FLEXION.FontSize = 14;
                HIP_FINAL_FLEXION.FontSize = 14;
                CL.FontSize = 14;
                CL_Open.FontSize = 14;
                PelvicVerPos.FontSize = 15;
                KneeAnteriorPos.FontSize = 15;
                AboveKneeBracketValuString.FontSize = 18;
                cbEnableStairs.FontSize = 14;
                FootPlatesize.FontSize = 18;
                DorseFlexxionText.FontSize = 12;
                string appPath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                if (GUIVersionType == "USA")
                {
                    HelpFileName = appPath + "\\TherapistManual_Italian\\TherapistManualUSA.pdf";
                }
                else if (GUIVersionType == "Other")
                {
                    HelpFileName = appPath + "\\TherapistManual_Italian\\TherapistManualOther.pdf";
                }
                Thickness margin = LowerLegLength.Margin;
                margin.Right = 144;
                LowerLegLength.Margin = margin;
            }

            private void Türkçe_Checked(object sender, RoutedEventArgs e)
            {
                ResourceDictionary myResourceDictionary = new ResourceDictionary();
         //       myResourceDictionary.Source = new Uri("..\\Translation\\StringResources-trTR.xaml", UriKind.Relative);
                myResourceDictionary.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "/Translation/StringResources-trTR.xaml", UriKind.RelativeOrAbsolute);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                LanguageSelectButton.Content = "Language (Türkçe)";
                Properties.Settings.Default.Language_Selection = "Türkçe";
                Properties.Settings.Default.Save();
                if (deviceData.Parameters.UnitsSelection == 0)
                    UnitsSelectButton.Content = (string)FindResource("Units (Metric)");
                else if (deviceData.Parameters.UnitsSelection == 1)
                    UnitsSelectButton.Content = (string)FindResource("US units");
                TbRightMenu.IsChecked = false;
                ReBuildStrings();
                EditBtn.Content = (string)FindResource("Edit");
                MAX_HIP_FLEXION.FontSize = 14;
                KNEE_FLEXION.FontSize = 14;
                HIP_FLEXION.FontSize = 14;
                HIP_FINAL_FLEXION.FontSize = 14;
                CL.FontSize = 14;
                CL_Open.FontSize = 14;
                PelvicVerPos.FontSize = 15;
                KneeAnteriorPos.FontSize = 15;
                AboveKneeBracketValuString.FontSize = 18;
                cbEnableStairs.FontSize = 14;
                FootPlatesize.FontSize = 18;
                DorseFlexxionText.FontSize = 14;
                string appPath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                if (GUIVersionType == "USA")
                {
                    HelpFileName = appPath + "\\TherapistManual_Turkish\\TherapistManualUSA.pdf";
                }
                else if (GUIVersionType == "Other")
                {
                    HelpFileName = appPath + "\\TherapistManual_Turkish\\TherapistManualOther.pdf";
                }
                Thickness margin = LowerLegLength.Margin;
                margin.Right = 144;
                LowerLegLength.Margin = margin;
            }

            private void 日本語_Checked(object sender, RoutedEventArgs e)
            {
                ResourceDictionary myResourceDictionary = new ResourceDictionary();
           //     myResourceDictionary.Source = new Uri("..\\Translation\\StringResources-jpJA.xaml", UriKind.Relative);
                myResourceDictionary.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "/Translation/StringResources-jpJA.xaml", UriKind.RelativeOrAbsolute);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                LanguageSelectButton.Content = "Language (日本語)";
                Properties.Settings.Default.Language_Selection = "日本語";
                Properties.Settings.Default.Save();
                if (deviceData.Parameters.UnitsSelection == 0)
                    UnitsSelectButton.Content = (string)FindResource("Units (Metric)");
                else if (deviceData.Parameters.UnitsSelection == 1)
                    UnitsSelectButton.Content = (string)FindResource("US units");
                TbRightMenu.IsChecked = false;
                ReBuildStrings();
                EditBtn.Content = (string)FindResource("Edit");
                MAX_HIP_FLEXION.FontSize = 14;
                KNEE_FLEXION.FontSize = 14;
                HIP_FLEXION.FontSize = 14;
                HIP_FINAL_FLEXION.FontSize = 14;
                CL.FontSize = 14;
                CL_Open.FontSize = 14;
                PelvicVerPos.FontSize = 15;
                KneeAnteriorPos.FontSize = 15;
                AboveKneeBracketValuString.FontSize = 18;
                cbEnableStairs.FontSize = 14;
                FootPlatesize.FontSize = 18;
                DorseFlexxionText.FontSize = 14;
                string appPath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                if (GUIVersionType == "USA")
                {
                    HelpFileName = appPath + "\\TherapistManual_Japan\\TherapistManualUSA.pdf";
                }
                else if (GUIVersionType == "Other")
                {
                    HelpFileName = appPath + "\\TherapistManual_Japan\\TherapistManualOther.pdf";
                }
                Thickness margin = LowerLegLength.Margin;
                margin.Right = 144;
                LowerLegLength.Margin = margin;
            }


            private void Español_Checked(object sender, RoutedEventArgs e)
            {
                ResourceDictionary myResourceDictionary = new ResourceDictionary();
              //  myResourceDictionary.Source = new Uri("..\\Translation\\StringResources-esES.xaml", UriKind.Relative);
                myResourceDictionary.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "/Translation/StringResources-esES.xaml", UriKind.RelativeOrAbsolute);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                LanguageSelectButton.Content = "Language (Español)";
                Properties.Settings.Default.Language_Selection = "Español";
                Properties.Settings.Default.Save();
                if (deviceData.Parameters.UnitsSelection == 0)
                    UnitsSelectButton.Content = (string)FindResource("Units (Metric)");
                else if (deviceData.Parameters.UnitsSelection == 1)
                    UnitsSelectButton.Content = (string)FindResource("US units");
                TbRightMenu.IsChecked = false;
                ReBuildStrings();
                EditBtn.Content = (string)FindResource("Edit");
                MAX_HIP_FLEXION.FontSize = 14;
                KNEE_FLEXION.FontSize = 14;
                HIP_FLEXION.FontSize = 14;
                HIP_FINAL_FLEXION.FontSize = 14;
                CL.FontSize = 14;
                CL_Open.FontSize = 14;
                PelvicVerPos.FontSize = 15;
                KneeAnteriorPos.FontSize = 15;
                AboveKneeBracketValuString.FontSize = 18;
                cbEnableStairs.FontSize = 14;
                FootPlatesize.FontSize = 18;
                DorseFlexxionText.FontSize = 14;
                string appPath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                if (GUIVersionType == "USA")
                {
                    HelpFileName = appPath + "\\TherapistManual_Spanish\\TherapistManualUSA.pdf";
                }
                else if (GUIVersionType == "Other")
                {
                    HelpFileName = appPath + "\\TherapistManual_Spanish\\TherapistManualOther.pdf";
                }
                Thickness margin = LowerLegLength.Margin;
                margin.Right = 144;
                LowerLegLength.Margin = margin;
            }
            private void Pусский_Checked(object sender, RoutedEventArgs e)
            {
                var myResourceDictionary = new ResourceDictionary();
            //    myResourceDictionary.Source = new Uri("..\\Translation\\StringResources-ruRU.xaml", UriKind.Relative);
                myResourceDictionary.Source = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "/Translation/StringResources-ruRU.xaml", UriKind.RelativeOrAbsolute);
                Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
                LanguageSelectButton.Content = "Language (Pусский)";
                Properties.Settings.Default.Language_Selection = "Pусский";
                Properties.Settings.Default.Save();
                if (deviceData.Parameters.UnitsSelection == 0)
                    UnitsSelectButton.Content = (string)FindResource("Units (Metric)");
                else if (deviceData.Parameters.UnitsSelection == 1)
                    UnitsSelectButton.Content = (string)FindResource("US units");
                TbRightMenu.IsChecked = false;
                ReBuildStrings();
                EditBtn.Content = (string)FindResource("Edit");
              
                MAX_HIP_FLEXION.FontSize = 11;
                KNEE_FLEXION.FontSize = 11;
                HIP_FLEXION.FontSize = 11;
                HIP_FINAL_FLEXION.FontSize = 11;
                CL.FontSize = 12;
                CL_Open.FontSize = 12;
                PelvicVerPos.FontSize = 12;
                KneeAnteriorPos.FontSize = 13;
                AboveKneeBracketValuString.FontSize = 18;
                cbEnableStairs.FontSize = 13;
                FootPlatesize.FontSize = 18;
                DorseFlexxionText.FontSize = 12;
                string appPath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
                if (GUIVersionType == "USA")
                {
                    HelpFileName = appPath + "\\TherapistManual_Russain\\TherapistManualUSA.pdf";
                }
                else if (GUIVersionType == "Other")
                {
                    HelpFileName = appPath + "\\TherapistManual_Russain\\TherapistManualOther.pdf";
                }
                Thickness margin = LowerLegLength.Margin;
                margin.Right = 144;
                LowerLegLength.Margin = margin;
            }


       
            private void SelectAllMotors_Checked(object sender, RoutedEventArgs e)
            {
                cbCalibLH.IsChecked = true;
                cbCalibLK.IsChecked = true;
                cbCalibRH.IsChecked = true;
                cbCalibRK.IsChecked = true;               
                btnStartCalib.IsEnabled = true;
                if ((short)deviceData.Parameters.DSP == 0x0909)
                {
                    BtnRunEncoderTest.IsEnabled = false;
                }
                else
                {
                    BtnRunEncoderTest.IsEnabled = true;
                }
            }

           private  bool EncoderTestInProcessForALL = false;
           private  bool CalibrationInProcess = false;
           private void SelectAllMotors_Unchecked(object sender, RoutedEventArgs e)
            {
                //if Calbiration in process or encoder test then do nothing
                if (EncoderTestInProcessForALL == false && CalibrationInProcess == false)
                {
                    cbCalibLH.IsChecked = false;
                    cbCalibLK.IsChecked = false;
                    cbCalibRH.IsChecked = false;
                    cbCalibRK.IsChecked = false;
                   
                    btnStartCalib.IsEnabled = false;
                    BtnRunEncoderTest.IsEnabled = false;
                    BtnRunEncoderTest.IsChecked = true;
                }
            }

 
        private void Technician_GotFocus(object sender, RoutedEventArgs e)
        {
            progressform.Focus();
            if (WarningMessageWasCalled == false) //Error Message Not Warning
                progressform.btnClearError.Visibility = Visibility.Visible;
        }

        private void Analyze_GotFocus(object sender, RoutedEventArgs e)
        {
            progressform.Focus();
            if (WarningMessageWasCalled == false) //Error Message Not Warning
                progressform.btnClearError.Visibility = Visibility.Visible;
        }

        private void Settings_GotFocus(object sender, RoutedEventArgs e)
        {
            progressform.Focus();
            if (WarningMessageWasCalled == false) //Error Message Not Warning
                progressform.btnClearError.Visibility = Visibility.Visible;
        }

        

        private double Weight_Metric_On;
        private double Weight_US_On;
        private double Height_Metric_On;
        private double Height_US_On;

        void InitParamWithUnitsConv()
        {
        /*    if (deviceData.Parameters.UnitsSelection == 0)
            {

                Weight_Metric_On = (double)deviceData.Parameters.Weight;               
                Weight_US_On = Math.Round(Weight_Metric_On * 2.20462);
                if (Weight_Metric_On <= 0 || Weight_Metric_On > 999)
                { UserWightInPut.Text = ""; UserWightOutPut.Text = ""; }
                else
                    UserWightInPut.Text = Convert.ToString(Weight_Metric_On);//dispaly now
                Height_Metric_On = (double)deviceData.Parameters.Height;
                Height_US_On = Math.Round(Height_Metric_On / 2.54);
                if (Height_Metric_On <= 0 || Height_Metric_On > 999)
                { UserHeightInPut.Text = ""; UserHeightInPut.Text = ""; }
                else
                    UserHeightInPut.Text = Convert.ToString(Height_Metric_On);//display now
                Metric_Units.IsChecked = true;
                US_Units.IsChecked = false;
            }

            if (deviceData.Parameters.UnitsSelection == 1)
            {

                Weight_US_On = (double)deviceData.Parameters.Weight;
                Weight_Metric_On = Math.Round(Weight_US_On / 2.20462);
                if (Weight_US_On <= 0 || Weight_US_On > 2202) //2202 lb = 999kg
                { UserWightInPut.Text = ""; UserWightOutPut.Text = ""; }
                else
                    UserWightInPut.Text = Convert.ToString(Weight_US_On);//display now
                Height_US_On = (double)deviceData.Parameters.Height;
                Height_Metric_On = Math.Round(Height_US_On * 2.54);
                if (Height_US_On <= 0 || Height_US_On > 393) //393 inch = 999cm
                { UserHeightInPut.Text = ""; UserHeightInPut.Text = ""; }
                else
                    UserHeightInPut.Text = Convert.ToString(Height_US_On);//display now

                US_Units.IsChecked = true;
                Metric_Units.IsChecked = false;
            }
            */
        }
        private void EditBtn_Checked(object sender, RoutedEventArgs e)
        {
            //Edit button - enables user to insert data- nothing to do 
            EditBtn.Content = (string)FindResource("SAVE");
         

        }

        private void EditBtn_Unchecked(object sender, RoutedEventArgs e) //Save button of units was pressed
        {
            EditBtn.Content = (string)FindResource("Edit");
            //Load to rewalk color to red
            Write.IsChecked = true;
            Write.IsChecked = true;
            Write4.Foreground = Brushes.Red;
            Write.IsChecked = true;
            Write2.Foreground = Brushes.Red;
            Write3.Foreground = Brushes.Red;

            if (BlueView.SenderViewModel.SelectDevice != null)
            {


                Commit_Blue_Data();

            }
            /*  try
              {



                  if (Metric_Units.IsChecked == true)
                  {
                      try
                      {
                          UserWightInPut.Text = (UserWightInPut.Text == null) ? 0.ToString() : UserWightInPut.Text;
                          UserWightInPut.Text = (UserWightInPut.Text == "") ? 0.ToString() : UserWightInPut.Text;
                          double weight_units = (Convert.ToDouble(UserWightInPut.Text));
                          if (weight_units <= 0 || weight_units > 999)
                          {
                              deviceData.Parameters.Weight = 0;
                              UserWightInPut.Text = ""; UserWightOutPut.Text = "";

                          }
                          else
                          {
                              weight_units = Math.Round(weight_units);
                              UserWightInPut.Text = Convert.ToString(weight_units);//display current
                              Weight_Metric_On = (short)weight_units;
                              Weight_US_On = Math.Round(weight_units * 2.20462);
                              //store the conversion of Metric
                              deviceData.Parameters.Weight = (short)Weight_Metric_On;
                          }
                      }
                      catch (Exception exc)
                      {
                          deviceData.Parameters.Weight = 0;
                          UserWightInPut.Text = ""; UserWightOutPut.Text = "";
                          Logger.Dummy_Error("", exc.Message);
                      }

                      try
                      {
                          UserHeightInPut.Text = (UserHeightInPut.Text == null) ? 0.ToString() : UserHeightInPut.Text;
                          UserHeightInPut.Text = (UserHeightInPut.Text == "") ? 0.ToString() : UserHeightInPut.Text;
                          double height_units = (Convert.ToDouble(UserHeightInPut.Text));
                          if (height_units <= 0 || height_units > 999)
                          {
                              deviceData.Parameters.Height = 0;
                              UserHeightInPut.Text = ""; UserHeightOutPut.Text = "";

                          }
                          else
                          {
                              height_units = Math.Round(height_units);
                              UserHeightInPut.Text = Convert.ToString(height_units);//display current
                              Height_Metric_On = (short)height_units;
                              Height_US_On = Math.Round(height_units / 2.54);
                              //store the conversion of Metric
                              deviceData.Parameters.Height = (short)Height_Metric_On;
                          }
                      }
                      catch (Exception exc)
                      {
                          deviceData.Parameters.Height = 0;
                          UserHeightInPut.Text = ""; UserHeightOutPut.Text = "";
                          Logger.Dummy_Error("", exc.Message);
                      }



                  }
                  else if (US_Units.IsChecked == true)
                  {
                      try
                      {
                          UserWightInPut.Text = (UserWightInPut.Text == null) ? 0.ToString() : UserWightInPut.Text;
                          UserWightInPut.Text = (UserWightInPut.Text == "") ? 0.ToString() : UserWightInPut.Text;
                          double weight_units = (Convert.ToDouble(UserWightInPut.Text));
                          if (weight_units <= 0 || weight_units > 2202)
                          {
                              deviceData.Parameters.Weight = 0;
                              UserWightInPut.Text = ""; UserWightOutPut.Text = "";

                          }
                          else
                          {
                              weight_units = Math.Round(weight_units);
                              UserWightInPut.Text = Convert.ToString(weight_units);//display current
                              Weight_US_On = (short)weight_units;
                              Weight_Metric_On = Math.Round(weight_units / 2.20462);
                              //store the conversion of US
                              deviceData.Parameters.Weight = (short)Weight_US_On;
                          }
                      }
                      catch(Exception exc)
                      {
                          deviceData.Parameters.Weight = 0;
                          UserWightInPut.Text = ""; UserWightOutPut.Text = "";          
                          Logger.Dummy_Error("", exc.Message);
                      }
                      try
                      {
                          UserHeightInPut.Text = (UserHeightInPut.Text == null) ? 0.ToString() : UserHeightInPut.Text;
                          UserHeightInPut.Text = (UserHeightInPut.Text == "") ? 0.ToString() : UserHeightInPut.Text;
                          double height_units = (Convert.ToDouble(UserHeightInPut.Text));
                          if (height_units <= 0 || height_units > 393)
                          {
                              deviceData.Parameters.Height = 0;
                              UserHeightInPut.Text = ""; UserHeightOutPut.Text = "";

                          }
                          else
                          {
                              height_units = Math.Round(height_units);
                              UserHeightInPut.Text = Convert.ToString(height_units);
                              Height_US_On = (short)height_units;
                              Height_Metric_On = Math.Round(height_units * 2.54);
                              //store the conversion of US
                              deviceData.Parameters.Height = (short)Height_US_On;
                          }
                      }
                      catch (Exception exc)
                      {
                          deviceData.Parameters.Height = 0;
                          UserHeightInPut.Text = ""; UserHeightOutPut.Text = "";
                          Logger.Dummy_Error("", exc.Message);
                      }
                  }
              }
              catch(Exception exc)
              {
                  deviceData.Parameters.Height = 0;
                  UserHeightInPut.Text = ""; UserHeightOutPut.Text = "";
                  deviceData.Parameters.Weight = 0;
                  UserWightInPut.Text = ""; UserWightOutPut.Text = ""; 
                  Logger.Dummy_Error("", exc.Message);
              }
                         */
        }

        private void F_Checked(object sender, RoutedEventArgs e)
        {
            M.IsChecked = false;
            Female.Opacity = 1;
            Male.Opacity = 0;
            deviceData.Parameters.Gender = false;

        }

        private void F_Unchecked(object sender, RoutedEventArgs e)
        {
            M.IsChecked = true;
        }

        private void M_Checked(object sender, RoutedEventArgs e)
        {
            F.IsChecked = false;
            Female.Opacity = 0; 
            Male.Opacity = 1;
            deviceData.Parameters.Gender = true;
        }

        private void M_Unchecked(object sender, RoutedEventArgs e)
        {
            F.IsChecked = true;
        }

      

        private void CbCalibLH_Unchecked(object sender, RoutedEventArgs e)
        {
            
            SelectAllMotors.IsChecked = false;
            if (EncoderTestInProcessForALL == false && CalibrationInProcess == false
                 && cbCalibLK.IsChecked == false
                 && cbCalibRH.IsChecked == false && cbCalibRK.IsChecked == false)
            {
                btnStartCalib.IsEnabled = false;
                BtnRunEncoderTest.IsEnabled = false;
            }
        }
      
        private void CbCalibLK_Unchecked(object sender, RoutedEventArgs e)
        {
          
            SelectAllMotors.IsChecked = false;
           
            if (EncoderTestInProcessForALL == false && CalibrationInProcess == false
                && cbCalibLH.IsChecked == false 
                && cbCalibRH.IsChecked == false && cbCalibRK.IsChecked == false)
            {
                btnStartCalib.IsEnabled = false;
                BtnRunEncoderTest.IsEnabled = false;
            }
        }

        private void CbCalibRK_Unchecked(object sender, RoutedEventArgs e)
        {
           
            SelectAllMotors.IsChecked = false;
            if (EncoderTestInProcessForALL == false && CalibrationInProcess == false
                && cbCalibLH.IsChecked == false && cbCalibLK.IsChecked == false
                && cbCalibRH.IsChecked == false)
            {
                btnStartCalib.IsEnabled = false;
                BtnRunEncoderTest.IsEnabled = false;
            }
        }

        private void CbCalibRH_Unchecked(object sender, RoutedEventArgs e)
        {
          
            SelectAllMotors.IsChecked = false;
            if (EncoderTestInProcessForALL == false && CalibrationInProcess == false
                && cbCalibLH.IsChecked == false && cbCalibLK.IsChecked == false
                && cbCalibRK.IsChecked == false)
            {
                btnStartCalib.IsEnabled = false;
                BtnRunEncoderTest.IsEnabled = false;
            }
        }

        private void MenuTechEnter_Click(object sender, RoutedEventArgs e)
        {

            PasswordCheckingMessage.Text = "";
            PasswordCheckingMessageStack.Visibility = Visibility.Collapsed;
            if (TechnicianScreenWasEntered == true)
            {
                Technician.Visibility = Visibility.Visible;
                Technician.IsSelected = true;
                TbRightMenu.IsChecked = false;
                TechnicianEntrance.Visibility = Visibility.Collapsed;
                TopToolBar.Visibility = Visibility.Collapsed;
                UD.Visibility = Visibility.Collapsed;
                EditBtn.Visibility = Visibility.Collapsed;
                MenuTechEnter.Visibility = Visibility.Collapsed;
            }
            else
            {
                Technician.Visibility = Visibility.Collapsed;
                Technician.IsSelected = false;
                TbRightMenu.IsChecked = true;
                TechnicianEntrance.Visibility = Visibility.Visible;
                MenuTechEnter.Visibility = Visibility.Collapsed;
               
            }
        }

    

        private void HomeScreenClick(object sender, RoutedEventArgs e)
        {
            PasswordCheckingMessage.Text = "";
            PasswordCheckingMessageStack.Visibility = Visibility.Collapsed;
            Physiotherapist.IsSelected = true;
            TbRightMenu.IsChecked = true;
            UD.Visibility = Visibility.Collapsed;
            EditBtn.Visibility = Visibility.Collapsed;
            TopToolBar.Visibility = Visibility.Visible;
            TopToolBarButtons.Visibility = Visibility.Collapsed;
            //   MenuTechEnter.Visibility = Visibility.Visible;
            SystemInfo.Visibility = Visibility.Collapsed;
            ActiveInactiveStrokeButton.Visibility = Visibility.Collapsed;
            ActiveInactiveStrokeText.Visibility = Visibility.Collapsed;
        }

        private void USERTEST_ProductionClick(object sender, RoutedEventArgs e)
        {
            Physiotherapist.IsSelected = true;
            TbRightMenu.IsChecked = false;
            UD.Visibility = Visibility.Collapsed;
            TopToolBar.Visibility = Visibility.Visible;
            TopToolBarButtons.Visibility = Visibility.Collapsed;
        }

        private void ATP_ProductionClick(object sender, RoutedEventArgs e)
        {
            progressform.Focus();
            if (WarningMessageWasCalled == false) //Error Message Not Warning
                progressform.btnClearError.Visibility = Visibility.Visible;
            Load.IsEnabled = true;
            ShowPYTH.IsEnabled = true;
            OpenUserfile.IsEnabled = true;
            TopToolBar.IsEnabled = true;
            progressform.Focus();
            TechnicianScreenWasEntered = true;
        }

       


        private void TechnicianPasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            string xword;
            PasswordBox aa = (sender as PasswordBox);
            if (aa != null)
            {
                xword = aa.Password;


                if (xword == GetSysPass())
                {
                    if ((short)deviceData.Parameters.DSP == 0x0909)
                    {
                        Load.IsEnabled = false;
                        OpenUserfile.IsEnabled = false;
                        ShowPYTH.IsEnabled = false;
                    }
                    else
                    {
                        ShowPYTH.IsEnabled = false;
                        if (USBConnectState == true)
                        {
                            ShowPYTH.IsEnabled = true;
                            Load.IsEnabled = true;
                            OpenUserfile.IsEnabled = true;
                        }
                    }
                    
                    TopToolBar.IsEnabled = true;
                    Technician.Visibility = Visibility.Visible;
                    Technician.IsSelected = true;
                    TopToolBar.Visibility = Visibility.Collapsed;
                    UD.Visibility = Visibility.Collapsed;
                    EditBtn.Visibility = Visibility.Collapsed;
                    TbRightMenu.IsChecked = false;
                    if (WarningMessageWasCalled == false) //Error Message Not Warning
                        progressform.btnClearError.Visibility = Visibility.Visible;
                    progressform.Focus();
                    TechnicianScreenWasEntered = true;
                    TechnicianEntrance.Visibility = Visibility.Collapsed;
                    PasswordCheckingMessage.Text = "";
                    PasswordCheckingMessageStack.Visibility = Visibility.Collapsed;
                    MenuTechEnter.Visibility = Visibility.Collapsed;
                }
                else
                {
                   // TechnicianPasswordBox.Password = "";
                    PasswordCheckingMessage.Text = (string)FindResource("Incorrect Password");
                    PasswordCheckingMessageStack.Visibility = Visibility.Visible;
                    Technician.Visibility = Visibility.Collapsed;
                    Technician.IsSelected = false;
                    TopToolBar.Visibility = Visibility.Visible;
                    TbRightMenu.IsChecked = true;
                    TechnicianEntrance.Visibility = Visibility.Visible;
                    //MenuTechEnter.Visibility = Visibility.Visible;
                }

                if (Production.IsSelected == true)
                {
                    TopToolBar.Visibility = Visibility.Collapsed;
                }
            }



            PasswordCheckingMessageStack.Visibility = Visibility.Collapsed;
        }

 
        private void TechnicianPasswordBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                string xword;
                PasswordBox aa = (sender as PasswordBox);
                if (aa != null)
                {
                    xword = aa.Password;


                    if (xword == GetSysPass())
                    {
                        if ((short)deviceData.Parameters.DSP == 0x0909)
                        {
                            Load.IsEnabled = false;
                            OpenUserfile.IsEnabled = false;
                            ShowPYTH.IsEnabled = false;
                        }
                        else
                        {
                            ShowPYTH.IsEnabled = false;
                            if (USBConnectState == true)
                            {
                                ShowPYTH.IsEnabled = true;
                                Load.IsEnabled = true;
                                OpenUserfile.IsEnabled = true;
                            }
                        }
                        TopToolBar.IsEnabled = true;
                        Technician.Visibility = Visibility.Visible;
                        Technician.IsSelected = true;
                        TopToolBar.Visibility = Visibility.Collapsed;
                        UD.Visibility = Visibility.Collapsed;
                        EditBtn.Visibility = Visibility.Collapsed;
                        TbRightMenu.IsChecked = false;
                        if (WarningMessageWasCalled == false) //Error Message Not Warning
                            progressform.btnClearError.Visibility = Visibility.Visible;
                        progressform.Focus();
                        TechnicianScreenWasEntered = true;
                        TechnicianEntrance.Visibility = Visibility.Collapsed;
                        PasswordCheckingMessage.Text = "";
                        PasswordCheckingMessageStack.Visibility = Visibility.Collapsed;
                        MenuTechEnter.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        TechnicianPasswordBox.Password = SetPassBoxEmpty();
                        PasswordCheckingMessage.Text = (string)FindResource("Incorrect Password");
                        PasswordCheckingMessageStack.Visibility = Visibility.Visible;
                        Technician.Visibility = Visibility.Collapsed;
                        Technician.IsSelected = false;
                        TopToolBar.Visibility = Visibility.Visible;
                        TbRightMenu.IsChecked = true;
                        TechnicianEntrance.Visibility = Visibility.Visible;
                        //MenuTechEnter.Visibility = Visibility.Visible;
                    }

                    if (Production.IsSelected == true)
                    {
                        TopToolBar.Visibility = Visibility.Collapsed;
                    }
                }
            
            }  
        }

     
        private void Analyze_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TbRightMenu.IsChecked = false;
        }

        private void Settings_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TbRightMenu.IsChecked = false;
        }

        private void CbEnableStairs_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write4.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;

            }
        }

        private void Physiotherapist_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TbRightMenu.IsChecked = false;
        }

        private void PT_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TbRightMenu.IsChecked = false;
        }

        private void TItemSystemMeasure_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TbRightMenu.IsChecked = false;
        }

        private void TItemStanding_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TbRightMenu.IsChecked = false;
        }

        private void TItemWalking_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TbRightMenu.IsChecked = false;
        }

        private void TItemStairs_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TbRightMenu.IsChecked = false;
        }

        private void TabControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TbRightMenu.IsChecked = false;
        }

        private void EditUser_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TbRightMenu.IsChecked = false;
        }

        private void Bars_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TbRightMenu.IsChecked = false;
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TbRightMenu.IsChecked = false;
        }

        private void TbRightMenu_Checked(object sender, RoutedEventArgs e)
        {
            PT.IsEnabled = false;
            TItemSystemMeasure.IsEnabled = false;
            TItemStanding.IsEnabled = false;
            TItemWalking.IsEnabled = false;
            TItemStairs.IsEnabled = false;
            //MenuTechEnter.Visibility = Visibility.Visible;                
        }

        private void TbRightMenu_Unchecked(object sender, RoutedEventArgs e)
        {
            PT.IsEnabled = true;
            TItemSystemMeasure.IsEnabled = true;
            TItemStanding.IsEnabled = true;
            TItemWalking.IsEnabled = true;
            TItemStairs.IsEnabled = true;
            TechnicianEntrance.Visibility = Visibility.Collapsed;       
            Help_About.Visibility = Visibility.Collapsed;
            Languages.Visibility = Visibility.Collapsed;
            Units.Visibility = Visibility.Collapsed;
        }

      
        private void WalkAfterStuckToggle_Click(object sender, RoutedEventArgs e)
        {
            Write.IsChecked = true;
            Write4.Foreground = Brushes.Red;
            Write.IsChecked = true;
            Write2.Foreground = Brushes.Red;
            Write3.Foreground = Brushes.Red;
        }

        private void BuzzerBetwStepsToggle_Click(object sender, RoutedEventArgs e)
        {
            Write.IsChecked = true;
            Write4.Foreground = Brushes.Red;
            Write.IsChecked = true;
            Write2.Foreground = Brushes.Red;
            Write3.Foreground = Brushes.Red;
        }

        private void InfSN_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write2.Foreground = Brushes.Red;
                Write2.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void LHSN_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write2.Foreground = Brushes.Red;
                Write2.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void LKSN_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write2.Foreground = Brushes.Red;
                Write2.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void RCSN_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write2.Foreground = Brushes.Red;
                Write2.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void RHSN_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write2.Foreground = Brushes.Red;
                Write2.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void RKSN_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write2.Foreground = Brushes.Red;
                Write2.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void Sn2_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write2.Foreground = Brushes.Red;
                Write2.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void Sn3_TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write2.Foreground = Brushes.Red;
                Write2.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

     

        private void cbMEMLogLevelData_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbCANLogLevelData_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbCANLogLevelWarning_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbCANLogLevelError_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbMCULogLevelData_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbMCULogLevelWarning_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbMCULogLevelError_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbRFLogLevelData_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbRFLogLevelWarning_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbRFLogLevelError_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbRCLogLevelData_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }
     

        private void cbRCLogLevelWarning_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbRCLogLevelError_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbADCLogLevelData_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbADCLogLevelWarning_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbADCLogLevelError_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbSTAIRSLogLevelData_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

       
        private void cbSTAIRSLogLevelWarning_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbSTAIRSLogLevelError_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbSITLogLevelData_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbSITLogLevelWarning_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbSITLogLevelError_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbWALKLogLevelData_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbWALKLogLevelWarning_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbWALKLogLevelError_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbUSBLogLevelData_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbUSBLogLevelWarning_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbUSBLogLevelError_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbBITLogLevelData_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbBITLogLevelWarning_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbBITLogLevelError_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

     

        private void cbMEMLogLevelWarning_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbMEMLogLevelError_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbOPSLogLevelData_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbOPSLogLevelWarning_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        private void cbOPSLogLevelError_Click(object sender, RoutedEventArgs e)
        {
            if (USBConnectState == true)
            {
                Write3.Foreground = Brushes.Red;
                Write3.IsEnabled = true;
                Write4.Foreground = Brushes.Red;
                Write.IsChecked = true;
                Write2.Foreground = Brushes.Red;
                Write3.Foreground = Brushes.Red;
            }
        }

        //private void TbBegginersMode_Checked(object sender, RoutedEventArgs e)
        //{
        //    FLEXIOFF.Visibility = Visibility.Collapsed;
        //    FLEXION.Visibility = Visibility.Visible;
        //}

        //private void TbBegginersMode_Unchecked(object sender, RoutedEventArgs e)
        //{
        //    FLEXIOFF.Visibility = Visibility.Visible;
        //    FLEXION.Visibility = Visibility.Collapsed;
        //}

        private void FirstStepAssWalkToggle_Checked(object sender, RoutedEventArgs e)
        {
            FirstStepAssistance.IsChecked = true;
            OFF1Sts.Visibility = Visibility.Collapsed;
            ON1Sts.Visibility = Visibility.Visible;
            F1sSBtns.Visibility = Visibility.Visible;
        }

        private void FirstStepAssWalkToggle_Unchecked(object sender, RoutedEventArgs e)
        {
            OFF1Sts.Visibility = Visibility.Visible;
            ON1Sts.Visibility = Visibility.Collapsed;
            F1sSBtns.Visibility = Visibility.Collapsed;
            ActiveBlueButton1.Visibility = Visibility.Hidden;
            DeActiveBlueButton1.Visibility = Visibility.Hidden;
        }

        private void WalkAfterStuckToggle_Checked(object sender, RoutedEventArgs e)
        {
            WASON.Visibility = Visibility.Visible;
            WASOFF.Visibility = Visibility.Collapsed;
            WASON1.Visibility = Visibility.Visible;
            WASOFF1.Visibility = Visibility.Collapsed;
            selectonebluedevicetext.Visibility = Visibility.Visible;
            BlueListDevices1.Visibility = Visibility.Visible;
            BlueListDevices1.IsEnabled = true;
            Messenger.Default.Send(true ? new Message(true) : new Message(false));

        }

        private void WalkAfterStuckToggle_Unchecked(object sender, RoutedEventArgs e)
        {
            WASON.Visibility = Visibility.Collapsed;
            WASOFF.Visibility = Visibility.Visible;
            WASON1.Visibility = Visibility.Collapsed;
            WASOFF1.Visibility = Visibility.Visible;
            BlueListDevices1.UnselectAll();
            BlueListDevices1.Visibility = Visibility.Hidden;
            ActiveBlueButton1.Visibility = Visibility.Collapsed;
            DeActiveBlueButton1.Visibility = Visibility.Collapsed;
            selectonebluedevicetext.Visibility = Visibility.Hidden;


        }

        private void BuzzerBetwStepsToggle_Checked(object sender, RoutedEventArgs e)
        {
            BBSOFF.Visibility = Visibility.Collapsed;
            BBSON.Visibility = Visibility.Visible;
        }

        private void BuzzerBetwStepsToggle_Unchecked(object sender, RoutedEventArgs e)
        {
            BBSOFF.Visibility = Visibility.Visible;
            BBSON.Visibility = Visibility.Collapsed;
        }

       

    
        private void FirstStepAssWalkToggle_Click_1(object sender, RoutedEventArgs e)
        {
            Write.IsChecked = true;
            Write.IsChecked = true;
            Write4.Foreground = Brushes.Red;
            Write.IsChecked = true;
            Write2.Foreground = Brushes.Red;
            Write3.Foreground = Brushes.Red;
        }

        private void FDTTBtn_Click_1(object sender, RoutedEventArgs e)
        {
            Write.IsChecked = true;
            Write.IsChecked = true;
            Write4.Foreground = Brushes.Red;
            Write.IsChecked = true;
            Write2.Foreground = Brushes.Red;
            Write3.Foreground = Brushes.Red;
        }

        private void FDTTBtn_Checked_1(object sender, RoutedEventArgs e)
        {
            OFF.Visibility = Visibility.Collapsed;
            ON.Visibility = Visibility.Visible;
        }

        private void FDTTBtn_Unchecked_1(object sender, RoutedEventArgs e)
        {
            OFF.Visibility = Visibility.Visible;
            ON.Visibility = Visibility.Collapsed;
        }


        void Test_Messages_Translation()
        {

            //366-275
            MessageBox.Show((string)this.FindResource("Illegal input please input only digits !"), (string)this.FindResource("Input Error"), MessageBoxButton.OK, MessageBoxImage.Error);
            // 367-368-276
            MessageBox.Show((string)this.FindResource("SN inputed is different from system SN.") + "\n" + (string)this.FindResource("Do you want to proceed?"), (string)this.FindResource("System SN Alert"), MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            //  359-360-272  
            MessageBox.Show(this, (string)this.FindResource("This file was created by an older Rewalk interface version and is not compatible to the current version. In order to continue the operation it will be converted to a compatible format and saved."),
                                                    (string)this.FindResource("User file Loading"), MessageBoxButton.OK, MessageBoxImage.Information);
            // 369-210-212
            Logger.Dummy_Error((string)this.FindResource("Load configuration failed"), "Could_not_connect");
            // 397
            MessageBox.Show((string)this.FindResource("Sorry the file can't be opened because it has different type of system"), "", MessageBoxButton.OK, MessageBoxImage.Hand);
            //237-238-303
            MessageBoxResult continue_download = MessageBox.Show(this, (string)this.FindResource("The current boot version is not compatible with this DSP version!") + "\n" + (string)this.FindResource("Please update boot version first.") + "\n" + (string)this.FindResource("Downloading this DSP version might cause the device to mulfunction.") + "\n" + (string)this.FindResource("Do you want to continue?"), (string)this.FindResource("INF app Downloading..."), MessageBoxButton.YesNoCancel, MessageBoxImage.Error);

            MessageBoxResult result = MessageBox.Show(this, (string)this.FindResource("You are about to update INF app.") + "\n" + (string)this.FindResource("This will take several seconds, please do not turn off the system!"), (string)this.FindResource("INF app Downloading..."), MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
            MessageBox.Show(this, (string)this.FindResource("Downloading new INF's software was finished successfully") + "\n" + (string)this.FindResource("Please, restart the system!!"), (string)this.FindResource("INF app Downloading..."), MessageBoxButton.OK, MessageBoxImage.Information);
            MessageBox.Show((string)this.FindResource("The selected file is not an upgrading INF App file !") + "\n" + (string)this.FindResource("Please select another file and try again."), (string)FindResource("INF app  Downloading..."), MessageBoxButton.OK, MessageBoxImage.Hand);
            MessageBox.Show(this, (string)FindResource("You are about to update the MCU SW version") + "\n" + (string)FindResource("This will take several seconds, please do not turn off the system!"), (string)FindResource("MCU app Downloading..."), MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
            MessageBox.Show((string)FindResource("The selected file is not an upgrading MCU file !") + "\n" + (string)FindResource("Please select another file and try again."), (string)FindResource("MCU app Downloading..."), MessageBoxButton.OK, MessageBoxImage.Hand);

            MessageBox.Show(this, (string)FindResource("No MCU file was selected!") + "\n" + (string)FindResource("Press the Browse button first and select an appropriate MCU file."), (string)FindResource("MCU app Downloading..."), MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
             MessageBox.Show(this, (string)FindResource("You are about to update the INF Boot SW version") + "\n" + (string)FindResource("This will take several seconds, please do not turn off the system!"), (string)FindResource("INF Boot Downloading..."), MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
           MessageBox.Show(this, (string)FindResource("Downloading new INF's Boot  software was finished successfully") + "\n" + (string)FindResource("Please, Restart the system!!"), (string)FindResource("INF Boot Downloading..."), MessageBoxButton.OK, MessageBoxImage.Information);
             MessageBox.Show(this, (string)FindResource("Downloading new INF's Boot  software was finished successfully") + "\n" + (string)FindResource("Please, Restart the system!!"), (string)FindResource("INF Boot Downloading..."), MessageBoxButton.OK, MessageBoxImage.Information);
             result = MessageBox.Show((string)FindResource("INF boot downloading has failed!") + "\n" + (string)FindResource("Error Code:") + deviceData.TestData.ErrorCodeRC.ToString(), (string)FindResource("INF Boot Downloading..."), MessageBoxButton.OK, MessageBoxImage.Error);
             result = MessageBox.Show((string)FindResource("The selected file is not an upgrading INF Boot file !") + "\n" + (string)FindResource("Please select another file and try again."), (string)FindResource("INF Boot Downloading..."), MessageBoxButton.OK, MessageBoxImage.Hand);
             result = MessageBox.Show((string)this.FindResource("No INF Boot file was selected!") + "\n" + (string)this.FindResource("Please select another file and try again."), (string)FindResource("INF Boot Downloading..."), MessageBoxButton.OK, MessageBoxImage.Exclamation);

             continue_download = MessageBox.Show(this, (string)this.FindResource("The current INF boot version is not compatible with this INF app version!") + "\n" + (string)this.FindResource("Please update the boot version first.") + "\n" + (string)this.FindResource("Downloading this INF app version might cause the device to mulfunction.") + "\n" + (string)this.FindResource("Do you want to continue?"), (string)this.FindResource("INF app Downloading..."), MessageBoxButton.YesNoCancel, MessageBoxImage.Error);
             result = MessageBox.Show(this, (string)this.FindResource("Save configuration file finished"), (string)this.FindResource("Save As"), MessageBoxButton.OK, MessageBoxImage.Information);
             result = MessageBox.Show((string)this.FindResource("Parameters were written successfully to the Rewalk"), (string)this.FindResource("Write"), MessageBoxButton.OK, MessageBoxImage.Exclamation);
             result = MessageBox.Show((string)this.FindResource("MCU is running in BOOT mode, Download new motors software or restart the device"), (string)this.FindResource("Warning_"), MessageBoxButton.OK, MessageBoxImage.Error);
             result = MessageBox.Show((string)this.FindResource("Rewalk is running in BOOT mode, limited operations are available!") + "\n" + (string)this.FindResource("Do you want to switch to operational mode?"), (string)this.FindResource("Warning_"), MessageBoxButton.YesNo, MessageBoxImage.Information);
             result = MessageBox.Show((string)this.FindResource("Are you sure you want to change address/ID Manually?") + "\n" + (string)this.FindResource("It is recommended to use the generate button."), (string)this.FindResource("Address ID Alert"), MessageBoxButton.YesNoCancel, MessageBoxImage.Exclamation);
             result = MessageBox.Show((string)this.FindResource("RC address or ID out of range") + "\n" + (string)this.FindResource("Please input address from 1 to 255, ID from 1 to 32767."), (string)this.FindResource("INF Address and ID"), MessageBoxButton.OK, MessageBoxImage.Exclamation);
             result = MessageBox.Show((string)this.FindResource("INF address and ID were updated successfully!") + "\n" + (string)this.FindResource("Please reset the system (by turning it off and on)."), (string)this.FindResource("INF Address and ID"), MessageBoxButton.OK, MessageBoxImage.Exclamation);
             result = MessageBox.Show(this, (string)this.FindResource("RC address or ID out of range") + "\n" + (string)this.FindResource("Please input address from 1 to 255, ID from 1 to 32767."));
             result = MessageBox.Show((string)this.FindResource("Please turn on RC and select hand icon in technician mode"));
             string message = (string)this.FindResource("RC address and ID were updated successfully!") + "\n" + (string)this.FindResource("Please reset the system (by turning it off and on).");
             result = MessageBox.Show(this, (string)this.FindResource("Please Turn on RC !"), (string)this.FindResource("RC Test"));
             message = (string)this.FindResource("RC Test Finished !") + "\n" + "\n" + (string)this.FindResource("Results:") + "\n" + (string)this.FindResource("RC error rate:") + 0 + "%" + "\n" + (string)this.FindResource("INF error rate:") + 0 + "%";
             result = MessageBox.Show(this, message, "RC Test Results");
             MessageBox.Show((string)this.FindResource("INF application downloading has failed") + "\n" + (string)this.FindResource("Error Code:") + deviceData.TestData.ErrorCodeINF.ToString(), (string)this.FindResource("INF app Downloading..."), MessageBoxButton.OK, MessageBoxImage.Error);
             result = MessageBox.Show(this, (string)this.FindResource("Downloading new INF's software was finished successfully") + "\n" + (string)this.FindResource("Please, restart the system!!"), (string)this.FindResource("INF app Downloading..."), MessageBoxButton.OK, MessageBoxImage.Information);
             result = MessageBox.Show(this, (string)this.FindResource("Calibration Finished.") + "\n" + (string)this.FindResource("Please restart the system."), (string)this.FindResource("Calibration"), MessageBoxButton.OK, MessageBoxImage.Information);
             result = MessageBox.Show(this, (string)this.FindResource("Downloading new INF's software was finished successfully") + "\n" + (string)this.FindResource("Please, restart the system!!"), (string)this.FindResource("INF app Downloading..."), MessageBoxButton.OK, MessageBoxImage.Information);
             result = MessageBox.Show(this, (string)this.FindResource("Calibration Failed!") + "\n" + (string)this.FindResource("Please restart and recalibrate the system."), (string)FindResource("Calibration"), MessageBoxButton.OK, MessageBoxImage.Error);
             result = MessageBox.Show((string)this.FindResource("Log File Download Complete.") + "\n" + (string)this.FindResource("Open?"), (string)this.FindResource("Save Log File"), MessageBoxButton.YesNo);
             result = MessageBox.Show((string)this.FindResource("Log Parser Failed"), (string)this.FindResource("Save Flash Log File"), MessageBoxButton.OK, MessageBoxImage.Error);
             result = MessageBox.Show((string)this.FindResource("Flash Error Log File Download Complete.") + "\n" + (string)this.FindResource("Open?"), (string)this.FindResource("Save Log File"), MessageBoxButton.YesNo);
             result = MessageBox.Show((string)this.FindResource("Flash Log File Download Succeeded !"), (string)this.FindResource("Save Log File"), MessageBoxButton.OK, MessageBoxImage.Information);
             result = MessageBox.Show((string)this.FindResource("Flash Log File Download Succeeded !"), (string)this.FindResource("Save Log File"), MessageBoxButton.OK, MessageBoxImage.Information);
             result = MessageBox.Show((string)this.FindResource("Are you sure you want to reset steps counter?"), (string)this.FindResource("Steps Counter Reset"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
             result = MessageBox.Show((string)this.FindResource("Are you sure you want to reset steps counter?"), (string)this.FindResource("Steps Counter Reset"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
             result = MessageBox.Show((string)this.FindResource("Log Parser Failed"), (string)this.FindResource("Save Flash Log File"), MessageBoxButton.OK, MessageBoxImage.Error);
             result = MessageBox.Show((string)this.FindResource("LH downloading has failed:- Check Power or COM of LH") + "\n" + (string)this.FindResource("Error Code:"), (string)this.FindResource("MCU LH Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
             result = MessageBox.Show((string)this.FindResource("LH downloading has failed:- Can't run LH in boot mode") + "\n" + (string)this.FindResource("Check Power or COM of LH") + "\n" + (string)this.FindResource("Error Code:") , (string)this.FindResource("MCU LH Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);

             result = MessageBox.Show((string)this.FindResource("LH downloading has failed:") + "\n" + (string)this.FindResource("Check Power or COM of LH") + "\n" + (string)this.FindResource("Error Code:"), (string)this.FindResource("MCU LH Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
            
              MessageBox.Show(this, (string)this.FindResource("Downloading new MCU's software was canceled") + "\n" + (string)this.FindResource("Please restart the system and try again!!"), (string)this.FindResource("MCU's Downloading..."), MessageBoxButton.OK, MessageBoxImage.Stop);
            
              result = MessageBox.Show((string)this.FindResource("LK downloading has failed:- Check Power or COM of LK") + "\n" + (string)this.FindResource("Error Code:") , (string)this.FindResource("MCU LK Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);

             result = MessageBox.Show((string)this.FindResource("LK downloading has failed:- Can't run LK in boot mode") + "\n" + (string)this.FindResource("Check Power or COM of LK") + "\n" + (string)this.FindResource("Error Code:") , (string)this.FindResource("MCU LK Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
            
             result = MessageBox.Show((string)this.FindResource("LK downloading has failed:") + "\n" + (string)this.FindResource("Check Power or COM of LK") + "\n" + (string)this.FindResource("Error Code:") , (string)this.FindResource("MCU LK Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
             result = MessageBox.Show((string)this.FindResource("RH downloading has failed:- Check Power or COM of RH") + "\n" + (string)this.FindResource("Error Code:") , (string)this.FindResource("MCU RH Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);

             result = MessageBox.Show((string)this.FindResource("RH downloading has failed:- Can't run RH in boot mode") + "\n" + (string)this.FindResource("Check Power or COM of RH") + "\n" + (string)this.FindResource("Error Code:") , (string)this.FindResource("MCU RH Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);

             result = MessageBox.Show((string)this.FindResource("RH downloading has failed:") + "\n" + (string)this.FindResource("Check Power or COM of RH") + "\n" + (string)this.FindResource("Error Code:") , (string)this.FindResource("MCU RH Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);

             result = MessageBox.Show((string)this.FindResource("RK downloading has failed:- Check Power or COM of RK") + "\n" + (string)this.FindResource("Error Code:") , (string)this.FindResource("MCU RK Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);

             result = MessageBox.Show((string)this.FindResource("RK downloading has failed:- Can't run RK in boot mode") + "\n" + (string)this.FindResource("Check Power or COM of RK") + "\n" + (string)this.FindResource("Error Code:"), (string)this.FindResource("MCU RK Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
             result = MessageBox.Show((string)this.FindResource("RK downloading has failed:") + "\n" + (string)this.FindResource("Check Power or COM of RK") + "\n" + (string)this.FindResource("Error Code:") , (string)this.FindResource("MCU RH Downloading"), MessageBoxButton.OK, MessageBoxImage.Error);
             result = MessageBox.Show(this, (string)this.FindResource("Downloading new MCU's software was finished successfully") + "\n" + (string)this.FindResource("Please, Restart the system!!"), (string)this.FindResource("MCU Downloading..."), MessageBoxButton.OK, MessageBoxImage.Information);
             result = MessageBox.Show((string)this.FindResource("Are you sure you want to reset stairs counter?"), (string)this.FindResource("Stairs Counter Reset"), MessageBoxButton.YesNo, MessageBoxImage.Warning);
             result = MessageBox.Show((string)this.FindResource("Reset was done successfully"), (string)this.FindResource("Confirmation"), MessageBoxButton.OK, MessageBoxImage.Warning);
             result = MessageBox.Show((string)this.FindResource("Are you sure you want to reset log file ?"), (string)this.FindResource("Log File Reset"), MessageBoxButton.YesNo, MessageBoxImage.Warning);         
             result = MessageBox.Show("", (string)this.FindResource("Confirmation"), MessageBoxButton.OK, MessageBoxImage.Warning);
             result = MessageBox.Show((string)FindResource("Stairs flexion exceed the allowed limits") + "\n" + 0, (string)FindResource("Stairs Calculation Warning"), MessageBoxButton.OK, MessageBoxImage.Warning);
             result = MessageBox.Show(this, (string)this.FindResource("Another instance of the Rewalk application is already running.") + "\n", (string)this.FindResource("Warning_"), MessageBoxButton.OK, MessageBoxImage.Hand);
            
          //  Logger.Error_MessageBox_Advanced("Loading System Configuration Parameters Failed:", "Loading System Configuration", "");
             Logger.Error_MessageBox_Advanced("Save Log File failed:", "Save Flash Log File", "");
             Logger.Error_MessageBox_Advanced("Save Flash Log File failed:", "Save Flash Log File", "");
             Logger.Error_MessageBox_Advanced("Finding Current Index was failed", "Finding Current Index", "");
             Logger.Error_MessageBox_Advanced("System measurements initialization has failed." + "\n" + "Perform Restore and then Load to Rewalk to continue.", "Intialiazing System Configuration", "");
      
        }

       
        private void TranMessTest_Click(object sender, RoutedEventArgs e)
        {
            Test_Messages_Translation();
        }

        private void OnBrowseViewFlashLogFile(object sender, RoutedEventArgs e)
        {
          
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.CheckFileExists = false;
            ofd.RestoreDirectory = true;
            ofd.Filter = "(*.dat)|*.dat|(*.*) |*.* | (*.txt)|*.txt";

            if (ofd.ShowDialog().Value == true)
            {

                tbSaveFlashLogFileLocation.Text = ofd.FileName;

                try
                {
                    string tbSaveFlashLogFileLocation1 = ofd.FileName;// tbViewLogFileLocation.Text;
                    string LogFileNameExtention = Path.GetExtension(tbSaveFlashLogFileLocation1);
                    if (LogFileNameExtention == ".dat")
                    {
                        tbSaveFlashLogFileLocation1 = tbSaveFlashLogFileLocation.Text.TrimEnd("dat".ToCharArray());
                        tbSaveFlashLogFileLocation1 = tbSaveFlashLogFileLocation1 + "txt";

                        LogParser.ParseFile(tbSaveFlashLogFileLocation.Text, false, tbSaveFlashLogFileLocation1);

                    }

                    Process.Start("notepad", tbSaveFlashLogFileLocation1);

                }
                catch (Exception exception)
                {
                    MessageBox.Show((string)this.FindResource("Log Parser Failed"), (string)this.FindResource("Save Flash Log File"), MessageBoxButton.OK, MessageBoxImage.Error);
                    Logger.Dummy_Error("", exception.Message);
                }
            }
        }

        private void TbBegginersMode_Click(object sender, RoutedEventArgs e)
        {                 
            Write4.Foreground = Brushes.Red;
            Write.IsChecked = true;
            Write2.Foreground = Brushes.Red;
            Write3.Foreground = Brushes.Red;
        }

       
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            //for windows 8 only
            System.Windows.Automation.AutomationElement asForm =
            System.Windows.Automation.AutomationElement.FromHandle(new WindowInteropHelper(this).Handle);
            InputPanelConfigurationLib.InputPanelConfiguration inputPanelConfig = new InputPanelConfigurationLib.InputPanelConfiguration();
            inputPanelConfig.EnableFocusTracking();
            
           
            


            /*
            PresentationSource source = PresentationSource.FromVisual(this);

            double dpiX, dpiY;
            if (source != null)
            {
                dpiX = 96.0 * source.CompositionTarget.TransformToDevice.M11;
                dpiY = 96.0 * source.CompositionTarget.TransformToDevice.M22;

                if (dpiX != 96 || dpiY != 96)
                {
                    MessageBox.Show("ReWalk application requires a screen resolution of 1366 x 768 and text size of 'Smaller – 100%'" + "\n" + "Please refer to the ReWalk Therapist Manual for further instructions.", "", MessageBoxButton.OK, MessageBoxImage.Hand);
                    CheckUSBConnectedTime.IsEnabled = false;
                    deviceData.Disconnect();
                    progressform.Close();
                    GenerateRCAddressIDForm.Close();
                    MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
                    MCUDownloadingProcessWindow.Close();

                    Application.Current.Shutdown();
                    System.Diagnostics.Process.GetCurrentProcess().Kill();

                 }
                }

            CurrentScreenHeight = SystemParameters.FullPrimaryScreenHeight;
            CurrentScreenWidth = SystemParameters.FullPrimaryScreenWidth;

            if (CurrentScreenWidth != (double)1366) // ||  CurrentScreenHeight < (double)700 )
            {
                MessageBox.Show("ReWalk application requires a screen resolution of 1366 x 768 and text size of 'Smaller – 100%'" + "\n" + "Please refer to the ReWalk Therapist Manual for further instructions.", "", MessageBoxButton.OK, MessageBoxImage.Hand);
                CheckUSBConnectedTime.IsEnabled = false;
                deviceData.Disconnect();
                progressform.Close();
                GenerateRCAddressIDForm.Close();
                MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
                MCUDownloadingProcessWindow.Close();

                Application.Current.Shutdown();
                System.Diagnostics.Process.GetCurrentProcess().Kill();
                //  Resolution.CResolution ChangeRes = new Resolution.CResolution(1366, 768);//desired screen resolution to run the GUI proparly
            }

      */
        }

        private void TechBack_Click(object sender, RoutedEventArgs e)
        {
         //   TbRightMenu.IsChecked = false;
        }

        private void UnitsSelectButton_Click(object sender, RoutedEventArgs e)
        {
            Units.Visibility = Visibility.Visible;
        }

        private void LanguageSelectButton_Click(object sender, RoutedEventArgs e)
        {
            Languages.Visibility = Visibility.Visible;
        }

        private void HelpaboutButton_Click(object sender, RoutedEventArgs e)
        {
            Help_About.Visibility = Visibility.Visible;
        }

        private void LHSN_TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            
            e.Handled = !AreAllValidNumericChars(e.Text);
            base.OnPreviewTextInput(e);

           
        }
        private bool AreAllValidNumericChars(string str)
        {
            foreach (char c in str)
            {
                if (!Char.IsNumber(c)) return false;
            }

            return true;
        }

        private void UserIDInput_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !AreAllValidNumericChars(e.Text);
            base.OnPreviewTextInput(e);
        }

        private void UserWightInPut_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
         //   e.Handled = !AreAllValidNumericChars(e.Text);
           // base.OnPreviewTextInput(e);
        }

        private void UserHeightInPut_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !AreAllValidNumericChars(e.Text);
            base.OnPreviewTextInput(e);
        }

        private void RCSN_TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !AreAllValidNumericChars(e.Text);
            base.OnPreviewTextInput(e);
        }

        private void RHSN_TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !AreAllValidNumericChars(e.Text);
            base.OnPreviewTextInput(e);
        }

        private void LKSN_TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !AreAllValidNumericChars(e.Text);
            base.OnPreviewTextInput(e);
        }

      

        private void BlueListDevices1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (BlueView.SenderViewModel.SelectDevice != null)
            {
                if (BlueView.SenderViewModel.SelectDevice.DeviceName.Contains("BC-"))
                {
                    if (DeActiveBlueButton1.Visibility == Visibility.Collapsed)
                    {
                        ActiveBlueButton1.Visibility = Visibility.Visible;
                        DeActiveBlueButton1.Visibility = Visibility.Collapsed;
                        BlueListDevices1.IsEnabled = true;
                    }

                }
            }
            else
            {
                ActiveBlueButton1.Visibility = Visibility.Collapsed;
                DeActiveBlueButton1.Visibility = Visibility.Collapsed;
         //       BlueView.ReceiverViewModel.StopCommand.Execute(null);
            }
         
        }

     
        private void DeactiveBlueButton_Click(object sender, RoutedEventArgs e)
        {

                BlueView.ReceiverViewModel.StopCommand.Execute("gg");
                SenderSendText.IsEnabled = false;
                SenderSendText.Text = "";
                SenderSendButton.IsEnabled = false;
                DeActiveBlueButton1.Visibility = Visibility.Collapsed;
                ActiveBlueButton1.Visibility = Visibility.Collapsed;
                BlueListDevices1.IsEnabled = true;
                BlueListDevices1.UnselectAll();
                BitmapImage b = new BitmapImage();
                b.BeginInit();
                b.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Bluetooth-2.ico");// "Bluetooth_alt.png");
                b.EndInit();
                BlueIconPictureToShow.Source = b;
                ShowPYTH.Content = "New";
                ActiveInactiveStrokeButton.IsChecked = false;
                ActiveInactiveStrokeButton.Visibility = Visibility.Collapsed;
                ActiveInactiveStrokeText.Visibility = Visibility.Collapsed;
                ActiveInactiveStrokeButton.IsEnabled = false;
            
        }

        private void ActiveBlueButton1_Click(object sender, RoutedEventArgs e)
        {

            Stopwatch stopwatch1 = new Stopwatch();
            stopwatch1.Start();
           
            BlueView.ReceiverViewModel.Status = "NOCONN";
            BlueView.ReceiverViewModel.StartCommand.Execute("gg");
             while (BlueView.ReceiverViewModel.Status == "NOCONN"  && stopwatch1.ElapsedMilliseconds < 8000) ;//wait max 3 seconds to set connection
            stopwatch1.Reset();

            if (BlueView.ReceiverViewModel.Status == "CONNECTED")
             {
                ActiveBlueButton1.Visibility = Visibility.Collapsed;
                DeActiveBlueButton1.Visibility = Visibility.Visible;
                SenderSendText.IsEnabled = true;
                SenderSendButton.IsEnabled = true;
                
                BitmapImage b = new BitmapImage();
                b.BeginInit();
                b.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Bluetooth_alt.png");
                b.EndInit();
            
                BlueIconPictureToShow.Source = b;
                ShowPYTH.Content = "Show";
                TbRightMenu.IsChecked = false;
                ActiveInactiveStrokeButton.Visibility = Visibility.Visible;
                ActiveInactiveStrokeText.Visibility = Visibility.Visible;
                ActiveInactiveStrokeButton.IsEnabled = true;
                BlueListDevices1.IsEnabled = false;
             }
        }

        private void Blue_image_loaded(object sender, RoutedEventArgs e)
        {
            BitmapImage b = new BitmapImage();
            b.BeginInit();
            b.UriSource = new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Bluetooth-2.ico" );// "Bluetooth_alt.png");
            b.EndInit();
            var image = sender as Image;
            // ... Assign Source.
            image.Source = b;
          //  BlueIconPictureToShow.Source = b;

        }

     

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            // ... Get DatePicker reference.
            var picker = sender as DatePicker;

            // ... Get nullable DateTime from SelectedDate.
            DateTime? date = picker.SelectedDate;
            if (date == null)
            {
                // ... A null object.
                //   this.Title = "No date";
                UserWightInPut.Text = "No Date";
            }
            else
            {
                // ... No need to display the time.
                UserWightInPut.Text = date.Value.ToShortDateString();
                UserWightOutPut.Text = date.Value.ToShortDateString(); 
            }
        }

       
        private void Write_session_graphs_data_init(string[] session_graph_data_log_par , string[] session_graph_data_log_par_units)
        {
            DateTime CurrentTimeDate;
            CurrentTimeDate = DateTime.Now;
            int dd = Default_Intial_Graph_Parameters.Length;
            try
            {
             //   File.OpenWrite(KST_Config_Data_File_Name); File.AppendText(KST_Config_Data_File_Name);
                using (StreamWriter kst_constant_configuration_data_file = File.CreateText(KST_Config_Data_File_Name))
                {
 
                    kst_constant_configuration_data_file.WriteLine("Data Plots Generated at :".PadRight(30) + CurrentTimeDate + "\n");

                    for (int i = 0; i < dd; i++)
                    {
                        kst_constant_configuration_data_file.Write(session_graph_data_log_par[i] + " ; ");
             //           kst_constant_configuration_data_file.WriteLine(session_graph_data_log_par_units[i] + " ; ");
                    }

                    kst_constant_configuration_data_file.WriteLine();


                    for (int i = 0; i < dd; i++)
                    {
                   //     kst_constant_configuration_data_file.WriteLine(session_graph_data_log_par[i] + " ; ");
                        kst_constant_configuration_data_file.Write(session_graph_data_log_par_units[i] + " ; ");
                    }

                    kst_constant_configuration_data_file.WriteLine();
                    //   kst_constant_configuration_data_file.WriteLine(session_graph_data_log_par[0] + " ; " + session_graph_data_log_par[1] + " ; " + session_graph_data_log_par[2] + " ; " + session_graph_data_log_par[3] + ";" + session_graph_data_log_par[4]);
                    //   kst_constant_configuration_data_file.WriteLine(session_graph_data_log_par_units[0] + " ; " + session_graph_data_log_par_units[1] + " ; " + session_graph_data_log_par_units[2] + " ; " + session_graph_data_log_par_units[3] + ";" + session_graph_data_log_par_units[4]);
                }



            }
            catch(Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void Write_session_log_data_init()
        {

            DateTime CurrentTimeDate;
            CurrentTimeDate = DateTime.Now;
            try
            {
             using (StreamWriter stroke_logger_data_file = File.CreateText(StrokeCurrentSession_LoggerDataFile))
                {

                    stroke_logger_data_file.WriteLine("Data Plots Generated at :".PadRight(30) + CurrentTimeDate + "\n");
                    stroke_logger_data_file.WriteLine("Patient Name: "+ US_ID.Text + "\n");
                    stroke_logger_data_file.WriteLine("Patient ID: " + US_ID1.Text + "\n");
                    if (M.IsChecked == true)
                        stroke_logger_data_file.WriteLine("Patient Gender: " + "Male" + "\n");
                    else if(M.IsChecked  == false)
                        stroke_logger_data_file.WriteLine("Patient Gender: " + "Female" + "\n");
                    stroke_logger_data_file.WriteLine("Data Of Birth: " + UserWightOutPut.Text + "\n");
                    stroke_logger_data_file.WriteLine("Injured Side: " + UserHeightOutPut.Text + "\n");
                    stroke_logger_data_file.WriteLine("Weight: " + PatientWeightBox.Text + "\n");
                    stroke_logger_data_file.WriteLine("Height: " + PatientHeightBox.Text + "\n");
                    stroke_logger_data_file.WriteLine("Waist Strap Size: " + PatientWaistStrapSize.Text + "\n");
                    stroke_logger_data_file.WriteLine("Foot Plate Size: " + PatientFootPlateSize.Text + "\n");
                    stroke_logger_data_file.WriteLine("Leg Length: " + PatientLegLengthBox.Text + "\n");
                    stroke_logger_data_file.WriteLine("Waist Circumference: " + PatientWaistCircumference.Text + "\n");
                    stroke_logger_data_file.WriteLine("Calf Wrap Size: " + PatientCalfWrapsize.Text + "\n");
                    stroke_logger_data_file.WriteLine("Cable Length: " + PatientCableLengthBox.Text + "\n");
                    stroke_logger_data_file.WriteLine("Calf Circumference: " + PatientCalfCircumference.Text + "\n");
                    stroke_logger_data_file.WriteLine("Foot size: " + PatientFootSize.Text + "\n");
                    stroke_logger_data_file.WriteLine("Reserved 1: " + PatientResv1.Text + "\n");
                    stroke_logger_data_file.WriteLine("Reserved 2: " + PatientResv2.Text + "\n");
                    stroke_logger_data_file.WriteLine(" ____________________________________________________\n");

                }

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }


        private void Write_session_log_data_online(string newdata)
        {
            try
            {
                using (StreamWriter stroke_logger_data_file = File.AppendText(StrokeCurrentSession_LoggerDataFile))
                {

                    stroke_logger_data_file.WriteLine(newdata + "\n");
                }

            }  
            catch(Exception ec)
            {
                MessageBox.Show(ec.Message);
            }



        }


        string StrokeCurrentSessionFolder;
        string StrokeCurrentSession_GraphData;
        string StrokeCurrentSession_LoggerDataFile;
        bool Stroke_Log_Data_Mode = false;
        int n_append_log_graph = 0;
        private void ActiveInactiveStrokeButton_Checked(object sender, RoutedEventArgs e)
        {
            try
            {


                DirectoryInfo dir_info;
                DateTime CurrentTimeDate;
                CurrentTimeDate = DateTime.Now;
                                  

                if (US_ID.Text != "")
                  {
                  
                    ActiveInactiveStrokeText.Foreground = Brushes.Green;
                    ActiveInactiveStrokeText.Text = "Active";

                    StrokeCurrentSessionFolder = StrokeFolder + "\\" + US_ID.Text;
                    //open new folder for seesion that will include log and graph files of this session
                    if (!Directory.Exists(StrokeCurrentSessionFolder))
                        dir_info = Directory.CreateDirectory(StrokeCurrentSessionFolder);
                    if (BlueView.SenderViewModel.SelectDevice != null)
                    {
                        BlueView.ReceiverViewModel.PF_FORCE = 2; //Active mode                       
                        BlueView.ReceiverViewModel.PF_FORCE_String_For_Display = "ACTIVE_MODE(2)";

                        Commit_Blue_Data();
                        //instead of 
                        //BlueView.SenderViewModel.Data = "MODE#1";
                        //BlueView.SenderViewModel.SendData();
                      
                    }
                    StrokeCurrentSession_LoggerDataFile = StrokeCurrentSessionFolder + "\\" + US_ID.Text + "_log" + ".txt";
                    if (!File.Exists(StrokeCurrentSession_LoggerDataFile))
                    {
                        Write_session_log_data_init();
                        Stroke_Log_Data_Mode = true;
                    }
                    else
                    {
                        Stroke_Log_Data_Mode = true;
                    }
                    Write_session_log_data_online("Session was Activated at" + CurrentTimeDate + ".\n");
                }

                else
                {
                    ActiveInactiveStrokeButton.IsChecked = false;
                    MessageBox.Show("Please fill all patient data before activating!"+"\n"+ "Parietic side is mandatory!");
                }
              

                //GraphTimerUpdate.IsEnabled = true;
               // GraphTimerUpdate.Start();
            }
            catch (Exception exe)
            {
                
                GraphTimerUpdate.Stop();
                MessageBox.Show("Error: " + exe.Message);
            }

        }

        private void ActiveInactiveStrokeButton_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {

                DateTime CurrentTimeDate;
               CurrentTimeDate = DateTime.Now;
          
               
                

                if (US_ID.Text != "")
                {
                         
                    ActiveInactiveStrokeText.Foreground = Brushes.Red;
                    ActiveInactiveStrokeText.Text = "Inactive";
                    GraphTimerUpdate.Stop();
                    Write_session_log_data_online("Session was Deactivated at " + CurrentTimeDate + ".\n");
                    Write_session_log_data_online(" ********************************************************************************\n");
                    if (BlueView.SenderViewModel.SelectDevice != null)
                    {
                        BlueView.ReceiverViewModel.PF_FORCE = 0; //Inactive mode                       
                        BlueView.ReceiverViewModel.PF_FORCE_String_For_Display = "INACTIVE_MODE(0)";

                        Commit_Blue_Data();
                        //instead of 
                        //BlueView.SenderViewModel.Data = "MODE#0";
                        //BlueView.SenderViewModel.SendData();
                    }
                    StrokeCurrentSession_GraphData = StrokeCurrentSessionFolder + "\\" + US_ID.Text + "_graph"+ ".txt";
                    if (!File.Exists(StrokeCurrentSession_GraphData))
                    {
                        File.Copy(KST_Config_Data_File_Name, StrokeCurrentSession_GraphData);
                        n_append_log_graph = 0;
                    }
                    else
                    {
                        n_append_log_graph = n_append_log_graph + 1;
                        StrokeCurrentSession_GraphData = StrokeCurrentSessionFolder + "\\" + US_ID.Text + "_graph" + n_append_log_graph.ToString() +  ".txt";
                        File.Copy(KST_Config_Data_File_Name, StrokeCurrentSession_GraphData);
                       
                    }
                }
                else
                {
                    
                 //   ActiveInactiveStrokeButton.IsChecked = true;
                 //   MessageBox.Show("Please fill all patient data before activating!"+"\n"+ "Parietic side is mandatory!");
                }
            }
            catch(Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void Leftinj_Checked(object sender, RoutedEventArgs e)
        {
            Rightinj.IsChecked = false;
            UserHeightOutPut.Text = " LEFT ";
            BlueView.ReceiverViewModel.DF_PULL_LENGTH =0;
            BlueView.ReceiverViewModel.DF_PULL_LENGTH_String_For_Display = "LEFT_PARIETIC(0)";


        }
        private void Leftinj_Unchecked(object sender, RoutedEventArgs e)
        {
            
            UserHeightOutPut.Text = " ";
        }

        private void Rightinj_Checked(object sender, RoutedEventArgs e)
        {
            Leftinj.IsChecked = false;
            UserHeightOutPut.Text = " RIGHT ";
            BlueView.ReceiverViewModel.DF_PULL_LENGTH = 1;          
            BlueView.ReceiverViewModel.DF_PULL_LENGTH_String_For_Display = "RIGHT_PARIETIC(1)";
           
        }

        private void Rightinj_Unchecked(object sender, RoutedEventArgs e)
        {
            UserHeightOutPut.Text = " ";
        }

        private void ShowGraphs_Clik(object sender, RoutedEventArgs e)
        {
       //     GraphTimerUpdate.IsEnabled = false;
         //   GraphTimerUpdate.Start();
            Write_session_graphs_data_init(Default_Intial_Graph_Parameters, Default_Intial_Graph_Parameters_Units);
            Process.Start(KST_App_Run_File_File_Name);
        }

        private void SenderSendButton_Click(object sender, RoutedEventArgs e)
        {
            if (BlueView.SenderViewModel.SelectDevice != null)
            {
                if (SenderSendText.Text != null)
                {
                    BlueView.SenderViewModel.Data = SenderSendText.Text;
                    BlueView.SenderViewModel.SendData();
                //    SenderSendText.Text = "";
                }
            }
        }

        private void StrokeModeToggle_Checked(object sender, RoutedEventArgs e)
        {
            BlueView.ReceiverViewModel.RESTORE_SYSTEM_OPERATION_MODE = 1;
        }

        private void StrokeModeToggle_Unchecked(object sender, RoutedEventArgs e)
        {
            BlueView.ReceiverViewModel.RESTORE_SYSTEM_OPERATION_MODE = 0;
        }

        private void ShowPYTH_Click(object sender, RoutedEventArgs e)
        {
            WASON.Visibility = Visibility.Visible;
            WASOFF.Visibility = Visibility.Collapsed;
            SystemInfo.Visibility = Visibility.Visible;

            if (BlueView.ReceiverViewModel.Data == "Can receive data.")
            {
                ActiveInactiveStrokeText.Visibility = Visibility.Visible;
                ActiveInactiveStrokeButton.Visibility = Visibility.Visible;
            }

            if (ShowPYTH.Content.ToString() == "New")
            {
                WASON1.Visibility = Visibility.Visible;
                WASOFF1.Visibility = Visibility.Collapsed;
                BlueListDevices1.UnselectAll();
                BlueListDevices1.Visibility = Visibility.Visible;
                WalkAfterStuckToggle1.IsChecked = true;
                Messenger.Default.Send(true ? new Message(true) : new Message(false));
                TbRightMenu.IsChecked = true;
            }
           

        }

    
       
       
        private void RKSN_TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !AreAllValidNumericChars(e.Text);
            base.OnPreviewTextInput(e);
        }

        private void Sn2_TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !AreAllValidNumericChars(e.Text);
            base.OnPreviewTextInput(e);
        }

        private void Sn3_TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !AreAllValidNumericChars(e.Text);
            base.OnPreviewTextInput(e);
        }

        private void InfSN_TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !AreAllValidNumericChars(e.Text);
            base.OnPreviewTextInput(e);
        }









        /********************************              Bluetooth Stroke Gui **************************/
         ViewModelLocator BlueView = new ViewModelLocator();
         
        private void RestartBlueSearch_Click(object sender, RoutedEventArgs e)
        {

            string[] mcupt_strings  =  new string[100];
            string[] mcuset_strings = new string[100];
            string[] mcusta_strings = new string[100];
         

            if (BlueView.SenderViewModel.SelectDevice != null)
            {


                BlueView.SenderViewModel.Data = "";
            
                mcuset_strings[0] = "MCSP";
                mcuset_strings[1] = BlueView.ReceiverViewModel.PF_FORCE.ToString().PadLeft(5);
                mcuset_strings[2] = BlueView.ReceiverViewModel.PF_TIMING.ToString().PadLeft(5);
                mcuset_strings[3] = BlueView.ReceiverViewModel.PF_PRETENSION_LEVEL.ToString().PadLeft(5);
                mcuset_strings[4] = BlueView.ReceiverViewModel.DF_PULL_LENGTH.ToString().PadLeft(5);
                mcuset_strings[5] = BlueView.ReceiverViewModel.DF_TIMING.ToString().PadLeft(5);
                mcuset_strings[6] = BlueView.ReceiverViewModel.DF_PRETENSION_LEVEL.ToString().PadLeft(5);
                mcuset_strings[7] = BlueView.ReceiverViewModel.RESTORE_SYSTEM_OPERATION_MODE.ToString().PadLeft(5);

                BlueView.SenderViewModel.Data += mcuset_strings[0] + '#' + mcuset_strings[1] + '#' + mcuset_strings[2] + '#' + mcuset_strings[3] + '#' +
                                                 mcuset_strings[4] + '#' + mcuset_strings[5] + '#' + mcuset_strings[6] + '#'+ mcuset_strings[7]+ '#';

  
                BlueView.SenderViewModel.SendData();



       
             
            }
            else
            {
               // MessageBox.Show("You have to select one device first!");
            }

          
        }
        private void Commit_Blue_Data()
        {
           string[] mcuset_strings = new string[100];
       

            if (BlueView.SenderViewModel.SelectDevice != null)
            {


                BlueView.SenderViewModel.Data = "";

                mcuset_strings[0] = "MCSP";
                mcuset_strings[1] = BlueView.ReceiverViewModel.PF_FORCE.ToString().PadLeft(5);
                mcuset_strings[2] = BlueView.ReceiverViewModel.PF_TIMING.ToString().PadLeft(5);
                mcuset_strings[3] = BlueView.ReceiverViewModel.PF_PRETENSION_LEVEL.ToString().PadLeft(5);
                mcuset_strings[4] = BlueView.ReceiverViewModel.DF_PULL_LENGTH.ToString().PadLeft(5);
                mcuset_strings[5] = BlueView.ReceiverViewModel.DF_TIMING.ToString().PadLeft(5);
                mcuset_strings[6] = BlueView.ReceiverViewModel.DF_PRETENSION_LEVEL.ToString().PadLeft(5);
                mcuset_strings[7] = BlueView.ReceiverViewModel.RESTORE_SYSTEM_OPERATION_MODE.ToString().PadLeft(5);

                BlueView.SenderViewModel.Data += mcuset_strings[0] + '#' + mcuset_strings[1] + '#' + mcuset_strings[2] + '#' + mcuset_strings[3] + '#' +
                                                 mcuset_strings[4] + '#' + mcuset_strings[5] + '#' + mcuset_strings[6] + '#' + mcuset_strings[7] + '#';


                BlueView.SenderViewModel.SendData();





            }
            else
            {
                // MessageBox.Show("You have to select one device first!");
            }

        }



    }
}

