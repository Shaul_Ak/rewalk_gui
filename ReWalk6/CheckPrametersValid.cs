﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Win32;
using Rewalk.Device;
using System.Diagnostics;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using RewalkControls;
using System.Threading;
using System.Windows.Input;
using System.Security;
using System.Text;
using System.Windows.Media;
using System.Windows.Documents;
using System.Net;
using System.Net.Mail;
using System.Windows.Markup;

namespace ReWalk6
{


    public class CheckPrametersValid
    {

        public CheckPrametersValid()
        {
        }


        private Int16 StepTimeMin;
        private Int16 StepTimeMax;
        private Int16 DelayBetweenStepsMin;
        private Int16 DelayBetweenStepsMax;
        private Int16 WalkCurrentThrMin;
        private Int16 StandCurrentThrMin;
        private Int16 StandCurrentThrMax;
        private Int16 TrainingStepTimeMin;
        private Int16 TrainingStepTimeMax;
        private Int16 WalkCurrentThrMax;
        private Int16 FirststepFlexionMin;
        private Int16 FirststepFlexionMax;
        private Int16 HipFlextionAngleMin;
        private Int16 HipFlextionAngleMax;
        private Int16 KneeFlextionAngleMin;
        private Int16 KneeFlextionAngleMax;
        private Int16 TiltDeltaMin;
        private Int16 TiltDeltaMax;
        private Int16 TiltTimeOutMin;
        private Int16 TiltTimeOutMax;
        private Int16 HipMaxFlextionAngleMin;
        private Int16 HipMaxFlextionAngleMax;
        private Int16 HipFinalFlextionAngleMin;
        private Int16 HipFinalFlextionAngleMax;
        private Int16 BeginnersStepTimeMin;
        private Int16 BeginnersStepTimeMax;
        private Int16 FallDetectionThrMin;
        private Int16 FallDetectionThrMax;
        private Int16 SystemCurrentThrMin;
        private Int16 SystemCurrentThrMax;
        private Int16 ASCSpeedMin;
        private Int16 ASCSpeedMax;
        private Int16 DSCSpeedMin;
        private Int16 DSCSpeedMax;
        private Int16 SysTypeMin;
        private Int16 SysTypeMax;
        private Int16 StairHeightMin;
        private Int16 StairHeightMax;
        private Int16 ASCDSCStairsMin;
        private Int16 ASCDSCStairsMax;

        private string PUpperStrapHolderSizeMin;
        private string PUpperStrapHolderSizeMax;
        private Int16 PUpperStrapHolderTypeMin;
        private Int16 PUpperStrapHolderTypeMax;
        private string PAboveKneeBracketTypeMin;
        private string PAboveKneeBracketTypeMax;
        private string RAboveKneeBracketTypeMin;
        private string RAboveKneeBracketTypeMax;
        private string PAboveKneeBracketAnteriorMin;
        private string PAboveKneeBracketAnteriorMax;
        private string RAboveKneeBracketAnteriorMin;
        private string RAboveKneeBracketAnteriorMax;
        private string PKneeBracketTypeMin;
        private string PKneeBracketTypeMax;
        private string RKneeBracketTypeMin;
        private string RKneeBracketTypeMax;
        private string PKneeBracketAnteriorPositionMin;
        private string PKneeBracketAnteriorPositionMax;
        private string RKneeBracketAnteriorPositionMin;
        private string RKneeBracketAnteriorPositionMax;
        private string PKneeBracketLateralPositionMin;
        private string PKneeBracketLateralPositionMax;
        private Int16 PPelvicSizeMin;
        private Int16 PPelvicSizeMax;
        private Int16 RPelvicSizeMin;
        private Int16 RPelvicSizeMax;
        private Int16 PPelvicAnteriorPositionMin;
        private Int16 PPelvicAnteriorPositionMax;
        private string PPelvicVerticalPositionMin;
        private string PPelvicVerticalPositionMax;
        private Int16 PUpperLegLengthMin;
        private Int16 PUpperLegLengthMax;
        private Int16 RUpperLegLengthMin;
        private Int16 RUpperLegLengthMax;
        private Int16 PLowerLegLengthMin;
        private Int16 PLowerLegLengthMax;
        private Int16 RLowerLegLengthMin;
        private Int16 RLowerLegLengthMax;
        private string PFootPlateTypeMin;
        private string PFootPlateTypeMax;
        private string RFootPlateTypeMin;
        private string RFootPlateTypeMax;
        private string PFootPlateSizeMin;
        private string PFootPlateSizeMax;
        private string RFootPlateSizeMin;
        private string RFootPlateSizeMax;
        private Int16 PFootPlateDorsiFlexionPositionMin;
        private Int16 PFootPlateDorsiFlexionPositionMax;
        private Int16 RFootPlateDorsiFlexionPositionMin;
        private Int16 RFootPlateDorsiFlexionPositionMax;
        private Int16 ShoeLenghtMin;
        private Int16 ShoeLenghtMax;


        private Int16 SwitchingTimeMin;
        private Int16 SwitchingTimeMax;
        private Int16 PlacingRHMin;
        private Int16 PlacingRHMax;
        private Int16 PlacingRKMin;
        private Int16 PlacingRKMax;

        //Length
        private int HipFlextionAngleLength;
        private int KneeFlextionAngleLength;
        private int StepTimeLength;
        private int BeginnersStepTimeLength;
        private int FirststepFlexionLength;
        private int TiltDeltaLength;
        private int TiltTimeOutLength;
        private int FallDetectionThrLength;
        private int SystemCurrentThrLength;
        private int WalkCurrentThrLength;
        private int DelayBetweenStepsLength;
        private int HipMaxFlextionAngleLength;
        private int HipFinalFlextionAngleLength;
        private int ASCDSCStairsLength;
        private int ASCSpeedLength;
        private int DSCSpeedLength;
        private int SysTypeLength;
        private int StairHeightLength;
        private int SwitchingTimeLength;
        private int PlacingRHLength;
        private int PlacingRKLength;
        private int PUpperStrapHolderSizeLength;
        private int PUpperStrapHolderTypeLength;
        private int PAboveKneeBracketTypeLength;
        private int RAboveKneeBracketTypeLength;
        private int PAboveKneeBracketAnteriorLength;
        private int RAboveKneeBracketAnteriorLength;
        private int PKneeBracketTypeLength;
        private int RKneeBracketTypeLength;
        private int PKneeBracketAnteriorPositionLength;
        private int RKneeBracketAnteriorPositionLength;
        private int PKneeBracketLateralPositionLength;
        private int PPelvicSizeLength;
        private int RPelvicSizeLength;
        private int PPelvicAnteriorPositionLength;
        private int PPelvicVerticalPositionLength;
        private int PUpperLegLengthLength;
        private int RUpperLegLengthLength;
        private int PLowerLegLengthLength;
        private int RLowerLegLengthLength;
        private int PFootPlateTypeLength;
        private int RFootPlateTypeLength;
        private int PFootPlateSizeLength;
        private int RFootPlateSizeLength;
        private int PFootPlateDorsiFlexionPositionLength;
        private int RFootPlateDorsiFlexionPositionLength;
        private int ShoeLenghtLength;

        public Int16 HipFlextionAngleMin_Value
        {
            get { return HipFlextionAngleMin; }
            set { HipFlextionAngleMin = value; }
        }
        public Int16 HipFlextionAngleMax_Value
        {
            get { return HipFlextionAngleMax; }
            set { HipFlextionAngleMax = value; }
        }

        public Int16 KneeFlextionAngleMin_Value
        {
            get { return KneeFlextionAngleMin; }
            set { KneeFlextionAngleMin = value; }
        }
        public Int16 KneeFlextionAngleMax_Value
        {
            get { return KneeFlextionAngleMax; }
            set { KneeFlextionAngleMax = value; }
        }

        public Int16 StepTimeMin_Value
        {
            get { return StepTimeMin; }
            set { StepTimeMin = value; }
        }
        public Int16 StepTimeMax_Value
        {
            get { return StepTimeMax; }
            set { StepTimeMax = value; }
        }

        public Int16 BeginnersStepTimeMin_Value
        {
            get { return BeginnersStepTimeMin; }
            set { BeginnersStepTimeMin = value; }
        }
        public Int16 BeginnersStepTimeMax_Value
        {
            get { return BeginnersStepTimeMax; }
            set { BeginnersStepTimeMax = value; }
        }

        public Int16 FirststepFlexionMin_Value
        {
            get { return FirststepFlexionMin; }
            set { FirststepFlexionMin = value; }
        }
        public Int16 FirststepFlexionMax_Value
        {
            get { return FirststepFlexionMax; }
            set { FirststepFlexionMax = value; }
        }

        public Int16 TiltDeltaMin_Value
        {
            get { return TiltDeltaMin; }
            set { TiltDeltaMin = value; }
        }
        public Int16 TiltDeltaMax_Value
        {
            get { return TiltDeltaMax; }
            set { TiltDeltaMax = value; }
        }

        public Int16 TiltTimeOutMin_Value
        {
            get { return TiltTimeOutMin; }
            set { TiltTimeOutMin = value; }
        }
        public Int16 TiltTimeOutMax_Value
        {
            get { return TiltTimeOutMax; }
            set { TiltTimeOutMax = value; }
        }


        public Int16 FallDetectionThrMin_Value
        {
            get { return FallDetectionThrMin; }
            set { FallDetectionThrMin = value; }
        }
        public Int16 FallDetectionThrMax_Value
        {
            get { return FallDetectionThrMax; }
            set { FallDetectionThrMax = value; }
        }

        public Int16 SystemCurrentThrMin_Value
        {
            get { return SystemCurrentThrMin; }
            set { SystemCurrentThrMin = value; }
        }
        public Int16 SystemCurrentThrMax_Value
        {
            get { return SystemCurrentThrMax; }
            set { SystemCurrentThrMax = value; }
        }

        public Int16 WalkCurrentThrMin_Value
        {
            get { return WalkCurrentThrMin; }
            set { WalkCurrentThrMin = value; }
        }

        public Int16 StandCurrentThrMin_Value
        {
            get { return StandCurrentThrMin; }
            set { StandCurrentThrMin = value; }
        }
        public Int16 StandCurrentThrMax_Value
        {
            get { return StandCurrentThrMax; }
            set { StandCurrentThrMax = value; }
        }

        public Int16 TrainingStepTimeMin_Value
        {
            get { return TrainingStepTimeMin; }
            set { TrainingStepTimeMin = value; }
        }
        public Int16 TrainingStepTimeMax_Value
        {
            get { return TrainingStepTimeMax; }
            set { TrainingStepTimeMax = value; }
        }
        public Int16 WalkCurrentThrMax_Value
        {
            get { return WalkCurrentThrMax; }
            set { WalkCurrentThrMax = value; }
        }

        public Int16 DelayBetweenStepsMin_Value
        {
            get { return DelayBetweenStepsMin; }
            set { DelayBetweenStepsMin = value; }
        }
        public Int16 DelayBetweenStepsMax_Value
        {
            get { return DelayBetweenStepsMax; }
            set { DelayBetweenStepsMax = value; }
        }

        public Int16 HipMaxFlextionAngleMin_Value
        {
            get { return HipMaxFlextionAngleMin; }
            set { HipMaxFlextionAngleMin = value; }
        }
        public Int16 HipMaxFlextionAngleMax_Value
        {
            get { return HipMaxFlextionAngleMax; }
            set { HipMaxFlextionAngleMax = value; }
        }

        public Int16 HipFinalFlextionAngleMin_Value
        {
            get { return HipFinalFlextionAngleMin; }
            set { HipFinalFlextionAngleMin = value; }
        }
        public Int16 HipFinalFlextionAngleMax_Value
        {
            get { return HipFinalFlextionAngleMax; }
            set { HipFinalFlextionAngleMax = value; }
        }

        public Int16 ASCDSCStairsMin_Value
        {
            get { return ASCDSCStairsMin; }
            set { ASCDSCStairsMin = value; }
        }
        public Int16 ASCDSCStairsMax_Value
        {
            get { return ASCDSCStairsMax; }
            set { ASCDSCStairsMax = value; }
        }

        public Int16 ASCSpeedMin_Value
        {
            get { return ASCSpeedMin; }
            set { ASCSpeedMin = value; }
        }
        public Int16 ASCSpeedMax_Value
        {
            get { return ASCSpeedMax; }
            set { ASCSpeedMax = value; }
        }

        public Int16 DSCSpeedMin_Value
        {
            get { return DSCSpeedMin; }
            set { DSCSpeedMin = value; }
        }
        public Int16 DSCSpeedMax_Value
        {
            get { return DSCSpeedMax; }
            set { DSCSpeedMax = value; }
        }

        public Int16 SysTypeMin_Value
        {
            get { return SysTypeMin; }
            set { SysTypeMin = value; }
        }
        public Int16 SysTypeMax_Value
        {
            get { return SysTypeMax; }
            set { SysTypeMax = value; }
        }
        public Int16 StairHeightMin_Value
        {
            get { return StairHeightMin; }
            set { StairHeightMin = value; }
        }
        public Int16 StairHeightMax_Value
        {
            get { return StairHeightMax; }
            set { StairHeightMax = value; }
        }

        public Int16 SwitchingTimeMin_Value
        {
            get { return SwitchingTimeMin; }
            set { SwitchingTimeMin = value; }
        }
        public Int16 SwitchingTimeMax_Value
        {
            get { return SwitchingTimeMax; }
            set { SwitchingTimeMax = value; }
        }

        public Int16 PlacingRHMin_Value
        {
            get { return PlacingRHMin; }
            set { PlacingRHMin = value; }
        }
        public Int16 PlacingRHMax_Value
        {
            get { return PlacingRHMax; }
            set { PlacingRHMax = value; }
        }

        public Int16 PlacingRKMin_Value
        {
            get { return PlacingRKMin; }
            set { PlacingRKMin = value; }
        }
        public Int16 PlacingRKMax_Value
        {
            get { return PlacingRKMax; }
            set { PlacingRKMax = value; }
        }
        public string PUpperStrapHolderSizeMin_Value
        {
            get { return PUpperStrapHolderSizeMin; }
            set { PUpperStrapHolderSizeMin = value; }
        }
        public string PUpperStrapHolderSizeMax_Value
        {
            get { return PUpperStrapHolderSizeMax; }
            set { PUpperStrapHolderSizeMax = value; }
        }
        public Int16 PUpperStrapHolderTypeMin_Value
        {
            get { return PUpperStrapHolderTypeMin; }
            set { PUpperStrapHolderTypeMin = value; }
        }
        public Int16 PUpperStrapHolderTypeMax_Value
        {
            get { return PUpperStrapHolderTypeMax; }
            set { PUpperStrapHolderTypeMax = value; }
        }
        public string PAboveKneeBracketTypeMin_Value
        {
            get { return PAboveKneeBracketTypeMin; }
            set { PAboveKneeBracketTypeMin = value; }
        }
        public string PAboveKneeBracketTypeMax_Value
        {
            get { return PAboveKneeBracketTypeMax; }
            set { PAboveKneeBracketTypeMax = value; }
        }
        public string RAboveKneeBracketTypeMin_Value
        {
            get { return RAboveKneeBracketTypeMin; }
            set { RAboveKneeBracketTypeMin = value; }
        }
        public string RAboveKneeBracketTypeMax_Value
        {
            get { return RAboveKneeBracketTypeMax; }
            set { RAboveKneeBracketTypeMax = value; }
        }
        public string PAboveKneeBracketAnteriorMin_Value
        {
            get { return PAboveKneeBracketAnteriorMin; }
            set { PAboveKneeBracketAnteriorMin = value; }
        }
        public string PAboveKneeBracketAnteriorMax_Value
        {
            get { return PAboveKneeBracketAnteriorMax; }
            set { PAboveKneeBracketAnteriorMax = value; }
        }
        public string RAboveKneeBracketAnteriorMin_Value
        {
            get { return RAboveKneeBracketAnteriorMin; }
            set { RAboveKneeBracketAnteriorMin = value; }
        }
        public string RAboveKneeBracketAnteriorMax_Value
        {
            get { return RAboveKneeBracketAnteriorMax; }
            set { RAboveKneeBracketAnteriorMax = value; }
        }
        public string PKneeBracketTypeMin_Value
        {
            get { return PKneeBracketTypeMin; }
            set { PKneeBracketTypeMin = value; }
        }
        public string PKneeBracketTypeMax_Value
        {
            get { return PKneeBracketTypeMax; }
            set { PKneeBracketTypeMax = value; }
        }
        public string RKneeBracketTypeMin_Value
        {
            get { return RKneeBracketTypeMin; }
            set { RKneeBracketTypeMin = value; }
        }
        public string RKneeBracketTypeMax_Value
        {
            get { return RKneeBracketTypeMax; }
            set { RKneeBracketTypeMax = value; }
        }
        public string PKneeBracketAnteriorPositionMin_Value
        {
            get { return PKneeBracketAnteriorPositionMin; }
            set { PKneeBracketAnteriorPositionMin = value; }
        }
        public string PKneeBracketAnteriorPositionMax_Value
        {
            get { return PKneeBracketAnteriorPositionMax; }
            set { PKneeBracketAnteriorPositionMax = value; }
        }
        public string RKneeBracketAnteriorPositionMin_Value
        {
            get { return RKneeBracketAnteriorPositionMin; }
            set { RKneeBracketAnteriorPositionMin = value; }
        }
        public string RKneeBracketAnteriorPositionMax_Value
        {
            get { return RKneeBracketAnteriorPositionMax; }
            set { RKneeBracketAnteriorPositionMax = value; }
        }
        public string PKneeBracketLateralPositionMin_Value
        {
            get { return PKneeBracketLateralPositionMin; }
            set { PKneeBracketLateralPositionMin = value; }
        }
        public string PKneeBracketLateralPositionMax_Value
        {
            get { return PKneeBracketLateralPositionMax; }
            set { PKneeBracketLateralPositionMax = value; }
        }
        public Int16 PPelvicSizeMin_Value
        {
            get { return PPelvicSizeMin; }
            set { PPelvicSizeMin = value; }
        }
        public Int16 PPelvicSizeMax_Value
        {
            get { return PPelvicSizeMax; }
            set { PPelvicSizeMax = value; }
        }
        public Int16 RPelvicSizeMin_Value
        {
            get { return RPelvicSizeMin; }
            set { RPelvicSizeMin = value; }
        }
        public Int16 RPelvicSizeMax_Value
        {
            get { return RPelvicSizeMax; }
            set { RPelvicSizeMax = value; }
        }
        public Int16 PPelvicAnteriorPositionMin_Value
        {
            get { return PPelvicAnteriorPositionMin; }
            set { PPelvicAnteriorPositionMin = value; }
        }
        public Int16 PPelvicAnteriorPositionMax_Value
        {
            get { return PPelvicAnteriorPositionMax; }
            set { PPelvicAnteriorPositionMax = value; }
        }
        public string PPelvicVerticalPositionMin_Value
        {
            get { return PPelvicVerticalPositionMin; }
            set { PPelvicVerticalPositionMin = value; }
        }
        public string PPelvicVerticalPositionMax_Value
        {
            get { return PPelvicVerticalPositionMax; }
            set { PPelvicVerticalPositionMax = value; }
        }
        public Int16 PUpperLegLengthMin_Value
        {
            get { return PUpperLegLengthMin; }
            set { PUpperLegLengthMin = value; }
        }
        public Int16 PUpperLegLengthMax_Value
        {
            get { return PUpperLegLengthMax; }
            set { PUpperLegLengthMax = value; }
        }
        public Int16 RUpperLegLengthMin_Value
        {
            get { return RUpperLegLengthMin; }
            set { RUpperLegLengthMin = value; }
        }
        public Int16 RUpperLegLengthMax_Value
        {
            get { return RUpperLegLengthMax; }
            set { RUpperLegLengthMax = value; }
        }
        public Int16 PLowerLegLengthMin_Value
        {
            get { return PLowerLegLengthMin; }
            set { PLowerLegLengthMin = value; }
        }
        public Int16 PLowerLegLengthMax_Value
        {
            get { return PLowerLegLengthMax; }
            set { PLowerLegLengthMax = value; }
        }
        public Int16 RLowerLegLengthMin_Value
        {
            get { return RLowerLegLengthMin; }
            set { RLowerLegLengthMin = value; }
        }
        public Int16 RLowerLegLengthMax_Value
        {
            get { return RLowerLegLengthMax; }
            set { RLowerLegLengthMax = value; }
        }
        public string PFootPlateTypeMin_Value
        {
            get { return PFootPlateTypeMin; }
            set { PFootPlateTypeMin = value; }
        }
        public string PFootPlateTypeMax_Value
        {
            get { return PFootPlateTypeMax; }
            set { PFootPlateTypeMax = value; }
        }
        public string RFootPlateTypeMin_Value
        {
            get { return RFootPlateTypeMin; }
            set { RFootPlateTypeMin = value; }
        }
        public string RFootPlateTypeMax_Value
        {
            get { return RFootPlateTypeMax; }
            set { RFootPlateTypeMax = value; }
        }
        public string PFootPlateSizeMin_Value
        {
            get { return PFootPlateSizeMin; }
            set { PFootPlateSizeMin = value; }
        }
        public string PFootPlateSizeMax_Value
        {
            get { return PFootPlateSizeMax; }
            set { PFootPlateSizeMax = value; }
        }
        public string RFootPlateSizeMin_Value
        {
            get { return RFootPlateSizeMin; }
            set { RFootPlateSizeMin = value; }
        }
        public string RFootPlateSizeMax_Value
        {
            get { return RFootPlateSizeMax; }
            set { RFootPlateSizeMax = value; }
        }
        public Int16 PFootPlateDorsiFlexionPositionMin_Value
        {
            get { return PFootPlateDorsiFlexionPositionMin; }
            set { PFootPlateDorsiFlexionPositionMin = value; }
        }
        public Int16 PFootPlateDorsiFlexionPositionMax_Value
        {
            get { return PFootPlateDorsiFlexionPositionMax; }
            set { PFootPlateDorsiFlexionPositionMax = value; }
        }
        public Int16 RFootPlateDorsiFlexionPositionMin_Value
        {
            get { return RFootPlateDorsiFlexionPositionMin; }
            set { RFootPlateDorsiFlexionPositionMin = value; }
        }
        public Int16 RFootPlateDorsiFlexionPositionMax_Value
        {
            get { return RFootPlateDorsiFlexionPositionMax; }
            set { RFootPlateDorsiFlexionPositionMax = value; }
        }
        public Int16 ShoeLenghtMin_Value
        {
            get { return ShoeLenghtMin; }
            set { ShoeLenghtMin = value; }
        }
        public Int16 ShoeLenghtMax_Value
        {
            get { return ShoeLenghtMax; }
            set { ShoeLenghtMax = value; }
        }

        /// <summary>
        /// Length
        /// </summary>



        public int HipFlextionAngleLength_Value
        {
            get { return HipFlextionAngleLength; }
            set { HipFlextionAngleLength = value; }
        }


        public int KneeFlextionAngleLength_Value
        {
            get { return KneeFlextionAngleLength; }
            set { KneeFlextionAngleLength = value; }
        }


        public int StepTimeLength_Value
        {
            get { return StepTimeLength; }
            set { StepTimeLength = value; }
        }


        public int BeginnersStepTimeLength_Value
        {
            get { return BeginnersStepTimeLength; }
            set { BeginnersStepTimeLength = value; }
        }

        public int FirststepFlexionLength_Value
        {
            get { return FirststepFlexionLength; }
            set { FirststepFlexionLength = value; }
        }

        public int TiltDeltaLength_Value
        {
            get { return TiltDeltaLength; }
            set { TiltDeltaLength = value; }
        }



        public int TiltTimeOutLength_Value
        {
            get { return TiltTimeOutLength; }
            set { TiltTimeOutLength = value; }
        }



        public int FallDetectionThrLength_Value
        {
            get { return FallDetectionThrLength; }
            set { FallDetectionThrLength = value; }
        }

        public int SystemCurrentThrLength_Value
        {
            get { return SystemCurrentThrLength; }
            set { SystemCurrentThrLength = value; }
        }


        public int WalkCurrentThrLength_Value
        {
            get { return WalkCurrentThrLength; }
            set { WalkCurrentThrLength = value; }
        }


        public int DelayBetweenStepsLength_Value
        {
            get { return DelayBetweenStepsLength; }
            set { DelayBetweenStepsLength = value; }
        }

        public int HipMaxFlextionAngleLength_Value
        {
            get { return HipMaxFlextionAngleLength; }
            set { HipMaxFlextionAngleLength = value; }
        }


        public int HipFinalFlextionAngleLength_Value
        {
            get { return HipFinalFlextionAngleLength; }
            set { HipFinalFlextionAngleLength = value; }
        }


        public int ASCDSCStairsLength_Value
        {
            get { return ASCDSCStairsLength; }
            set { ASCDSCStairsLength = value; }
        }


        public int ASCSpeedLength_Value
        {
            get { return ASCSpeedLength; }
            set { ASCSpeedLength = value; }
        }


        public int DSCSpeedLength_Value
        {
            get { return DSCSpeedLength; }
            set { DSCSpeedLength = value; }
        }


        public int SysTypeLength_Value
        {
            get { return SysTypeLength; }
            set { SysTypeLength = value; }
        }
        public int StairHeightLength_Value
        {
            get { return StairHeightLength; }
            set { StairHeightLength = value; }
        }


        public int SwitchingTimeLength_Value
        {
            get { return SwitchingTimeLength; }
            set { SwitchingTimeLength = value; }
        }


        public int PlacingRHLength_Value
        {
            get { return PlacingRHLength; }
            set { PlacingRHLength = value; }
        }


        public int PlacingRKLength_Value
        {
            get { return PlacingRKLength; }
            set { PlacingRKLength = value; }
        }


        public int PUpperStrapHolderSizeLength_Value
        {
            get { return PUpperStrapHolderSizeLength; }
            set { PUpperStrapHolderSizeLength = value; }
        }
        public int PUpperStrapHolderTypeLength_Value
        {
            get { return PUpperStrapHolderTypeLength; }
            set { PUpperStrapHolderTypeLength = value; }
        }
        public int PAboveKneeBracketTypeLength_Value
        {
            get { return PAboveKneeBracketTypeLength; }
            set { PAboveKneeBracketTypeLength = value; }
        }
        public int RAboveKneeBracketTypeLength_Value
        {
            get { return RAboveKneeBracketTypeLength; }
            set { RAboveKneeBracketTypeLength = value; }
        }
        public int PAboveKneeBracketAnteriorLength_Value
        {
            get { return PAboveKneeBracketAnteriorLength; }
            set { PAboveKneeBracketAnteriorLength = value; }
        }


        public int RAboveKneeBracketAnteriorLength_Value
        {
            get { return RAboveKneeBracketAnteriorLength; }
            set { RAboveKneeBracketAnteriorLength = value; }
        }
        public int PKneeBracketTypeLength_Value
        {
            get { return PKneeBracketTypeLength; }
            set { PKneeBracketTypeLength = value; }
        }
        public int RKneeBracketTypeLength_Value
        {
            get { return RKneeBracketTypeLength; }
            set { RKneeBracketTypeLength = value; }
        }
        public int PKneeBracketAnteriorPositionLength_Value
        {
            get { return PKneeBracketAnteriorPositionLength; }
            set { PKneeBracketAnteriorPositionLength = value; }
        }
        public int RKneeBracketAnteriorPositionLength_Value
        {
            get { return RKneeBracketAnteriorPositionLength; }
            set { RKneeBracketAnteriorPositionLength = value; }
        }
        public int PKneeBracketLateralPositionLength_Value
        {
            get { return PKneeBracketLateralPositionLength; }
            set { PKneeBracketLateralPositionLength = value; }
        }
        public int PPelvicSizeLength_Value
        {
            get { return PPelvicSizeLength; }
            set { PPelvicSizeLength = value; }
        }
        public int RPelvicSizeLength_Value
        {
            get { return RPelvicSizeLength; }
            set { RPelvicSizeLength = value; }
        }
        public int PPelvicAnteriorPositionLength_Value
        {
            get { return PPelvicAnteriorPositionLength; }
            set { PPelvicAnteriorPositionLength = value; }
        }
        public int PPelvicVerticalPositionLength_Value
        {
            get { return PPelvicVerticalPositionLength; }
            set { PPelvicVerticalPositionLength = value; }
        }
        public int PUpperLegLengthLength_Value
        {
            get { return PUpperLegLengthLength; }
            set { PUpperLegLengthLength = value; }
        }
        public int RUpperLegLengthLength_Value
        {
            get { return RUpperLegLengthLength; }
            set { RUpperLegLengthLength = value; }
        }
        public int PLowerLegLengthLength_Value
        {
            get { return PLowerLegLengthLength; }
            set { PLowerLegLengthLength = value; }
        }
        public int RLowerLegLengthLength_Value
        {
            get { return RLowerLegLengthLength; }
            set { RLowerLegLengthLength = value; }
        }
        public int PFootPlateTypeLength_Value
        {
            get { return PFootPlateTypeLength; }
            set { PFootPlateTypeLength = value; }
        }
        public int RFootPlateTypeLength_Value
        {
            get { return RFootPlateTypeLength; }
            set { RFootPlateTypeLength = value; }
        }
        public int PFootPlateSizeLength_Value
        {
            get { return PFootPlateSizeLength; }
            set { PFootPlateSizeLength = value; }
        }
        public int RFootPlateSizeLength_Value
        {
            get { return RFootPlateSizeLength; }
            set { RFootPlateSizeLength = value; }
        }
        public int PFootPlateDorsiFlexionPositionLength_Value
        {
            get { return PFootPlateDorsiFlexionPositionLength; }
            set { PFootPlateDorsiFlexionPositionLength = value; }
        }
        public int RFootPlateDorsiFlexionPositionLength_Value
        {
            get { return RFootPlateDorsiFlexionPositionLength; }
            set { RFootPlateDorsiFlexionPositionLength = value; }
        }
        public int ShoeLenghtLength_Value
        {
            get { return ShoeLenghtLength; }
            set { ShoeLenghtLength = value; }
        }

        public string[] hx { get; set; }
        public string[] kx { get; set; }
        public string[] Spt { get; set; }
        public string[] Tspt { get; set; }
        public string[] Fsf { get; set; }
        public string[] Tiltd { get; set; }
        public string[] Tiltt { get; set; }
        public string[] Faldet { get; set; }
        public string[] Stct { get; set; }
        public string[] Wact { get; set; }
        public string[] Hmaxf { get; set; }
        public string[] Hfinf { get; set; }
        public string[] Dbets { get; set; }
        public string[] Stairs { get; set; }
        public string[] Systype { get; set; }
        public string[] Stairheight { get; set; }
        public string[] Switcht { get; set; }
        public string[] Ascsp { get; set; }
        public string[] Dscsp { get; set; }
        public string[] Prh { get; set; }
        public string[] Prk { get; set; }
        public string[] PUpStrapSize { get; set; }
        public string[] PUpStrapType { get; set; }
        public string[] PAKBType { get; set; }
        public string[] RAKBType { get; set; }
        public string[] PAKBAnterior { get; set; }
        public string[] RAKBAnterior { get; set; }
        public string[] PKBType { get; set; }
        public string[] RKBType { get; set; }
        public string[] PKBAnteriorPos { get; set; }
        public string[] RKBAnteriorPos { get; set; }
        public string[] PKBLateralPos { get; set; }
        public string[] PPelvicSize { get; set; }
        public string[] RPelvicSize { get; set; }
        public string[] PPelvicAnteriorPos { get; set; }
        public string[] PPelvicVerticalPos { get; set; }
        public string[] PUpLegLen { get; set; }
        public string[] RUpLegLen { get; set; }
        public string[] PLowLegLen { get; set; }
        public string[] RLowLegLen { get; set; }
        public string[] PFPlateType { get; set; }
        public string[] RFPlateType { get; set; }
        public string[] PFPlateSize { get; set; }
        public string[] RFPlateSize { get; set; }
        public string[] PFPDorsiFlexPos { get; set; }
        public string[] RFPDorsiFlexPos { get; set; }
        public string[] ShoeLen { get; set; }
        public string[] ASCSpeedStrings { get; set; }
        public string[] DSCSpeedStrings { get; set; }


        public void GainMinMaxLengthValues() // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions"
        {
            XmlDocument deviceSettings = new XmlDocument();
            deviceSettings.Load(System.AppDomain.CurrentDomain.BaseDirectory + "DeviceSettingsValues.xml");

            hx = deviceSettings.SelectSingleNode("/DeviceSettingsValues/HipFlexionValues").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            kx = deviceSettings.SelectSingleNode("/DeviceSettingsValues/KneeFlexionValues").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Spt = deviceSettings.SelectSingleNode("/DeviceSettingsValues/StepTime").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Tspt = deviceSettings.SelectSingleNode("/DeviceSettingsValues/BeginnersStepTime").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Fsf = deviceSettings.SelectSingleNode("/DeviceSettingsValues/FirstStepAssistanceAngle").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Tiltd = deviceSettings.SelectSingleNode("/DeviceSettingsValues/TiltDelta").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Tiltt = deviceSettings.SelectSingleNode("/DeviceSettingsValues/TiltTimeout").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Faldet = deviceSettings.SelectSingleNode("/DeviceSettingsValues/StandingUpFallDetectionthreshold").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Stct = deviceSettings.SelectSingleNode("/DeviceSettingsValues/SystemCurrent").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Wact = deviceSettings.SelectSingleNode("/DeviceSettingsValues/WalkCurrentThreshold").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Hmaxf = deviceSettings.SelectSingleNode("/DeviceSettingsValues/HipMaxFlexion").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Hfinf = deviceSettings.SelectSingleNode("/DeviceSettingsValues/HipFinalFlexion").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Dbets = deviceSettings.SelectSingleNode("/DeviceSettingsValues/DelayBetweenSteps").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Stairs = deviceSettings.SelectSingleNode("/DeviceSettingsValues/StairsValues").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Systype = deviceSettings.SelectSingleNode("/DeviceSettingsValues/SysType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Stairheight = deviceSettings.SelectSingleNode("/DeviceSettingsValues/StairHeight").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Switcht = deviceSettings.SelectSingleNode("/DeviceSettingsValues/SwitchingTimeValues").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Ascsp = deviceSettings.SelectSingleNode("/DeviceSettingsValues/ASCSpeed").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Dscsp = deviceSettings.SelectSingleNode("/DeviceSettingsValues/DSCSpeed").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Prh = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PlacingRightHip").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Prk = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PlacingRightKnee").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PUpStrapSize = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PUpperStrapHolderSize").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PUpStrapType = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PUpperStrapHolderType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PAKBType = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PAboveKneeBracketType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            RAKBType = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RAboveKneeBracketType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PAKBAnterior = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PAboveKneeBracketAnterior").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            RAKBAnterior = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RAboveKneeBracketAnterior").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PKBType = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PKneeBracketType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            RKBType = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RKneeBracketType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PKBAnteriorPos = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PKneeBracketAnteriorPosition").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            RKBAnteriorPos = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RKneeBracketAnteriorPosition").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PKBLateralPos = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PKneeBracketLateralPosition").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PPelvicSize = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PPelvicSize").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            RPelvicSize = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RPelvicSize").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PPelvicAnteriorPos = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PPelvicAnteriorPosition").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PPelvicVerticalPos = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PPelvicVerticalPosition").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PUpLegLen = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PUpperLegLength").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            RUpLegLen = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RUpperLegLength").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PLowLegLen = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PLowerLegLength").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            RLowLegLen = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RLowerLegLength").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PFPlateType = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PFootPlateType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            RFPlateType = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RFootPlateType").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PFPlateSize = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PFootPlateSize").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            RFPlateSize = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RFootPlateSize").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            PFPDorsiFlexPos = deviceSettings.SelectSingleNode("/DeviceSettingsValues/PFootPlateDorsiFlexionPosition").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            RFPDorsiFlexPos = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RFootPlateDorsiFlexionPosition").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            ShoeLen = deviceSettings.SelectSingleNode("/DeviceSettingsValues/ShoeLenght").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            ASCSpeedStrings = deviceSettings.SelectSingleNode("/DeviceSettingsValues/ASCSpeedStrings").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            DSCSpeedStrings = deviceSettings.SelectSingleNode("/DeviceSettingsValues/DSCSpeedStrings").Attributes["Values"].Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            HipFlextionAngleMin_Value = System.Convert.ToInt16(hx[0]);
            HipFlextionAngleMax_Value = System.Convert.ToInt16((hx[hx.Length - 1]));
            HipFlextionAngleLength_Value = hx.Length - 1;


            KneeFlextionAngleMin_Value = System.Convert.ToInt16(kx[0]);
            KneeFlextionAngleMax_Value = System.Convert.ToInt16((kx[kx.Length - 1]));
            KneeFlextionAngleLength_Value = kx.Length - 1;

            StepTimeMin_Value = System.Convert.ToInt16(Spt[0]);
            StepTimeMax_Value = System.Convert.ToInt16((Spt[Spt.Length - 1]));
            StepTimeLength_Value = Spt.Length - 1;

            BeginnersStepTimeMin_Value = System.Convert.ToInt16(Tspt[0]);
            BeginnersStepTimeMax_Value = System.Convert.ToInt16((Tspt[Tspt.Length - 1]));
            BeginnersStepTimeLength_Value = Tspt.Length - 1;

            FirststepFlexionMin_Value = System.Convert.ToInt16(Fsf[0]);
            FirststepFlexionMax_Value = System.Convert.ToInt16((Fsf[Fsf.Length - 1]));
            FirststepFlexionLength_Value = Fsf.Length - 1;

            TiltDeltaMin_Value = System.Convert.ToInt16(Tiltd[0]);
            TiltDeltaMax_Value = System.Convert.ToInt16((Tiltd[Tiltd.Length - 1]));
            TiltDeltaLength_Value = Tiltd.Length - 1;

            TiltTimeOutMin_Value = System.Convert.ToInt16(Tiltt[0]);
            TiltTimeOutMax_Value = System.Convert.ToInt16((Tiltt[Tiltt.Length - 1]));
            TiltTimeOutLength_Value = Tiltt.Length - 1;

            FallDetectionThrMin_Value = System.Convert.ToInt16(Faldet[0]);
            FallDetectionThrMax_Value = System.Convert.ToInt16((Faldet[Faldet.Length - 1]));
            FallDetectionThrLength_Value = Faldet.Length - 1;

            SystemCurrentThrMin_Value = System.Convert.ToInt16(Stct[0]);
            SystemCurrentThrMax_Value = System.Convert.ToInt16((Stct[Stct.Length - 1]));
            SystemCurrentThrLength_Value = Stct.Length - 1;

            WalkCurrentThrMin_Value = System.Convert.ToInt16(Wact[0]);
            WalkCurrentThrMax_Value = System.Convert.ToInt16((Wact[Wact.Length - 1]));
            WalkCurrentThrLength_Value = Wact.Length - 1;

            HipMaxFlextionAngleMin_Value = System.Convert.ToInt16(Hmaxf[0]);
            HipMaxFlextionAngleMax_Value = System.Convert.ToInt16((Hmaxf[Hmaxf.Length - 1]));
            HipMaxFlextionAngleLength_Value = Hmaxf.Length - 1;

            HipFinalFlextionAngleMin_Value = System.Convert.ToInt16(Hfinf[0]);
            HipFinalFlextionAngleMax_Value = System.Convert.ToInt16((Hfinf[Hfinf.Length - 1]));
            HipFinalFlextionAngleLength_Value = Hfinf.Length - 1;

            DelayBetweenStepsMin_Value = System.Convert.ToInt16(Dbets[0]);
            DelayBetweenStepsMax_Value = System.Convert.ToInt16((Dbets[Dbets.Length - 1]));
            DelayBetweenStepsLength_Value = Dbets.Length - 1;

            ASCDSCStairsMin_Value = System.Convert.ToInt16(Stairs[0]);
            ASCDSCStairsMax_Value = System.Convert.ToInt16((Stairs[Stairs.Length - 1]));
            ASCDSCStairsLength_Value = Stairs.Length - 1;

            SysTypeMin_Value = System.Convert.ToInt16(Systype[0]);
            SysTypeMax_Value = System.Convert.ToInt16((Systype[Systype.Length - 1]));
            SysTypeLength_Value = Systype.Length - 1;

            StairHeightMin_Value = System.Convert.ToInt16(Stairheight[0]);
            StairHeightMax_Value = System.Convert.ToInt16(Stairheight[Stairheight.Length - 1]);
            StairHeightLength_Value = Stairheight.Length - 1;

            SwitchingTimeMin_Value = System.Convert.ToInt16(Switcht[0]);
            SwitchingTimeMax_Value = System.Convert.ToInt16((Switcht[Switcht.Length - 1]));
            SwitchingTimeLength_Value = Switcht.Length - 1;

            ASCSpeedMin_Value = System.Convert.ToInt16(Ascsp[0]);
            ASCSpeedMax_Value = System.Convert.ToInt16((Ascsp[Ascsp.Length - 1]));
            ASCSpeedLength_Value = Ascsp.Length - 1;

            DSCSpeedMin_Value = System.Convert.ToInt16(Dscsp[0]);
            DSCSpeedMax_Value = System.Convert.ToInt16((Dscsp[Dscsp.Length - 1]));
            DSCSpeedLength_Value = Dscsp.Length - 1;

            PlacingRHMin_Value = System.Convert.ToInt16(Prh[0]);
            PlacingRHMax_Value = System.Convert.ToInt16((Prh[Prh.Length - 1]));
            PlacingRHLength_Value = Prh.Length - 1;

            PlacingRKMin_Value = System.Convert.ToInt16(Prk[0]);
            PlacingRKMax_Value = System.Convert.ToInt16((Prk[Prk.Length - 1]));
            PlacingRKLength_Value = Prk.Length - 1;


            PUpperStrapHolderSizeMin_Value = System.Convert.ToString(PUpStrapSize[0]);
            PUpperStrapHolderSizeMax_Value = System.Convert.ToString((PUpStrapSize[PUpStrapSize.Length - 1]));
            PUpperStrapHolderSizeLength_Value = PUpStrapSize.Length - 1;

            PUpperStrapHolderTypeMin_Value = System.Convert.ToInt16(PUpStrapType[0]);
            PUpperStrapHolderTypeMax_Value = System.Convert.ToInt16((PUpStrapType[PUpStrapType.Length - 1]));
            PUpperStrapHolderTypeLength_Value = PUpStrapType.Length - 1;


            PAboveKneeBracketTypeMin_Value = System.Convert.ToString(PAKBType[0]);
            PAboveKneeBracketTypeMax_Value = System.Convert.ToString((PAKBType[PAKBType.Length - 1]));
            PAboveKneeBracketTypeLength_Value = PAKBType.Length - 1;

            RAboveKneeBracketTypeMin_Value = System.Convert.ToString(RAKBType[0]);
            RAboveKneeBracketTypeMax_Value = System.Convert.ToString((RAKBType[RAKBType.Length - 1]));
            RAboveKneeBracketTypeLength_Value = RAKBType.Length - 1;

            PAboveKneeBracketAnteriorMin_Value = System.Convert.ToString(PAKBAnterior[0]);
            PAboveKneeBracketAnteriorMax_Value = System.Convert.ToString((PAKBAnterior[PAKBAnterior.Length - 1]));
            PAboveKneeBracketAnteriorLength_Value = PAKBAnterior.Length - 1;

            RAboveKneeBracketAnteriorMin_Value = System.Convert.ToString(RAKBAnterior[0]);
            RAboveKneeBracketAnteriorMax_Value = System.Convert.ToString((RAKBAnterior[RAKBAnterior.Length - 1]));
            RAboveKneeBracketAnteriorLength_Value = RAKBAnterior.Length - 1;

            PKneeBracketTypeMin_Value = System.Convert.ToString(PKBType[0]);
            PKneeBracketTypeMax_Value = System.Convert.ToString((PKBType[PKBType.Length - 1]));
            PKneeBracketTypeLength_Value = PKBType.Length - 1;

            RKneeBracketTypeMin_Value = System.Convert.ToString(RKBType[0]);
            RKneeBracketTypeMax_Value = System.Convert.ToString((RKBType[RKBType.Length - 1]));
            RKneeBracketTypeLength_Value = RKBType.Length - 1;

            PKneeBracketAnteriorPositionMin_Value = System.Convert.ToString(PKBAnteriorPos[0]);
            PKneeBracketAnteriorPositionMax_Value = System.Convert.ToString((PKBAnteriorPos[PKBAnteriorPos.Length - 1]));
            PKneeBracketAnteriorPositionLength_Value = PKBAnteriorPos.Length - 1;

            RKneeBracketAnteriorPositionMin_Value = System.Convert.ToString(RKBAnteriorPos[0]);
            RKneeBracketAnteriorPositionMax_Value = System.Convert.ToString((RKBAnteriorPos[RKBAnteriorPos.Length - 1]));
            RKneeBracketAnteriorPositionLength_Value = RKBAnteriorPos.Length - 1;

            PKneeBracketLateralPositionMin_Value = System.Convert.ToString(PKBLateralPos[0]);
            PKneeBracketLateralPositionMax_Value = System.Convert.ToString((PKBLateralPos[PKBLateralPos.Length - 1]));
            PKneeBracketLateralPositionLength_Value = PKBLateralPos.Length - 1;

            PPelvicSizeMin_Value = System.Convert.ToInt16(PPelvicSize[0]);
            PPelvicSizeMax_Value = System.Convert.ToInt16((PPelvicSize[PPelvicSize.Length - 1]));
            PPelvicSizeLength_Value = PPelvicSize.Length - 1;

            RPelvicSizeMin_Value = System.Convert.ToInt16(RPelvicSize[0]);
            RPelvicSizeMax_Value = System.Convert.ToInt16((RPelvicSize[RPelvicSize.Length - 1]));
            RPelvicSizeLength_Value = RPelvicSize.Length - 1;

            PPelvicAnteriorPositionMin_Value = System.Convert.ToInt16(PPelvicAnteriorPos[0]);
            PPelvicAnteriorPositionMax_Value = System.Convert.ToInt16((PPelvicAnteriorPos[PPelvicAnteriorPos.Length - 1]));
            PPelvicAnteriorPositionLength_Value = PPelvicAnteriorPos.Length - 1;

            PPelvicVerticalPositionMin_Value = System.Convert.ToString(PPelvicVerticalPos[0]);
            PPelvicVerticalPositionMax_Value = System.Convert.ToString((PPelvicVerticalPos[PPelvicVerticalPos.Length - 1]));
            PPelvicVerticalPositionLength_Value = PPelvicVerticalPos.Length - 1;

            PUpperLegLengthMin_Value = System.Convert.ToInt16(PUpLegLen[0]);
            PUpperLegLengthMax_Value = System.Convert.ToInt16((PUpLegLen[PUpLegLen.Length - 1]));
            PUpperLegLengthLength_Value = PUpLegLen.Length - 1;

            RUpperLegLengthMin_Value = System.Convert.ToInt16(RUpLegLen[0]);
            RUpperLegLengthMax_Value = System.Convert.ToInt16((RUpLegLen[RUpLegLen.Length - 1]));
            RUpperLegLengthLength_Value = RUpLegLen.Length - 1;

            PLowerLegLengthMin_Value = System.Convert.ToInt16(PLowLegLen[0]);
            PLowerLegLengthMax_Value = System.Convert.ToInt16((PLowLegLen[PLowLegLen.Length - 1]));
            PLowerLegLengthLength_Value = PLowLegLen.Length - 1;

            RLowerLegLengthMin_Value = System.Convert.ToInt16(RLowLegLen[0]);
            RLowerLegLengthMax_Value = System.Convert.ToInt16((RLowLegLen[RLowLegLen.Length - 1]));
            RLowerLegLengthLength_Value = RLowLegLen.Length - 1;

            PFootPlateTypeMin_Value = System.Convert.ToString(PFPlateType[0]);
            PFootPlateTypeMax_Value = System.Convert.ToString((PFPlateType[PFPlateType.Length - 1]));
            PFootPlateTypeLength_Value = PFPlateType.Length - 1;

            RFootPlateTypeMin_Value = System.Convert.ToString(RFPlateType[0]);
            RFootPlateTypeMax_Value = System.Convert.ToString((RFPlateType[RFPlateType.Length - 1]));
            RFootPlateTypeLength_Value = RFPlateType.Length - 1;

            PFootPlateSizeMin_Value = System.Convert.ToString(PFPlateSize[0]);
            PFootPlateSizeMax_Value = System.Convert.ToString((PFPlateSize[PFPlateSize.Length - 1]));
            PFootPlateSizeLength_Value = PFPlateSize.Length - 1;

            RFootPlateSizeMin_Value = System.Convert.ToString(RFPlateSize[0]);
            RFootPlateSizeMax_Value = System.Convert.ToString((RFPlateSize[RFPlateSize.Length - 1]));
            RFootPlateSizeLength_Value = RFPlateSize.Length - 1;

            PFootPlateDorsiFlexionPositionMin_Value = System.Convert.ToInt16(PFPDorsiFlexPos[0]);
            PFootPlateDorsiFlexionPositionMax_Value = System.Convert.ToInt16((PFPDorsiFlexPos[PFPDorsiFlexPos.Length - 1]));
            PFootPlateDorsiFlexionPositionLength_Value = PFPDorsiFlexPos.Length - 1;

            RFootPlateDorsiFlexionPositionMin_Value = System.Convert.ToInt16(RFPDorsiFlexPos[0]);
            RFootPlateDorsiFlexionPositionMax_Value = System.Convert.ToInt16((RFPDorsiFlexPos[RFPDorsiFlexPos.Length - 1]));
            RFootPlateDorsiFlexionPositionLength_Value = RFPDorsiFlexPos.Length - 1;

            ShoeLenghtMin_Value = System.Convert.ToInt16(ShoeLen[0]);
            ShoeLenghtMax_Value = System.Convert.ToInt16((ShoeLen[ShoeLen.Length - 1]));
            ShoeLenghtLength_Value = ShoeLen.Length - 1;



        }



        public bool ValidValueofInt16Pram(Int16 v, Int16 vmin, Int16 vmax)
        {
            if (v >= vmin && v <= vmax)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        public bool ValidValueofStairsASCInt16Pram(Int16 v)
        {
            int i;
            for (i = 0; i < ASCSpeedLength_Value+1; i++)
            {
                if (v == System.Convert.ToInt16(Ascsp[i]))
                {
                    return true;
                }
            }

            return false;

        }

        public bool ValidValueofStairsDSCInt16Pram(Int16 v)
        {
            int i;
            for (i = 0; i < DSCSpeedLength_Value+1; i++)
            {
                if (v == System.Convert.ToInt16(Dscsp[i]))
                {
                    return true;
                }
            }

            return false;

        }
    }
}