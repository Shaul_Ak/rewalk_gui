﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReceiverViewModel.cs" company="saramgsilva">
//   Copyright (c) 2014 saramgsilva. All rights reserved.
// </copyright>
// <summary>
//   The Receiver view model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Win32;
using System.ComponentModel;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Input;
using ReWalk6.Services;
using ReWalk6.Shared.ViewModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.Generic;

namespace ReWalk6.ViewModel
{
    /// <summary>
    /// The Receiver view model.
    /// </summary>
    /// 
   
    public sealed class ReceiverViewModel : ViewModelBase
    {
        private readonly IReceiverBluetoothService _receiverBluetoothService;
        private string _data;
        private bool _isStarEnabled;
        private string _status;
        private string[] data_strings;
        private string[] new_blue_que_data_strings;

        decimal pf_force;
        decimal pf_timing;
        decimal pf_pretension_level;

        decimal df_pull_length;
        decimal df_timing;
        decimal df_pretension_level;

        decimal step_count;
        decimal stroke_system_type_version = 0;
        string  session_time="";
        string mcu_sw_version = "0.0.0.0";
        decimal restore_system_operation_mode = 0;
        decimal walking_speed;
        decimal battery_level;
        Stopwatch stopwatch = new Stopwatch();
        string sensor_string="";
        bool sensor_data_ready = false;
        float imu_l0, imu_l1;
        string kst_data_file;
        string blue_strings_Data_File;
        string pf_force_string, df_pull_length_string;

        private DispatcherTimer stroke_session_timer = new DispatcherTimer();
        /// <summary>
        /// Initializes a new instance of the <see cref="ReceiverViewModel" /> class.
        /// </summary>
        /// <param name="receiverBluetoothService">The Receiver bluetooth service.</param>
        public ReceiverViewModel(IReceiverBluetoothService receiverBluetoothService)
        {
            _receiverBluetoothService = receiverBluetoothService;
            _receiverBluetoothService.PropertyChanged += ReceiverBluetoothService_PropertyChanged;
            IsStarEnabled = true;
            Data = "N/D";
            Status = "N/D";
            STROKE_SYSTEM_TYPE_VERSION = 0;
            stroke_session_timer.Interval = TimeSpan.FromSeconds(1);
            stroke_session_timer.Tick += new EventHandler(stroke_session_timer_tick);
            SESSION_TIME = "";// "00:00:00";
            StartCommand = new RelayCommand(() =>
            {
                _receiverBluetoothService.Start(SetData,"kk");
                IsStarEnabled = true;
             //   Data = "Can receive data.";
                
                STROKE_SYSTEM_TYPE_VERSION = 1;
                SESSION_TIME = "00:00:00";
                BATTERY_LEVEL = 9;
                stroke_session_timer.IsEnabled = true;
                stroke_session_timer.Start();
                stopwatch.Start();
               
            });

            StopCommand = new RelayCommand(() =>
            {
                _receiverBluetoothService.Stop();
                IsStarEnabled = true;
            //    Data = "Cannot receive data.";         
            //    Status = "DISCONNECTED";
                stopwatch.Stop();
            });

            Messenger.Default.Register<Message>(this, ResetAll);
        }

        /// <summary>
        /// Resets all.
        /// </summary>
        /// <param name="message">The message.</param>
        public void ResetAll(Message message)
        {
            if (!message.IsToShowDevices)
            {
                if (_receiverBluetoothService.WasStarted)
                {
                    _receiverBluetoothService.Stop();
                }
                IsStarEnabled = true;
                Data = "N/D";
                Status = "N/D";
            }
        }


        public static float BinaryStringToSingle(string s)
        {
            int i = Convert.ToInt32(s, 2);
            byte[] b = BitConverter.GetBytes(i);
            return BitConverter.ToSingle(b, 0);

          
        }
     /// <summary>
        /// The set data received.
        /// </summary>
        /// <param name="data">
        /// The data.
        /// </param
        /// >
        /// 

        private int graph_refresh_time_ = 0,index_=0;
        int index = 0;
       
        public void SetData(string data)
        {
            Data = data;
            /*
            using (StreamWriter Blue_strings_data_file = File.AppendText(Blue_strings_Data_File_Name))
            {

                Blue_strings_data_file.WriteLine(index.ToString() + ": " + data + "\n"); index++;

            }
            */
            new_blue_que_data_strings = Data.Split('#');
            if (new_blue_que_data_strings[0] == "KSEN")
            {
                index_ = 0;
                using (StreamWriter kst_constant_configuration_data_file = File.AppendText(KST_Config_Data_File_Name_Global))
                {
                    kst_constant_configuration_data_file.Write(graph_refresh_time_.ToString() + " ; ");// + graph_data[2]);
                    graph_refresh_time_++;
                }
                index_ = 1;
                foreach (string qq in (new_blue_que_data_strings))
                {
                    //  data_strings = qq.Split('$');
                  
                    if (qq != "KSEN" && qq != "$" && qq != "%")
                    {
                       

                        //  var bytes = Encoding.ASCII.GetBytes(qq);


                        if (index_ == 1 || index_ == 2 )
                        {
                            Int32 Lc =  Convert.ToInt32(qq, 16);
                            using (StreamWriter kst_constant_configuration_data_file = File.AppendText(KST_Config_Data_File_Name_Global))
                            {
                                kst_constant_configuration_data_file.Write(Lc.ToString() + " ; ");// + graph_data[2]);

                            }


                        }
                        
                      else   if (index_ > 0)
                        {
                            uint num = uint.Parse(qq, System.Globalization.NumberStyles.AllowHexSpecifier);

                            byte[] floatVals = BitConverter.GetBytes(num);
                            float f = BitConverter.ToSingle(floatVals, 0);

                            using (StreamWriter kst_constant_configuration_data_file = File.AppendText(KST_Config_Data_File_Name_Global))
                            {
                                kst_constant_configuration_data_file.Write(f.ToString() + " ; ");// + graph_data[2]);

                            }
                        }
                        index_++;

                       

                    
                    }

                }
                using (StreamWriter kst_constant_configuration_data_file = File.AppendText(KST_Config_Data_File_Name_Global))
                {
                    kst_constant_configuration_data_file.WriteLine(" ");// + graph_data[2]);               
                }
            }


           else if (new_blue_que_data_strings[0] == "RESD")
            {

                BATTERY_LEVEL = System.Convert.ToDecimal(new_blue_que_data_strings[1]);

                uint num1 = uint.Parse(new_blue_que_data_strings[2], System.Globalization.NumberStyles.AllowHexSpecifier);
                byte[] floatVals1 = BitConverter.GetBytes(num1);
                float f1 = BitConverter.ToSingle(floatVals1, 0);
                STEP_COUNT  = Convert.ToUInt32(f1);


                MCU_SW_VERSION = new_blue_que_data_strings[3];

               
                }

            else if (new_blue_que_data_strings[0] == "PARS")
            {

                PF_FORCE = System.Convert.ToUInt16(new_blue_que_data_strings[1],16);
                if (PF_FORCE == 0)
                {
                   PF_FORCE_String_For_Display = "INACTIVE_MODE(0)";
                }
                else if (PF_FORCE == 1)
                {
                    PF_FORCE_String_For_Display = "PRETENSION_(1)";
                }
                else if (PF_FORCE == 2)
                {
                    PF_FORCE_String_For_Display = "ACTIVE_MODE(2)";
                }
                PF_TIMING = System.Convert.ToUInt16(new_blue_que_data_strings[2], 16);
                PF_PRETENSION_LEVEL = System.Convert.ToUInt16(new_blue_que_data_strings[3], 16);
                DF_PULL_LENGTH = System.Convert.ToUInt16(new_blue_que_data_strings[4], 16);

                if (DF_PULL_LENGTH == 0)
                {
                    DF_PULL_LENGTH_String_For_Display = "LEFT_PARIETIC(0)";
                }
                else if (DF_PULL_LENGTH == 1)
                {
                   DF_PULL_LENGTH_String_For_Display = "RIGHT_PARIETIC(1)";
                }
                else if (DF_PULL_LENGTH == 2)
                {
                    DF_PULL_LENGTH_String_For_Display = "NOT_SPECIFIED(2)";
                }
                DF_TIMING = System.Convert.ToUInt16(new_blue_que_data_strings[5], 16);
                DF_PRETENSION_LEVEL = System.Convert.ToUInt16(new_blue_que_data_strings[6], 16);
                RESTORE_SYSTEM_OPERATION_MODE = System.Convert.ToUInt16(new_blue_que_data_strings[7], 16);

            }

           
         }

        private void stroke_session_timer_tick(object sender, EventArgs e)
        {
            SESSION_TIME = string.Format("{0:D2}:{1:D2}:{2:D2}",stopwatch.Elapsed.Hours,stopwatch.Elapsed.Minutes,stopwatch.Elapsed.Seconds);
        }

        public string  GetData()
        {
            return _data;
        }
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data received.
        /// </value>
        public string Data
        {
            get { return _data; }
            set { Set(() => Data, ref _data, value); }
        }

        public string KST_Config_Data_File_Name_Global
        {
            get { return kst_data_file; }
            set { Set(()=> KST_Config_Data_File_Name_Global,ref  kst_data_file, value); }
        }
        public string Blue_strings_Data_File_Name
        {
            get { return blue_strings_Data_File; }
            set { Set(() => Blue_strings_Data_File_Name, ref blue_strings_Data_File, value); }
        }
        public decimal PF_FORCE
        {
            get { return pf_force; }
            set { Set(() => PF_FORCE, ref pf_force, value); }
        }
        public string PF_FORCE_String_For_Display
        {
            get { return pf_force_string; }
            set { Set(() => PF_FORCE_String_For_Display, ref pf_force_string, value); }
        }
        public decimal PF_TIMING
        {
            get { return pf_timing; }
            set { Set(() => PF_TIMING, ref pf_timing, value); }
        }

        public decimal PF_PRETENSION_LEVEL
        {
            get { return pf_pretension_level; }
            set { Set(() => PF_PRETENSION_LEVEL, ref pf_pretension_level, value); }
        }

        public decimal DF_PULL_LENGTH
        {
            get { return df_pull_length; }
            set { Set(() => DF_PULL_LENGTH, ref df_pull_length, value); }
        }
        public string DF_PULL_LENGTH_String_For_Display
        {
            get { return df_pull_length_string; }
            set { Set(() => DF_PULL_LENGTH_String_For_Display, ref df_pull_length_string, value); }
        }
        public decimal DF_TIMING
        {
            get { return df_timing; }
            set { Set(() => DF_TIMING, ref df_timing, value); }
        }

        public decimal DF_PRETENSION_LEVEL
        {
            get { return df_pretension_level; }
            set { Set(() => DF_PRETENSION_LEVEL, ref df_pretension_level, value); }
        }
        public decimal STEP_COUNT
        {
            get { return step_count; }
            set { Set(() => STEP_COUNT, ref step_count, value); }
        }
        public string SESSION_TIME
        {
            get { return session_time ; }
            set { Set(() => SESSION_TIME, ref session_time, value); }
        }
        public decimal WALKING_SPEED
        {
            get { return walking_speed; }
            set { Set(() => WALKING_SPEED, ref walking_speed, value); }
        }
        public decimal BATTERY_LEVEL
        {
            get { return battery_level; }
            set { Set(() => BATTERY_LEVEL, ref battery_level, value); }

        }        
        public decimal STROKE_SYSTEM_TYPE_VERSION
        {
            get { return stroke_system_type_version; }
            set { Set(() => STROKE_SYSTEM_TYPE_VERSION, ref stroke_system_type_version, value); }
        }
        
        
        public string MCU_SW_VERSION
        {
            get { return mcu_sw_version; }
            set { Set(() => MCU_SW_VERSION, ref mcu_sw_version, value); }
        }
        public decimal RESTORE_SYSTEM_OPERATION_MODE
        {
            get { return restore_system_operation_mode; }
            set { Set(() => RESTORE_SYSTEM_OPERATION_MODE, ref restore_system_operation_mode, value); }
        }

    
        public string SENSOR_DATA
        {
            get { return sensor_string; }
        }
        public bool SENSOR_DATA_READY
        {
            get { return sensor_data_ready; }
        }
        public float IMU_L0
        {
            get { return imu_l0; }
            set { Set(() => IMU_L0, ref imu_l0, value); }
        }
        public float IMU_L1
        {
            get { return imu_l1; }
            set { Set(() => IMU_L1, ref imu_l1, value); }
        }
        /// <summary>
        /// Gets the start command.
        /// </summary>
        /// <value>
        /// The start command.
        /// </value>
        public ICommand StartCommand { get; private set; }

        /// <summary>
        /// Gets the stop command.
        /// </summary>
        /// <value>
        /// The stop command.
        /// </value>
        public ICommand StopCommand { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether is star enabled.
        /// </summary>
        /// <value>
        /// The is star enabled.
        /// </value>
        public bool IsStarEnabled
        {
            get
            {
                return _isStarEnabled;
            }
            set
            {
                Set(() => IsStarEnabled, ref _isStarEnabled, value);
                RaisePropertyChanged(() => IsStopEnabled);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is stop enabled.
        /// </summary>
        /// <value>
        /// The is stop enabled.
        /// </value>
        public bool IsStopEnabled
        {
            get
            {
                return !_isStarEnabled;
            }
            set
            {
                Set(() => IsStopEnabled, ref _isStarEnabled, !value);
                RaisePropertyChanged(() => IsStarEnabled);
            }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public string Status
        {
            get { return _status; }
            set { Set(() => Status, ref _status, value); }
        }

       /// <summary>
        /// Handles the PropertyChanged event of the ReceiverBluetoothService control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void ReceiverBluetoothService_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "WasStarted")
            {
                IsStarEnabled = true;
            }
        }
    }
}
