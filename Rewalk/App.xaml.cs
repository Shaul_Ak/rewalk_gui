﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace Rewalk
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public bool DeptName_Connected { get; set; }
        public short DeptName_AFTCONWALKST { get; set; }
        public short DeptName_AFTCONSITSTANDST { get; set; }
        public short DeptName_AFTCONASCST { get; set; }
        public short DeptName_AFTCONDSCST { get; set; }
        public short DeptName_AFTCONWALKCOUNTER { get; set; }
        public short DeptName_AFTCONSITSTANDCOUNTER { get; set; }
        public short DeptName_AFTCONASCCOUNTER { get; set; }
        public short DeptName_AFTCONDSCCOUNTER { get; set; }
        public short DeptName_CurrentTestRunning { get; set; }
        public string DeptName_TesterFirstName { get; set; }
        public bool   DeptName_RunINFtoApplication { get; set; }
      
          
    }
    
}
