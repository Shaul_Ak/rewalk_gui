using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RewalkControls;

namespace Rewalk
{
	public partial class pagRegistration
	{
        public pagRegistration()
		{
			this.InitializeComponent();

			// Insert code required on object creation below this point.
		}

        // Pretend we did something and return to the main page.
        private void btnOk_Click(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("Tester registration completed.", "Tester name created",
                MessageBoxButton.OK, MessageBoxImage.Information);
            testerfirstname.IsEnabled = false;
            testerlastname.IsEnabled = false;
            if (!string.IsNullOrEmpty(testerfirstname.Text))
            {

                (App.Current as App).DeptName_TesterFirstName = testerfirstname.Text;
               
               // if (NavigationService.CanGoBack)
              //  {
               //     NavigationService.GoBack();
               // }
               // else
               // {
                NavigationService.Navigate(new pagAFTtests());
              //  }
            }
            else
            {
                MessageBox.Show("First name field should be filled", "Tester name created",
                MessageBoxButton.OK, MessageBoxImage.Error);
                testerfirstname.IsEnabled = true;
                testerfirstname.IsEnabled = true;
            }
        }

        // Return to the main page.
        private void btnCancel_Click(object sender, MouseButtonEventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
            else
            {
                NavigationService.Navigate(new pagMain());
            }
        }

        private void Image_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }
	}
}
