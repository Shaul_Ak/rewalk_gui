﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Rewalk
{
   public  class Int32ToVersionConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Int32 version = (Int32)value;

           // short version = (short)value;
            return string.Format("{0}.{1}.{2}.{3}", version >> 8, (byte)version,4, 5);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

       
    }

  
}

