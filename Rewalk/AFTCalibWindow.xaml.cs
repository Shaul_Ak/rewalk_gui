﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Win32;
using Rewalk.Device;
using System.Diagnostics;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using RewalkControls;
using System.Threading;
using System.Windows.Input;
using System.Security;
using System.Text;
using System.Windows.Media;
using System.Windows.Documents;
using System.Net;
using System.Net.Mail;
using System.Windows.Markup;
using FileParser;


namespace Rewalk
{
    /// <summary>
    /// Interaction logic for AFTCalibWindow.xaml
    /// </summary>
    public partial class AFTCalibWindow : Window
    {
        public AFTCalibWindow()
        {
            InitializeComponent();
            // Prepare the BackgroundWorker.
            AFT_BackgroundWorker = new BackgroundWorker();
            AFT_BackgroundWorker.WorkerReportsProgress = true;
            AFT_BackgroundWorker.WorkerSupportsCancellation = true;

            AFT_BackgroundWorker.DoWork += BackgroundWorker_DoWork;
            AFT_BackgroundWorker.ProgressChanged += BackgroundWorker_ProgressChanged;
            AFT_BackgroundWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;
        }
        
     
        public DispatcherTimer AFTCalbirationRunnnigWindowTimer = new DispatcherTimer();
        Stopwatch stopwatch = new Stopwatch();


        // Simulate a long task.
        private void BackgroundWorker_DoWork(Object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            int _val = 0;
            // Random rand = new Random();
            while (pvalue < 10000 && (AFT_calibrate_in_process == 1))
            {
                // Wait a little while.
                // Thread.Sleep(200);
                stopwatch.Start();
                while (stopwatch.ElapsedMilliseconds < 100)
                {
                    _val++;
                }
                stopwatch.Reset();
                _val = 0;

                // Add a bit to the progress.
                pvalue += 1000;

                // Update the UI thread.
                //lblWorking.Content = " LH change"+ value.ToString();
                AFT_BackgroundWorker.ReportProgress(pvalue);
            }

        }
        // The BackgroundWorker that will perform the long task.
        private BackgroundWorker AFT_BackgroundWorker;
        private int pvalue = 0;
        private int AFT_calibrate_in_process = 0;
        private bool AFT_WALK_in_process = false;
        private bool AFT_SITSTAND_in_process = false;
        private bool AFT_ASC_in_process = false;
        private bool AFT_DSC_in_process = false;
        private float AFTtoolconcurrentthr;
        private float AFTWalkToolContCurrentThr;
        private float AFTSitStandToolContCurrentThr;
        private float AFTASCToolContCurrentThr;
        private float AFTDSCToolContCurrentThr;
        private float Run1measuredcurrent;
        private bool AFTToolCalbirationFinish = false;
        public bool AFTToolRUN1FINISH = false;
        public bool AFTToolRUN2FINISH = false; 
        private bool Aftrun1 = false;
        private bool Aftrun2 = false;
        private bool Aftcalb = false;
        private bool EnableAFTRun1But = false;
        private bool EnableAFTRun2But = false;
        private AFTTestStatus aftcontWalktreportstatus;
        private AFTTestStatus aftcontSitStandreportstatus;
        private AFTTestStatus aftcontASCreportstatus;
        private AFTTestStatus aftcontDSCreportstatus;

        public float AFTTOOLCONTCURRENTTHR
        {
            set { AFTtoolconcurrentthr = value; }
            get { return AFTtoolconcurrentthr; }
        }
        public bool AFT_WALK_IN_PROCESS
        {
            get { return AFT_WALK_in_process; }
            set { AFT_WALK_in_process = value; }
        }

        public bool AFT_SITSTAND_IN_PROCESS
        {
            get { return AFT_SITSTAND_in_process; }
            set { AFT_SITSTAND_in_process = value; }
        }
        public bool AFT_ASC_IN_PROCESS
        {
            get { return AFT_ASC_in_process; }
            set { AFT_ASC_in_process = value; }
        }
        public bool AFT_DSC_IN_PROCESS
        {
            get { return AFT_DSC_in_process; }
            set { AFT_DSC_in_process = value; }
        }
        public float AFTWALKTOOLCONTCURRENTTHR
        {
            get { return AFTWalkToolContCurrentThr; }
            set { AFTWalkToolContCurrentThr = value; }
        }
        public float AFTSITSTANDTOOLCONTCURRENTTHR
        {
            get { return AFTSitStandToolContCurrentThr; }
            set { AFTSitStandToolContCurrentThr = value; }
        }
        public float AFTASCTOOLCONTCURRENTTHR
        {
            get { return AFTASCToolContCurrentThr; }
            set { AFTASCToolContCurrentThr = value; }
        }
        public float AFTDSCTOOLCONTCURRENTTHR
        {
            get { return AFTDSCToolContCurrentThr; }
            set { AFTDSCToolContCurrentThr = value; }
        }
        public float RUN1MEASUREDCURRENT
        {
            get { return Run1measuredcurrent; }
            set { Run1measuredcurrent = value; }
        }
        public bool AFTTOOLCALBIRATIONFINISH
        {
            get { return AFTToolCalbirationFinish; }
            set { AFTToolCalbirationFinish = value; }
        }

        public AFTTestStatus CONWALKREPORTSTATUS
        {
            get { return aftcontWalktreportstatus; }
            set { aftcontWalktreportstatus = value; }
        }
        public AFTTestStatus CONSITSTANDREPORTSTATUS
        {
            get { return aftcontSitStandreportstatus; }
            set { aftcontSitStandreportstatus = value; }
        }
        public AFTTestStatus CONASCREPORTSTATUS
        {
            get { return aftcontASCreportstatus; }
            set { aftcontASCreportstatus = value; }
        }
        public AFTTestStatus CONDSCREPORTSTATUS
        {
            get { return aftcontDSCreportstatus; }
            set { aftcontDSCreportstatus = value; }
        }   

        public bool AFTTOOLRUN1
        {
            get { return Aftrun1; }
            set { Aftrun1 = value; }
        }
        public bool AFTTOOLRUN2
        {
            get { return Aftrun2; }
            set { Aftrun2 = value; }
        }

        public bool AFTCALBIRATE
        {
            get { return Aftcalb; }
            set { Aftcalb = value; }
        }
        public BackgroundWorker M_BACKGROUNDWORKER
        {
            get { return AFT_BackgroundWorker; }
            set { AFT_BackgroundWorker = value; }
        }
        public int AFTCALIBRATEINPROCESS
        {
            get { return AFT_calibrate_in_process; }
            set { AFT_calibrate_in_process = value; }
        }
        public int VALUE
        {
            get { return pvalue; }
            set { pvalue = value; }
        }
        public bool ENABLEAFTRUN1BUT
        {
            get { return EnableAFTRun1But; }
            set { EnableAFTRun1But = value;}
        }
        public bool ENABLEAFTRUN2BUT
        {
            get { return EnableAFTRun2But; }
            set { EnableAFTRun2But = value; }
        }
      
        // Start the long task.
        public void Show_progressbar()
        {
            // Display the progress controls.
            AFTprgWorking.Value = 0;
            AFTprgWorking.Visibility = Visibility.Visible;
            AFTlblWorking.Visibility = Visibility.Visible;
        }

        // Start the long task.
        public void Hide_progressbar()
        {
            // Display the progress controls.
            AFTprgWorking.Value = 0;
            AFTprgWorking.Visibility = Visibility.Hidden;
            AFTlblWorking.Visibility = Visibility.Hidden;
            AFTlblWorkingFinish.Visibility = Visibility.Hidden;

           
        }

        float ActualAverageCurrent;
        public float ACTUALAVERAGECURRENT
        {
            set { ActualAverageCurrent = value; }
            get { return ActualAverageCurrent; }
        }
        // Display the progress.
        private void BackgroundWorker_ProgressChanged(Object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            AFTprgWorking.Value = e.ProgressPercentage;
        }
        private  Data AFTdevicedata = new Data();
      
        // Reset the UI.
          private void BackgroundWorker_RunWorkerCompleted(Object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
           {
              string mmm = "";
              

              AFTlblWorkingFinish.Visibility = Visibility.Visible;
              AFTlblWorking.Visibility = Visibility.Hidden;
              
              ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read actual current from MCU
              this.Focus();
     
              if (AFTCALBIRATE == true) //Calibration request
              {

                  if ((AFTTOOLCONTCURRENTTHR <= (ActualAverageCurrent + 0.5)) && ((AFTTOOLCONTCURRENTTHR >= ActualAverageCurrent - 0.5)))
                  {
                      Main MainWindow = new Main();
                      MainWindow.autosavelogfile(" Calbiration");
                      AFTlblWorkingFinish.Content = "Finish Success";
                      AFTTOOLCALBIRATIONFINISH = true;
                      AFTCALBIRATE = false;
                      mmm = "";
                      MessageBox.Show(this, AFTlblWorkingFinish.Content.ToString() + "\nThe measured current is: " + ActualAverageCurrent.ToString() + "\n Continue to Run1" + mmm, "AFT Calibration", MessageBoxButton.OK, MessageBoxImage.Information);
                      ENABLEAFTRUN1BUT = true; //Go to run1
                      CheckAFTCalbirationTool.IsEnabled = false;
                     // AFTCalbirationRunnnigWindowTimer.Start();
                                      
                  }
                  else
                  {
                      AFTlblWorkingFinish.Content = "Failed. Press start to re-calibrate button again";
                      this.Focus();
                      if (ActualAverageCurrent < AFTTOOLCONTCURRENTTHR)
                          mmm = "Increase Load!";
                      else if (ActualAverageCurrent > AFTTOOLCONTCURRENTTHR)
                          mmm = "Decrease Load!";
                      MessageBox.Show(this, AFTlblWorkingFinish.Content.ToString() + "\nThe measured current is: " + ActualAverageCurrent.ToString() + "\n" + mmm, "AFT Calibration", MessageBoxButton.OK, MessageBoxImage.Information);
                      CheckAFTCalbirationTool.Visibility = Visibility.Hidden;
                      CheckAFTCalbirationTool.IsEnabled = false;
                      StartAFTCalbirationTool.Visibility = Visibility.Visible;
                      AFTTOOLCALBIRATIONFINISH = false;
                      Hide_progressbar();                     
                  }

              }


              else if (AFTTOOLRUN1 == true)
              {
                 
                      RUN1MEASUREDCURRENT = ActualAverageCurrent;
                      Main MainWindow = new Main();
                      MainWindow.autosavelogfile(" FirstPhase");
                      AFTTOOLRUN1 = false;
                      EnableAFTRun2But = true;
                      AFTToolRUN1FINISH = true;
                      MessageBox.Show(this, "Run 1 test finish - Continue to Run2" + ActualAverageCurrent.ToString() + "\n" + mmm, "RUN1", MessageBoxButton.OK, MessageBoxImage.Information);
                
                  
              }
              else if (AFTTOOLRUN2 == true)
              {
                  
                  if ((ActualAverageCurrent <= (RUN1MEASUREDCURRENT + 0.5)) && ((ActualAverageCurrent >= RUN1MEASUREDCURRENT - 0.5)))
                  {
                      Main MainWindow = new Main();                   
                      Backlash.Visibility = Visibility.Visible;
                      Backlash_Value.Visibility = Visibility.Visible;
                      AFTlblWorkingFinish.Content = "Insert backlash value here:";
                      AFTlblWorkingFinish.Visibility = Visibility.Visible;
                      ok.Visibility = Visibility.Visible;
                      Backlash_value_inserted = false;
                      SetTestStatus(AFTTestStatus.PASS);
                    //  while (!Backlash_value_inserted) ;
                   /*   MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName + " " + (App.Current as App).DeptName_TesterSecondName), AFTTestStatus.PASS.ToString(), "End", testname, ActualAverageCurrent.ToString(), false, Backlash_value_counts);
                      SetTestStatus(AFTTestStatus.PASS);
                      MainWindow.autosavelogfile(" SecondPhase");
                      AFTTOOLRUN2 = false;
                      MessageBox.Show(this, "Run 2 test finished with success." + ActualAverageCurrent.ToString() + "\n" + mmm, "RUN2", MessageBoxButton.OK, MessageBoxImage.Information);
                     this.Hide();*/
                  }
                  else
                  {
                      Main MainWindow = new Main();
                      Backlash.Visibility = Visibility.Visible;
                      Backlash_Value.Visibility = Visibility.Visible;
                      AFTlblWorkingFinish.Content = "Insert backlash value here:";
                      AFTlblWorkingFinish.Visibility = Visibility.Visible;
                      ok.Visibility = Visibility.Visible;
                      Backlash_value_inserted = false;
                      SetTestStatus(AFTTestStatus.FAIL);
                     // while (!Backlash_value_inserted) ;
                    /*  MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName + " " + (App.Current as App).DeptName_TesterSecondName), AFTTestStatus.FAIL.ToString(), "End", testname, ActualAverageCurrent.ToString(), false, Backlash_value_counts);
                      SetTestStatus(AFTTestStatus.FAIL);
                      MainWindow.autosavelogfile(" SecondPhase");
                      AFTTOOLRUN2 = false;
                      MessageBox.Show(this, "Run 2 test finish with fail." + ActualAverageCurrent.ToString() + "\n" + mmm, "RUN2", MessageBoxButton.OK, MessageBoxImage.Information);*/
                  }

              }
              

           }

          private string GetTestName()
          {
              string testname="";
              if (AFT_WALK_in_process == true)
              {
                  testname = "Continuous Walk";
                  byte[] data = { 0x4f, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; 
                  byte[] result_LH = devdat.RunCustomCommand(5, data);
              }
              else if (AFT_SITSTAND_in_process == true)
              {
                  testname = "Continuous Sit-Stand";
                  byte[] data = { 0x4e, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                  byte[] result_LH = devdat.RunCustomCommand(5, data);
              }
              else if (AFT_ASC_IN_PROCESS == true)
              {
                  testname = "Continuous ASC";
                  byte[] data = { 0x5a, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                  byte[] result_LH = devdat.RunCustomCommand(5, data);
              }
              else if (AFT_DSC_IN_PROCESS == true)
              {
                  testname = "Continuous DSC";
                  byte[] data = { 0x5b, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                  byte[] result_LH = devdat.RunCustomCommand(5, data);
              }
              return testname;
          }


          public string BacklashInWalkTest;
          public string BacklashInSitstandTest;
          public string BacklashInASCTest;
          public string BacklashInDSCTest;


          private void SetTestStatus(AFTTestStatus status)
          {
             
              if (AFT_WALK_in_process == true)
              {
                  CONWALKREPORTSTATUS = status;               
                  BacklashInWalkTest = Backlash_Value.Text;

              }
              else if (AFT_SITSTAND_in_process == true)
              {
                  CONSITSTANDREPORTSTATUS = status;
                  BacklashInSitstandTest = Backlash_Value.Text;
              }
              else if (AFT_ASC_IN_PROCESS == true)
              {
                  CONASCREPORTSTATUS = status;
                  BacklashInASCTest = Backlash_Value.Text;
              }
              else if (AFT_DSC_IN_PROCESS == true)
              {
                  CONDSCREPORTSTATUS = status;
                  BacklashInDSCTest = Backlash_Value.Text;
              }
  
          }
     private void Window_Closing(object sender, CancelEventArgs e)
          {
              this.Hide();
          }

     private void Delay_ms(double ms)
          {
              double _val = 0;
              Stopwatch stopwatch1 = new Stopwatch();
              stopwatch1.Start();

              while (stopwatch1.ElapsedMilliseconds < ms)
              {
                  _val++;
              }
              stopwatch1.Reset();
              _val = 0;

          }
         
     private void StartAFTCalbirationTool_Click(object sender, RoutedEventArgs e)
          {
              byte[] data = {0x4f,0,1,0,5,0,0,0,0,0}; //cont walk of 5 steps - 10 bytes include identifier

              if (AFT_WALK_IN_PROCESS == true) data[0] = 0x4f;
              else if (AFT_SITSTAND_IN_PROCESS == true) data[0] = 0x4e;
              else if (AFT_ASC_IN_PROCESS == true) data[0] = 0x5a;
              else if (AFT_DSC_IN_PROCESS == true) data[0] = 0x5b;
              byte[] result_LH = devdat.RunCustomCommand(5, data);
              MessageBox.Show("Please disconnect the USB cable from the computer to start calibration\nOnce finished connect the USB cable again and press check", "Tool calibration", MessageBoxButton.OK, MessageBoxImage.Information);
              CheckAFTCalbirationTool.Visibility = Visibility.Visible;
              AFTlblWorking.Visibility = Visibility.Hidden;
              CheckAFTCalbirationTool.IsEnabled = false;
              StartAFTCalbirationTool.Visibility = Visibility.Hidden;
              AFTCalbirationRunnnigWindowTimer.Start();
              (App.Current as App).DeptName = false;
            
              this.Focus();
           
          }

     private void CheckAFTCalbirationTool_Click(object sender, RoutedEventArgs e)
          {

              AFT_calibrate_in_process = 1;//start bar filling
              VALUE = 0;
              Show_progressbar();
              AFTlblWorkingFinish.Content = "";
              M_BACKGROUNDWORKER.RunWorkerAsync();
              AFTlblWorking.Visibility = Visibility.Visible;
              AFTlblWorking.Content = "Calibration check in process...";
              this.Focus();
          }


        
     private float ReadAverageToolCalibMCUCurrent()
        {
            byte[] data = {0x30,0x03,0x81,0,0,0,0,0,0,0}; //Read average current
            byte[] result;
            Int32  LH_AvgCurrent, LK_AvgCurrent, RH_AvgCurrent, RK_AvgCurrent;
            float AverageCurrent; 


                    data[0] = 0x30;
                    result = devdat.RunCustomCommand(1, data);
                    LH_AvgCurrent = result[2];
                    LH_AvgCurrent = (LH_AvgCurrent << 8) + result[3];

                    data[0] = 0x31;
                    result = devdat.RunCustomCommand(2, data);
                    LK_AvgCurrent = result[2];
                    LK_AvgCurrent = (LK_AvgCurrent << 8) + result[3];

                    data[0] = 0x32;
                    result = devdat.RunCustomCommand(3, data);
                    RH_AvgCurrent = result[2];
                    RH_AvgCurrent = (RH_AvgCurrent << 8) + result[3];

                    data[0] = 0x33;
                    result = devdat.RunCustomCommand(4, data);
                    RK_AvgCurrent = result[2];
                    RK_AvgCurrent = (RK_AvgCurrent << 8) + result[3];

                    AverageCurrent = LH_AvgCurrent + LK_AvgCurrent + RH_AvgCurrent + RK_AvgCurrent;
                    return (AverageCurrent/416); //4*104=416 , average of four joints and 104 is current mcu divider
        }

     Data devdat = new Data();
            
     /*
      void AFTCalbirationRunnnigWindowTimer_Tick(object sender, EventArgs e)
        {
            if ((App.Current as App).DeptName == true)
            {
                CheckAFTCalbirationTool.IsEnabled = true;
                AFTCalbirationRunnnigWindowTimer.Stop();
               // (App.Current as App).DeptName = false;
            }
            else
            {
                CheckAFTCalbirationTool.IsEnabled = false;
            }
        }
        */

      private bool Backlash_value_inserted = false;
      private Int32 Backlash_value_counts;

      private void Backlash_Value_TextChanged(object sender, TextChangedEventArgs e)
      {
        /*  string testname = "";
          Main MainWindow = new Main();
          testname = GetTestName();
          Backlash_value_inserted = true;
          Backlash_value_counts = (Convert.ToInt32(Backlash_Value.Text));
          MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName + " " + (App.Current as App).DeptName_TesterSecondName), AFTTestStatus.PASS.ToString(), "End", testname, ActualAverageCurrent.ToString(), false, Backlash_value_counts);
      
          MainWindow.autosavelogfile(" SecondPhase");
          AFTTOOLRUN2 = false;
          MessageBox.Show(this, "Run 2 test finished with success." + ActualAverageCurrent.ToString() + "\n" , "RUN2", MessageBoxButton.OK, MessageBoxImage.Information);
          Thread.Sleep(1000);

          this.Hide();*/
          
      }

      private void button1_Click(object sender, RoutedEventArgs e)
      {
          string testname = "";
          Main MainWindow = new Main();
          testname = GetTestName();
          Backlash_value_inserted = true;
          Backlash_value_counts = (Convert.ToInt32(Backlash_Value.Text));
          MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName + " " + (App.Current as App).DeptName_TesterSecondName), AFTTestStatus.PASS.ToString(), "End", testname, ActualAverageCurrent.ToString(), true, "");

          MainWindow.autosavelogfile(" SecondPhase");
          AFTTOOLRUN2 = false;
          MessageBox.Show(this, "Run 2 test finished with success." + ActualAverageCurrent.ToString() + "\n", "RUN2", MessageBoxButton.OK, MessageBoxImage.Information);
          Thread.Sleep(1000);
          ok.Visibility = Visibility.Hidden;
          this.Hide();
      }




    }
}
