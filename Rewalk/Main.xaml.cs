﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Win32;
using Rewalk.Device;
using System.Diagnostics;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using RewalkControls;
using System.Threading;
using System.Windows.Input;
using System.Security;
using System.Text;
//using Spire.Doc;
//using Spire.Doc.Documents;
//using System.Drawing;

using System.Windows.Media;

using System.Windows.Documents;

using System.Net;
using System.Net.Mail;
using System.Windows.Markup;
using FileParser;


namespace Rewalk
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : Window // parasoft-suppress  CS.OOM.MI "Coding style - method logic was verified"
    {
        
        string InstallingAppDir = Directory.GetCurrentDirectory();
        private bool LogFileSent = false;
        [Flags]
        enum ReportType { ERROR, NORMAL };

        private string LogPath;
        private string SysLogPath;
        private string EncoderTestPath;

        private short Short_GUIVersion;

        public Main() // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions" CS.MLC "Methods executing atomic functions"
        {
            System.Array Instances;
            Instances = System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location));
            if (Instances.Length > 1)
            {
                MessageBoxResult result = MessageBox.Show(this, "Another instance of the application is already running.\n", "", MessageBoxButton.OK, MessageBoxImage.Hand);                  

                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            InitializeComponent();
            ReWalkStartUpConfiguration();
            AFTToolScreenSet();
            // tbInterfaceVersion.Text = "Rewalk Interface " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            string InterfaceVersion = "Rewalk Interface " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            System.Version GUIVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            Short_GUIVersion = (short)(GUIVersion.Major * 100 + GUIVersion.Build * 10 + GUIVersion.Revision);
            tbInterfaceVersion.Text = Logger.Validate(InterfaceVersion);
            
            DataContext = deviceData;
            deviceData.Parameters.RCAddress = 0; deviceData.Parameters.RCId = 0;
            COMMAND6_7_Panel.Visibility = Visibility.Hidden;
           // COMMAND6_7_Panel.Visibility = Visibility.Visible;
            Events_Init();
            this.Closed += new EventHandler(OnMainClosed);
            if (DesignerProperties.GetIsInDesignMode(this) == false)
            {
                deviceSettingsValuesDataProvider = (XmlDataProvider)FindResource("deviceSettingsValues");
                deviceSettingsValuesDataProvider.Document = new XmlDocument();
                deviceSettingsValuesDataProvider.Document.Load("DeviceSettingsValues.xml"); deviceSettingsValuesDataProvider.XPath = "DeviceSettingsValues";
               
            }
            SetTitle(null);
            btnUpdateINF.IsEnabled = false; collectOnline.IsOn = false; btnSetSN.IsEnabled = false; btnUpdateRC.IsEnabled = false;//Must do this after InitializeComponent to run animation                 
            Knee_Flexion.Items.CopyTo(array1, 0);
            btnBootBrowse.IsEnabled = false; btnBootDownload.IsEnabled = false;          
            string temp = AppDomain.CurrentDomain.BaseDirectory; string[] temp1;          
            DirectoryInfo dir_info;
            temp1 = temp.Split('\\'); LogPath = temp1[0] + "\\Argo\\logs"; EncoderTestPath = temp1[0] + "\\Argo\\Encoder_Plots"; string AFTFolderPath = temp1[0] + "\\Argo\\AFT";                  
            try
            {
                if (!Directory.Exists(AFTFolderPath))
                    dir_info = Directory.CreateDirectory(AFTFolderPath);
                if (!Directory.Exists(LogPath))
                    dir_info = Directory.CreateDirectory(LogPath);
                SysLogPath = temp1[0] + "\\Argo\\logs\\sys_logs"; 
                if (!Directory.Exists(SysLogPath))
                    dir_info = Directory.CreateDirectory(SysLogPath);
                if (!Directory.Exists(EncoderTestPath))
                    dir_info = Directory.CreateDirectory(EncoderTestPath);              
            }
            catch (Exception exception)
            {
               // MessageBox.Show(exception.Message);
                Logger.Error(null, exception.Message);
            }
            OnDisconenctUSB();
            
        }

     
        private void Events_Init()
        {
            onlineTimer.Tick += new EventHandler(OnCollectDataTimer);
            testTimer.Interval = TimeSpan.FromSeconds(10);
            testTimer.Tick += new EventHandler(OnTestTimer_Tick);
            Trainning.OnChanged += new EventHandler(Trainingmode_OnChanged);
            FirstStep.OnChanged += new EventHandler(OnEnableDisableFirstStep);
            safetyonoff.OnChanged += new EventHandler(OnOffBox_OnChanged);
            collectOnline.OnChanged += new EventHandler(OnCollectOnlineDataChanged);
            RFtestTimer.Tick += new EventHandler(OnRFTestTimer_Tick);
            RCUpdateTimer.Tick += new EventHandler(OnUpdateRCAddressIDTimer_Tick);
            calibTimer.Tick += new EventHandler(OnCalibrationTimer_Tick);
            CheckUSBConnectedTime.Tick += new EventHandler(OnCheckUSBConnectedTimeTimer_Tick);
            TestEncoderTimer.Tick += new EventHandler(OnTestEncoderTimer_Tick);
            RCUpdateTimer.Interval = TimeSpan.FromSeconds(1);
            RFtestTimer.Interval = TimeSpan.FromSeconds(1);
            calibTimer.Interval = TimeSpan.FromSeconds(1);
            TestEncoderTimer.Interval = TimeSpan.FromSeconds(1);
            CheckUSBConnectedTime.Interval = TimeSpan.FromMilliseconds(200);

         
            calibTimer.IsEnabled = false;
            TestEncoderTimer.IsEnabled = false;
            CheckUSBConnectedTime.IsEnabled = true;
            testTimer.IsEnabled = false;

         }

        void OnMainClosed(object sender, EventArgs e)
        {
            
            CheckUSBConnectedTime.IsEnabled = false;
            deviceData.Disconnect();
            progressform.Close();
            GenerateRCAddressIDForm.Close();

            MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
            MCUDownloadingProcessWindow.Close();

            
        }

        private void OnCaptionButton(object sender, RoutedEventArgs e)
        {
            if (sender == _CloseButton)
            {
                Close();
            }
            else if (sender == _MinimizeButton)
            {
                WindowState = WindowState.Minimized;
            }
            else
            {
                WindowState = (WindowState == WindowState.Maximized) ? WindowState.Normal : WindowState.Maximized;
            }
        }



        private void RemoveTitle()
        {
            UserFileloadedName.Visibility = Visibility.Hidden;
            UserFileloadedName.Text = "";
        }
        private void   SetTitle(string file)
        {
           // FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location);


           //  System.Version GUIVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
           //  Short_GUIVersion = (short)(GUIVersion.Major * 100 + GUIVersion.Build * 10 + GUIVersion.Revision);
            //deviceData_temp.Parameters.SampleTime =(short)(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);

           // deviceData.Parameters.SampleTime = (short)(GUIVersion.Major * 100 + GUIVersion.Build * 10 + GUIVersion.Revision);
           // string version = string.Format("{0}.{1}", fvi.FileMajorPart, fvi.FileMinorPart);
           // string version = string.Format("{0}.{1}", Interface_Rasem_Version_major, Interface_Rasem_Version_minor);
            //this.Title = "ReWalk Interface " + version + ((file == null) ? "" : " - " + file);
           // this.Title = "ReWalk Interface "  + ((file == null) ? "" : " - " + file);
            UserFileloadedName.Visibility = Visibility.Visible;
            UserFileloadedName.Text = "" + ((file == null) ? "" : "  " + file);
        }


       
        private void OnLoadConfiguration(object sender, RoutedEventArgs e) // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions" CS.MLC "Methods executing atomic functions"
        {
            OpenFileDialog ofd = new OpenFileDialog();
            Data deviceData_temp = new Data();
            ofd.CheckFileExists = true;
            ofd.Filter = "Argo Config files (*.xarg)|*.xarg|(*.*) |*.*";
            FileStream fs;
            try
            {
                if (ofd.ShowDialog().Value == true)
                {
                    using (fs = new FileStream(ofd.FileName, FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(DeviceParameters));
                        deviceData_temp.Parameters = (DeviceParameters)serializer.Deserialize(fs);
                        Temp_ParamData_StructureCopy(deviceData_temp);

                    }
                        if (deviceData_temp.Parameters.SampleTime >= Short_GUIVersion)
                        {
                            UserParamData_VerfiytoSave(deviceData_temp);                          
                            using (fs = new FileStream(ofd.FileName, FileMode.Create))
                            {
                                XmlSerializer serializer_save = new XmlSerializer(typeof(DeviceParameters));
                                deviceData.Parameters.SampleTime = Short_GUIVersion;
                                serializer_save.Serialize(fs, deviceData.Parameters);
                            }
                            fs.Close();
                            SetTitle(Path.GetFileNameWithoutExtension(ofd.FileName));

                        }
                        else
                        {
                            MessageBox.Show(this, "This file was created by an older Rewalk interface version and is not compatible to the current version. In order to continue the operation it will be converted to a compatible format and saved.",
                                                   "User file Loading", MessageBoxButton.OK, MessageBoxImage.Information);
                            deviceData.RestoreParameters();
                            UserParamData_VerfiytoSave(deviceData_temp);
                            // Overwrite the configuration file 
                            using (fs = new FileStream(ofd.FileName, FileMode.Create))
                            {
                                XmlSerializer serializer_save = new XmlSerializer(typeof(DeviceParameters));
                                deviceData.Parameters.SampleTime = Short_GUIVersion;
                                serializer_save.Serialize(fs, deviceData.Parameters);
                            }
                            fs.Close();
                            SetTitle(Path.GetFileNameWithoutExtension(ofd.FileName));
                        }
                    


                    Write.IsEnabled = true;
                    if (deviceData.Parameters.SwitchFSRs == false) //disable Training mode fields if training button is OFF
                    {
                        hip_final.IsEnabled = false;
                        hip_max.IsEnabled = false;
                        t_steptime.IsEnabled = false;
                    }
                    else
                    {
                        hip_final.IsEnabled = true;
                        hip_max.IsEnabled = true;
                        t_steptime.IsEnabled = true;
                    }
                    if (deviceData.Parameters.SafetyWhileStanding == false) //disable safety if SafetyWhileStanding                                   
                        Y_Thr.IsEnabled = false;
                    else
                        Y_Thr.IsEnabled = true;

                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(this, "Load configuration failed: Corrupted configuration file.");// + exception.Message);
                Logger.Error("Load configuration failed: ", exception.Message);
            }
        }


        private  bool ValidValueofHipFlexion(short v)
        {

            if (v >= Convert.ToInt16(Hip_Flexion.Items.GetItemAt(0)) && v <= Convert.ToInt16(Hip_Flexion.Items.GetItemAt(Hip_Flexion.Items.Count - 1))) 
             {
                 return true;
             }

            else
             return false;
        }
        private  bool ValidValueofKneeFlexion(short v)
        {

            if (v >= Convert.ToInt16(Knee_Flexion.Items.GetItemAt(0)) && v <= Convert.ToInt16(Knee_Flexion.Items.GetItemAt(Knee_Flexion.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofBackHipAngle(short v)
        {

            if (v >= Convert.ToInt16(t_steptime.Items.GetItemAt(0)) && v <= Convert.ToInt16(t_steptime.Items.GetItemAt(t_steptime.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofMaxVelocity(short v)
        {

            if (v >= Convert.ToInt16(Step_Time.Items.GetItemAt(0)) && v <= Convert.ToInt16(Step_Time.Items.GetItemAt(Step_Time.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofTiltDelta(short v)
        {

            if (v >= Convert.ToInt16(tilt_delta.Items.GetItemAt(0)) && v <= Convert.ToInt16(tilt_delta.Items.GetItemAt(tilt_delta.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofTiltTimeout(short v)
        {

            if (v >= Convert.ToInt16(tilt_timeout.Items.GetItemAt(0)) && v <= Convert.ToInt16(tilt_timeout.Items.GetItemAt(tilt_timeout.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofYThreshold(short v)
        {

            if (v >= Convert.ToInt16(Y_Thr.Items.GetItemAt(0)) && v <= Convert.ToInt16(Y_Thr.Items.GetItemAt(Y_Thr.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofDelayBetweenSteps(short v)
        {

            if (v >= Convert.ToInt16(delay_between_steps.Items.GetItemAt(0)) && v <= Convert.ToInt16(delay_between_steps.Items.GetItemAt(delay_between_steps.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofFSRThreshold(short v)
        {

            if (v >= Convert.ToInt16(hip_max.Items.GetItemAt(0)) && v <= Convert.ToInt16(hip_max.Items.GetItemAt(hip_max.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofFSRWalkTimeout(short v)
        {

            if (v >= Convert.ToInt16(hip_final.Items.GetItemAt(0)) && v <= Convert.ToInt16(hip_final.Items.GetItemAt(hip_final.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofFirstStepFlexion(short v)
        {

            if (v >= Convert.ToInt16(cbFirstStepFlexion.Items.GetItemAt(0)) && v <= Convert.ToInt16(cbFirstStepFlexion.Items.GetItemAt(cbFirstStepFlexion.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofWalk_Current_Threshold(short v)
        {

            if (v >= Convert.ToInt16(Walk_Current_Thr.Items.GetItemAt(0)) && v <= Convert.ToInt16(Walk_Current_Thr.Items.GetItemAt(Walk_Current_Thr.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofStairs_Current_Threshold(short v)
        {

            if (v >= Convert.ToInt16(StairCurrentThreshold.Items.GetItemAt(0)) && v <= Convert.ToInt16(StairCurrentThreshold.Items.GetItemAt(StairCurrentThreshold.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofLHAsc(short v)
        {

            if (v >= Convert.ToInt16(LHASC.Items.GetItemAt(0)) && v <= Convert.ToInt16(LHASC.Items.GetItemAt(LHASC.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofLHDsc(short v)
        {

            if (v >= Convert.ToInt16(LHDSC.Items.GetItemAt(0)) && v <= Convert.ToInt16(LHDSC.Items.GetItemAt(LHDSC.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofLKAsc(short v)
        {

            if (v >= Convert.ToInt16(LKASC.Items.GetItemAt(0)) && v <= Convert.ToInt16(LKASC.Items.GetItemAt(LKASC.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofLKDsc(short v)
        {

            if (v >= Convert.ToInt16(LKDSC.Items.GetItemAt(0)) && v <= Convert.ToInt16(LKDSC.Items.GetItemAt(LKDSC.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofRHAsc(short v)
        {

            if (v >= Convert.ToInt16(RHASC.Items.GetItemAt(0)) && v <= Convert.ToInt16(RHASC.Items.GetItemAt(RHASC.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofRHDsc(short v)
        {

            if (v >= Convert.ToInt16(RHDSC.Items.GetItemAt(0)) && v <= Convert.ToInt16(RHDSC.Items.GetItemAt(RHDSC.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofRKAsc(short v)
        {

            if (v >= Convert.ToInt16(RKASC.Items.GetItemAt(0)) && v <= Convert.ToInt16(RKASC.Items.GetItemAt(RKASC.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }
        private bool ValidValueofRKDsc(short v)
        {

            if (v >= Convert.ToInt16(RKDSC.Items.GetItemAt(0)) && v <= Convert.ToInt16(RKDSC.Items.GetItemAt(RKDSC.Items.Count - 1)))
            {
                return true;
            }

            else
                return false;
        }            


        private void UserParamData_VerfiytoSave(Data deviceData_temp) // parasoft-suppress  METRICS.MCCC "Methods executing atomic functions" METRICS.MLOC "Methods executing atomic functions" CS.MLC "Methods executing atomic functions"
        {
            if ( ValidValueofHipFlexion(deviceData_temp.Parameters.HipAngle) == true)
                deviceData.Parameters.HipAngle = deviceData_temp.Parameters.HipAngle;

            if ( ValidValueofKneeFlexion(deviceData_temp.Parameters.KneeAngle) == true)
                deviceData.Parameters.KneeAngle = deviceData_temp.Parameters.KneeAngle;

            if (ValidValueofBackHipAngle(deviceData_temp.Parameters.BackHipAngle) == true)
                deviceData.Parameters.BackHipAngle =  deviceData_temp.Parameters.BackHipAngle;

            if (ValidValueofMaxVelocity(deviceData_temp.Parameters.MaxVelocity) == true)
                deviceData.Parameters.MaxVelocity = deviceData_temp.Parameters.MaxVelocity;//Step time

            if ( ValidValueofTiltDelta(deviceData_temp.Parameters.TiltDelta) == true)
                deviceData.Parameters.TiltDelta = deviceData_temp.Parameters.TiltDelta;

            if ( ValidValueofTiltTimeout(deviceData_temp.Parameters.TiltTimeout) == true)
               deviceData.Parameters.TiltTimeout = deviceData_temp.Parameters.TiltTimeout;

            if (deviceData_temp.Parameters.SafetyWhileStanding == true || deviceData_temp.Parameters.SafetyWhileStanding == false)
                deviceData.Parameters.SafetyWhileStanding = deviceData_temp.Parameters.SafetyWhileStanding;

            if (deviceData_temp.Parameters.XThreshold != 0)
                deviceData.Parameters.XThreshold = deviceData_temp.Parameters.XThreshold;

            if ( ValidValueofYThreshold(deviceData_temp.Parameters.YThreshold) == true)
                deviceData.Parameters.YThreshold = deviceData_temp.Parameters.YThreshold;

            if ( ValidValueofDelayBetweenSteps(deviceData_temp.Parameters.DelayBetweenSteps) == true)
                deviceData.Parameters.DelayBetweenSteps = deviceData_temp.Parameters.DelayBetweenSteps;

            if ( deviceData_temp.Parameters.BuzzerBetweenSteps == true || deviceData_temp.Parameters.BuzzerBetweenSteps == false)
                deviceData.Parameters.BuzzerBetweenSteps = deviceData_temp.Parameters.BuzzerBetweenSteps;

            if (deviceData_temp.Parameters.Record == true || deviceData_temp.Parameters.Record == false)
                deviceData.Parameters.Record = deviceData_temp.Parameters.Record;//Enable walk sfter stuck

            if ( deviceData_temp.Parameters.SwitchFSRs == true ||  deviceData_temp.Parameters.SwitchFSRs == false)
                deviceData.Parameters.SwitchFSRs = deviceData_temp.Parameters.SwitchFSRs;//Beginner mode enable\disable

            if ( ValidValueofFSRThreshold(deviceData_temp.Parameters.FSRThreshold) == true )
                deviceData.Parameters.FSRThreshold = deviceData_temp.Parameters.FSRThreshold;//Beginner Hip max Flexion

            if ( ValidValueofFSRWalkTimeout(deviceData_temp.Parameters.FSRWalkTimeout) == true)
                deviceData.Parameters.FSRWalkTimeout = deviceData_temp.Parameters.FSRWalkTimeout;//Beginner Hip final flexion

            if ( deviceData_temp.Parameters.EnableFirstStep == true || deviceData_temp.Parameters.EnableFirstStep == false)
                deviceData.Parameters.EnableFirstStep = deviceData_temp.Parameters.EnableFirstStep;

            if ( ValidValueofFirstStepFlexion(deviceData_temp.Parameters.FirstStepFlexion) ==  true)
               deviceData.Parameters.FirstStepFlexion = deviceData_temp.Parameters.FirstStepFlexion;

            if ( ValidValueofWalk_Current_Threshold(deviceData_temp.Parameters.Walk_Current_Threshold) == true)
               deviceData.Parameters.Walk_Current_Threshold = deviceData_temp.Parameters.Walk_Current_Threshold;

            if ( ValidValueofStairs_Current_Threshold(deviceData_temp.Parameters.Stairs_Current_Threshold) == true)
                deviceData.Parameters.Stairs_Current_Threshold = deviceData_temp.Parameters.Stairs_Current_Threshold;

            if ( ValidValueofLHAsc(deviceData_temp.Parameters.LHAsc) == true)
                deviceData.Parameters.LHAsc = deviceData_temp.Parameters.LHAsc;

            if ( ValidValueofLHDsc(deviceData_temp.Parameters.LHDsc) == true)
                deviceData.Parameters.LHDsc = deviceData_temp.Parameters.LHDsc;

            if ( ValidValueofLKAsc(deviceData_temp.Parameters.LKAsc) == true)
                deviceData.Parameters.LKAsc = deviceData_temp.Parameters.LKAsc;

            if ( ValidValueofLKDsc(deviceData_temp.Parameters.LKDsc) == true)
                deviceData.Parameters.LKDsc = deviceData_temp.Parameters.LKDsc;

            if ( ValidValueofRHAsc(deviceData_temp.Parameters.RHAsc) == true)
                deviceData.Parameters.RHAsc = deviceData_temp.Parameters.RHAsc;

            if ( ValidValueofRHDsc(deviceData_temp.Parameters.RHDsc) == true)
                deviceData.Parameters.RHDsc = deviceData_temp.Parameters.RHDsc;

            if (ValidValueofRKAsc(deviceData_temp.Parameters.RKAsc) == true)
                deviceData.Parameters.RKAsc = deviceData_temp.Parameters.RKAsc;

            if (ValidValueofRKDsc(deviceData_temp.Parameters.RKDsc) == true)
                deviceData.Parameters.RKDsc = deviceData_temp.Parameters.RKDsc;



         
        }

        private void Temp_ParamData_StructureCopy(Data deviceData_temp)
         {
                        deviceData_temp.Parameters.RCAddress = deviceData.Parameters.RCAddress;
                        deviceData_temp.Parameters.RCId = deviceData.Parameters.RCId;
                        deviceData_temp.Parameters.RCBER = deviceData.Parameters.RCBER;
                        deviceData_temp.Parameters.Remote = deviceData.Parameters.Remote;
                        deviceData_temp.Parameters.RFProgress = deviceData.Parameters.RFProgress;                       
                        deviceData_temp.Parameters.Sn1Char = deviceData.Parameters.Sn1Char;
                        deviceData_temp.Parameters.Sn1 = deviceData.Parameters.Sn1;
                        deviceData_temp.Parameters.Sn2 = deviceData.Parameters.Sn2;
                        deviceData_temp.Parameters.Sn3 = deviceData.Parameters.Sn3;
                        deviceData_temp.Parameters.StairsEnabled = deviceData.Parameters.StairsEnabled;
                        deviceData_temp.Parameters.SysType = deviceData.Parameters.SysType;
                        deviceData_temp.Parameters.SysTypeString = deviceData.Parameters.SysTypeString;
                        deviceData_temp.Parameters.DSP = deviceData.Parameters.DSP;
                        deviceData_temp.Parameters.Boot = deviceData.Parameters.Boot;
                        deviceData_temp.Parameters.Motor = deviceData.Parameters.Motor;
                        deviceData_temp.Parameters.FPGA = deviceData.Parameters.FPGA;
                        deviceData_temp.Parameters.Device = deviceData.Parameters.Device;
                        deviceData_temp.Parameters.RcSN = deviceData.Parameters.RcSN;
                        deviceData_temp.Parameters.InfSN = deviceData.Parameters.InfSN;
                        deviceData_temp.Parameters.LkSN = deviceData.Parameters.LkSN;
                        deviceData_temp.Parameters.LhSN = deviceData.Parameters.LhSN;
                        deviceData_temp.Parameters.RkSN = deviceData.Parameters.RkSN;
                        deviceData_temp.Parameters.RhSN = deviceData.Parameters.RhSN;
                        deviceData_temp.Parameters.EnableSystemStuckOnError_int = deviceData.Parameters.EnableSystemStuckOnError_int;
                        deviceData_temp.Parameters.SwitchingTime = deviceData.Parameters.SwitchingTime;
                        deviceData_temp.Parameters.StepCounter = deviceData.Parameters.StepCounter;

                        deviceData_temp.Parameters.LogLevel.ADC = deviceData.Parameters.LogLevel.ADC;
                        deviceData_temp.Parameters.LogLevel.CAN = deviceData.Parameters.LogLevel.CAN;
                        deviceData_temp.Parameters.LogLevel.STAIRS = deviceData.Parameters.LogLevel.STAIRS;
                        deviceData_temp.Parameters.LogLevel.USB = deviceData.Parameters.LogLevel.USB;
                        deviceData_temp.Parameters.LogLevel.MCU = deviceData.Parameters.LogLevel.MCU;
                        deviceData_temp.Parameters.LogLevel.RF = deviceData.Parameters.LogLevel.RF;
                        deviceData_temp.Parameters.LogLevel.BIT = deviceData.Parameters.LogLevel.BIT;
                        deviceData_temp.Parameters.LogLevel.RC = deviceData.Parameters.LogLevel.RC;
                        deviceData_temp.Parameters.LogLevel.SIT_STN = deviceData.Parameters.LogLevel.SIT_STN;
                        deviceData_temp.Parameters.LogLevel.WALK = deviceData.Parameters.LogLevel.WALK;

                       
        }



        byte[] MCUFileArray, INFDSPFileArray, INFBootFileArray;


        private void Delay_ms(double ms)
        {
            double _val = 0;
            Stopwatch stopwatch1 = new Stopwatch();
             stopwatch1.Start();
                            
                            while (stopwatch1.ElapsedMilliseconds < ms)
                            {
                                _val++;
                            }
                            stopwatch1.Reset();
                            _val = 0;
      
        }


        
        private void OnDownloadDspFile(object sender, RoutedEventArgs e)
        {
           
         
                INFDSPFileArray = File.ReadAllBytes(DspCodeFile);                
                string inf_app = File.ReadAllText(DspCodeFile);
                byte[] INFDSPFileArray_WithoutSignature = new byte[INFDSPFileArray.Length - inf_app.IndexOf('6')];
                Array.Copy(INFDSPFileArray, inf_app.IndexOf('6'), INFDSPFileArray_WithoutSignature, 0, INFDSPFileArray_WithoutSignature.Length);
                if (inf_app.Contains("#INF Successful Generated App#") || (short)deviceData.Parameters.DSP <= 0x0221) //from version INF DSP 2.33 this signature was added
                {                  
                    if ((short)deviceData.Parameters.Boot < 0x0202)
                    {
                        MessageBoxResult continue_download = MessageBox.Show(this, "The current boot version is not compatible with this DSP version!\nPlease update the boot version first.\nDownloading this DSP version might cause the device to mulfunction.\nDo you want to continue?", "INF app Downloading...", MessageBoxButton.YesNoCancel, MessageBoxImage.Error);
                        if ((MessageBoxResult.No == continue_download) || (MessageBoxResult.Cancel == continue_download)) return;                                                                       
                    }
                    MessageBoxResult result = MessageBox.Show(this, "You are about to update the INF SW version\nThis will take several seconds, please do not turn off the system!\n", "INF app Downloading...", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);                  
                    if (MessageBoxResult.OK == result)
                    {                       
                       if ((short)deviceData.Parameters.DSP <= 0x0221)
                        {                          
                            Mouse.OverrideCursor = true ? Cursors.Wait : null;
                            deviceData.DownloadDSP(INFDSPFileArray_WithoutSignature);
                            //Thread.Sleep(5000);
                            Delay_ms(5000);  Mouse.OverrideCursor = this.Cursor;                           
                            MessageBox.Show(this, "Downloading new INF's software was finished successfully\nPlease, Restart the system!!\n", "INF app Downloading...", MessageBoxButton.OK, MessageBoxImage.Information);
                         }
                        else
                        {
                            Mouse.OverrideCursor = true ? Cursors.Wait : null;
                            deviceData.DownloadDSP(INFDSPFileArray_WithoutSignature);
                            //Thread.Sleep(5000);
                            Delay_ms(5000); Mouse.OverrideCursor = this.Cursor;                            
                            //reset status
                            deviceData.TestData.ErrorCodeINF = 0;
                            deviceData.RequestTestStatus();
                            if (deviceData.TestData.ErrorCodeINF == (short)0x7fff) //success download                       
                                MessageBox.Show(this, "Downloading new INF's software was finished successfully\nPlease, Restart the system!!\n", "INF app Downloading...", MessageBoxButton.OK, MessageBoxImage.Information);                          
                            else                     
                               MessageBox.Show("INF application downloading has failed!\nError Code: " + deviceData.TestData.ErrorCodeINF.ToString(), "INF app Downloading...", MessageBoxButton.OK, MessageBoxImage.Error);                   
                        }                   

                    }
                    else
                        return;
               }
                else
                {
                    MessageBox.Show("The selected file is not an upgrading INF App file !\nPlease select another file and try again.", "INF app  Downloading...", MessageBoxButton.OK, MessageBoxImage.Hand);
                    return;
                }

                         
           /* catch (Exception exception)
            {
                //MessageBox.Show(this, "INF file download failed: " + exception.Message, "DSP Download Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Logger.Error_MessageBox_Advanced("INF file download failed: ", "DSP Download Error",exception.Message);
                
            }*/
            
        }

   
  
        private   Window1 MCUDownloadingProcessWindow = new Window1();
        private void OnDownloadMcuFile(object sender, RoutedEventArgs e)
        {
           
            try
            {                
                if (ISMCUFileWasSelected)
                {
                    MCUFileArray = File.ReadAllBytes(MCUCodeFile);
                    string mcu = File.ReadAllText((MCUCodeFile));
              if ( mcu.Contains("#MCU Successful Generated File#") && mcu.Contains("$A") )
                {
                MessageBoxResult result = MessageBox.Show(this, "You are about to update the MCU SW version\nThis will take several seconds, please do not turn off the system!\n", "MCU app Downloading...", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
               
                if (MessageBoxResult.OK == result)
                {
                            //start downloading - restart downloading parameters
                            Mouse.OverrideCursor = true ? Cursors.Wait : null;
                            Joint_Being_Downloaded = 0x330;
                            MCUDownloadingProcessWindow.MCUDOWNLOADINGINPROCESS = 1;//start bar filling
                            MCUDownloadingProcessWindow.VALUE = 0;
                            MCUDownloadingProcessWindow.lblWorking.Content = "Starting LH Dowloading...";
                            deviceData.DownloadMCU(MCUFileArray);
                            //reset status
                            deviceData.TestData.ErrorCodeLH = 0; deviceData.TestData.ErrorCodeLK = 0; deviceData.TestData.ErrorCodeRH = 0; deviceData.TestData.ErrorCodeRK = 0;
                            btnMCUDownload.IsEnabled = false;
                            testTimer.Start();                         
                            MCUDownloadingProcessWindow.Show();
                            MCUDownloadingProcessWindow.Focus();
                            MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.RunWorkerAsync();
                        }
                        else //cancel button was pressed
                         return;
                      
                    }
                    else
              
                         { MessageBox.Show("The selected file is not an upgrading MCU file !\nPlease select another file and try again.", "MCU app Downloading...", MessageBoxButton.OK, MessageBoxImage.Hand);}
                                    
                }
                else
                        { MessageBoxResult result = MessageBox.Show(this, "No MCU file was selected!\nPress the Browse button first and select an appropriate MCU file.", "MCU app Downloading...", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);}

            }
            catch (Exception exception)
            {
                //MessageBox.Show(this, "MCU file download failed: " + exception.Message,"MCU Download Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Logger.Error_MessageBox_Advanced("MCU file download failed: ", "MCU Download Error", exception.Message);
            }
            
        }

        private void OnDownloadBootFile(object sender, RoutedEventArgs e)
        {
            //try
            //{
               INFBootFileArray = File.ReadAllBytes(BootCodeFile);
               string inf_boot = File.ReadAllText(BootCodeFile);
               byte[] INFBootFileArray_WithoutSignature = new byte[INFBootFileArray.Length - inf_boot.IndexOf('6')];
               Array.Copy(INFBootFileArray, inf_boot.IndexOf('6'), INFBootFileArray_WithoutSignature, 0, INFBootFileArray_WithoutSignature.Length);
               if (inf_boot.Contains("#INF Successful Generated Boot#") || (short)deviceData.Parameters.Boot <= 0x0203)//Last version of boot 2.3
                {
                MessageBoxResult result = MessageBox.Show(this, "You are about to update the INF Boot SW version\nThis will take several seconds, please do not turn off the system!\n", "INF Boot Downloading...", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);             
                if (MessageBoxResult.OK == result)
                {
                    if ((short)deviceData.Parameters.DSP <= 0x0221)
                    {    
                         Mouse.OverrideCursor = true ? Cursors.Wait : null;
                         deviceData.DownloadRC(INFBootFileArray_WithoutSignature);//tbRcFileLocation.Text));
                        // Thread.Sleep(3000);
                         Delay_ms(3000); // replace Thread.Sleep
                         Mouse.OverrideCursor = this.Cursor;
                         MessageBox.Show(this, "Downloading new INF's Boot  software was finished successfully\nPlease, Restart the system!!\n", "INF Boot Downloading...", MessageBoxButton.OK, MessageBoxImage.Information);                        
                    }
                    else
                    {
                         // MessageBox.Show("new version");                                     
                        Mouse.OverrideCursor = true ? Cursors.Wait : null;
                        deviceData.DownloadRC(INFBootFileArray_WithoutSignature);//tbRcFileLocation.Text));
                       // Thread.Sleep(3000);
                        Delay_ms(3000); // replace Thread.Sleep
                        //reset status
                        deviceData.TestData.ErrorCodeRC = 0;
                        deviceData.RequestTestStatus();
                        Mouse.OverrideCursor = this.Cursor;
                        if (deviceData.TestData.ErrorCodeRC == (short)0x7fff) //success download
                            MessageBox.Show(this, "Downloading new INF's Boot  software was finished successfully\nPlease, Restart the system!!\n", "INF Boot Downloading...", MessageBoxButton.OK, MessageBoxImage.Information);
                        else
                            MessageBox.Show("INF boot downloading has failed!\nError Code: " + deviceData.TestData.ErrorCodeRC.ToString(), "INF Boot Downloading...", MessageBoxButton.OK, MessageBoxImage.Error);
                       }
                 }
               // }
                else
                {
                   MessageBox.Show("The selected file is not an upgrading INF Boot file !\nPlease select another file and try again.", "INF Boot Downloading...", MessageBoxButton.OK, MessageBoxImage.Hand);
                   return;
                }
            }
           /* catch (Exception exception)
            {
               // MessageBox.Show(this, "Boot file download failed: " + exception.Message);
                Logger.Error("Boot file download failed: " , exception.Message);
            }*/
         

   
        }

        string DspCodeFile, BootCodeFile, MCUCodeFile;

        private void OnBrowseDspFile(object sender, RoutedEventArgs e)
        {
            //string file = BrowseTextFile();
            string file = BrowseINFAppFile();
            DspCodeFile = file;
            if (((short)deviceData.Parameters.Boot < 0x0202) && ((short)deviceData.Parameters.Boot != 0))
            {
                MessageBoxResult continue_download = MessageBox.Show(this, "The current INF boot version is not compatible with this INF app version!\nPlease update the boot version first.\nDownloading this INF app version might cause the device to mulfunction.\nDo you want to continue?", "INF app Downloading...", MessageBoxButton.YesNoCancel, MessageBoxImage.Error);
               
                if ((MessageBoxResult.No == continue_download) || (MessageBoxResult.Cancel == continue_download))
                {
                    return;
                }
            }

            if (!string.IsNullOrEmpty(file))
            {
                if (file.Length > 33)
                {
                    tbDspFileLocation.Text = file.Remove(33);
                    tbDspFileLocation.Text = tbDspFileLocation.Text + "...";
                }
                else
                {
                    tbDspFileLocation.Text = file;
                }
            }
        }
        private bool ISMCUFileWasSelected = false; 
        private void OnBrowseMcuFile(object sender, RoutedEventArgs e)
        {
          

            //string file = BrowseTextFile();
            string file = BrowseMCUFile();
            MCUCodeFile = file;
            if (!string.IsNullOrEmpty(file))
            {

                if (file.Length > 33)
                {
                    tbMcuFileLocation.Text = file.Remove(33);
                    tbMcuFileLocation.Text = tbMcuFileLocation.Text + "...";
                   
                }
                else
                {
                    tbDspFileLocation.Text = file;
                }

                ISMCUFileWasSelected = true;



            }
            else
            {
                ISMCUFileWasSelected = false;
            }
        }

        private void OnBrowseBootFile(object sender, RoutedEventArgs e)
        {
           // string file = BrowseTextFile();
            string file = BrowseINFBootFile();
            BootCodeFile = file;
            if (!string.IsNullOrEmpty(file))
            {
                if (file.Length > 33)
                {
                    tbRcFileLocation.Text = file.Remove(33);
                    tbRcFileLocation.Text = tbRcFileLocation.Text + "...";
                }
                else
                {
                    tbRcFileLocation.Text = file;
                }
            }
        }

      
        private string BrowseMCUFile()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.CheckFileExists = true;
            ofd.RestoreDirectory = true;
            ofd.Filter = "MCU Files (*.mcu)|*.mcu";
            if (ofd.ShowDialog().Value == true)
            {
                return ofd.FileName;
            }
            else
            {
                return null;
            }
        }

        private string BrowseINFAppFile()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.CheckFileExists = true;
            ofd.RestoreDirectory = true;
            ofd.Filter = "INF App Files (*.ainf)|*.ainf";
            if (ofd.ShowDialog().Value == true)
            {
                return ofd.FileName;
            }
            else
            {
                return null;
            }
        }
        private string BrowseINFBootFile()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.CheckFileExists = true;
            ofd.RestoreDirectory = true;
            ofd.Filter = "INF Boot Files (*.binf)|*.binf";
            if (ofd.ShowDialog().Value == true)
            {
                return ofd.FileName;
            }
            else
            {
                return null;
            }
        }

        
        private void OnSaveConfiguration(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "(*.xarg)|*.xarg|(*.*) |*.*";
            try
            {
                if (sfd.ShowDialog().Value == true)
                {
                    using (FileStream fs = new FileStream(sfd.FileName, FileMode.OpenOrCreate))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(DeviceParameters));
                        deviceData.Parameters.SampleTime = Short_GUIVersion;
                        serializer.Serialize(fs, deviceData.Parameters);
                        fs.Close();
                        SetTitle(Path.GetFileNameWithoutExtension(sfd.FileName));
                       
                    }
                }
            }
            catch (Exception exception)
            {
                //MessageBox.Show(this, "Save configuration failed: " + exception.Message);
                Logger.Error("Save configuration failed: ", exception.Message);
            }
        }
        private void OnUpdateSettings(object sender, RoutedEventArgs e)
        {
            try
            {
                              
                deviceData.WriteParameters();
                RemoveTitle();
               // Thread.Sleep(1200);
                Delay_ms(1200); // replace Thread.Sleep
                MessageBox.Show("Parameters were written successfully to the Rewalk","Write",MessageBoxButton.OK ,MessageBoxImage.Exclamation);
            }
            catch (DeviceOperationException exception)
            {
               // MessageBox.Show(this, "Device Operation failed: " + exception.Message);
                Logger.Error("Device Operation failed: ", exception.Message);
            }
        }
        private void OnReadDeviceSettings(object sender, RoutedEventArgs e)
        {

            ReadDeviceSettings();
            //Check_KneeFlexion_Selection();
            //Thread.Sleep(1200);
            //Check_StairsThreshold(); moved into ReadDeviceSettings 
        }
        private bool ReadDeviceSettings(bool throwException = true) // parasoft-suppress  CS.MLC "Methods executing atomic functions"
        {
            try
            {
                deviceData.ReadData();
                RemoveTitle();
                if (GUIVersionType == "USA")
                {
                    //Enforce StairsEnable to be false in "INF"  -- Stairs is forbidden in USA
                    if (true == deviceData.Parameters.StairsEnabled)
                    {
                        deviceData.Parameters.StairsEnabled = false;
                        deviceData.WriteParameters();
                    }
                    else
                    {
                        deviceData.Parameters.StairsEnabled = false;
                    }
                }
                Write.IsEnabled = true; Expander.IsEnabled = true; Expander.IsExpanded = true;

                Check_DSP_MOTOR_AppVer();

                if ((short)deviceData.Parameters.DSP != 0x0909) // dsp application version 
                {
                    Check_DSP_IfBeyondVer_030A();
                    Par_set_true_inRead();
                   
                    
                }

                else if ((short)deviceData.Parameters.DSP == 0x0909)//boot version ,0x0909 code of boot version
                {          
                    Parm_set_false_inRead();
                    if (!(App.Current as App).DeptName_RunINFtoApplication)
                    {
                    MessageBoxResult result = MessageBox.Show("Rewalk is running in BOOT mode, limited operations are available!\nDo you want to switch to operational mode?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Information);
                    if (MessageBoxResult.Yes == result)
                        deviceData.Disconnect();                   
                    }
                    else
                    {
                        deviceData.Disconnect();
                        RunINFtoApplication = false;

                    }
                }
                if ((short)deviceData.Parameters.Motor == 0x0909)//boot version
                {
                    DSP_boot_ver_txtblock.Visibility = System.Windows.Visibility.Visible;
                    MessageBox.Show("MCU is running in BOOT mode, Download new motors software or restart the device", "Warning", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                Check_Safety_Trainning_Status();
               

                Check_StairsThreshold();
                
       
                return true;
            }//end try



            catch (DeviceOperationException exception){
            
                if (throwException)
                    Logger.Error("Device Operation failed: ", exception.Message);

                return false; 
            }
        }
        private  void Check_Safety_Trainning_Status()
        {
            if (safetyonoff.IsOn == false)
                Y_Thr.IsEnabled = false;
            else if (safetyonoff.IsOn == true)
                Y_Thr.IsEnabled = true;

            if (Trainning.IsOn == false)
            { hip_final.IsEnabled = false; hip_max.IsEnabled = false; t_steptime.IsEnabled = false; }
            else if (Trainning.IsOn == true)
            { hip_final.IsEnabled = true; hip_max.IsEnabled = true; t_steptime.IsEnabled = true; }

        }
        private void Check_DSP_IfBeyondVer_030A()
        {

             if ((short)deviceData.Parameters.DSP > 0x030A)
                    {
                        short SysErrorStatus = deviceData.Parameters.SysErrorType;//deviceData.GetErrorStatus();
                        string  ReportProcessWindow = "System Error";
                        if (-1 != SysErrorStatus)
                        {
                            btnClearSysErr.IsEnabled = true;
                            btnFlashLogBrowse.IsEnabled = true;
                            tbSysErr.Text = SysErrorStatus.ToString();

                            if (!LogFileSent)
                            {
                                
                                if (SysErrorStatus > 30)
                                {
                                    ReportProcessWindow = "System Warning";//"System Error #";
                                }
                                
                                progressform = (Report_Progress)Application.LoadComponent(new Uri("Report_Progress.xaml", UriKind.Relative));
                                progressform.labelErrorReport.Content = ReportProcessWindow + SysErrorStatus;
                                progressform.AppendProgress(ReportProcessWindow + " occured\n");
                                progressform.AppendProgress("Please contact technical support before using the system again.\n");
                                string output = string.Format("{0,0} ", "Saving log files......"); progressform.AppendProgress(output);
                                
                                progressform.Visibility = System.Windows.Visibility.Visible;
                                progressform.Show();
                                SendEmailReport((ushort)SysErrorStatus); LogFileSent = true; 
                                                          
                            }
                        }
                        else
                        {

                            btnFlashLogBrowse.IsEnabled = false; btnClearSysErr.IsEnabled = false; tbSysErr.Text = "None";
                            
                           
                        }
                    }
}
        private void Check_DSP_MOTOR_AppVer()
              {
                if (((short)deviceData.Parameters.Motor == 0x0909) && ((short)deviceData.Parameters.DSP != 0x0909)) // dsp application version 
                {
                    string temp = DSP_boot_ver_txtblock.Text;
                    string[] temp_arr = new string[8];
                    temp_arr = temp.Split(' ');
                    string version = temp_arr[temp_arr.Length - 1];
                    DSP_boot_ver_txtblock.Text = "MCU Boot: " + version;
                }
                else
                {
                    string temp = DSP_boot_ver_txtblock.Text;
                    string[] temp_arr = new string[8];
                    temp_arr = temp.Split(' ');
                    string version = temp_arr[temp_arr.Length - 1];
                 //   DSP_boot_ver_txtblock.Text = "INF Boot: " + version;
                }
              }
        public void Par_set_true_inRead()
        {
            btnBootBrowse.IsEnabled = true;
            btnBootDownload.IsEnabled = true;
            Write.IsEnabled = true;
            Load.IsEnabled = true;
            Restore.IsEnabled = true;
            Save.IsEnabled = true;
            gbWalk.IsEnabled = true;
            gbTilt.IsEnabled = true;
            gbSafety.IsEnabled = true;
            gbBeginner.IsEnabled = true;
            gbStairs.IsEnabled = true;
            gbRC.IsEnabled = true;
            gbCalibration.IsEnabled = true;
            gbSerialNumbers.IsEnabled = true;
            gbSysConfig.IsEnabled = true;
            collectOnline.IsEnabled = true;
            gbLogSettings.IsEnabled = true;
            btnBrowseLog.IsEnabled = true;
            btnSaveSendLog.IsEnabled = true;
            gbLogData.IsEnabled = true;
            gbCustomCommands.IsEnabled = true;
            btnCom1.IsEnabled = true;
            btnCom2.IsEnabled = true;
            btnCom3.IsEnabled = true;
            btnCom4.IsEnabled = true;
            btnCom5.IsEnabled = true;
            btnCom6.IsEnabled = true;
            btnCom7.IsEnabled = true;
            pbSysConfig.IsEnabled = true;
            btnDSPBrowse.IsEnabled = true;
            btnDSPDownload.IsEnabled = true;
            btnMCUBrowse.IsEnabled = true;
            btnMCUDownload.IsEnabled = true;
            groupflashlog.IsEnabled = true;
            gbErrors.IsEnabled = true;
            DSP_App_ver_txtblock.Visibility = System.Windows.Visibility.Visible;
            MOTOR_ver_txtblock.Visibility = System.Windows.Visibility.Visible;
            RC_ver_txtblock.Visibility = System.Windows.Visibility.Visible;
            DSP_boot_ver_txtblock.Visibility = System.Windows.Visibility.Visible;
            
            App temp = App.Current as App;
            if (temp != null)
             {
 
             temp.DeptName_Connected = true;
             temp.DeptName_AFTCONWALKST =deviceData.Parameters.AFTWalkStatus;
             temp.DeptName_AFTCONWALKCOUNTER = (short)(deviceData.Parameters.StepCounter - PrevStepCounter);
             PrevStepCounter = deviceData.Parameters.StepCounter;
             temp.DeptName_AFTCONASCST= deviceData.Parameters.AFTASCStatus;
             temp.DeptName_AFTCONASCCOUNTER = deviceData.Parameters.AFTASCCounter;
             temp.DeptName_AFTCONDSCST= deviceData.Parameters.AFTDSCStatus;
             temp.DeptName_AFTCONDSCCOUNTER = deviceData.Parameters.AFTDSCCounter;
             temp.DeptName_AFTCONSITSTANDST= deviceData.Parameters.AFTSitStatus;
             temp.DeptName_AFTCONSITSTANDCOUNTER = deviceData.Parameters.AFTSitCounter;
			 }
        }
        UInt32 PrevStepCounter = 0;
        public void Parm_set_false_inRead()
        {
            Write.IsEnabled = false;
            Load.IsEnabled = false;
            Restore.IsEnabled = false;
            Save.IsEnabled = false;
            gbWalk.IsEnabled = false;
            gbTilt.IsEnabled = false;
            gbSafety.IsEnabled = false;
            gbBeginner.IsEnabled = false;
            gbStairs.IsEnabled = false;
            gbRC.IsEnabled = false;
            gbErrors.IsEnabled = false;
            BtnRunEncoderTest.IsEnabled = false;
            DSP_App_ver_txtblock.Visibility = System.Windows.Visibility.Hidden;
            MOTOR_ver_txtblock.Visibility = System.Windows.Visibility.Hidden;
            RC_ver_txtblock.Visibility = System.Windows.Visibility.Hidden;
            DSP_boot_ver_txtblock.Visibility = System.Windows.Visibility.Visible;
            btnBootBrowse.IsEnabled = false;
            btnBootDownload.IsEnabled = false;
            
            App temp = App.Current as App;
            if (temp != null)
                //(App.Current as App).DeptName_Connected = false;
                temp.DeptName_Connected = false;
         }
        private void OnRestoreDeviceSettings(object sender, RoutedEventArgs e)
        {
            
            
            try
            {
                deviceData.RestoreParameters();
                if (GUIVersionType == "USA")
                {
                    deviceData.Parameters.StairsEnabled = false;
                }
            }
            catch (DeviceOperationException exception)
            {
               // MessageBox.Show(this, "Device Operation failed: " + exception.Message);
                Logger.Error("Device Operation failed: ", exception.Message);
            }
        }

        private void OnEditRemoteAddress(object sender, RoutedEventArgs e)
        {
            //btnSetRCId.IsEnabled = true;
            MessageBoxResult Result;
            Result = MessageBox.Show("Are you sure you want to change address/ID Manually?\nIt is recommended to use the generate button.", "Address ID Alert", MessageBoxButton.YesNoCancel, MessageBoxImage.Exclamation);
            //while (Result != MessageBoxResult.None) ;
            if (Result != MessageBoxResult.Yes)
                return;
            tbRCId.IsEnabled = true;
            btnUpdateINF.IsEnabled = true;
            btnUpdateRC.IsEnabled = true;
            tbRCAddress.IsEnabled = true;

        }
        private void OnCalculateRemoteAddressID(object sender, RoutedEventArgs e)
        {
            

            GenerateRCAddressIDForm = (GenerateRCAddressID)Application.LoadComponent(new Uri("GenerateRCAddressID.xaml", UriKind.Relative)); ;
            GenerateRCAddressIDForm.Visibility = System.Windows.Visibility.Visible;
            GenerateRCAddressIDForm.Init(SN_ComBox.SelectedIndex, deviceData.Parameters.Sn2.ToString(), deviceData.Parameters.Sn3.ToString());
            //GenerateRCAddressIDForm.Show();
            GenerateRCAddressIDForm.ShowDialog();

            if (!GenerateRCAddressIDForm.IsDone)
                return;
            btnUpdateINF.IsEnabled = true;
            btnUpdateRC.IsEnabled = true;
            deviceData.Parameters.RCAddress = GenerateRCAddressIDForm.Address;// (short)((deviceData.Parameters.Sn3 - deviceData.Parameters.Sn3 / 1000 * 1000) % 255);
            deviceData.Parameters.RCId = GenerateRCAddressIDForm.ID; //(short)(deviceData.Parameters.Sn3 + 10000 * deviceData.Parameters.SysType);
           
        }
        private GenerateRCAddressID GenerateRCAddressIDForm = new GenerateRCAddressID();  
        private void OnUpdateINFAddressID(object sender, RoutedEventArgs e)
        {
            //btnSetRCId.IsEnabled = false;
            tbRCId.IsEnabled = false;
            btnUpdateINF.IsEnabled = false;
            tbRCAddress.IsEnabled = false;
            if ((deviceData.Parameters.RCAddress <= 0) || (deviceData.Parameters.RCId <= 0) || (deviceData.Parameters.RCAddress > 255) || (deviceData.Parameters.RCId >65535))
            {
                MessageBox.Show("RC address or ID out of range\nPlease input address from 1 to 255, ID from 1 to 32767.", "INF Address and ID", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                
                return;
            }

            try
            {

                deviceData.SetINFAddress();
                try
                {
                    deviceData.SetINFId();
                }
                catch (DeviceOperationException exception)
                {
                    //MessageBox.Show(this, "Device Operation failed: " + exception.Message);
                    Logger.Error("Device Operation failed: ", exception.Message);

                }
                
            }
            catch (DeviceOperationException exception)
            {
               // MessageBox.Show(this, "Device Operation failed: " + exception.Message);
                Logger.Error("Device Operation failed: " , exception.Message);
            }
            //Thread.Sleep(1200);
            Delay_ms(1200); // replace Thread.Sleep
            MessageBox.Show("INF address and ID were updated successfully!\nPlease reset the system (by turning it off and on).", "INF Address and ID", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            
        }
        private void OnUpdateRCAddressID(object sender, RoutedEventArgs e)
        {
            //btnSetRCId.IsEnabled = false;
            tbRCId.IsEnabled = false;
            btnUpdateRC.IsEnabled = false;
            tbRCAddress.IsEnabled = false;

            if ((deviceData.Parameters.RCAddress <= 0) || (deviceData.Parameters.RCId <= 0) || (deviceData.Parameters.RCAddress > 255) || (deviceData.Parameters.RCId > ushort.MaxValue))
            {
                MessageBox.Show(this, "RC address or ID out of range\nPlease input address from 1 to 255, ID from 1 to 32767.");
                return;
            }
            try
            {
                             
                MessageBoxResult result = MessageBox.Show(this, "Do you want to change RC address and ID?", "", MessageBoxButton.OKCancel);
                

                
                if (MessageBoxResult.OK == result)
                {
                    MessageBox.Show(this, "Please turn on RC and select hand icon in technician mode");
                    tbRCStartTest.Text = "Updating RC address and ID...";
                    RCUpdateTimer.Start();
                    deviceData.SetRCAddressID();
                }
                else
                {
                    btnUpdateINF.IsEnabled = false;
                    return;
                }
               
            }
            catch (DeviceOperationException exception)
            {
               // MessageBox.Show(this, "Device Operation failed: " + exception.Message);
                Logger.Error("Device Operation failed: ", exception.Message);
                tbRCStartTest.Text = "";
                btnUpdateINF.IsEnabled = false;
            }

        }
        void OnUpdateRCAddressIDTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (deviceData.RequestRCAddressIDStatus())
                {
                    RCUpdateTimer.Stop();
                    tbRCStartTest.Text = "";
                    string message = "RC address and ID were updated successfully!\nPlease reset the system (by turning it off and on).";
                    MessageBoxResult result = MessageBox.Show(this, message);
                    
                }
            }
            catch (DeviceOperationException exception)
            {
                tbRCStartTest.Text = "";
                RCUpdateTimer.Stop();
               // MessageBox.Show(this, "Update RC address and ID Operation failed: " + exception.Message);
                Logger.Error("Update RC address and ID Operation failed: ", exception.Message);
                

            }
        }
        private void OnDisconenctUSB()
        {
            tbConnected.Text = "Disconnected";
            tbConnected.Background = Brushes.Red;
            Read.IsEnabled = false;
            Write.IsEnabled = false;
            Load.IsEnabled = false;
            Restore.IsEnabled = false;
            Save.IsEnabled = false;
            gbWalk.IsEnabled = false;
            gbTilt.IsEnabled = false;
            gbSafety.IsEnabled = false;
            gbBeginner.IsEnabled = false;
            gbStairs.IsEnabled = false;
            gbRC.IsEnabled = false;
            gbCalibration.IsEnabled = false;
            gbSerialNumbers.IsEnabled = false;
            gbSysConfig.IsEnabled = false;
            collectOnline.IsEnabled = false;
            collectOnline.IsOn = false;
            gbLogSettings.IsEnabled = false;
            //btnExcelBrowse.IsEnabled = false;
            //btnExcelLoad.IsEnabled = false;
            btnBrowseLog.IsEnabled = false;
            btnSaveSendLog.IsEnabled = false;
            //btnLoadLog.IsEnabled = false;
            gbLogData.IsEnabled = false;
            gbCustomCommands.IsEnabled = false;
            btnCom1.IsEnabled = false;
            btnCom2.IsEnabled = false;
            btnCom3.IsEnabled = false;
            btnCom4.IsEnabled = false;
            btnCom5.IsEnabled = false;
            btnCom6.IsEnabled = false;
            btnCom7.IsEnabled = false;
            //pbSysConfig.Password = "";
            pbSysConfig.IsEnabled = false;
            btnBootBrowse.IsEnabled = false;
            btnBootDownload.IsEnabled = false;
            btnDSPBrowse.IsEnabled = false;
            btnDSPDownload.IsEnabled = false;
            btnMCUBrowse.IsEnabled = false;
            btnMCUDownload.IsEnabled = false;
            groupflashlog.IsEnabled = false;
            gbErrors.IsEnabled = false;
            LogFileSent = false;
            USBConnectState = false;
            BtnRunEncoderTest.IsEnabled = false;
            Expander.IsEnabled = false; Expander.IsExpanded = false;
            USBConnectDelay = 0;
            USBDisConnectDelay = 0;
            App temp = App.Current as App;
            if (temp != null)
                //(App.Current as App).DeptName_Connected = false;
                temp.DeptName_Connected = false;
           
           
            
        }

        void OnCheckUSBConnectedTimeTimer_Tick(object sender, EventArgs e) // parasoft-suppress  OOM.CYCLO "Coding style - method logic was verified"
        {

     

               if ((App.Current as App).DeptName_Connected == false && (App.Current as App).DeptName_RunINFtoApplication == true)
               {
                ReadDeviceSettings(false);
                Delay_ms(1000);
                return;
               }

            try
            {
                USBConnected = deviceData.CheckUSBConnected();
                if (!USBConnected)
                {
                    USBDisConnectDelay++;
                }
            }
            catch (DeviceOperationException exception)
            {
                USBConnected = false;
                Logger.Dummy_Error("", exception.Message);
            }
            if (true == USBConnectState)
            {
                if (USBDisConnectDelay >= 2)
                    OnDisconenctUSB();
            }
            else
            {
                if (USBConnected)
                {
                    USBConnectDelay++;
                    if (USBConnectDelay >= 5)
                    {
                        bool EnableReadOnOpen = false;
                        XmlDocument doc = new XmlDocument();
                        doc.Load("DeviceSettingsValues.xml");
                        try
                        {
                            XmlNodeList ReadOnOpenNode = doc.SelectNodes("//ReadOnOpen");
                            if (ReadOnOpenNode.Count == 1)
                                foreach (XmlNode value in ReadOnOpenNode)
                                { EnableReadOnOpen = value.Attributes["Values"].Value == "true" || value.Attributes["Values"].Value == "True"; }
                        }
                        catch (Exception exception) { Logger.Dummy_Error("", exception.Message); }
                        if (EnableReadOnOpen)
                            ReadDeviceSettings(false);
                        LogGUIConnections(); tbConnected.Text = "Connected";
                        tbConnected.Background = Brushes.Green; USBDisConnectDelay = 0; USBConnectDelay = 0;
                        ControlsEnabled();

                       
                       

                    }
                }
            }
        }
        private void ControlsEnabled()
        {
                        Read.IsEnabled = true;
                        collectOnline.IsEnabled = true;
                        gbCustomCommands.IsEnabled = true;
                        btnCom1.IsEnabled = true;
                        btnCom2.IsEnabled = true;
                        btnCom3.IsEnabled = true;
                        btnCom4.IsEnabled = true;
                        btnCom5.IsEnabled = true;
                        btnCom6.IsEnabled = true;
                        btnCom7.IsEnabled = true;
                        btnDSPBrowse.IsEnabled = true;
                        btnDSPDownload.IsEnabled = true;
                        btnMCUBrowse.IsEnabled = true;
                        btnMCUDownload.IsEnabled = true;                          
                        USBConnectState = true;
                        BtnRunEncoderTest.IsEnabled = true;
           
        } 
        private void OnStartRFTest(object sender, RoutedEventArgs e)
        {
            if (!RFTestStarted)
            {
                try
                {
                    
                    //btnStartRFTest.IsEnabled = false;
                    RFTestStarted = true;
                    btnStartRCTest.Content = "Stop";
                    tbRCStartTest.Text = "Please Turn on RC !";
                    MessageBoxResult result = MessageBox.Show(this,"Please Turn on RC !", "RC Test");
                   
                    deviceData.StartRFTest();
                    RFtestTimer.Start();
                    deviceData.ResetRFTest();
                    //tbRCStartTest.Visibility = "Visible";

                }
                catch (DeviceOperationException exception)
                {
                    tbRCStartTest.Text = "";
                    btnStartRCTest.Content = "Start RC Test";
                  //  MessageBox.Show(this, "Start RF test Operation failed: " + exception.Message);
                    Logger.Error("Start RF test Operation failed: " , exception.Message);
                }
            }
            else
            {
                RFtestTimer.Stop();
                btnStartRCTest.Content = "Start RC Test";
                tbRCStartTest.Text = "";
                RFTestStarted = false;
                deviceData.ResetRFTest();
            }
        }   		
		void OnRFTestTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (deviceData.RequestRFTestStatus())
                {
                    short RCerrRate = deviceData.GetRCErrRate();
                    short INFerrRate = deviceData.GetINFErrRate();
                    RFtestTimer.Stop();
                    btnStartRCTest.Content = "Start RC Test";
                    RFTestStarted = false;
                    tbRCStartTest.Text = "";
                    string message = "RC Test Finished !\n\nResults:\nRC error rate: " +RCerrRate.ToString()+"%\nINF error rate: " +INFerrRate.ToString()+"%";
                    MessageBoxResult result = MessageBox.Show(this, message, "RC Test Results");
                    deviceData.ResetRFTest();
                }
            }
            catch (DeviceOperationException exception)
            {
                //MessageBox.Show(this, "Get RF Test Status Operation failed: " + exception.Message);
                Logger.Error("Get RF Test Status Operation failed: ", exception.Message);
                RFtestTimer.Stop();
                
            }
        }

        /*private void OnEditRemoteId(object sender, RoutedEventArgs e)
        {
            btnSetRCId.IsEnabled = true;
            tbRCId.IsEnabled = true;
        }

        private void OnSetRemoteId(object sender, RoutedEventArgs e)
        {
            btnSetRCId.IsEnabled = false;
            tbRCId.IsEnabled = false;

            try
            {
                deviceData.SetRemoteId();
            }
            catch (DeviceOperationException exception)
            {
                MessageBox.Show(this, "Device Operation failed: " + exception.Message);
            }
        }*/

       

        void OnCollectDataTimer(object sender, EventArgs e)
        {
            try
            {
                deviceData.ReadOnlineData();
                tbOnlineError.Visibility = Visibility.Hidden;
            }
            catch (DeviceOperationException exception)
            {
                tbOnlineError.Text = "Read online data failed: ";// +exception.Message;
                tbOnlineError.Visibility = Visibility.Visible;
                Logger.Dummy_Error("", exception.Message);
            }
        }
        private void OnRunCustomCommand(FrameworkElement sender, byte[] data)
        {
           // int command = int.Parse(sender.Tag.ToString());
            int command ;
            bool cc = int.TryParse(sender.Tag.ToString(), out  command );
            if (command == 5 && data [0] == 0x99 && data [1] == 0x99 && data [2] == 0x99)
            {
                //Report_Progress progressform = new Report_Progress();
                object s = Application.LoadComponent(new Uri("Report_Progress.xaml", UriKind.Relative));

                progressform = (Report_Progress)s; progressform.Visibility = System.Windows.Visibility.Visible; progressform.Show();               
                progressform.rtfUserReport.AppendText("System error occured\n"); progressform.rtfUserReport.AppendText("Saving log files......");
            
                SendEmailReport(00);
            }

            try
            {
                if (command == 5 && data[0] == (byte)0x39) //BR window 
                {
                    data[0] = 0x30;
                    byte[] result_LH = deviceData.RunCustomCommand(1, data);  btnCom1_LH.SetResult(result_LH);
                   

                    data[0] = 0x31;
                    byte[] result_LK = deviceData.RunCustomCommand(2, data); btnCom2_LK.SetResult(result_LK);
                   

                    data[0] = 0x32;
                    byte[] result_RH = deviceData.RunCustomCommand(3, data); btnCom3_RH.SetResult(result_RH);
                   

                    data[0] = 0x33;
                    byte[] result_RK = deviceData.RunCustomCommand(4, data); btnCom4_RK.SetResult(result_RK);

                    
                }
                else
                {
                    byte[] result = deviceData.RunCustomCommand(command, data);
                   // (sender as CommandCtrl).SetResult(result);
                    CommandCtrl aa = sender as CommandCtrl;
                    if (aa != null) aa.SetResult(result);
                   
                }
            }
            catch (DeviceOperationException exception)
            { Logger.Error("Custom Command Operation failed: ", exception.Message);
         //   deviceData.Disconnect();
              //      OnDisconenctUSB();
                //    CheckUSBConnectedTime.IsEnabled = false;
                //    Restore.IsEnabled = true;
            }
            
        }
     
        private void OnParameterlessCustomCommandRun(object sender, RoutedEventArgs e)
        {
           
          //  int command = int.Parse((sender as FrameworkElement).Tag.ToString());
            

            try
            {
                int command;
                bool cc;
           
                FrameworkElement aa = sender as FrameworkElement;
                if (aa != null)
                {
                    cc = int.TryParse(aa.Tag.ToString(), out command);
                    deviceData.RunCustomCommand(command, null);
                    if (command == 6)//command 6 , INF exit_usb=0 and  command 7 setWDG to do reset
                    {
                        deviceData.Disconnect();
                        OnDisconenctUSB();
                        RunINFtoApplication = true;
                        
                    }
                    else if (command == 7)
                    {
                       // RunINFtoApplication = true;
                      /*  USBConnected = false;
                        USBConnectState = false;
                        deviceData.Disconnect();
                        OnDisconenctUSB();*/
                        
                       
                }
                
               
            }
                
               
            }
            catch (DeviceOperationException exception)
            {
               // MessageBox.Show(this, "Custom Command Operation failed: " + exception.Message);
                Logger.Error("Custom Command Operation failed: ", exception.Message);
            }
        }
        public void RunSystem()
       {
           deviceData.Disconnect();
           OnDisconenctUSB();
           RunINFtoApplication = true;
       }

        private bool RunINFtoApplication = false;
        
        private  int Joint_Being_Downloaded = 0x330;

        void OnTestTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                testTimer.Stop(); 
            
                deviceData.RequestTestStatus();
                
                    switch (Joint_Being_Downloaded)
                    {
                        case 0x330:

                         LH_Download();
                                   
                       break;
                       case 0x331:

                         LK_Download();
       
                      break;
                      case 0x332:

                        RH_Download();

                      break;

                      
                     case 0x333:

                      RK_Download(); 
  
                     break;
                       
                     default:
                      break;
                           
                           
                    }

                   
                  
                
            }
            catch (DeviceOperationException exception)
            {
                //MessageBox.Show(this, "Get Test Status Operation failed: " + exception.Message);
                Logger.Error("Get Test Status Operation failed: ", exception.Message);   testTimer.Stop();                         
            }
        }

        private void OnStartCalibration(object sender, RoutedEventArgs e)
        {
            try
            {
                deviceData.StartCalibration();               
                btnStartCalib.IsEnabled = false;
               // btnDoCalib.IsEnabled = true;
               // cbCalibLH.IsEnabled = cbCalibLK.IsEnabled = cbCalibRH.IsEnabled = cbCalibRK.IsEnabled = true;
                calibTimer.Start();
            }
            catch (DeviceOperationException exception)
            {
               // MessageBox.Show(this, "Start Calibration Operation failed: " + exception.Message);
                Logger.Error("Start Calibration Operation failed: " , exception.Message);
            }
        }
       
        private int cal_timeout_cnt = 0;
        void OnCalibrationTimer_Tick(object sender, EventArgs e) // parasoft-suppress  OOM.CYCLO "Coding style - method logic was verified"
        {
            cal_timeout_cnt++;
            TestStatus CalibStatus;
            try
            {
                CalibStatus = deviceData.RequestCalibrationStatus((bool)cbCalibLH.IsChecked, (bool)cbCalibLK.IsChecked, (bool)cbCalibRH.IsChecked, (bool)cbCalibRK.IsChecked);
                //if  ==     deviceData.RequestCalibrationStatus( )
                if( TestStatus.Success == CalibStatus)
                {
                    calibTimer.Stop();

                    if (cbCalibLH.IsChecked == true)
                    { cbCalibLH.IsEnabled = true; cbCalibLH.IsChecked = false; }
                   

                    if (cbCalibRH.IsChecked == true)
                    {   cbCalibRH.IsEnabled = true; cbCalibRH.IsChecked = false; }

                    if (cbCalibLK.IsChecked == true)
                    {    cbCalibLK.IsEnabled = true; cbCalibLK.IsChecked = false; }


                    if (cbCalibRK.IsChecked == true)
                    {   cbCalibRK.IsEnabled = true; cbCalibRK.IsChecked = false; }

                    Delay_ms(2000); 
                    MessageBox.Show(this, "Calibration Finished.\nPlease restart the system.\n", "Calibration", MessageBoxButton.OK, MessageBoxImage.Information);
                
  
                }
                else if (TestStatus.Fail == CalibStatus)
                {
                    calibTimer.Stop();
                    MessageBox.Show(this, "Calibration Failed!\nPlease restart and recalibrate the system.\n", "Calibration", MessageBoxButton.OK, MessageBoxImage.Error);
                }

           
            }
            catch (DeviceOperationException exception)
            {
              //  MessageBox.Show(this, "Get Calibration Status Operation failed: " + exception.Message);
                Logger.Error("Get Calibration Status Operation failed: " , exception.Message);
                calibTimer.Stop();
                btnStartCalib.IsEnabled = true;
                cbCalibLH.IsEnabled = cbCalibLK.IsEnabled = cbCalibRH.IsEnabled = cbCalibRK.IsEnabled = false;
            }
        }
        //Encoder Test Check Status ; 
        bool EncoderTestInProcess;
        int JointCounter;
        bool LHJointUnderTest;
        bool RHJointUnderTest;
        bool LKJointUnderTest;
        bool RKJointUnderTest;

        void OnTestEncoderTimer_Tick(object sender, EventArgs e) // parasoft-suppress  OOM.CYCLO "Coding style - method logic was verified"
        {
            try
            {
                byte[] result = { }; int command = 1;  
                
                if (cbCalibLH.IsChecked == true && EncoderTestInProcess)//
                {
                    byte[] DataArray2 = { 0x30, 0x3, 0x30, 0, 0, 1, 0, 2, 0, 0 }; result = deviceData.RunCustomCommand(command, DataArray2); EncoderTestInProcess = false; LHJointUnderTest = true;                                        
                }

                if (cbCalibLK.IsChecked == true && EncoderTestInProcess) // 
                {
                   byte[] DataArray2 = { 0x31, 0x3, 0x30, 0, 0, 1, 0, 2, 0, 0 }; result = deviceData.RunCustomCommand(command, DataArray2); EncoderTestInProcess = false; LKJointUnderTest = true;                                        
                }

               if (cbCalibRH.IsChecked == true && EncoderTestInProcess)//
               {
                  byte[] DataArray3 = { 0x32, 0x3, 0x30, 0, 0, 1, 0, 2, 0, 0 }; result = deviceData.RunCustomCommand(command, DataArray3); EncoderTestInProcess = false; RHJointUnderTest = true;  
               }

              if (cbCalibRK.IsChecked == true && EncoderTestInProcess)//
               {
                 byte[] DataArray4 = { 0x33, 0x3, 0x30, 0, 0, 1, 0, 2, 0, 0 }; result = deviceData.RunCustomCommand(command, DataArray4); EncoderTestInProcess = false; RKJointUnderTest = true;                                            
               }

           
                 //check status 
              if (deviceData.RequestTestEncoderStatus())
              {
                  HipsCheck();
                  KneesCheck();
              }

              IfJointCounterEqualZero();
            }
                

            catch (DeviceOperationException exception)
            {
                Logger.Error("Encoder Test Status Operation failed: ", exception.Message);
                BtnRunEncoderTest.IsEnabled = true; TestEncoderTimer.Stop(); cbCalibLH.IsEnabled = cbCalibLK.IsEnabled = cbCalibRH.IsEnabled = cbCalibRK.IsEnabled = false;                
               
            }

        }

        public void HipsCheck()
        {
            if (cbCalibLH.IsChecked == true && LHJointUnderTest)
            {
                cbCalibLH.IsEnabled = true; cbCalibLH.IsChecked = false; EncoderTestInProcess = true; LHJointUnderTest = false; JointCounter--;
            }

            if (cbCalibRH.IsChecked == true && RHJointUnderTest)//
            {
                cbCalibRH.IsEnabled = true; cbCalibRH.IsChecked = false; EncoderTestInProcess = true; RHJointUnderTest = false; JointCounter--;
            }
        }
        public void KneesCheck()
        {
            if (cbCalibLK.IsChecked == true && LKJointUnderTest)//
            {
                cbCalibLK.IsEnabled = true; cbCalibLK.IsChecked = false; EncoderTestInProcess = true; LKJointUnderTest = false; JointCounter--;
            }

            if (cbCalibRK.IsChecked == true && RKJointUnderTest)//
            {
                cbCalibRK.IsEnabled = true; cbCalibRK.IsChecked = false; EncoderTestInProcess = true; RKJointUnderTest = false; JointCounter--;
            }    
        }
        DateTime CurrentTime;
        public void IfJointCounterEqualZero()
        {
            if (JointCounter == 0)
            {
                TestEncoderTimer.Stop();
                CurrentTime = DateTime.Now;
                string EncoderPlotPath = EncoderTestPath +
                "\\Encoder_Plot_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
                deviceData.SaveLogFile(EncoderPlotPath);
                string EncoderPlotPathText = EncoderPlotPath.TrimEnd("dat".ToCharArray());
                EncoderPlotPathText = EncoderPlotPathText + "txt";

                LogParser.ParseFile(EncoderPlotPath, false, EncoderPlotPathText);
                bool TimeStampParserFlag = LogParser.TimeStampFlagIncluded();
                //string LogFileName = tbBrowseViewPlot.Text;
                string TimeStampVariable;
                string Path_plot_encoder_readings = Directory.GetCurrentDirectory();
                string plot_encoder_readings = @Path_plot_encoder_readings + "\\EncoderPlot_py_ver8\\plot_encoder_readings_REV08.exe";
                // string plot_encoder_readings = @"C:\\plot_encoder_readings_REV03.py";

                //string plot_encoder_readings = @Path_plot_encoder_readings + "\\plot_encoder_readings_REV03.py";
                //string LogFileNameCheck = Path.GetFileNameWithoutExtension(LogFileName);
                //string LogFileNameExtention = Path.GetExtension(LogFileNameCheck);


                if (!TimeStampParserFlag)
                {
                    TimeStampVariable = "3";
                }
                else
                {
                    TimeStampVariable = "4";
                }

                Process.Start(plot_encoder_readings, "\"" + EncoderPlotPathText + "\"" + " " + TimeStampVariable);
                BtnRunEncoderTest.IsEnabled = true;
               
            }
        }
        private void OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            string xword;//
            PasswordBox aa = (sender as PasswordBox);
            if (aa != null)
            {
                xword = aa.Password;
                advancedGrid.Visibility = (xword == GetSysPass()) ? Visibility.Visible : Visibility.Hidden;
                DiagnosticsGrid.Visibility = toolbar.Visibility = technicianGrid.Visibility = debugGrid.Visibility = advancedGrid.Visibility;

                //Sync password for two panes
                if (pbTechnician.Password != xword)
                    pbTechnician.Password = xword;
                if (pbAdvanced.Password != xword)
                    pbAdvanced.Password = xword;
                if (pbDebug.Password != xword)
                    pbDebug.Password = xword;
               /* if (pbAFT.Password != xword)
                    pbAFT.Password = xword;*/
            }

        }
        private string GetSysPass()
        {
            string st;
            st = "argoadmin";
            return st;
        }
        private  string GetSysConfigPass()
        {
            string st;
            st = "kingradi";
            return st;
        }
        private void OnSysConfigPasswordChanged(object sender, RoutedEventArgs e)
        {
            string xxssword;  //= (sender as PasswordBox).Password;
            PasswordBox aa = sender as PasswordBox;
            if (aa != null)
            {
                xxssword = aa.Password;
                cbSysType.IsEnabled = (xxssword == GetSysConfigPass()) ? true : false;
                cbEnableStairs.IsEnabled = cbSysType.IsEnabled;
                SerialNumbers_Expander.IsEnabled = cbSysType.IsEnabled;
                cbSwitchingTime.IsEnabled = cbSysType.IsEnabled;
                if ((short)deviceData.Parameters.DSP != 0x0909)
                    btnResetStepCounter.IsEnabled = cbSysType.IsEnabled;
                    btnResetStairCounter.IsEnabled = cbSysType.IsEnabled;

                //Sync password for two panes
                if (pbSysConfig.Password != xxssword)
                    pbSysConfig.Password = xxssword;
            }
           
        }

        private void OnEditSerialNumbers(object sender, RoutedEventArgs e)
        {
            gridSerialNumbers.IsEnabled = true;
            btnSetSN.IsEnabled = true;
            SN_ComBox.IsEnabled = false;
        }

        private void OnSetSerialNumbers(object sender, RoutedEventArgs e)
        {
            btnSetSN.IsEnabled = false;
            gridSerialNumbers.IsEnabled = false;

            try
            {
                deviceData.SetSerialNumbers();
            }
            catch (DeviceOperationException exception)
            {
               // MessageBox.Show(this, "Write Serial Numbers Operation failed: " + exception.Message);
                Logger.Error("Write Serial Numbers Operation failed: " , exception.Message);
            }
        }

        /*private void OnBrowseSaveExcelFile(object sender, RoutedEventArgs e)
        {
            SaveFileDialog ofd = new SaveFileDialog();
            ofd.CheckFileExists = false;
            ofd.RestoreDirectory = true;
            ofd.Filter = "CSV files (*.csv)|*.csv|(*.*) |*.*";
            if (ofd.ShowDialog().Value == true)
            {
                tbSaveExcelFileLocation.Text = ofd.FileName;
            }
            try
            {
                //Just to make sure that file might be created
                using (FileStream fs = new FileStream(tbSaveExcelFileLocation.Text, FileMode.Create))
                {
                    fs.Close();
                }

                deviceData.SaveExcelData(tbSaveExcelFileLocation.Text);
            }
            catch (Exception exception)
            {
                MessageBox.Show(this, "Save to Excel failed: " + exception.Message);
            }
        }

        private void OnSaveExcelFile(object sender, RoutedEventArgs e)
        {
            try
            {
                //Just to make sure that file might be created
                using (FileStream fs = new FileStream(tbSaveExcelFileLocation.Text, FileMode.Create))
                {
                    fs.Close();
                }

                deviceData.SaveExcelData(tbSaveExcelFileLocation.Text);
            }
            catch (Exception exception)
            {
                MessageBox.Show(this, "Save to Excel failed: " + exception.Message);
            }
        }*/

        private void OnBrowseSaveLogFile(object sender, RoutedEventArgs e)
        {
            SaveFileDialog ofd = new SaveFileDialog();
            ofd.CheckFileExists = false;
            ofd.RestoreDirectory = true;
            ofd.Filter = "DAT files (*.dat)|*.dat|(*.*) |*.*";
            
            if (ofd.ShowDialog().Value == true)
            {
                tbSaveLogFileLocation.Text = ofd.FileName;
            
                try
                {
                    //Just to make sure that file might be created
                    using (FileStream fs = new FileStream(tbSaveLogFileLocation.Text, FileMode.Create))
                    {
                        fs.Close();
                    }
                    deviceData.SaveLogFile(tbSaveLogFileLocation.Text);
                    try
                    {
                        string LogFileLocationText = tbSaveLogFileLocation.Text.TrimEnd("dat".ToCharArray());
                        LogFileLocationText = LogFileLocationText + "txt";

                        LogParser.ParseFile(tbSaveLogFileLocation.Text, false, LogFileLocationText);
                        // MessageBox.Show("Log File Download Succeeded !", "Save Log File", MessageBoxButton.OK, MessageBoxImage.Information);
                        MessageBoxResult Result = MessageBox.Show("Log File Download Complete.\nOpen?", "Save Log File", MessageBoxButton.YesNo);
                        if (Result == MessageBoxResult.Yes)
                        {
                            Process.Start("notepad", LogFileLocationText);
                        }
                    }
                    catch (DeviceOperationException exception)
                    {
                        MessageBox.Show("Log Parser Failed", "Save Flash Log File", MessageBoxButton.OK, MessageBoxImage.Error);
                        Logger.Dummy_Error("", exception.Message);
                    }

                }
                catch (Exception exception)
                {
                   // MessageBox.Show("Save Log File failed: " + exception.Message, "Save Flash Log File", MessageBoxButton.OK, MessageBoxImage.Error);
                    Logger.Error_MessageBox_Advanced("Save Log File failed: ", "Save Flash Log File", exception.Message);
                }
            }
        }

        private void OnSaveLogFile(object sender, RoutedEventArgs e)
        {
            try
            {
                //Just to make sure that file might be created
                using (FileStream fs = new FileStream(tbSaveLogFileLocation.Text, FileMode.Create))
                {
                    fs.Close();
                }

                deviceData.SaveLogFile(tbSaveLogFileLocation.Text);
                MessageBox.Show("Log File Download Succeeded !", "Save Log File", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
              //  MessageBox.Show("Save Log File failed: " + exception.Message, "Save Flash Log File", MessageBoxButton.OK, MessageBoxImage.Error);
                Logger.Error_MessageBox_Advanced("Save Log File failed: ", "Save Flash Log File", exception.Message);
            }
        }



        private void OnBrowseSaveFlashLogFile(object sender, RoutedEventArgs e)
        {
            SaveFileDialog ofd = new SaveFileDialog();
            ofd.CheckFileExists = false;
            ofd.RestoreDirectory = true;
            ofd.Filter = "DAT files (*.dat)|*.dat|(*.*) |*.*";

            if (ofd.ShowDialog().Value == true)
            {

                tbSaveFlashLogFileLocation.Text = ofd.FileName;
            
                try
                {
                    //Just to make sure that file might be created
                    using (FileStream fs = new FileStream(tbSaveFlashLogFileLocation.Text, FileMode.Create))
                    {
                        fs.Close();
                    }

                    deviceData.SaveFlashLogFile(tbSaveFlashLogFileLocation.Text);
                    try
                    {
                        string LogFileLocationText = tbSaveFlashLogFileLocation.Text.TrimEnd("dat".ToCharArray());
                        LogFileLocationText = LogFileLocationText + "txt";
                        //deviceData.SaveLogFile(tbSaveLogFileLocation.Text);
                        LogParser.ParseFile(tbSaveFlashLogFileLocation.Text, false, LogFileLocationText);
                        // MessageBox.Show("Log File Download Succeeded !", "Save Log File", MessageBoxButton.OK, MessageBoxImage.Information);
                        MessageBoxResult Result = MessageBox.Show("Flash Error Log File Download Complete.\nOpen?", "Save Log File", MessageBoxButton.YesNo);
                        if (Result == MessageBoxResult.Yes)
                        {
                            Process.Start("notepad", LogFileLocationText);
                        }

                        //MessageBox.Show("Flash Log File Download Succeeded !", "Save Log File", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    catch(Exception exception) 
                    {
                        MessageBox.Show("Log Parser Failed !", "Save Flash Log File", MessageBoxButton.OK, MessageBoxImage.Error);
                        Logger.Dummy_Error("", exception.Message);
                    }

                }
                catch (Exception exception)
                {
                   // MessageBox.Show("Save Flash Log File failed: " + exception.Message, "Save Flash Log File", MessageBoxButton.OK, MessageBoxImage.Error);
                    Logger.Error_MessageBox_Advanced("Save Flash Log File failed: ", "Save Flash Log File", exception.Message);
                
                }
            }
        }
/*
        private void OnSaveLogFlashFile(object sender, RoutedEventArgs e)
        {
            try
            {
                //Just to make sure that file might be created
                using (FileStream fs = new FileStream(tbSaveFlashLogFileLocation.Text, FileMode.Create))
                {
                    fs.Close();
                }

                deviceData.SaveFlashLogFile(tbSaveFlashLogFileLocation.Text);
            }
            catch (Exception exception)
            {
               // MessageBox.Show(this, "Save Flash Log File failed: " + exception.Message);
                Logger.Error("Save Flash Log File failed: " , exception.Message);
            }
        }
*/

       private void OnShowHelp(object sender, RoutedEventArgs e)
        {


            //string AppDir = Directory.GetCurrentDirectory();
            string HelpFileName = InstallingAppDir + "/Help.chm";
            try
            {
                System.Diagnostics.Process.Start(@HelpFileName);
              
            }
            catch (Exception exception)
            {
                //MessageBox.Show(this, "Help Document: " + exception.Message,"Help",MessageBoxButton.OK,MessageBoxImage.Error );
                Logger.Error_MessageBox_Advanced("Help Document: ", "Help", exception.Message);
            }
            
            
          /*  Process help = new Process();
            ProcessStartInfo psi = new ProcessStartInfo("Help.chm");//("UserManual.pdf");("Online_Help.chm")
            psi.Verb = "open";
            help.StartInfo = psi;
            help.Start();*/

        }

        private void OnTabSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           // int selected = (sender as TabControl).SelectedIndex;
            int selected;
            TabControl aa = sender as TabControl;
            if (aa != null)
            {
                selected = aa.SelectedIndex;
                toolbar.Visibility = (selected > 0 && advancedGrid.Visibility != Visibility.Visible) ? Visibility.Hidden : Visibility.Visible;
            }
        }

        private Parser LogParser = new Parser(); 
        private XmlDataProvider deviceSettingsValuesDataProvider;
        public Data deviceData = new Data();
        private DispatcherTimer onlineTimer = new DispatcherTimer();
        private DispatcherTimer testTimer = new DispatcherTimer();
        private DispatcherTimer calibTimer = new DispatcherTimer();

        private DispatcherTimer TestEncoderTimer = new DispatcherTimer();

     

        private bool USBConnected = false;
        private bool USBConnectState = false;
        private int USBConnectDelay = 0;
        private int USBDisConnectDelay = 0;
        bool RFTestStarted = false;
		private DispatcherTimer RFtestTimer = new DispatcherTimer();
        private DispatcherTimer RCUpdateTimer = new DispatcherTimer();
        private DispatcherTimer CheckUSBConnectedTime = new DispatcherTimer();
        string[] array1 = new string[31];
        string[] array2 = new string[31];
        int[] array2_int = new int[31];
      //  string[] StepTimeArr = new string[31];
        private const int KNEE_MAX_SPEED = 80; // in degrees/sec
        private const int MAX_STAIRS_THRESHOLD = 19;
 

        
        private void HipFlexion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int i;
            array1.CopyTo(array2, 0);
            for (i = 0; i < Knee_Flexion.Items.Count; i++)
            {
                array2_int[i] = Convert.ToInt32(Hip_Flexion.SelectedItem) + 8 + i - 1;
            }
            for (i = 0; i < Knee_Flexion.Items.Count; i++)
            {
                array2[i] = Convert.ToString(array2_int[i]);
            }
            Knee_Flexion.ItemsSource = array2;
            Knee_Flexion.Items.Refresh();
            if ((Convert.ToInt32(Hip_Flexion.SelectedItem) + 8) > Convert.ToInt32(Knee_Flexion.SelectedItem))
                Knee_Flexion.SelectedIndex = 0;

        }
  
        private void Check_StairsThreshold()
        {
            
            if (Convert.ToInt32(StairCurrentThreshold.Text) > MAX_STAIRS_THRESHOLD)
            {
                MessageBox.Show("Stand current threshold exceeds maximum value, please change it", "Stand Current", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
        /* not used */
       /* private void KneeFlexion_SelectionChanged( object sender, SelectionChangedEventArgs e)
        {
            int i;
            
            int MinStepTime;
            MinStepTime = 1000* Convert.ToInt32(Knee_Flexion.SelectedItem) / (KNEE_MAX_SPEED / 2);
            
            for (i = 0; i < Step_Time.Items.Count; i++)
            {
                StepTimeArr[i] = Convert.ToString(MinStepTime + i * 100);
            }

            Step_Time.ItemsSource = StepTimeArr;
            Step_Time.Items.Refresh();
            Step_Time.SelectedIndex = 0;
            if (Convert.ToInt32(Step_Time.Text) != 0)
            {
                MessageBox.Show("Step time reset, please pick new value ", "Step Time", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }

        }*/

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Hip_Flexion.SelectedIndex = -1;
            tbDspFileLocation.IsEnabled = false;
            tbRcFileLocation.IsEnabled = false;
            Write.IsEnabled = false;
            //SampleTime.IsEnabled = false;
            if (safetyonoff.IsOn == false)
            {
                //X_Thr.IsEnabled = false;
                Y_Thr.IsEnabled = false;
            }
            if (Trainning.IsOn == false)
            {
                hip_final.IsEnabled = false;
                hip_max.IsEnabled = false;
                t_steptime.IsEnabled = false;
            }
        }


        private void OnEnableDisableFirstStep(object sender, EventArgs e)
        {
            if(FirstStep.IsOn )
                cbFirstStepFlexion.Text = "7";
            else
                cbFirstStepFlexion.Text = "0";
        }
    
        private void OnOffBox_OnChanged(object sender, EventArgs e)
        {
            if (safetyonoff.IsOn == false)
            {
                //X_Thr.IsEnabled = false;
                Y_Thr.IsEnabled = false;
            }
            else if (safetyonoff.IsOn == true)
            {
                //X_Thr.IsEnabled = true;
                Y_Thr.IsEnabled = true;
            }
        
        }

        private void Trainingmode_OnChanged(object sender, EventArgs e)
        {
            if (Trainning.IsOn == false)
            {
                hip_final.IsEnabled = false;
                hip_max.IsEnabled = false;
                t_steptime.IsEnabled = false;
            }
            else if (Trainning.IsOn == true)
            {
                hip_final.IsEnabled = true;
                hip_max.IsEnabled = true;
                t_steptime.IsEnabled = true;
            }
        }
      

        private void CbSysType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            SN_ComBox.SelectedIndex = cbSysType.SelectedIndex;
        }

        
    

       
       

        private void CbCalibLH_Checked(object sender, RoutedEventArgs e)
        {
            btnStartCalib.IsEnabled = true;
        }

        private void CbCalibLK_Checked(object sender, RoutedEventArgs e)
        {
            btnStartCalib.IsEnabled = true;

        }

        private void CbCalibRH_Checked(object sender, RoutedEventArgs e)
        {
            btnStartCalib.IsEnabled = true;

        }

        private void CbCalibRK_Checked(object sender, RoutedEventArgs e)
        {
            btnStartCalib.IsEnabled = true;
        }

       
        private void TestInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            string InputString;
            
            FlowDocument rtbContents = TestInput.Document;
            TextRange textRange = new TextRange(
                // TextPointer to the start of content in the RichTextBox.
            rtbContents.ContentStart,
                // TextPointer to the end of content in the RichTextBox.
            rtbContents.ContentEnd);

            InputString = textRange.Text;
            
            if (InputString.Contains("\r\n\r\n"))
            {
                InputString = InputString.TrimEnd('\n');
                InputString = InputString.TrimEnd('\r');
                
                TestResult.AppendText("> "+InputString);
                InputString = InputString.TrimEnd('\n');
                InputString = InputString.TrimEnd('\r');
               // deviceData.SendDebugCommand(InputString);
                TestInput.Document.Blocks.Clear();
                
            }
        }


        private DateTime LastLogTime, FirstLogTime;
        private TimeSpan DeltaTime;

        private void LogGUIConnections() // parasoft-suppress  CS.MLC "Methods executing atomic functions" OOM.CYCLO "Coding style - method logic was verified"
        {
            CurrentTime = DateTime.Now; LastLogTime = CurrentTime; FirstLogTime = DateTime.MinValue; DeltaTime = TimeSpan.Zero;
  
            string path = SysLogPath; int LogSendDeltaTimeInDays = 100000; bool SendLog = false,cc=false; string FileName = "\\system_report_" + deviceData.Parameters.SN + ".rwk";
            try
            {
                using (StreamReader f = new StreamReader(File.Open(path + FileName, FileMode.Open)))
                {
                    string[] StringArray; string line = "-1"; bool LogSent = false, FoundLogSent = false;

                    do
                    {
                        if (line != "-1")
                        {
                            line = line.TrimStart('<'); line = line.TrimEnd('>'); StringArray = line.Split('|');
                            LogSent = System.Convert.ToBoolean(StringArray[2]);
                            if (!FoundLogSent) FoundLogSent = LogSent;
                           // if (LogSent) LastLogTime = DateTime.Parse(StringArray[0]);
                            if (LogSent)
                                cc= DateTime.TryParse(StringArray[0], out LastLogTime);
                            //if (FirstLogTime == DateTime.MinValue) FirstLogTime = DateTime.Parse(StringArray[0]);
                            if (FirstLogTime == DateTime.MinValue)
                                cc = DateTime.TryParse(StringArray[0], out FirstLogTime);

                        }
                        line = f.ReadLine();
                    } while (line != null);

                    if (!FoundLogSent) LastLogTime = FirstLogTime;
                       
                    DeltaTime = CurrentTime.Subtract(LastLogTime);

                    f.Close();
                }

            }
            catch (Exception exception) { Logger.Dummy_Error("", exception.Message); }

            try
            {
                string LogSendDeltaTimeInDaysString = null;
                XmlDocument doc = new XmlDocument(); doc.Load("DeviceSettingsValues.xml");
                XmlNodeList LogSendDeltaTimeNode = doc.SelectNodes("//ConnectionLogDeltaTime");
                if (LogSendDeltaTimeNode.Count == 1)
                    foreach (XmlNode value in LogSendDeltaTimeNode)
                    { LogSendDeltaTimeInDaysString = value.Attributes["Values"].Value; }
                LogSendDeltaTimeInDays = System.Convert.ToInt32(LogSendDeltaTimeInDaysString);
            }
            catch (Exception exception) { LogSendDeltaTimeInDays = 100000; Logger.Dummy_Error("", exception.Message); }



            if (DeltaTime.TotalDays >= LogSendDeltaTimeInDays) SendLog = true;
               
            try
            {
                using (StreamWriter f = new StreamWriter(File.Open(path + FileName, FileMode.Append)))

                { f.WriteLine("<{0,0} | {1,1} | {2,2}>", CurrentTime, deviceData.Parameters.StepCounter, SendLog); f.Close(); }

            }
            catch (Exception exception) { Logger.Dummy_Error("", exception.Message); }

            if (SendLog)
                SendEmailReport(0, true, false, ReportType.NORMAL);
        }

        private bool AFTToolScreenSet()
        {
                XmlDocument deviceSettings = new XmlDocument();
                deviceSettings.Load("DeviceSettingsValues.xml");
                string yesno = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTToolScreenEnable").OuterXml;
                string[] yesno_answer = yesno.Split('#');
              
                if (yesno_answer[1] == "YES")
                {
                    AFT.Visibility = Visibility.Visible;
                    return true;
                }
                else 
                {
                    AFT.Visibility = Visibility.Hidden;
                    return false;
                }
        }
        private string GUIVersionType = null;

        private void ReWalkStartUpConfiguration() //this Function will read xml configration
        {
            try
            {
                XmlDocument deviceSettings = new XmlDocument();
                deviceSettings.Load("DeviceSettingsValues.xml");
                string SelectedArea = deviceSettings.SelectSingleNode("/DeviceSettingsValues/RegionConfig").OuterXml;
                string[] name = SelectedArea.Split('#');
                if (name[1] == "USA" || name[1] == "Other")
                {
                    if (name[1] == "USA")
                    {
                        gbStairs.Visibility = Visibility.Collapsed;
                        gbStairEnable.Visibility = Visibility.Collapsed;
                      //  StairsLogPanel.Visibility = Visibility.Collapsed;
                        StairsLogGroupBox.Visibility = Visibility.Collapsed;
                        Grid.SetColumn(gbSoftwareDownload, 0);
                        GUIVersionType = "USA";

                    }
                    else if (name[1] == "Other")
                    {
                        GUIVersionType = "Other";
                    }
                }
                else
                {
                    Close();
                }
            }
            catch (Exception exception)
            {
                //MessageBox.Show("Loading System Configuration Parameters Failed: " + exception.Message, "Loading System Configuration", MessageBoxButton.OK, MessageBoxImage.Error);
                Logger.Error_MessageBox_Advanced("Loading System Configuration Parameters Failed: ", "Loading System Configuration", exception.Message);

            }

        }

      
        private string GetSecureString()
        {
            string st = String.Empty;
            /* Code for getting the password */
            st = "reports2013";
            return st;
        }

        private string GetGmailPassword()
        {
            string st = String.Empty;
            /* Code for getting the password */
            st = "argoadmin";
            return st;
        }

        private Attachment LogFileAttachement, FlashLogFileAttachement, PCLogFileAttachement;
        private MailMessage message;// = new MailMessage;

        public void AutoSaveLogfile(string filename)
        {
            
            string path = CheckDirectory();
            string datfilename = path + "\\" + deviceData.Parameters.SN + filename  + ".dat";
            string txtfilename = path + "\\" + deviceData.Parameters.SN + filename + ".txt";


            deviceData.SaveLogFile(datfilename);
            LogParser.ParseFile(datfilename , false, txtfilename);
            //Process.Start("notepad", txtfilename);
         
        }
        private string CheckDirectory()
        {
            deviceData.ReadData();
            string temp = AppDomain.CurrentDomain.BaseDirectory; string[] temp1;
            DirectoryInfo dir_info;
            temp1 = temp.Split('\\'); string AFTFolderPath = temp1[0] + "\\Argo\\AFT"; string SystemSerialNumber = temp1[0] + "\\Argo\\AFT\\" + deviceData.Parameters.SN;
            try
            {   
                if (!Directory.Exists(AFTFolderPath))
                    dir_info = Directory.CreateDirectory(AFTFolderPath);
                if (!Directory.Exists(SystemSerialNumber))
                    dir_info = Directory.CreateDirectory(SystemSerialNumber);
                return SystemSerialNumber;
            }
            catch (Exception exception)
            {
                // MessageBox.Show(exception.Message);
                Logger.Error(null, exception.Message);
                return null;
            }
        
        }
        public void AFTReport (string testername , string result ,string status , string testname ,string current ,bool backlashflag , string backlashvalue) // parasoft-suppress  METRICS.MLOC "Methods executing atomic functions" CS.MLC "Methods executing atomic functions" SEC.AUSD "Method inspected and no security violation found - only used for log record"
        {
        string path = CheckDirectory();
        DateTime CurrentTimeDate ;
        CurrentTimeDate = DateTime.Now;
        string FileLocation = path + "\\" + deviceData.Parameters.SN + " Calbiration" + ".dat"; 
        string SystemParameterHeader  = ReadSysParamsFromLog(FileLocation);

        string[] lines = SystemParameterHeader.Split('\n');
        int NumberOfLines = lines.Length;
        string filenamepath = path + "\\" + deviceData.Parameters.SN + "_AFT_Report" + ".txt";

       
        
      

        if (!File.Exists(filenamepath))
        {
            using (StreamWriter AFTFileReport = File.CreateText(filenamepath))
            {
                AFTFileReport.WriteLine("Rewalk AFT Report Generated at :".PadRight(30) + CurrentTimeDate + "\n");
                AFTFileReport.WriteLine("*********************************************************\n");
                AFTFileReport.WriteLine("ReWalk Parameters :\n");
                AFTFileReport.WriteLine(Environment.NewLine + " Tester Name:".PadRight(32) + testername + "\n");
                for (int i = 3; i <= NumberOfLines - 22; i++)
                {
                    AFTFileReport.WriteLine(lines[i]);
                }

                //AFTFileReport.WriteLine(Environment.NewLine + "____________ Test Name:- " + TestName + " _____________\n");
                //AFTFileReport.WriteLine(Environment.NewLine + " Calibration Current:".PadRight(32) + current + "\n");
                //AFTFileReport.WriteLine(Environment.NewLine + " Start Run 1 Test Time:".PadRight(32) + CurrentTimeDate + "\n");   
                
            }

        }

        else 
        {
            using (StreamWriter AFTFileReport = File.AppendText(filenamepath))
            {
                if (status == "Calibration")
                {
                    AFTFileReport.WriteLine(Environment.NewLine + "__Test Name:- " + testname + " _____________\n");
                    AFTFileReport.WriteLine(Environment.NewLine + " Calibration Current:".PadRight(32) + current + "    Result:-  " + result + "\n");
                
                }
                if (status == "Start")
                {

                    AFTFileReport.WriteLine(Environment.NewLine + " Start Test Time:".PadRight(32) + CurrentTimeDate + "  Test Name:- " + testname + "\n");   
                 //   AFTFileReport.WriteLine(Environment.NewLine + " Run 1 Current:".PadRight(32) + current + "\n");
                 //   AFTFileReport.WriteLine(Environment.NewLine + " End Run 1 Test:".PadRight(32)  + CurrentTimeDate + "\n");
                 //   AFTFileReport.WriteLine(Environment.NewLine + " Start Run 2 Test Time:".PadRight(32) + CurrentTimeDate + "\n");

                }
                if (status == "End")
                {
                   //    AFTFileReport.WriteLine(Environment.NewLine + " Start Current:".PadRight(32) + current + "\n");
                    if (backlashflag)
                        AFTFileReport.WriteLine(Environment.NewLine + " Backlash Result:".PadRight(32) + backlashvalue + "\n");
                    else
                        AFTFileReport.WriteLine(Environment.NewLine + " End Test:".PadRight(32) + CurrentTimeDate + " Test Name:- " + testname + "    Result:-  " + result + "\n".PadRight(32) + current + "\n");
                }
            /*    if (status == "Start_RUN_2")
                {

                    AFTFileReport.WriteLine(Environment.NewLine + " Start Run 2 Test Time:".PadRight(32) + CurrentTimeDate + "\n");   
               
                }

*/
                if (status == "End_")
                {
                    AFTFileReport.WriteLine(Environment.NewLine + " Current Test Result:".PadRight(32) + current + "\n");
                    AFTFileReport.WriteLine(Environment.NewLine + " End Run  Test:".PadRight(32) + CurrentTimeDate + "\n");
                    if (backlashflag)
                        AFTFileReport.WriteLine(Environment.NewLine + " Backlash Test Result:".PadRight(32) +"\n".PadRight(32)+ backlashvalue + "\n");
                 
                   // AFTFileReport.WriteLine(Environment.NewLine + " Test Result:".PadRight(32) + result + "\n");
                 //   AFTFileReport.WriteLine(Environment.NewLine + "Test Result:".PadRight(32) +"\n".PadRight(32)+ result + "\n");
                  //  AFTFileReport.WriteLine(Environment.NewLine + "______________________________________________________");

                }
                if (status == "End_All")
                {
                 
                    AFTFileReport.WriteLine(Environment.NewLine + " Test Result:".PadRight(32) + result + "\n");
                    AFTFileReport.WriteLine(Environment.NewLine + "______________________________________________________");

                }

            }
        }

        

        
        }
        private string ReadSysParamsFromLog( string erroreilelocation )
        {
            try
            {
                using (BinaryReader b = new BinaryReader(File.Open(erroreilelocation, FileMode.Open)))
                {
                
                    int length = (int)b.BaseStream.Length;
                    char[] LUTData = new char[length];
                    int ret_int =  b.Read(LUTData, 0, length);
                    string LUTStringData = new string(LUTData);
                    string SysParamsInfo = null;
                
                    bool SysParamsIncluded = LUTStringData.Contains("SYS_PARAMS");
                    if (SysParamsIncluded)
                    {
                        int index = LUTStringData.IndexOf("SYS_PARAMS");
                       // DateTime CurrentTimeLog = DateTime.Now;
                        string[] dataarray = new string[5000];
                        string[] ParamsArray = new string[1000];
                        string line2write;
                        dataarray = LUTStringData.Split('[');

                        LUTStringData = LUTStringData.Substring(index);//dataarray[4].Split(']');
                        ParamsArray = LUTStringData.Split('[');
                        ParamsArray = ParamsArray[0].Split(']');
                        ParamsArray = ParamsArray[1].Split('>');
                    
                        SysParamsInfo = "\nRewalk parameters downloaded at " + CurrentTime + "\n";
                        SysParamsInfo += "***********************************************************\n";
                        SysParamsInfo += "\n";

                   
                        foreach (string line in ParamsArray)
                        {
                            line2write = line.Replace('<', ' ');
                            SysParamsInfo += line2write + "\n";
                        }
                        
                    }
                    b.Close();
                    return SysParamsInfo;
                }
            }
            catch (Exception exception)
            {
                 Logger.Dummy_Error("", exception.Message);
                 return null;
            }
        }

        private void SendEmailReport(ushort errtype, bool sendReport = false, bool reportProgress = true, ReportType reportTypeVal = ReportType.ERROR) // parasoft-suppress  CS.MLC "Methods executing atomic functions" OOM.CYCLO "Coding style - method logic was verified" METRICS.MCCC "Coding style - method logic was verified" METRICS.MLOC "Methods executing atomic functions"
        {
            
            CurrentTime = DateTime.Now;
            string SystemLogLocation;
            var fromAddress = new MailAddress("Rewalk.reports@argomedtec.com", "Rewalk Error Reports " + deviceData.Parameters.SN);
            var toAddress = new MailAddress("Rewalk.reports@argomedtec.com");

           // string fromPass = GetSecureString();// "reports2013";
       
            string subject;
            string body;

            if (reportTypeVal == ReportType.ERROR)
            {
                subject = "Do Not Reply - Error report for system " + deviceData.Parameters.SN + " error #" + errtype;
                body = "This is an automatic report generated at " + CurrentTime + " for system " + deviceData.Parameters.SN + "\nerror #" + errtype + " occured, log and log flash file are attached.";
            }
            else
            {
                subject = "System Log Report System #" + deviceData.Parameters.SN;
                body = "This is an automatic report generated at " + CurrentTime + " for system " + deviceData.Parameters.SN + "log file is attached.";
 
            }

            string xmldata;
            string[] emails = null;
            string path = LogPath;//AppDomain.CurrentDomain.BaseDirectory;// Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData); //Directory.GetCurrentDirectory();
            
            bool DisableEmail = false;
            string output;
            string ErrorFileLocation;
            string ErrorFlashFileLocation;
            /*try
            {

                path = LogPath;
                if (!Directory.Exists(path))
                   Directory.CreateDirectory(path);
            }
            catch
            {
            }*/
            SystemLogLocation = path +"\\sys_logs\\system_report_" + deviceData.Parameters.SN +".rwk";
            
            //MessageBoxResult result = MessageBox.Show("Saving log files...", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            if (reportTypeVal == ReportType.ERROR)
            {
                ErrorFileLocation = path + "\\log_error_report_system_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
                ErrorFlashFileLocation = path + "\\flash_log_error_report_system_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
            }
            else
            {
                ErrorFileLocation = path + "\\log_file_report_system_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
                if (deviceData.Parameters.SysErrorType != -1)
                {
                    ErrorFlashFileLocation =  path + "\\flash_log_error_report_system_" + deviceData.Parameters.SN + "_" + CurrentTime.Day + "_" + CurrentTime.Month + "_" + CurrentTime.Year + "_" + CurrentTime.Hour + "_" + CurrentTime.Minute + "_" + CurrentTime.Second + ".dat";
                }
                else
                    ErrorFlashFileLocation = null;
            }
            try
            {
                //Just to make sure that file might be created
                using (FileStream fs = new FileStream(ErrorFileLocation, FileMode.Create))
                {
                    fs.Close();
                }

                deviceData.SaveLogFile(ErrorFileLocation);
            }
            catch (Exception exception)
            {
                if (reportProgress)
                {
                    output = string.Format("{0,15} ", "Failed\n");
                    progressform.rtfUserReport.AppendText(output);
                    progressform.Close_Report();
                }
                Logger.Dummy_Error("", exception.Message); 
                return;
            }

            try
            {
                if (reportTypeVal == ReportType.ERROR ||
                    deviceData.Parameters.SysErrorType != -1)
                {
                    //Just to make sure that file might be created
                    using (FileStream fs = new FileStream(ErrorFlashFileLocation, FileMode.Create))
                    {
                        fs.Close();
                    }

                    deviceData.SaveFlashLogFile(ErrorFlashFileLocation);
                    deviceData.SaveLogFile(ErrorFileLocation);
                }
            }
            catch (Exception exception)
            {
                if (reportProgress)
                {
                    output = string.Format("{0,15} ", "Failed\n");
                    progressform.rtfUserReport.AppendText(output);
                    progressform.Close_Report();
                }
                 Logger.Dummy_Error("", exception.Message); 
                return;
            }
            if (reportProgress)
            {
                output = string.Format("{0,15} ", "Done\n");
                progressform.rtfUserReport.AppendText(output);
            }


            string EmailInfo = ReadSysParamsFromLog(ErrorFileLocation);
                        body += EmailInfo;
            XmlDocument doc = new XmlDocument();
            doc.Load("DeviceSettingsValues.xml");

            try
            {
                XmlNodeList DisableEmailNode = doc.SelectNodes("//DisableEmail");
                if (DisableEmailNode.Count == 1)
                    foreach (XmlNode value in DisableEmailNode)
                    {
                        DisableEmail = value.Attributes["Values"].Value == "true" || value.Attributes["Values"].Value == "True";
                        

                    }
            }
            catch(Exception exception)
            {
                 Logger.Dummy_Error("", exception.Message); 
            }


            if (DisableEmail && false == sendReport)
            {
                //MessageBox.Show("Error Logs saved.", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                progressform.Close_Report();
                return;
            }
            if (reportProgress)
            {
                output = string.Format("{0,0} ", "Sending error logs......");
                progressform.rtfUserReport.AppendText(output);
                bool bb = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
                if (!bb)
                {
                    output = string.Format("{0,15} ", "No internet connection available\n");
                    progressform.rtfUserReport.AppendText(output);
                    progressform.Close_Report();
                    return;
                }
            }
            try 
            {
                XmlNodeList EmailNode = doc.SelectNodes("//Email");
                foreach (XmlNode email in EmailNode)
                {
                    xmldata = email.Attributes["Values"].Value;
                    emails = xmldata.Split(',');

                }
            }
            catch (Exception exception)
            {
                 Logger.Dummy_Error("", exception.Message); 
            }


        
            /*var smtp = new SmtpClient
            {
                Host = "mail.argomedtec.com",
                Port = 25,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = true,//false,
                Credentials = new NetworkCredential(fromAddress.User, GetSecureString(), "argoext")// (fromAddress.Address, fromPassword)
       
            };*/
            string GmailUser = "rewalk.reports123";

            var gmail = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(GmailUser, GetGmailPassword())// (fromAddress.Address, fromPassword)

            };

            message = new MailMessage(fromAddress, toAddress);
           
            message.Subject = subject;
            message.Body = body;

            foreach (string email in emails)
            {
               // if (email != "" && email != null)
                if (!String.IsNullOrEmpty(email))
                {
                   
                        try
                        {
                            message.CC.Add(email);
                        }
                        catch (Exception exception)
                        {
                            Logger.Dummy_Error("", exception.Message);
                        }
                    
                }
            }

            try
            {

                if (File.Exists(SystemLogLocation))
                {
                    PCLogFileAttachement = new Attachment(SystemLogLocation);
                    message.Attachments.Add(PCLogFileAttachement);
                }
                if (ErrorFileLocation != null)
                {
                    LogFileAttachement = new Attachment(ErrorFileLocation);
                    message.Attachments.Add(new Attachment(ErrorFileLocation));
                }
                if (ErrorFlashFileLocation != null)
                {
                    FlashLogFileAttachement = new Attachment(ErrorFlashFileLocation);
                    message.Attachments.Add(new Attachment(ErrorFlashFileLocation));
                }

            }
            catch(Exception exception)
            {
                    output = string.Format("{0,15} ", "Error PC Log file writing!\n");
                    progressform.rtfUserReport.AppendText(output);
                    progressform.Close_Report();            
                    Logger.Dummy_Error("", exception.Message);
                    return;
            }
            try
            {

                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };
                if (reportProgress)
                    //smtp.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(Client_SendCompleted);
                    gmail.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(Client_SendCompleted);
                //smtp.SendAsync(message, null);  

                gmail.SendAsync(message, null);


            }
            catch (Exception exception)
            {
                if (reportProgress)
                {
                    output = string.Format("{0,15} ", "Check internet connection!\n");
                    progressform.rtfUserReport.AppendText(output);
                    progressform.Close_Report();
                    return;
                }
                Logger.Dummy_Error("", exception.Message); 
            }
            
            
        }
 
        void Client_SendCompleted(object sender, AsyncCompletedEventArgs e) // parasoft-suppress  CS.MLC "Methods executing atomic functions"
        {
            string output;
            

            if (e.Error != null)
            {
                var fromAddress = new MailAddress("Rewalk.reports@argomedtec.com", "Rewalk Error Reports " + deviceData.Parameters.SN);
                var toAddress = new MailAddress("Rewalk.reports@argomedtec.com");

                var smtp = new SmtpClient
                {
                    Host = "mail.argomedtec.com",
                    Port = 25,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = true,//false,
                    Credentials = new NetworkCredential(fromAddress.User, GetSecureString(), "argoext")// (fromAddress.Address, fromPassword)

                };

                /*string GmailUser = "rewalk.reports";
                
                var gmail = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(GmailUser, GetGmailPassword())// (fromAddress.Address, fromPassword)

                };*/
                
                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors) { return true; };
                                
                //gmail.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(GmailClient_SendCompleted);

                //gmail.SendAsync(message, null);
                smtp.SendCompleted += new System.Net.Mail.SendCompletedEventHandler(SecondaryClient_SendCompleted);

                smtp.SendAsync(message, null);  
            }
            else
            {
                output = string.Format("{0,15} ", "Done\n");
                progressform.rtfUserReport.AppendText(output);
                try
                {
                    message.Dispose();
                    PCLogFileAttachement.Dispose();
                    LogFileAttachement.Dispose();
                    FlashLogFileAttachement.Dispose();
                }
                catch (Exception exception)
                {
                    Logger.Dummy_Error("", exception.Message);
                }
                progressform.Close_Report();
            }
			
            
        }

        void SecondaryClient_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            string output;
            try
            {
                message.Dispose();
                PCLogFileAttachement.Dispose();
                LogFileAttachement.Dispose();
                FlashLogFileAttachement.Dispose();
            }
            catch (Exception exception)
            {
                Logger.Dummy_Error("", exception.Message);
            }

            if (e.Error != null)
            {
                
                output = string.Format("{0,15} ", "Failed\n");
                progressform.rtfUserReport.AppendText(output);
            }
            else
            {
                output = string.Format("{0,15} ", "Done\n");
                progressform.rtfUserReport.AppendText(output);
                
                
            }
            progressform.Close_Report();

        }


        private void OnClearSystemError(object sender, RoutedEventArgs e)
        {
            deviceData.ClearSystemError();
            deviceData.Parameters.SysErrorType = deviceData.GetErrorStatus();
            tbSysErr.Text = deviceData.Parameters.SysErrorType == -1 ? "None" : deviceData.Parameters.SysErrorType.ToString();
            MessageBox.Show("System error cleared successfully !", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            btnClearSysErr.IsEnabled = false;
            btnFlashLogBrowse.IsEnabled = false;
            

        }

    

        private Report_Progress progressform = new Report_Progress();        

        private void OnResetStepCounter(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to reset steps counter?", "Steps Counter Reset", MessageBoxButton.YesNo, MessageBoxImage.Warning);
           if (result == MessageBoxResult.Yes)
           {
                UInt32 StepsCounterReset = deviceData.ResetStepsCounter();
                if (StepsCounterReset == 0)
                {
                    MessageBox.Show("Reset was done successfully ", "Confirmation", MessageBoxButton.OK, MessageBoxImage.Warning);

                }
           }
          

        }

        private void OnSendLogFile(object sender, RoutedEventArgs e)
        {
            object s;
            s = Application.LoadComponent(new Uri("Report_Progress.xaml", UriKind.Relative));

            progressform = (Report_Progress)s;
            progressform.labelErrorReport.Content = "Sending system report";

            string output = string.Format("{0,0} ", "Saving log files......");
            progressform.AppendProgress(output);
            progressform.Visibility = System.Windows.Visibility.Visible;

            progressform.Show();

           
            SendEmailReport(0,true,true,ReportType.NORMAL);
        }

        public void BtnCom1_Loaded(object sender, RoutedEventArgs e)
        {
            btnCom1_LH = (CommandCtrl)sender;
        }

        private void BtnCom2_Loaded(object sender, RoutedEventArgs e)
        {
            btnCom2_LK = (CommandCtrl)sender;
        }

        private void BtnCom3_Loaded(object sender, RoutedEventArgs e)
        {
            btnCom3_RH = (CommandCtrl)sender;
        }

        private void BtnCom4_Loaded(object sender, RoutedEventArgs e)
        {
            btnCom4_RK = (CommandCtrl)sender;
        }

        CommandCtrl btnCom1_LH = new CommandCtrl();
        CommandCtrl btnCom2_LK = new CommandCtrl();
        CommandCtrl btnCom3_RH = new CommandCtrl();
        CommandCtrl btnCom4_RK = new CommandCtrl();

        private void OnBrowseViewLogFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.CheckFileExists = false;
            ofd.RestoreDirectory = true;
            ofd.Filter = "(*.dat)|*.dat|(*.*) |*.* | (*.txt)|*.txt";

            if (ofd.ShowDialog().Value == true)
            {
                tbViewLogFileLocation.Text = ofd.FileName;
            
                try
                {
                    string LogFileLocationText = tbViewLogFileLocation.Text;
                    string LogFileNameExtention = Path.GetExtension(LogFileLocationText);
                    if (LogFileNameExtention == ".dat")
                    {
                        LogFileLocationText = tbViewLogFileLocation.Text.TrimEnd("dat".ToCharArray());
                        LogFileLocationText = LogFileLocationText + "txt";

                        LogParser.ParseFile(tbViewLogFileLocation.Text, false, LogFileLocationText);
                    

                    }
                
                    Process.Start("notepad", LogFileLocationText);
                
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Log Parser Failed", "Save Flash Log File", MessageBoxButton.OK, MessageBoxImage.Error);
                    Logger.Dummy_Error("", exception.Message); 
                }
            }
        }


        private void BtnViewPlot_Click(object sender, RoutedEventArgs e)
        {
            try 
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.CheckFileExists = false;
                ofd.RestoreDirectory = true;
                ofd.Filter = "(*.dat)|*.dat|(*.*) |*.* | (*.txt)|*.txt";

                if (ofd.ShowDialog().Value == true)
                {
                    tbBrowseViewPlot.Text = ofd.FileName;


                    string LogFileLocationText = tbBrowseViewPlot.Text;
                    string LogFileNameExtention = Path.GetExtension(LogFileLocationText);
                    if (LogFileNameExtention == ".dat")
                    {
                        LogFileLocationText = tbBrowseViewPlot.Text.TrimEnd("dat".ToCharArray());
                        LogFileLocationText = LogFileLocationText + "txt";

                        LogParser.ParseFile(tbBrowseViewPlot.Text, false, LogFileLocationText);


                    }

                    bool TimeStampParserFlag = LogParser.TimeStampFlagIncluded();
                    string LogFileName = LogFileLocationText;//tbBrowseViewPlot.Text;
                    string TimeStampVariable;
                    string Path_plot_encoder_readings = Directory.GetCurrentDirectory();
                    string plot_encoder_readings = @Path_plot_encoder_readings + "\\EncoderPlot_py_ver8\\plot_encoder_readings_REV08.exe";

                    if (!TimeStampParserFlag)
                    {
                        TimeStampVariable = "3";
                    }
                    else
                    {
                        TimeStampVariable = "4";
                    }

                    Process.Start(plot_encoder_readings, "\"" + LogFileName + "\"" + " " + TimeStampVariable);
                }
                
            }

            catch (Exception exception)
            {
                //MessageBox.Show(this, "Encoder Readings Failed " + exception.Message);
                Logger.Error("Encoder Readings Failed " , exception.Message);
            }
        
        }

        
        private void BtnRunEncoderTest_Click(object sender, RoutedEventArgs e) // parasoft-suppress  OOM.CYCLO "Coding style - method logic was verified"
        {

            try
            {

                JointCounter = 0;

               if (cbCalibLH.IsChecked == true)
                   JointCounter++;
               if (cbCalibLK.IsChecked == true)
                    JointCounter++;
                if (cbCalibRH.IsChecked == true)
                    JointCounter++;
                if (cbCalibRK.IsChecked == true)
                   JointCounter++;

                if (JointCounter != 0)
                {
                    BtnRunEncoderTest.IsEnabled = false;
                    TestEncoderTimer.Start();
                    EncoderTestInProcess = true;
                }


            }
            catch (Exception exception)
            { Logger.Error("Encoder Test Failed ", exception.Message); }
           

        }

        private void OnCollectOnlineDataChanged(object sender, EventArgs e)
        {
           // bool isOn = (sender as OnOffBox).IsOn;
           
            int msec;
            bool cc; bool isOn;
            OnOffBox aa = sender as OnOffBox;
            if (aa != null)
            {
                isOn = aa.IsOn;
                collectInterval.IsEnabled = !isOn;
                if (isOn)
                {
                   
                    // int msec = Int32.Parse((collectInterval.SelectedItem as ComboBoxItem).Tag as string);
                    ComboBoxItem bb = collectInterval.SelectedItem as ComboBoxItem;
                    if (bb != null)
                    {
                        cc = Int32.TryParse(bb.Tag as string, out msec);
                        onlineTimer.Interval = new TimeSpan(0, 0, 0, 0, msec);
                        onlineTimer.Start();
                    }
                }
                else
                {
                    
                    onlineTimer.Stop();
                }
            }
           }

       private void LH_Download()
        {
            if (deviceData.TestData.ErrorCodeLH == (short)0x7fff) //success
            {
                MCUDownloadingProcessWindow.lblWorking.Content = "LH Downloading Finished Successfully.\nStarting LK Downloading...";
                deviceData.DownloadMCU(MCUFileArray);
                Joint_Being_Downloaded = 0x331;
                testTimer.Start();
            }
            else  //failed   
            {
                btnMCUDownload.IsEnabled = true;
                MCUDownloadingProcessWindow.MCUDOWNLOADINGINPROCESS = 0;//stop bar filling                                   
                MCUDownloadingProcessWindow.lblWorking.Content += "LH Downloading has failed!";
                Mouse.OverrideCursor = this.Cursor;
                //   MCUDownloadingProcessWindow.m_BackgroundWorker.CancelAsync();
                MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
                if (deviceData.TestData.ErrorCodeLH == (short)10)
                {
                    MessageBox.Show("LH downloading has failed:- Check Power or COM of LH\nError Code: " + deviceData.TestData.ErrorCodeLH.ToString(), "MCU LH Downloading", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (deviceData.TestData.ErrorCodeLH == (short)9)
                {
                    MessageBox.Show("LH downloading has failed:- Can't run LH in boot mode\nCheck Power or COM of LH\nError Code: " + deviceData.TestData.ErrorCodeLH.ToString(), "MCU LH Downloading", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show("LH downloading has failed:-?\nCheck Power or COM of LH\nError Code: " + deviceData.TestData.ErrorCodeLH.ToString(), "MCU LH Downloading", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                MessageBox.Show(this, "Downloading new MCU's software was canceled\nPlease restart the system and try again!!\n", "MCU's Downloading...", MessageBoxButton.OK, MessageBoxImage.Stop);
                //Thread.Sleep(2000);
                Delay_ms(2000); // replace Thread.Sleep
                MCUDownloadingProcessWindow.Hide();


                return;
            }
        }

       private void LK_Download()
       {
           if (deviceData.TestData.ErrorCodeLK == (short)0x7fff) //succsees
           {
               MCUDownloadingProcessWindow.lblWorking.Content = "LH Downloading Finished Successfully.\nLK Downloading Finished Successfully.\nStarting RH Downloading...";
               deviceData.DownloadMCU(MCUFileArray);
               Joint_Being_Downloaded = 0x332;
               testTimer.Start();
           }
           else  //failed   
           {
               btnMCUDownload.IsEnabled = true;
               MCUDownloadingProcessWindow.MCUDOWNLOADINGINPROCESS = 0;//stop bar filling                                   
               MCUDownloadingProcessWindow.lblWorking.Content += "LK Downloading has failed!";
               Mouse.OverrideCursor = this.Cursor;
               MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
               if (deviceData.TestData.ErrorCodeLH == (short)10)
               {
                   MessageBox.Show("LK downloading has failed:- Check Power or COM of LK\nError Code: " + deviceData.TestData.ErrorCodeLK.ToString(), "MCU LK Downloading", MessageBoxButton.OK, MessageBoxImage.Error);
               }
               else if (deviceData.TestData.ErrorCodeLH == (short)9)
               {
                   MessageBox.Show("LK downloading has failed:- Can't run LK in boot mode\nCheck Power or COM of LK\nError Code: " + deviceData.TestData.ErrorCodeLK.ToString(), "MCU LK Downloading", MessageBoxButton.OK, MessageBoxImage.Error);
               }
               else
               {
                   MessageBox.Show("LK downloading has failed \nCheck Power or COM of LK\nError Code: " + deviceData.TestData.ErrorCodeLK.ToString(), "MCU LK Downloading", MessageBoxButton.OK, MessageBoxImage.Error);
               }

               MessageBox.Show(this, "Downloading new MCU's software was canceled\nPlease restart the system and try again!!\n", "MCU's Downloading...", MessageBoxButton.OK, MessageBoxImage.Stop);
               //Thread.Sleep(2000);
               Delay_ms(2000); // replace Thread.Sleep
               MCUDownloadingProcessWindow.Hide();


               return;
           }
       }

       private void RH_Download()
       {
           if (deviceData.TestData.ErrorCodeRH == (short)0x7fff) //success
           {
               MCUDownloadingProcessWindow.lblWorking.Content = "LH Downloading Finished Successfully.\nLK Downloading Finished Successfully.\nRH Downloading Finished Successfully.\nStarting RK Downloading...";
               deviceData.DownloadMCU(MCUFileArray);
               Joint_Being_Downloaded = 0x333;
               testTimer.Start();
           }
           else  //failed   
           {

               btnMCUDownload.IsEnabled = true;
               MCUDownloadingProcessWindow.MCUDOWNLOADINGINPROCESS = 0;//stop bar filling                                   
               MCUDownloadingProcessWindow.lblWorking.Content += "RH Downloading has failed!";
               Mouse.OverrideCursor = this.Cursor;
               MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
               if (deviceData.TestData.ErrorCodeRH == (short)10)
               {
                   MessageBox.Show("RH downloading has failed:- Check Power or COM of RH\nError Code: " + deviceData.TestData.ErrorCodeRH.ToString(), "MCU RH Downloading", MessageBoxButton.OK, MessageBoxImage.Error);
               }
               else if (deviceData.TestData.ErrorCodeRH == (short)9)
               {
                   MessageBox.Show("RH downloading has failed:- Can't run RH in boot mode\nCheck Power or COM of RH\nError Code: " + deviceData.TestData.ErrorCodeRH.ToString(), "MCU RH Downloading", MessageBoxButton.OK, MessageBoxImage.Error);
               }
               else
               {
                   MessageBox.Show("RH downloading has failed:-?\nCheck Power or COM of RH\nError Code: " + deviceData.TestData.ErrorCodeRH.ToString(), "MCU RH Downloading", MessageBoxButton.OK, MessageBoxImage.Error);
               }

               MessageBox.Show(this, "Downloading new MCU's software was canceled\nPlease restart the system and try again!!\n", "MCU's Downloading...", MessageBoxButton.OK, MessageBoxImage.Stop);
               //Thread.Sleep(2000);
               Delay_ms(2000); // replace Thread.Sleep
               MCUDownloadingProcessWindow.Hide();


               return;
           }
       }

       private void RK_Download()
       {
           if (deviceData.TestData.ErrorCodeRK == (short)0x7fff) //success
           {
               MCUDownloadingProcessWindow.lblWorking.Content = "LH Downloading Finished Successfully.\nLK Downloading Finished Successfully.\nRH Downloading Finished Successfully.\nRK Downloading Finished Successfully.";
               Joint_Being_Downloaded = 0xfff;
               testTimer.Stop();
           }
           else  //failed   
           {
               btnMCUDownload.IsEnabled = true;
               MCUDownloadingProcessWindow.MCUDOWNLOADINGINPROCESS = 0;//stop bar filling                                   
               MCUDownloadingProcessWindow.lblWorking.Content += "RK Downloading has failed!";
               Mouse.OverrideCursor = this.Cursor;
               MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
               if (deviceData.TestData.ErrorCodeRK == (short)10)
               {
                   MessageBox.Show("RK downloading has failed:- Check Power or COM of RK\nError Code: " + deviceData.TestData.ErrorCodeRK.ToString(), "MCU RK Downloading", MessageBoxButton.OK, MessageBoxImage.Error);
               }
               else if (deviceData.TestData.ErrorCodeRK == (short)9)
               {
                   MessageBox.Show("RK downloading has failed:- Can't run RK in boot mode\nCheck Power or COM of RK\nError Code: " + deviceData.TestData.ErrorCodeRK.ToString(), "MCU RK Downloading", MessageBoxButton.OK, MessageBoxImage.Error);
               }
               else
               {
                   MessageBox.Show("RK downloading has failed:-?\nCheck Power or COM of RK\nError Code: " + deviceData.TestData.ErrorCodeRH.ToString(), "MCU RH Downloading", MessageBoxButton.OK, MessageBoxImage.Error);
               }

               MessageBox.Show(this, "Downloading new MCU's software was canceled\nPlease restart the system and try again!!\n", "MCU's Downloading...", MessageBoxButton.OK, MessageBoxImage.Stop);
               //Thread.Sleep(2000);
               Delay_ms(2000); // replace Thread.Sleep
               MCUDownloadingProcessWindow.Hide();
               testTimer.Stop();
               return;
           }
           Mouse.OverrideCursor = this.Cursor;
           //Thread.Sleep(1000);          
           Delay_ms(1000); // replace Thread.Sleep
           MessageBox.Show(this, "Downloading new MCU's software was finished successfully\nPlease, Restart the system!!\n", "MCU Downloading...", MessageBoxButton.OK, MessageBoxImage.Information);
           //Thread.Sleep(2000);
           Delay_ms(2000); // replace Thread.Sleep
           btnMCUDownload.IsEnabled = true;
           MCUDownloadingProcessWindow.M_BACKGROUNDWORKER.CancelAsync();
           MCUDownloadingProcessWindow.Hide();
       }

     
       private void AFT_GotFocus(object sender, RoutedEventArgs e)
       {
           toolbar.Visibility = Visibility.Hidden;
       }

       private void OnResetStairCounter(object sender, RoutedEventArgs e)
       {

           MessageBoxResult result = MessageBox.Show("Are you sure you want to reset stairs counter?", "Stairs Counter Reset", MessageBoxButton.YesNo, MessageBoxImage.Warning);
           if (result == MessageBoxResult.Yes)
           {
               
               UInt32 StairCounterReset = deviceData.ResetStairsCounter();
               if (StairCounterReset == 0)
               {
                   MessageBox.Show("Reset was done successfully ", "Confirmation", MessageBoxButton.OK, MessageBoxImage.Warning);
               
               }
           }
          

    
          
       }

       private void btnClearLogFile_Click(object sender, RoutedEventArgs e)
       {
           MessageBoxResult result = MessageBox.Show("Are you sure you want to reset log file ?", "Log File Reset", MessageBoxButton.YesNo, MessageBoxImage.Warning);
           if (result == MessageBoxResult.Yes)
           {
               string ClearLogFileStatus = deviceData.ClearLogFile();
               MessageBox.Show(ClearLogFileStatus, "Confirmation", MessageBoxButton.OK, MessageBoxImage.Warning);

           }
       }



       private void btnStairsCalc_Click(object sender, RoutedEventArgs e)
       {
          

           //ASCRightHip, ASCRightKnee, PlacingRighHip, PlacingRightKnee, DSCLeftHip, DSCLeftKnee, DSCRightHip 
          StairAnglesCalculator StCalc = new StairAnglesCalculator();
           StairAnglesCalculator.StairsFlexions StairsFlexions = new StairAnglesCalculator.StairsFlexions();

           StairsFlexions = StCalc.CalculateStarisAngle(Convert.ToDouble(tbUpperLegMeasurement.Text), Convert.ToDouble(tbLowerLegMeasurement.Text), Convert.ToDouble(tbSoleHeightMeasurement.Text), Convert.ToDouble(tbShoeSizeMeasurement.Text), Convert.ToDouble(tbStairHeightMeasurement.Text));
           LHASC.Text = StairsFlexions.ASC_LH.ToString();
           LKASC.Text = StairsFlexions.ASC_LK.ToString();
           RHASC.Text = StairsFlexions.ASC_RH.ToString();
           RKASC.Text = StairsFlexions.ASC_RK.ToString();

           PlacingRightHip.Text = StairsFlexions.PLACING_RH.ToString();
           PlacingRightKnee.Text = StairsFlexions.PLACING_RK.ToString();

           LHDSC.Text = StairsFlexions.DSC_LH.ToString();
           LKDSC.Text = StairsFlexions.DSC_Lk.ToString();
           RHDSC.Text = StairsFlexions.DSC_RH.ToString();
           RKDSC.Text = StairsFlexions.DSC_RK.ToString(); 

       }



      

    }
}
