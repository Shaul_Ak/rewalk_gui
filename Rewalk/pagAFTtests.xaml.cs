using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Win32;
using Rewalk.Device;
using System.Diagnostics;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Runtime.InteropServices;
using RewalkControls;
using System.Threading;
using System.Windows.Input;
using System.Security;
using System.Text;
//using Spire.Doc;
//using Spire.Doc.Documents;
//using System.Drawing;

using System.Windows.Media;

using System.Windows.Documents;

using System.Net;
using System.Net.Mail;
using System.Windows.Markup;
using FileParser;
namespace Rewalk
{

    public enum AFTTestStatus { Unknown, NOTCOMPLETED, PASS, FAIL };
    public enum AFTTests { CONTWALK, CONTSITSTAND, CONTASC, CONTDSC };


    public partial class pagAFTtests
    {

        Data AFTDeviceData = new Data();
        private DispatcherTimer WalkOnInProgressTimer;
        private DispatcherTimer DSCOnInProgressTimer;
        private DispatcherTimer ASCOnInProgressTimer;
        private DispatcherTimer SITSTANDOnInProgressTimer;
        private DispatcherTimer MotorPerformanceOnInProgressTimer;
        private DispatcherTimer BacklashOnInProgressTimer;
   

        public pagAFTtests()
        {
            InitializeComponent();
            WalkOnInProgressTimer = new DispatcherTimer(TimeSpan.FromMilliseconds(300), DispatcherPriority.Background, WalkOnInProgressTimer_Tick, this.Dispatcher);
            DSCOnInProgressTimer = new DispatcherTimer(TimeSpan.FromMilliseconds(300), DispatcherPriority.Background, DSCOnInProgressTimer_Tick, this.Dispatcher);
            ASCOnInProgressTimer = new DispatcherTimer(TimeSpan.FromMilliseconds(300), DispatcherPriority.Background, ASCOnInProgressTimer_Tick, this.Dispatcher);
            SITSTANDOnInProgressTimer = new DispatcherTimer(TimeSpan.FromMilliseconds(300), DispatcherPriority.Background, SITSTANDOnInProgressTimer_Tick, this.Dispatcher);
            MotorPerformanceOnInProgressTimer = new DispatcherTimer(TimeSpan.FromMilliseconds(300), DispatcherPriority.Background, MotorPerformanceOnInProgressTimer_Tick, this.Dispatcher);
            BacklashOnInProgressTimer = new DispatcherTimer(TimeSpan.FromMilliseconds(300), DispatcherPriority.Background, BacklashOnInProgressTimer_Tick, this.Dispatcher);
           
            
            CurrentRunningTestTimer = new DispatcherTimer(TimeSpan.FromMilliseconds(1000), DispatcherPriority.Background, CurrentRunningTest_SM, this.Dispatcher);
            WalkOnInProgressTimer.IsEnabled = false;
            DSCOnInProgressTimer.IsEnabled = false;
            ASCOnInProgressTimer.IsEnabled = false;
            SITSTANDOnInProgressTimer.IsEnabled = false;
            MotorPerformanceOnInProgressTimer.IsEnabled = false;
            BacklashOnInProgressTimer.IsEnabled = false;
            CurrentRunningTestTimer.IsEnabled = false;
       
            try
            {

                XmlDocument deviceSettings = new XmlDocument();
                deviceSettings.Load("DeviceSettingsValues.xml");

                string WalkCalCounts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTWalkCalibrationCounts").Attributes["Values"].Value;
                WalkCalibrationCounts = System.Convert.ToInt16(WalkCalCounts);
                string ASCCalCounts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTASCCalibrationCounts").Attributes["Values"].Value;
                ASCCalibrationCounts = System.Convert.ToInt16(ASCCalCounts);
                string DSCCalCounts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTDSCCalibrationCounts").Attributes["Values"].Value;
                DSCCalibrationCounts = System.Convert.ToInt16(DSCCalCounts);
                string SitStandCalCounts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTSitStandCalibrationCounts").Attributes["Values"].Value;
                SitStandCalibrationCounts = System.Convert.ToInt16(SitStandCalCounts);

                string WalkR1Counts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTWalkRun1Counts").Attributes["Values"].Value;
                WalkRun1Counts = System.Convert.ToInt16(WalkR1Counts);
                string ASCR1Counts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTASCRun1Counts").Attributes["Values"].Value;
                ASCRun1Counts = System.Convert.ToInt16(ASCR1Counts);
                string DSCR1Counts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTDSCRun1Counts").Attributes["Values"].Value;
                DSCRun1Counts = System.Convert.ToInt16(DSCR1Counts);
                string SitStandR1Counts = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTSitStandRun1Counts").Attributes["Values"].Value;
                SitStandRun1Counts = System.Convert.ToInt16(SitStandR1Counts);


                string HipMax = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTHipMaxAngle").Attributes["Values"].Value;
                HipMaxAngle = System.Convert.ToInt16(HipMax);
                string HipMin = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTHipMinAngle").Attributes["Values"].Value;
                HipMinAngle = System.Convert.ToInt16(HipMin);

                string KneeMax = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTKneeMaxAngle").Attributes["Values"].Value;
                KneeMaxAngle = System.Convert.ToInt16(KneeMax);
                string KneeMin = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTKneeMinAngle").Attributes["Values"].Value;
                KneeMinAngle = System.Convert.ToInt16(KneeMin);        
            
            }
            catch (Exception exception)
            {
                Logger.Error_MessageBox_Advanced("Loading System Configuration Parameters Failed: ", "Loading System Configuration", exception.Message);

            }

        }

        private void WalkOnInProgressTimer_Tick(object sender, EventArgs e)
        {
            if (ContWalkPassEllipse.Fill == inProgressBrush)
            {
                ContWalkPassEllipse.Fill = inProgressBrushBlink;
            }
            else
            {
                ContWalkPassEllipse.Fill = inProgressBrush;
            }
        }

        private void DSCOnInProgressTimer_Tick(object sender, EventArgs e)
        {
            if (ContDSCPassEllipse.Fill == inProgressBrush)
            {
                ContDSCPassEllipse.Fill = inProgressBrushBlink;
            }
            else
            {
                ContDSCPassEllipse.Fill = inProgressBrush;
            }
        }
        private void ASCOnInProgressTimer_Tick(object sender, EventArgs e)
        {
            if (ContASCPassEllipse.Fill == inProgressBrush)
            {
                ContASCPassEllipse.Fill = inProgressBrushBlink;
            }
            else
            {
                ContASCPassEllipse.Fill = inProgressBrush;
            }
        }
        private void SITSTANDOnInProgressTimer_Tick(object sender, EventArgs e)
        {
            if (ContSitStandPassEllipse.Fill == inProgressBrush)
            {
                ContSitStandPassEllipse.Fill = inProgressBrushBlink;
            }
            else
            {
                ContSitStandPassEllipse.Fill = inProgressBrush;
            }
        }
        private void MotorPerformanceOnInProgressTimer_Tick(object sender, EventArgs e)
        {
            if (MotorPerformancePassEllipse.Fill == inProgressBrush)
            {
                MotorPerformancePassEllipse.Fill = inProgressBrushBlink;
            }
            else
            {
                MotorPerformancePassEllipse.Fill = inProgressBrush;
            }
        }
        private void BacklashOnInProgressTimer_Tick(object sender, EventArgs e)
        {
            if (BacklashPassEllipse.Fill == inProgressBrush)
            {
                BacklashPassEllipse.Fill = inProgressBrushBlink;
            }
            else
            {
                BacklashPassEllipse.Fill = inProgressBrush;
            }
        }
        private int TimeOut_Cnt = 0;

        //Main Proccess
        private void CurrentRunningTest_SM(object sender, EventArgs e)
        {
            string mmm = "";
           
            AFTTestStatus status;
            if ((App.Current as App).DeptName_Connected)
            {

                if (AFTCALBIRATE)
                {

                    ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read actual current from MCU
                    if (enablerecordcurrent)
                    {
                        RecordCurrentsEnable();
                    }
                    string testname = GetTestName();
                    if ((AFTToolConCurrentthr <= (ActualAverageCurrent + 0.5)) && ((AFTToolConCurrentthr >= ActualAverageCurrent - 0.5)))
                    {
                        Main MainWindow = new Main();
                        MainWindow.AutoSaveLogfile(" Calbiration");
                        MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName), "", "", "", "", false, "");
                        MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName), AFTTestStatus.PASS.ToString(), "Calibration", testname,"I: "+ motors_current, false, "");


                        AFTCALBIRATE = false;
                        Starttool.IsEnabled = true;
                        ActualSavedCurrent = ActualAverageCurrent;
                        mmm = "";

                        MessageBox.Show(("Success" + "\nThe measured average current is: " + ActualAverageCurrent.ToString() + "\n Press Start to run test." + mmm), "AFT Calibration", MessageBoxButton.OK, MessageBoxImage.Information);

                    }
                    else
                    {

                        AFTCALBIRATE = false;
                        if (ActualAverageCurrent < AFTToolConCurrentthr)
                            mmm = "Increase Load!";
                        else if (ActualAverageCurrent > AFTToolConCurrentthr)
                            mmm = "Decrease Load!";

                       
                        MessageBox.Show("Failed " + "\nThe measured current is: " + ActualAverageCurrent.ToString() + "\n" + mmm, "AFT Calibration", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    return;
                }//AFTCALbirate
                if (StartWalkTestRunning)
                {

                    if (AFT_All_in_process || AFT_WALK_in_process)
                    {
                        AFT_WALK_in_process = true;
                        ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read actual current from MCU
                        if ((ActualAverageCurrent <= (ActualSavedCurrent + 0.5)) && (ActualAverageCurrent >= (ActualSavedCurrent - 0.5)) && (App.Current as App).DeptName_AFTCONWALKST == 2)
                        {
                           

                            SetTestStatus(AFTTestStatus.PASS, true);
                          
                            Main MainWindow = new Main();
                            MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName), AFTTestStatus.PASS.ToString(), "End", "Continuous Walk", "I: " + motors_current + "  Steps: " + (App.Current as App).DeptName_AFTCONWALKCOUNTER, false, "");

                        }
                        else
                        {
                          
                            SetTestStatus(AFTTestStatus.FAIL, true);
                            Motors_release();
                            Main MainWindow = new Main();
                            MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName), AFTTestStatus.FAIL.ToString(), "End", "Continuous Walk", "I: " + motors_current + "  Steps: " + (App.Current as App).DeptName_AFTCONWALKCOUNTER, false, "");
                            Write_final_status();
                        }
                        if (AFT_WALK_in_process == true && AFT_All_in_process == false)
                        {
                            AFT_WALK_in_process = false;
                            StartWalkTestRunning = false;
                            CurrentRunningTestTimer.IsEnabled = false;
                            return;
                        }
                        //go to DSC
                        if (AFT_All_in_process)
                        {
                            Main MainWindow1 = new Main();

                            MainWindow1.AFTReport(((App.Current as App).DeptName_TesterFirstName), "", "Start", "Continuous DSC", ActualAverageCurrent.ToString(), false, "");
                            //send custom command 
                            byte[] data = { 0x5b, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
                            Int16 run1Ccounts = DSCRun1Counts; //DSC
                            data[4] = (byte)(run1Ccounts & 0xFF);
                            data[3] = (byte)(run1Ccounts >> 8);
                            data[5] = 1;
                            data[9] = 100; data[8] = 100;
                            byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                            byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
                            MainWindow1.RunSystem();
                            StartDSCTestRunning = true;
                            AFT_DSC_in_process = true;
                            AFT_WALK_in_process = false;
                            StartWalkTestRunning = false;
                            DSCOnInProgressTimer.IsEnabled = true;
                            return;
                        }
                    }

                } //End Walk
                if (StartDSCTestRunning)
                {

                    if (AFT_All_in_process || AFT_DSC_in_process)
                    {
                        //go to ASC
                        ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read actual current from MCU
                        if ((App.Current as App).DeptName_AFTCONDSCST == 2)// + 0.5)) && ((ActualAverageCurrent >= ActualSavedCurrent - 0.5)))
                        {
                           
                            SetTestStatus(AFTTestStatus.PASS, true);
                         

                            Main MainWindow = new Main();
                            MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName), AFTTestStatus.PASS.ToString(), "End", "Continuous DSC", "I: " + motors_current + "  Steps: " + (App.Current as App).DeptName_AFTCONDSCCOUNTER, false, "");

                        }
                        else
                        {
                            
                            SetTestStatus(AFTTestStatus.FAIL, true);
                            Motors_release();
                            Main MainWindow = new Main();
                            MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName), AFTTestStatus.FAIL.ToString(), "End", "Continuous DSC", "I: " + motors_current + "  Steps: " + (App.Current as App).DeptName_AFTCONDSCCOUNTER, false, "");
                            Write_final_status();
                        }
                        if (AFT_DSC_in_process == true && AFT_All_in_process == false)
                        {
                            AFT_DSC_in_process = false;
                            StartDSCTestRunning = false;
                            CurrentRunningTestTimer.IsEnabled = false;
                            return;
                        }
                        //go to ASC                    
                        Main MainWindow2 = new Main();
                        MainWindow2.AFTReport(((App.Current as App).DeptName_TesterFirstName), "", "Start", "Continuous ASC", ActualAverageCurrent.ToString(), false, "");
                        //send custom command 
                        byte[] data = { 0x5a, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
                        Int16 run1Ccounts = ASCRun1Counts; //ASC
                        data[4] = (byte)(run1Ccounts & 0xFF);
                        data[3] = (byte)(run1Ccounts >> 8);
                        data[5] = 1;
                        data[9] = 100; data[8] = 100;
                        byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                        byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
                        MainWindow2.RunSystem();
                        StartASCTestRunning = true;
                        StartDSCTestRunning = false;
                        AFT_DSC_in_process = false;
                        AFT_ASC_in_process = true;
                        ASCOnInProgressTimer.IsEnabled = true;
                       
                        return;
                    }//end DSC
                }
                if (StartASCTestRunning)
                {

                    if (AFT_All_in_process || AFT_ASC_in_process)
                    {
                        //Check ASC
                        ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read actual current from MCU
                        if ((App.Current as App).DeptName_AFTCONASCST == 2)// + 0.5)) && ((ActualAverageCurrent >= ActualSavedCurrent - 0.5)))
                        {
                           
                            SetTestStatus(AFTTestStatus.PASS, true);
                         
                            Main MainWindow = new Main();
                            MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName), AFTTestStatus.PASS.ToString(), "End", "Continuous ASC", "I: " + motors_current + "  Steps: " + (App.Current as App).DeptName_AFTCONASCCOUNTER, false, "");

                        }
                        else
                        {
                            
                            SetTestStatus(AFTTestStatus.FAIL, true);
                            Motors_release();
                            Main MainWindow = new Main();
                            MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName), AFTTestStatus.FAIL.ToString(), "End", "Continuous ASC", "I: " + motors_current + "  Steps: " + (App.Current as App).DeptName_AFTCONASCCOUNTER, false, "");
                            Write_final_status();
                        }


                        if (AFT_ASC_in_process == true && AFT_All_in_process == false)
                        {
                            AFT_ASC_in_process = false;
                            StartASCTestRunning = false;
                            CurrentRunningTestTimer.IsEnabled = false;
                            return;
                        }

                        //go to Sit Stand

                        Main MainWindow2 = new Main();
                        MainWindow2.AFTReport(((App.Current as App).DeptName_TesterFirstName), "", "Start", "Continuous Sit_Stand", ActualAverageCurrent.ToString(), false, "");
                        //send custom command 
                        byte[] data = { 0x4e, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
                        Int16 run1Ccounts = SitStandRun1Counts;//SitStand
                        data[4] = (byte)(run1Ccounts & 0xFF);
                        data[3] = (byte)(run1Ccounts >> 8);
                        data[5] = 1;
                        data[9] = 100; data[8] = 100;
                        byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                        byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
                        MainWindow2.RunSystem();
                        StartASCTestRunning = false;
                        StartSitStandTestRunning = true;
                      
                        AFT_DSC_in_process = false;
                        AFT_ASC_in_process = false;
                        AFT_SITSTAND_in_process = true;
                        SITSTANDOnInProgressTimer.IsEnabled = true;
                       
                        return;
                    } //end Asc
                }
                if (StartSitStandTestRunning || AFT_SITSTAND_in_process)
                {

                    if (AFT_All_in_process || AFT_SITSTAND_in_process)
                    {
                        //Check ASC
                        ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read actual current from MCU
                        if ((App.Current as App).DeptName_AFTCONSITSTANDST == 2)// + 0.5)) && ((ActualAverageCurrent >= ActualSavedCurrent - 0.5)))
                        {
                           
                            SetTestStatus(AFTTestStatus.PASS, true);
                           
                            Main MainWindow = new Main();
                            MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName), AFTTestStatus.PASS.ToString(), "End", "Continuous Sit_Stand", "I: " + motors_current + "   Steps: " + (App.Current as App).DeptName_AFTCONSITSTANDCOUNTER, false, "");

                        }
                        else
                        {
                           
                            SetTestStatus(AFTTestStatus.FAIL, true);
                            Motors_release();
                            Main MainWindow = new Main();
                            MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName), AFTTestStatus.FAIL.ToString(), "End", "Continuous Sit_Stand", "I: " + motors_current + "  Steps: " + (App.Current as App).DeptName_AFTCONSITSTANDCOUNTER, false, "");
                            Write_final_status();
                        }


                        if (AFT_SITSTAND_in_process == true && AFT_All_in_process == false)
                        {
                            AFT_SITSTAND_in_process = false;
                            StartSitStandTestRunning = false;
                            CurrentRunningTestTimer.IsEnabled = false;
                            return;
                        }
                        //go to check current final

                        FinalCurrenttestinWalk();
                      //  Main MainWindow2 = new Main();
                     //   MainWindow2.AFTReport(((App.Current as App).DeptName_TesterFirstName), "", "Start", "Backlash", ActualAverageCurrent.ToString(), false, "");
                      /*  MessageBox.Show("Measure Backlash of each joint and then press Backlash button.", "Backlash", MessageBoxButton.OK, MessageBoxImage.Information);
                  

                        CurrentRunningTestTimer.IsEnabled = true;
                        backlash.IsEnabled = true;
                        ReadEncoders1.IsEnabled = true;
                        ReadEncoders2.IsEnabled = true;*/
                    

                        //send custom command 
                    
                        StartASCTestRunning = false;
                        StartSitStandTestRunning = false;
                       
                        AFT_DSC_in_process = false;
                        AFT_ASC_in_process = false;
                        AFT_SITSTAND_in_process = false;
                        AFT_WALK_in_process = false;
                        AFT_MotorPerformance_in_process = false;
                        AFT_Backlash_in_process = false;
                                            
                        return;
                    } //end SitStand  
                }
                if (AFT_MotorPerformance_in_process)
                {

                    if (AFT_All_in_process)
                    {
                        Main MainWindow2 = new Main();
                        MainWindow2.AFTReport(((App.Current as App).DeptName_TesterFirstName), "", "Start", "Motors Performance", "", false, "");

                        CurrentRunningTestTimer.IsEnabled = false;

                        if (CheckMotorPerformance())
                        {
                            SetTestStatus(AFTTestStatus.PASS, true);
                           
                            MainWindow2.AFTReport(((App.Current as App).DeptName_TesterFirstName), AFTTestStatus.PASS.ToString(), "End", "Motors Performance", (AFTTestStatus.PASS.ToString() + " -> " + " LH: " + Motors_perf[0] + " LK: " + Motors_perf[1] + " RH: " + Motors_perf[2] + " RK: " + Motors_perf[3]), false, "");

                        }
                        else
                        {
                            SetTestStatus(AFTTestStatus.FAIL, true);
                            Motors_release();
                            MainWindow2.AFTReport(((App.Current as App).DeptName_TesterFirstName), AFTTestStatus.FAIL.ToString(), "End", "Motors Performance", (AFTTestStatus.FAIL.ToString() + " -> " + " LH: " + Motors_perf[0] + " LK: " + Motors_perf[1] + " RH: " + Motors_perf[2] + " RK: " + Motors_perf[3]), false, "");
                           
                        }
                        ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read actual current from MCU
                        AFT_MotorPerformance_in_process = false;

                        Write_final_status();

                    
                        MessageBox.Show("Test finished.", "Test", MessageBoxButton.OK, MessageBoxImage.Information);
                        AFT_All_in_process = false;
                        CurrentRunningTestTimer.IsEnabled = false;
                        Calibratetool.IsEnabled = true;
                        Backlash_TestRunning = false;
                     

                    }
                }
                if (Backlash_TestRunning)
                {

                    if (AFT_All_in_process)
                    {
                        RecordCurrentsDisable();
                        ActualAverageCurrent = ReadAverageToolCalibMCUCurrent();//read final actual current from MCU

                        string testname = GetTestName();
                        if ((ActualSavedCurrent <= (ActualAverageCurrent + 0.5)) && ((ActualSavedCurrent >= ActualAverageCurrent - 0.5)))
                        {

                            CurrentStatus = AFTTestStatus.PASS;
                            Current_Final_Test_Result = AFTTestStatus.PASS;
                        }
                        else
                        {
                            CurrentStatus = AFTTestStatus.FAIL;
                            Current_Final_Test_Result = AFTTestStatus.FAIL;
                        }

                        if (CurrentStatus == AFTTestStatus.PASS && Backlash_Test_Result == AFTTestStatus.PASS)
                        {
                            status = AFTTestStatus.PASS;
                        }
                        else
                        {
                            status = AFTTestStatus.FAIL;
                        }


                        Main MainWindow = new Main();

                        ResetINFTest();
                        MainWindow.AFTReport((App.Current as App).DeptName_TesterFirstName, status.ToString(), "End_", "Test", (CurrentStatus.ToString() + " I: " + motors_current), false, (Backlash_Test_Result.ToString() + " -> LH: " + lh_b.Text + " LK: " + lk_b.Text + " RH: " + rh_b.Text + " RK: " + rk_b.Text + " "));
                        MainWindow.AutoSaveLogfile(" RunTest");
                        AFT_MotorPerformance_in_process = false;
                        MotorPerformanceOnInProgressTimer.IsEnabled = false;
                        Backlash_TestRunning = false;
                        MessageBox.Show("Measure Backlash of each joint and then press Backlash button.", "Backlash", MessageBoxButton.OK, MessageBoxImage.Information);
                  

                        CurrentRunningTestTimer.IsEnabled = true;
                        backlash.IsEnabled = true;
                        ReadEncoders1.IsEnabled = true;
                        ReadEncoders2.IsEnabled = true;
                        return;
                    }
                } //end backlash

            }
        }


        void Write_final_status()
        {
            AFTTestStatus status;
            Main MainWindow = new Main();

            if (aftcontWalktreportstatus == AFTTestStatus.PASS && aftcontSitStandreportstatus == AFTTestStatus.PASS && aftcontASCreportstatus == AFTTestStatus.PASS && aftcontDSCreportstatus == AFTTestStatus.PASS && Backlash_Test_Result == AFTTestStatus.PASS && Current_Final_Test_Result == AFTTestStatus.PASS)
            {
                status = AFTTestStatus.PASS;
            }
            else{
                status = AFTTestStatus.FAIL;
            }

            MainWindow.AFTReport((App.Current as App).DeptName_TesterFirstName, status.ToString(), "End_All", "Test", (CurrentStatus.ToString() + " I: " + motors_current), false, (Backlash_Test_Result.ToString() + " -> LH: " + lh_b.Text + " LK: " + lk_b.Text + " RH: " + rh_b.Text + " RK: " + rk_b.Text + " "));
            
        }
                 


        byte[] motors_unrelease = { 0x30, 3, 0x24, 0, 0, 0, 0, 0, 0, 0 };
        byte[] motors_release = { 0x30, 3, 0x23, 0, 0, 0, 0, 0, 0, 0 };
        byte[] motor_convergetostand = { 0x30, 3, 0, 0, 0, 0, 0, 0, 0, 0 };

        void Motors_unrelease()
        {
            short angle;
            motors_unrelease[0] = 0x30;
            AFTDeviceData.RunCustomCommand(1, motors_unrelease);
            motors_unrelease[0] = 0x31;
            AFTDeviceData.RunCustomCommand(2, motors_unrelease);
            motors_unrelease[0] = 0x32;
            AFTDeviceData.RunCustomCommand(3, motors_unrelease);
            motors_unrelease[0] = 0x33;
            AFTDeviceData.RunCustomCommand(4, motors_unrelease);

            //move to stand 
            angle = 0x2c0;
            motor_convergetostand[6] = (byte)(angle);
            motor_convergetostand[5] = (byte)(angle >> 8);
            motor_convergetostand[0] = 0x30;
            AFTDeviceData.RunCustomCommand(1, motor_convergetostand);//LH
            motor_convergetostand[0] = 0x32;
            AFTDeviceData.RunCustomCommand(3, motor_convergetostand);//RH

            angle = -136;
            motor_convergetostand[6] = (byte)(angle);
            motor_convergetostand[5] = (byte)(angle >> 8);
            motor_convergetostand[0] = 0x31;
            AFTDeviceData.RunCustomCommand(2, motor_convergetostand);//LH
            motor_convergetostand[0] = 0x33;
            AFTDeviceData.RunCustomCommand(4, motor_convergetostand);//RH
        

        }

        void Motors_unrelease_withoutmove()
        {
           
            motors_unrelease[0] = 0x30;
            AFTDeviceData.RunCustomCommand(1, motors_unrelease);
            motors_unrelease[0] = 0x31;
            AFTDeviceData.RunCustomCommand(2, motors_unrelease);
            motors_unrelease[0] = 0x32;
            AFTDeviceData.RunCustomCommand(3, motors_unrelease);
            motors_unrelease[0] = 0x33;
            AFTDeviceData.RunCustomCommand(4, motors_unrelease);
        }
       void  Motors_release_running()
        {
        
            motors_release[0] = 0x30;
            AFTDeviceData.RunCustomCommand(1, motors_release);
            motors_release[0] = 0x31;
            AFTDeviceData.RunCustomCommand(2, motors_release);
            motors_release[0] = 0x32;
            AFTDeviceData.RunCustomCommand(3, motors_release);
            motors_release[0] = 0x33;
            AFTDeviceData.RunCustomCommand(4, motors_release);
    }
        void Motors_release()
        {

            motors_release[0] = 0x30;
            AFTDeviceData.RunCustomCommand(1, motors_release);
            motors_release[0] = 0x31;
            AFTDeviceData.RunCustomCommand(2, motors_release);
            motors_release[0] = 0x32;
            AFTDeviceData.RunCustomCommand(3, motors_release);
            motors_release[0] = 0x33;
            AFTDeviceData.RunCustomCommand(4, motors_release);
            MessageBox.Show("Test was failed!");

            WalkOnInProgressTimer.IsEnabled = false;
            DSCOnInProgressTimer.IsEnabled = false;
            ASCOnInProgressTimer.IsEnabled = false;
            SITSTANDOnInProgressTimer.IsEnabled = false;
            MotorPerformanceOnInProgressTimer.IsEnabled = false;
            BacklashOnInProgressTimer.IsEnabled = false;
            CurrentRunningTestTimer.IsEnabled = false;
            ReadEncoders1.IsEnabled = false;
            ReadEncoders2.IsEnabled = false;
            backlash.IsEnabled = false;
          
            AFT_Backlash_in_process = false;
            AFT_WALK_in_process = false;
            AFT_SITSTAND_in_process = false;
            AFT_ASC_in_process = false;
            AFT_DSC_in_process = false;
            AFT_All_in_process = false;
            AFT_MotorPerformance_in_process = false;
            AllRadBut.IsEnabled = false;
            ContWalkRadBut.IsEnabled = false;
            ContDSCRadBut.IsEnabled = false;
            ContASCRadBut.IsEnabled = false;
            BacklashRadBut.IsEnabled = false;
            ContSitStandRadBut.IsEnabled = false;
            MotorPerformanceRadBut.IsEnabled = false;
            Calibratetool.IsEnabled = false;
            Starttool.IsEnabled = false;



        }
   
        private DispatcherTimer CurrentRunningTestTimer;


        private bool AFT_All_in_process = false;
        private bool AFT_WALK_in_process = false;
        private bool AFT_SITSTAND_in_process = false;
        private bool AFT_ASC_in_process = false;
        private bool AFT_DSC_in_process = false;
        bool AFT_MotorPerformance_in_process = false;
        bool AFT_Backlash_in_process = false;
        private float AFTToolConCurrentthr;
        private float AFTWalkToolContCurrentThr;   
        public bool AFTToolRUN1FINISH = false;
        public bool AFTToolRUN2FINISH = false;
        private bool AFTCALBIRATE = false;
        private AFTTestStatus aftcontWalktreportstatus;
        private AFTTestStatus aftcontSitStandreportstatus;
        private AFTTestStatus aftcontASCreportstatus;
        private AFTTestStatus aftcontDSCreportstatus;
        private AFTTestStatus aftbacklashreportstatus;
        private AFTTestStatus aftmotorperfreportstatus;
        private Int16 WalkCalibrationCounts;
        private Int16 ASCCalibrationCounts;
        private Int16 DSCCalibrationCounts;
        private Int16 SitStandCalibrationCounts;
        private Int16 WalkRun1Counts;
        private Int16 ASCRun1Counts;
        private Int16 DSCRun1Counts;
        private Int16 SitStandRun1Counts;
     
        private bool enablerecordcurrent = false;
        float ActualAverageCurrent;
        float ActualSavedCurrent;
      
        bool StartWalkTestRunning = false;
        bool StartDSCTestRunning = false;
        bool StartASCTestRunning = false;
        bool StartSitStandTestRunning = false;
        bool Backlash_TestRunning = false;

        private void btnOk_Click(object sender, MouseButtonEventArgs e)
        {


            if (!string.IsNullOrEmpty(testerfirstname.Text))
            {

                try
                {
                    if ((App.Current as App).DeptName_Connected)
                    {
                        XmlDocument deviceSettings = new XmlDocument();
                        deviceSettings.Load("DeviceSettingsValues.xml");
                        string en = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTContRecordCurrent").Attributes["Values"].Value;
                        if (en == "1")
                        {
                            RecordCurrentsEnable();
                            enablerecordcurrent = true;
                        }

                        string end = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTDefaultParameters").Attributes["Values"].Value;
                        if (end == "1")
                        {
                            AFTDeviceData.RestoreParameters();
                            AFTDeviceData.WriteParameters();

                        }
                        MessageBox.Show("Tester registration completed.", "Tester name created", MessageBoxButton.OK, MessageBoxImage.Information);
                        testerfirstname.IsEnabled = false;
                        (App.Current as App).DeptName_TesterFirstName = testerfirstname.Text;
                        AllRadBut.IsEnabled = true;
                        ContWalkRadBut.IsEnabled = true;
                        ContDSCRadBut.IsEnabled = true;
                        ContASCRadBut.IsEnabled = true;
                        BacklashRadBut.IsEnabled = true;
                        ContSitStandRadBut.IsEnabled = true;
                        MotorPerformanceRadBut.IsEnabled = true;
                      



                    }
                    else
                    {
                        MessageBox.Show("The USB should be connected first!", "User", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }

                catch (Exception exception)
                {
                    Logger.Error_MessageBox_Advanced("Loading System Configuration Failed: The USB should be connected first!", "Loading System Configuration", exception.Message);

                }

            }
            else
            {
                MessageBox.Show("Tester name should be filled", "Tester name created",
                MessageBoxButton.OK, MessageBoxImage.Error);
                testerfirstname.IsEnabled = true;

            }
        }

        private void AllRadBut_Checked(object sender, RoutedEventArgs e)
        {
            AFT_All_in_process = true;
            AFT_WALK_in_process = false;
            AFT_SITSTAND_in_process = false;
            AFT_ASC_in_process = false;
            AFT_DSC_in_process = false;
            AFT_Backlash_in_process = false;
            AFT_MotorPerformance_in_process = false;
            Calibratetool.IsEnabled = true;
            try
            {
                XmlDocument deviceSettings = new XmlDocument();
                deviceSettings.Load("DeviceSettingsValues.xml");
                string WalkCurrent = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTContWalkCurrent").Attributes["Values"].Value;
                AFTWalkToolContCurrentThr = System.Convert.ToInt32(WalkCurrent);
                AFTToolConCurrentthr = AFTWalkToolContCurrentThr;
                (App.Current as App).DeptName_RunINFtoApplication = true;
            }
            catch (Exception exception)
            {
                Logger.Error_MessageBox_Advanced("Loading System Configuration Parameters Failed: ", "Loading System Configuration", exception.Message);

            }

        }

        private void ContDSCRadBut_Checked(object sender, RoutedEventArgs e)
        {
            (App.Current as App).DeptName_RunINFtoApplication = true;


            Main MainWindow1 = new Main();

            MainWindow1.AFTReport(((App.Current as App).DeptName_TesterFirstName), "", "Start", "Continuous DSC", ActualAverageCurrent.ToString(), false, "");
            //send custom command 
            byte[] data = { 0x5b, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
            Int16 run1Ccounts = DSCRun1Counts; //DSC
            data[4] = (byte)(run1Ccounts & 0xFF);
            data[3] = (byte)(run1Ccounts >> 8);
            data[5] = 1;
            data[9] = 100; data[8] = 100;
            byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
            byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
            MainWindow1.RunSystem();
            StartDSCTestRunning = true;
            AFT_DSC_in_process = true;
            AFT_WALK_in_process = false;
            StartWalkTestRunning = false;
            AFT_ASC_in_process = false;
            AFT_All_in_process = false;
            AFT_MotorPerformance_in_process = false;
            AFT_Backlash_in_process = false;
            CurrentRunningTestTimer.IsEnabled = true;
            DSCOnInProgressTimer.IsEnabled = true;
           
        }

        private void ContWalkRadBut_Checked(object sender, RoutedEventArgs e)
        {
            
            Calibratetool.IsEnabled = true;
            AFT_WALK_in_process = true;
            AFT_SITSTAND_in_process = false;
            AFT_ASC_in_process = false;
            AFT_DSC_in_process = false;
            AFT_All_in_process = false;
            AFT_MotorPerformance_in_process = false;
            AFT_Backlash_in_process = false;
          
            try
            {
                XmlDocument deviceSettings = new XmlDocument();
                deviceSettings.Load("DeviceSettingsValues.xml");
                string WalkCurrent = deviceSettings.SelectSingleNode("/DeviceSettingsValues/AFTContWalkCurrent").Attributes["Values"].Value;
                AFTWalkToolContCurrentThr = System.Convert.ToInt32(WalkCurrent);
                AFTToolConCurrentthr = AFTWalkToolContCurrentThr;
                (App.Current as App).DeptName_RunINFtoApplication = true;
               
            }
            catch (Exception exception)
            {
                Logger.Error_MessageBox_Advanced("Loading System Configuration Parameters Failed: ", "Loading System Configuration", exception.Message);

            }
        }


        private static SolidColorBrush unknownBrush = new SolidColorBrush(Color.FromRgb(0xBB, 0xBB, 0xBB));
        private static SolidColorBrush failBrush = new SolidColorBrush(Color.FromRgb(0xDD, 0x11, 0x11));
        private static SolidColorBrush inProgressBrush = new SolidColorBrush(Color.FromRgb(0xDD, 0xDD, 0x11));
        private static SolidColorBrush inProgressBrushBlink = new SolidColorBrush(Color.FromRgb(0xFF, 0xFF, 0x00));
        private static SolidColorBrush successBrush = new SolidColorBrush(Color.FromRgb(0x11, 0xBB, 0x11));

        private void Calibratetool_Click(object sender, RoutedEventArgs e)
        {

            (App.Current as App).DeptName_RunINFtoApplication = true;
            AFTCALBIRATE = true;


            byte[] data = { 0x4f, 0, 1, 0, 5, 1, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier


            if (AFT_All_in_process == true)
            {
                data[0] = 0x4f;
                Int16 calcounts = WalkCalibrationCounts;
                data[4] = (byte)(calcounts & 0xFF);
                data[3] = (byte)(calcounts >> 8);

            }
            else if (AFT_WALK_in_process == true)
            {
                data[0] = 0x4f;
                Int16 calcounts = WalkCalibrationCounts;
                data[4] = (byte)(calcounts & 0xFF);
                data[3] = (byte)(calcounts >> 8);
                // timer.IsEnabled = true;
            }
            else if (AFT_SITSTAND_in_process == true)
            {
                data[0] = 0x4e;
                Int16 calcounts = SitStandCalibrationCounts;
                data[4] = (byte)(calcounts & 0xFF);
                data[3] = (byte)(calcounts >> 8);
            }
            else if (AFT_ASC_in_process == true)
            {
                Int16 calcounts = ASCCalibrationCounts;
                data[4] = (byte)(calcounts & 0xFF);
                data[3] = (byte)(calcounts >> 8);
                data[0] = 0x5a;
            }
            else if (AFT_DSC_in_process == true)
            {
                Int16 calcounts = DSCCalibrationCounts;
                data[4] = (byte)(calcounts & 0xFF);
                data[3] = (byte)(calcounts >> 8);
                data[0] = 0x5b;
            }
            data[5] = 1;
            data[8] = 100;//data module count
            data[9] = 100;//data module count
            byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
            byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);

            Main MainWindow = new Main();
            MainWindow.RunSystem();
            CurrentRunningTestTimer.IsEnabled = true;

        }
        byte[] WDG_Enable = { 0x6a, 0, 0, 0, 0, 0, 0, 0, 0 };
        private void RecordCurrentsEnable()
        {
            byte[] data = { 0x30, 0x03, 0x30, 1, 0, 10, 0, 0, 0, 0 }; //Read average current
            byte[] result;

            data[0] = 0x30;
            result = AFTDeviceData.RunCustomCommand(1, data);

            data[0] = 0x31;
            result = AFTDeviceData.RunCustomCommand(2, data);

            data[0] = 0x32;
            result = AFTDeviceData.RunCustomCommand(3, data);

            data[0] = 0x33;
            result = AFTDeviceData.RunCustomCommand(4, data);

        }
        private void RecordCurrentsDisable()
        {
            byte[] data = { 0x30, 0x03, 0x30, 2, 0, 4, 0, 0, 0, 0 }; //Read average current
            byte[] result;

            data[0] = 0x30;
            result = AFTDeviceData.RunCustomCommand(1, data);

            data[0] = 0x31;
            result = AFTDeviceData.RunCustomCommand(2, data);

            data[0] = 0x32;
            result = AFTDeviceData.RunCustomCommand(3, data);

            data[0] = 0x33;
            result = AFTDeviceData.RunCustomCommand(4, data);


        }
        private string GetTestName()
        {
            string testname = "";
            if (AFT_All_in_process == true)
            {
                testname = "All";
            }
            if (AFT_WALK_in_process == true)
            {
                testname = "Continuous Walk";
            }
            else if (AFT_SITSTAND_in_process == true)
            {
                testname = "Continuous Sit-Stand";
            }
            else if (AFT_ASC_in_process == true)
            {
                testname = "Continuous ASC";
            }
            else if (AFT_DSC_in_process == true)
            {
                testname = "Continuous DSC";
            }
            return testname;
        }
        AFTTestStatus CurrentStatus = AFTTestStatus.Unknown;
        private void SetTestStatus(AFTTestStatus status, bool fill)
        {

            if (AFT_WALK_in_process == true)
            {
                aftcontWalktreportstatus = status;
                CurrentStatus = status;
                WalkOnInProgressTimer.IsEnabled = false;
                if (fill)
                    FillContWalkEllipse(aftcontWalktreportstatus);
                WalkCheckBox.IsEnabled = true;
                WalkCheckBox.IsChecked = true;
                WalkCheckBox.IsEnabled = false;
             

            }
            else if (AFT_SITSTAND_in_process == true)
            {
                aftcontSitStandreportstatus = status;

                CurrentStatus = status;
                SITSTANDOnInProgressTimer.IsEnabled = false;
                if (fill) FillContSitStandEllipse(aftcontSitStandreportstatus);
                SitStandCheckBox.IsEnabled = true;
                SitStandCheckBox.IsChecked = true;
                SitStandCheckBox.IsEnabled = false;
            }
            else if (AFT_ASC_in_process == true)
            {
                aftcontASCreportstatus = status;

                CurrentStatus = status;
                ASCOnInProgressTimer.IsEnabled = false;
                if (fill) FillContASCEllipse(aftcontASCreportstatus);
                ASCCheckBox.IsEnabled = true;
                ASCCheckBox.IsChecked = true;
                ASCCheckBox.IsEnabled = false;

            }
            else if (AFT_DSC_in_process == true)
            {
                aftcontDSCreportstatus = status;

                CurrentStatus = status;
                DSCOnInProgressTimer.IsEnabled = false;
                if (fill) FillContDSCEllipse(aftcontDSCreportstatus);
                DSCCheckBox.IsEnabled = true;
                DSCCheckBox.IsChecked = true;
                DSCCheckBox.IsEnabled = false;
            }
            else if (AFT_Backlash_in_process == true)
            {
                aftbacklashreportstatus = status;

                CurrentStatus = status;
                BacklashOnInProgressTimer.IsEnabled = false;
                if (fill) FillContBacklashEllipse(aftbacklashreportstatus);
                BacklashCheckBox.IsEnabled = true;
                BacklashCheckBox.IsChecked = true;
                BacklashCheckBox.IsEnabled = false;
            }
            else if (AFT_MotorPerformance_in_process == true)
            {
                aftmotorperfreportstatus = status;

                CurrentStatus = status;
                MotorPerformanceOnInProgressTimer.IsEnabled = false;
                if (fill) FillContMotorPerfotmanceEllipse(aftmotorperfreportstatus);
                MotorPerformanceCheckBox.IsEnabled = true;
                MotorPerformanceCheckBox.IsChecked = true;
                MotorPerformanceCheckBox.IsEnabled = false;
            }


        }
        string motors_current = "";
        private float ReadAverageToolCalibMCUCurrent()
        {
            byte[] data = { 0x30, 0x03, 0x81, 0, 0, 0, 0, 0, 0, 0 }; //Read average current
            byte[] result;
            Int32 LH_AvgCurrent, LK_AvgCurrent, RH_AvgCurrent, RK_AvgCurrent;
            float fLH_AvgCurrent, fLK_AvgCurrent, fRH_AvgCurrent, fRK_AvgCurrent; 
            float AverageCurrent;


            data[0] = 0x30;
            result = AFTDeviceData.RunCustomCommand(1, data);
            LH_AvgCurrent = result[2];
            LH_AvgCurrent = (LH_AvgCurrent << 8) + result[3];

            data[0] = 0x31;
            result = AFTDeviceData.RunCustomCommand(2, data);
            LK_AvgCurrent = result[2];
            LK_AvgCurrent = (LK_AvgCurrent << 8) + result[3];

            data[0] = 0x32;
            result = AFTDeviceData.RunCustomCommand(3, data);
            RH_AvgCurrent = result[2];
            RH_AvgCurrent = (RH_AvgCurrent << 8) + result[3];

            data[0] = 0x33;
            result = AFTDeviceData.RunCustomCommand(4, data);
            RK_AvgCurrent = result[2];
            RK_AvgCurrent = (RK_AvgCurrent << 8) + result[3];

            AverageCurrent =  LH_AvgCurrent + LK_AvgCurrent + RH_AvgCurrent + RK_AvgCurrent;
            AverageCurrent =  (AverageCurrent / 416);
            fLH_AvgCurrent =  LH_AvgCurrent;
            fLH_AvgCurrent =  (fLH_AvgCurrent /104);

            fRH_AvgCurrent =  RH_AvgCurrent;
            fRH_AvgCurrent =  (fRH_AvgCurrent / 104);

            fLK_AvgCurrent =  LK_AvgCurrent;
            fLK_AvgCurrent =  (fLK_AvgCurrent / 104);

            fRK_AvgCurrent =   RK_AvgCurrent;
            fRK_AvgCurrent =  (fRK_AvgCurrent / 104);





            motors_current = (AverageCurrent.ToString()+" -> "+" LH: "+ fLH_AvgCurrent.ToString() +" LK: "+ fLK_AvgCurrent.ToString()+" RH: "+fRH_AvgCurrent.ToString()+" RK: "+fRK_AvgCurrent.ToString());
            return (AverageCurrent); //4*104=416 , average of four joints and 104 is current mcu divider
        }


        private string ResetINFTest()
        {
            string testname = "";
            if (AFT_WALK_in_process == true)
            {
                testname = "Continuous Walk";
                byte[] data = { 0x4f, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
            }
            else if (AFT_SITSTAND_in_process == true)
            {
                testname = "Continuous Sit-Stand";
                byte[] data = { 0x4e, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
            }
            else if (AFT_ASC_in_process == true)
            {
                testname = "Continuous ASC";
                byte[] data = { 0x5a, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
            }
            else if (AFT_DSC_in_process == true)
            {
                testname = "Continuous DSC";
                byte[] data = { 0x5b, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
            }
            return testname;
        }

        Int32 Backlash_lh, Backlash_lk, Backlash_rh, Backlash_rk;
        AFTTestStatus Backlash_Test_Result, Current_Final_Test_Result;
        private void backlash_Click(object sender, RoutedEventArgs e)
        {
            
     
            AFTTestStatus status;
            string testname = "";

            testname = GetTestName();
            BacklashOnInProgressTimer.IsEnabled = true; 

         

            if (!string.IsNullOrEmpty(lh_b.Text) && !string.IsNullOrEmpty(lk_b.Text) && !string.IsNullOrEmpty(rh_b.Text) && !string.IsNullOrEmpty(rk_b.Text))
            {
                try
                {
                  //  Backlash_lh = (Convert.ToInt16(lh_b.Text));
                  //  Backlash_lk = (Convert.ToInt16(lk_b.Text));
                  //  Backlash_rh = (Convert.ToInt16(rh_b.Text));
                  //  Backlash_rk = (Convert.ToInt16(rk_b.Text));


                    if ((Backlash_lh > 45) || (Backlash_lk > 45) || (Backlash_rh > 45) || (Backlash_rk > 45))
                    {
                        status = AFTTestStatus.FAIL;

                    }
                    else
                    {
                        status = AFTTestStatus.PASS;

                    }


                   
                    Backlash_Test_Result = status;
                    backlash.IsEnabled = false;
                    lh_b.IsEnabled = false;
                    lk_b.IsEnabled = false;
                    rh_b.IsEnabled = false;
                    rk_b.IsEnabled = false;
              //      lh.Text = "";
               //     lk.Text = "";
               ///     rh.Text = "";
                //    rk.Text = "";

                    Main MainWindow = new Main();
                    MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName), "", "Start", "Backlash", ActualAverageCurrent.ToString(), false, "");
            
                    if (AFT_All_in_process)
                    {
                        AFT_Backlash_in_process = true;
                        SetTestStatus(status, true);
                       // Backlash_TestRunning = true;
                        AFT_Backlash_in_process = false;
                        AFT_MotorPerformance_in_process = true;
                        MainWindow.AFTReport((App.Current as App).DeptName_TesterFirstName, status.ToString(), "End", "Backlash", (CurrentStatus.ToString() + "I: " + ActualAverageCurrent.ToString()), true, (Backlash_Test_Result.ToString() + "-> LH: " + lh_b.Text + " C " + lh_b_d.Text + " deg," + " LK: " + lk_b.Text + " C " + lk_b_d.Text + " deg,"
                 + " RH: " + rh_b.Text + " C " + rh_b_d.Text + " deg," + " RK: " + rk_b.Text + " C " + rk_b_d.Text + " deg"));
                       // FinalCurrenttestinWalk();
                    }
                    else if (AFT_Backlash_in_process == true)
                    {
                        SetTestStatus(status, true);
                        AFT_Backlash_in_process = false;
                        
                        MainWindow.AFTReport((App.Current as App).DeptName_TesterFirstName, status.ToString(), "End", "Backlash", (CurrentStatus.ToString() + "I: " + ActualAverageCurrent.ToString()), true, (Backlash_Test_Result.ToString() + "-> LH: " + lh_b.Text + " C " + lh_b_d.Text + " deg," + " LK: " + lk_b.Text + " C " + lk_b_d.Text + " deg,"
                            + " RH: " + rh_b.Text +" C " + rh_b_d.Text + " deg," + " RK: " + rk_b.Text  +" C " + rk_b_d.Text +" deg"));

                    }
                }

                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message + ": " + "Enter numbers only!");
                }
            }
            else
            {
                MessageBox.Show("Backlash value should be inserted for all joints!", "Backlash", MessageBoxButton.OK, MessageBoxImage.Information);
            }


        }


        void FinalCurrenttestinWalk()
        {
            byte[] data = { 0x4f, 0, 1, 0, 5, 1, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier



            data[0] = 0x4f;
            Int16 calcounts = WalkCalibrationCounts;
            data[4] = (byte)(calcounts & 0xFF);
            data[3] = (byte)(calcounts >> 8);
            data[5] = 1;
            data[8] = 100;//data module count
            data[9] = 100;//data module count
            byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
            byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
            Backlash_TestRunning = true;
            Main MainWindow = new Main();
            MainWindow.RunSystem();


        }
        public void FillContWalkEllipse(AFTTestStatus aftstatus)
        {
            switch (aftstatus)
            {

                case AFTTestStatus.PASS:
                    ContWalkPassEllipse.Fill = successBrush;
                    break;
                case AFTTestStatus.NOTCOMPLETED:
                    ContWalkPassEllipse.Fill = inProgressBrush;
                    break;
                case AFTTestStatus.FAIL:
                    ContWalkPassEllipse.Fill = failBrush;
                    break;
                default:
                    ContWalkPassEllipse.Fill = unknownBrush;
                    break;
            }
        }
        private void FillContSitStandEllipse(AFTTestStatus aftstatus)
        {
            switch (aftstatus)
            {
                case AFTTestStatus.PASS:
                    ContSitStandPassEllipse.Fill = successBrush;
                    break;
                case AFTTestStatus.NOTCOMPLETED:
                    ContSitStandPassEllipse.Fill = inProgressBrush;
                    break;
                case AFTTestStatus.FAIL:
                    ContSitStandPassEllipse.Fill = failBrush;
                    break;
                default:
                    ContSitStandPassEllipse.Fill = unknownBrush;
                    break;
            }
        }
        private void FillContASCEllipse(AFTTestStatus aftstatus)
        {
            switch (aftstatus)
            {
                case AFTTestStatus.PASS:
                    ContASCPassEllipse.Fill = successBrush;
                    break;
                case AFTTestStatus.NOTCOMPLETED:
                    ContASCPassEllipse.Fill = inProgressBrush;
                    break;
                case AFTTestStatus.FAIL:
                    ContASCPassEllipse.Fill = failBrush;
                    break;
                default:
                    ContASCPassEllipse.Fill = unknownBrush;
                    break;
            }
        }
        private void FillContDSCEllipse(AFTTestStatus aftstatus)
        {
            switch (aftstatus)
            {
                case AFTTestStatus.PASS:
                    ContDSCPassEllipse.Fill = successBrush;
                    break;
                case AFTTestStatus.NOTCOMPLETED:
                    ContDSCPassEllipse.Fill = inProgressBrush;
                    break;
                case AFTTestStatus.FAIL:
                    ContDSCPassEllipse.Fill = failBrush;
                    break;
                default:
                    ContDSCPassEllipse.Fill = unknownBrush;
                    break;
            }
        }
        private void FillContBacklashEllipse(AFTTestStatus aftstatus)
        {
            switch (aftstatus)
            {
                case AFTTestStatus.PASS:
                    BacklashPassEllipse.Fill = successBrush;
                    break;
                case AFTTestStatus.NOTCOMPLETED:
                    BacklashPassEllipse.Fill = inProgressBrush;
                    break;
                case AFTTestStatus.FAIL:
                    BacklashPassEllipse.Fill = failBrush;
                    break;
                default:
                    BacklashPassEllipse.Fill = unknownBrush;
                    break;
            }
        }
        private void FillContMotorPerfotmanceEllipse(AFTTestStatus aftstatus)
        {
            switch (aftstatus)
            {
                case AFTTestStatus.PASS:
                    MotorPerformancePassEllipse.Fill = successBrush;
                    break;
                case AFTTestStatus.NOTCOMPLETED:
                    MotorPerformancePassEllipse.Fill = inProgressBrush;
                    break;
                case AFTTestStatus.FAIL:
                    MotorPerformancePassEllipse.Fill = failBrush;
                    break;
                default:
                    MotorPerformancePassEllipse.Fill = unknownBrush;
                    break;
            }
        }

        private void Starttool_Click(object sender, RoutedEventArgs e)
        {
            if (AFT_All_in_process == true || AFT_WALK_in_process == true)
            {
                (App.Current as App).DeptName_RunINFtoApplication = true;
                Main MainWindow = new Main();

                MainWindow.AFTReport(((App.Current as App).DeptName_TesterFirstName), "", "Start", "Continuous Walk", ActualAverageCurrent.ToString(), false, "");
                //send custom command 
                byte[] data = { 0x4f, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier

                Int16 run1Ccounts = WalkRun1Counts; //Walk
                data[4] = (byte)(run1Ccounts & 0xFF);
                data[3] = (byte)(run1Ccounts >> 8);
                data[5] = 1;
                data[8] = 100;
                data[9] = 100;
                byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
                byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);

                MainWindow.RunSystem();
                StartWalkTestRunning = true;
                AFT_WALK_in_process = true;
                WalkOnInProgressTimer.IsEnabled = true;

            }

        }

        short HipMaxAngle;
        short HipMinAngle;
        short KneeMaxAngle;
        short KneeMinAngle;
       
       
      
        private void Delay_ms(double ms)
        {
            double _val = 0;
            Stopwatch stopwatch1 = new Stopwatch();
            stopwatch1.Start();

            while (stopwatch1.ElapsedMilliseconds < ms)
            {
                _val++;
            }
            stopwatch1.Reset();
            _val = 0;

        }
        bool WaitUntillHipsReachDestination(byte joint, int j, short des)
        {

            byte[] data = { joint, 0x03, 0x01, 0, 0, 0, 0, 0, 0, 0 }; //Read average current
            byte[] result;
            short encoder;
            data[0] = joint;
            result = AFTDeviceData.RunCustomCommand(j, data);
            encoder = result[3];
            encoder = (short)((encoder << 8) + result[4]);
            if ((des <= (encoder + 46)) && (des > (encoder - 46)))//2 degrees window
                return true;



            return false;
        }
        bool WaitUntillKneesReachDestination(byte joint, int j, short des)
        {

            byte[] data = { joint, 0x03, 0x01, 0, 0, 0, 0, 0, 0, 0 }; //Read average current
            byte[] result;
            short encoder;
            data[0] = joint;
            result = AFTDeviceData.RunCustomCommand(j, data);
            encoder = result[3];
            encoder = (short)((encoder << 8) + result[4]);

            des = Math.Abs(des);
            encoder = Math.Abs(encoder);

            if ((des <= (encoder + 46)) && (des > (encoder - 46)))//2 degrees window
                return true;



            return false;
        }
        string[] Motors_perf = new string[4];
        private bool CheckMotorPerformance()
        {
            Motors_perf[0] = "FAIL"; Motors_perf[1] = "FAIL"; Motors_perf[2] = "FAIL"; Motors_perf[3] = "FAIL";
            Motors_release_running();
            if (run_hipmotors_test_performance(0x330))
            {
                Motors_perf[0] = "PASS";
                if (run_kneemotors_test_performance(0x331))
                {
                    Motors_perf[1] = "PASS";
                    if (run_hipmotors_test_performance(0x332))
                    {
                        Motors_perf[2] = "PASS";
                        if (run_kneemotors_test_performance(0x333))
                        {
                            Motors_perf[3] = "PASS";
                            return true;
                        }
                    }
                }
            }

            return false;
        }
        private bool run_hipmotors_test_performance(short motor)
        {
            short angle;
            byte Joint = 0;
            int J = 0;
            string m = "";
            if (motor == 0x330)
            {
                Joint = 0x30;
                J = 1;
                m = "LH";
                motors_unrelease[0] = 0x30;
                AFTDeviceData.RunCustomCommand(J, motors_unrelease);
            }
            else if (motor == 0x332)
            {
                Joint = 0x32;
                J = 3;
                m = "RH";
                motors_unrelease[0] = 0x32;
                AFTDeviceData.RunCustomCommand(J, motors_unrelease);
            }

            try
            {
                byte[] result_LH;
                byte[] data_move = { Joint, 3, 0, 0, 0, 0, 0, 3, 0, 0 }; //move Bytes 4&5=angle and 6&7=velocity ='0x0300'   
                byte[] motor_release = { Joint, 3, 0x23, 0, 0, 0, 0, 3, 0, 0 };
                short HIP_MINMUM_ANGLE = (short)( HipMinAngle * 23);
                short HIP_MAXIMUM_ANGLE = (short)(HipMaxAngle * 23);
                angle = HIP_MINMUM_ANGLE;
                bool res = true;


                while (angle <= HIP_MAXIMUM_ANGLE && res)
                {

                    data_move[6] = (byte)(angle);
                    data_move[5] = (byte)(angle >> 8);
                    result_LH = AFTDeviceData.RunCustomCommand(J, data_move);
                    Delay_ms(1000);
                    TimeOut_Cnt = 0;
                    res = false;
                    while (!res && TimeOut_Cnt < 10)
                    {
                        res = WaitUntillHipsReachDestination(Joint, J, angle);
                        Delay_ms(500);
                        TimeOut_Cnt++;
                    }
                    angle += 46 * 2;
                }
                if (!res || TimeOut_Cnt >= 10)
                {
                    MessageBox.Show(m + " has stopped unexpectedly");
                    return false;
                }

                angle -= 46 * 2;
                while (angle >= HIP_MINMUM_ANGLE && res)
                {

                    data_move[6] = (byte)(angle);
                    data_move[5] = (byte)(angle >> 8);
                    result_LH = AFTDeviceData.RunCustomCommand(J, data_move);
                    Delay_ms(500);
                    TimeOut_Cnt = 0;
                    res = false;
                    while (!res && TimeOut_Cnt < 10)
                    {
                        res = WaitUntillHipsReachDestination(Joint, J, angle);
                        Delay_ms(500);
                        TimeOut_Cnt++;

                    }
                    angle -= 46 * 2;
                }

                if (res && TimeOut_Cnt < 10)
                {
                    //    MessageBox.Show(m+" ran all over his spectrum successfully!");

                    //move to 30 degree
                    angle = 0x2c0;
                    data_move[6] = (byte)(angle);
                    data_move[5] = (byte)(angle >> 8);
                    result_LH = AFTDeviceData.RunCustomCommand(J, data_move);
                    Delay_ms(2000);
                    result_LH = AFTDeviceData.RunCustomCommand(J, motor_release);
                    Delay_ms(2000);
                    return true;
                }
                else
                {
                    MessageBox.Show(m + " LH has stopped unexpectedly");
                    return false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Stopped unexpectedly:  " + exception.Message);
                return false;
            }
        }
        private bool run_kneemotors_test_performance(short motor)
        {
            short angle;
            byte Joint = 0;
            int J = 0;
            string m = "";
            if (motor == 0x331)
            {
                Joint = 0x31;
                J = 2;
                m = "LK";
                motors_unrelease[0] = 0x31;
                AFTDeviceData.RunCustomCommand(J, motors_unrelease);
            }
            else if (motor == 0x333)
            {
                Joint = 0x33;
                J = 4;
                m = "RK";
                motors_unrelease[0] = 0x33;
                AFTDeviceData.RunCustomCommand(J, motors_unrelease);
            }

            try
            {
                byte[] result_LH;
                byte[] data_move = { Joint, 3, 0, 0, 0, 0, 0, 3, 0, 0 }; //move Bytes 4&5=angle and 6&7=velocity ='0x0300'   
                byte[] motor_release = { Joint, 3, 0x23, 0, 0, 0, 0, 3, 0, 0 };
                short KNEE_MINMUM_ANGLE = (short)(-KneeMinAngle * 23);
                short KNEE_MAXIMUM_ANGLE = (short)(-KneeMaxAngle * 23);
                angle = KNEE_MINMUM_ANGLE;
                bool res = true;


                while (angle >= KNEE_MAXIMUM_ANGLE && res)
                {

                    data_move[6] = (byte)(angle);
                    data_move[5] = (byte)(angle >> 8);
                    result_LH = AFTDeviceData.RunCustomCommand(J, data_move);
                    Delay_ms(500);
                    TimeOut_Cnt = 0;
                    res = false;
                    while (!res && TimeOut_Cnt < 10)
                    {
                        res = WaitUntillKneesReachDestination(Joint, J, angle);
                        Delay_ms(500);
                        TimeOut_Cnt++;
                    }
                    angle -= 46 * 2;
                }
                if (!res || TimeOut_Cnt >= 10)
                {
                    MessageBox.Show(m + " has stopped unexpectedly");
                    return false;
                }

                angle += 46 * 2;
                while (angle <= KNEE_MINMUM_ANGLE && res)
                {

                    data_move[6] = (byte)(angle);
                    data_move[5] = (byte)(angle >> 8);
                    result_LH = AFTDeviceData.RunCustomCommand(J, data_move);
                    Delay_ms(500);
                    TimeOut_Cnt = 0;
                    res = false;
                    while (!res && TimeOut_Cnt < 10)
                    {
                        res = WaitUntillKneesReachDestination(Joint, J, angle);
                        Delay_ms(500);
                        TimeOut_Cnt++;

                    }
                    angle += 46 * 2;
                }

                if (res && TimeOut_Cnt < 10)
                {
                    // MessageBox.Show(m+" ran all over his spectrum successfully!");
                    //move to 0
         
                    angle = -136;
                    data_move[6] = (byte)(angle);
                    data_move[5] = (byte)(angle >> 8);
                    result_LH = AFTDeviceData.RunCustomCommand(J, data_move);
                    Delay_ms(2000);
                    result_LH = AFTDeviceData.RunCustomCommand(J, motor_release);
                    return true;
                }
                else
                {
                    MessageBox.Show(m + " LH has stopped unexpectedly");
                    return false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Stopped unexpectedly:  " + exception.Message);
                return false;
            }
        }


        private void MotorPerformanceRadBut_Checked(object sender, RoutedEventArgs e)
        {
            (App.Current as App).DeptName_RunINFtoApplication = true;
            Calibratetool.IsEnabled = true;
            AFT_WALK_in_process = false;
            AFT_SITSTAND_in_process = false;
            AFT_ASC_in_process = false;
            AFT_DSC_in_process = false;
            AFT_MotorPerformance_in_process = true;
            AFT_All_in_process = false;
            AFT_Backlash_in_process = false;
            Main MainWindow2 = new Main();
            MainWindow2.AFTReport(((App.Current as App).DeptName_TesterFirstName), "", "Start", "Motors Performance", "", false, "");
            MotorPerformanceOnInProgressTimer.IsEnabled = true;
            if (CheckMotorPerformance())
            {
                SetTestStatus(AFTTestStatus.PASS, true);
                MainWindow2.AFTReport(((App.Current as App).DeptName_TesterFirstName), AFTTestStatus.PASS.ToString(), "End", "Motors Performance", (AFTTestStatus.PASS.ToString() + " -> " + " LH: " + Motors_perf[0] + " LK: " + Motors_perf[1] + " RH: " + Motors_perf[2] + " RK: " + Motors_perf[3]), false, "");

            }
            else
            {
                SetTestStatus(AFTTestStatus.FAIL, true);
                MainWindow2.AFTReport(((App.Current as App).DeptName_TesterFirstName), AFTTestStatus.FAIL.ToString(), "End", "Motors Performance", (AFTTestStatus.FAIL.ToString() + " -> " + " LH: " + Motors_perf[0] + " LK: " + Motors_perf[1] + " RH: " + Motors_perf[2] + " RK: " + Motors_perf[3]), false, "");

            }

        }
        short LH_encoder, LK_encoder, RH_encoder, RK_encoder;
        private void ReadAEncodersMax()
        {
            byte[] data = { 0x30, 0x03, 0x01, 0, 0, 0, 0, 0, 0, 0 }; //Read average current
            byte[] result;

           

            if (LH.IsChecked == true)
            {
                data[0] = 0x30;
                result = AFTDeviceData.RunCustomCommand(1, data);
                LH_encoder = result[3];
                LH_encoder = (short)((LH_encoder << 8) + result[4]);
                lh.Text = LH_encoder.ToString();
                if (!string.IsNullOrEmpty(lh.Text) && !string.IsNullOrEmpty(lh2.Text))
                {
                    Backlash_lh = (Int32)(Math.Abs(Math.Abs(System.Convert.ToInt16(lh.Text)) - Math.Abs(System.Convert.ToInt16(lh2.Text))));
                    lh_b.Text = Backlash_lh.ToString();
                    fBacklash_lh = Backlash_lh;
                    fBacklash_lh = (float)(fBacklash_lh / 22.75);
                    lh_b_d.Text = fBacklash_lh.ToString("0.0###");
                }
            }
            if (LK.IsChecked == true)
            {
                data[0] = 0x31;
                result = AFTDeviceData.RunCustomCommand(2, data);
                LK_encoder = result[3];
                LK_encoder = (short)((LK_encoder << 8) + result[4]);
                lk.Text = LK_encoder.ToString();
                if (!string.IsNullOrEmpty(lk.Text) && !string.IsNullOrEmpty(lk2.Text))
                {
                    Backlash_lk = (Int16)(Math.Abs(Math.Abs(System.Convert.ToInt16(lk.Text)) - Math.Abs(System.Convert.ToInt16(lk2.Text))));
                    lk_b.Text = Backlash_lk.ToString();
                    fBacklash_lk = Backlash_lk;
                    fBacklash_lk = (float)(fBacklash_lk / 22.755);
                    lk_b_d.Text = fBacklash_lk.ToString("0.0###");
                }
            }
            if (RH.IsChecked == true)
            {
                data[0] = 0x32;
                result = AFTDeviceData.RunCustomCommand(3, data);
                RH_encoder = result[3];
                RH_encoder = (short)((RH_encoder << 8) + result[4]);
                rh.Text = RH_encoder.ToString();
                if (!string.IsNullOrEmpty(rh.Text) && !string.IsNullOrEmpty(rh2.Text))
                {
                    Backlash_rh = (Int16)(Math.Abs(Math.Abs(System.Convert.ToInt16(rh.Text)) - Math.Abs(System.Convert.ToInt16(rh2.Text))));
                    rh_b.Text = Backlash_rh.ToString();
                    fBacklash_rh = Backlash_rh;
                    fBacklash_rh = (float)(fBacklash_rh / 22.755);
                    rh_b_d.Text = fBacklash_rh.ToString("0.0###");
                }
            }
            if (RK.IsChecked == true)
            {
                data[0] = 0x33;
                result = AFTDeviceData.RunCustomCommand(4, data);
                RK_encoder = result[3];
                RK_encoder = (short)((RK_encoder << 8) + result[4]);
                rk.Text = RK_encoder.ToString();
                if (!string.IsNullOrEmpty(rk.Text) && !string.IsNullOrEmpty(rk2.Text))
                {
                    Backlash_rk = (Int16)(Math.Abs(Math.Abs(System.Convert.ToInt16(rk.Text)) - Math.Abs(System.Convert.ToInt16(rk2.Text)) ));
                    rk_b.Text = Backlash_rk.ToString();
                    fBacklash_rk = Backlash_rk;
                    fBacklash_rk = (float)(fBacklash_rk / 22.755);
                    rk_b_d.Text = fBacklash_rk.ToString("0.0###");
                }
            }



        }

        float fBacklash_lh, fBacklash_lk, fBacklash_rh, fBacklash_rk;
        private void ReadAEncodersMin()
        {
            byte[] data = { 0x30, 0x03, 0x01, 0, 0, 0, 0, 0, 0, 0 }; //Read average current
            byte[] result;

           

            if (LH.IsChecked == true)
            {
                data[0] = 0x30;
                result = AFTDeviceData.RunCustomCommand(1, data);
                LH_encoder = result[3];
                LH_encoder = (short)((LH_encoder << 8) + result[4]);
                lh2.Text = LH_encoder.ToString();
                if (!string.IsNullOrEmpty(lh.Text) && !string.IsNullOrEmpty(lh2.Text))
                {
                    Backlash_lh = (Int32)(Math.Abs(Math.Abs(System.Convert.ToInt16(lh.Text)) - Math.Abs(System.Convert.ToInt16(lh2.Text))));
                    lh_b.Text = Backlash_lh.ToString();

                    fBacklash_lh = Backlash_lh;
                    fBacklash_lh = (float)(fBacklash_lh/22.755);
                    lh_b_d.Text = fBacklash_lh.ToString("0.0###");

                }
            }
            if (LK.IsChecked == true)
            {
                data[0] = 0x31;
                result = AFTDeviceData.RunCustomCommand(2, data);
                LK_encoder = result[3];
                LK_encoder = (short)((LK_encoder << 8) + result[4]);
                lk2.Text = LK_encoder.ToString();
                if (!string.IsNullOrEmpty(lk.Text) && !string.IsNullOrEmpty(lk2.Text))
                {
                    Backlash_lk = (Int16)(Math.Abs(Math.Abs(System.Convert.ToInt16(lk.Text)) - Math.Abs(System.Convert.ToInt16(lk2.Text))));
                    lk_b.Text = Backlash_lk.ToString();
                    fBacklash_lk = Backlash_lk;
                    fBacklash_lk = (float)(fBacklash_lk / 22.755);
                    lk_b_d.Text = fBacklash_lk.ToString("0.0###");
                }
            }
            if (RH.IsChecked == true)
            {
                data[0] = 0x32;
                result = AFTDeviceData.RunCustomCommand(3, data);
                RH_encoder = result[3];
                RH_encoder = (short)((RH_encoder << 8) + result[4]);
                rh2.Text = RH_encoder.ToString();
                if (!string.IsNullOrEmpty(rh.Text) && !string.IsNullOrEmpty(rh2.Text))
                {
                    Backlash_rh = (Int16)(Math.Abs(Math.Abs(System.Convert.ToInt16(rh.Text)) - Math.Abs(System.Convert.ToInt16(rh2.Text))));
                    rh_b.Text = Backlash_rh.ToString();
                    fBacklash_rh = Backlash_rh;
                    fBacklash_rh = (float)(fBacklash_rh / 22.755);
                    rh_b_d.Text = fBacklash_rh.ToString("0.0###");
                }
            }
            if (RK.IsChecked == true)
            {
                data[0] = 0x33;
                result = AFTDeviceData.RunCustomCommand(4, data);
                RK_encoder = result[3];
                RK_encoder = (short)((RK_encoder << 8) + result[4]);
                rk2.Text = RK_encoder.ToString();
                if (!string.IsNullOrEmpty(rk.Text) && !string.IsNullOrEmpty(rk2.Text))
                {
                    Backlash_rk = (Int16)(Math.Abs(Math.Abs(System.Convert.ToInt16(rk.Text)) - Math.Abs(System.Convert.ToInt16(rk2.Text))));
                    rk_b.Text = Backlash_rk.ToString();
                    fBacklash_rk = Backlash_rk;
                    fBacklash_rk = (float)(fBacklash_rk / 22.755);
                    rk_b_d.Text = fBacklash_rk.ToString("0.0###");
                }
            }



        }

        private void BacklashRadBut_Checked(object sender, RoutedEventArgs e)
        {
            ReadEncoders1.IsEnabled = true;
            ReadEncoders2.IsEnabled = true;
            backlash.IsEnabled = true;          
            AFT_Backlash_in_process = true;
            AFT_WALK_in_process = false;
            AFT_SITSTAND_in_process = false;
            AFT_ASC_in_process = false;
            AFT_DSC_in_process = false;
            AFT_All_in_process = false;
            AFT_MotorPerformance_in_process = false;
            Main MainWindow2 = new Main();
            MainWindow2.AFTReport(((App.Current as App).DeptName_TesterFirstName), "", "Start", "Backlash", ActualAverageCurrent.ToString(), false, "");
        //    MessageBox.Show("Type calculated Backlash of each joint and then click Backlash calculate.", "Backlash", MessageBoxButton.OK, MessageBoxImage.Information);
            BacklashOnInProgressTimer.IsEnabled = true;
      
            
        }

        private void ContSitStandRadBut_Checked(object sender, RoutedEventArgs e)
        {
            (App.Current as App).DeptName_RunINFtoApplication = true;
            Main MainWindow2 = new Main();
            MainWindow2.AFTReport(((App.Current as App).DeptName_TesterFirstName), "", "Start", "Continuous Sit_Stand", ActualAverageCurrent.ToString(), false, "");
            //send custom command 
            byte[] data = { 0x4e, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
            Int16 run1Ccounts = SitStandRun1Counts;//SitStand
            data[4] = (byte)(run1Ccounts & 0xFF);
            data[3] = (byte)(run1Ccounts >> 8);
            data[5] = 1;
            data[9] = 100; data[8] = 100;
            byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
            byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
            MainWindow2.RunSystem();
            StartASCTestRunning = false;
            StartSitStandTestRunning = true;
            AFT_DSC_in_process = false;
            AFT_ASC_in_process = false;
            AFT_SITSTAND_in_process = true;
            AFT_All_in_process = false;
            AFT_WALK_in_process = false;
            AFT_Backlash_in_process = false;
            AFT_MotorPerformance_in_process = false;
            CurrentRunningTestTimer.IsEnabled = true;
            SITSTANDOnInProgressTimer.IsEnabled = true;
           
        }

        private void ContASCRadBut_Checked(object sender, RoutedEventArgs e)
        {
            //go to ASC    
            (App.Current as App).DeptName_RunINFtoApplication = true;    
            Main MainWindow2 = new Main();
            MainWindow2.AFTReport(((App.Current as App).DeptName_TesterFirstName), "", "Start", "Continuous ASC", ActualAverageCurrent.ToString(), false, "");
            //send custom command 
            byte[] data = { 0x5a, 0, 1, 0, 10, 0, 0, 0, 0, 0 }; //cont walk of 5 steps - 10 bytes include identifier
            Int16 run1Ccounts = ASCRun1Counts; //ASC
            data[4] = (byte)(run1Ccounts & 0xFF);
            data[3] = (byte)(run1Ccounts >> 8);
            data[5] = 1;
            data[9] = 100; data[8] = 100;
            byte[] result_LK = AFTDeviceData.RunCustomCommand(5, WDG_Enable);
            byte[] result_LH = AFTDeviceData.RunCustomCommand(5, data);
            MainWindow2.RunSystem();
            StartASCTestRunning = true;
            StartDSCTestRunning = false;
            AFT_DSC_in_process = false;
            AFT_ASC_in_process = true;
            AFT_All_in_process = false;
            AFT_SITSTAND_in_process = false;
            AFT_WALK_in_process = false;
            AFT_MotorPerformance_in_process = false;
            CurrentRunningTestTimer.IsEnabled = true;
            ASCOnInProgressTimer.IsEnabled = true;
         
        }

        private void Reports_Click(object sender, RoutedEventArgs e)
        {

            if (Directory.Exists("\\Argo\\AFT"))
            {
                Process.Start(@"C:\Argo\AFT");
            }
            else
            {
                MessageBox.Show("The folder 'AFT' doesn't exist yet!");
            }

        }

        private void ReadEncodersMAx_Click(object sender, RoutedEventArgs e)
        {
            if ((App.Current as App).DeptName_Connected)
            {
                if (LH.IsChecked == true || LK.IsChecked == true || RH.IsChecked == true || RK.IsChecked == true)
                {
                    ReadAEncodersMax();
                }
                else
                {
                    MessageBox.Show("One joint should be checked at least first!", "User", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                MessageBox.Show("The USB should be connected first!", "User", MessageBoxButton.OK, MessageBoxImage.Information);

            }

        }

        private void ReadEncodersMIN_Click(object sender, RoutedEventArgs e)
        {
            if ((App.Current as App).DeptName_Connected)
            {
                if (LH.IsChecked == true || LK.IsChecked == true || RH.IsChecked == true || RK.IsChecked == true)
                {
                    ReadAEncodersMin();
                }
                else
                {
                    MessageBox.Show("One joint should be checked at least first!", "User", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                MessageBox.Show("The USB should be connected first!", "User", MessageBoxButton.OK, MessageBoxImage.Information);

            }
        }

     

       

        private void LHRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            lh.Text = "";
            lh2.Text = "";
            lh_b.IsEnabled = false;
        }

        private void RHRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            rh.Text = "";
            rh2.Text = "";
            rh_b.IsEnabled = false;
        }

        private void RKRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            rk.Text = "";
            rk2.Text = "";
            rk_b.IsEnabled = false;
        }

        private void LKRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            lk.Text = "";
            lk2.Text = "";
            lk_b.IsEnabled = false;
        }

        private void LH_Checked(object sender, RoutedEventArgs e)
        {
            LK.IsChecked = false;
            RH.IsChecked = false;
            RK.IsChecked = false;

        }

        private void LK_Checked(object sender, RoutedEventArgs e)
        {
            LH.IsChecked = false;
            RH.IsChecked = false;
            RK.IsChecked = false;
        }

        private void RH_Checked(object sender, RoutedEventArgs e)
        {
            LH.IsChecked = false;
            LK.IsChecked = false;
            RK.IsChecked = false;
        }

        private void RK_Checked(object sender, RoutedEventArgs e)
        {
            LH.IsChecked = false;
            RH.IsChecked = false;
            LK.IsChecked = false;
        }

    









    }

}
