﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rewalk
{
    [TemplatePart(Name="slider")]
    public class OnOffBox : Control
    {
        static OnOffBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(OnOffBox), new FrameworkPropertyMetadata(typeof(OnOffBox)));
        }

        public bool IsOn
        {
            get { return (bool)GetValue(IsOnProperty); }
            set { SetValue(IsOnProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsOn.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsOnProperty =
            DependencyProperty.Register("IsOn", typeof(bool), typeof(OnOffBox), new UIPropertyMetadata(true));

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            Border aa = GetTemplateChild("slider") as Border;
            if (aa != null)
            {
                aa.MouseLeftButtonUp += new MouseButtonEventHandler(OnOffBox_MouseLeftButtonUp);
            }
        }

        void OnOffBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            IsOn = !IsOn;

            if (OnChanged != null)
                OnChanged(this, EventArgs.Empty);
        }

        public event EventHandler OnChanged;
    }
}
