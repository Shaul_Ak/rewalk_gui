﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Xml;

namespace Rewalk
{
   sealed class SplitStringConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                ReadOnlyCollection<XmlNode> nodes = value as ReadOnlyCollection<XmlNode>;
                if (nodes != null && nodes.Count > 0)
                {
                    return nodes[0].InnerText.Split(new char[] { ',', ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
