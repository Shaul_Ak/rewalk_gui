﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Rewalk
{
    /// <summary>
    /// Interaction logic for Report_Progress.xaml
    /// </summary>
    public partial class Report_Progress : Window
    {
        public Report_Progress()
        {
            InitializeComponent();
           
        }

        public void AppendProgress(string text)
        {
            this.rtfUserReport.AppendText(text);
        }
        
        public void Close_Report()
        {
            //rtfUserReport.AppendText("Please contact technical support before using the system again.");
            btnClose.IsEnabled = true;
        }
    
        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            Close(); //this.Close();
        }

      
    }
}
